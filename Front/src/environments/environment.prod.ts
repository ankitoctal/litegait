export const environment = {
  production: true,
  siteName :'LiteGait',
  //apiUrl : 'http://14.98.110.245:5003/api', 
  //serverUrl : 'http://14.98.110.245:5003',
  apiUrl : 'https://mobilityresearch.org:5003/api', 
  serverUrl : 'https://mobilityresearch.org:5003/',
  galleryDir : '/gallery',

  instagramUrl : 'https://www.instagram.com/',
  facebookUrl : 'https://www.facebook.com/',
  linkedinUrl : 'https://www.linkedin.com/',
  twitterUrl : 'https://www.twitter.com/',
  vimeoUrl:'https://vimeo.com/',
  bingUrl :'https://www.bing.com/',
};
