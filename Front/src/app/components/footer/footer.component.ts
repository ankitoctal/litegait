import { Component, OnInit } from '@angular/core';

import { environment } from '../../../environments/environment';
import { CommonService } from '../../services/common.service';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {
	
	env = environment;	
  	menu1Data:any = [];
  	menu2Data:any = [];
	
	constructor(private commonService: CommonService) { }

	ngOnInit() {

		this.commonService.getMenuList('footer1').subscribe((response:any) => {
  				this.menu1Data = response.data;   				      		
    	});
    	this.commonService.getMenuList('footer2').subscribe((response:any) => {
  				this.menu2Data = response.data;       		
    	});

	}

}
