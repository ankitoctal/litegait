import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { Title,Meta }  from '@angular/platform-browser';
import { ActivatedRoute } from '@angular/router';
import { environment } from '../../../environments/environment';
import { CommonService } from '../../services/common.service';
import { WebinarService } from '../../services/webinar.service';


@Component({
    selector: 'app-gallery',
    templateUrl: './gallery.component.html',
    styleUrls: ['./gallery.component.css']
})
export class GalleryComponent implements OnInit {

    env = environment;
    galleryListPhotoData: any = [];
    galleryListVideoData: any = [];
    galleryData: any = [];
    slug = '';
    keyword: any = [];
    keywordtmp: any = [];
    p = 1;
    p2 = 1;
    itemsPerPage = 18;
    itemsPerPage2 = 5;
    filterMetadata = { count: 0 };
    webinars: any = [];
    webinar_categories: any = [];
    show: boolean = false; 
    tags: any = '' ; 
    dropdownList = [];
    selectedItems = [];
    dropdownSettings = {};
    orderByType:boolean = false;
    old_gallery: any = {};
    counter = 0;

    constructor(private commonService: CommonService, private webinarService: WebinarService,  private titleService: Title, private activatedRoute: ActivatedRoute, private sanitizer: DomSanitizer) { }

    ngOnInit() {

        this.slug = this.activatedRoute.snapshot.paramMap.get('slug');
        this.titleService.setTitle(environment.siteName + ' - Gallery');

        if (this.slug != null) {
            this.commonService.getGalleryView(this.slug).subscribe((response: any) => {
                this.galleryData = response.data;
            });
        } else {
            this.commonService.getGalleryList().subscribe((response: any) => {
                this.galleryListPhotoData = response.data_photo;                
                this.galleryListVideoData = response.data_video;
            });
        }

        this.webinarService.getWebinarList(null, null, null).subscribe((response: any) => {
            this.webinar_categories = response.categories;
            this.webinars = response.data;
            console.log(this.webinars);return false;
        });


        this.commonService.getTagLsit().subscribe((response: any) => {            
            this.tags = response.data; 
            this.dropdownList = this.tags; 
        });


        this.dropdownSettings = {
          singleSelection: false,
          enableCheckAll: false,
          idField: '_id',
          textField: 'name',
          selectAllText: 'Select All',
          unSelectAllText: 'UnSelect All',
          itemsShowLimit: 3,
          allowSearchFilter: false,
          maxHeight: 150,
        };

    }

    vimeoURL(url: string) {
        const vimeoRegex = /(?:vimeo)\.com.*(?:videos|video|channels|)\/([\d]+)/i;
        const parsed = url.match(vimeoRegex);
        return url = '//player.vimeo.com/video/' + parsed[1];
        //return this.sanitizer.bypassSecurityTrustResourceUrl(url);
    }

    onbtnClick(view: string){        
        if(view == 'list'){
            this.show = true; 
        }else {
            this.show = false; 
        }
    }

  	onItemSelect(item: any) {    
	    if(this.keyword){
	        var i = 0; 
	        var temp: any = Array(); 
	        this.keyword.forEach(function (arrayItem) {            
	             temp[i] =  arrayItem.name; 
	             i++; 
	        });        
	        this.keywordtmp = temp; 
	    }   
	}

 	onItemUnSelect(item: any) {    
	    if(this.keyword){
	        var i = 0; 
	        var temp: any = Array(); 
	        this.keyword.forEach(function (arrayItem) {            
	            temp[i] =  arrayItem.name; 
	            i++ ; 
	        });        
	        this.keywordtmp = temp; 
	    }
  	}
  	onSelectAll(items: any) {    
  	
  	}

  	galelryTrack(val: any){      
    	this.old_gallery = val; 
  	}

  	startCounter(){
  		this.counter = 1;
  	}
  	updateCounter(){
  		this.counter++;
  	}


}
