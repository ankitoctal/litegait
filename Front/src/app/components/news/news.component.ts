import { Component, OnInit } from '@angular/core';
import { Title,Meta }  from '@angular/platform-browser';
import { Router, ActivatedRoute, Params } from '@angular/router';

import { CommonService } from '../../services/common.service';
import { environment } from '../../../environments/environment';

@Component({
    selector: 'app-news',
    templateUrl: './news.component.html',
    styleUrls: ['./news.component.css']
})
export class NewsComponent implements OnInit {

    env = environment;
    newsData: any = [];
    slug = '';
    p = 1;
    itemsPerPage = 10;
    filterMetadata = { count: 0 };
    keyword = '';

    constructor(private commonService: CommonService, private titleService: Title, private activatedRoute: ActivatedRoute) { }

    ngOnInit() {

        this.slug = this.activatedRoute.snapshot.paramMap.get('slug');
        this.titleService.setTitle(environment.siteName + ' - News');

        if (this.slug != null) {
            this.commonService.getNewsView(this.slug).subscribe((response: any) => {
                this.newsData = response.data;
            });
        } else {
            this.commonService.getNewsList().subscribe((response: any) => {
                this.newsData = response.data;
            });
        }

    }

}
