import { Component, OnInit } from '@angular/core';
import { Title, Meta } from '@angular/platform-browser';
import { FormBuilder, FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';
import * as $ from 'jquery';

import { AuthGuard } from '../../services/authguard.service';
import { environment } from '../../../environments/environment';
import { OrderService } from '../../services/order.service';

@Component({
	selector: 'app-checkout',
	templateUrl: './checkout.component.html',
	styleUrls: ['./checkout.component.css']
})
export class CheckoutComponent implements OnInit {

	cart: any = [];
	env = environment;
	billingForm: FormGroup;
	formError: any = [];
	user;
	couponCode = '';
	couponSuccessMsg = '';
	couponErrorMsg = '';
	show_shipping: boolean = false; 

	constructor(private authGuard: AuthGuard, private orderService: OrderService, private titleService: Title, private router: Router, private activatedRoute: ActivatedRoute) {

		this.router.routeReuseStrategy.shouldReuseRoute = function () {
			return false;
		};
	}

	ngOnInit() {
		this.user = this.authGuard.getUser();
		this.cart = JSON.parse(localStorage.getItem("mycart"));
		this.titleService.setTitle(environment.siteName + ' - Checkout');
  		if(this.cart && this.cart.items.length > 0){
          this.cart.items.forEach((message) => {
              // console.log(message.type); 
              if(message.type != "webinar"){
                this.show_shipping = true; 
              }
          });
        }
		this.initBillingForm();
	}

	initBillingForm() {
		if(this.show_shipping){
			this.billingForm = new FormGroup({
				'billing_fname': new FormControl('', [
					Validators.required,
				]),
				'billing_lname': new FormControl(),
				'billing_phone': new FormControl('', [
					Validators.required,
				]),
				'billing_email': new FormControl('', [
					Validators.required,
				]),
				'billing_addr1': new FormControl('', [
					Validators.required,
				]),
				'billing_addr2': new FormControl(),
				'billing_city': new FormControl('', [
					Validators.required,
				]),
				'billing_state': new FormControl('', [
					Validators.required,
				]),
				'billing_country': new FormControl('', [
					Validators.required,
				]),
				'billing_zipcode': new FormControl('', [
					Validators.required,
					Validators.pattern("^[0-9]*$")
				]),

				'shipping_fname': new FormControl('', [
					Validators.required,
				]),
				'shipping_lname': new FormControl(),
				'shipping_phone': new FormControl('', [
					Validators.required,
				]),
				'shipping_email': new FormControl('', [
					Validators.required,
				]),
				'shipping_addr1': new FormControl('', [
					Validators.required,
				]),
				'shipping_addr2': new FormControl(),
				'shipping_city': new FormControl('', [
					Validators.required,
				]),
				'shipping_state': new FormControl('', [
					Validators.required,
				]),
				'shipping_country': new FormControl('', [
					Validators.required,
				]),
				'shipping_zipcode': new FormControl('', [
					Validators.required,
					Validators.pattern("^[0-9]*$")
				]),
			});
		}else{
			this.billingForm = new FormGroup({
				'billing_fname': new FormControl('', [
					Validators.required,
				]),
				'billing_lname': new FormControl(),
				'billing_phone': new FormControl('', [
					Validators.required,
				]),
				'billing_email': new FormControl('', [
					Validators.required,
				]),
				'billing_addr1': new FormControl('', [
					Validators.required,
				]),
				'billing_addr2': new FormControl(),
				'billing_city': new FormControl('', [
					Validators.required,
				]),
				'billing_state': new FormControl('', [
					Validators.required,
				]),
				'billing_country': new FormControl('', [
					Validators.required,
				]),
				'billing_zipcode': new FormControl('', [
					Validators.required,
					Validators.pattern("^[0-9]*$")
				])
			});
		}

		var email = localStorage.getItem('email');
		this.billingForm.controls.shipping_email.setValue(email);
		this.billingForm.controls.billing_email.setValue(email);


	}

	sameAsBilling() {

		//this.billingForm.controls.shipping_fname.setValue(this.billingForm.controls.billing_fname.value);

		$(document).ready(()=>{
			if($('#same_as_billing').is(":checked"))
			{
				this.billingForm.controls.shipping_fname.setValue(this.billingForm.controls.billing_fname.value);
				this.billingForm.controls.shipping_lname.setValue(this.billingForm.controls.billing_lname.value);
				this.billingForm.controls.shipping_phone.setValue(this.billingForm.controls.billing_phone.value);
				this.billingForm.controls.shipping_email.setValue(this.billingForm.controls.billing_email.value);
				this.billingForm.controls.shipping_addr1.setValue(this.billingForm.controls.billing_addr1.value);
				this.billingForm.controls.shipping_addr2.setValue(this.billingForm.controls.billing_addr2.value);
				this.billingForm.controls.shipping_city.setValue(this.billingForm.controls.billing_city.value);
				this.billingForm.controls.shipping_state.setValue(this.billingForm.controls.billing_state.value);
				this.billingForm.controls.shipping_state.setValue(this.billingForm.controls.billing_state.value);
				this.billingForm.controls.shipping_country.setValue(this.billingForm.controls.billing_country.value);
				this.billingForm.controls.shipping_zipcode.setValue(this.billingForm.controls.billing_zipcode.value);

				$('.shippingdiv').hide();
			}
			else
			{
				$('.shippingdiv').show();
			}	
		});		
	}

	submitAddressForm() {		
		if (this.billingForm.valid) {
			localStorage.setItem("checkout_address", JSON.stringify(this.billingForm.value));
			this.router.navigate(['/payment']);
		} else {
			this.validateAllFormFields(this.billingForm);
		}
	}

	validateAllFormFields(formGroup: FormGroup) {
		Object.keys(formGroup.controls).forEach(field => {			
			const control = formGroup.get(field);
			if (control instanceof FormControl) {
				control.markAsTouched({ onlySelf: true });
			} else if (control instanceof FormGroup) {
				this.validateAllFormFields(control);
			}
		});
	}

	applyCoupon() {

		if (this.couponCode == "") {
			this.couponErrorMsg = "Please Enter Coupon Code";
			return true;
		}

		this.orderService.checkCouponCode(this.couponCode).subscribe((response: any) => {

			if (response.status == 'success') {
				this.cart.discount = response.discount;
				this.cart.discountcode = response.discountcode;
				this.cart.total = (this.cart.subtotal - response.discount).toFixed(2);
				this.couponSuccessMsg = response.msg;
				this.couponErrorMsg = '';				

				localStorage.setItem("mycart", JSON.stringify(this.cart));

				return true;
			} else {
				this.cart.discount = 0;
				this.cart.discountcode = '';				
				this.cart.total = this.cart.subtotal;
				this.couponErrorMsg = response.msg;
				this.couponSuccessMsg = '';

				localStorage.setItem("mycart", JSON.stringify(this.cart));
				return true;
			}

		});

	}

}
