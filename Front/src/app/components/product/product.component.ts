import { Component, OnInit, ViewChild, ElementRef, TemplateRef} from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Title, Meta } from '@angular/platform-browser';
import { DomSanitizer, SafeResourceUrl, SafeUrl } from '@angular/platform-browser';
import * as $ from 'jquery';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { environment } from '../../../environments/environment';
import { ProductService } from '../../services/product.service';

@Component({
    selector: 'app-product',
    templateUrl: './product.component.html',
    styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {

    env = environment;
    product: any = [];
    banner_image = '';
    cart: any = [];
    specification_model = '';
    specification_row = '';
    datasheetStr:any;
	@ViewChild('template') template:ElementRef;

  	modalRef: BsModalRef;

    constructor(private modalService: BsModalService, private productService: ProductService, private router: Router, private activatedRoute: ActivatedRoute, private titleService: Title, private meta: Meta, private sanitizer: DomSanitizer) {
        this.router.routeReuseStrategy.shouldReuseRoute = function () {
            return false;
        };
    }

    ngOnInit() {
        let slug = this.activatedRoute.snapshot.paramMap.get('slug');
        this.productService.getProduct(slug).subscribe((response: any) => {
            this.product = response.data;  
            if(this.product.banner_image){
                this.banner_image = this.product.banner_image;
            }else{
                this.banner_image = this.product.category.banner_image;
            }
            this.titleService.setTitle(environment.siteName + ' - ' + response.data.meta_title);    
            this.meta.addTag({ name: 'description', content: response.data.meta_description });      

        });
    }

    ngAfterViewInit() {
        $(document).ready(function () {

            setTimeout(function () {

                (function ($) {

                    (<any>$('.device-slider')).owlCarousel({
                        autoplay: false,
                        autoplayTimeout: 1000,
                        autoplayHoverPause: true,
                        pagination: false,
                        navigation: true,
                        navigationText: ['<img src=\'assets/images/device-slider-left.png\' alt=\'\' />', '<img src=\'assets/images/device-slider-right.png\' alt=\'\' />'],
                        items: 4,
                        margin: 5,
                        loop: true,

                        //itemsDesktop: [1199, 4],
                        itemsDesktopSmall: [991, 3],
                        itemsTablet: [767, 2],
                        itemsMobile: [575, 1],
                    });

                   var slider =  (<any>$('#image-gallery')).lightSlider({
                        gallery: true,
                        item: 1,
                        thumbItem: 4,
                        slideMargin: 0,
                        speed: 500,
                        auto: false,
                        loop: false,
                        thumbnail:true,
                        onSliderLoad: function () {
                            $('#image-gallery').removeClass('cS-hidden');
                        }
                    });
                    $('#goToPrevSlide').on('click', function () {
                        slider.goToPrevSlide();
                    });
                    $('#goToNextSlide').on('click', function () {
                        slider.goToNextSlide();
                    });
                    // (<any>$('.cloud-zoom')).CloudZoom();

                })(jQuery);

            }, 5000);
        });
    }

    vimeoURL(url: string) {
        if (url == undefined) {
            return this.sanitizer.bypassSecurityTrustResourceUrl('#');
        }


        let vimeoRegex = /(?:vimeo)\.com.*(?:videos|video|channels|)\/([\d]+)/i;
        let parsed = url.match(vimeoRegex);
        url = '//player.vimeo.com/video/' + parsed[1];
        return this.sanitizer.bypassSecurityTrustResourceUrl(url);
    }

    videoURL(url) {
        if (url == undefined) {
            return this.sanitizer.bypassSecurityTrustResourceUrl("#");
        }

        let video = this.env.serverUrl + this.env.galleryDir + url;
        return this.sanitizer.bypassSecurityTrustResourceUrl(video);
    }

    stringtoArray(str) {

        if (str == undefined || str == '') {
            return new Array();
        }

        str = str.replace('model,', '');        
        let array = str.split(',');        
        return array;
    }

    checkSpecification(itemid,str){
    	if (str != undefined) {            
        	let array = str.split(',');         	      	
        	var found = array.includes(itemid);        	
        	if(found == true)
        		return true;
        	else
        	    return false;
			
        }       
    }
    asIsOrder(a, b) {
        return 1;
    }

    getSpecificationkey(obj){    	
        /* for(let key in this.product.productSpecification[0]) {
        	console.log(key);
        } */
        if(obj){
        	var keys = Object.keys(obj[0]);
			keys.splice(0, 1); // becuase we dont need _id
        	return keys;
        }	
    }

    /* specificationAddRow(){    	
    	if(this.specification_row !=""){
    		if(this.product.specification_row_default=="")
    		{
    			this.product.specification_row_default = this.specification_row;
    		}
    		else
    		{
    			this.product.specification_row_default = this.product.specification_row_default+','+this.specification_row;
    		}	
    		this.specification_row='';    		
    	}    	
	} */
	
	specificationAddRow(){
    	if (this.specification_row != "") {
			var arr = this.specification_row.split(':');
			this.product.specification_row_default[arr[0]] = arr[1];
    		this.specification_row = '';
		}
    }

    specificationAddModel(){    	
    	if(this.specification_model !=""){
    		this.product.specification_default = this.product.specification_default+','+this.specification_model;
    		this.specification_model='';
    	}
	}

	specificationRemoveRow(str) {
		delete this.product.specification_row_default[str];
    }

    /* specificationRemoveRow(str){    	    	
    	let array = this.product.specification_row_default.split(',');
    	var key = Object.keys(array).find(key => array[key] === str);
    	array.splice(key, 1);	
    	this.product.specification_row_default = array.toString();
    } */

    specificationRemoveModel(str){    	    	
    	let array = this.product.specification_default.split(',');
    	var key = Object.keys(array).find(key => array[key] === str);
    	array.splice(key, 1);	
    	this.product.specification_default = array.toString();
    }




}
