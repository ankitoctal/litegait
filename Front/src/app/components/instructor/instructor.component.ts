import { Component, OnInit } from '@angular/core';
import { Title,Meta }  from '@angular/platform-browser';
import {Router, ActivatedRoute, Params} from '@angular/router';
import * as $ from 'jquery';

import { environment } from '../../../environments/environment';
import { WebinarService } from '../../services/webinar.service';

@Component({
  selector: 'app-instructor',
  templateUrl: './instructor.component.html',
  styleUrls: ['./instructor.component.css']
})
export class InstructorComponent implements OnInit {

	env = environment;
  	instructor:any = [];     	

  	constructor(private webinarService: WebinarService, private titleService: Title, private meta: Meta, private router: Router, private activatedRoute: ActivatedRoute) { 
  	}

  	ngOnInit() {
               
          var slug = this.activatedRoute.snapshot.paramMap.get('slug');

          this.webinarService.getInstructorView(slug).subscribe((response:any) => { 
    			this.instructor = response.data; 
          if(response.data.meta_title)
    			  this.titleService.setTitle(environment.siteName + ' - '+response.data.meta_title);	 
          if(response.data.meta_description)	
            this.meta.addTag({ name: 'description', content: response.data.meta_description });     		
        });   		  
  	}

  	ngAfterViewInit() {
 		/* $(document).ready(function(){
 			setTimeout(function(){
	 			var weblist = $('.webinarlist ul li').length;
	 			var semlist = $('.seminarlist ul li').length;
	 			if(weblist == 0)
	 			{
	 				$('.webinarlist').hide();
	 			}

	 			if(semlist == 0)
	 			{
	 				$('.seminarlist').hide();
	 			}
 			},5000); 
 		}); */
 	}		

  	checkRecording(date){
  		if(new Date(date).getTime() < new Date().getTime())
  			return true;
  		else
  			return false;

  	}

}
