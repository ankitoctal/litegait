import { Component, OnInit } from '@angular/core';
import { Title, Meta }  from '@angular/platform-browser';
import {Router, ActivatedRoute, Params} from '@angular/router';
import { FormBuilder, FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';
import { HttpClient} from '@angular/common/http';
import { FlashMessagesService } from 'angular2-flash-messages';

import { AuthGuard } from '../../services/authguard.service';
import { UserService } from '../../services/user.service';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

	userForm: FormGroup;
	error = '';
	routeData: any;

  	constructor(private authGuard: AuthGuard, private userService: UserService, private http: HttpClient, private _flashMessagesService: FlashMessagesService, private router: Router, private activatedRoute: ActivatedRoute, private titleService: Title) { }

	ngOnInit() {

		this.titleService.setTitle(environment.siteName + ' - Login');

		this.activatedRoute.queryParams.subscribe(params => {
            var uid = params['user_id'];
            if(uid)
            {
            	this.userService.getUser(uid).subscribe(response => {
            		this.authGuard.setUser(response["data"]);
					localStorage.setItem('login_as_admin', '1');
            		this.router.navigate(['/user/dashboard']);
            	});	
            }
        })

		this.routeData = this.activatedRoute.snapshot.data;
		if (this.routeData['page'] == 'logout') {
			this.logOut();
		}

  		this.userForm = new FormGroup({
      		'email': new FormControl('', [
        		Validators.required,
        		Validators.email,
      		]),
      		'password': new FormControl('', [
        		Validators.required,
        		Validators.minLength(6),
      		]),
    	});
  	}

  	public submitForm() {

  		var str = this.userService.loginUser(this.userForm.value).subscribe(response => {

       		if (response["status"] == 'success') {
       			this.authGuard.setUser(response["data"]);
       			if (this.routeData['page'] == 'checkout') {
					this.router.navigate(['/checkout']);
				}
				else
				{
  					this.router.navigate(['/user/dashboard']);
  				}	
  			} else {
  				this.error = response["msg"];

  				setTimeout(()=>{
      				this.error = "";
 				}, 10000);
  			}
    	});

  	}

  	public logOut() {
  		this.authGuard.deleteUser();
  		this._flashMessagesService.show('Logout Successfull.', { cssClass: 'alert-success', timeout: 5000 });
		this.router.navigate(['/login']);
  	}

}
