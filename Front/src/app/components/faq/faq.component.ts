import { Component, OnInit } from '@angular/core';
import {Router, ActivatedRoute, Params} from '@angular/router';
import * as $ from 'jquery';

import { CommonService } from '../../services/common.service';

@Component({
  selector: 'app-faq',
  templateUrl: './faq.component.html',
  styleUrls: ['./faq.component.css']
})
export class FaqComponent implements OnInit {

  	faqData: any = [];
  	catData: any = [];
  	categories: any = [];
  	related: any = [];
  	p = 1;
    itemsPerPage = 100;
    filterMetadata = { count: 0 };
    keyword = '';
    orderByType = false;
    pagetype = "";
    slug = "";

  	constructor(private commonService: CommonService, private router: Router, private activatedRoute: ActivatedRoute) {

  		this.router.routeReuseStrategy.shouldReuseRoute = function() {
        	return false;
     	};
  	}

  	ngOnInit() {
  		this.slug = this.activatedRoute.snapshot.paramMap.get('slug');
  		this.pagetype = this.activatedRoute.snapshot.paramMap.get('pagetype');

  		if (this.pagetype == 'faq' && this.slug) {
  			this.commonService.getFaqView(this.slug).subscribe((response: any) => {
				this.faqData = response.data;
				this.related = response.related;
	    	});
  		} else if (this.pagetype == 'category' && this.slug) {
  			this.commonService.getFaqCategoryList(this.slug).subscribe((response: any) => {
				this.faqData = response.data;
				this.categories = response.categories;
	    	});
  		} else { 
	  		this.commonService.getFaqList().subscribe((response: any) => {
				//this.faqData = response.data;
				this.categories = response.categories;
	    	});
  		}


  	}

  	getLength(obj) {
  		return Object.keys(obj).length;
	}
	  
	childStringToArray(str) {
		var nameArr = str.split(',');
		return nameArr;
	}
  	//

  	ngAfterViewInit() {
 		$(document).ready(function() {

 			setTimeout(function() {

      			(function ($) {

					//$('[data-toggle="tooltip"]').tooltip();

        	})(jQuery);

    		}, 1000);
  		});
 	}

}
