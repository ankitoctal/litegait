import { Component, OnInit } from '@angular/core';
import { Title,Meta }  from '@angular/platform-browser';
import { DomSanitizer, SafeResourceUrl, SafeUrl } from '@angular/platform-browser';
import {Router, ActivatedRoute, Params} from '@angular/router';

import { environment } from '../../../environments/environment';
import { PartService } from '../../services/part.service';

@Component({
  selector: 'app-part',
  templateUrl: './part.component.html',
  styleUrls: ['./part.component.css']
})
export class PartComponent implements OnInit {

	env = environment;
  	part:any = [];      	    
  	categories:any = [];  
  	banner_image='';
  	type='parts';
  	
  	constructor(private partService: PartService, private titleService: Title,private meta: Meta,private router: Router, private activatedRoute: ActivatedRoute, private sanitizer: DomSanitizer) { 
  		this.router.routeReuseStrategy.shouldReuseRoute = function(){
        	return false; 
     	}
     }

  	ngOnInit() {

  		this.titleService.setTitle(environment.siteName + ' - Part List');	

        var slug = this.activatedRoute.snapshot.paramMap.get('slug');                

  		this.partService.getPart(slug).subscribe((response:any) => { 
            this.part = response.data; 
            console.log(this.part);
			this.type = this.part.type; 
			//this.banner_image = this.part.category.banner_image;
			if(this.part.banner_image){
                this.banner_image = this.part.banner_image;
            }else{
                this.banner_image = this.part.category.banner_image;
            }	            	
			this.categories = response.categories;		
			if(response.data.meta_title)
				this.titleService.setTitle(environment.siteName + ' - ' + response.data.meta_title);  
			if(response.data.meta_description)
				this.meta.addTag({ name: 'description', content: response.data.meta_description });  	
        }); 
  	}

  	ngAfterViewInit() {
      $(document).ready(function(){

      setTimeout(function(){
            
            (function ($) {

            (<any>$('.device-slider')).owlCarousel({
                autoplay: false,
                autoplayTimeout: 1000,
                autoplayHoverPause: true,
                pagination: false,
                navigation: true,
                navigationText: ["<img src='assets/images/device-slider-left.png' alt='' />", "<img src='assets/images/device-slider-right.png' alt='' />"],
                items: 4,
                margin: 5,
                loop: true,

                itemsDesktop: [1199, 1],
                itemsDesktopSmall: [991, 3],
                itemsTablet: [768, 2],
                itemsMobile: [479, 1],
            });

     		(<any>$('#image-gallery')).lightSlider({
                gallery: true,
                item: 1,
                thumbItem: 4,
                slideMargin: 0,
                speed: 500,
                auto: false,
                loop: false,
                onSliderLoad: function () {
                    $('#image-gallery').removeClass('cS-hidden');
                }
            });

          })(jQuery);

        },5000);            
      }); 
    }


    vimeoURL(url: string) {
        if (url == undefined) {
            return this.sanitizer.bypassSecurityTrustResourceUrl('#');
        }


        let vimeoRegex = /(?:vimeo)\.com.*(?:videos|video|channels|)\/([\d]+)/i;
        let parsed = url.match(vimeoRegex);
        url = '//player.vimeo.com/video/' + parsed[1];
        return this.sanitizer.bypassSecurityTrustResourceUrl(url);
    }

    videoURL(url) {
        if (url == undefined) {
            return this.sanitizer.bypassSecurityTrustResourceUrl("#");
        }

        let video = this.env.serverUrl + this.env.galleryDir + url;
        return this.sanitizer.bypassSecurityTrustResourceUrl(video);
    }

}
