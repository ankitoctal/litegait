import { Component, OnInit } from '@angular/core';
import { Title, Meta } from '@angular/platform-browser';
import { FormBuilder, FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';
import {Router, ActivatedRoute, Params} from '@angular/router';
import { FlashMessagesService } from 'angular2-flash-messages';

import { AuthGuard } from '../../services/authguard.service';
import { environment } from '../../../environments/environment';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-guestcheckout',
  templateUrl: './guestcheckout.component.html',
  styleUrls: ['./guestcheckout.component.css']
})
export class GuestcheckoutComponent implements OnInit {

  	userForm: FormGroup;
	error = '';

  	constructor(private authGuard: AuthGuard, private userService: UserService, private _flashMessagesService: FlashMessagesService, private router: Router, private titleService: Title) { }

  	ngOnInit() {
  		this.titleService.setTitle(environment.siteName + ' - Register');
  		this.userForm = new FormGroup({
      		'email': new FormControl('', [
        		Validators.required,
        		Validators.email,
      		]),
    	});
  	}

  	public submitForm() {
  		let str = this.userService.addGuestUser(this.userForm.value).subscribe(response => {
       		if (response["status"] == 'success') {
       			// this._flashMessagesService.show('Countinue checkout as guest user.', { cssClass: 'alert-success', timeout: 5000 });
      			this.authGuard.setUser(response["data"]);
				this.router.navigate(['/checkout']);
  			} else {
  				this.error = response["msg"];
  			}
    	});
  	}

}
