import { Component, OnInit } from '@angular/core';
import {Router, ActivatedRoute, Params} from '@angular/router';
import { Title,Meta }  from '@angular/platform-browser';

import { AuthGuard } from '../../../services/authguard.service';
import { environment } from '../../../../environments/environment';
import { UserService } from '../../../services/user.service';

@Component({
  selector: 'app-myorder',
  templateUrl: './myorder.component.html',
  styleUrls: ['./myorder.component.css']
})
export class MyorderComponent implements OnInit {

    env = environment;   
  	order;
    user;

  	constructor(private userService: UserService, private authGuard: AuthGuard, private titleService: Title, private activatedRoute: ActivatedRoute) { }

  	ngOnInit() {

  		this.titleService.setTitle(environment.siteName + ' - Order View');

  		var order_id = this.activatedRoute.snapshot.paramMap.get('order_id');

  		this.user = this.authGuard.getUser();	  		
  		this.userService.getOrder(order_id).subscribe((response:any) => { 
	  		this.order = response.data;	    
	    });
  	}

}
