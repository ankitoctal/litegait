import { Component, OnInit } from '@angular/core';
import {Router, ActivatedRoute, Params} from '@angular/router';
import { Title,Meta }  from '@angular/platform-browser';

import { AuthGuard } from '../../../services/authguard.service';
import { environment } from '../../../../environments/environment';
import { UserService } from '../../../services/user.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

    env = environment;   
  	orders;
    user;

  	constructor(private userService: UserService, private authGuard: AuthGuard, private titleService: Title) { }

  	ngOnInit() {

  		this.titleService.setTitle(environment.siteName + ' - My Profile');	

  		this.user = this.authGuard.getUser();	  		
  		this.userService.getOrders(this.user.user_id).subscribe((response:any) => { 
	  		this.orders = response.data;	  		  		
	    });
  	}

}
