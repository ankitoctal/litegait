import { Component, OnInit } from '@angular/core';
import { Title,Meta }  from '@angular/platform-browser';
import { FormBuilder, FormGroup, FormControl, Validators,AbstractControl } from '@angular/forms';
import { HttpClient} from '@angular/common/http';
import { FlashMessagesService } from 'angular2-flash-messages';
import {Router} from "@angular/router"

import { UserService } from '../../../services/user.service';
import { environment } from '../../../../environments/environment';


@Component({
  selector: 'app-forgotpassword',
  templateUrl: './forgotpassword.component.html',
  styleUrls: ['./forgotpassword.component.css']
})
export class ForgotpasswordComponent implements OnInit {


  	userForm: FormGroup; 
  	error ='';		

  	constructor(private userService: UserService,private http:HttpClient,private _flashMessagesService: FlashMessagesService,private router: Router,private titleService: Title) { }

  	ngOnInit() {  

  		this.titleService.setTitle(environment.siteName + ' - Forgot Password');

  		this.userForm = new FormGroup({
      		'email': new FormControl('',[
        		Validators.required,
        		Validators.email,         		
      		])    		
    	});
  	}

  	public submitForm(){ 	


  		var str = this.userService.sendForgotPasswordMail(this.userForm.value).subscribe(response => {       		
       		if(response["status"] == 'success')
       		{
       			this._flashMessagesService.show('Please check your email to reset password.', { cssClass: 'alert-success', timeout: 5000 });       
  			}
  			else
  			{
  				this.error = response["msg"];
  			}	
    	});
    	 
  	}



}





