import { Component, OnInit } from '@angular/core';
import {Router, ActivatedRoute, Params} from '@angular/router';
import { Title,Meta }  from '@angular/platform-browser';

import { AuthGuard } from '../../../services/authguard.service';
import { environment } from '../../../../environments/environment';
import { UserService } from '../../../services/user.service';

@Component({
  selector: 'app-mywebinar',
  templateUrl: './mywebinar.component.html',
  styleUrls: ['./mywebinar.component.css']
})
export class MywebinarComponent implements OnInit {

  	env = environment;   
  	orders;
    user;

  	constructor(private userService: UserService, private authGuard: AuthGuard, private titleService: Title) { }

  	ngOnInit() {

  		this.titleService.setTitle(environment.siteName + ' - My Webinar');	

  		this.user = this.authGuard.getUser();	  		
  		this.userService.getWebinars(this.user.user_id).subscribe((response:any) => { 
	  		this.orders = response.data;	  		  		
	    });
  	}

}
