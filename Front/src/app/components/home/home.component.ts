import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { FormBuilder, FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';
import * as $ from 'jquery';

import { HomeService } from '../../services/home.service';
import { CategoryService } from '../../services/category.service';
import { environment } from '../../../environments/environment';

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

    bannerData;
    featuredProductMain: any = {};
    featuredProducts: any = [];
    categories: any = [];
    products: any = [];
    webinars: any = [];
    testimonials: any = [];
    news: any = [];
    env = environment;

    constructor(private homeService: HomeService, private categoryService: CategoryService, private router: Router, private activatedRoute: ActivatedRoute) { }

    ngOnInit() {

        this.homeService.getBanner().subscribe((response: any) => {
            this.bannerData = response.data;
            console.log(this.bannerData);
			this.showCarousel();
        });

        this.homeService.getFeaturedProduct().subscribe((response: any) => {
            this.featuredProductMain = response.featured_product_main;
            this.featuredProducts = response.featured_product;
        });

        this.categoryService.getList('product').subscribe((response: any) => {
            this.categories = response.data;
            this.products = response.products;
        });

        this.homeService.getUpcomingWebinar().subscribe((response: any) => {
            this.webinars = response.data;
        });

        this.homeService.getTestimonails().subscribe((response: any) => {
            this.testimonials = response.data;
        });

        this.homeService.getLatestNews().subscribe((response: any) => {
            this.news = response.data;  
        });

	}

	showCarousel(){
		$(document).ready(function () {
			setTimeout(function () {
				(function ($) {	
					(<any>$('.home_banner')).owlCarousel({
						autoplay: true,
						autoplayTimeout: 1000,
						slideSpeed: 800,
						autoplayHoverPause: true,
						nav: true,
						dots: true,
						navigation: true,
						pagination: true,
						navigationText: ["<img src='assets/images/top_slider_left.png' alt='' />", "<img src='assets/images/top_slider_right.png' alt='' />"],
						items: 1,
						margin: 0,
						loop: true,
						itemsDesktop: [1199, 1],
						itemsDesktopSmall: [980, 1],
						itemsTablet: [768, 1],
						itemsMobile: [479, 1],
					});
				})(jQuery);
				$('.slider_bottom').css('display','block');
			}, 1000);
		});	
	}

    ngAfterViewInit() {
        $(document).ready(function () {

            setTimeout(function () {

                (function ($) {

					$('.support_section .tab_menu li:first').addClass('active');
                    $('.support_section .tab-content .tab-pane:first').addClass('active');

                    (<any>$('.newslist')).owlCarousel({
                        autoplay: true,
                        autoplayTimeout: 1000,
                        slideSpeed: 800,
                        autoplayHoverPause: true,
                        nav: true,
                        dots: true,
                        navigation: true,
                        pagination: true,
                        //navigationText:["<img src='assets/images/top_slider_left.png' alt='' />","<img src='assets/images/top_slider_right.png' alt='' />"],
                        items: 1,
                        margin: 0,
                        loop: true,
                        itemsDesktop: [1199, 1],
                        itemsDesktopSmall: [980, 1],
                        itemsTablet: [768, 1],
                        itemsMobile: [479, 1],
					});

					(<any>$('.overview_slider')).owlCarousel({
                        autoplay: true,
                        autoplayTimeout: 1000,
                        autoplayHoverPause: true,
                        pagination: false,
                        navigation: true,
                        navigationText: ["<img src='assets/images/arrow-left.png' alt='' />", "<img src='assets/images/arrow-right.png' alt='' />"],
                        items: 4,
                        margin: 5,
                        loop: true,
					});

					/*
                    (<any>$('.home_banner1')).owlCarousel({
                        autoplay: true,
                        autoplayTimeout: 1000,
                        slideSpeed: 800,
                        autoplayHoverPause: true,
                        nav: true,
                        dots: true,
                        navigation: true,
                        pagination: true,
                        navigationText: ["<img src='assets/images/top_slider_left.png' alt='' />", "<img src='assets/images/top_slider_right.png' alt='' />"],
                        items: 1,
                        margin: 0,
                        loop: true,
                        itemsDesktop: [1199, 1],
                        itemsDesktopSmall: [980, 1],
                        itemsTablet: [768, 1],
                        itemsMobile: [479, 1],
					});

					$('.slider_bottom1').css('display','block');

				     (<any>$('.support')).owlCarousel({
				        autoplay: true,
				        autoplayTimeout: 1000,
				        slideSpeed:500,
				        autoplayHoverPause: true,
				        navigation:true,
				        pagination:false,
				        navigationText:["<img src='assets/images/arrow-left.png' alt='' />","<img src='assets/images/arrow-right.png' alt='' />"],
				        items: 1,
				        margin: 0,
				        loop: true,
				        itemsDesktop : [1199,1],
				    	itemsDesktopSmall : [980,1],
				    	itemsTablet: [768,1],
				    	itemsMobile : [479,1],
				    }); */


                    /* (<any>$('.overview_slider')).owlCarousel({
                        autoplay: true,
                        autoplayTimeout: 1000,
                        autoplayHoverPause: true,
                        pagination: false,
                        navigation: true,
                        navigationText: ["<img src='assets/images/arrow-left.png' alt='' />", "<img src='assets/images/arrow-right.png' alt='' />"],
                        items: 4,
                        margin: 5,
                        loop: true,
					}); */

                })(jQuery);

			}, 5000);

        });
    }

}
