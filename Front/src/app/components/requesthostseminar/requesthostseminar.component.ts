import { Component, OnInit } from '@angular/core';
import {Router, ActivatedRoute, Params} from '@angular/router';
import { Title,Meta }  from '@angular/platform-browser';
import { DomSanitizer, SafeResourceUrl, SafeUrl } from '@angular/platform-browser';
import { FormBuilder, FormGroup, FormControl, Validators,AbstractControl } from '@angular/forms';
import { FlashMessagesService } from 'angular2-flash-messages';

import { environment } from '../../../environments/environment';
import { WebinarService } from '../../services/webinar.service';

@Component({
  selector: 'app-requesthostseminar',
  templateUrl: './requesthostseminar.component.html',
  styleUrls: ['./requesthostseminar.component.css']
})
export class RequesthostseminarComponent implements OnInit {

  	env = environment;    
    seminar:any = [];    
    quoteForm: any = {};    
    error :'';    
    slug ="";

  	constructor(private webinarService: WebinarService, private router: Router, private activatedRoute: ActivatedRoute,private titleService: Title,private _flashMessagesService: FlashMessagesService) {
		
  	}

  	ngOnInit() {       
  		
  		this.slug = this.activatedRoute.snapshot.paramMap.get('slug');
  		this.webinarService.getSeminarView(this.slug).subscribe((response:any) => {             
            this.seminar = response.data;  
            this.titleService.setTitle(environment.siteName + ' - '+response.data.title+' - Request To Host Seminar');				
	    });

  	}

  	public submitForm(){
  	  	this.quoteForm.seminar_id = this.seminar._id;             
  		var str = this.webinarService.saveWebinarServiceForm(this.quoteForm).subscribe(response => {       		
       		if(response["status"] == 'success')
       		{
       			this._flashMessagesService.show('Thank you for your Request! Our service representatives will call you as soon as possible.', { cssClass: 'alert-success', timeout: 5000 });
  				this.router.navigate(['/seminar/'+this.seminar.slug]);                
          	}
          	else
          	{
  				this.error = response["msg"];
          	}	
    	});
  	}	

  	stringtoArray(str)
    {      
		if(str == undefined)
		return new Array();			
		
		var array = str.split(';');					
		return array;
    }


}
