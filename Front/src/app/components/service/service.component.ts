import { Component, OnInit } from '@angular/core';
import { Title, Meta, DomSanitizer}  from '@angular/platform-browser';
import * as $ from 'jquery';
import { Router,ActivatedRoute } from '@angular/router';
import { HttpClient} from '@angular/common/http';

import { environment } from '../../../environments/environment';
import { CategoryService } from '../../services/category.service';
import { ServicesService } from '../../services/services.service';
import { HomeService } from '../../services/home.service';

@Component({
    selector: 'app-service',
    templateUrl: './service.component.html',
    styleUrls: ['./service.component.css']
})
export class ServiceComponent implements OnInit {


	env = environment;
	categories:any = [];          
	products:any = [];  
	bottom_banner: any = null; 
	serialno='';
	error='';
	res:any;
    bannerData :any;

    constructor(private categoryService: CategoryService,private servicesService: ServicesService, private titleService: Title, private activatedRoute: ActivatedRoute, private router: Router , private sanitizer: DomSanitizer,private http:HttpClient,private homeService: HomeService) { }

    ngOnInit() {

          this.titleService.setTitle(environment.siteName + ' - Product List');    

          this.categoryService.getList('product').subscribe((response:any) => { 
              this.categories = response.data;  
              this.products = response.products;                            
          });

          this.servicesService.getServiceBanner().subscribe((response:any) => {               
              this.bottom_banner = response.data;                          
          });

          this.homeService.getBanner().subscribe((response: any) => {
			this.bannerData = response.data;
        });


    }

    ngAfterViewInit() {
        $(document).ready(function () {

            setTimeout(function () {

                (function ($) {

                    (<any>$('.product_search_slider')).owlCarousel({
                        autoplay: true,
                        autoplayTimeout: 1000,
                        slideSpeed: 800,
                        autoplayHoverPause: true,
                        nav: true,
                        dots: true,
                        navigation: true,
                        pagination: true,
                        navigationText: ["<img src='assets/images/top_slider_left.png' alt='' />", "<img src='assets/images/top_slider_right.png' alt='' />"],
                        items: 4,
                        margin: 5,
                        loop: true,
                        itemsDesktop: [1199, 4],
                        itemsDesktopSmall: [991, 3],
                        itemsTablet: [768, 2],
                        itemsMobile: [479, 1],
                    });
                    
					(<any>$('.overview_slider')).owlCarousel({
                        autoplay: true,
                        autoplayTimeout: 1000,
                        autoplayHoverPause: true,
                        pagination: false,
                        navigation: true,
                        navigationText: ["<img src='assets/images/arrow-left.png' alt='' />", "<img src='assets/images/arrow-right.png' alt='' />"],
                        items: 4,
                        margin: 5,
                        loop: true,
                    });
                    
                })(jQuery);

            }, 1000);
        });
    }

    searchBySerialNo(){    	

    	this.http.post(environment.apiUrl+"/product/searchbyserialno",{'serialno':this.serialno}).subscribe((response) => {      						
			this.res = response;			
			if(this.res.status == 'success') 			
			{
				this.router.navigate(['/service/product/'+this.res.data.slug]);
			}
			else if(this.res.serial != undefined && this.res.serial.name !='') 			
			{
				this.router.navigate(['/page/service-contact-us']);
			}

			this.error = this.res.msg;
        });
    }

}
