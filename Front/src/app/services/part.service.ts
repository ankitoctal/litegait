import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { map } from 'rxjs/operators';

import { environment } from '../../environments/environment';
import { AuthGuard } from './authguard.service';

@Injectable({
  providedIn: 'root'
})
export class PartService {
	
	languageCode='en';
	regionCode='us';

  	constructor(private http: HttpClient, private authGuard: AuthGuard) { }

   	getList(type,category,product) {   		 
    	this.languageCode = this.authGuard.getUserLanguageCode();				
    	this.regionCode = this.authGuard.getUserRegionCode();
		  return this.http.post(environment.apiUrl+'/parts/list',{'languageCode':this.languageCode,'regionCode':this.regionCode,'type':type,'category':category,'is_product':product});   		
    }

    getPart(slug) { 
    	this.languageCode = this.authGuard.getUserLanguageCode();				
    	this.regionCode = this.authGuard.getUserRegionCode();
		  return this.http.post(environment.apiUrl+'/parts/view',{'languageCode':this.languageCode,'regionCode':this.regionCode,slug:slug});   		
    }   

}
