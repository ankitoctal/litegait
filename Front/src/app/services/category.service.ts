import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { map } from 'rxjs/operators';

import { environment } from '../../environments/environment';
import { AuthGuard } from './authguard.service';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {
	
	languageCode='en';
	regionCode='us';

  	constructor(private http: HttpClient, private authGuard: AuthGuard) { }

   	getList(type) { 
    	this.languageCode = this.authGuard.getUserLanguageCode();				
    	this.regionCode = this.authGuard.getUserRegionCode();
	   	return this.http.post(environment.apiUrl+'/category/list/'+type,{'languageCode':this.languageCode,'regionCode':this.regionCode});   		
    }

   

}
