import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { map } from 'rxjs/operators';

import { environment } from '../../environments/environment';
import { AuthGuard } from './authguard.service';

@Injectable({
  providedIn: 'root'
})
export class OrderService {
	
	languageCode='en';
	regionCode='us';

  	constructor(private http: HttpClient, private authGuard: AuthGuard) { }

   	saveOrder(orderData) {             
		  return this.http.post(environment.apiUrl+'/order/save',orderData);   		
    }

    checkCouponCode(couponCode) {  
    	var user = this.authGuard.getUser(); 
    	var cart = JSON.parse(localStorage.getItem("mycart"));          
		return this.http.post(environment.apiUrl+'/order/check_coupon_code',{'couponCode':couponCode,'user':user,'cart':cart});   		
    }
    

}
