import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

import { environment } from '../../environments/environment';
import { AuthGuard } from './authguard.service';

@Injectable({
    providedIn: 'root'
})
export class CommonService {

    languageCode = 'en';
    regionCode = 'us';
    pageBanner;

    constructor(private http: HttpClient, private authGuard: AuthGuard) {

        this.http.get(environment.apiUrl + '/common/banner_page').subscribe((response: any) => {
            // localStorage.setItem('pageBanner',JSON.stringify(response.data));
            this.pageBanner = response.data;
        });
    }

    getLanguageList() {
        return this.http.get(environment.apiUrl + '/common/language_list');
    }
    getRegionList() {
        return this.http.get(environment.apiUrl + '/common/region_list');
    }

    getMenuList(menutype) {
        return this.http.get(environment.apiUrl + '/common/menu_list/' + menutype);
    }

    getNewsList() {
        this.languageCode = this.authGuard.getUserLanguageCode();
        return this.http.post(environment.apiUrl + '/common/news_list', { 'languageCode': this.languageCode });
    }

    getNewsView(id) {
        this.languageCode = this.authGuard.getUserLanguageCode();
        return this.http.post(environment.apiUrl + '/common/news_view/' + id, { 'languageCode': this.languageCode });
    }

    getFaqList() {
        this.languageCode = this.authGuard.getUserLanguageCode();
        return this.http.post(environment.apiUrl + '/common/faq_list', { 'languageCode': this.languageCode });
    }
    getFaqCategoryList(slug) {
        this.languageCode = this.authGuard.getUserLanguageCode();
        return this.http.post(environment.apiUrl + '/common/faq_category_list', { 'slug': slug, 'languageCode': this.languageCode });
    }

    getFaqView(id) {
        this.languageCode = this.authGuard.getUserLanguageCode();
        return this.http.post(environment.apiUrl + '/common/faq_view/' + id, { 'languageCode': this.languageCode });
    }


    getGalleryList() {
        this.languageCode = this.authGuard.getUserLanguageCode();
        return this.http.post(environment.apiUrl + '/common/gallery_list', { 'languageCode': this.languageCode });
    }

    getGalleryView(id) {
        this.languageCode = this.authGuard.getUserLanguageCode();
        return this.http.post(environment.apiUrl + '/common/gallery_view/' + id, { 'languageCode': this.languageCode });
    }

    getBlogList() {
        this.languageCode = this.authGuard.getUserLanguageCode();
        return this.http.post(environment.apiUrl + '/common/blog_list', { 'languageCode': this.languageCode });
    }

    getBlogView(id) {
        this.languageCode = this.authGuard.getUserLanguageCode();
        return this.http.post(environment.apiUrl + '/common/blog_view/' + id, { 'languageCode': this.languageCode });
    }

    getElpList(id = null) {
        return this.http.get(environment.apiUrl + '/common/elp_list');
    }

    getElpView(id = null) {
        return this.http.get(environment.apiUrl + '/common/elp_view/' + id);
    }

    saveElpPayment(data) {
        return this.http.post(environment.apiUrl + '/common/elp_payment/', { 'data': data });
    }
    
    saveElpByCoupon(data) { 
        return this.http.post(environment.apiUrl+'/common/elp_save_by_coupon/',{'data':data});           
    }     

    getPageTestimonials(page, id) {
        return this.http.post(environment.apiUrl + '/common/page_testimonials', { 'page': page, '_id': id });
    }


    sendContactEmail(data) {
       return this.http.post(environment.apiUrl+'/common/send_contact_email',{'data':data}); 
    }
    getTagLsit(){
        return this.http.get(environment.apiUrl + '/common/get_tags_list');
    }

    getDistributorList() {
        this.regionCode = this.authGuard.getUserRegionCode();
        return this.http.post(environment.apiUrl + '/common/distributor_list', { 'regionCode': this.regionCode });
    }

    getBulletinList() {        
        return this.http.get(environment.apiUrl + '/common/bulletin_list');
    }

}
