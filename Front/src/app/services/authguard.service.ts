import { Injectable, Output, EventEmitter }     from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { FlashMessagesService } from 'angular2-flash-messages';

@Injectable({
  providedIn: 'root'
})

export class AuthGuard implements CanActivate {

	@Output() change: EventEmitter<any> = new EventEmitter();

	constructor(private router: Router, private _flashMessagesService: FlashMessagesService) {

		var languageCode = localStorage.getItem('languageCode');
		if (languageCode == null || languageCode=="") {
			localStorage.setItem('languageCode', 'en');
			localStorage.setItem('languageName', 'English');			
		}
		var regionCode = localStorage.getItem('regionCode');
		if (regionCode == null || regionCode=="") {
			localStorage.setItem('regionCode', 'NA');			
		}
		
	}

	public isAuthenticated(): boolean {

    	var user_id = localStorage.getItem('user_id');
    	if (user_id != null) {
    		return true;
    	} else {
    		return false;
    	}

  	}

	canActivate() {

		if (this.isAuthenticated() == false) {
			this._flashMessagesService.show('Please login to visit this page!', { cssClass: 'alert-danger', timeout: 5000 });
			this.router.navigate(['/login']);
			return false;
		} else {
			return true;
		}
	}

	getUser() {

		var user = {};
		user["user_id"] = localStorage.getItem('user_id');
		user["name"] = localStorage.getItem('name');
		user["email"] = localStorage.getItem('email');
		user["languageCode"] = localStorage.getItem('languageCode');
		user["languageName"] = localStorage.getItem('languageName');
		user["regionCode"] = localStorage.getItem('regionCode');

		user["elp_purchase"] = localStorage.getItem('elp_purchase');
		user["elp_expire"] = localStorage.getItem('elp_expire');
		user["elp_payment_id"] = localStorage.getItem('elp_payment_id');
		user["elp_plan"] = localStorage.getItem('elp_plan');
		return user;
    }

    setUser(user) {

		localStorage.setItem('user_id', user['_id']);
		localStorage.setItem('name', user['name']);
		localStorage.setItem('email', user['email']);

		localStorage.setItem('elp_purchase', user['elp_purchase']);
		localStorage.setItem('elp_expire', user['elp_expire']);
		localStorage.setItem('elp_payment_id', user['elp_payment_id']);
		localStorage.setItem('elp_plan', user['elp_plan']);		

		this.change.emit('user_update');
    }

    deleteUser() {

		localStorage.removeItem('user_id');
		localStorage.removeItem('name');
		localStorage.removeItem('email');
		localStorage.setItem('login_as_admin', '0');

		this.change.emit('user_update');
    }

    getUserLanguageCode() {
    	var languageCode = localStorage.getItem('languageCode');
    	return languageCode;
    }

    getUserRegionCode() {
    	var regionCode = localStorage.getItem('regionCode');
    	return regionCode;
    }

    changeLanguage(languageCode, languageName) {

    	localStorage.setItem('languageCode', languageCode);
    	localStorage.setItem('languageName', languageName);
    	this.change.emit(languageName);
  	}

  	changeRegion(regionCode) {

    	localStorage.setItem('regionCode', regionCode);
    	this.change.emit(regionCode);
  	}
}
