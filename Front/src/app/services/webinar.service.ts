import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { map } from 'rxjs/operators';

import { environment } from '../../environments/environment';
import { AuthGuard } from './authguard.service';

@Injectable({
  providedIn: 'root'
})
export class WebinarService {
	
	languageCode='en';
	regionCode='us';

  	constructor(private http: HttpClient, private authGuard: AuthGuard) { }

   	getInstructorList() { 
    	this.languageCode = this.authGuard.getUserLanguageCode();				
    	this.regionCode = this.authGuard.getUserRegionCode();
		  return this.http.post(environment.apiUrl+'/webinar/instructor_list',{'languageCode':this.languageCode,'regionCode':this.regionCode});   		
    }

    getInstructorView(slug) { 
    	this.languageCode = this.authGuard.getUserLanguageCode();				
    	this.regionCode = this.authGuard.getUserRegionCode();
		return this.http.post(environment.apiUrl+'/webinar/instructor_view/'+slug,{'languageCode':this.languageCode,'regionCode':this.regionCode,slug:slug});   		
    }

    getWebinarList(type,category, subcat) { 
    	this.languageCode = this.authGuard.getUserLanguageCode();				
    	this.regionCode = this.authGuard.getUserRegionCode();
		  return this.http.post(environment.apiUrl+'/webinar/webinar_list',{'languageCode':this.languageCode,'regionCode':this.regionCode,'type':type,'category':category, 'subcat':subcat});   		
    }

    getWebinarView(slug) { 
    	this.languageCode = this.authGuard.getUserLanguageCode();				
    	this.regionCode = this.authGuard.getUserRegionCode();
		return this.http.post(environment.apiUrl+'/webinar/webinar_view/'+slug,{'languageCode':this.languageCode,'regionCode':this.regionCode,slug:slug});   		
    }

    getSeminarList() { 
    	this.languageCode = this.authGuard.getUserLanguageCode();				
    	this.regionCode = this.authGuard.getUserRegionCode();
		  return this.http.post(environment.apiUrl+'/webinar/seminar_list',{'languageCode':this.languageCode,'regionCode':this.regionCode});   		
    }

    getSeminarView(slug) { 
    	this.languageCode = this.authGuard.getUserLanguageCode();				
    	this.regionCode = this.authGuard.getUserRegionCode();
		return this.http.post(environment.apiUrl+'/webinar/seminar_view/'+slug,{'languageCode':this.languageCode,'regionCode':this.regionCode,slug:slug});   		
    }

    saveWebinarServiceForm(data) {         
        return this.http.post(environment.apiUrl+'/webinar/saverequesthostform',data);      
    }

    purchaseByPlan(data){
       return this.http.post(environment.apiUrl+'/webinar/purchase_by_plan',data);       
    }

    purchaseSeminarByPlan(data){
       return this.http.post(environment.apiUrl+'/webinar/purchase_seminar_by_plan',data);       
    }  


    purchase_webinar_directly(data){
       return this.http.post(environment.apiUrl+'/webinar/purchase_webinar_directly',data);       
    } 

}
