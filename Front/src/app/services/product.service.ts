import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { map } from 'rxjs/operators';

import { environment } from '../../environments/environment';
import { AuthGuard } from './authguard.service';

@Injectable({
  providedIn: 'root'
})
export class ProductService {
	
	languageCode='en';
	regionCode='us';

  	constructor(private http: HttpClient, private authGuard: AuthGuard) { }

   	getList(slug) { 
    	this.languageCode = this.authGuard.getUserLanguageCode();				
    	this.regionCode = this.authGuard.getUserRegionCode();
		  return this.http.post(environment.apiUrl+'/products/list',{'languageCode':this.languageCode,'regionCode':this.regionCode,'category':slug});   		
    }

    getProduct(slug) { 
    	this.languageCode = this.authGuard.getUserLanguageCode();				
    	this.regionCode = this.authGuard.getUserRegionCode();
		  return this.http.post(environment.apiUrl+'/product/view',{'languageCode':this.languageCode,'regionCode':this.regionCode,slug:slug});   		
    }

    getProductQuoteForm(type,slug) { 
      this.languageCode = this.authGuard.getUserLanguageCode();       
      this.regionCode = this.authGuard.getUserRegionCode();
      return this.http.post(environment.apiUrl+'/product/getquoteform',{'languageCode':this.languageCode,'regionCode':this.regionCode,slug:slug,'type':type});      
    }

    saveProductQuoteForm(data) {         
        return this.http.post(environment.apiUrl+'/product/savequoteform',data);      
    }
    saveProductServiceForm(data) {         
        return this.http.post(environment.apiUrl+'/product/saveserviceform',data);      
    }

}
