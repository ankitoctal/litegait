import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { map } from 'rxjs/operators';

import { environment } from '../../environments/environment';
import { AuthGuard } from './authguard.service';

@Injectable({
  providedIn: 'root'
})
export class HomeService {
	
	languageCode='en';

  	constructor(private http: HttpClient, private authGuard: AuthGuard) { }

   	getBanner() { 
		return this.http.get(environment.apiUrl+'/common/banner_list');   		
    }

    getFeaturedProduct() { 
		return this.http.get(environment.apiUrl+'/products/home_page_featured_list');   		
    }

    getUpcomingWebinar() { 
    	this.languageCode = this.authGuard.getUserLanguageCode();				
		return this.http.post(environment.apiUrl+'/webinar/upcoming_list',{'languageCode':this.languageCode,'limit':4});   		
    }

    getTestimonails() { 
		return this.http.post(environment.apiUrl+'/common/testimonail_list',{'page':'home_page'});   		
    }

    getLatestNews() { 
    	this.languageCode = this.authGuard.getUserLanguageCode();
		return this.http.post(environment.apiUrl+'/common/latest_news',{'languageCode':this.languageCode});   		  		
    }

    newsletterSubscribe(newsletterData) { 
		return this.http.post(environment.apiUrl+'/common/newsletter_subscribe',newsletterData);   		
    } 
}
