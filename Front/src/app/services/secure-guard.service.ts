import {Injectable, isDevMode} from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot} from '@angular/router';

import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})

export class IsSecureGuard implements CanActivate {

	canActivate(route: ActivatedRouteSnapshot): boolean {

		/* if (environment.production == true && location.protocol !== 'https:')) {
      		location.href = 'https:' + window.location.href.substring(window.location.protocol.length);
      		return false;
    	} */
    	return true;
 	}

}