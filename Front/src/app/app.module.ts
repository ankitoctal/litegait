import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { FlashMessagesModule } from 'angular2-flash-messages';
import { NgxPaginationModule } from 'ngx-pagination';
import { NgxPayPalModule } from 'ngx-paypal';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';


// for angular-loading-bar
import { LoadingBarHttpClientModule } from '@ngx-loading-bar/http-client';
import { LoadingBarHttpModule } from '@ngx-loading-bar/http';
import { LoadingBarRouterModule } from '@ngx-loading-bar/router';
import { LoadingBarModule } from '@ngx-loading-bar/core';

import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ModalModule } from 'ngx-bootstrap/modal';
import { TooltipModule } from 'ngx-bootstrap/tooltip';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { StaticpageComponent } from './components/staticpage/staticpage.component';
import { RegisterComponent } from './components/register/register.component';
import { LoginComponent } from './components/login/login.component';
import { NewsComponent } from './components/news/news.component';
import { FaqComponent } from './components/faq/faq.component';
import { GalleryComponent } from './components/gallery/gallery.component';
import { BlogComponent } from './components/blog/blog.component';
import { ProductComponent } from './components/product/product.component';
import { CategoryComponent } from './components/category/category.component';
import { ProductlistComponent } from './components/productlist/productlist.component';
import { WebinarlistComponent } from './components/webinarlist/webinarlist.component';
import { WebinarComponent } from './components/webinar/webinar.component';
import { CartComponent } from './components/cart/cart.component';
import { CheckoutComponent } from './components/checkout/checkout.component';
import { PaymentComponent } from './components/payment/payment.component';

import { StringfilterPipe } from './pipe/stringfilter.pipe';
import { SafeUrlPipe } from './pipe/safeurl.pipe';
import { nl2br } from './pipe/nl2br.pipe';
import { replace } from './pipe/replace.pipe';
import { OrderByPipe } from './pipe/order-by.pipe';
import { PinkbarComponent } from './components/shared/pinkbar/pinkbar.component';
import { CategorylistComponent } from './components/categorylist/categorylist.component';
import { RequestquoteComponent } from './components/requestquote/requestquote.component';
import { RequestserviceComponent } from './components/requestservice/requestservice.component';
import { DashboardComponent } from './components/user/dashboard/dashboard.component';
import { ChangepasswordComponent } from './components/user/changepassword/changepassword.component';
import { MyprofileComponent } from './components/user/myprofile/myprofile.component';
import { MyorderComponent } from './components/user/myorder/myorder.component';
import { PartlistComponent } from './components/partlist/partlist.component';
import { PartComponent } from './components/part/part.component';
import { InstructorlistComponent } from './components/instructorlist/instructorlist.component';
import { InstructorComponent } from './components/instructor/instructor.component';
import { SeminarlistComponent } from './components/seminarlist/seminarlist.component';
import { SeminarComponent } from './components/seminar/seminar.component';
import { RequesthostseminarComponent } from './components/requesthostseminar/requesthostseminar.component';
import { LeftbarComponent } from './components/user/leftbar/leftbar.component';
import { MyseminarComponent } from './components/user/myseminar/myseminar.component';
import { MywebinarComponent } from './components/user/mywebinar/mywebinar.component';
import { ElpComponent } from './components/shared/elp/elp.component';
import { ElppaymentComponent } from './components/elppayment/elppayment.component';
import { PagetestimonialsComponent } from './components/shared/pagetestimonials/pagetestimonials.component';
import { SafeHtmlPipe } from './pipe/safe-html.pipe';
import { ServiceComponent } from './components/service/service.component';
import { NewsletterComponent } from './components/shared/newsletter/newsletter.component';
import { ContactusComponent } from './components/shared/contactus/contactus.component';
import { GuestcheckoutComponent } from './components/guestcheckout/guestcheckout.component';
import { ForgotpasswordComponent } from './components/user/forgotpassword/forgotpassword.component';
import { ResetpasswordComponent } from './components/user/resetpassword/resetpassword.component';
import { StriphtmlPipe } from './pipe/striphtml.pipe';
import { ServicedocumentsComponent } from './components/service/servicedocuments/servicedocuments.component';
import { ServiceproductComponent } from './components/service/serviceproduct/serviceproduct.component';
import { ServicecontactusComponent } from './components/service/servicecontactus/servicecontactus.component';
import { BulletinComponent } from './components/bulletin/bulletin.component';
import { EducationComponent } from './components/education/education.component';


@NgModule({
	declarations: [
		AppComponent,
		HomeComponent,
		HeaderComponent,
		FooterComponent,
		StaticpageComponent,
		RegisterComponent,
		LoginComponent,
		DashboardComponent,
		NewsComponent,
		FaqComponent,
		GalleryComponent,
		BlogComponent,
		ProductComponent,
		CategoryComponent,
		ProductlistComponent,
		WebinarlistComponent,
		WebinarComponent,
		CartComponent,
		CheckoutComponent,
		PaymentComponent,
		StringfilterPipe,
		SafeUrlPipe,
		replace,
		nl2br,
		OrderByPipe,
		PinkbarComponent,
		CategorylistComponent,
		RequestquoteComponent,
		RequestserviceComponent,
		ChangepasswordComponent,
		MyprofileComponent,
		MyorderComponent,
		PartlistComponent,
		PartComponent,
		InstructorlistComponent,
		InstructorComponent,
		SeminarlistComponent,
		SeminarComponent,
		RequesthostseminarComponent,
		LeftbarComponent,
		MyseminarComponent,
		MywebinarComponent,
		ElpComponent,
		ElppaymentComponent,
		PagetestimonialsComponent,
		SafeHtmlPipe,
		ServiceComponent,
		NewsletterComponent,
		ContactusComponent,
		GuestcheckoutComponent,
	    ForgotpasswordComponent,
	    ResetpasswordComponent,
	    StriphtmlPipe,
	    ServicedocumentsComponent,
	    ServiceproductComponent,
	    ServicecontactusComponent,
	    BulletinComponent,
	    EducationComponent,
	],
	imports: [
		BrowserModule,
		AppRoutingModule,
		HttpClientModule,
		FormsModule,
		ReactiveFormsModule,
		FlashMessagesModule.forRoot(),
		NgxPaginationModule,
		LoadingBarHttpClientModule,
		LoadingBarHttpModule,
		LoadingBarRouterModule,
		LoadingBarModule,
		NgxPayPalModule,
		ModalModule.forRoot(),
		TooltipModule.forRoot(),
		BrowserAnimationsModule,
		CommonModule,
		NgMultiSelectDropDownModule.forRoot()
	],
	providers: [],
	bootstrap: [AppComponent]
})
export class AppModule { }
