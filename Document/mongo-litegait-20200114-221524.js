
/** banner_page indexes **/
db.getCollection("banner_page").ensureIndex({
  "_id": NumberInt(1)
},[
  
]);

/** banners indexes **/
db.getCollection("banners").ensureIndex({
  "_id": NumberInt(1)
},[
  
]);

/** blog_contents indexes **/
db.getCollection("blog_contents").ensureIndex({
  "_id": NumberInt(1)
},[
  
]);

/** blogs indexes **/
db.getCollection("blogs").ensureIndex({
  "_id": NumberInt(1)
},[
  
]);

/** bulletins indexes **/
db.getCollection("bulletins").ensureIndex({
  "_id": NumberInt(1)
},[
  
]);

/** bulletins indexes **/
db.getCollection("bulletins").ensureIndex({
  "slug": NumberInt(1)
},{
  "unique": true
});

/** case_study indexes **/
db.getCollection("case_study").ensureIndex({
  "_id": NumberInt(1)
},[
  
]);

/** case_study indexes **/
db.getCollection("case_study").ensureIndex({
  "slug": NumberInt(1)
},{
  "unique": true
});

/** case_study_contents indexes **/
db.getCollection("case_study_contents").ensureIndex({
  "_id": NumberInt(1)
},[
  
]);

/** categories indexes **/
db.getCollection("categories").ensureIndex({
  "_id": NumberInt(1)
},[
  
]);

/** categories indexes **/
db.getCollection("categories").ensureIndex({
  "slug": NumberInt(1)
},{
  "unique": true
});

/** category_contents indexes **/
db.getCollection("category_contents").ensureIndex({
  "_id": NumberInt(1)
},[
  
]);

/** coupons indexes **/
db.getCollection("coupons").ensureIndex({
  "_id": NumberInt(1)
},[
  
]);

/** distributors indexes **/
db.getCollection("distributors").ensureIndex({
  "_id": NumberInt(1)
},[
  
]);

/** elp_payment indexes **/
db.getCollection("elp_payment").ensureIndex({
  "_id": NumberInt(1)
},[
  
]);

/** elps indexes **/
db.getCollection("elps").ensureIndex({
  "_id": NumberInt(1)
},[
  
]);

/** emails indexes **/
db.getCollection("emails").ensureIndex({
  "_id": NumberInt(1)
},[
  
]);

/** faq_contents indexes **/
db.getCollection("faq_contents").ensureIndex({
  "_id": NumberInt(1)
},[
  
]);

/** faqs indexes **/
db.getCollection("faqs").ensureIndex({
  "_id": NumberInt(1)
},[
  
]);

/** forms indexes **/
db.getCollection("forms").ensureIndex({
  "_id": NumberInt(1)
},[
  
]);

/** forms indexes **/
db.getCollection("forms").ensureIndex({
  "slug": NumberInt(1)
},{
  "unique": true
});

/** galleries indexes **/
db.getCollection("galleries").ensureIndex({
  "_id": NumberInt(1)
},[
  
]);

/** gallery_contents indexes **/
db.getCollection("gallery_contents").ensureIndex({
  "_id": NumberInt(1)
},[
  
]);

/** languages indexes **/
db.getCollection("languages").ensureIndex({
  "_id": NumberInt(1)
},[
  
]);

/** menus indexes **/
db.getCollection("menus").ensureIndex({
  "_id": NumberInt(1)
},[
  
]);

/** news indexes **/
db.getCollection("news").ensureIndex({
  "_id": NumberInt(1)
},[
  
]);

/** news_contents indexes **/
db.getCollection("news_contents").ensureIndex({
  "_id": NumberInt(1)
},[
  
]);

/** newsletters indexes **/
db.getCollection("newsletters").ensureIndex({
  "_id": NumberInt(1)
},[
  
]);

/** order_address indexes **/
db.getCollection("order_address").ensureIndex({
  "_id": NumberInt(1)
},[
  
]);

/** order_products indexes **/
db.getCollection("order_products").ensureIndex({
  "_id": NumberInt(1)
},[
  
]);

/** orders indexes **/
db.getCollection("orders").ensureIndex({
  "_id": NumberInt(1)
},[
  
]);

/** part_contents indexes **/
db.getCollection("part_contents").ensureIndex({
  "_id": NumberInt(1)
},[
  
]);

/** part_gallery indexes **/
db.getCollection("part_gallery").ensureIndex({
  "_id": NumberInt(1)
},[
  
]);

/** parts indexes **/
db.getCollection("parts").ensureIndex({
  "_id": NumberInt(1)
},[
  
]);

/** parts indexes **/
db.getCollection("parts").ensureIndex({
  "slug": NumberInt(1)
},{
  "unique": true
});

/** product_contents indexes **/
db.getCollection("product_contents").ensureIndex({
  "_id": NumberInt(1)
},[
  
]);

/** product_featured indexes **/
db.getCollection("product_featured").ensureIndex({
  "_id": NumberInt(1)
},[
  
]);

/** product_files indexes **/
db.getCollection("product_files").ensureIndex({
  "_id": NumberInt(1)
},[
  
]);

/** product_gallery indexes **/
db.getCollection("product_gallery").ensureIndex({
  "_id": NumberInt(1)
},[
  
]);

/** product_specification indexes **/
db.getCollection("product_specification").ensureIndex({
  "_id": NumberInt(1)
},[
  
]);

/** product_specification_group indexes **/
db.getCollection("product_specification_group").ensureIndex({
  "_id": NumberInt(1)
},[
  
]);

/** products indexes **/
db.getCollection("products").ensureIndex({
  "_id": NumberInt(1)
},[
  
]);

/** products indexes **/
db.getCollection("products").ensureIndex({
  "slug": NumberInt(1)
},{
  "unique": true
});

/** quote_forms indexes **/
db.getCollection("quote_forms").ensureIndex({
  "_id": NumberInt(1)
},[
  
]);

/** quote_forms indexes **/
db.getCollection("quote_forms").ensureIndex({
  "slug": NumberInt(1)
},{
  "unique": true
});

/** regions indexes **/
db.getCollection("regions").ensureIndex({
  "_id": NumberInt(1)
},[
  
]);

/** requests indexes **/
db.getCollection("requests").ensureIndex({
  "_id": NumberInt(1)
},[
  
]);

/** seminar_contents indexes **/
db.getCollection("seminar_contents").ensureIndex({
  "_id": NumberInt(1)
},[
  
]);

/** seminar_request indexes **/
db.getCollection("seminar_request").ensureIndex({
  "_id": NumberInt(1)
},[
  
]);

/** seminars indexes **/
db.getCollection("seminars").ensureIndex({
  "_id": NumberInt(1)
},[
  
]);

/** seminars indexes **/
db.getCollection("seminars").ensureIndex({
  "slug": NumberInt(1)
},{
  "unique": true
});

/** services indexes **/
db.getCollection("services").ensureIndex({
  "_id": NumberInt(1)
},[
  
]);

/** speaker_contents indexes **/
db.getCollection("speaker_contents").ensureIndex({
  "_id": NumberInt(1)
},[
  
]);

/** speakers indexes **/
db.getCollection("speakers").ensureIndex({
  "_id": NumberInt(1)
},[
  
]);

/** speakers indexes **/
db.getCollection("speakers").ensureIndex({
  "slug": NumberInt(1)
},{
  "unique": true
});

/** states indexes **/
db.getCollection("states").ensureIndex({
  "_id": NumberInt(1)
},[
  
]);

/** static_page_contents indexes **/
db.getCollection("static_page_contents").ensureIndex({
  "_id": NumberInt(1)
},[
  
]);

/** static_pages indexes **/
db.getCollection("static_pages").ensureIndex({
  "_id": NumberInt(1)
},[
  
]);

/** tags indexes **/
db.getCollection("tags").ensureIndex({
  "_id": NumberInt(1)
},[
  
]);

/** testimonials indexes **/
db.getCollection("testimonials").ensureIndex({
  "_id": NumberInt(1)
},[
  
]);

/** topics indexes **/
db.getCollection("topics").ensureIndex({
  "_id": NumberInt(1)
},[
  
]);

/** topics indexes **/
db.getCollection("topics").ensureIndex({
  "slug": NumberInt(1)
},{
  "unique": true
});

/** users indexes **/
db.getCollection("users").ensureIndex({
  "_id": NumberInt(1)
},[
  
]);

/** users indexes **/
db.getCollection("users").ensureIndex({
  "email": NumberInt(1)
},{
  "unique": true
});

/** webinar_contents indexes **/
db.getCollection("webinar_contents").ensureIndex({
  "_id": NumberInt(1)
},[
  
]);

/** webinars indexes **/
db.getCollection("webinars").ensureIndex({
  "_id": NumberInt(1)
},[
  
]);

/** webinars indexes **/
db.getCollection("webinars").ensureIndex({
  "slug": NumberInt(1)
},{
  "unique": true
});

/** banner_page records **/

/** banners records **/
db.getCollection("banners").insert({
  "_id": ObjectId("5c36ecee8ba4672adbd9400a"),
  "title": "",
  "banner_text_one": "",
  "banner_text_two": "",
  "image": "/Banners/banner_litegait_2.png",
  "status": NumberInt(1),
  "created_at": ISODate("2019-01-10T06:57:50.457Z"),
  "updated_at": ISODate("2019-12-05T18:09:50.327Z"),
  "type": "top"
});
db.getCollection("banners").insert({
  "_id": ObjectId("5c36ed408ba4672adbd94014"),
  "title": "",
  "banner_text_one": "",
  "banner_text_two": "",
  "image": "/video/450x2000 front page banner size - for video.gif",
  "status": NumberInt(1),
  "created_at": ISODate("2019-01-10T06:59:12.606Z"),
  "updated_at": ISODate("2019-12-05T16:55:45.830Z"),
  "type": "top"
});
db.getCollection("banners").insert({
  "_id": ObjectId("5c61b126aa4fee7be94b60db"),
  "title": "",
  "banner_text_one": "",
  "banner_text_two": "",
  "image": "/about_video.jpg",
  "status": NumberInt(1),
  "created_at": ISODate("2019-02-11T17:30:14.19Z"),
  "updated_at": ISODate("2019-07-30T14:45:41.277Z"),
  "type": "top"
});
db.getCollection("banners").insert({
  "_id": ObjectId("5ce79c96fb42690c45d4410b"),
  "type": "support",
  "title": "Clinical support",
  "banner_text_one": "<p style=\"text-align:center\">Speak to a Physical Therapist about the clinical use of any of our products, or to ask questions about treatment with a particular client.</p>\r\n\r\n<p style=\"text-align:center\">clinicalsupport@litegait.com</p>\r\n\r\n<p style=\"text-align:center\"><span style=\"font-size:24px\"><a href=\"#\">View More</a></span></p>\r\n",
  "banner_text_two": "",
  "image": "/Banners/homepg_support/clinical support_homepg-02.jpg",
  "status": NumberInt(1),
  "created_at": ISODate("2019-05-24T07:26:14.787Z"),
  "updated_at": ISODate("2019-08-31T00:41:59.164Z")
});
db.getCollection("banners").insert({
  "_id": ObjectId("5ce7af99fb42690c45d44112"),
  "type": "support",
  "title": "Service & Parts",
  "banner_text_one": "<p>Service &amp; Parts&nbsp;Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n\r\n<p style=\"text-align:center\"><a href=\"http://182.156.245.83:2030/litegaitweb/service\">View More</a></p>\r\n",
  "banner_text_two": "",
  "image": "/Banners/homepg_support/service_homepg-01.jpg",
  "status": NumberInt(1),
  "created_at": ISODate("2019-05-24T08:47:21.860Z"),
  "updated_at": ISODate("2019-08-31T00:43:46.197Z")
});
db.getCollection("banners").insert({
  "_id": ObjectId("5ce7afa8fb42690c45d44116"),
  "type": "support",
  "title": "Training",
  "banner_text_one": "<p>Training Training Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n\r\n<p style=\"text-align:center\"><a href=\"http://182.156.245.83:2030/litegaitweb/training\">View More</a></p>\r\n",
  "banner_text_two": "",
  "image": "/Banners/homepg_support/training_homepg.jpg",
  "status": NumberInt(1),
  "created_at": ISODate("2019-05-24T08:47:36.590Z"),
  "updated_at": ISODate("2019-09-13T04:06:55.225Z")
});
db.getCollection("banners").insert({
  "_id": ObjectId("5d3952232a02f42595837231"),
  "type": "top",
  "title": "",
  "banner_text_one": "",
  "banner_text_two": "",
  "image": "/Banners/banner_lgv.png",
  "status": NumberInt(1),
  "created_at": ISODate("2019-07-25T06:54:27.377Z"),
  "updated_at": ISODate("2019-07-30T03:52:19.543Z")
});
db.getCollection("banners").insert({
  "_id": ObjectId("5d53fc64257ed64e047dd0c9"),
  "type": "service",
  "title": "Summer Sale",
  "banner_text_one": "<p>This space is for items on sale or specials<br />\r\nspecial announcements...<br />\r\nit can be a graphic banner that we can turn on and off or it can be the news feed from the home page</p>\r\n",
  "banner_text_two": "",
  "image": "",
  "status": NumberInt(1),
  "created_at": ISODate("2019-08-14T12:19:48.26Z"),
  "updated_at": ISODate("2019-08-14T12:37:51.243Z")
});

/** blog_contents records **/
db.getCollection("blog_contents").insert({
  "_id": ObjectId("5c3708d9f315ad49251397f6"),
  "blog_id": ObjectId("5c3708d9f315ad49251397f5"),
  "lang": "en",
  "short_description": "I remember when my best friend in fifth grade couldn’t make our much-anticipated end-of-the-school-year camping trip because he had just undergone surgery for appendicitis. Now I prevent kids from participating in their school activities for four to six weeks after I remove their appendix. But what is the appendix, why do we have an organ that causes so many problems, and do you need surgery for appendicitis?",
  "content": "<p>I remember when my best friend in fifth grade couldn&rsquo;t make our much-anticipated end-of-the-school-year camping trip because he had just undergone surgery for&nbsp;<a href=\"https://www.health.harvard.edu/a_to_z/appendicitis-a-to-z\">appendicitis</a>. Now I prevent kids from participating in their school activities for four to six weeks after I remove their appendix. But what is the appendix, why do we have an organ that causes so many problems, and do you need surgery for appendicitis?</p>\r\n\r\n<h3>Role of the appendix is unclear</h3>\r\n\r\n<p>The appendix is a fingerlike tube, about three to four inches long, that comes off of the first portion of the colon. It is normally located in the lower right abdomen, just after the small intestine (needed for digestion and absorption) turns into the colon (whose purpose is to reclaim water and remove waste products).</p>\r\n\r\n<p>The true function of the appendix remains unknown today, but one debated theory is that the appendix acts as a storehouse for good bacteria, to reboot the digestive system after a diarrheal illness. Other experts believe the appendix is just a useless remnant from our evolutionary past. Surgical removal of the appendix appears to cause no observable health problems.</p>\r\n\r\n<h3>Today, appendicitis is usually treated with surgery</h3>\r\n\r\n<p>In the medical community, the suffix &ldquo;-itis&rdquo; refers to inflammation (think arthritis, which is inflammation of a joint). Many times, &ldquo;-itis&rdquo; is due to an infection &mdash; pharyngitis, or strep throat, for example. After much research and debate, the cause of &ldquo;-itis&rdquo; of the appendix is still unclear. However, it appears that most causes of appendicitis are infectious agents, such as bacteria, viruses, parasites, or fungi.</p>\r\n\r\n<p>Whatever the cause, whenever there is an obstruction of the entrance to the appendix &mdash; either from swelling or inflammation, or from mechanical blockage, like a hard piece of stool or a tumor &mdash; appendicitis may ensue. The real danger from appendicitis comes from the potential of the appendix to perforate, or burst, which can spread infection throughout the abdomen.</p>\r\n\r\n<p>Even before 1886, when Dr. Reginald Fitz, a Harvard pathologist, first described appendicitis as a surgical disease, physicians had dealt with the pain and complications stemming from this tiny, menacing organ. Today, the standard of care for the treatment of appendicitis remains surgical removal of the appendix (appendectomy), along with intravenous fluids and antibiotics. In fact, appendectomy is one of the most common abdominal operations in the world. It is also the most common emergency general surgical operation performed in the United States. Most appendectomies are performed by the laparoscopic technique, also known as &ldquo;keyhole&rdquo; or minimally invasive surgery. Patients usually remain at the hospital for less than 24 hours post-operatively.</p>\r\n\r\n<h3>Emerging evidence suggests antibiotics alone may be enough to treat appendicitis</h3>\r\n\r\n<p>Many studies have demonstrated that surgery may not be necessary for all cases of appendicitis. A paper published in June 2015 received international visibility and challenged the status quo when antibiotic therapy was compared with surgery for the treatment of appendicitis. The conclusion of the&nbsp;<a href=\"https://jamanetwork.com/journals/jama/fullarticle/2320315\" target=\"_blank\">APPAC trial</a>&nbsp;(APPendicitis ACuta), which ran in Finland from November 2009 to June 2012, was that most patients who were treated with antibiotics for uncomplicated acute appendicitis did not require surgery during the one-year follow-up period. (Uncomplicated appendicitis refers to those cases in which there is no evidence of perforation or abscess formation, and in which the inflammation is mostly confined to the appendix.) Those who eventually did require appendectomy after failure of the antibiotic regimen did not experience significant complications.</p>\r\n\r\n<p>In 2018, the APPAC authors published a&nbsp;<a href=\"https://jamanetwork.com/journals/jama/article-abstract/2703354\" target=\"_blank\">follow-up</a>&nbsp;in which they concluded that six out every 10 patients who were initially treated with antibiotics for uncomplicated acute appendicitis remained disease-free at five years. They again concluded that antibiotic treatment alone appears feasible as an alternative to surgery for uncomplicated acute appendicitis. Many additional studies also support a nonoperative approach to appendicitis. (And having spent almost 15 years in the navy, I know that for sailors suffering from appendicitis at sea, the use of powerful antibiotics has been the standard of care for decades when access to a surgeon is not readily available.)</p>\r\n\r\n<p>As is always the case in scientific research, these studies have many limitations, including basic study design, multiple confounding variables, misinterpretation of results, and intrinsic flaws known to anyone using statistics. You can also find many articles and rebuttals describing the problems with using medication for a &ldquo;surgical disease.&rdquo; So as of now, while we eagerly await more data on the integrity of antibiotics for the safe use and definitive treatment of uncomplicated appendicitis, surgery remains the gold standard.</p>\r\n",
  "created_at": ISODate("2019-01-10T08:56:57.936Z"),
  "updated_at": ISODate("2019-01-10T08:59:07.723Z")
});
db.getCollection("blog_contents").insert({
  "_id": ObjectId("5c3708daf315ad49251397f8"),
  "blog_id": ObjectId("5c3708d9f315ad49251397f5"),
  "lang": "fr",
  "short_description": "",
  "content": "",
  "created_at": ISODate("2019-01-10T08:56:58.112Z"),
  "updated_at": ISODate("2019-01-10T08:59:07.723Z")
});
db.getCollection("blog_contents").insert({
  "_id": ObjectId("5c3708daf315ad49251397fa"),
  "blog_id": ObjectId("5c3708d9f315ad49251397f5"),
  "lang": "Gr",
  "short_description": "",
  "content": "",
  "created_at": ISODate("2019-01-10T08:56:58.117Z"),
  "updated_at": ISODate("2019-01-10T08:59:07.723Z")
});
db.getCollection("blog_contents").insert({
  "_id": ObjectId("5c3708daf315ad49251397fc"),
  "blog_id": ObjectId("5c3708d9f315ad49251397f5"),
  "lang": "Sp",
  "short_description": "",
  "content": "",
  "created_at": ISODate("2019-01-10T08:56:58.120Z"),
  "updated_at": ISODate("2019-01-10T08:59:07.723Z")
});
db.getCollection("blog_contents").insert({
  "_id": ObjectId("5c370903f315ad4925139802"),
  "blog_id": ObjectId("5c370903f315ad4925139801"),
  "lang": "en",
  "short_description": "Very often I encounter women who are far more worried about breast cancer than they are about heart disease. But women have a greater risk of dying from heart disease than from all cancers combined. This is true for women of all races and ethnicities. Yet only about 50% of women realize that they are at greater risk from heart disease than from anything else.",
  "content": "<p>Very often I encounter women who are far more worried about breast cancer than they are about heart disease. But women have a&nbsp;<a href=\"https://www.cdc.gov/women/lcod/2015/all-females/index.htm\" target=\"_blank\">greater risk of dying from heart disease&nbsp;</a>than from all cancers combined. This is true for women of all races and ethnicities. Yet only about&nbsp;<a href=\"https://www.cdc.gov/dhdsp/data_statistics/fact_sheets/fs_women_heart.htm\" target=\"_blank\">50% of women</a>&nbsp;realize that they are at greater risk from heart disease than from anything else.</p>\r\n\r\n<p>Currently in the US,&nbsp;<a href=\"https://seer.cancer.gov/statfacts/html/breast.html\" target=\"_blank\">three million women</a>&nbsp;are living with breast cancer, which causes one in 31 deaths. Almost&nbsp;<a href=\"https://www.ahajournals.org/doi/pdf/10.1161/CIR.0000000000000485\" target=\"_blank\">50 million women</a>&nbsp;have cardiovascular disease, which encompasses heart disease and strokes and causes one in three deaths.</p>\r\n\r\n<p>Here&rsquo;s what&rsquo;s really interesting, though: heart disease and breast cancer share many of the same risk factors. What&rsquo;s more, there are two big risk categories that we can do something about: exercise and diet.</p>\r\n\r\n<h3>Heart disease and breast cancer: How much exercise is needed?</h3>\r\n\r\n<p><a href=\"https://www.ahajournals.org/doi/10.1161/CIR.0000000000000556\" target=\"_blank\">Many studies</a>&nbsp;have shown that the less physically active a woman is, the higher her risks are for cardiovascular disease and breast cancer. Of course, the flip side is that the more physically active she is, the lower her risks.</p>\r\n\r\n<p>How much physical activity is recommended? Well, the latest government&nbsp;<a href=\"https://health.gov/paguidelines/second-edition/10things/\" target=\"_blank\">physical activity guidelines</a>&nbsp;for Americans and the&nbsp;<a href=\"https://www.heart.org/en/healthy-living/fitness/fitness-basics/aha-recs-for-physical-activity-in-adults\" target=\"_blank\">American Heart Association guidelines on activity</a>&nbsp;both call for at least 150 minutes of moderate physical activity weekly. That&rsquo;s only 21 minutes daily. More is better. But by current statistics, less than 18% of women are meeting that minimum of 21 minutes a day. Everything counts! Walking, gardening, taking the stairs, dancing around, cleaning house. Exercise does not have to be at the gym. Avoiding long periods of time sitting is key. So, sit less, move more.</p>\r\n\r\n<h3>Heart disease and breast cancer: How can diet help?</h3>\r\n\r\n<p>Research also shows that a diet high in fruits and vegetables, whole grains, and healthy protein (like seafood, tofu, or beans) and low in refined grains, added sugars, and red and processed meats is associated with a lower risk of both heart disease and breast cancer. The American Cancer Society&nbsp;<a href=\"https://www.cancer.org/healthy/eat-healthy-get-active/acs-guidelines-nutrition-physical-activity-cancer-prevention/summary.html\" target=\"_blank\">nutrition guidelines for cancer prevention</a>&nbsp;and the American Heart Association&nbsp;<a href=\"https://www.heart.org/en/healthy-living/healthy-eating/eat-smart/nutrition-basics/aha-diet-and-lifestyle-recommendations\" target=\"_blank\">nutrition guidelines for heart disease prevention</a>&nbsp;are essentially the same:</p>\r\n\r\n<ul>\r\n\t<li>DO Eat mostly plants, meaning fruits and vegetables; aim for plant proteins like beans, lentils, nuts, and seeds; eat whole grains like brown rice, quinoa, and corn instead of refined grains; if you&rsquo;re going to eat meat, eat fish or poultry.</li>\r\n\t<li>DON&rsquo;T eat refined grains (things made with white flour; white rice); avoid added sugars and sugary beverages; try not to eat red or processed meats or other processed foods with chemicals (like fast foods or frozen dinners).</li>\r\n</ul>\r\n\r\n<h3>What else is important to know?</h3>\r\n\r\n<p>It&rsquo;s critical to understand your risk factors for heart disease &mdash; and what you can do to lower those risks. Sixty-four percent of women who die of heart disease never have any symptoms beforehand. Beyond an unhealthy diet and physical inactivity, other major risk factors include smoking, obesity, diabetes, high cholesterol, high blood pressure, growing older (particularly post-menopause), and a family history of heart disease. It may be important to check your &ldquo;numbers&rdquo; (blood sugars, cholesterol, blood pressure) in order to know if any of these are a problem. For women who have risk factors, we can screen for any heart disease with a coronary artery CT scan.</p>\r\n\r\n<p>It&rsquo;s also important to know that women can have different symptoms of heart disease than men. In my own practice, most of my female patients who have had heart attacks thought they had acid reflux. They experienced a burning feeling in their chest, accompanied by nausea and even burping. One was even seen in urgent care and told that she had acid reflux. The clue in all cases was that the sensation was brought on by activity, not eating.</p>\r\n\r\n<p>Mammograms are very important for breast cancer screening. What age to start them and how often to have them is&nbsp;<a href=\"https://www.health.harvard.edu/blog/screening-mammograms-one-recommendation-may-not-fit-all-2018041613603\">somewhat controversial</a>. It should be individualized to the patient.</p>\r\n\r\n<p>I encourage everyone to meet with their doctor and discuss their risks for heart disease and breast cancer, as further testing may be required.</p>\r\n\r\n<h3>What&rsquo;s the bottom line?</h3>\r\n\r\n<p>Physical activity and a healthy, plant-based diet are key for heart disease and breast cancer prevention. Also, cardiovascular disease and cancer treatment outcomes are better in patients who adopt healthy lifestyle habits, especially&nbsp;<a href=\"https://www.health.harvard.edu/blog/exercise-as-part-of-cancer-treatment-2018061314035\">regular exercise</a>. Basically, a plant-based Mediterranean diet and plenty of physical activity are sensible measures that are important for prevention and even treatment of cardiovascular disease and breast cancer &mdash; both major health issues for women.</p>\r\n",
  "created_at": ISODate("2019-01-10T08:57:39.918Z"),
  "updated_at": ISODate("2019-07-29T10:43:08.34Z")
});
db.getCollection("blog_contents").insert({
  "_id": ObjectId("5c370903f315ad4925139804"),
  "blog_id": ObjectId("5c370903f315ad4925139801"),
  "lang": "fr",
  "short_description": "",
  "content": "",
  "created_at": ISODate("2019-01-10T08:57:39.920Z"),
  "updated_at": ISODate("2019-07-29T10:43:08.34Z")
});
db.getCollection("blog_contents").insert({
  "_id": ObjectId("5c370903f315ad4925139806"),
  "blog_id": ObjectId("5c370903f315ad4925139801"),
  "lang": "Gr",
  "short_description": "",
  "content": "",
  "created_at": ISODate("2019-01-10T08:57:39.921Z"),
  "updated_at": ISODate("2019-07-29T10:43:08.34Z")
});
db.getCollection("blog_contents").insert({
  "_id": ObjectId("5c370903f315ad4925139808"),
  "blog_id": ObjectId("5c370903f315ad4925139801"),
  "lang": "Sp",
  "short_description": "",
  "content": "",
  "created_at": ISODate("2019-01-10T08:57:39.923Z"),
  "updated_at": ISODate("2019-07-29T10:43:08.34Z")
});
db.getCollection("blog_contents").insert({
  "_id": ObjectId("5c37093bf315ad492513980e"),
  "blog_id": ObjectId("5c37093bf315ad492513980d"),
  "lang": "en",
  "short_description": "NSAIDs Nonsteroidal anti-inflammatory drugs, commonly referred to as NSAIDs, are one of the most common medications used to treat pain and inflammation. Ibuprofen, naproxen, indomethacin, and other NSAIDs are effective across a variety of common conditions, from acute musculoskeletal pain to chronic arthritis. They work by blocking specific proteins, called COX enzymes. This results in the reduction of prostaglandins, which play a key role in pain and inflammation. There are two types of NSAIDs: nonselective NSAIDs and COX-2 selective NSAIDs (these are sometimes referred to as “coxibs”).",
  "content": "<p>Nonsteroidal anti-inflammatory drugs, commonly referred to as NSAIDs, are one of the most common medications used to treat pain and inflammation. Ibuprofen, naproxen, indomethacin, and other NSAIDs are effective across a variety of common conditions, from acute musculoskeletal pain to chronic arthritis. They work by blocking specific proteins, called COX enzymes. This results in the reduction of prostaglandins, which play a key role in pain and inflammation. There are two types of NSAIDs: nonselective NSAIDs and COX-2 selective NSAIDs (these are sometimes referred to as &ldquo;coxibs&rdquo;).</p>\r\n\r\n<p>There is a growing body of evidence that NSAIDs may increase the risk of harmful cardiovascular events including heart attack, stroke, heart failure, and atrial fibrillation. Given the widespread use of NSAIDs, these findings have generated significant concern among patients and healthcare providers. I am frequently asked by patients: is it safe to continue to take NSAIDs?</p>\r\n\r\n<h3>NSAIDs and cardiovascular disease: Minimizing the risks</h3>\r\n\r\n<p>There are several factors to consider when evaluating the potential risk of NSAID therapy. The first is the duration of treatment. The risk of having a heart attack or stroke is extremely small over a short course of therapy (less than one month), such as would be the case in treating acute pain from a musculoskeletal injury like tendonitis. Another important consideration is dose and frequency. The risk tends to increase with higher doses and increased frequency. The third factor is whether the person has existing cardiovascular disease. In people without known cardiovascular disease, the absolute increase in risk is incredibly small (one to two excess cardiovascular events for every 1,000 people who take NSAIDs).</p>\r\n\r\n<p>My general principles for NSAID use are:</p>\r\n\r\n<ol>\r\n\t<li>In all patients, I recommend the lowest effective NSAID dose for the shortest duration of time to limit potential side effects.</li>\r\n\t<li>In people without known cardiovascular disease, the increase in risk is so minimal that it rarely influences my decision about whether to use NSAIDs.</li>\r\n\t<li>In patients with known cardiovascular disease, I might advise an alternative treatment. Many patients with pre-existing heart disease can be safely treated with short courses of NSAIDs. However, the choice of specific NSAID and dose is more important in these patients. I generally recommend the nonselective NSAID naproxen or the COX-2 selective NSAID celecoxib, as studies have demonstrated that these two drugs may have the best safety profile in higher-risk patients.</li>\r\n</ol>\r\n\r\n<p>In summary, although all NSAIDs are associated with an increased cardiovascular risk, the magnitude of the increased risk is minimal for most people without cardiovascular disease taking them for short periods of time. For patients who have heart disease or who require long-term treatment with high doses of NSAIDs, the increased risk is more of a concern. If you fall into this category, discuss your options with your healthcare provider to determine whether an&nbsp;<a href=\"https://www.health.harvard.edu/pain/acetaminophen-safety-be-cautious-but-not-afraid\">alternative therapy</a>&nbsp;is possible, or to help select the safest NSAID option for you.</p>\r\n",
  "created_at": ISODate("2019-01-10T08:58:35.194Z"),
  "updated_at": ISODate("2019-01-10T08:58:35.194Z")
});
db.getCollection("blog_contents").insert({
  "_id": ObjectId("5c37093bf315ad4925139810"),
  "blog_id": ObjectId("5c37093bf315ad492513980d"),
  "lang": "fr",
  "short_description": "",
  "content": "",
  "created_at": ISODate("2019-01-10T08:58:35.196Z"),
  "updated_at": ISODate("2019-01-10T08:58:35.196Z")
});
db.getCollection("blog_contents").insert({
  "_id": ObjectId("5c37093bf315ad4925139812"),
  "blog_id": ObjectId("5c37093bf315ad492513980d"),
  "lang": "Gr",
  "short_description": "",
  "content": "",
  "created_at": ISODate("2019-01-10T08:58:35.198Z"),
  "updated_at": ISODate("2019-01-10T08:58:35.198Z")
});
db.getCollection("blog_contents").insert({
  "_id": ObjectId("5c37093bf315ad4925139814"),
  "blog_id": ObjectId("5c37093bf315ad492513980d"),
  "lang": "Sp",
  "short_description": "",
  "content": "",
  "created_at": ISODate("2019-01-10T08:58:35.200Z"),
  "updated_at": ISODate("2019-01-10T08:58:35.200Z")
});

/** blogs records **/
db.getCollection("blogs").insert({
  "_id": ObjectId("5c3708d9f315ad49251397f5"),
  "lang_content": [
    ObjectId("5c3708d9f315ad49251397f6"),
    ObjectId("5c3708daf315ad49251397f8"),
    ObjectId("5c3708daf315ad49251397fa"),
    ObjectId("5c3708daf315ad49251397fc")
  ],
  "title": "Surgery for appendicitis? Antibiotics alone may be enough",
  "image": "/accessories_img2.jpg",
  "short_description": "I remember when my best friend in fifth grade couldn’t make our much-anticipated end-of-the-school-year camping trip because he had just undergone surgery for appendicitis. Now I prevent kids from participating in their school activities for four to six weeks after I remove their appendix. But what is the appendix, why do we have an organ that causes so many problems, and do you need surgery for appendicitis?",
  "content": "<p>I remember when my best friend in fifth grade couldn&rsquo;t make our much-anticipated end-of-the-school-year camping trip because he had just undergone surgery for&nbsp;<a href=\"https://www.health.harvard.edu/a_to_z/appendicitis-a-to-z\">appendicitis</a>. Now I prevent kids from participating in their school activities for four to six weeks after I remove their appendix. But what is the appendix, why do we have an organ that causes so many problems, and do you need surgery for appendicitis?</p>\r\n\r\n<h3>Role of the appendix is unclear</h3>\r\n\r\n<p>The appendix is a fingerlike tube, about three to four inches long, that comes off of the first portion of the colon. It is normally located in the lower right abdomen, just after the small intestine (needed for digestion and absorption) turns into the colon (whose purpose is to reclaim water and remove waste products).</p>\r\n\r\n<p>The true function of the appendix remains unknown today, but one debated theory is that the appendix acts as a storehouse for good bacteria, to reboot the digestive system after a diarrheal illness. Other experts believe the appendix is just a useless remnant from our evolutionary past. Surgical removal of the appendix appears to cause no observable health problems.</p>\r\n\r\n<h3>Today, appendicitis is usually treated with surgery</h3>\r\n\r\n<p>In the medical community, the suffix &ldquo;-itis&rdquo; refers to inflammation (think arthritis, which is inflammation of a joint). Many times, &ldquo;-itis&rdquo; is due to an infection &mdash; pharyngitis, or strep throat, for example. After much research and debate, the cause of &ldquo;-itis&rdquo; of the appendix is still unclear. However, it appears that most causes of appendicitis are infectious agents, such as bacteria, viruses, parasites, or fungi.</p>\r\n\r\n<p>Whatever the cause, whenever there is an obstruction of the entrance to the appendix &mdash; either from swelling or inflammation, or from mechanical blockage, like a hard piece of stool or a tumor &mdash; appendicitis may ensue. The real danger from appendicitis comes from the potential of the appendix to perforate, or burst, which can spread infection throughout the abdomen.</p>\r\n\r\n<p>Even before 1886, when Dr. Reginald Fitz, a Harvard pathologist, first described appendicitis as a surgical disease, physicians had dealt with the pain and complications stemming from this tiny, menacing organ. Today, the standard of care for the treatment of appendicitis remains surgical removal of the appendix (appendectomy), along with intravenous fluids and antibiotics. In fact, appendectomy is one of the most common abdominal operations in the world. It is also the most common emergency general surgical operation performed in the United States. Most appendectomies are performed by the laparoscopic technique, also known as &ldquo;keyhole&rdquo; or minimally invasive surgery. Patients usually remain at the hospital for less than 24 hours post-operatively.</p>\r\n\r\n<h3>Emerging evidence suggests antibiotics alone may be enough to treat appendicitis</h3>\r\n\r\n<p>Many studies have demonstrated that surgery may not be necessary for all cases of appendicitis. A paper published in June 2015 received international visibility and challenged the status quo when antibiotic therapy was compared with surgery for the treatment of appendicitis. The conclusion of the&nbsp;<a href=\"https://jamanetwork.com/journals/jama/fullarticle/2320315\" target=\"_blank\">APPAC trial</a>&nbsp;(APPendicitis ACuta), which ran in Finland from November 2009 to June 2012, was that most patients who were treated with antibiotics for uncomplicated acute appendicitis did not require surgery during the one-year follow-up period. (Uncomplicated appendicitis refers to those cases in which there is no evidence of perforation or abscess formation, and in which the inflammation is mostly confined to the appendix.) Those who eventually did require appendectomy after failure of the antibiotic regimen did not experience significant complications.</p>\r\n\r\n<p>In 2018, the APPAC authors published a&nbsp;<a href=\"https://jamanetwork.com/journals/jama/article-abstract/2703354\" target=\"_blank\">follow-up</a>&nbsp;in which they concluded that six out every 10 patients who were initially treated with antibiotics for uncomplicated acute appendicitis remained disease-free at five years. They again concluded that antibiotic treatment alone appears feasible as an alternative to surgery for uncomplicated acute appendicitis. Many additional studies also support a nonoperative approach to appendicitis. (And having spent almost 15 years in the navy, I know that for sailors suffering from appendicitis at sea, the use of powerful antibiotics has been the standard of care for decades when access to a surgeon is not readily available.)</p>\r\n\r\n<p>As is always the case in scientific research, these studies have many limitations, including basic study design, multiple confounding variables, misinterpretation of results, and intrinsic flaws known to anyone using statistics. You can also find many articles and rebuttals describing the problems with using medication for a &ldquo;surgical disease.&rdquo; So as of now, while we eagerly await more data on the integrity of antibiotics for the safe use and definitive treatment of uncomplicated appendicitis, surgery remains the gold standard.</p>\r\n",
  "status": NumberInt(1),
  "created_at": ISODate("2019-01-10T08:56:57.740Z"),
  "updated_at": ISODate("2019-01-10T08:59:07.715Z")
});
db.getCollection("blogs").insert({
  "_id": ObjectId("5c370903f315ad4925139801"),
  "lang_content": [
    ObjectId("5c370903f315ad4925139802"),
    ObjectId("5c370903f315ad4925139804"),
    ObjectId("5c370903f315ad4925139806"),
    ObjectId("5c370903f315ad4925139808")
  ],
  "title": "Heart disease and breast cancer: Can women cut risk for both?",
  "image": "/accessories_img3.jpg",
  "short_description": "Very often I encounter women who are far more worried about breast cancer than they are about heart disease. But women have a greater risk of dying from heart disease than from all cancers combined. This is true for women of all races and ethnicities. Yet only about 50% of women realize that they are at greater risk from heart disease than from anything else.",
  "content": "<p>Very often I encounter women who are far more worried about breast cancer than they are about heart disease. But women have a&nbsp;<a href=\"https://www.cdc.gov/women/lcod/2015/all-females/index.htm\" target=\"_blank\">greater risk of dying from heart disease&nbsp;</a>than from all cancers combined. This is true for women of all races and ethnicities. Yet only about&nbsp;<a href=\"https://www.cdc.gov/dhdsp/data_statistics/fact_sheets/fs_women_heart.htm\" target=\"_blank\">50% of women</a>&nbsp;realize that they are at greater risk from heart disease than from anything else.</p>\r\n\r\n<p>Currently in the US,&nbsp;<a href=\"https://seer.cancer.gov/statfacts/html/breast.html\" target=\"_blank\">three million women</a>&nbsp;are living with breast cancer, which causes one in 31 deaths. Almost&nbsp;<a href=\"https://www.ahajournals.org/doi/pdf/10.1161/CIR.0000000000000485\" target=\"_blank\">50 million women</a>&nbsp;have cardiovascular disease, which encompasses heart disease and strokes and causes one in three deaths.</p>\r\n\r\n<p>Here&rsquo;s what&rsquo;s really interesting, though: heart disease and breast cancer share many of the same risk factors. What&rsquo;s more, there are two big risk categories that we can do something about: exercise and diet.</p>\r\n\r\n<h3>Heart disease and breast cancer: How much exercise is needed?</h3>\r\n\r\n<p><a href=\"https://www.ahajournals.org/doi/10.1161/CIR.0000000000000556\" target=\"_blank\">Many studies</a>&nbsp;have shown that the less physically active a woman is, the higher her risks are for cardiovascular disease and breast cancer. Of course, the flip side is that the more physically active she is, the lower her risks.</p>\r\n\r\n<p>How much physical activity is recommended? Well, the latest government&nbsp;<a href=\"https://health.gov/paguidelines/second-edition/10things/\" target=\"_blank\">physical activity guidelines</a>&nbsp;for Americans and the&nbsp;<a href=\"https://www.heart.org/en/healthy-living/fitness/fitness-basics/aha-recs-for-physical-activity-in-adults\" target=\"_blank\">American Heart Association guidelines on activity</a>&nbsp;both call for at least 150 minutes of moderate physical activity weekly. That&rsquo;s only 21 minutes daily. More is better. But by current statistics, less than 18% of women are meeting that minimum of 21 minutes a day. Everything counts! Walking, gardening, taking the stairs, dancing around, cleaning house. Exercise does not have to be at the gym. Avoiding long periods of time sitting is key. So, sit less, move more.</p>\r\n\r\n<h3>Heart disease and breast cancer: How can diet help?</h3>\r\n\r\n<p>Research also shows that a diet high in fruits and vegetables, whole grains, and healthy protein (like seafood, tofu, or beans) and low in refined grains, added sugars, and red and processed meats is associated with a lower risk of both heart disease and breast cancer. The American Cancer Society&nbsp;<a href=\"https://www.cancer.org/healthy/eat-healthy-get-active/acs-guidelines-nutrition-physical-activity-cancer-prevention/summary.html\" target=\"_blank\">nutrition guidelines for cancer prevention</a>&nbsp;and the American Heart Association&nbsp;<a href=\"https://www.heart.org/en/healthy-living/healthy-eating/eat-smart/nutrition-basics/aha-diet-and-lifestyle-recommendations\" target=\"_blank\">nutrition guidelines for heart disease prevention</a>&nbsp;are essentially the same:</p>\r\n\r\n<ul>\r\n\t<li>DO Eat mostly plants, meaning fruits and vegetables; aim for plant proteins like beans, lentils, nuts, and seeds; eat whole grains like brown rice, quinoa, and corn instead of refined grains; if you&rsquo;re going to eat meat, eat fish or poultry.</li>\r\n\t<li>DON&rsquo;T eat refined grains (things made with white flour; white rice); avoid added sugars and sugary beverages; try not to eat red or processed meats or other processed foods with chemicals (like fast foods or frozen dinners).</li>\r\n</ul>\r\n\r\n<h3>What else is important to know?</h3>\r\n\r\n<p>It&rsquo;s critical to understand your risk factors for heart disease &mdash; and what you can do to lower those risks. Sixty-four percent of women who die of heart disease never have any symptoms beforehand. Beyond an unhealthy diet and physical inactivity, other major risk factors include smoking, obesity, diabetes, high cholesterol, high blood pressure, growing older (particularly post-menopause), and a family history of heart disease. It may be important to check your &ldquo;numbers&rdquo; (blood sugars, cholesterol, blood pressure) in order to know if any of these are a problem. For women who have risk factors, we can screen for any heart disease with a coronary artery CT scan.</p>\r\n\r\n<p>It&rsquo;s also important to know that women can have different symptoms of heart disease than men. In my own practice, most of my female patients who have had heart attacks thought they had acid reflux. They experienced a burning feeling in their chest, accompanied by nausea and even burping. One was even seen in urgent care and told that she had acid reflux. The clue in all cases was that the sensation was brought on by activity, not eating.</p>\r\n\r\n<p>Mammograms are very important for breast cancer screening. What age to start them and how often to have them is&nbsp;<a href=\"https://www.health.harvard.edu/blog/screening-mammograms-one-recommendation-may-not-fit-all-2018041613603\">somewhat controversial</a>. It should be individualized to the patient.</p>\r\n\r\n<p>I encourage everyone to meet with their doctor and discuss their risks for heart disease and breast cancer, as further testing may be required.</p>\r\n\r\n<h3>What&rsquo;s the bottom line?</h3>\r\n\r\n<p>Physical activity and a healthy, plant-based diet are key for heart disease and breast cancer prevention. Also, cardiovascular disease and cancer treatment outcomes are better in patients who adopt healthy lifestyle habits, especially&nbsp;<a href=\"https://www.health.harvard.edu/blog/exercise-as-part-of-cancer-treatment-2018061314035\">regular exercise</a>. Basically, a plant-based Mediterranean diet and plenty of physical activity are sensible measures that are important for prevention and even treatment of cardiovascular disease and breast cancer &mdash; both major health issues for women.</p>\r\n",
  "status": NumberInt(1),
  "created_at": ISODate("2019-01-10T08:57:39.917Z"),
  "updated_at": ISODate("2019-07-29T10:43:08.30Z")
});
db.getCollection("blogs").insert({
  "_id": ObjectId("5c37093bf315ad492513980d"),
  "lang_content": [
    ObjectId("5c37093bf315ad492513980e"),
    ObjectId("5c37093bf315ad4925139810"),
    ObjectId("5c37093bf315ad4925139812"),
    ObjectId("5c37093bf315ad4925139814")
  ],
  "title": "NSAIDs: How dangerous are they for your heart?",
  "image": "/gallery4.jpg",
  "short_description": "NSAIDs Nonsteroidal anti-inflammatory drugs, commonly referred to as NSAIDs, are one of the most common medications used to treat pain and inflammation. Ibuprofen, naproxen, indomethacin, and other NSAIDs are effective across a variety of common conditions, from acute musculoskeletal pain to chronic arthritis. They work by blocking specific proteins, called COX enzymes. This results in the reduction of prostaglandins, which play a key role in pain and inflammation. There are two types of NSAIDs: nonselective NSAIDs and COX-2 selective NSAIDs (these are sometimes referred to as “coxibs”).",
  "content": "<p>Nonsteroidal anti-inflammatory drugs, commonly referred to as NSAIDs, are one of the most common medications used to treat pain and inflammation. Ibuprofen, naproxen, indomethacin, and other NSAIDs are effective across a variety of common conditions, from acute musculoskeletal pain to chronic arthritis. They work by blocking specific proteins, called COX enzymes. This results in the reduction of prostaglandins, which play a key role in pain and inflammation. There are two types of NSAIDs: nonselective NSAIDs and COX-2 selective NSAIDs (these are sometimes referred to as &ldquo;coxibs&rdquo;).</p>\r\n\r\n<p>There is a growing body of evidence that NSAIDs may increase the risk of harmful cardiovascular events including heart attack, stroke, heart failure, and atrial fibrillation. Given the widespread use of NSAIDs, these findings have generated significant concern among patients and healthcare providers. I am frequently asked by patients: is it safe to continue to take NSAIDs?</p>\r\n\r\n<h3>NSAIDs and cardiovascular disease: Minimizing the risks</h3>\r\n\r\n<p>There are several factors to consider when evaluating the potential risk of NSAID therapy. The first is the duration of treatment. The risk of having a heart attack or stroke is extremely small over a short course of therapy (less than one month), such as would be the case in treating acute pain from a musculoskeletal injury like tendonitis. Another important consideration is dose and frequency. The risk tends to increase with higher doses and increased frequency. The third factor is whether the person has existing cardiovascular disease. In people without known cardiovascular disease, the absolute increase in risk is incredibly small (one to two excess cardiovascular events for every 1,000 people who take NSAIDs).</p>\r\n\r\n<p>My general principles for NSAID use are:</p>\r\n\r\n<ol>\r\n\t<li>In all patients, I recommend the lowest effective NSAID dose for the shortest duration of time to limit potential side effects.</li>\r\n\t<li>In people without known cardiovascular disease, the increase in risk is so minimal that it rarely influences my decision about whether to use NSAIDs.</li>\r\n\t<li>In patients with known cardiovascular disease, I might advise an alternative treatment. Many patients with pre-existing heart disease can be safely treated with short courses of NSAIDs. However, the choice of specific NSAID and dose is more important in these patients. I generally recommend the nonselective NSAID naproxen or the COX-2 selective NSAID celecoxib, as studies have demonstrated that these two drugs may have the best safety profile in higher-risk patients.</li>\r\n</ol>\r\n\r\n<p>In summary, although all NSAIDs are associated with an increased cardiovascular risk, the magnitude of the increased risk is minimal for most people without cardiovascular disease taking them for short periods of time. For patients who have heart disease or who require long-term treatment with high doses of NSAIDs, the increased risk is more of a concern. If you fall into this category, discuss your options with your healthcare provider to determine whether an&nbsp;<a href=\"https://www.health.harvard.edu/pain/acetaminophen-safety-be-cautious-but-not-afraid\">alternative therapy</a>&nbsp;is possible, or to help select the safest NSAID option for you.</p>\r\n",
  "status": NumberInt(1),
  "created_at": ISODate("2019-01-10T08:58:35.193Z"),
  "updated_at": ISODate("2019-01-10T08:58:35.201Z")
});

/** bulletins records **/
db.getCollection("bulletins").insert({
  "_id": ObjectId("5dc003c37f18d24e968657a7"),
  "title": "Bulletin Issue 08",
  "slug": "bulletin-issue-08",
  "publish_date": ISODate("2019-10-29T18:30:00.0Z"),
  "link": "/LG200P_300P_4606.png",
  "status": NumberInt(1),
  "created_at": ISODate("2019-11-04T10:56:03.133Z"),
  "updated_at": ISODate("2019-11-05T10:51:50.455Z"),
  "htmlfile": "one - Copy.html",
  "image": "/LG200P_300P_4606.png"
});
db.getCollection("bulletins").insert({
  "_id": ObjectId("5dc003d97f18d24e968657b0"),
  "title": "Bulletin Issue 09",
  "slug": "bulletin-issue-09",
  "publish_date": ISODate("2019-12-03T18:30:00.0Z"),
  "link": "/LG200P_300P_4606.png",
  "status": NumberInt(1),
  "created_at": ISODate("2019-11-04T10:56:25.232Z"),
  "updated_at": ISODate("2019-11-07T15:27:42.865Z"),
  "htmlfile": "newsletters_19_bulletin05-june.html",
  "image": "/monitor-1307227_1920.jpg",
  "description": "<p>Bulletin Issue 09 description will come here. please go to admin and edit this.</p>\r\n"
});
db.getCollection("bulletins").insert({
  "_id": ObjectId("5dc162efe0e8812e9f7ce453"),
  "title": "How much Quantity You want",
  "slug": "how-much-quantity-you-want",
  "publish_date": ISODate("2019-09-25T18:30:00.0Z"),
  "image": "/Home_featuired_img.png",
  "htmlfile": "one - Copy.html",
  "status": NumberInt(1),
  "created_at": ISODate("2019-11-05T11:54:23.606Z"),
  "updated_at": ISODate("2019-11-05T11:54:23.606Z")
});
db.getCollection("bulletins").insert({
  "_id": ObjectId("5dc1a1f0e0e8812e9f7ce49a"),
  "title": "Bulletin Issue 06",
  "slug": "bulletin-issue-06",
  "publish_date": ISODate("2019-06-04T18:30:00.0Z"),
  "image": "",
  "htmlfile": "newsletters_19_bulletin05-june.html",
  "status": NumberInt(1),
  "created_at": ISODate("2019-11-05T16:23:12.535Z"),
  "updated_at": ISODate("2019-11-05T16:23:12.535Z")
});
db.getCollection("bulletins").insert({
  "_id": ObjectId("5dc25f78e0e8812e9f7ce54b"),
  "title": "2018 Issue 01",
  "slug": "2018-issue-01",
  "publish_date": ISODate("2018-01-02T18:30:00.0Z"),
  "image": "",
  "htmlfile": "newsletters_19_bulletin05-june.html",
  "status": NumberInt(1),
  "created_at": ISODate("2019-11-06T05:51:52.403Z"),
  "updated_at": ISODate("2019-11-06T06:09:51.175Z")
});
db.getCollection("bulletins").insert({
  "_id": ObjectId("5dc2637be0e8812e9f7ce57b"),
  "title": "2019 Issue 01",
  "slug": "2019-issue-01",
  "publish_date": ISODate("2019-01-02T18:30:00.0Z"),
  "image": "",
  "htmlfile": "newsletters_19_bulletin06-july.html",
  "status": NumberInt(1),
  "created_at": ISODate("2019-11-06T06:08:59.781Z"),
  "updated_at": ISODate("2019-11-06T06:08:59.781Z")
});
db.getCollection("bulletins").insert({
  "_id": ObjectId("5dc26401e0e8812e9f7ce597"),
  "title": "2019 Issue 02",
  "slug": "2019-issue-02",
  "publish_date": ISODate("2019-02-05T18:30:00.0Z"),
  "image": "",
  "htmlfile": "newsletters_19_bulletin05-june.html",
  "status": NumberInt(1),
  "created_at": ISODate("2019-11-06T06:11:13.318Z"),
  "updated_at": ISODate("2019-11-06T06:11:13.318Z")
});

/** case_study records **/
db.getCollection("case_study").insert({
  "_id": ObjectId("5bf3fc5ab77904549084f601"),
  "lang_content": [
    ObjectId("5bf3fc5ab77904549084f602"),
    ObjectId("5bf3fc5ab77904549084f604")
  ],
  "title": "Case Study one",
  "slug": "case-study-one",
  "synopsis": "<p>Case Study oneCase Study oneCase Study oneCase Study one</p>\r\n",
  "description": "<p>Case Study oneCase Study oneCase Study oneCase Study oneCase Study oneCase Study oneCase Study oneCase Study one</p>\r\n",
  "meta_title": "Case Study one",
  "meta_description": "Case Study one",
  "status": NumberInt(1),
  "created_at": ISODate("2018-11-20T12:21:46.210Z"),
  "updated_at": ISODate("2018-11-20T12:21:46.416Z")
});

/** case_study_contents records **/
db.getCollection("case_study_contents").insert({
  "_id": ObjectId("5bf3fc5ab77904549084f602"),
  "case_study_id": ObjectId("5bf3fc5ab77904549084f601"),
  "lang": "en",
  "description": "<p>Case Study oneCase Study oneCase Study oneCase Study oneCase Study oneCase Study oneCase Study oneCase Study one</p>\r\n",
  "created_at": ISODate("2018-11-20T12:21:46.215Z"),
  "updated_at": ISODate("2018-11-20T12:21:46.215Z")
});
db.getCollection("case_study_contents").insert({
  "_id": ObjectId("5bf3fc5ab77904549084f604"),
  "case_study_id": ObjectId("5bf3fc5ab77904549084f601"),
  "lang": "fr",
  "description": "<p>Case Study oneCase Study oneCase Study one</p>\r\n",
  "created_at": ISODate("2018-11-20T12:21:46.413Z"),
  "updated_at": ISODate("2018-11-20T12:21:46.413Z")
});

/** categories records **/
db.getCollection("categories").insert({
  "_id": ObjectId("5c5d422055a6cc290aae530b"),
  "lang_content": [
    ObjectId("5c5d422055a6cc290aae530c"),
    ObjectId("5c5d422055a6cc290aae530e"),
    ObjectId("5c5d422055a6cc290aae5310"),
    ObjectId("5c5d422055a6cc290aae5312")
  ],
  "type": "product",
  "name": "LiteGait-Adult",
  "slug": "litegait-adult",
  "parent_id": "0",
  "image": "/Product/LG400/1category_photos_scene_lg.jpg",
  "feature": "<p>Adult Partial Weight Bearing devices</p>\r\n\r\n<p>Encourages postural stability and biomechanically-correct posture</p>\r\n",
  "short_description": "<p>Meet the needs of all your adult patients with our full range of LiteGait products. From our smallest adult LiteGait, the LiteGait 200P that supports patients up to 200lbs, to our largest, the LG500 that can lift a 500 lb patient into a full upright standing position, we have a LiteGait for your facility. With up to 40&quot; of powered lift available, our LiteGait product line will meet your gait therapy needs for all your patients. Entry level adult LiteGaits start at less than $8,000.</p>\r\n",
  "status": NumberInt(1),
  "created_at": ISODate("2019-02-08T08:47:28.382Z"),
  "updated_at": ISODate("2019-12-05T23:53:43.322Z"),
  "banner_image": "/Banners/2000x300_Size/web_banner_2019_litegait2.jpg",
  "position": NumberInt(1)
});
db.getCollection("categories").insert({
  "_id": ObjectId("5c6689bfbc19ad187c839c60"),
  "lang_content": [
    ObjectId("5c6689bfbc19ad187c839c61"),
    ObjectId("5c6689bfbc19ad187c839c63"),
    ObjectId("5c6689bfbc19ad187c839c65"),
    ObjectId("5c6689bfbc19ad187c839c67")
  ],
  "type": "parts",
  "name": "Batteries",
  "slug": "batteries",
  "parent_id": "0",
  "image": "/about_contact_bg.jpg",
  "feature": "<p>LiteGait I Battery</p>\r\n",
  "short_description": "LiteGait I Battery",
  "status": NumberInt(1),
  "created_at": ISODate("2019-02-15T09:43:27.107Z"),
  "updated_at": ISODate("2019-07-18T17:10:37.453Z"),
  "banner_image": "",
  "position": NumberInt(1)
});
db.getCollection("categories").insert({
  "_id": ObjectId("5c6689d2bc19ad187c839c6c"),
  "lang_content": [
    ObjectId("5c6689d2bc19ad187c839c6d"),
    ObjectId("5c6689d2bc19ad187c839c6f"),
    ObjectId("5c6689d2bc19ad187c839c71"),
    ObjectId("5c6689d2bc19ad187c839c73")
  ],
  "type": "parts",
  "name": "Control Box",
  "slug": "control-box",
  "parent_id": "0",
  "image": "/accessories_banner.jpg",
  "feature": "",
  "short_description": "Control Box",
  "status": NumberInt(1),
  "created_at": ISODate("2019-02-15T09:43:46.860Z"),
  "updated_at": ISODate("2019-07-24T05:14:22.676Z"),
  "banner_image": "",
  "position": NumberInt(3)
});
db.getCollection("categories").insert({
  "_id": ObjectId("5c6c5dcd092aa62529ff4ce2"),
  "lang_content": [
    ObjectId("5c6c5dcd092aa62529ff4ce3"),
    ObjectId("5c6c5dcd092aa62529ff4ce5"),
    ObjectId("5c6c5dcd092aa62529ff4ce7"),
    ObjectId("5c6c5dcd092aa62529ff4ce9")
  ],
  "type": "webinar",
  "name": "Clinical Classes",
  "slug": "clinical-classes",
  "parent_id": "0",
  "image": "/webinars/clinical_class_01.jpg",
  "feature": "<p>these classes are done by speakers</p>\r\n",
  "short_description": "Clinicians present clinical cases of varied patient scenarios using LiteGait. Specific cases will relate to patient diagnoses or to a specific targeted outcome. Each segment features an individual patient, followed by Q & A.",
  "status": NumberInt(1),
  "created_at": ISODate("2019-02-19T19:49:33.591Z"),
  "updated_at": ISODate("2019-09-09T03:51:54.119Z"),
  "banner_image": "",
  "position": null
});
db.getCollection("categories").insert({
  "_id": ObjectId("5c6c5e15092aa62529ff4cee"),
  "lang_content": [
    ObjectId("5c6c5e15092aa62529ff4cef"),
    ObjectId("5c6c5e15092aa62529ff4cf1"),
    ObjectId("5c6c5e15092aa62529ff4cf3"),
    ObjectId("5c6c5e15092aa62529ff4cf5")
  ],
  "type": "webinar",
  "name": "Clinical Tips and Tricks",
  "slug": "clinical-tips-and-tricks",
  "parent_id": "0",
  "image": "/webinars/clinical_tips&tricks.jpg",
  "feature": "<p>Kayli, does her thing</p>\r\n",
  "short_description": "Webinar series dedicated to the ongoing education of LiteGait users. It is designed to provide instruction to new users, updates to veteran users, re-energize the occasional user and empower the every day user.",
  "status": NumberInt(1),
  "created_at": ISODate("2019-02-19T19:50:45.928Z"),
  "updated_at": ISODate("2019-08-16T23:11:17.688Z"),
  "banner_image": "",
  "position": null
});
db.getCollection("categories").insert({
  "_id": ObjectId("5c748820092aa62529ff4d70"),
  "lang_content": [
    ObjectId("5c748821092aa62529ff4d71"),
    ObjectId("5c748821092aa62529ff4d73"),
    ObjectId("5c748821092aa62529ff4d75"),
    ObjectId("5c748821092aa62529ff4d77")
  ],
  "type": "product",
  "name": "LiteGait Pediatric",
  "slug": "litegait-pediatric",
  "parent_id": "0",
  "image": "/Product/LG100mx/2category_photos_scene_lgpediatric.jpg",
  "feature": "<p>Mobility Research provides a full line of pediatric devices.</p>\r\n\r\n<p>Our pediatric designs create an ideal setting to assess.</p>\r\n",
  "short_description": "<p>Mobility Research provides a full line of pediatric devices. Our pediatric designs create an ideal setting to assess, evaluate and treat individuals of varying sizes, impairments and functional levels. From our smallest pediatric device, the LG75&nbsp;up to the LG200P that supports infants through young adults.&nbsp;</p>\r\n",
  "status": NumberInt(1),
  "created_at": ISODate("2019-02-26T00:28:16.987Z"),
  "updated_at": ISODate("2019-12-05T23:54:53.365Z"),
  "banner_image": "/Banners/2000x300_Size/web_banner_2019_litegaitpeds.jpg",
  "position": NumberInt(2)
});
db.getCollection("categories").insert({
  "_id": ObjectId("5c78f2e88592290d03e1ff90"),
  "lang_content": [
    ObjectId("5c78f2e88592290d03e1ff91"),
    ObjectId("5c78f2e88592290d03e1ff93"),
    ObjectId("5c78f2e88592290d03e1ff95"),
    ObjectId("5c78f2e88592290d03e1ff97")
  ],
  "type": "faqs",
  "name": "Billing and Membership",
  "slug": "billing-and-membership",
  "parent_id": "0",
  "image": "",
  "feature": "<p>Billing and Membership</p>\r\n",
  "short_description": "Billing and Membership",
  "status": NumberInt(1),
  "created_at": ISODate("2019-03-01T08:52:56.274Z"),
  "updated_at": ISODate("2019-09-13T00:06:30.137Z"),
  "banner_image": "",
  "position": null
});
db.getCollection("categories").insert({
  "_id": ObjectId("5c78f2f48592290d03e1ff9c"),
  "lang_content": [
    ObjectId("5c78f2f48592290d03e1ff9d"),
    ObjectId("5c78f2f48592290d03e1ff9f"),
    ObjectId("5c78f2f48592290d03e1ffa1"),
    ObjectId("5c78f2f48592290d03e1ffa3")
  ],
  "type": "faqs",
  "name": "Billing and Payment",
  "slug": "billing-and-payment",
  "parent_id": "0",
  "image": "",
  "feature": "<p>Billing and Payment</p>\r\n",
  "short_description": "Billing and Payment",
  "status": NumberInt(1),
  "created_at": ISODate("2019-03-01T08:53:08.12Z"),
  "updated_at": ISODate("2019-03-01T08:53:08.21Z")
});
db.getCollection("categories").insert({
  "_id": ObjectId("5c78f3038592290d03e1ffa8"),
  "lang_content": [
    ObjectId("5c78f3038592290d03e1ffa9"),
    ObjectId("5c78f3038592290d03e1ffab"),
    ObjectId("5c78f3038592290d03e1ffad"),
    ObjectId("5c78f3038592290d03e1ffaf")
  ],
  "type": "faqs",
  "name": "Account Settings",
  "slug": "account-settings",
  "parent_id": "0",
  "image": "",
  "feature": "<p>Account Settings</p>\r\n",
  "short_description": "Account Settings",
  "status": NumberInt(1),
  "created_at": ISODate("2019-03-01T08:53:23.52Z"),
  "updated_at": ISODate("2019-03-01T08:53:23.62Z")
});
db.getCollection("categories").insert({
  "_id": ObjectId("5c78f6718592290d03e20040"),
  "lang_content": [
    ObjectId("5c78f6718592290d03e20041"),
    ObjectId("5c78f6718592290d03e20043"),
    ObjectId("5c78f6718592290d03e20045"),
    ObjectId("5c78f6718592290d03e20047")
  ],
  "type": "product",
  "name": "LiteGait 4 Home",
  "slug": "litegait-4-home",
  "parent_id": "0",
  "image": "/Product/LG4H/4category_photos_scene_lg4h.jpg",
  "feature": "<p>LiteGait 4 Home</p>\r\n",
  "short_description": "<p>LiteGait 4 Home Description comes here. LiteGait 4 Home Description comes here.LiteGait 4 Home Description comes here.LiteGait 4 Home Description comes here.LiteGait 4 Home Description comes here.LiteGait 4 Home Description comes here.LiteGait 4 Home Description comes here.LiteGait 4 Home Description comes here.LiteGait 4 Home Description comes here.LiteGait 4 Home Description comes here.</p>\r\n",
  "status": NumberInt(1),
  "created_at": ISODate("2019-03-01T09:08:01.845Z"),
  "updated_at": ISODate("2019-12-05T23:57:57.455Z"),
  "banner_image": "/Banners/2000x300_Size/web_banner_2019_lg4h.jpg",
  "position": NumberInt(3)
});
db.getCollection("categories").insert({
  "_id": ObjectId("5c78f6ae8592290d03e2004c"),
  "lang_content": [
    ObjectId("5c78f6ae8592290d03e2004d"),
    ObjectId("5c78f6ae8592290d03e2004f"),
    ObjectId("5c78f6ae8592290d03e20051"),
    ObjectId("5c78f6ae8592290d03e20053")
  ],
  "type": "product",
  "name": "Treadmills",
  "slug": "treadmills",
  "parent_id": "0",
  "image": "/Product/GaitKeeper/3category_photos_scene_gk.jpg",
  "feature": "<p>Rehabilitation treadmills designed for both adults and children</p>\r\n\r\n<p>True zero start speed - no need to straddle the belt</p>\r\n",
  "short_description": "<p>Rehabilitation treadmills designed for both adults and children True zero start speed - no need to straddle the beltRehabilitation treadmills designed for both adults and children True zero start speed - no need to straddle the beltRehabilitation treadmills designed for both adults and children True zero start speed - no need to straddle the beltRehabilitation treadmills designed for both adults and children True zero start speed - no need to straddle the belt</p>\r\n",
  "status": NumberInt(1),
  "created_at": ISODate("2019-03-01T09:09:02.65Z"),
  "updated_at": ISODate("2019-12-05T23:59:24.5Z"),
  "banner_image": "/Banners/2000x300_Size/web_banner_2019_gaitkeeper.jpg",
  "position": NumberInt(3)
});
db.getCollection("categories").insert({
  "_id": ObjectId("5c88fdde8592290d03e200fa"),
  "lang_content": [
    ObjectId("5c88fdde8592290d03e200fb"),
    ObjectId("5c88fdde8592290d03e200fd"),
    ObjectId("5c88fdde8592290d03e200ff"),
    ObjectId("5c88fdde8592290d03e20101")
  ],
  "type": "webinar",
  "name": "Case Reports",
  "slug": "case-reports",
  "parent_id": "0",
  "image": "/webinars/case_report_idea.jpg",
  "feature": "<p>Case ReportsCase ReportsCase ReportsCase Reports</p>\r\n",
  "short_description": "CEU clinical webinars are designed for busy professionals. 90 minute presentations are packed with useful information augmented with instructional videos on a multitude of topics",
  "status": NumberInt(1),
  "created_at": ISODate("2019-03-13T12:55:58.44Z"),
  "updated_at": ISODate("2019-08-16T23:14:41.328Z"),
  "banner_image": "",
  "position": null
});
db.getCollection("categories").insert({
  "_id": ObjectId("5c88fdf88592290d03e20105"),
  "lang_content": [
    ObjectId("5c88fdf88592290d03e20106"),
    ObjectId("5c88fdf88592290d03e20108"),
    ObjectId("5c88fdf88592290d03e2010a"),
    ObjectId("5c88fdf88592290d03e2010c")
  ],
  "type": "webinar",
  "name": "Journal Club",
  "slug": "journal-club",
  "parent_id": "0",
  "image": "/webinars/journal_club_idea.jpg",
  "feature": "<p>Journal ClubJournal Club</p>\r\n",
  "short_description": "Our quarterly Journal Club is part of our live, moderated online forum. It is interactive and designed to explain and discuss current published work.",
  "status": NumberInt(1),
  "created_at": ISODate("2019-03-13T12:56:24.544Z"),
  "updated_at": ISODate("2019-08-16T23:16:34.413Z"),
  "banner_image": "",
  "position": null
});
db.getCollection("categories").insert({
  "_id": ObjectId("5cf0096dfb42690c45d441be"),
  "lang_content": [
    ObjectId("5cf0096dfb42690c45d441bf"),
    ObjectId("5cf0096dfb42690c45d441c1"),
    ObjectId("5cf0096dfb42690c45d441c3"),
    ObjectId("5cf0096dfb42690c45d441c5")
  ],
  "type": "product",
  "name": "Therapy Mouse",
  "slug": "therapy-mouse",
  "parent_id": "0",
  "image": "/Product/TherapyMouse/9category_photos_scene_tm.jpg",
  "banner_image": "/Banners/2000x300_Size/web_banner_2019_therapymouse.jpg",
  "feature": "<p>Therapy Mouse is a low cost, motion-sensing, wearable pointing device</p>\r\n\r\n<p>translate user movements to precise, proportional computer control.&nbsp;</p>\r\n",
  "short_description": "<p>Therapy Mouse is a low cost, motion-sensing, wearable pointing device, that allows individuals who have difficulties using their hands to control a computer using any alternative body part. Simply plug into your computer and attach the sensor to the desired body part; the Therapy Mouse will translate user movements to precise, proportional computer control.&nbsp;</p>\r\n",
  "position": NumberInt(9),
  "status": NumberInt(1),
  "created_at": ISODate("2019-05-30T16:48:45.499Z"),
  "updated_at": ISODate("2019-12-06T00:07:49.951Z")
});
db.getCollection("categories").insert({
  "_id": ObjectId("5cfa390860b1b8748527eda0"),
  "lang_content": [
    ObjectId("5cfa390860b1b8748527eda1"),
    ObjectId("5cfa390860b1b8748527eda3"),
    ObjectId("5cfa390860b1b8748527eda5"),
    ObjectId("5cfa390860b1b8748527eda7")
  ],
  "type": "faqs",
  "name": "My Profile",
  "slug": "my-profile",
  "parent_id": "5c78f3038592290d03e1ffa8",
  "image": "",
  "banner_image": "",
  "feature": "",
  "short_description": "My ProfileMy ProfileMy Profile",
  "position": null,
  "status": NumberInt(1),
  "created_at": ISODate("2019-06-07T10:14:32.497Z"),
  "updated_at": ISODate("2019-06-07T10:14:32.514Z")
});
db.getCollection("categories").insert({
  "_id": ObjectId("5cfa391d60b1b8748527edac"),
  "lang_content": [
    ObjectId("5cfa391d60b1b8748527edad"),
    ObjectId("5cfa391d60b1b8748527edaf"),
    ObjectId("5cfa391d60b1b8748527edb1"),
    ObjectId("5cfa391d60b1b8748527edb3")
  ],
  "type": "faqs",
  "name": "My Payment",
  "slug": "my-payment",
  "parent_id": "5c78f3038592290d03e1ffa8",
  "image": "",
  "banner_image": "",
  "feature": "",
  "short_description": "My PaymentMy PaymentMy Payment",
  "position": null,
  "status": NumberInt(1),
  "created_at": ISODate("2019-06-07T10:14:53.184Z"),
  "updated_at": ISODate("2019-06-07T10:14:53.193Z")
});
db.getCollection("categories").insert({
  "_id": ObjectId("5cfa393b60b1b8748527edb9"),
  "lang_content": [
    ObjectId("5cfa393b60b1b8748527edba"),
    ObjectId("5cfa393b60b1b8748527edbc"),
    ObjectId("5cfa393b60b1b8748527edbe"),
    ObjectId("5cfa393b60b1b8748527edc0")
  ],
  "type": "faqs",
  "name": "Payment Plan",
  "slug": "payment-plan",
  "parent_id": "5c78f2e88592290d03e1ff90",
  "image": "",
  "banner_image": "",
  "feature": "",
  "short_description": "Payment PlanPayment Plan",
  "position": null,
  "status": NumberInt(1),
  "created_at": ISODate("2019-06-07T10:15:23.528Z"),
  "updated_at": ISODate("2019-06-07T10:15:23.537Z")
});
db.getCollection("categories").insert({
  "_id": ObjectId("5d09056560b1b8748527eefa"),
  "lang_content": [
    ObjectId("5d09056560b1b8748527eefb"),
    ObjectId("5d09056560b1b8748527eefd"),
    ObjectId("5d09056560b1b8748527eeff"),
    ObjectId("5d09056560b1b8748527ef01")
  ],
  "type": "faqs",
  "name": "Webinars",
  "slug": "webinars",
  "parent_id": "0",
  "image": "",
  "banner_image": "",
  "feature": "",
  "short_description": "webinars",
  "position": null,
  "status": NumberInt(1),
  "created_at": ISODate("2019-06-18T15:38:13.330Z"),
  "updated_at": ISODate("2019-06-18T15:38:13.348Z")
});
db.getCollection("categories").insert({
  "_id": ObjectId("5d0905c260b1b8748527ef06"),
  "lang_content": [
    ObjectId("5d0905c260b1b8748527ef07"),
    ObjectId("5d0905c260b1b8748527ef09"),
    ObjectId("5d0905c260b1b8748527ef0b"),
    ObjectId("5d0905c260b1b8748527ef0d")
  ],
  "type": "faqs",
  "name": "Attending a Webinar",
  "slug": "attending-a-webinar",
  "parent_id": "5d09056560b1b8748527eefa",
  "image": "",
  "banner_image": "",
  "feature": "",
  "short_description": "Attending a Webinar",
  "position": null,
  "status": NumberInt(1),
  "created_at": ISODate("2019-06-18T15:39:46.630Z"),
  "updated_at": ISODate("2019-06-18T15:39:46.665Z")
});
db.getCollection("categories").insert({
  "_id": ObjectId("5d0905df60b1b8748527ef12"),
  "lang_content": [
    ObjectId("5d0905df60b1b8748527ef13"),
    ObjectId("5d0905df60b1b8748527ef15"),
    ObjectId("5d0905df60b1b8748527ef17"),
    ObjectId("5d0905df60b1b8748527ef19")
  ],
  "type": "faqs",
  "name": "Presenting a Webinar",
  "slug": "presenting-a-webinar",
  "parent_id": "5d09056560b1b8748527eefa",
  "image": "",
  "banner_image": "",
  "feature": "",
  "short_description": "Presenting a Webinar",
  "position": null,
  "status": NumberInt(1),
  "created_at": ISODate("2019-06-18T15:40:15.497Z"),
  "updated_at": ISODate("2019-06-18T15:40:15.507Z")
});
db.getCollection("categories").insert({
  "_id": ObjectId("5d09060960b1b8748527ef1e"),
  "lang_content": [
    ObjectId("5d09060960b1b8748527ef1f"),
    ObjectId("5d09060960b1b8748527ef21"),
    ObjectId("5d09060960b1b8748527ef23"),
    ObjectId("5d09060960b1b8748527ef25")
  ],
  "type": "faqs",
  "name": "Webinar Recordings",
  "slug": "webinar-recordings",
  "parent_id": "5d09056560b1b8748527eefa",
  "image": "",
  "banner_image": "",
  "feature": "",
  "short_description": "webinar recordings",
  "position": null,
  "status": NumberInt(1),
  "created_at": ISODate("2019-06-18T15:40:57.103Z"),
  "updated_at": ISODate("2019-06-18T15:40:57.111Z")
});
db.getCollection("categories").insert({
  "_id": ObjectId("5d14f1540a2c2e561c73512b"),
  "lang_content": [
    ObjectId("5d14f1540a2c2e561c73512c"),
    ObjectId("5d14f1540a2c2e561c73512e"),
    ObjectId("5d14f1540a2c2e561c735130"),
    ObjectId("5d14f1540a2c2e561c735132")
  ],
  "type": "product",
  "name": "Gait Analysis",
  "slug": "gait-sens",
  "parent_id": "0",
  "image": "/Product/GaitSens/category_photos_scene_gaitanalysis.jpg",
  "banner_image": "/Banners/2000x300_Size/web_banner_2019_GaitSens.jpg",
  "feature": "<p>Objective gait data</p>\r\n\r\n<p>Available on any Gaitkeeper treadmill</p>\r\n",
  "short_description": "<p>Mobility Research brings the latest advancements in Gait, Balance assessment and therapy. Our assessment tools quickly and easily provide targeted, meaningful gait and balance assessment data to help you assess and monitor objective progress of your patient.</p>\r\n",
  "position": NumberInt(5),
  "status": NumberInt(1),
  "created_at": ISODate("2019-06-27T16:39:48.835Z"),
  "updated_at": ISODate("2019-11-05T18:37:27.690Z")
});
db.getCollection("categories").insert({
  "_id": ObjectId("5d2de7f4837f487c405e62e2"),
  "lang_content": [
    ObjectId("5d2de7f4837f487c405e62e3"),
    ObjectId("5d2de7f4837f487c405e62e5"),
    ObjectId("5d2de7f4837f487c405e62e7"),
    ObjectId("5d2de7f4837f487c405e62e9")
  ],
  "type": "product",
  "name": "Q-pads",
  "slug": "q-pads",
  "parent_id": "0",
  "image": "/Product/Qpads/category_photos_scene_qpads.jpg",
  "banner_image": "/Banners/2000x300_Size/web_banner_2019_qpads-alternate.jpg",
  "feature": "<p><strong>The Innovative Balance Training System</strong></p>\r\n\r\n<p>Q-pads are an interactive rehabilitation system providing the user and clinician visual feedback via a pressure sensitive surface and bright multi-colored LEDs. Appropriate for children and adults, upper and lower limbs, in sitting, standing or quadruped. They are easy to set up, strong enough to step on and even magnetic for vertical use. Different functions are available allowing the therapist to customize activities using multiple pads to tailor to the various needs of each use</p>\r\n",
  "short_description": "<p>Work on motor and cognitive skills in an entertaining wa</p>\r\n",
  "position": NumberInt(8),
  "status": NumberInt(1),
  "created_at": ISODate("2019-07-16T15:06:28.213Z"),
  "updated_at": ISODate("2019-11-06T21:52:50.731Z")
});
db.getCollection("categories").insert({
  "_id": ObjectId("5d2de90b837f487c405e62ee"),
  "lang_content": [
    ObjectId("5d2de90b837f487c405e62ef"),
    ObjectId("5d2de90b837f487c405e62f1"),
    ObjectId("5d2de90b837f487c405e62f3"),
    ObjectId("5d2de90b837f487c405e62f5")
  ],
  "type": "product",
  "name": "HugN-Go",
  "slug": "hugn-go",
  "parent_id": "0",
  "image": "/Product/HugNgo/5category_photos_scene_hng.jpg",
  "banner_image": "/Banners/2000x300_Size/web_banner_2019_hngo.jpg",
  "feature": "<p>The versatile Mobility Device&trade; lift walker product line provides supported ambulation, sit-to-stand assistance and seated-to-seated transfers for patients. The easy-to-use harness simplifies the process of getting patients upright, and assists balance during ambulation for a comfortable and confidence-building experience. Three models are available to fit different patient sizes.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n",
  "short_description": "<p>Our Supported Ambulation Mobility Devices, HugN-Go, provide the versatility of three devices in one - supported ambulation, sit to stand and seated to seated transfers. The HugN-Go hugs the user to provide a safe, upright posture with plenty of leg room so the patient can GO! We hug and the user goes!</p>\r\n",
  "position": NumberInt(5),
  "status": NumberInt(1),
  "created_at": ISODate("2019-07-16T15:11:07.548Z"),
  "updated_at": ISODate("2019-12-06T19:27:12.596Z")
});
db.getCollection("categories").insert({
  "_id": ObjectId("5d37d4302a02f4259583713f"),
  "lang_content": [
    ObjectId("5d37d4302a02f42595837140"),
    ObjectId("5d37d4302a02f42595837142"),
    ObjectId("5d37d4302a02f42595837144"),
    ObjectId("5d37d4302a02f42595837146")
  ],
  "type": "product",
  "name": "LiteGait Vet",
  "slug": "litegait-vet",
  "parent_id": "0",
  "image": "/Product/LGV/7category_photos_scene_lgv.jpg",
  "banner_image": "/Banners/2000x300_Size/web_banner_2019_lg-v.jpg",
  "feature": "<p>Leverage all the benefits of LiteGait for our four legged friends</p>\r\n",
  "short_description": "<p>Assist animal in moving over ground or a slow treadmill Assist handlers in lifting the animal into standing position Leverage all the benefits of LiteGait for our four legged friends</p>\r\n",
  "position": NumberInt(7),
  "status": NumberInt(1),
  "created_at": ISODate("2019-07-24T03:44:48.1Z"),
  "updated_at": ISODate("2019-12-06T00:06:23.743Z")
});
db.getCollection("categories").insert({
  "_id": ObjectId("5d37df0c2a02f42595837165"),
  "lang_content": [
    ObjectId("5d37df0c2a02f42595837166"),
    ObjectId("5d37df0c2a02f42595837168"),
    ObjectId("5d37df0c2a02f4259583716a"),
    ObjectId("5d37df0c2a02f4259583716c")
  ],
  "type": "product",
  "name": "CrawlAhead",
  "slug": "crawlahead",
  "parent_id": "0",
  "image": "/Product/crawlahead/10category_photos_scene_ca_2.jpg",
  "banner_image": "/Banners/2000x300_Size/web_banner_2019_crawlahead.jpg",
  "feature": "<p><strong><span style=\"color:#ab3585\">Turn little function to big advantage!</span></strong><br />\r\nIt supports the body in a quadruped posture for weight bearing and crawling, offering a multitude of benefits while helping to achieve an important milestone in an infant&rsquo;s life. Appropriate for facility or home use.</p>\r\n",
  "short_description": "<p>Gentle Crawling Assistance</p>\r\n",
  "position": NumberInt(10),
  "status": NumberInt(1),
  "created_at": ISODate("2019-07-24T04:31:08.70Z"),
  "updated_at": ISODate("2019-12-06T19:20:07.389Z")
});
db.getCollection("categories").insert({
  "_id": ObjectId("5d37ea652a02f4259583719c"),
  "lang_content": [
    ObjectId("5d37ea652a02f4259583719d"),
    ObjectId("5d37ea652a02f4259583719f"),
    ObjectId("5d37ea652a02f425958371a1"),
    ObjectId("5d37ea652a02f425958371a3")
  ],
  "type": "parts",
  "name": "Chargers | Power Cords",
  "slug": "chargers--power-cords",
  "parent_id": "0",
  "image": "/parts/power cord_ps20x.PNG",
  "banner_image": "",
  "feature": "",
  "short_description": "A/C adapters for LiteGait",
  "position": NumberInt(2),
  "status": NumberInt(1),
  "created_at": ISODate("2019-07-24T05:19:33.360Z"),
  "updated_at": ISODate("2019-07-24T05:19:33.368Z")
});
db.getCollection("categories").insert({
  "_id": ObjectId("5d37ebfa2a02f425958371a8"),
  "lang_content": [
    ObjectId("5d37ebfa2a02f425958371a9"),
    ObjectId("5d37ebfa2a02f425958371ab"),
    ObjectId("5d37ebfa2a02f425958371ad"),
    ObjectId("5d37ebfa2a02f425958371af")
  ],
  "type": "parts",
  "name": "Hand Controls",
  "slug": "hand-controls",
  "parent_id": "0",
  "image": "/parts/handswitch_ps35e-e.PNG",
  "banner_image": "",
  "feature": "",
  "short_description": "Hand Switches for LiteGait and HugN-Go",
  "position": NumberInt(4),
  "status": NumberInt(1),
  "created_at": ISODate("2019-07-24T05:26:18.679Z"),
  "updated_at": ISODate("2019-09-10T15:14:34.681Z")
});
db.getCollection("categories").insert({
  "_id": ObjectId("5d37f4352a02f425958371b6"),
  "lang_content": [
    ObjectId("5d37f4352a02f425958371b7"),
    ObjectId("5d37f4352a02f425958371b9"),
    ObjectId("5d37f4352a02f425958371bb"),
    ObjectId("5d37f4352a02f425958371bd")
  ],
  "type": "parts",
  "name": "Casters",
  "slug": "casters",
  "parent_id": "0",
  "image": "/parts/caster_b10R25-c.PNG",
  "banner_image": "",
  "feature": "",
  "short_description": "Casters for LiteGait",
  "position": NumberInt(5),
  "status": NumberInt(1),
  "created_at": ISODate("2019-07-24T06:01:25.645Z"),
  "updated_at": ISODate("2019-07-24T06:01:25.653Z")
});
db.getCollection("categories").insert({
  "_id": ObjectId("5d485a4ca9421434c38e68b5"),
  "lang_content": [
    ObjectId("5d485a4ca9421434c38e68b6"),
    ObjectId("5d485a4ca9421434c38e68b8"),
    ObjectId("5d485a4ca9421434c38e68ba"),
    ObjectId("5d485a4ca9421434c38e68bc")
  ],
  "type": "faqs",
  "name": "Missed Webinars",
  "slug": "missed-webinars",
  "parent_id": "5d09056560b1b8748527eefa",
  "image": "",
  "banner_image": "",
  "feature": "",
  "short_description": "Missed the webinar I paid for",
  "position": null,
  "status": NumberInt(1),
  "created_at": ISODate("2019-08-05T16:33:16.511Z"),
  "updated_at": ISODate("2019-08-05T16:33:16.522Z")
});
db.getCollection("categories").insert({
  "_id": ObjectId("5d77c2b2f3c6d306896b26bc"),
  "lang_content": [
    ObjectId("5d77c2b2f3c6d306896b26bd"),
    ObjectId("5d77c2b2f3c6d306896b26bf"),
    ObjectId("5d77c2b2f3c6d306896b26c1"),
    ObjectId("5d77c2b2f3c6d306896b26c3")
  ],
  "type": "parts",
  "name": "Handle Bars",
  "slug": "handle-bars",
  "parent_id": "0",
  "image": "/parts/Handlebar/HB50E-G.JPG",
  "banner_image": "",
  "feature": "<p>Variety of interchangable designs. Handle bars can be locked in place up and down the LiteGait depending on patient needs.</p>\r\n",
  "short_description": "sss",
  "position": null,
  "status": NumberInt(1),
  "created_at": ISODate("2019-09-10T15:35:14.586Z"),
  "updated_at": ISODate("2019-09-10T15:35:14.599Z")
});
db.getCollection("categories").insert({
  "_id": ObjectId("5d7acdf9f3c6d306896b2847"),
  "lang_content": [
    ObjectId("5d7acdf9f3c6d306896b2848"),
    ObjectId("5d7acdf9f3c6d306896b284a"),
    ObjectId("5d7acdf9f3c6d306896b284c"),
    ObjectId("5d7acdf9f3c6d306896b284e")
  ],
  "type": "accessories",
  "name": "Device Add-Ons",
  "slug": "device-add-ons",
  "parent_id": "0",
  "image": "",
  "banner_image": "",
  "feature": "",
  "short_description": "Many of  our newer devices incorporate features and improvements that increase functionality and ease of use.",
  "position": NumberInt(1),
  "status": NumberInt(1),
  "created_at": ISODate("2019-09-12T23:00:09.403Z"),
  "updated_at": ISODate("2019-09-12T23:00:09.412Z")
});
db.getCollection("categories").insert({
  "_id": ObjectId("5d7ad51af3c6d306896b28a8"),
  "lang_content": [
    ObjectId("5d7ad51af3c6d306896b28a9"),
    ObjectId("5d7ad51af3c6d306896b28ab"),
    ObjectId("5d7ad51af3c6d306896b28ad"),
    ObjectId("5d7ad51af3c6d306896b28af")
  ],
  "type": "accessories",
  "name": "FreeDome",
  "slug": "freedome",
  "parent_id": "5d7acdf9f3c6d306896b2847",
  "image": "",
  "banner_image": "",
  "feature": "",
  "short_description": "rotate",
  "position": NumberInt(2),
  "status": NumberInt(1),
  "created_at": ISODate("2019-09-12T23:30:34.763Z"),
  "updated_at": ISODate("2019-09-12T23:30:34.771Z")
});
db.getCollection("categories").insert({
  "_id": ObjectId("5d7ad53ff3c6d306896b28b4"),
  "lang_content": [
    ObjectId("5d7ad53ff3c6d306896b28b5"),
    ObjectId("5d7ad53ff3c6d306896b28b7"),
    ObjectId("5d7ad53ff3c6d306896b28b9"),
    ObjectId("5d7ad53ff3c6d306896b28bb")
  ],
  "type": "accessories",
  "name": "BiSym",
  "slug": "bisym",
  "parent_id": "5d7acdf9f3c6d306896b2847",
  "image": "",
  "banner_image": "",
  "feature": "",
  "short_description": "measure",
  "position": NumberInt(1),
  "status": NumberInt(1),
  "created_at": ISODate("2019-09-12T23:31:11.484Z"),
  "updated_at": ISODate("2019-09-12T23:31:11.492Z")
});
db.getCollection("categories").insert({
  "_id": ObjectId("5d7adb25f3c6d306896b28ee"),
  "lang_content": [
    ObjectId("5d7adb25f3c6d306896b28ef"),
    ObjectId("5d7adb25f3c6d306896b28f1"),
    ObjectId("5d7adb25f3c6d306896b28f3"),
    ObjectId("5d7adb25f3c6d306896b28f5")
  ],
  "type": "accessories",
  "name": "Companion Devices",
  "slug": "companion-devices",
  "parent_id": "0",
  "image": "",
  "banner_image": "",
  "feature": "",
  "short_description": "Mobility Research products that enhance both you and your clients rehabilitation therapy.",
  "position": NumberInt(2),
  "status": NumberInt(1),
  "created_at": ISODate("2019-09-12T23:56:21.134Z"),
  "updated_at": ISODate("2019-09-12T23:56:21.142Z")
});
db.getCollection("categories").insert({
  "_id": ObjectId("5d7add62f3c6d306896b28fd"),
  "lang_content": [
    ObjectId("5d7add62f3c6d306896b28fe"),
    ObjectId("5d7add62f3c6d306896b2900"),
    ObjectId("5d7add62f3c6d306896b2902"),
    ObjectId("5d7add62f3c6d306896b2904")
  ],
  "type": "accessories",
  "name": "Harnesses",
  "slug": "harnesses",
  "parent_id": "0",
  "image": "/accessories/harness_array_01-2.jpg",
  "banner_image": "",
  "feature": "",
  "short_description": "Creates biomechanically-appropriate posture for the patient",
  "position": NumberInt(3),
  "status": NumberInt(1),
  "created_at": ISODate("2019-09-13T00:05:54.146Z"),
  "updated_at": ISODate("2019-09-13T00:06:53.564Z")
});
db.getCollection("categories").insert({
  "_id": ObjectId("5d9e36877dbdaa69d72e5f20"),
  "lang_content": [
    ObjectId("5d9e36877dbdaa69d72e5f21"),
    ObjectId("5d9e36877dbdaa69d72e5f23"),
    ObjectId("5d9e36877dbdaa69d72e5f25"),
    ObjectId("5d9e36877dbdaa69d72e5f27")
  ],
  "type": "product",
  "name": "Mini",
  "slug": "mini",
  "parent_id": "5c78f6ae8592290d03e2004c",
  "image": "/Product/GaitKeeper/gkmini_handlebar.jpg",
  "banner_image": "",
  "feature": "<p>GK mini is the best</p>\r\n",
  "short_description": "<p>GKmini</p>\r\n",
  "position": null,
  "status": NumberInt(1),
  "created_at": ISODate("2019-10-09T19:35:35.493Z"),
  "updated_at": ISODate("2019-10-09T19:35:35.562Z")
});

/** category_contents records **/
db.getCollection("category_contents").insert({
  "_id": ObjectId("5c5d422055a6cc290aae530c"),
  "lang": "en",
  "feature": "<p>Adult Partial Weight Bearing devices</p>\r\n\r\n<p>Encourages postural stability and biomechanically-correct posture</p>\r\n",
  "short_description": "<p>Meet the needs of all your adult patients with our full range of LiteGait products. From our smallest adult LiteGait, the LiteGait 200P that supports patients up to 200lbs, to our largest, the LG500 that can lift a 500 lb patient into a full upright standing position, we have a LiteGait for your facility. With up to 40&quot; of powered lift available, our LiteGait product line will meet your gait therapy needs for all your patients. Entry level adult LiteGaits start at less than $8,000.</p>\r\n",
  "created_at": ISODate("2019-02-08T08:47:28.386Z"),
  "updated_at": ISODate("2019-12-05T23:53:43.330Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5c5d422055a6cc290aae530e"),
  "lang": "fr",
  "feature": "",
  "short_description": "",
  "created_at": ISODate("2019-02-08T08:47:28.613Z"),
  "updated_at": ISODate("2019-12-05T23:53:43.330Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5c5d422055a6cc290aae5310"),
  "lang": "Gr",
  "feature": "",
  "short_description": "",
  "created_at": ISODate("2019-02-08T08:47:28.618Z"),
  "updated_at": ISODate("2019-12-05T23:53:43.330Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5c5d422055a6cc290aae5312"),
  "lang": "Sp",
  "feature": "",
  "short_description": "",
  "created_at": ISODate("2019-02-08T08:47:28.621Z"),
  "updated_at": ISODate("2019-12-05T23:53:43.330Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5c5d422e55a6cc290aae5319"),
  "lang": "en",
  "feature": "<p>Pediatric Partial Weight Bearing devices</p>\r\n\r\n<p>Provides postural support and biomechnically appropriate posture</p>\r\n\r\n<p>Use overground or over a treadmill</p>\r\n",
  "short_description": "Meet the needs of the smallest of patients through teenagers with our full range of pediatric LiteGait products. From our smallest pediatric device, the walkable 75 to the 200P that supports pediatrics through young adult, our pediatric devices can provide all your patients with a safe supported partial weight bearing environment for gait therapy. Pediatric LiteGait models start at less than $2,500",
  "created_at": ISODate("2019-02-08T08:47:42.762Z"),
  "updated_at": ISODate("2019-05-03T09:47:24.305Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5c5d422e55a6cc290aae531b"),
  "lang": "fr",
  "feature": "",
  "short_description": "",
  "created_at": ISODate("2019-02-08T08:47:42.764Z"),
  "updated_at": ISODate("2019-05-03T09:47:24.305Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5c5d422e55a6cc290aae531d"),
  "lang": "Gr",
  "feature": "",
  "short_description": "",
  "created_at": ISODate("2019-02-08T08:47:42.766Z"),
  "updated_at": ISODate("2019-05-03T09:47:24.305Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5c5d422e55a6cc290aae531f"),
  "lang": "Sp",
  "feature": "",
  "short_description": "",
  "created_at": ISODate("2019-02-08T08:47:42.768Z"),
  "updated_at": ISODate("2019-05-03T09:47:24.305Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5c6689bfbc19ad187c839c61"),
  "lang": "en",
  "feature": "<p>LiteGait I Battery</p>\r\n",
  "short_description": "LiteGait I Battery",
  "created_at": ISODate("2019-02-15T09:43:27.148Z"),
  "updated_at": ISODate("2019-07-18T17:10:37.456Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5c6689bfbc19ad187c839c63"),
  "lang": "fr",
  "feature": "",
  "short_description": "",
  "created_at": ISODate("2019-02-15T09:43:27.226Z"),
  "updated_at": ISODate("2019-07-18T17:10:37.457Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5c6689bfbc19ad187c839c65"),
  "lang": "Gr",
  "feature": "",
  "short_description": "",
  "created_at": ISODate("2019-02-15T09:43:27.237Z"),
  "updated_at": ISODate("2019-07-18T17:10:37.457Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5c6689bfbc19ad187c839c67"),
  "lang": "Sp",
  "feature": "",
  "short_description": "",
  "created_at": ISODate("2019-02-15T09:43:27.244Z"),
  "updated_at": ISODate("2019-07-18T17:10:37.457Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5c6689d2bc19ad187c839c6d"),
  "lang": "en",
  "feature": "",
  "short_description": "Control Box",
  "created_at": ISODate("2019-02-15T09:43:46.861Z"),
  "updated_at": ISODate("2019-07-24T05:14:22.680Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5c6689d2bc19ad187c839c6f"),
  "lang": "fr",
  "feature": "",
  "short_description": "",
  "created_at": ISODate("2019-02-15T09:43:46.864Z"),
  "updated_at": ISODate("2019-07-24T05:14:22.680Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5c6689d2bc19ad187c839c71"),
  "lang": "Gr",
  "feature": "",
  "short_description": "",
  "created_at": ISODate("2019-02-15T09:43:46.866Z"),
  "updated_at": ISODate("2019-07-24T05:14:22.680Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5c6689d2bc19ad187c839c73"),
  "lang": "Sp",
  "feature": "",
  "short_description": "",
  "created_at": ISODate("2019-02-15T09:43:46.868Z"),
  "updated_at": ISODate("2019-07-24T05:14:22.680Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5c6c5dcd092aa62529ff4ce3"),
  "lang": "en",
  "feature": "<p>these classes are done by speakers</p>\r\n",
  "short_description": "Clinicians present clinical cases of varied patient scenarios using LiteGait. Specific cases will relate to patient diagnoses or to a specific targeted outcome. Each segment features an individual patient, followed by Q & A.",
  "created_at": ISODate("2019-02-19T19:49:33.593Z"),
  "updated_at": ISODate("2019-09-09T03:51:54.124Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5c6c5dcd092aa62529ff4ce5"),
  "lang": "fr",
  "feature": "",
  "short_description": "",
  "created_at": ISODate("2019-02-19T19:49:33.596Z"),
  "updated_at": ISODate("2019-09-09T03:51:54.124Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5c6c5dcd092aa62529ff4ce7"),
  "lang": "Gr",
  "feature": "",
  "short_description": "",
  "created_at": ISODate("2019-02-19T19:49:33.599Z"),
  "updated_at": ISODate("2019-09-09T03:51:54.124Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5c6c5dcd092aa62529ff4ce9"),
  "lang": "Sp",
  "feature": "",
  "short_description": "",
  "created_at": ISODate("2019-02-19T19:49:33.602Z"),
  "updated_at": ISODate("2019-09-09T03:51:54.124Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5c6c5e15092aa62529ff4cef"),
  "lang": "en",
  "feature": "<p>Kayli, does her thing</p>\r\n",
  "short_description": "Webinar series dedicated to the ongoing education of LiteGait users. It is designed to provide instruction to new users, updates to veteran users, re-energize the occasional user and empower the every day user.",
  "created_at": ISODate("2019-02-19T19:50:45.929Z"),
  "updated_at": ISODate("2019-08-16T23:11:17.693Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5c6c5e15092aa62529ff4cf1"),
  "lang": "fr",
  "feature": "",
  "short_description": "",
  "created_at": ISODate("2019-02-19T19:50:45.931Z"),
  "updated_at": ISODate("2019-08-16T23:11:17.693Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5c6c5e15092aa62529ff4cf3"),
  "lang": "Gr",
  "feature": "",
  "short_description": "",
  "created_at": ISODate("2019-02-19T19:50:45.933Z"),
  "updated_at": ISODate("2019-08-16T23:11:17.693Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5c6c5e15092aa62529ff4cf5"),
  "lang": "Sp",
  "feature": "",
  "short_description": "",
  "created_at": ISODate("2019-02-19T19:50:45.935Z"),
  "updated_at": ISODate("2019-08-16T23:11:17.693Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5c748821092aa62529ff4d71"),
  "lang": "en",
  "feature": "<p>Mobility Research provides a full line of pediatric devices.</p>\r\n\r\n<p>Our pediatric designs create an ideal setting to assess.</p>\r\n",
  "short_description": "<p>Mobility Research provides a full line of pediatric devices. Our pediatric designs create an ideal setting to assess, evaluate and treat individuals of varying sizes, impairments and functional levels. From our smallest pediatric device, the LG75&nbsp;up to the LG200P that supports infants through young adults.&nbsp;</p>\r\n",
  "created_at": ISODate("2019-02-26T00:28:17.29Z"),
  "updated_at": ISODate("2019-12-05T23:54:53.368Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5c748821092aa62529ff4d73"),
  "lang": "fr",
  "feature": "",
  "short_description": "",
  "created_at": ISODate("2019-02-26T00:28:17.38Z"),
  "updated_at": ISODate("2019-12-05T23:54:53.369Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5c748821092aa62529ff4d75"),
  "lang": "Gr",
  "feature": "",
  "short_description": "",
  "created_at": ISODate("2019-02-26T00:28:17.44Z"),
  "updated_at": ISODate("2019-12-05T23:54:53.369Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5c748821092aa62529ff4d77"),
  "lang": "Sp",
  "feature": "",
  "short_description": "",
  "created_at": ISODate("2019-02-26T00:28:17.50Z"),
  "updated_at": ISODate("2019-12-05T23:54:53.369Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5c78f2e88592290d03e1ff91"),
  "lang": "en",
  "feature": "<p>Billing and Membership</p>\r\n",
  "short_description": "Billing and Membership",
  "created_at": ISODate("2019-03-01T08:52:56.313Z"),
  "updated_at": ISODate("2019-09-13T00:06:30.141Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5c78f2e88592290d03e1ff93"),
  "lang": "fr",
  "feature": "",
  "short_description": "",
  "created_at": ISODate("2019-03-01T08:52:56.325Z"),
  "updated_at": ISODate("2019-09-13T00:06:30.141Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5c78f2e88592290d03e1ff95"),
  "lang": "Gr",
  "feature": "",
  "short_description": "",
  "created_at": ISODate("2019-03-01T08:52:56.330Z"),
  "updated_at": ISODate("2019-09-13T00:06:30.141Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5c78f2e88592290d03e1ff97"),
  "lang": "Sp",
  "feature": "",
  "short_description": "",
  "created_at": ISODate("2019-03-01T08:52:56.334Z"),
  "updated_at": ISODate("2019-09-13T00:06:30.141Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5c78f2f48592290d03e1ff9d"),
  "lang": "en",
  "feature": "<p>Billing and Payment</p>\r\n",
  "short_description": "Billing and Payment",
  "created_at": ISODate("2019-03-01T08:53:08.14Z"),
  "updated_at": ISODate("2019-03-01T08:53:08.14Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5c78f2f48592290d03e1ff9f"),
  "lang": "fr",
  "feature": "",
  "short_description": "",
  "created_at": ISODate("2019-03-01T08:53:08.16Z"),
  "updated_at": ISODate("2019-03-01T08:53:08.16Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5c78f2f48592290d03e1ffa1"),
  "lang": "Gr",
  "feature": "",
  "short_description": "",
  "created_at": ISODate("2019-03-01T08:53:08.18Z"),
  "updated_at": ISODate("2019-03-01T08:53:08.18Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5c78f2f48592290d03e1ffa3"),
  "lang": "Sp",
  "feature": "",
  "short_description": "",
  "created_at": ISODate("2019-03-01T08:53:08.20Z"),
  "updated_at": ISODate("2019-03-01T08:53:08.20Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5c78f3038592290d03e1ffa9"),
  "lang": "en",
  "feature": "<p>Account Settings</p>\r\n",
  "short_description": "Account Settings",
  "created_at": ISODate("2019-03-01T08:53:23.54Z"),
  "updated_at": ISODate("2019-03-01T08:53:23.54Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5c78f3038592290d03e1ffab"),
  "lang": "fr",
  "feature": "",
  "short_description": "",
  "created_at": ISODate("2019-03-01T08:53:23.57Z"),
  "updated_at": ISODate("2019-03-01T08:53:23.57Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5c78f3038592290d03e1ffad"),
  "lang": "Gr",
  "feature": "",
  "short_description": "",
  "created_at": ISODate("2019-03-01T08:53:23.59Z"),
  "updated_at": ISODate("2019-03-01T08:53:23.59Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5c78f3038592290d03e1ffaf"),
  "lang": "Sp",
  "feature": "",
  "short_description": "",
  "created_at": ISODate("2019-03-01T08:53:23.61Z"),
  "updated_at": ISODate("2019-03-01T08:53:23.61Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5c78f39e8592290d03e1ffdd"),
  "lang": "en",
  "feature": "<p>Battery</p>\r\n",
  "short_description": "Battery",
  "created_at": ISODate("2019-03-01T08:55:58.737Z"),
  "updated_at": ISODate("2019-03-01T08:55:58.737Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5c78f39e8592290d03e1ffdf"),
  "lang": "fr",
  "feature": "",
  "short_description": "",
  "created_at": ISODate("2019-03-01T08:55:58.740Z"),
  "updated_at": ISODate("2019-03-01T08:55:58.740Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5c78f39e8592290d03e1ffe1"),
  "lang": "Gr",
  "feature": "",
  "short_description": "",
  "created_at": ISODate("2019-03-01T08:55:58.741Z"),
  "updated_at": ISODate("2019-03-01T08:55:58.741Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5c78f39e8592290d03e1ffe3"),
  "lang": "Sp",
  "feature": "",
  "short_description": "",
  "created_at": ISODate("2019-03-01T08:55:58.743Z"),
  "updated_at": ISODate("2019-03-01T08:55:58.743Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5c78f3cd8592290d03e1ffee"),
  "lang": "en",
  "feature": "<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n",
  "short_description": "Objective measurement provided by the amount of LiteGait support",
  "created_at": ISODate("2019-03-01T08:56:45.654Z"),
  "updated_at": ISODate("2019-09-12T15:51:01.403Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5c78f3cd8592290d03e1fff0"),
  "lang": "fr",
  "feature": "",
  "short_description": "",
  "created_at": ISODate("2019-03-01T08:56:45.656Z"),
  "updated_at": ISODate("2019-09-12T15:51:01.403Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5c78f3cd8592290d03e1fff2"),
  "lang": "Gr",
  "feature": "",
  "short_description": "",
  "created_at": ISODate("2019-03-01T08:56:45.658Z"),
  "updated_at": ISODate("2019-09-12T15:51:01.403Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5c78f3cd8592290d03e1fff4"),
  "lang": "Sp",
  "feature": "",
  "short_description": "",
  "created_at": ISODate("2019-03-01T08:56:45.660Z"),
  "updated_at": ISODate("2019-09-12T15:51:01.403Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5c78f6718592290d03e20041"),
  "lang": "en",
  "feature": "<p>LiteGait 4 Home</p>\r\n",
  "short_description": "<p>LiteGait 4 Home Description comes here. LiteGait 4 Home Description comes here.LiteGait 4 Home Description comes here.LiteGait 4 Home Description comes here.LiteGait 4 Home Description comes here.LiteGait 4 Home Description comes here.LiteGait 4 Home Description comes here.LiteGait 4 Home Description comes here.LiteGait 4 Home Description comes here.LiteGait 4 Home Description comes here.</p>\r\n",
  "created_at": ISODate("2019-03-01T09:08:01.847Z"),
  "updated_at": ISODate("2019-12-05T23:57:57.458Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5c78f6718592290d03e20043"),
  "lang": "fr",
  "feature": "",
  "short_description": "",
  "created_at": ISODate("2019-03-01T09:08:01.849Z"),
  "updated_at": ISODate("2019-12-05T23:57:57.458Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5c78f6718592290d03e20045"),
  "lang": "Gr",
  "feature": "",
  "short_description": "",
  "created_at": ISODate("2019-03-01T09:08:01.851Z"),
  "updated_at": ISODate("2019-12-05T23:57:57.458Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5c78f6718592290d03e20047"),
  "lang": "Sp",
  "feature": "",
  "short_description": "",
  "created_at": ISODate("2019-03-01T09:08:01.852Z"),
  "updated_at": ISODate("2019-12-05T23:57:57.459Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5c78f6ae8592290d03e2004d"),
  "lang": "en",
  "feature": "<p>Rehabilitation treadmills designed for both adults and children</p>\r\n\r\n<p>True zero start speed - no need to straddle the belt</p>\r\n",
  "short_description": "<p>Rehabilitation treadmills designed for both adults and children True zero start speed - no need to straddle the beltRehabilitation treadmills designed for both adults and children True zero start speed - no need to straddle the beltRehabilitation treadmills designed for both adults and children True zero start speed - no need to straddle the beltRehabilitation treadmills designed for both adults and children True zero start speed - no need to straddle the belt</p>\r\n",
  "created_at": ISODate("2019-03-01T09:09:02.66Z"),
  "updated_at": ISODate("2019-12-05T23:59:24.9Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5c78f6ae8592290d03e2004f"),
  "lang": "fr",
  "feature": "",
  "short_description": "",
  "created_at": ISODate("2019-03-01T09:09:02.68Z"),
  "updated_at": ISODate("2019-12-05T23:59:24.9Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5c78f6ae8592290d03e20051"),
  "lang": "Gr",
  "feature": "",
  "short_description": "",
  "created_at": ISODate("2019-03-01T09:09:02.70Z"),
  "updated_at": ISODate("2019-12-05T23:59:24.9Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5c78f6ae8592290d03e20053"),
  "lang": "Sp",
  "feature": "",
  "short_description": "",
  "created_at": ISODate("2019-03-01T09:09:02.72Z"),
  "updated_at": ISODate("2019-12-05T23:59:24.9Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5c88fdde8592290d03e200fb"),
  "lang": "en",
  "feature": "<p>Case ReportsCase ReportsCase ReportsCase Reports</p>\r\n",
  "short_description": "CEU clinical webinars are designed for busy professionals. 90 minute presentations are packed with useful information augmented with instructional videos on a multitude of topics",
  "created_at": ISODate("2019-03-13T12:55:58.88Z"),
  "updated_at": ISODate("2019-08-16T23:14:41.333Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5c88fdde8592290d03e200fd"),
  "lang": "fr",
  "feature": "",
  "short_description": "",
  "created_at": ISODate("2019-03-13T12:55:58.97Z"),
  "updated_at": ISODate("2019-08-16T23:14:41.333Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5c88fdde8592290d03e200ff"),
  "lang": "Gr",
  "feature": "",
  "short_description": "",
  "created_at": ISODate("2019-03-13T12:55:58.99Z"),
  "updated_at": ISODate("2019-08-16T23:14:41.333Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5c88fdde8592290d03e20101"),
  "lang": "Sp",
  "feature": "",
  "short_description": "",
  "created_at": ISODate("2019-03-13T12:55:58.100Z"),
  "updated_at": ISODate("2019-08-16T23:14:41.333Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5c88fdf88592290d03e20106"),
  "lang": "en",
  "feature": "<p>Journal ClubJournal Club</p>\r\n",
  "short_description": "Our quarterly Journal Club is part of our live, moderated online forum. It is interactive and designed to explain and discuss current published work.",
  "created_at": ISODate("2019-03-13T12:56:24.545Z"),
  "updated_at": ISODate("2019-08-16T23:16:34.418Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5c88fdf88592290d03e20108"),
  "lang": "fr",
  "feature": "",
  "short_description": "",
  "created_at": ISODate("2019-03-13T12:56:24.547Z"),
  "updated_at": ISODate("2019-08-16T23:16:34.418Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5c88fdf88592290d03e2010a"),
  "lang": "Gr",
  "feature": "",
  "short_description": "",
  "created_at": ISODate("2019-03-13T12:56:24.548Z"),
  "updated_at": ISODate("2019-08-16T23:16:34.418Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5c88fdf88592290d03e2010c"),
  "lang": "Sp",
  "feature": "",
  "short_description": "",
  "created_at": ISODate("2019-03-13T12:56:24.550Z"),
  "updated_at": ISODate("2019-08-16T23:16:34.418Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5cf0096dfb42690c45d441bf"),
  "lang": "en",
  "feature": "<p>Therapy Mouse is a low cost, motion-sensing, wearable pointing device</p>\r\n\r\n<p>translate user movements to precise, proportional computer control.&nbsp;</p>\r\n",
  "short_description": "<p>Therapy Mouse is a low cost, motion-sensing, wearable pointing device, that allows individuals who have difficulties using their hands to control a computer using any alternative body part. Simply plug into your computer and attach the sensor to the desired body part; the Therapy Mouse will translate user movements to precise, proportional computer control.&nbsp;</p>\r\n",
  "created_at": ISODate("2019-05-30T16:48:45.500Z"),
  "updated_at": ISODate("2019-12-06T00:07:49.958Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5cf0096dfb42690c45d441c1"),
  "lang": "fr",
  "feature": "",
  "short_description": "",
  "created_at": ISODate("2019-05-30T16:48:45.503Z"),
  "updated_at": ISODate("2019-12-06T00:07:49.958Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5cf0096dfb42690c45d441c3"),
  "lang": "Gr",
  "feature": "",
  "short_description": "",
  "created_at": ISODate("2019-05-30T16:48:45.505Z"),
  "updated_at": ISODate("2019-12-06T00:07:49.958Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5cf0096dfb42690c45d441c5"),
  "lang": "Sp",
  "feature": "",
  "short_description": "",
  "created_at": ISODate("2019-05-30T16:48:45.507Z"),
  "updated_at": ISODate("2019-12-06T00:07:49.958Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5cfa390860b1b8748527eda1"),
  "lang": "en",
  "feature": "",
  "short_description": "My ProfileMy ProfileMy Profile",
  "created_at": ISODate("2019-06-07T10:14:32.501Z"),
  "updated_at": ISODate("2019-06-07T10:14:32.501Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5cfa390860b1b8748527eda3"),
  "lang": "fr",
  "feature": "",
  "short_description": "",
  "created_at": ISODate("2019-06-07T10:14:32.506Z"),
  "updated_at": ISODate("2019-06-07T10:14:32.506Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5cfa390860b1b8748527eda5"),
  "lang": "Gr",
  "feature": "",
  "short_description": "",
  "created_at": ISODate("2019-06-07T10:14:32.508Z"),
  "updated_at": ISODate("2019-06-07T10:14:32.508Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5cfa390860b1b8748527eda7"),
  "lang": "Sp",
  "feature": "",
  "short_description": "",
  "created_at": ISODate("2019-06-07T10:14:32.512Z"),
  "updated_at": ISODate("2019-06-07T10:14:32.512Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5cfa391d60b1b8748527edad"),
  "lang": "en",
  "feature": "",
  "short_description": "My PaymentMy PaymentMy Payment",
  "created_at": ISODate("2019-06-07T10:14:53.186Z"),
  "updated_at": ISODate("2019-06-07T10:14:53.186Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5cfa391d60b1b8748527edaf"),
  "lang": "fr",
  "feature": "",
  "short_description": "",
  "created_at": ISODate("2019-06-07T10:14:53.188Z"),
  "updated_at": ISODate("2019-06-07T10:14:53.188Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5cfa391d60b1b8748527edb1"),
  "lang": "Gr",
  "feature": "",
  "short_description": "",
  "created_at": ISODate("2019-06-07T10:14:53.190Z"),
  "updated_at": ISODate("2019-06-07T10:14:53.190Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5cfa391d60b1b8748527edb3"),
  "lang": "Sp",
  "feature": "",
  "short_description": "",
  "created_at": ISODate("2019-06-07T10:14:53.192Z"),
  "updated_at": ISODate("2019-06-07T10:14:53.192Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5cfa393b60b1b8748527edba"),
  "lang": "en",
  "feature": "",
  "short_description": "Payment PlanPayment Plan",
  "created_at": ISODate("2019-06-07T10:15:23.530Z"),
  "updated_at": ISODate("2019-06-07T10:15:23.530Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5cfa393b60b1b8748527edbc"),
  "lang": "fr",
  "feature": "",
  "short_description": "",
  "created_at": ISODate("2019-06-07T10:15:23.532Z"),
  "updated_at": ISODate("2019-06-07T10:15:23.532Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5cfa393b60b1b8748527edbe"),
  "lang": "Gr",
  "feature": "",
  "short_description": "",
  "created_at": ISODate("2019-06-07T10:15:23.534Z"),
  "updated_at": ISODate("2019-06-07T10:15:23.534Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5cfa393b60b1b8748527edc0"),
  "lang": "Sp",
  "feature": "",
  "short_description": "",
  "created_at": ISODate("2019-06-07T10:15:23.536Z"),
  "updated_at": ISODate("2019-06-07T10:15:23.536Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5cffc88060b1b8748527ee04"),
  "lang": "en",
  "feature": "",
  "short_description": "test test\r\npay here",
  "created_at": ISODate("2019-06-11T15:28:00.910Z"),
  "updated_at": ISODate("2019-06-11T15:28:00.910Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5cffc88060b1b8748527ee06"),
  "lang": "fr",
  "feature": "",
  "short_description": "",
  "created_at": ISODate("2019-06-11T15:28:00.912Z"),
  "updated_at": ISODate("2019-06-11T15:28:00.912Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5cffc88060b1b8748527ee08"),
  "lang": "Gr",
  "feature": "",
  "short_description": "",
  "created_at": ISODate("2019-06-11T15:28:00.914Z"),
  "updated_at": ISODate("2019-06-11T15:28:00.914Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5cffc88060b1b8748527ee0a"),
  "lang": "Sp",
  "feature": "",
  "short_description": "",
  "created_at": ISODate("2019-06-11T15:28:00.916Z"),
  "updated_at": ISODate("2019-06-11T15:28:00.916Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5d09056560b1b8748527eefb"),
  "lang": "en",
  "feature": "",
  "short_description": "webinars",
  "created_at": ISODate("2019-06-18T15:38:13.333Z"),
  "updated_at": ISODate("2019-06-18T15:38:13.333Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5d09056560b1b8748527eefd"),
  "lang": "fr",
  "feature": "",
  "short_description": "",
  "created_at": ISODate("2019-06-18T15:38:13.338Z"),
  "updated_at": ISODate("2019-06-18T15:38:13.338Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5d09056560b1b8748527eeff"),
  "lang": "Gr",
  "feature": "",
  "short_description": "",
  "created_at": ISODate("2019-06-18T15:38:13.342Z"),
  "updated_at": ISODate("2019-06-18T15:38:13.342Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5d09056560b1b8748527ef01"),
  "lang": "Sp",
  "feature": "",
  "short_description": "",
  "created_at": ISODate("2019-06-18T15:38:13.346Z"),
  "updated_at": ISODate("2019-06-18T15:38:13.346Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5d0905c260b1b8748527ef07"),
  "lang": "en",
  "feature": "",
  "short_description": "Attending a Webinar",
  "created_at": ISODate("2019-06-18T15:39:46.659Z"),
  "updated_at": ISODate("2019-06-18T15:39:46.659Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5d0905c260b1b8748527ef09"),
  "lang": "fr",
  "feature": "",
  "short_description": "",
  "created_at": ISODate("2019-06-18T15:39:46.661Z"),
  "updated_at": ISODate("2019-06-18T15:39:46.661Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5d0905c260b1b8748527ef0b"),
  "lang": "Gr",
  "feature": "",
  "short_description": "",
  "created_at": ISODate("2019-06-18T15:39:46.663Z"),
  "updated_at": ISODate("2019-06-18T15:39:46.663Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5d0905c260b1b8748527ef0d"),
  "lang": "Sp",
  "feature": "",
  "short_description": "",
  "created_at": ISODate("2019-06-18T15:39:46.665Z"),
  "updated_at": ISODate("2019-06-18T15:39:46.665Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5d0905df60b1b8748527ef13"),
  "lang": "en",
  "feature": "",
  "short_description": "Presenting a Webinar",
  "created_at": ISODate("2019-06-18T15:40:15.498Z"),
  "updated_at": ISODate("2019-06-18T15:40:15.498Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5d0905df60b1b8748527ef15"),
  "lang": "fr",
  "feature": "",
  "short_description": "",
  "created_at": ISODate("2019-06-18T15:40:15.500Z"),
  "updated_at": ISODate("2019-06-18T15:40:15.500Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5d0905df60b1b8748527ef17"),
  "lang": "Gr",
  "feature": "",
  "short_description": "",
  "created_at": ISODate("2019-06-18T15:40:15.502Z"),
  "updated_at": ISODate("2019-06-18T15:40:15.502Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5d0905df60b1b8748527ef19"),
  "lang": "Sp",
  "feature": "",
  "short_description": "",
  "created_at": ISODate("2019-06-18T15:40:15.504Z"),
  "updated_at": ISODate("2019-06-18T15:40:15.504Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5d09060960b1b8748527ef1f"),
  "lang": "en",
  "feature": "",
  "short_description": "webinar recordings",
  "created_at": ISODate("2019-06-18T15:40:57.104Z"),
  "updated_at": ISODate("2019-06-18T15:40:57.104Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5d09060960b1b8748527ef21"),
  "lang": "fr",
  "feature": "",
  "short_description": "",
  "created_at": ISODate("2019-06-18T15:40:57.106Z"),
  "updated_at": ISODate("2019-06-18T15:40:57.106Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5d09060960b1b8748527ef23"),
  "lang": "Gr",
  "feature": "",
  "short_description": "",
  "created_at": ISODate("2019-06-18T15:40:57.108Z"),
  "updated_at": ISODate("2019-06-18T15:40:57.108Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5d09060960b1b8748527ef25"),
  "lang": "Sp",
  "feature": "",
  "short_description": "",
  "created_at": ISODate("2019-06-18T15:40:57.110Z"),
  "updated_at": ISODate("2019-06-18T15:40:57.110Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5d14f1540a2c2e561c73512c"),
  "lang": "en",
  "feature": "<p>Objective gait data</p>\r\n\r\n<p>Available on any Gaitkeeper treadmill</p>\r\n",
  "short_description": "<p>Mobility Research brings the latest advancements in Gait, Balance assessment and therapy. Our assessment tools quickly and easily provide targeted, meaningful gait and balance assessment data to help you assess and monitor objective progress of your patient.</p>\r\n",
  "created_at": ISODate("2019-06-27T16:39:48.837Z"),
  "updated_at": ISODate("2019-11-05T18:37:27.693Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5d14f1540a2c2e561c73512e"),
  "lang": "fr",
  "feature": "",
  "short_description": "",
  "created_at": ISODate("2019-06-27T16:39:48.840Z"),
  "updated_at": ISODate("2019-11-05T18:37:27.693Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5d14f1540a2c2e561c735130"),
  "lang": "Gr",
  "feature": "",
  "short_description": "",
  "created_at": ISODate("2019-06-27T16:39:48.842Z"),
  "updated_at": ISODate("2019-11-05T18:37:27.693Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5d14f1540a2c2e561c735132"),
  "lang": "Sp",
  "feature": "",
  "short_description": "",
  "created_at": ISODate("2019-06-27T16:39:48.844Z"),
  "updated_at": ISODate("2019-11-05T18:37:27.693Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5d2de7f4837f487c405e62e3"),
  "lang": "en",
  "feature": "<p><strong>The Innovative Balance Training System</strong></p>\r\n\r\n<p>Q-pads are an interactive rehabilitation system providing the user and clinician visual feedback via a pressure sensitive surface and bright multi-colored LEDs. Appropriate for children and adults, upper and lower limbs, in sitting, standing or quadruped. They are easy to set up, strong enough to step on and even magnetic for vertical use. Different functions are available allowing the therapist to customize activities using multiple pads to tailor to the various needs of each use</p>\r\n",
  "short_description": "<p>Work on motor and cognitive skills in an entertaining wa</p>\r\n",
  "created_at": ISODate("2019-07-16T15:06:28.214Z"),
  "updated_at": ISODate("2019-11-06T21:52:50.735Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5d2de7f4837f487c405e62e5"),
  "lang": "fr",
  "feature": "",
  "short_description": "",
  "created_at": ISODate("2019-07-16T15:06:28.219Z"),
  "updated_at": ISODate("2019-11-06T21:52:50.735Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5d2de7f4837f487c405e62e7"),
  "lang": "Gr",
  "feature": "",
  "short_description": "",
  "created_at": ISODate("2019-07-16T15:06:28.221Z"),
  "updated_at": ISODate("2019-11-06T21:52:50.735Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5d2de7f4837f487c405e62e9"),
  "lang": "Sp",
  "feature": "",
  "short_description": "",
  "created_at": ISODate("2019-07-16T15:06:28.223Z"),
  "updated_at": ISODate("2019-11-06T21:52:50.735Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5d2de90b837f487c405e62ef"),
  "lang": "en",
  "feature": "<p>The versatile Mobility Device&trade; lift walker product line provides supported ambulation, sit-to-stand assistance and seated-to-seated transfers for patients. The easy-to-use harness simplifies the process of getting patients upright, and assists balance during ambulation for a comfortable and confidence-building experience. Three models are available to fit different patient sizes.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n",
  "short_description": "<p>Our Supported Ambulation Mobility Devices, HugN-Go, provide the versatility of three devices in one - supported ambulation, sit to stand and seated to seated transfers. The HugN-Go hugs the user to provide a safe, upright posture with plenty of leg room so the patient can GO! We hug and the user goes!</p>\r\n",
  "created_at": ISODate("2019-07-16T15:11:07.549Z"),
  "updated_at": ISODate("2019-12-06T19:27:12.607Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5d2de90b837f487c405e62f1"),
  "lang": "fr",
  "feature": "",
  "short_description": "",
  "created_at": ISODate("2019-07-16T15:11:07.553Z"),
  "updated_at": ISODate("2019-12-06T19:27:12.607Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5d2de90b837f487c405e62f3"),
  "lang": "Gr",
  "feature": "",
  "short_description": "",
  "created_at": ISODate("2019-07-16T15:11:07.555Z"),
  "updated_at": ISODate("2019-12-06T19:27:12.607Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5d2de90b837f487c405e62f5"),
  "lang": "Sp",
  "feature": "",
  "short_description": "",
  "created_at": ISODate("2019-07-16T15:11:07.556Z"),
  "updated_at": ISODate("2019-12-06T19:27:12.607Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5d37d4302a02f42595837140"),
  "lang": "en",
  "feature": "<p>Leverage all the benefits of LiteGait for our four legged friends</p>\r\n",
  "short_description": "<p>Assist animal in moving over ground or a slow treadmill Assist handlers in lifting the animal into standing position Leverage all the benefits of LiteGait for our four legged friends</p>\r\n",
  "created_at": ISODate("2019-07-24T03:44:48.3Z"),
  "updated_at": ISODate("2019-12-06T00:06:23.746Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5d37d4302a02f42595837142"),
  "lang": "fr",
  "feature": "",
  "short_description": "",
  "created_at": ISODate("2019-07-24T03:44:48.5Z"),
  "updated_at": ISODate("2019-12-06T00:06:23.746Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5d37d4302a02f42595837144"),
  "lang": "Gr",
  "feature": "",
  "short_description": "",
  "created_at": ISODate("2019-07-24T03:44:48.7Z"),
  "updated_at": ISODate("2019-12-06T00:06:23.746Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5d37d4302a02f42595837146"),
  "lang": "Sp",
  "feature": "",
  "short_description": "",
  "created_at": ISODate("2019-07-24T03:44:48.9Z"),
  "updated_at": ISODate("2019-12-06T00:06:23.746Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5d37df0c2a02f42595837166"),
  "lang": "en",
  "feature": "<p><strong><span style=\"color:#ab3585\">Turn little function to big advantage!</span></strong><br />\r\nIt supports the body in a quadruped posture for weight bearing and crawling, offering a multitude of benefits while helping to achieve an important milestone in an infant&rsquo;s life. Appropriate for facility or home use.</p>\r\n",
  "short_description": "<p>Gentle Crawling Assistance</p>\r\n",
  "created_at": ISODate("2019-07-24T04:31:08.72Z"),
  "updated_at": ISODate("2019-12-06T19:20:07.392Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5d37df0c2a02f42595837168"),
  "lang": "fr",
  "feature": "",
  "short_description": "",
  "created_at": ISODate("2019-07-24T04:31:08.74Z"),
  "updated_at": ISODate("2019-12-06T19:20:07.392Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5d37df0c2a02f4259583716a"),
  "lang": "Gr",
  "feature": "",
  "short_description": "",
  "created_at": ISODate("2019-07-24T04:31:08.76Z"),
  "updated_at": ISODate("2019-12-06T19:20:07.392Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5d37df0c2a02f4259583716c"),
  "lang": "Sp",
  "feature": "",
  "short_description": "",
  "created_at": ISODate("2019-07-24T04:31:08.78Z"),
  "updated_at": ISODate("2019-12-06T19:20:07.392Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5d37ea652a02f4259583719d"),
  "lang": "en",
  "feature": "",
  "short_description": "A/C adapters for LiteGait",
  "created_at": ISODate("2019-07-24T05:19:33.362Z"),
  "updated_at": ISODate("2019-07-24T05:19:33.362Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5d37ea652a02f4259583719f"),
  "lang": "fr",
  "feature": "",
  "short_description": "",
  "created_at": ISODate("2019-07-24T05:19:33.364Z"),
  "updated_at": ISODate("2019-07-24T05:19:33.364Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5d37ea652a02f425958371a1"),
  "lang": "Gr",
  "feature": "",
  "short_description": "",
  "created_at": ISODate("2019-07-24T05:19:33.365Z"),
  "updated_at": ISODate("2019-07-24T05:19:33.365Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5d37ea652a02f425958371a3"),
  "lang": "Sp",
  "feature": "",
  "short_description": "",
  "created_at": ISODate("2019-07-24T05:19:33.367Z"),
  "updated_at": ISODate("2019-07-24T05:19:33.367Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5d37ebfa2a02f425958371a9"),
  "lang": "en",
  "feature": "",
  "short_description": "Hand Switches for LiteGait and HugN-Go",
  "created_at": ISODate("2019-07-24T05:26:18.683Z"),
  "updated_at": ISODate("2019-09-10T15:14:34.686Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5d37ebfa2a02f425958371ab"),
  "lang": "fr",
  "feature": "",
  "short_description": "",
  "created_at": ISODate("2019-07-24T05:26:18.687Z"),
  "updated_at": ISODate("2019-09-10T15:14:34.686Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5d37ebfa2a02f425958371ad"),
  "lang": "Gr",
  "feature": "",
  "short_description": "",
  "created_at": ISODate("2019-07-24T05:26:18.691Z"),
  "updated_at": ISODate("2019-09-10T15:14:34.686Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5d37ebfa2a02f425958371af"),
  "lang": "Sp",
  "feature": "",
  "short_description": "",
  "created_at": ISODate("2019-07-24T05:26:18.694Z"),
  "updated_at": ISODate("2019-09-10T15:14:34.687Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5d37f4352a02f425958371b7"),
  "lang": "en",
  "feature": "",
  "short_description": "Casters for LiteGait",
  "created_at": ISODate("2019-07-24T06:01:25.647Z"),
  "updated_at": ISODate("2019-07-24T06:01:25.647Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5d37f4352a02f425958371b9"),
  "lang": "fr",
  "feature": "",
  "short_description": "",
  "created_at": ISODate("2019-07-24T06:01:25.649Z"),
  "updated_at": ISODate("2019-07-24T06:01:25.649Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5d37f4352a02f425958371bb"),
  "lang": "Gr",
  "feature": "",
  "short_description": "",
  "created_at": ISODate("2019-07-24T06:01:25.651Z"),
  "updated_at": ISODate("2019-07-24T06:01:25.651Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5d37f4352a02f425958371bd"),
  "lang": "Sp",
  "feature": "",
  "short_description": "",
  "created_at": ISODate("2019-07-24T06:01:25.653Z"),
  "updated_at": ISODate("2019-07-24T06:01:25.653Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5d3b1471622b4e58c2f18aab"),
  "lang": "en",
  "feature": "",
  "short_description": "no",
  "created_at": ISODate("2019-07-26T14:55:45.0Z"),
  "updated_at": ISODate("2019-07-26T14:55:45.0Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5d3b1471622b4e58c2f18aad"),
  "lang": "fr",
  "feature": "",
  "short_description": "",
  "created_at": ISODate("2019-07-26T14:55:45.4Z"),
  "updated_at": ISODate("2019-07-26T14:55:45.4Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5d3b1471622b4e58c2f18aaf"),
  "lang": "Gr",
  "feature": "",
  "short_description": "",
  "created_at": ISODate("2019-07-26T14:55:45.6Z"),
  "updated_at": ISODate("2019-07-26T14:55:45.6Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5d3b1471622b4e58c2f18ab1"),
  "lang": "Sp",
  "feature": "",
  "short_description": "",
  "created_at": ISODate("2019-07-26T14:55:45.8Z"),
  "updated_at": ISODate("2019-07-26T14:55:45.8Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5d485690a9421434c38e6899"),
  "lang": "en",
  "feature": "<p>If you miss the webinar you paid for a recording link will automatically be emailed to you. This recording link will be good for 3 months.</p>\r\n",
  "short_description": "where",
  "created_at": ISODate("2019-08-05T16:17:20.60Z"),
  "updated_at": ISODate("2019-08-05T16:17:20.60Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5d485690a9421434c38e689b"),
  "lang": "fr",
  "feature": "",
  "short_description": "",
  "created_at": ISODate("2019-08-05T16:17:20.84Z"),
  "updated_at": ISODate("2019-08-05T16:17:20.84Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5d485690a9421434c38e689d"),
  "lang": "Gr",
  "feature": "",
  "short_description": "",
  "created_at": ISODate("2019-08-05T16:17:20.87Z"),
  "updated_at": ISODate("2019-08-05T16:17:20.87Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5d485690a9421434c38e689f"),
  "lang": "Sp",
  "feature": "",
  "short_description": "",
  "created_at": ISODate("2019-08-05T16:17:20.89Z"),
  "updated_at": ISODate("2019-08-05T16:17:20.89Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5d485a4ca9421434c38e68b6"),
  "lang": "en",
  "feature": "",
  "short_description": "Missed the webinar I paid for",
  "created_at": ISODate("2019-08-05T16:33:16.516Z"),
  "updated_at": ISODate("2019-08-05T16:33:16.516Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5d485a4ca9421434c38e68b8"),
  "lang": "fr",
  "feature": "",
  "short_description": "",
  "created_at": ISODate("2019-08-05T16:33:16.518Z"),
  "updated_at": ISODate("2019-08-05T16:33:16.518Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5d485a4ca9421434c38e68ba"),
  "lang": "Gr",
  "feature": "",
  "short_description": "",
  "created_at": ISODate("2019-08-05T16:33:16.519Z"),
  "updated_at": ISODate("2019-08-05T16:33:16.519Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5d485a4ca9421434c38e68bc"),
  "lang": "Sp",
  "feature": "",
  "short_description": "",
  "created_at": ISODate("2019-08-05T16:33:16.521Z"),
  "updated_at": ISODate("2019-08-05T16:33:16.521Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5d767643d4748747ea5759bc"),
  "lang": "en",
  "feature": "<p>FlexAble offers the ability to adjust the flexibility of the LiteGait&reg; yoke.</p>\r\n",
  "short_description": " An adjustable knob allows the clinician to vary the rigidity of the yoke according to the patient's individual needs, providing valuable control to enhance a patient's therapy. As the patient improves, the FlexAble accommodates the vertical displacement that occurs at higher walking speeds",
  "created_at": ISODate("2019-09-09T15:56:51.519Z"),
  "updated_at": ISODate("2019-09-12T23:21:44.176Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5d767643d4748747ea5759be"),
  "lang": "fr",
  "feature": "",
  "short_description": "",
  "created_at": ISODate("2019-09-09T15:56:51.521Z"),
  "updated_at": ISODate("2019-09-12T23:21:44.176Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5d767643d4748747ea5759c0"),
  "lang": "Gr",
  "feature": "",
  "short_description": "",
  "created_at": ISODate("2019-09-09T15:56:51.523Z"),
  "updated_at": ISODate("2019-09-12T23:21:44.176Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5d767643d4748747ea5759c2"),
  "lang": "Sp",
  "feature": "",
  "short_description": "",
  "created_at": ISODate("2019-09-09T15:56:51.527Z"),
  "updated_at": ISODate("2019-09-12T23:21:44.177Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5d7679bbd4748747ea5759e7"),
  "lang": "en",
  "feature": "<p>Allows the patient/user to freely rotate in place beneath the yoke while remaining securely supported by the LiteGait&reg; harness.</p>\r\n",
  "short_description": "The FreeDome can be locked in any position allowing patient to walk sideways or backward on or off the treadmill, or simply walk with assistance when facing out of the LiteGait.",
  "created_at": ISODate("2019-09-09T16:11:39.906Z"),
  "updated_at": ISODate("2019-09-12T23:21:31.870Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5d7679bbd4748747ea5759e9"),
  "lang": "fr",
  "feature": "",
  "short_description": "",
  "created_at": ISODate("2019-09-09T16:11:39.909Z"),
  "updated_at": ISODate("2019-09-12T23:21:31.870Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5d7679bbd4748747ea5759eb"),
  "lang": "Gr",
  "feature": "",
  "short_description": "",
  "created_at": ISODate("2019-09-09T16:11:39.911Z"),
  "updated_at": ISODate("2019-09-12T23:21:31.871Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5d7679bbd4748747ea5759ed"),
  "lang": "Sp",
  "feature": "",
  "short_description": "",
  "created_at": ISODate("2019-09-09T16:11:39.913Z"),
  "updated_at": ISODate("2019-09-12T23:21:31.871Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5d77c2b2f3c6d306896b26bd"),
  "lang": "en",
  "feature": "<p>Variety of interchangable designs. Handle bars can be locked in place up and down the LiteGait depending on patient needs.</p>\r\n",
  "short_description": "sss",
  "created_at": ISODate("2019-09-10T15:35:14.589Z"),
  "updated_at": ISODate("2019-09-10T15:35:14.589Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5d77c2b2f3c6d306896b26bf"),
  "lang": "fr",
  "feature": "",
  "short_description": "",
  "created_at": ISODate("2019-09-10T15:35:14.594Z"),
  "updated_at": ISODate("2019-09-10T15:35:14.594Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5d77c2b2f3c6d306896b26c1"),
  "lang": "Gr",
  "feature": "",
  "short_description": "",
  "created_at": ISODate("2019-09-10T15:35:14.596Z"),
  "updated_at": ISODate("2019-09-10T15:35:14.596Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5d77c2b2f3c6d306896b26c3"),
  "lang": "Sp",
  "feature": "",
  "short_description": "",
  "created_at": ISODate("2019-09-10T15:35:14.598Z"),
  "updated_at": ISODate("2019-09-10T15:35:14.598Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5d79bfcbf3c6d306896b27e9"),
  "lang": "en",
  "feature": "<p>Phone</p>\r\n",
  "short_description": "Phone",
  "created_at": ISODate("2019-09-12T03:47:23.526Z"),
  "updated_at": ISODate("2019-09-12T03:47:23.526Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5d79bfcbf3c6d306896b27eb"),
  "lang": "fr",
  "feature": "",
  "short_description": "",
  "created_at": ISODate("2019-09-12T03:47:23.529Z"),
  "updated_at": ISODate("2019-09-12T03:47:23.529Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5d79bfcbf3c6d306896b27ed"),
  "lang": "Gr",
  "feature": "",
  "short_description": "",
  "created_at": ISODate("2019-09-12T03:47:23.531Z"),
  "updated_at": ISODate("2019-09-12T03:47:23.531Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5d79bfcbf3c6d306896b27ef"),
  "lang": "Sp",
  "feature": "",
  "short_description": "",
  "created_at": ISODate("2019-09-12T03:47:23.533Z"),
  "updated_at": ISODate("2019-09-12T03:47:23.533Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5d7acdf9f3c6d306896b2848"),
  "lang": "en",
  "feature": "",
  "short_description": "Many of  our newer devices incorporate features and improvements that increase functionality and ease of use.",
  "created_at": ISODate("2019-09-12T23:00:09.404Z"),
  "updated_at": ISODate("2019-09-12T23:00:09.404Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5d7acdf9f3c6d306896b284a"),
  "lang": "fr",
  "feature": "",
  "short_description": "",
  "created_at": ISODate("2019-09-12T23:00:09.406Z"),
  "updated_at": ISODate("2019-09-12T23:00:09.406Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5d7acdf9f3c6d306896b284c"),
  "lang": "Gr",
  "feature": "",
  "short_description": "",
  "created_at": ISODate("2019-09-12T23:00:09.409Z"),
  "updated_at": ISODate("2019-09-12T23:00:09.409Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5d7acdf9f3c6d306896b284e"),
  "lang": "Sp",
  "feature": "",
  "short_description": "",
  "created_at": ISODate("2019-09-12T23:00:09.411Z"),
  "updated_at": ISODate("2019-09-12T23:00:09.411Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5d7ad51af3c6d306896b28a9"),
  "lang": "en",
  "feature": "",
  "short_description": "rotate",
  "created_at": ISODate("2019-09-12T23:30:34.765Z"),
  "updated_at": ISODate("2019-09-12T23:30:34.765Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5d7ad51af3c6d306896b28ab"),
  "lang": "fr",
  "feature": "",
  "short_description": "",
  "created_at": ISODate("2019-09-12T23:30:34.767Z"),
  "updated_at": ISODate("2019-09-12T23:30:34.767Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5d7ad51af3c6d306896b28ad"),
  "lang": "Gr",
  "feature": "",
  "short_description": "",
  "created_at": ISODate("2019-09-12T23:30:34.768Z"),
  "updated_at": ISODate("2019-09-12T23:30:34.768Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5d7ad51af3c6d306896b28af"),
  "lang": "Sp",
  "feature": "",
  "short_description": "",
  "created_at": ISODate("2019-09-12T23:30:34.770Z"),
  "updated_at": ISODate("2019-09-12T23:30:34.770Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5d7ad53ff3c6d306896b28b5"),
  "lang": "en",
  "feature": "",
  "short_description": "measure",
  "created_at": ISODate("2019-09-12T23:31:11.485Z"),
  "updated_at": ISODate("2019-09-12T23:31:11.485Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5d7ad53ff3c6d306896b28b7"),
  "lang": "fr",
  "feature": "",
  "short_description": "",
  "created_at": ISODate("2019-09-12T23:31:11.487Z"),
  "updated_at": ISODate("2019-09-12T23:31:11.487Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5d7ad53ff3c6d306896b28b9"),
  "lang": "Gr",
  "feature": "",
  "short_description": "",
  "created_at": ISODate("2019-09-12T23:31:11.489Z"),
  "updated_at": ISODate("2019-09-12T23:31:11.489Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5d7ad53ff3c6d306896b28bb"),
  "lang": "Sp",
  "feature": "",
  "short_description": "",
  "created_at": ISODate("2019-09-12T23:31:11.491Z"),
  "updated_at": ISODate("2019-09-12T23:31:11.491Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5d7adb25f3c6d306896b28ef"),
  "lang": "en",
  "feature": "",
  "short_description": "Mobility Research products that enhance both you and your clients rehabilitation therapy.",
  "created_at": ISODate("2019-09-12T23:56:21.135Z"),
  "updated_at": ISODate("2019-09-12T23:56:21.135Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5d7adb25f3c6d306896b28f1"),
  "lang": "fr",
  "feature": "",
  "short_description": "",
  "created_at": ISODate("2019-09-12T23:56:21.138Z"),
  "updated_at": ISODate("2019-09-12T23:56:21.138Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5d7adb25f3c6d306896b28f3"),
  "lang": "Gr",
  "feature": "",
  "short_description": "",
  "created_at": ISODate("2019-09-12T23:56:21.139Z"),
  "updated_at": ISODate("2019-09-12T23:56:21.139Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5d7adb25f3c6d306896b28f5"),
  "lang": "Sp",
  "feature": "",
  "short_description": "",
  "created_at": ISODate("2019-09-12T23:56:21.141Z"),
  "updated_at": ISODate("2019-09-12T23:56:21.141Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5d7add62f3c6d306896b28fe"),
  "lang": "en",
  "feature": "",
  "short_description": "Creates biomechanically-appropriate posture for the patient",
  "created_at": ISODate("2019-09-13T00:05:54.147Z"),
  "updated_at": ISODate("2019-09-13T00:06:53.567Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5d7add62f3c6d306896b2900"),
  "lang": "fr",
  "feature": "",
  "short_description": "",
  "created_at": ISODate("2019-09-13T00:05:54.150Z"),
  "updated_at": ISODate("2019-09-13T00:06:53.567Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5d7add62f3c6d306896b2902"),
  "lang": "Gr",
  "feature": "",
  "short_description": "",
  "created_at": ISODate("2019-09-13T00:05:54.151Z"),
  "updated_at": ISODate("2019-09-13T00:06:53.567Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5d7add62f3c6d306896b2904"),
  "lang": "Sp",
  "feature": "",
  "short_description": "",
  "created_at": ISODate("2019-09-13T00:05:54.153Z"),
  "updated_at": ISODate("2019-09-13T00:06:53.567Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5d9e34d67dbdaa69d72e5f0a"),
  "lang": "en",
  "feature": "<p>Mini is the best</p>\r\n",
  "short_description": "<p>gkmini</p>\r\n",
  "created_at": ISODate("2019-10-09T19:28:22.3Z"),
  "updated_at": ISODate("2019-10-09T19:28:22.3Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5d9e34d67dbdaa69d72e5f0c"),
  "lang": "fr",
  "feature": "",
  "short_description": "",
  "created_at": ISODate("2019-10-09T19:28:22.13Z"),
  "updated_at": ISODate("2019-10-09T19:28:22.13Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5d9e34d67dbdaa69d72e5f0e"),
  "lang": "Gr",
  "feature": "",
  "short_description": "",
  "created_at": ISODate("2019-10-09T19:28:22.18Z"),
  "updated_at": ISODate("2019-10-09T19:28:22.18Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5d9e34d67dbdaa69d72e5f10"),
  "lang": "Sp",
  "feature": "",
  "short_description": "",
  "created_at": ISODate("2019-10-09T19:28:22.21Z"),
  "updated_at": ISODate("2019-10-09T19:28:22.21Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5d9e36877dbdaa69d72e5f21"),
  "lang": "en",
  "feature": "<p>GK mini is the best</p>\r\n",
  "short_description": "<p>GKmini</p>\r\n",
  "created_at": ISODate("2019-10-09T19:35:35.535Z"),
  "updated_at": ISODate("2019-10-09T19:35:35.535Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5d9e36877dbdaa69d72e5f23"),
  "lang": "fr",
  "feature": "",
  "short_description": "",
  "created_at": ISODate("2019-10-09T19:35:35.544Z"),
  "updated_at": ISODate("2019-10-09T19:35:35.544Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5d9e36877dbdaa69d72e5f25"),
  "lang": "Gr",
  "feature": "",
  "short_description": "",
  "created_at": ISODate("2019-10-09T19:35:35.550Z"),
  "updated_at": ISODate("2019-10-09T19:35:35.550Z")
});
db.getCollection("category_contents").insert({
  "_id": ObjectId("5d9e36877dbdaa69d72e5f27"),
  "lang": "Sp",
  "feature": "",
  "short_description": "",
  "created_at": ISODate("2019-10-09T19:35:35.557Z"),
  "updated_at": ISODate("2019-10-09T19:35:35.557Z")
});

/** coupons records **/
db.getCollection("coupons").insert({
  "_id": ObjectId("5d8e61bd4d2cd025ee1831a8"),
  "applied_on": [
    "product"
  ],
  "title": "CrawlAhead",
  "code": "CAP-10",
  "type": "percent",
  "amount": null,
  "percent": NumberInt(10),
  "min": null,
  "max": null,
  "expire": ISODate("2019-12-31T18:30:00.0Z"),
  "single_use": NumberInt(0),
  "users": null,
  "status": NumberInt(1),
  "created_at": ISODate("2019-09-27T19:23:41.974Z"),
  "updated_at": ISODate("2019-11-20T10:12:16.555Z"),
  "category": null
});
db.getCollection("coupons").insert({
  "_id": ObjectId("5d94f510df71be4b6137f2c8"),
  "applied_on": [
    "parts"
  ],
  "title": "montest",
  "code": "montest1",
  "type": "percent",
  "amount": NumberInt(5),
  "percent": NumberInt(5),
  "min": null,
  "max": null,
  "expire": ISODate("2020-02-13T18:30:00.0Z"),
  "single_use": NumberInt(0),
  "users": null,
  "status": NumberInt(1),
  "created_at": ISODate("2019-10-02T19:05:52.478Z"),
  "updated_at": ISODate("2019-12-02T16:03:39.333Z"),
  "category": null
});
db.getCollection("coupons").insert({
  "_id": ObjectId("5dd511aded5cd71a196d0f44"),
  "applied_on": [
    "parts"
  ],
  "title": "PART10",
  "code": "PART10",
  "type": "percent",
  "amount": NumberInt(10),
  "percent": NumberInt(10),
  "min": null,
  "max": null,
  "expire": ISODate("2020-03-30T18:30:00.0Z"),
  "single_use": NumberInt(0),
  "status": NumberInt(1),
  "created_at": ISODate("2019-11-20T10:13:01.858Z"),
  "updated_at": ISODate("2019-11-27T16:13:28.887Z"),
  "category": null,
  "users": null
});

/** distributors records **/
db.getCollection("distributors").insert({
  "_id": ObjectId("5da070ee322b13156dc77811"),
  "region": "NA",
  "name": "Doug Reed",
  "address": "P.O.Box 432\r\nAmerica",
  "national": NumberInt(1),
  "certified": NumberInt(1),
  "high_level_product": NumberInt(1),
  "status": NumberInt(1),
  "created_at": ISODate("2019-10-11T12:09:18.861Z"),
  "updated_at": ISODate("2019-10-11T12:09:18.861Z")
});
db.getCollection("distributors").insert({
  "_id": ObjectId("5da070f7322b13156dc77818"),
  "region": "NA",
  "name": "RAJ VERMA",
  "address": "WARD NO 17,\r\nNear MPS school",
  "national": NumberInt(1),
  "status": NumberInt(1),
  "created_at": ISODate("2019-10-11T12:09:27.228Z"),
  "updated_at": ISODate("2019-10-11T12:09:27.228Z")
});
db.getCollection("distributors").insert({
  "_id": ObjectId("5da070ff322b13156dc7781f"),
  "region": "uk",
  "name": "RAJ KISHORE VERMA",
  "address": "WARD NO 17,",
  "certified": NumberInt(1),
  "high_level_product": NumberInt(1),
  "status": NumberInt(1),
  "created_at": ISODate("2019-10-11T12:09:35.138Z"),
  "updated_at": ISODate("2019-10-11T12:09:35.138Z")
});

/** elp_payment records **/
db.getCollection("elp_payment").insert({
  "_id": ObjectId("5cb05f1f2a31271b4165f361"),
  "elp_id": ObjectId("5cb05e002a31271b4165f357"),
  "user_id": ObjectId("5badf3695115941ea80970ce"),
  "amount": NumberInt(2),
  "payment_method": "paypal",
  "payment_id": "PAYID-LSYF6CQ54059170DB972225K",
  "status": NumberInt(1),
  "created_at": ISODate("2019-04-12T09:49:19.148Z"),
  "updated_at": ISODate("2019-04-12T09:49:19.148Z")
});
db.getCollection("elp_payment").insert({
  "_id": ObjectId("5d2733dacfcece7c92e90423"),
  "coupon_used": NumberInt(0),
  "elp_id": ObjectId("5cb05e0e2a31271b4165f35b"),
  "user_id": ObjectId("5d273333017cd5612df4449f"),
  "amount": NumberInt(5),
  "payment_method": "paypal",
  "payment_id": "PAYID-LUTTHPI9DJ043478S173082A",
  "status": NumberInt(1),
  "created_at": ISODate("2019-07-11T13:04:26.102Z"),
  "updated_at": ISODate("2019-07-11T13:04:26.102Z")
});
db.getCollection("elp_payment").insert({
  "_id": ObjectId("5d368dd781421734b162fba4"),
  "coupon_used": NumberInt(3),
  "elp_id": ObjectId("5cb05e0e2a31271b4165f35b"),
  "user_id": ObjectId("5badf3695115941ea80970ce"),
  "amount": NumberInt(5),
  "payment_method": "paypal",
  "payment_id": "PAYID-LU3I3NQ7DJ055106D936114P",
  "status": NumberInt(1),
  "created_at": ISODate("2019-07-23T04:32:23.151Z"),
  "updated_at": ISODate("2019-09-27T05:16:20.743Z")
});
db.getCollection("elp_payment").insert({
  "_id": ObjectId("5d3991a5667cf16bdd7a0161"),
  "coupon_used": NumberInt(1),
  "elp_id": ObjectId("5cb05e0e2a31271b4165f35b"),
  "user_id": ObjectId("5d3971dc2a02f4259583724a"),
  "amount": NumberInt(5),
  "payment_method": "paypal",
  "payment_id": "PAYID-LU4ZDEA1YH289405N702633W",
  "facility_name": "my test facility",
  "status": NumberInt(1),
  "created_at": ISODate("2019-07-25T11:25:25.970Z"),
  "updated_at": ISODate("2019-07-25T11:59:47.291Z")
});

/** elps records **/
db.getCollection("elps").insert({
  "_id": ObjectId("5cb05e002a31271b4165f357"),
  "name": "Basic Plan",
  "display_name": "$2/Year",
  "display_number": NumberInt(1),
  "price": NumberInt(2),
  "duration": NumberInt(12),
  "status": NumberInt(1),
  "created_at": ISODate("2019-04-12T09:44:32.223Z"),
  "updated_at": ISODate("2019-07-23T04:31:28.112Z"),
  "display_sub_name": "Single Person",
  "persons": NumberInt(1)
});
db.getCollection("elps").insert({
  "_id": ObjectId("5cb05e0e2a31271b4165f35b"),
  "name": "Gold Plan",
  "display_name": "$5/Year",
  "display_number": NumberInt(2),
  "price": NumberInt(5),
  "duration": NumberInt(12),
  "status": NumberInt(1),
  "created_at": ISODate("2019-04-12T09:44:46.254Z"),
  "updated_at": ISODate("2019-08-05T05:02:34.191Z"),
  "persons": NumberInt(12),
  "display_sub_name": "Group up to 12 clinicians"
});

/** emails records **/
db.getCollection("emails").insert({
  "_id": ObjectId("5d44178bbe9c79245c687857"),
  "slug": "forget_password",
  "subject": "Forget Password Request on Litegait",
  "body": "<p>Hi {{name}},</p>\r\n\r\n<p>Please click this link to reset your password on LiteGait -&nbsp;&nbsp;<a href=\"\\\">Reset Password</a></p>\r\n\r\n<p>Or copy this link in the browser address bar -&nbsp;{{reset_pwd_link}}</p>\r\n\r\n<p>As always, if you have any questions or concerns please feel free to email us at {{admin_mail}} or call 1-800-920-9527. We&rsquo;d love to hear from you!<br />\r\n<br />\r\nTruly,&nbsp;<br />\r\nYour Customer Care Team<br />\r\n{{site_name}}</p>\r\n",
  "created_at": ISODate("2019-08-02T10:59:23.757Z"),
  "updated_at": ISODate("2019-08-02T10:59:23.757Z")
});
db.getCollection("emails").insert({
  "_id": ObjectId("5d4417cbbe9c79245c68785d"),
  "slug": "user_register",
  "subject": "Registration Successfull on litegait",
  "body": "<p>Hi {{name}},</p>\r\n\r\n<p>Thank you for registering and account on LiteGait.</p>\r\n\r\n<p>You can login to your account by visiting {{site_url}}/login.<br />\r\nAs always, if you have any questions or concerns please feel free to email us at {{admin_mail}} or call 1-800-920-9527. We&rsquo;d love to hear from you!</p>\r\n\r\n<p>Truly,&nbsp;<br />\r\nYour Customer Care Team<br />\r\n{{site_name}}</p>\r\n",
  "created_at": ISODate("2019-08-02T11:00:27.557Z"),
  "updated_at": ISODate("2019-08-02T11:00:27.557Z")
});
db.getCollection("emails").insert({
  "_id": ObjectId("5d44180bbe9c79245c687861"),
  "slug": "order_success",
  "subject": "Litegait Order Receipt",
  "body": "<p>Hi {{name}}</p>\r\n\r\n<p>Thank you for order on {{site_name}}. Here&#39;s a summary of your order.</p>\r\n\r\n<p>{{details}}<br />\r\n<br />\r\nWe Will send an email when your order is ready for pickup.</p>\r\n\r\n<p>Regards<br />\r\n{{site_name}}</p>\r\n\r\n<p>If you have any concern please feel free to contact at {{admin_mail}}.</p>\r\n",
  "created_at": ISODate("2019-08-02T11:01:31.152Z"),
  "updated_at": ISODate("2019-08-02T11:01:31.152Z")
});
db.getCollection("emails").insert({
  "_id": ObjectId("5d441846be9c79245c687865"),
  "slug": "webinar_purchase",
  "subject": "Litegait Webinar Purchase Receipt",
  "body": "<p>Hi {{name}}<br />\r\n<br />\r\nThank you for order on {{site_name}}. Here&#39;s a summary of your order.</p>\r\n\r\n<p>{{details}}<br />\r\n<br />\r\nWe Will send an email when your order is ready for pickup.</p>\r\n\r\n<p>Regards<br />\r\n{{site_name}}</p>\r\n\r\n<p>If you have any concern please feel free to contact at {{admin_mail}}.</p>\r\n",
  "created_at": ISODate("2019-08-02T11:02:30.978Z"),
  "updated_at": ISODate("2019-08-02T11:02:30.978Z")
});
db.getCollection("emails").insert({
  "_id": ObjectId("5d441886be9c79245c687869"),
  "slug": "contact_us",
  "subject": "Contact Us Request on Litegait",
  "body": "<p>Hi Admin,</p>\r\n\r\n<p>You got new Contact us message. Please check below details:</p>\r\n\r\n<p>First Name: {{first_name}}<br />\r\nLast Name: {{last_name}}<br />\r\nEmail: {{email}}<br />\r\nAddress: {{address}}<br />\r\nMessage: {{message}}</p>\r\n\r\n<p>Truly,&nbsp;<br />\r\nYour Customer Care Team<br />\r\n{{site_name}}</p>\r\n",
  "created_at": ISODate("2019-08-02T11:03:34.411Z"),
  "updated_at": ISODate("2019-08-02T11:03:34.411Z")
});

/** faq_contents records **/
db.getCollection("faq_contents").insert({
  "_id": ObjectId("5c36efcff315ad492513976e"),
  "faq_id": ObjectId("5c36efcff315ad492513976d"),
  "lang": "en",
  "question": "What is litegait",
  "answer": "<p><strong>LiteGait</strong>&reg; is a gait training device that simultaneously controls weight bearing, posture, and balance over a treadmill or over ground. It creates an ideal environment for treating patients with a wide range of impairments and functional levels.</p>\r\n",
  "created_at": ISODate("2019-01-10T07:10:07.977Z"),
  "updated_at": ISODate("2019-06-07T10:16:02.912Z")
});
db.getCollection("faq_contents").insert({
  "_id": ObjectId("5c36efd0f315ad4925139770"),
  "faq_id": ObjectId("5c36efcff315ad492513976d"),
  "lang": "fr",
  "question": "",
  "answer": "",
  "created_at": ISODate("2019-01-10T07:10:08.192Z"),
  "updated_at": ISODate("2019-06-07T10:16:02.912Z")
});
db.getCollection("faq_contents").insert({
  "_id": ObjectId("5c36efd0f315ad4925139772"),
  "faq_id": ObjectId("5c36efcff315ad492513976d"),
  "lang": "Gr",
  "question": "",
  "answer": "",
  "created_at": ISODate("2019-01-10T07:10:08.201Z"),
  "updated_at": ISODate("2019-06-07T10:16:02.912Z")
});
db.getCollection("faq_contents").insert({
  "_id": ObjectId("5c36efd0f315ad4925139774"),
  "faq_id": ObjectId("5c36efcff315ad492513976d"),
  "lang": "Sp",
  "question": "",
  "answer": "",
  "created_at": ISODate("2019-01-10T07:10:08.208Z"),
  "updated_at": ISODate("2019-06-07T10:16:02.912Z")
});
db.getCollection("faq_contents").insert({
  "_id": ObjectId("5c36efdbf315ad492513977a"),
  "faq_id": ObjectId("5c36efdbf315ad4925139779"),
  "lang": "en",
  "question": "How liteGait works",
  "answer": "<p>Our stroke rehabilitation program has earned Disease-Specific Care Certification from The Joint Commission.&nbsp;Our stroke rehabilitation program has earned Disease-Specific Care Certification from The Joint Commission.</p>\r\n",
  "created_at": ISODate("2019-01-10T07:10:19.42Z"),
  "updated_at": ISODate("2019-06-07T10:15:54.818Z")
});
db.getCollection("faq_contents").insert({
  "_id": ObjectId("5c36efdbf315ad492513977c"),
  "faq_id": ObjectId("5c36efdbf315ad4925139779"),
  "lang": "fr",
  "question": "",
  "answer": "",
  "created_at": ISODate("2019-01-10T07:10:19.45Z"),
  "updated_at": ISODate("2019-06-07T10:15:54.818Z")
});
db.getCollection("faq_contents").insert({
  "_id": ObjectId("5c36efdbf315ad492513977e"),
  "faq_id": ObjectId("5c36efdbf315ad4925139779"),
  "lang": "Gr",
  "question": "",
  "answer": "",
  "created_at": ISODate("2019-01-10T07:10:19.47Z"),
  "updated_at": ISODate("2019-06-07T10:15:54.818Z")
});
db.getCollection("faq_contents").insert({
  "_id": ObjectId("5c36efdbf315ad4925139780"),
  "faq_id": ObjectId("5c36efdbf315ad4925139779"),
  "lang": "Sp",
  "question": "",
  "answer": "",
  "created_at": ISODate("2019-01-10T07:10:19.49Z"),
  "updated_at": ISODate("2019-06-07T10:15:54.818Z")
});
db.getCollection("faq_contents").insert({
  "_id": ObjectId("5c3709c6f315ad4925139820"),
  "faq_id": ObjectId("5c3709c6f315ad492513981f"),
  "lang": "en",
  "question": "How much year of experience of litegait have in this field?",
  "answer": "<p>Litegait have more then <span style=\"color:#2ecc71\"><span style=\"font-size:22px\"><u><em><strong>25</strong></em></u></span></span> year of experience in this field.</p>\r\n",
  "created_at": ISODate("2019-01-10T09:00:54.138Z"),
  "updated_at": ISODate("2019-06-07T10:16:40.182Z")
});
db.getCollection("faq_contents").insert({
  "_id": ObjectId("5c3709c6f315ad4925139822"),
  "faq_id": ObjectId("5c3709c6f315ad492513981f"),
  "lang": "fr",
  "question": "",
  "answer": "",
  "created_at": ISODate("2019-01-10T09:00:54.141Z"),
  "updated_at": ISODate("2019-06-07T10:16:40.182Z")
});
db.getCollection("faq_contents").insert({
  "_id": ObjectId("5c3709c6f315ad4925139824"),
  "faq_id": ObjectId("5c3709c6f315ad492513981f"),
  "lang": "Gr",
  "question": "",
  "answer": "",
  "created_at": ISODate("2019-01-10T09:00:54.142Z"),
  "updated_at": ISODate("2019-06-07T10:16:40.182Z")
});
db.getCollection("faq_contents").insert({
  "_id": ObjectId("5c3709c6f315ad4925139826"),
  "faq_id": ObjectId("5c3709c6f315ad492513981f"),
  "lang": "Sp",
  "question": "",
  "answer": "",
  "created_at": ISODate("2019-01-10T09:00:54.144Z"),
  "updated_at": ISODate("2019-06-07T10:16:40.183Z")
});
db.getCollection("faq_contents").insert({
  "_id": ObjectId("5d09018660b1b8748527eedb"),
  "faq_id": ObjectId("5d09018660b1b8748527eeda"),
  "lang": "en",
  "question": "How to Submit a PO",
  "answer": "<p>You can submit a PO a few ways</p>\r\n\r\n<p>Upload hear</p>\r\n\r\n<p>Fax to xxx.xxx.xxx</p>\r\n\r\n<p>email at litegaitpo..</p>\r\n",
  "created_at": ISODate("2019-06-18T15:21:42.790Z"),
  "updated_at": ISODate("2019-07-29T10:47:37.466Z")
});
db.getCollection("faq_contents").insert({
  "_id": ObjectId("5d09018660b1b8748527eedd"),
  "faq_id": ObjectId("5d09018660b1b8748527eeda"),
  "lang": "fr",
  "question": "",
  "answer": "",
  "created_at": ISODate("2019-06-18T15:21:42.792Z"),
  "updated_at": ISODate("2019-07-29T10:47:37.466Z")
});
db.getCollection("faq_contents").insert({
  "_id": ObjectId("5d09018660b1b8748527eedf"),
  "faq_id": ObjectId("5d09018660b1b8748527eeda"),
  "lang": "Gr",
  "question": "",
  "answer": "",
  "created_at": ISODate("2019-06-18T15:21:42.794Z"),
  "updated_at": ISODate("2019-07-29T10:47:37.467Z")
});
db.getCollection("faq_contents").insert({
  "_id": ObjectId("5d09018660b1b8748527eee1"),
  "faq_id": ObjectId("5d09018660b1b8748527eeda"),
  "lang": "Sp",
  "question": "",
  "answer": "",
  "created_at": ISODate("2019-06-18T15:21:42.795Z"),
  "updated_at": ISODate("2019-07-29T10:47:37.467Z")
});

/** faqs records **/
db.getCollection("faqs").insert({
  "_id": ObjectId("5c36efcff315ad492513976d"),
  "lang_content": [
    ObjectId("5c36efcff315ad492513976e"),
    ObjectId("5c36efd0f315ad4925139770"),
    ObjectId("5c36efd0f315ad4925139772"),
    ObjectId("5c36efd0f315ad4925139774")
  ],
  "question": "What is litegait",
  "answer": "<p><strong>LiteGait</strong>&reg; is a gait training device that simultaneously controls weight bearing, posture, and balance over a treadmill or over ground. It creates an ideal environment for treating patients with a wide range of impairments and functional levels.</p>\r\n",
  "status": NumberInt(1),
  "created_at": ISODate("2019-01-10T07:10:07.973Z"),
  "updated_at": ISODate("2019-06-07T10:16:02.909Z"),
  "category": "5cfa390860b1b8748527eda0,5cfa391d60b1b8748527edac"
});
db.getCollection("faqs").insert({
  "_id": ObjectId("5c36efdbf315ad4925139779"),
  "lang_content": [
    ObjectId("5c36efdbf315ad492513977a"),
    ObjectId("5c36efdbf315ad492513977c"),
    ObjectId("5c36efdbf315ad492513977e"),
    ObjectId("5c36efdbf315ad4925139780")
  ],
  "question": "How liteGait works",
  "answer": "<p>Our stroke rehabilitation program has earned Disease-Specific Care Certification from The Joint Commission.&nbsp;Our stroke rehabilitation program has earned Disease-Specific Care Certification from The Joint Commission.</p>\r\n",
  "status": NumberInt(1),
  "created_at": ISODate("2019-01-10T07:10:19.41Z"),
  "updated_at": ISODate("2019-06-07T10:15:54.814Z"),
  "category": "5cfa390860b1b8748527eda0"
});
db.getCollection("faqs").insert({
  "_id": ObjectId("5c3709c6f315ad492513981f"),
  "lang_content": [
    ObjectId("5c3709c6f315ad4925139820"),
    ObjectId("5c3709c6f315ad4925139822"),
    ObjectId("5c3709c6f315ad4925139824"),
    ObjectId("5c3709c6f315ad4925139826")
  ],
  "question": "How much year of experience of litegait have in this field?",
  "answer": "<p>Litegait have more then <span style=\"color:#2ecc71\"><span style=\"font-size:22px\"><u><em><strong>25</strong></em></u></span></span> year of experience in this field.</p>\r\n",
  "status": NumberInt(1),
  "created_at": ISODate("2019-01-10T09:00:54.137Z"),
  "updated_at": ISODate("2019-06-07T10:16:40.179Z"),
  "category": "5cfa393b60b1b8748527edb9,5cfa390860b1b8748527eda0,5cfa391d60b1b8748527edac"
});
db.getCollection("faqs").insert({
  "_id": ObjectId("5d09018660b1b8748527eeda"),
  "lang_content": [
    ObjectId("5d09018660b1b8748527eedb"),
    ObjectId("5d09018660b1b8748527eedd"),
    ObjectId("5d09018660b1b8748527eedf"),
    ObjectId("5d09018660b1b8748527eee1")
  ],
  "category": "5c78f2e88592290d03e1ff90",
  "question": "How to Submit a PO",
  "answer": "<p>You can submit a PO a few ways</p>\r\n\r\n<p>Upload hear</p>\r\n\r\n<p>Fax to xxx.xxx.xxx</p>\r\n\r\n<p>email at litegaitpo..</p>\r\n",
  "status": NumberInt(1),
  "created_at": ISODate("2019-06-18T15:21:42.788Z"),
  "updated_at": ISODate("2019-07-29T10:47:37.463Z")
});

/** forms records **/

/** galleries records **/
db.getCollection("galleries").insert({
  "_id": ObjectId("5c36f0fdf315ad49251397ad"),
  "lang_content": [
    ObjectId("5c36f0fdf315ad49251397ae"),
    ObjectId("5c36f0fdf315ad49251397b0"),
    ObjectId("5c36f0fdf315ad49251397b2"),
    ObjectId("5c36f0fdf315ad49251397b4")
  ],
  "title": "Accessary one",
  "type": "photo",
  "file": "/accessories_img1.jpg",
  "vimeo_url": "",
  "tag": [
    "LG400"
  ],
  "description": "<p>The Model S and X are Tesla&rsquo;s second and third major lines of electric vehicles, and are higher-priced lines compared to its newest Model 3. The 75 kWh version of the Model S and X that are being retired come with a battery that garners a range of 259 miles for the Model S, and a 237 miles for the Model X.</p>\r\n\r\n<p>Higher-end versions of the S and X, equipped with a 100 kWh battery, will still be made and sold by Tesla, but at a higher price. Following the changes announced by Musk, the starting price for a new Model S will be $94,000 and the starting price for a Model X will be $97,000 from next week.</p>\r\n\r\n<p>Tesla did not respond to inquiries as to why the S and X offerings were pared back, or whether any team reviewed Musk&rsquo;s tweet before he shared it.</p>\r\n\r\n<p>Retiring the lower-range Model S and X vehicles may push prospective customers to buy Tesla&rsquo;s high-end Model 3 sedans instead.</p>\r\n\r\n<p><a href=\"https://www.cnbc.com/2018/05/19/elon-musk-teases-specifications-for-teslas-model-3-calling-it-amazing.html\" target=\"\">A Model 3 with bells and whistles</a>&nbsp;&mdash; like dual-motor all wheel drive, enhanced Autopilot, and a $5,000 performance upgrade package &mdash; will cost around $67,000, according to the company&rsquo;s Model 3 configurator online.</p>\r\n\r\n<p>Reducing the number of variations of Model S and X vehicles that Tesla has to make may help the company control costs, or reallocate materials and labor to other initiatives, such as Model 3 production at its packed car plant in Fremont, California or spare parts production in nearby Lathrop.</p>\r\n",
  "status": NumberInt(1),
  "created_at": ISODate("2019-01-10T07:15:09.438Z"),
  "updated_at": ISODate("2019-08-06T04:21:24.894Z"),
  "position": NumberInt(2)
});
db.getCollection("galleries").insert({
  "_id": ObjectId("5c36f148f315ad49251397ba"),
  "lang_content": [
    ObjectId("5c36f148f315ad49251397bb"),
    ObjectId("5c36f148f315ad49251397bd"),
    ObjectId("5c36f148f315ad49251397bf"),
    ObjectId("5c36f148f315ad49251397c1")
  ],
  "title": "Billionaire Democrat Tom Steyer",
  "type": "photo",
  "file": "/product_img4.jpg",
  "vimeo_url": "",
  "tag": [
    "LG300"
  ],
  "description": "<p>Democrat Tom Steyer, a billionaire who has been pushing Congress to impeach President&nbsp;<a href=\"https://www.cnbc.com/donald-trump/\" target=\"\">Donald Trump</a>, will not run for president in the 2020 campaign.</p>\r\n\r\n<p>CNBC confirmed with a Steyer representative that he wouldn&rsquo;t run after&nbsp;<a href=\"https://www.nytimes.com/2019/01/09/us/politics/tom-steyer-trump-2020.html\" target=\"\">The New York Times</a>&nbsp;first reported the development. The Times obtained prepared remarks from a speech Steyer was scheduled to give late Wednesday afternoon.</p>\r\n\r\n<p>Steyer&rsquo;s decision came as other potential Democratic candidates considered whether to run. A flood of announcements is expected in the coming weeks.</p>\r\n\r\n<p>Sen.&nbsp;<a href=\"https://www.cnbc.com/elizabeth-warren/\" target=\"\">Elizabeth Warren</a>, D-Mass., has already announced that she formed an exploratory committee, which is a big step toward declaring a full candidacy. Other top Democrats, such as former Vice President&nbsp;<a href=\"https://www.cnbc.com/joe-biden/\" target=\"\">Joe Biden</a>, and Sen.&nbsp;<a href=\"https://www.cnbc.com/bernie-sanders/\" target=\"\">Bernie Sanders</a>, I-Vt., are also considering whether to run in what could be a large field. The first contests of the presidential primary season start a year from now.</p>\r\n\r\n<p>Steyer, of California, was one of the top donors during the 2018 congressional midterm elections.</p>\r\n",
  "status": NumberInt(1),
  "created_at": ISODate("2019-01-10T07:16:24.744Z"),
  "updated_at": ISODate("2019-08-06T04:21:30.203Z"),
  "position": NumberInt(3)
});
db.getCollection("galleries").insert({
  "_id": ObjectId("5c36f208f315ad49251397c7"),
  "lang_content": [
    ObjectId("5c36f208f315ad49251397c8"),
    ObjectId("5c36f208f315ad49251397ca"),
    ObjectId("5c36f208f315ad49251397cc"),
    ObjectId("5c36f208f315ad49251397ce")
  ],
  "title": "THE BIBLE: The Sacrifice of Isaac",
  "type": "video",
  "file": "/movie.mp4",
  "vimeo_url": "",
  "tag": "The Bibile, vimeo, sacrifice, walker, freedome, BiSym, ",
  "description": "<p>Written &amp; Directed by Dean Fleischer-Camp<br />\r\nProduced by Riel Roch Decter &amp; Sebastian Pardo</p>\r\n\r\n<p>CAST:<br />\r\nAbraham- Ben Sinclair<br />\r\nSarah- Veronica Osorio<br />\r\nIsaac- Ronin Thompson<br />\r\nVillager- Myles Cranford&nbsp;<br />\r\nFarmer- Kat Purgal<br />\r\nBrother- Noel Arthur<br />\r\nServant 1- Nelson Cheng<br />\r\nServant 2- William Walton<br />\r\nAngel- Brandi Austin</p>\r\n\r\n<p>CAMERA<br />\r\nDP- James Wall<br />\r\n1st AC- Travis Daking<br />\r\n2nd AC- Jeanna Kim<br />\r\nDIT- Timothy Gaer</p>\r\n\r\n<p>SOUND<br />\r\nSound Mixer- Blake Christian<br />\r\nBoom Operator</p>\r\n\r\n<p>ELECTRIC<br />\r\nGaffer- Robert Oliva<br />\r\nBest Boy- Matthew Martinez</p>\r\n\r\n<p>GRIP<br />\r\nKey Grip- Danny Barfield<br />\r\nBB Grip- Travis Huston<br />\r\nGrip- Brian Dumois<br />\r\nGrip- Robert Richards<br />\r\nGrip- Gabriel Borquez<br />\r\nGrip- Josh Sides<br />\r\nGrip Driver- Dave Blackman</p>\r\n\r\n<p>PRODUCTION<br />\r\nScript Supervisor- Vaughn Greve<br />\r\nFirst AD- Bryan Goeres<br />\r\nSecond AD- Mike Tsucalas<br />\r\nLine Producer- Nick Batchelder<br />\r\nProduction Supervisor- Maria Felix Oviedo<br />\r\nOffice PA- Will Crowther</p>\r\n\r\n<p>MAKE-UP<br />\r\nKey HMU Artist- Marina Coria<br />\r\nHMU Assistant- Emily Mefford</p>\r\n\r\n<p>WARDROBE<br />\r\nCostume Designer- Natasha Noorvash&nbsp;<br />\r\nAssistant Costume Designer- Amanda Lee</p>\r\n\r\n<p>ART<br />\r\nProduction Designer- Almitra Corey<br />\r\nProp Master- Elana Farley<br />\r\nLeadman- Davey Cooperwasser</p>\r\n\r\n<p>CASTING&nbsp;<br />\r\nDoyle/Fiorilli Casting</p>\r\n\r\n<p>POST<br />\r\nPost Supervisor- Grant Keiner<br />\r\nPost Coordinator- Karen Rodriguez<br />\r\nEditing- Aaron Beckum &amp; Dean Fleischer-Camp<br />\r\nSound Mixing and Design- Brent Kiser, Unbridled Sound<br />\r\nVFX- Eric Overton</p>\r\n\r\n<p>ORIGINAL SCORE by Will Wiesenfeld</p>\r\n\r\n<p>TITLE DESIGN by Teddy Blanks</p>\r\n\r\n<p>FOR SUPER DELUXE:<br />\r\nSVP Originals- Winnie Kemp<br />\r\nHead of Prod- Josef Lieck<br />\r\nEP, Web Series- Mia Di Pasquale&nbsp;<br />\r\nSD Line Producer- Scott Keiner</p>\r\n",
  "status": NumberInt(1),
  "created_at": ISODate("2019-01-10T07:19:36.551Z"),
  "updated_at": ISODate("2019-06-21T09:17:08.38Z")
});
db.getCollection("galleries").insert({
  "_id": ObjectId("5cb7b87d2a31271b4165f3ff"),
  "lang_content": [
    ObjectId("5cb7b87d2a31271b4165f400"),
    ObjectId("5cb7b87d2a31271b4165f402"),
    ObjectId("5cb7b87d2a31271b4165f404"),
    ObjectId("5cb7b87d2a31271b4165f406")
  ],
  "title": "LiteGait Overgroun",
  "type": "photo",
  "file": "/gallery11.jpg",
  "vimeo_url": "",
  "tag": [
    "LG400"
  ],
  "description": "<p>this may be a dog but he is overground</p>\r\n",
  "status": NumberInt(1),
  "created_at": ISODate("2019-04-17T23:36:29.894Z"),
  "updated_at": ISODate("2019-08-06T04:21:34.629Z"),
  "position": null
});
db.getCollection("galleries").insert({
  "_id": ObjectId("5cefff25fb42690c45d4418a"),
  "lang_content": [
    ObjectId("5cefff25fb42690c45d4418b"),
    ObjectId("5cefff25fb42690c45d4418d"),
    ObjectId("5cefff25fb42690c45d4418f"),
    ObjectId("5cefff25fb42690c45d44191")
  ],
  "title": "LiteGait with GaitSens",
  "type": "photo",
  "file": "/product_img6.jpg",
  "vimeo_url": "",
  "tag": [
    "LG400",
    "LG300"
  ],
  "description": "<p>clinical gait assessment tools and reports at your fingertips.</p>\r\n",
  "status": NumberInt(1),
  "created_at": ISODate("2019-05-30T16:04:53.6Z"),
  "updated_at": ISODate("2019-08-06T04:23:42.858Z"),
  "position": NumberInt(1)
});
db.getCollection("galleries").insert({
  "_id": ObjectId("5d0ca0af0a2c2e561c735077"),
  "lang_content": [
    ObjectId("5d0ca0af0a2c2e561c735078"),
    ObjectId("5d0ca0af0a2c2e561c73507a"),
    ObjectId("5d0ca0af0a2c2e561c73507c"),
    ObjectId("5d0ca0af0a2c2e561c73507e")
  ],
  "title": "Test Video",
  "type": "vimeo_video",
  "file": "",
  "vimeo_url": "https://vimeo.com/352357004",
  "tag": [
    "Q-pad"
  ],
  "description": "<p>Test VideoTest VideoTest VideoTest VideoTest VideoTest Video</p>\r\n",
  "status": NumberInt(1),
  "created_at": ISODate("2019-06-21T09:17:35.256Z"),
  "updated_at": ISODate("2019-10-01T15:50:00.571Z"),
  "position": null
});
db.getCollection("galleries").insert({
  "_id": ObjectId("5d7b577fe696962cf1b33aad"),
  "tag": [
    "LiteGait Overground"
  ],
  "lang_content": [
    ObjectId("5d7b577fe696962cf1b33aae"),
    ObjectId("5d7b577fe696962cf1b33ab0"),
    ObjectId("5d7b577fe696962cf1b33ab2"),
    ObjectId("5d7b577fe696962cf1b33ab4")
  ],
  "title": "About US on Vimeo",
  "position": null,
  "type": "vimeo_video",
  "file": "",
  "vimeo_url": "https://vimeo.com/308795729",
  "description": "<p>About US on Vimeo</p>\r\n",
  "status": NumberInt(1),
  "created_at": ISODate("2019-09-13T08:46:55.480Z"),
  "updated_at": ISODate("2019-09-13T08:46:55.493Z")
});
db.getCollection("galleries").insert({
  "_id": ObjectId("5d81b43de696962cf1b33b50"),
  "tag": [
    "Service Check"
  ],
  "lang_content": [
    ObjectId("5d81b43de696962cf1b33b51"),
    ObjectId("5d81b43de696962cf1b33b53"),
    ObjectId("5d81b43de696962cf1b33b55"),
    ObjectId("5d81b43de696962cf1b33b57")
  ],
  "title": "New device service check",
  "position": NumberInt(1),
  "type": "vimeo_video",
  "file": "",
  "vimeo_url": "https://vimeo.com/257937020",
  "description": "<p>&nbsp;Once you have received and assembled your LiteGait we ask that you complete a new equipment service check. Please review this video while near the equipment. Physically check each assembly to ensure for proper functionality. The completion of this check helps to ensure the equipment is assembled and functioning properly, that you have received all components and are ready to begin use of the LiteGait.</p>\r\n",
  "status": NumberInt(1),
  "created_at": ISODate("2019-09-18T04:36:13.670Z"),
  "updated_at": ISODate("2019-09-18T04:38:11.63Z")
});

/** gallery_contents records **/
db.getCollection("gallery_contents").insert({
  "_id": ObjectId("5c36f0fdf315ad49251397ae"),
  "gallery_id": ObjectId("5c36f0fdf315ad49251397ad"),
  "description": "<p>The Model S and X are Tesla&rsquo;s second and third major lines of electric vehicles, and are higher-priced lines compared to its newest Model 3. The 75 kWh version of the Model S and X that are being retired come with a battery that garners a range of 259 miles for the Model S, and a 237 miles for the Model X.</p>\r\n\r\n<p>Higher-end versions of the S and X, equipped with a 100 kWh battery, will still be made and sold by Tesla, but at a higher price. Following the changes announced by Musk, the starting price for a new Model S will be $94,000 and the starting price for a Model X will be $97,000 from next week.</p>\r\n\r\n<p>Tesla did not respond to inquiries as to why the S and X offerings were pared back, or whether any team reviewed Musk&rsquo;s tweet before he shared it.</p>\r\n\r\n<p>Retiring the lower-range Model S and X vehicles may push prospective customers to buy Tesla&rsquo;s high-end Model 3 sedans instead.</p>\r\n\r\n<p><a href=\"https://www.cnbc.com/2018/05/19/elon-musk-teases-specifications-for-teslas-model-3-calling-it-amazing.html\" target=\"\">A Model 3 with bells and whistles</a>&nbsp;&mdash; like dual-motor all wheel drive, enhanced Autopilot, and a $5,000 performance upgrade package &mdash; will cost around $67,000, according to the company&rsquo;s Model 3 configurator online.</p>\r\n\r\n<p>Reducing the number of variations of Model S and X vehicles that Tesla has to make may help the company control costs, or reallocate materials and labor to other initiatives, such as Model 3 production at its packed car plant in Fremont, California or spare parts production in nearby Lathrop.</p>\r\n",
  "lang": "en",
  "created_at": ISODate("2019-01-10T07:15:09.659Z"),
  "updated_at": ISODate("2019-08-06T04:21:24.898Z")
});
db.getCollection("gallery_contents").insert({
  "_id": ObjectId("5c36f0fdf315ad49251397b0"),
  "gallery_id": ObjectId("5c36f0fdf315ad49251397ad"),
  "description": "",
  "lang": "fr",
  "created_at": ISODate("2019-01-10T07:15:09.874Z"),
  "updated_at": ISODate("2019-08-06T04:21:24.898Z")
});
db.getCollection("gallery_contents").insert({
  "_id": ObjectId("5c36f0fdf315ad49251397b2"),
  "gallery_id": ObjectId("5c36f0fdf315ad49251397ad"),
  "description": "",
  "lang": "Gr",
  "created_at": ISODate("2019-01-10T07:15:09.882Z"),
  "updated_at": ISODate("2019-08-06T04:21:24.898Z")
});
db.getCollection("gallery_contents").insert({
  "_id": ObjectId("5c36f0fdf315ad49251397b4"),
  "gallery_id": ObjectId("5c36f0fdf315ad49251397ad"),
  "description": "",
  "lang": "Sp",
  "created_at": ISODate("2019-01-10T07:15:09.889Z"),
  "updated_at": ISODate("2019-08-06T04:21:24.898Z")
});
db.getCollection("gallery_contents").insert({
  "_id": ObjectId("5c36f148f315ad49251397bb"),
  "gallery_id": ObjectId("5c36f148f315ad49251397ba"),
  "description": "<p>Democrat Tom Steyer, a billionaire who has been pushing Congress to impeach President&nbsp;<a href=\"https://www.cnbc.com/donald-trump/\" target=\"\">Donald Trump</a>, will not run for president in the 2020 campaign.</p>\r\n\r\n<p>CNBC confirmed with a Steyer representative that he wouldn&rsquo;t run after&nbsp;<a href=\"https://www.nytimes.com/2019/01/09/us/politics/tom-steyer-trump-2020.html\" target=\"\">The New York Times</a>&nbsp;first reported the development. The Times obtained prepared remarks from a speech Steyer was scheduled to give late Wednesday afternoon.</p>\r\n\r\n<p>Steyer&rsquo;s decision came as other potential Democratic candidates considered whether to run. A flood of announcements is expected in the coming weeks.</p>\r\n\r\n<p>Sen.&nbsp;<a href=\"https://www.cnbc.com/elizabeth-warren/\" target=\"\">Elizabeth Warren</a>, D-Mass., has already announced that she formed an exploratory committee, which is a big step toward declaring a full candidacy. Other top Democrats, such as former Vice President&nbsp;<a href=\"https://www.cnbc.com/joe-biden/\" target=\"\">Joe Biden</a>, and Sen.&nbsp;<a href=\"https://www.cnbc.com/bernie-sanders/\" target=\"\">Bernie Sanders</a>, I-Vt., are also considering whether to run in what could be a large field. The first contests of the presidential primary season start a year from now.</p>\r\n\r\n<p>Steyer, of California, was one of the top donors during the 2018 congressional midterm elections.</p>\r\n",
  "lang": "en",
  "created_at": ISODate("2019-01-10T07:16:24.746Z"),
  "updated_at": ISODate("2019-08-06T04:21:30.206Z")
});
db.getCollection("gallery_contents").insert({
  "_id": ObjectId("5c36f148f315ad49251397bd"),
  "gallery_id": ObjectId("5c36f148f315ad49251397ba"),
  "description": "",
  "lang": "fr",
  "created_at": ISODate("2019-01-10T07:16:24.748Z"),
  "updated_at": ISODate("2019-08-06T04:21:30.206Z")
});
db.getCollection("gallery_contents").insert({
  "_id": ObjectId("5c36f148f315ad49251397bf"),
  "gallery_id": ObjectId("5c36f148f315ad49251397ba"),
  "description": "",
  "lang": "Gr",
  "created_at": ISODate("2019-01-10T07:16:24.750Z"),
  "updated_at": ISODate("2019-08-06T04:21:30.206Z")
});
db.getCollection("gallery_contents").insert({
  "_id": ObjectId("5c36f148f315ad49251397c1"),
  "gallery_id": ObjectId("5c36f148f315ad49251397ba"),
  "description": "",
  "lang": "Sp",
  "created_at": ISODate("2019-01-10T07:16:24.752Z"),
  "updated_at": ISODate("2019-08-06T04:21:30.206Z")
});
db.getCollection("gallery_contents").insert({
  "_id": ObjectId("5c36f208f315ad49251397c8"),
  "gallery_id": ObjectId("5c36f208f315ad49251397c7"),
  "description": "<p>Written &amp; Directed by Dean Fleischer-Camp<br />\r\nProduced by Riel Roch Decter &amp; Sebastian Pardo</p>\r\n\r\n<p>CAST:<br />\r\nAbraham- Ben Sinclair<br />\r\nSarah- Veronica Osorio<br />\r\nIsaac- Ronin Thompson<br />\r\nVillager- Myles Cranford&nbsp;<br />\r\nFarmer- Kat Purgal<br />\r\nBrother- Noel Arthur<br />\r\nServant 1- Nelson Cheng<br />\r\nServant 2- William Walton<br />\r\nAngel- Brandi Austin</p>\r\n\r\n<p>CAMERA<br />\r\nDP- James Wall<br />\r\n1st AC- Travis Daking<br />\r\n2nd AC- Jeanna Kim<br />\r\nDIT- Timothy Gaer</p>\r\n\r\n<p>SOUND<br />\r\nSound Mixer- Blake Christian<br />\r\nBoom Operator</p>\r\n\r\n<p>ELECTRIC<br />\r\nGaffer- Robert Oliva<br />\r\nBest Boy- Matthew Martinez</p>\r\n\r\n<p>GRIP<br />\r\nKey Grip- Danny Barfield<br />\r\nBB Grip- Travis Huston<br />\r\nGrip- Brian Dumois<br />\r\nGrip- Robert Richards<br />\r\nGrip- Gabriel Borquez<br />\r\nGrip- Josh Sides<br />\r\nGrip Driver- Dave Blackman</p>\r\n\r\n<p>PRODUCTION<br />\r\nScript Supervisor- Vaughn Greve<br />\r\nFirst AD- Bryan Goeres<br />\r\nSecond AD- Mike Tsucalas<br />\r\nLine Producer- Nick Batchelder<br />\r\nProduction Supervisor- Maria Felix Oviedo<br />\r\nOffice PA- Will Crowther</p>\r\n\r\n<p>MAKE-UP<br />\r\nKey HMU Artist- Marina Coria<br />\r\nHMU Assistant- Emily Mefford</p>\r\n\r\n<p>WARDROBE<br />\r\nCostume Designer- Natasha Noorvash&nbsp;<br />\r\nAssistant Costume Designer- Amanda Lee</p>\r\n\r\n<p>ART<br />\r\nProduction Designer- Almitra Corey<br />\r\nProp Master- Elana Farley<br />\r\nLeadman- Davey Cooperwasser</p>\r\n\r\n<p>CASTING&nbsp;<br />\r\nDoyle/Fiorilli Casting</p>\r\n\r\n<p>POST<br />\r\nPost Supervisor- Grant Keiner<br />\r\nPost Coordinator- Karen Rodriguez<br />\r\nEditing- Aaron Beckum &amp; Dean Fleischer-Camp<br />\r\nSound Mixing and Design- Brent Kiser, Unbridled Sound<br />\r\nVFX- Eric Overton</p>\r\n\r\n<p>ORIGINAL SCORE by Will Wiesenfeld</p>\r\n\r\n<p>TITLE DESIGN by Teddy Blanks</p>\r\n\r\n<p>FOR SUPER DELUXE:<br />\r\nSVP Originals- Winnie Kemp<br />\r\nHead of Prod- Josef Lieck<br />\r\nEP, Web Series- Mia Di Pasquale&nbsp;<br />\r\nSD Line Producer- Scott Keiner</p>\r\n",
  "lang": "en",
  "created_at": ISODate("2019-01-10T07:19:36.552Z"),
  "updated_at": ISODate("2019-06-21T09:17:08.42Z")
});
db.getCollection("gallery_contents").insert({
  "_id": ObjectId("5c36f208f315ad49251397ca"),
  "gallery_id": ObjectId("5c36f208f315ad49251397c7"),
  "description": "",
  "lang": "fr",
  "created_at": ISODate("2019-01-10T07:19:36.556Z"),
  "updated_at": ISODate("2019-06-21T09:17:08.42Z")
});
db.getCollection("gallery_contents").insert({
  "_id": ObjectId("5c36f208f315ad49251397cc"),
  "gallery_id": ObjectId("5c36f208f315ad49251397c7"),
  "description": "",
  "lang": "Gr",
  "created_at": ISODate("2019-01-10T07:19:36.560Z"),
  "updated_at": ISODate("2019-06-21T09:17:08.42Z")
});
db.getCollection("gallery_contents").insert({
  "_id": ObjectId("5c36f208f315ad49251397ce"),
  "gallery_id": ObjectId("5c36f208f315ad49251397c7"),
  "description": "",
  "lang": "Sp",
  "created_at": ISODate("2019-01-10T07:19:36.562Z"),
  "updated_at": ISODate("2019-06-21T09:17:08.42Z")
});
db.getCollection("gallery_contents").insert({
  "_id": ObjectId("5cb7b87d2a31271b4165f400"),
  "gallery_id": ObjectId("5cb7b87d2a31271b4165f3ff"),
  "description": "<p>this may be a dog but he is overground</p>\r\n",
  "lang": "en",
  "created_at": ISODate("2019-04-17T23:36:29.897Z"),
  "updated_at": ISODate("2019-08-06T04:21:34.632Z")
});
db.getCollection("gallery_contents").insert({
  "_id": ObjectId("5cb7b87d2a31271b4165f402"),
  "gallery_id": ObjectId("5cb7b87d2a31271b4165f3ff"),
  "description": "",
  "lang": "fr",
  "created_at": ISODate("2019-04-17T23:36:29.925Z"),
  "updated_at": ISODate("2019-08-06T04:21:34.632Z")
});
db.getCollection("gallery_contents").insert({
  "_id": ObjectId("5cb7b87d2a31271b4165f404"),
  "gallery_id": ObjectId("5cb7b87d2a31271b4165f3ff"),
  "description": "",
  "lang": "Gr",
  "created_at": ISODate("2019-04-17T23:36:29.931Z"),
  "updated_at": ISODate("2019-08-06T04:21:34.632Z")
});
db.getCollection("gallery_contents").insert({
  "_id": ObjectId("5cb7b87d2a31271b4165f406"),
  "gallery_id": ObjectId("5cb7b87d2a31271b4165f3ff"),
  "description": "",
  "lang": "Sp",
  "created_at": ISODate("2019-04-17T23:36:29.940Z"),
  "updated_at": ISODate("2019-08-06T04:21:34.632Z")
});
db.getCollection("gallery_contents").insert({
  "_id": ObjectId("5cefff25fb42690c45d4418b"),
  "gallery_id": ObjectId("5cefff25fb42690c45d4418a"),
  "description": "<p>clinical gait assessment tools and reports at your fingertips.</p>\r\n",
  "lang": "en",
  "created_at": ISODate("2019-05-30T16:04:53.8Z"),
  "updated_at": ISODate("2019-08-06T04:23:42.862Z")
});
db.getCollection("gallery_contents").insert({
  "_id": ObjectId("5cefff25fb42690c45d4418d"),
  "gallery_id": ObjectId("5cefff25fb42690c45d4418a"),
  "description": "",
  "lang": "fr",
  "created_at": ISODate("2019-05-30T16:04:53.12Z"),
  "updated_at": ISODate("2019-08-06T04:23:42.862Z")
});
db.getCollection("gallery_contents").insert({
  "_id": ObjectId("5cefff25fb42690c45d4418f"),
  "gallery_id": ObjectId("5cefff25fb42690c45d4418a"),
  "description": "",
  "lang": "Gr",
  "created_at": ISODate("2019-05-30T16:04:53.14Z"),
  "updated_at": ISODate("2019-08-06T04:23:42.862Z")
});
db.getCollection("gallery_contents").insert({
  "_id": ObjectId("5cefff25fb42690c45d44191"),
  "gallery_id": ObjectId("5cefff25fb42690c45d4418a"),
  "description": "",
  "lang": "Sp",
  "created_at": ISODate("2019-05-30T16:04:53.16Z"),
  "updated_at": ISODate("2019-08-06T04:23:42.863Z")
});
db.getCollection("gallery_contents").insert({
  "_id": ObjectId("5d0ca0af0a2c2e561c735078"),
  "gallery_id": ObjectId("5d0ca0af0a2c2e561c735077"),
  "description": "<p>Test VideoTest VideoTest VideoTest VideoTest VideoTest Video</p>\r\n",
  "lang": "en",
  "created_at": ISODate("2019-06-21T09:17:35.257Z"),
  "updated_at": ISODate("2019-10-01T15:50:00.575Z")
});
db.getCollection("gallery_contents").insert({
  "_id": ObjectId("5d0ca0af0a2c2e561c73507a"),
  "gallery_id": ObjectId("5d0ca0af0a2c2e561c735077"),
  "description": "",
  "lang": "fr",
  "created_at": ISODate("2019-06-21T09:17:35.261Z"),
  "updated_at": ISODate("2019-10-01T15:50:00.575Z")
});
db.getCollection("gallery_contents").insert({
  "_id": ObjectId("5d0ca0af0a2c2e561c73507c"),
  "gallery_id": ObjectId("5d0ca0af0a2c2e561c735077"),
  "description": "",
  "lang": "Gr",
  "created_at": ISODate("2019-06-21T09:17:35.263Z"),
  "updated_at": ISODate("2019-10-01T15:50:00.575Z")
});
db.getCollection("gallery_contents").insert({
  "_id": ObjectId("5d0ca0af0a2c2e561c73507e"),
  "gallery_id": ObjectId("5d0ca0af0a2c2e561c735077"),
  "description": "",
  "lang": "Sp",
  "created_at": ISODate("2019-06-21T09:17:35.265Z"),
  "updated_at": ISODate("2019-10-01T15:50:00.576Z")
});
db.getCollection("gallery_contents").insert({
  "_id": ObjectId("5d7b577fe696962cf1b33aae"),
  "gallery_id": ObjectId("5d7b577fe696962cf1b33aad"),
  "description": "<p>About US on Vimeo</p>\r\n",
  "lang": "en",
  "created_at": ISODate("2019-09-13T08:46:55.483Z"),
  "updated_at": ISODate("2019-09-13T08:46:55.483Z")
});
db.getCollection("gallery_contents").insert({
  "_id": ObjectId("5d7b577fe696962cf1b33ab0"),
  "gallery_id": ObjectId("5d7b577fe696962cf1b33aad"),
  "description": "",
  "lang": "fr",
  "created_at": ISODate("2019-09-13T08:46:55.488Z"),
  "updated_at": ISODate("2019-09-13T08:46:55.488Z")
});
db.getCollection("gallery_contents").insert({
  "_id": ObjectId("5d7b577fe696962cf1b33ab2"),
  "gallery_id": ObjectId("5d7b577fe696962cf1b33aad"),
  "description": "",
  "lang": "Gr",
  "created_at": ISODate("2019-09-13T08:46:55.490Z"),
  "updated_at": ISODate("2019-09-13T08:46:55.490Z")
});
db.getCollection("gallery_contents").insert({
  "_id": ObjectId("5d7b577fe696962cf1b33ab4"),
  "gallery_id": ObjectId("5d7b577fe696962cf1b33aad"),
  "description": "",
  "lang": "Sp",
  "created_at": ISODate("2019-09-13T08:46:55.492Z"),
  "updated_at": ISODate("2019-09-13T08:46:55.492Z")
});
db.getCollection("gallery_contents").insert({
  "_id": ObjectId("5d81b43de696962cf1b33b51"),
  "gallery_id": ObjectId("5d81b43de696962cf1b33b50"),
  "description": "<p>&nbsp;Once you have received and assembled your LiteGait we ask that you complete a new equipment service check. Please review this video while near the equipment. Physically check each assembly to ensure for proper functionality. The completion of this check helps to ensure the equipment is assembled and functioning properly, that you have received all components and are ready to begin use of the LiteGait.</p>\r\n",
  "lang": "en",
  "created_at": ISODate("2019-09-18T04:36:13.671Z"),
  "updated_at": ISODate("2019-09-18T04:38:11.67Z")
});
db.getCollection("gallery_contents").insert({
  "_id": ObjectId("5d81b43de696962cf1b33b53"),
  "gallery_id": ObjectId("5d81b43de696962cf1b33b50"),
  "description": "",
  "lang": "fr",
  "created_at": ISODate("2019-09-18T04:36:13.677Z"),
  "updated_at": ISODate("2019-09-18T04:38:11.67Z")
});
db.getCollection("gallery_contents").insert({
  "_id": ObjectId("5d81b43de696962cf1b33b55"),
  "gallery_id": ObjectId("5d81b43de696962cf1b33b50"),
  "description": "",
  "lang": "Gr",
  "created_at": ISODate("2019-09-18T04:36:13.679Z"),
  "updated_at": ISODate("2019-09-18T04:38:11.67Z")
});
db.getCollection("gallery_contents").insert({
  "_id": ObjectId("5d81b43de696962cf1b33b57"),
  "gallery_id": ObjectId("5d81b43de696962cf1b33b50"),
  "description": "",
  "lang": "Sp",
  "created_at": ISODate("2019-09-18T04:36:13.680Z"),
  "updated_at": ISODate("2019-09-18T04:38:11.67Z")
});

/** languages records **/
db.getCollection("languages").insert({
  "_id": ObjectId("5bd16dfab605151b7e93d862"),
  "code": "en",
  "name": "English",
  "status": NumberInt(1),
  "created_at": ISODate("2018-10-25T07:17:14.383Z"),
  "updated_at": ISODate("2018-10-25T07:17:14.383Z")
});
db.getCollection("languages").insert({
  "_id": ObjectId("5bd16e01b605151b7e93d866"),
  "code": "fr",
  "name": "French",
  "status": NumberInt(1),
  "created_at": ISODate("2018-10-25T07:17:21.393Z"),
  "updated_at": ISODate("2018-10-25T07:17:21.393Z")
});
db.getCollection("languages").insert({
  "_id": ObjectId("5c0acea4b52d1035f4b14186"),
  "code": "Gr",
  "name": "German",
  "status": NumberInt(1),
  "created_at": ISODate("2018-12-07T19:48:52.872Z"),
  "updated_at": ISODate("2018-12-07T19:48:52.872Z")
});
db.getCollection("languages").insert({
  "_id": ObjectId("5c0aced4b52d1035f4b1418a"),
  "code": "Sp",
  "name": "Spanish",
  "status": NumberInt(1),
  "created_at": ISODate("2018-12-07T19:49:40.590Z"),
  "updated_at": ISODate("2018-12-07T19:49:40.590Z")
});

/** menus records **/
db.getCollection("menus").insert({
  "_id": ObjectId("5c3469e9f0c69030a0af90e5"),
  "menuname": "footer1",
  "type": "static_url",
  "name": "Education",
  "parent_id": "0",
  "page_id": "",
  "category_id": "",
  "product_id": "",
  "static_url": "/education",
  "status": NumberInt(1),
  "created_at": ISODate("2019-01-08T09:14:17.324Z"),
  "updated_at": ISODate("2019-01-08T12:52:18.504Z")
});
db.getCollection("menus").insert({
  "_id": ObjectId("5c3469fdf0c69030a0af90ee"),
  "menuname": "footer1",
  "type": "product",
  "name": "Products",
  "parent_id": "0",
  "page_id": "",
  "category_id": "",
  "product_id": "5bd01d4a3046362654f47be6",
  "static_url": "",
  "status": NumberInt(1),
  "created_at": ISODate("2019-01-08T09:14:37.932Z"),
  "updated_at": ISODate("2019-01-08T09:16:43.457Z")
});
db.getCollection("menus").insert({
  "_id": ObjectId("5c346a8df0c69030a0af9122"),
  "menuname": "footer1",
  "type": "page",
  "name": "About US",
  "parent_id": "0",
  "page_id": "5bcd6e066c5a3c2a6cffaf47",
  "category_id": "",
  "product_id": "",
  "static_url": "",
  "status": NumberInt(1),
  "created_at": ISODate("2019-01-08T09:17:02.1Z"),
  "updated_at": ISODate("2019-01-08T09:17:02.1Z")
});
db.getCollection("menus").insert({
  "_id": ObjectId("5c346a9bf0c69030a0af912b"),
  "menuname": "footer1",
  "type": "page",
  "name": "Info Request",
  "parent_id": "0",
  "page_id": "5c346a25f0c69030a0af90fc",
  "category_id": "",
  "product_id": "",
  "static_url": "",
  "status": NumberInt(1),
  "created_at": ISODate("2019-01-08T09:17:15.756Z"),
  "updated_at": ISODate("2019-01-08T09:17:15.756Z")
});
db.getCollection("menus").insert({
  "_id": ObjectId("5c346abdf0c69030a0af913e"),
  "menuname": "footer1",
  "type": "static_url",
  "name": "Blogs",
  "parent_id": "0",
  "page_id": "",
  "category_id": "",
  "product_id": "",
  "static_url": "/blogs",
  "status": NumberInt(1),
  "created_at": ISODate("2019-01-08T09:17:49.899Z"),
  "updated_at": ISODate("2019-01-08T09:17:49.899Z")
});
db.getCollection("menus").insert({
  "_id": ObjectId("5c346ad1f0c69030a0af914c"),
  "menuname": "footer1",
  "type": "category",
  "name": "Adult Litegait",
  "parent_id": "0",
  "page_id": "",
  "category_id": "5c3869574b62c059899cf4cc",
  "product_id": "",
  "static_url": "",
  "status": NumberInt(1),
  "created_at": ISODate("2019-01-08T09:18:09.323Z"),
  "updated_at": ISODate("2019-01-11T10:01:14.8Z")
});
db.getCollection("menus").insert({
  "_id": ObjectId("5c346ae9f0c69030a0af9157"),
  "menuname": "footer2",
  "type": "page",
  "name": "CEU Webinars",
  "parent_id": "0",
  "page_id": "5bcd6e066c5a3c2a6cffaf47",
  "category_id": "",
  "product_id": "",
  "static_url": "",
  "status": NumberInt(1),
  "created_at": ISODate("2019-01-08T09:18:33.926Z"),
  "updated_at": ISODate("2019-10-11T05:06:52.483Z")
});
db.getCollection("menus").insert({
  "_id": ObjectId("5c346af5f0c69030a0af9160"),
  "menuname": "footer2",
  "type": "page",
  "name": "Ask a PT",
  "parent_id": "0",
  "page_id": "5bcd6e066c5a3c2a6cffaf47",
  "category_id": "",
  "product_id": "",
  "static_url": "",
  "status": NumberInt(1),
  "created_at": ISODate("2019-01-08T09:18:45.256Z"),
  "updated_at": ISODate("2019-01-08T09:18:45.256Z")
});
db.getCollection("menus").insert({
  "_id": ObjectId("5c346b09f0c69030a0af9169"),
  "menuname": "footer2",
  "type": "static_url",
  "name": "Sign up",
  "parent_id": "0",
  "page_id": "",
  "category_id": "",
  "product_id": "",
  "static_url": "/register",
  "status": NumberInt(1),
  "created_at": ISODate("2019-01-08T09:19:05.697Z"),
  "updated_at": ISODate("2019-01-08T09:19:05.697Z")
});
db.getCollection("menus").insert({
  "_id": ObjectId("5c346b1df0c69030a0af9172"),
  "menuname": "footer2",
  "type": "static_url",
  "name": "Login",
  "parent_id": "0",
  "page_id": "",
  "category_id": "",
  "product_id": "",
  "static_url": "/login",
  "status": NumberInt(1),
  "created_at": ISODate("2019-01-08T09:19:25.390Z"),
  "updated_at": ISODate("2019-01-08T09:19:25.390Z")
});
db.getCollection("menus").insert({
  "_id": ObjectId("5c346b4ff0c69030a0af91a3"),
  "menuname": "footer1",
  "type": "category",
  "name": "Cycling",
  "parent_id": "0",
  "page_id": "",
  "category_id": "5c38693e4b62c059899cf4c7",
  "product_id": "",
  "static_url": "",
  "status": NumberInt(1),
  "created_at": ISODate("2019-01-08T09:20:15.423Z"),
  "updated_at": ISODate("2019-01-11T10:01:18.905Z")
});
db.getCollection("menus").insert({
  "_id": ObjectId("5c346b5ff0c69030a0af91ac"),
  "menuname": "footer1",
  "type": "product",
  "name": "Test Product",
  "parent_id": "0",
  "page_id": "",
  "category_id": "",
  "product_id": "5c24667cd4568d68ad108584",
  "static_url": "",
  "status": NumberInt(1),
  "created_at": ISODate("2019-01-08T09:20:31.19Z"),
  "updated_at": ISODate("2019-01-11T10:02:05.408Z")
});
db.getCollection("menus").insert({
  "_id": ObjectId("5c346b77f0c69030a0af91c8"),
  "menuname": "footer2",
  "type": "product",
  "name": "Products",
  "parent_id": "0",
  "page_id": "",
  "category_id": "",
  "product_id": "5bd01d4a3046362654f47be6",
  "static_url": "",
  "status": NumberInt(1),
  "created_at": ISODate("2019-01-08T09:20:55.97Z"),
  "updated_at": ISODate("2019-01-08T09:20:55.97Z")
});
db.getCollection("menus").insert({
  "_id": ObjectId("5c346b84f0c69030a0af91d1"),
  "menuname": "footer2",
  "type": "category",
  "name": "Adult Litegait",
  "parent_id": "0",
  "page_id": "",
  "category_id": "5c3869574b62c059899cf4cc",
  "product_id": "",
  "static_url": "",
  "status": NumberInt(1),
  "created_at": ISODate("2019-01-08T09:21:08.868Z"),
  "updated_at": ISODate("2019-01-11T10:01:28.273Z")
});
db.getCollection("menus").insert({
  "_id": ObjectId("5c3593057377a702f8f1b013"),
  "menuname": "main",
  "type": "page",
  "name": "About US",
  "parent_id": "0",
  "page_id": "5bcd6e066c5a3c2a6cffaf47",
  "category_id": "",
  "product_id": "",
  "static_url": "",
  "status": NumberInt(1),
  "created_at": ISODate("2019-01-09T06:21:57.238Z"),
  "updated_at": ISODate("2019-12-05T18:20:45.870Z"),
  "weight": NumberInt(0)
});
db.getCollection("menus").insert({
  "_id": ObjectId("5c35931a7377a702f8f1b01c"),
  "menuname": "main",
  "type": "page",
  "name": "Why Litegait",
  "parent_id": "0",
  "page_id": "5bf53d41c798c41b04a9f4d0",
  "category_id": "",
  "product_id": "",
  "static_url": "",
  "status": NumberInt(1),
  "created_at": ISODate("2019-01-09T06:22:18.414Z"),
  "updated_at": ISODate("2019-12-05T18:20:45.911Z"),
  "weight": NumberInt(4)
});
db.getCollection("menus").insert({
  "_id": ObjectId("5c3593267377a702f8f1b025"),
  "menuname": "main",
  "type": "static_url",
  "name": "Products",
  "parent_id": "0",
  "page_id": "",
  "category_id": "",
  "product_id": "",
  "static_url": "/products",
  "status": NumberInt(1),
  "created_at": ISODate("2019-01-09T06:22:30.601Z"),
  "updated_at": ISODate("2019-12-05T18:20:45.894Z"),
  "weight": NumberInt(1)
});
db.getCollection("menus").insert({
  "_id": ObjectId("5c35955f7377a702f8f1b040"),
  "menuname": "main",
  "type": "static_url",
  "name": "Education",
  "parent_id": "5c35955f7377a702f8f1b040",
  "page_id": "",
  "category_id": "",
  "product_id": "",
  "static_url": "/Education",
  "status": NumberInt(1),
  "created_at": ISODate("2019-01-09T06:31:59.1Z"),
  "updated_at": ISODate("2019-07-30T15:05:03.635Z"),
  "weight": NumberInt(2)
});
db.getCollection("menus").insert({
  "_id": ObjectId("5c3595777377a702f8f1b049"),
  "menuname": "main",
  "type": "static_url",
  "name": "Trainings | Inservices",
  "parent_id": "5c35955f7377a702f8f1b040",
  "page_id": "",
  "category_id": "",
  "product_id": "",
  "static_url": "/trainings",
  "status": NumberInt(1),
  "created_at": ISODate("2019-01-09T06:32:23.241Z"),
  "updated_at": ISODate("2019-07-24T04:39:30.74Z"),
  "weight": NumberInt(1)
});
db.getCollection("menus").insert({
  "_id": ObjectId("5c3595957377a702f8f1b05b"),
  "menuname": "main",
  "type": "static_url",
  "name": "Support",
  "parent_id": "0",
  "page_id": "",
  "category_id": "",
  "product_id": "",
  "static_url": "/",
  "status": NumberInt(1),
  "created_at": ISODate("2019-01-09T06:32:53.25Z"),
  "updated_at": ISODate("2019-12-05T18:20:45.907Z"),
  "weight": NumberInt(3)
});
db.getCollection("menus").insert({
  "_id": ObjectId("5c3595a17377a702f8f1b064"),
  "menuname": "main",
  "type": "static_url",
  "name": "Service",
  "parent_id": "5c3595957377a702f8f1b05b",
  "page_id": "",
  "category_id": "",
  "product_id": "",
  "static_url": "/service",
  "status": NumberInt(1),
  "created_at": ISODate("2019-01-09T06:33:05.610Z"),
  "updated_at": ISODate("2019-12-05T18:20:45.908Z"),
  "weight": NumberInt(0)
});
db.getCollection("menus").insert({
  "_id": ObjectId("5c3595b17377a702f8f1b06d"),
  "menuname": "main",
  "type": "page",
  "name": "Info Request",
  "parent_id": "5c3595957377a702f8f1b05b",
  "page_id": "5c346a25f0c69030a0af90fc",
  "category_id": "",
  "product_id": "",
  "static_url": "",
  "status": NumberInt(1),
  "created_at": ISODate("2019-01-09T06:33:21.879Z"),
  "updated_at": ISODate("2019-12-05T18:20:45.910Z"),
  "weight": NumberInt(3)
});
db.getCollection("menus").insert({
  "_id": ObjectId("5c3595c37377a702f8f1b076"),
  "menuname": "main",
  "type": "static_url",
  "name": "News",
  "parent_id": "0",
  "page_id": "",
  "category_id": "",
  "product_id": "",
  "static_url": "/news",
  "status": NumberInt(1),
  "created_at": ISODate("2019-01-09T06:33:39.904Z"),
  "updated_at": ISODate("2019-12-05T18:20:45.912Z"),
  "weight": NumberInt(5)
});
db.getCollection("menus").insert({
  "_id": ObjectId("5c3595d27377a702f8f1b07f"),
  "menuname": "main",
  "type": "static_url",
  "name": "Gallery",
  "parent_id": "0",
  "page_id": "",
  "category_id": "",
  "product_id": "",
  "static_url": "/gallery",
  "status": NumberInt(1),
  "created_at": ISODate("2019-01-09T06:33:54.955Z"),
  "updated_at": ISODate("2019-12-05T18:20:45.914Z"),
  "weight": NumberInt(6)
});
db.getCollection("menus").insert({
  "_id": ObjectId("5c3595e37377a702f8f1b088"),
  "menuname": "main",
  "type": "static_url",
  "name": "Faq",
  "parent_id": "0",
  "page_id": "",
  "category_id": "",
  "product_id": "",
  "static_url": "/faqs",
  "status": NumberInt(1),
  "created_at": ISODate("2019-01-09T06:34:11.712Z"),
  "updated_at": ISODate("2019-12-05T18:20:45.915Z"),
  "weight": NumberInt(7)
});
db.getCollection("menus").insert({
  "_id": ObjectId("5c4aebae49503811ce915836"),
  "menuname": "main",
  "type": "category",
  "name": "LiteGait-Adult",
  "parent_id": "5c3593267377a702f8f1b025",
  "page_id": "",
  "category_id": "5c5d422055a6cc290aae530b",
  "product_id": "",
  "static_url": "",
  "status": NumberInt(1),
  "created_at": ISODate("2019-01-25T10:57:50.191Z"),
  "updated_at": ISODate("2019-12-05T18:20:45.895Z"),
  "weight": NumberInt(0)
});
db.getCollection("menus").insert({
  "_id": ObjectId("5c668beabc19ad187c839ca7"),
  "menuname": "main",
  "type": "static_url",
  "name": "Online Parts Store",
  "parent_id": "5c3595957377a702f8f1b05b",
  "page_id": "",
  "category_id": "",
  "product_id": "",
  "static_url": "/parts",
  "status": NumberInt(1),
  "created_at": ISODate("2019-02-15T09:52:42.862Z"),
  "updated_at": ISODate("2019-12-05T18:20:45.910Z"),
  "weight": NumberInt(2)
});
db.getCollection("menus").insert({
  "_id": ObjectId("5c748880092aa62529ff4d82"),
  "menuname": "main",
  "type": "category",
  "name": "LiteGait-Pediatric",
  "parent_id": "5c3593267377a702f8f1b025",
  "page_id": "",
  "category_id": "5c748820092aa62529ff4d70",
  "product_id": "",
  "static_url": "",
  "status": NumberInt(1),
  "created_at": ISODate("2019-02-26T00:29:52.673Z"),
  "updated_at": ISODate("2019-12-05T18:20:45.895Z"),
  "weight": NumberInt(1)
});
db.getCollection("menus").insert({
  "_id": ObjectId("5c78f57b8592290d03e20026"),
  "menuname": "main",
  "type": "static_url",
  "name": "Parts",
  "parent_id": "5c3593267377a702f8f1b025",
  "page_id": "",
  "category_id": "",
  "product_id": "",
  "static_url": "/parts",
  "status": NumberInt(1),
  "created_at": ISODate("2019-03-01T09:03:55.781Z"),
  "updated_at": ISODate("2019-12-05T18:20:45.902Z"),
  "weight": NumberInt(11)
});
db.getCollection("menus").insert({
  "_id": ObjectId("5c78f58c8592290d03e2002f"),
  "menuname": "main",
  "type": "static_url",
  "name": "Accessories",
  "parent_id": "5c3593267377a702f8f1b025",
  "page_id": "",
  "category_id": "",
  "product_id": "",
  "static_url": "/accessories",
  "status": NumberInt(1),
  "created_at": ISODate("2019-03-01T09:04:12.152Z"),
  "updated_at": ISODate("2019-12-05T18:20:45.902Z"),
  "weight": NumberInt(10)
});
db.getCollection("menus").insert({
  "_id": ObjectId("5c78f70d8592290d03e20077"),
  "menuname": "main",
  "type": "category",
  "name": "LiteGait 4 Home",
  "parent_id": "5c3593267377a702f8f1b025",
  "page_id": "",
  "category_id": "5c78f6718592290d03e20040",
  "product_id": "",
  "static_url": "",
  "status": NumberInt(1),
  "created_at": ISODate("2019-03-01T09:10:37.721Z"),
  "updated_at": ISODate("2019-12-05T18:20:45.896Z"),
  "weight": NumberInt(2)
});
db.getCollection("menus").insert({
  "_id": ObjectId("5c78f7218592290d03e20080"),
  "menuname": "main",
  "type": "category",
  "name": "Treadmills",
  "parent_id": "5c3593267377a702f8f1b025",
  "page_id": "",
  "category_id": "5c78f6ae8592290d03e2004c",
  "product_id": "",
  "static_url": "",
  "status": NumberInt(1),
  "created_at": ISODate("2019-03-01T09:10:57.764Z"),
  "updated_at": ISODate("2019-12-05T18:20:45.897Z"),
  "weight": NumberInt(3)
});
db.getCollection("menus").insert({
  "_id": ObjectId("5c890a5fc33f2b242a17f27e"),
  "menuname": "main",
  "type": "static_url",
  "name": "Webinars",
  "parent_id": "5c35955f7377a702f8f1b040",
  "page_id": "",
  "category_id": "",
  "product_id": "",
  "static_url": "/webinars",
  "status": NumberInt(1),
  "created_at": ISODate("2019-03-13T13:49:19.6Z"),
  "updated_at": ISODate("2019-07-24T04:39:30.73Z"),
  "weight": NumberInt(0)
});
db.getCollection("menus").insert({
  "_id": ObjectId("5c890a6dc33f2b242a17f287"),
  "menuname": "main",
  "type": "static_url",
  "name": "Seminars | Workshops",
  "parent_id": "5c35955f7377a702f8f1b040",
  "page_id": "",
  "category_id": "",
  "product_id": "",
  "static_url": "/seminars",
  "status": NumberInt(1),
  "created_at": ISODate("2019-03-13T13:49:33.297Z"),
  "updated_at": ISODate("2019-07-24T04:39:30.75Z"),
  "weight": NumberInt(2)
});
db.getCollection("menus").insert({
  "_id": ObjectId("5c890a8dc33f2b242a17f290"),
  "menuname": "main",
  "type": "static_url",
  "name": "Instructors & Trainers",
  "parent_id": "5c35955f7377a702f8f1b040",
  "page_id": "",
  "category_id": "",
  "product_id": "",
  "static_url": "/instructors",
  "status": NumberInt(1),
  "created_at": ISODate("2019-03-13T13:50:05.368Z"),
  "updated_at": ISODate("2019-07-24T04:39:30.77Z"),
  "weight": NumberInt(4)
});
db.getCollection("menus").insert({
  "_id": ObjectId("5cb7c0a02a31271b4165f44f"),
  "menuname": "main",
  "type": "static_url",
  "name": "Clinical Support",
  "parent_id": "5c35955f7377a702f8f1b040",
  "page_id": "",
  "category_id": "",
  "product_id": "",
  "static_url": "/clinical-support",
  "status": NumberInt(1),
  "created_at": ISODate("2019-04-18T00:11:12.122Z"),
  "updated_at": ISODate("2019-07-24T04:39:30.76Z"),
  "weight": NumberInt(3)
});
db.getCollection("menus").insert({
  "_id": ObjectId("5d14eb560a2c2e561c735121"),
  "menuname": "main",
  "type": "page",
  "name": "Clinical Support",
  "parent_id": "5c3595957377a702f8f1b05b",
  "page_id": "5d53e5dbfbb7781db271cd98",
  "category_id": "",
  "product_id": "",
  "static_url": "",
  "status": NumberInt(1),
  "created_at": ISODate("2019-06-27T16:14:14.328Z"),
  "updated_at": ISODate("2019-12-05T18:20:45.909Z"),
  "weight": NumberInt(1)
});
db.getCollection("menus").insert({
  "_id": ObjectId("5d14f7a00a2c2e561c735140"),
  "menuname": "main",
  "type": "category",
  "name": "Gait Analysis",
  "parent_id": "5c3593267377a702f8f1b025",
  "page_id": "",
  "category_id": "5d14f1540a2c2e561c73512b",
  "product_id": "",
  "static_url": "",
  "status": NumberInt(1),
  "created_at": ISODate("2019-06-27T17:06:40.278Z"),
  "updated_at": ISODate("2019-12-05T18:20:45.897Z"),
  "weight": NumberInt(4)
});
db.getCollection("menus").insert({
  "_id": ObjectId("5d2de704837f487c405e62d6"),
  "menuname": "main",
  "type": "category",
  "name": "Therapy Mouse",
  "parent_id": "5c3593267377a702f8f1b025",
  "page_id": "",
  "category_id": "5cf0096dfb42690c45d441be",
  "product_id": "",
  "static_url": "",
  "status": NumberInt(1),
  "created_at": ISODate("2019-07-16T15:02:28.997Z"),
  "updated_at": ISODate("2019-12-05T18:20:45.900Z"),
  "weight": NumberInt(8)
});
db.getCollection("menus").insert({
  "_id": ObjectId("5d2deccc837f487c405e633e"),
  "menuname": "main",
  "type": "category",
  "name": "HugN-Go",
  "parent_id": "5c3593267377a702f8f1b025",
  "page_id": "",
  "category_id": "5d2de90b837f487c405e62ee",
  "product_id": "",
  "static_url": "",
  "status": NumberInt(1),
  "created_at": ISODate("2019-07-16T15:27:08.925Z"),
  "updated_at": ISODate("2019-12-05T18:20:45.898Z"),
  "weight": NumberInt(5)
});
db.getCollection("menus").insert({
  "_id": ObjectId("5d37d4952a02f42595837151"),
  "menuname": "main",
  "type": "category",
  "name": "LiteGait Vet",
  "parent_id": "5c3593267377a702f8f1b025",
  "page_id": "",
  "category_id": "5d37d4302a02f4259583713f",
  "product_id": "",
  "static_url": "",
  "status": NumberInt(1),
  "created_at": ISODate("2019-07-24T03:46:29.78Z"),
  "updated_at": ISODate("2019-12-05T18:20:45.899Z"),
  "weight": NumberInt(6)
});
db.getCollection("menus").insert({
  "_id": ObjectId("5d37e0ba2a02f4259583717a"),
  "menuname": "main",
  "type": "category",
  "name": "Qpads",
  "parent_id": "5c3593267377a702f8f1b025",
  "page_id": "",
  "category_id": "5d2de7f4837f487c405e62e2",
  "product_id": "",
  "static_url": "",
  "status": NumberInt(1),
  "created_at": ISODate("2019-07-24T04:38:18.760Z"),
  "updated_at": ISODate("2019-12-05T18:20:45.900Z"),
  "weight": NumberInt(7)
});
db.getCollection("menus").insert({
  "_id": ObjectId("5d37e0e72a02f42595837185"),
  "menuname": "main",
  "type": "category",
  "name": "CrawlAhead",
  "parent_id": "5c3593267377a702f8f1b025",
  "page_id": "",
  "category_id": "5d37df0c2a02f42595837165",
  "product_id": "",
  "static_url": "",
  "status": NumberInt(1),
  "created_at": ISODate("2019-07-24T04:39:03.577Z"),
  "updated_at": ISODate("2019-12-05T18:20:45.901Z"),
  "weight": NumberInt(9)
});
db.getCollection("menus").insert({
  "_id": ObjectId("5d4060e8e7f8521544da1491"),
  "menuname": "main",
  "type": "page",
  "name": "Education",
  "parent_id": "0",
  "page_id": "5d405c57e7f8521544da144b",
  "category_id": "",
  "product_id": "",
  "static_url": "",
  "status": NumberInt(1),
  "created_at": ISODate("2019-07-30T15:23:20.729Z"),
  "updated_at": ISODate("2019-12-05T18:20:45.903Z"),
  "weight": NumberInt(2)
});
db.getCollection("menus").insert({
  "_id": ObjectId("5d4061dee7f8521544da14a8"),
  "menuname": "main",
  "type": "static_url",
  "name": "Webinars",
  "parent_id": "5d4060e8e7f8521544da1491",
  "page_id": "",
  "category_id": "",
  "product_id": "",
  "static_url": "/webinars",
  "status": NumberInt(1),
  "created_at": ISODate("2019-07-30T15:27:26.84Z"),
  "updated_at": ISODate("2019-12-05T18:20:45.904Z"),
  "weight": NumberInt(0)
});
db.getCollection("menus").insert({
  "_id": ObjectId("5d4062dde7f8521544da14c4"),
  "menuname": "main",
  "type": "static_url",
  "name": "Trainings | Inservices",
  "parent_id": "5d4060e8e7f8521544da1491",
  "page_id": "",
  "category_id": "",
  "product_id": "",
  "static_url": "/trainings-inservices",
  "status": NumberInt(1),
  "created_at": ISODate("2019-07-30T15:31:41.378Z"),
  "updated_at": ISODate("2019-12-05T18:20:45.905Z"),
  "weight": NumberInt(1)
});
db.getCollection("menus").insert({
  "_id": ObjectId("5d40630ee7f8521544da14cd"),
  "menuname": "main",
  "type": "static_url",
  "name": "Seminars | Workshops",
  "parent_id": "5d4060e8e7f8521544da1491",
  "page_id": "",
  "category_id": "",
  "product_id": "",
  "static_url": "/seminars",
  "status": NumberInt(1),
  "created_at": ISODate("2019-07-30T15:32:30.409Z"),
  "updated_at": ISODate("2019-12-05T18:20:45.905Z"),
  "weight": NumberInt(2)
});
db.getCollection("menus").insert({
  "_id": ObjectId("5d406352e7f8521544da14d6"),
  "menuname": "main",
  "type": "page",
  "name": "Clinical Support",
  "parent_id": "5d4060e8e7f8521544da1491",
  "page_id": "5d53e5dbfbb7781db271cd98",
  "category_id": "",
  "product_id": "",
  "static_url": "",
  "status": NumberInt(1),
  "created_at": ISODate("2019-07-30T15:33:38.165Z"),
  "updated_at": ISODate("2019-12-05T18:20:45.906Z"),
  "weight": NumberInt(3)
});
db.getCollection("menus").insert({
  "_id": ObjectId("5d40637de7f8521544da14df"),
  "menuname": "main",
  "type": "static_url",
  "name": "Intructors & Trainers",
  "parent_id": "5d4060e8e7f8521544da1491",
  "page_id": "",
  "category_id": "",
  "product_id": "",
  "static_url": "/instructors",
  "status": NumberInt(1),
  "created_at": ISODate("2019-07-30T15:34:21.301Z"),
  "updated_at": ISODate("2019-12-05T18:20:45.907Z"),
  "weight": NumberInt(4)
});
db.getCollection("menus").insert({
  "_id": ObjectId("5dc0040a7f18d24e968657be"),
  "menuname": "main",
  "type": "static_url",
  "name": "Monthly Bulletin | Newsletter",
  "parent_id": "5c3595c37377a702f8f1b076",
  "page_id": "",
  "category_id": "",
  "product_id": "",
  "static_url": "/newsletter",
  "status": NumberInt(1),
  "created_at": ISODate("2019-11-04T10:57:14.305Z"),
  "updated_at": ISODate("2019-12-05T18:20:45.913Z"),
  "weight": NumberInt(1)
});
db.getCollection("menus").insert({
  "_id": ObjectId("5dcbc52c6256f133e1771f6a"),
  "menuname": "main",
  "type": "static_url",
  "name": "News",
  "parent_id": "5c3595c37377a702f8f1b076",
  "page_id": "",
  "category_id": "",
  "product_id": "",
  "static_url": "/news",
  "status": NumberInt(1),
  "created_at": ISODate("2019-11-13T08:56:12.488Z"),
  "updated_at": ISODate("2019-12-05T18:20:45.912Z"),
  "weight": NumberInt(0)
});

/** news records **/
db.getCollection("news").insert({
  "_id": ObjectId("5c36f055f315ad4925139788"),
  "lang_content": [
    ObjectId("5c36f055f315ad4925139789"),
    ObjectId("5c36f056f315ad492513978b"),
    ObjectId("5c36f056f315ad492513978d"),
    ObjectId("5c36f056f315ad492513978f")
  ],
  "title": "We went inside the world’s largest start-up campus, located in Paris",
  "short_description": "Walk inside Paris’ Station F — dubbed the world’s largest start-up campus — and you might notice how far the concept goes beyond start-ups: Amazon, Facebook and Microsoft are all among the global companies that have set up inside. That’s all part of its fabric and mission to connect corporates, organizations and universities to more than 1,000 start-ups working there.",
  "description": "<p>Walk inside Paris&rsquo; Station F &mdash; dubbed the world&rsquo;s largest start-up campus &mdash; and you might notice how far the concept goes beyond start-ups:&nbsp;<a href=\"https://www.cnbc.com/quotes/?symbol=AMZN\" target=\"\">Amazon</a>,&nbsp;<a href=\"https://www.cnbc.com/quotes/?symbol=FB\" target=\"\">Facebook</a>&nbsp;and&nbsp;<a href=\"https://www.cnbc.com/quotes/?symbol=MSFT\" target=\"\">Microsoft</a>&nbsp;are all among the global companies that have set up inside.</p>\r\n\r\n<p>That&rsquo;s all part of its fabric and mission to connect corporates, organizations and universities to more than 1,000 start-ups working there.</p>\r\n\r\n<p>CNBC recently visited the campus, which opened in 2017 and for which start-ups must apply through a variety of incubators and programs. And it&rsquo;s not just a home for tech, either: The campus also includes the likes of&nbsp;<a href=\"https://www.cnbc.com/quotes/?symbol=OR-FR\" target=\"\">L&rsquo;Oreal</a>,&nbsp;<a href=\"https://www.cnbc.com/quotes/?symbol=MC-FR\" target=\"\">LVMH</a>&nbsp;and&nbsp;<a href=\"https://www.cnbc.com/quotes/?symbol=BNP-FR\" target=\"\">BNP Paribas</a>.</p>\r\n\r\n<p>&ldquo;They love being able to talk to people that have encountered the same difficulties, that have resources, that have contacts, so they&rsquo;re really sharing with each other and I think that&rsquo;s the main resource that you find here,&rdquo; Roxanne Varza, the program director for Station F, said of the start-ups and entrepreneurs there.</p>\r\n\r\n<p>Station F was backed by French telecom billionaire Xavier Niel, who poured $285 million into the project and was launched around the same time French President Emmanuel Macron took office. Macron has&nbsp;<a href=\"https://www.cnbc.com/2017/11/27/french-president-emmanuel-macron-wants-a-nation-of-internet-start-ups.html\" target=\"\">vowed</a>&nbsp;to make France a &ldquo;startup nation.&rdquo;</p>\r\n\r\n<p>At Station F, Benjamin Joffe, a partner at hardware accelerator HAX which is part of venture capital firm SOSV, praised France&rsquo;s recent improvements in its available talent, government support and access to early-stage capital.</p>\r\n\r\n<p>&ldquo;France now has a lot of the elements necessary to actually create new companies,&rdquo; he said.</p>\r\n\r\n<p>But, while the aim of Station F is to put France on the international start-up map, Joffe also still sees potential barriers.</p>\r\n\r\n<p>&ldquo;What&rsquo;s still missing in the ecosystem are things around skills for growth, maybe marketing skills as well, knowledge of international markets and the last thing that&rsquo;s also missing, I think, in the environment, is the habits and knowledge around exits,&rdquo; he added. &ldquo;A lot of start-ups are being created, but really the proof comes when you have an exit.&rdquo;</p>\r\n",
  "expire_date": ISODate("2020-03-29T18:30:00.0Z"),
  "status": NumberInt(1),
  "created_at": ISODate("2019-01-10T07:12:21.994Z"),
  "updated_at": ISODate("2019-07-29T10:42:58.236Z"),
  "image": "/monitor-1307227_1920.jpg"
});
db.getCollection("news").insert({
  "_id": ObjectId("5c36f081f315ad4925139794"),
  "lang_content": [
    ObjectId("5c36f081f315ad4925139795"),
    ObjectId("5c36f081f315ad4925139797"),
    ObjectId("5c36f081f315ad4925139799"),
    ObjectId("5c36f081f315ad492513979b")
  ],
  "title": "Ford unveils the new 2020 Explorer, the popular SUV’s first redesign in 8 years",
  "short_description": "With more and more consumers looking to buy a midsize SUV, Ford is rolling out a fresh version of its iconic Explorer model. Unveiled Wednesday in New York, it’s the first all-new version of the Explorer in eight years.",
  "description": "<p>With more and more consumers looking to buy a midsize SUV,&nbsp;<a href=\"https://www.cnbc.com/quotes/?symbol=F\" target=\"\">Ford</a>&nbsp;is rolling out a fresh version of its iconic Explorer model. Unveiled Wednesday in New York, it&rsquo;s the first all-new version of the Explorer in eight years.</p>\r\n\r\n<p>&ldquo;The company thrives when we have fresh product, it is just as simple as that,&rdquo; Ford&rsquo;s president of global markets, Jim Farley, told CNBC after unveiling the new Explorer. &ldquo;With this new line-up of utilities from top to bottom, it is just really going to allow us to grow.&rdquo;</p>\r\n\r\n<p>The 2020 Explorer has been redesigned with special features, new technology and creature comforts SUV owners have come to expect. That includes Ford Co-Pilot360 which provides driver assistance to avoid collisions. In addition, Ford increased the towing capacity by 66 percent while making the Explorer lighter and more fuel efficient.</p>\r\n\r\n<p>Starting at $33,000, the new model goes on sale this summer, Ford said.</p>\r\n\r\n<p>&ldquo;The new Explorer is very important for Ford,&rdquo; said Jeff Schuster, analyst at automotive consulting firm LMC Automotive. &ldquo;Even though the Explorer is the top-selling midsize SUV, there&rsquo;s a lot more competition and new models coming in the next couple of years.&rdquo;</p>\r\n\r\n<p>While there were 21 midsize SUV&rsquo;s available in the U.S. last year, LMC Automotive expects that number to climb to 26 next year and to 30 models by 2022.</p>\r\n\r\n<p>That competition will be looking to cut into the Explorer&rsquo;s No. 1 position in the midsize SUV market. Last year, Ford sold more than 261,000 Explorers. An impressive number considering the model has not had a major makeover in years.</p>\r\n\r\n<p>&ldquo;The Explorer is really in the sweet spot of the market,&rdquo; said Schuster. &ldquo;Last year, midsize SUVs made up almost 14 percent of the market and we think the segment will keep growing.&rdquo;</p>\r\n\r\n<p>Whether or not the Explorer remains the leader of the SUV market remains to be seen, but the new model should position Ford to continue doing well in that segment. That&rsquo;s critical since Ford will be relying on its trucks and SUVs to generate profits while it restructures its business, investing billions in future technology like autonomous drive vehicles.</p>\r\n\r\n<p>&ldquo;We are on our toes again,&rdquo; said Farley. &ldquo;We will grow more revenue, better profitability, you know, this is when the car business is great.&rdquo;</p>\r\n",
  "expire_date": ISODate("2020-03-29T18:30:00.0Z"),
  "status": NumberInt(1),
  "created_at": ISODate("2019-01-10T07:13:05.467Z"),
  "updated_at": ISODate("2019-04-05T08:48:47.652Z")
});
db.getCollection("news").insert({
  "_id": ObjectId("5c36f0a8f315ad49251397a0"),
  "lang_content": [
    ObjectId("5c36f0a8f315ad49251397a1"),
    ObjectId("5c36f0a8f315ad49251397a3"),
    ObjectId("5c36f0a8f315ad49251397a5"),
    ObjectId("5c36f0a8f315ad49251397a7")
  ],
  "title": "Tesla to stop selling the lowest-priced versions of its Model S and X vehicles on Monday, Elon Musk tweets",
  "short_description": "The Model S and X are Tesla’s second and third major lines of electric vehicles, and are higher-priced lines compared to its newest Model 3. The 75 kWh version of the Model S and X that are being retired come with a battery that garners a range of 259 miles for the Model S, and a 237 miles for the Model X.",
  "description": "<p>The Model S and X are Tesla&rsquo;s second and third major lines of electric vehicles, and are higher-priced lines compared to its newest Model 3. The 75 kWh version of the Model S and X that are being retired come with a battery that garners a range of 259 miles for the Model S, and a 237 miles for the Model X.</p>\r\n\r\n<p>Higher-end versions of the S and X, equipped with a 100 kWh battery, will still be made and sold by Tesla, but at a higher price. Following the changes announced by Musk, the starting price for a new Model S will be $94,000 and the starting price for a Model X will be $97,000 from next week.</p>\r\n\r\n<p>Tesla did not respond to inquiries as to why the S and X offerings were pared back, or whether any team reviewed Musk&rsquo;s tweet before he shared it.</p>\r\n\r\n<p>Retiring the lower-range Model S and X vehicles may push prospective customers to buy Tesla&rsquo;s high-end Model 3 sedans instead.</p>\r\n\r\n<p><a href=\"https://www.cnbc.com/2018/05/19/elon-musk-teases-specifications-for-teslas-model-3-calling-it-amazing.html\" target=\"\">A Model 3 with bells and whistles</a>&nbsp;&mdash; like dual-motor all wheel drive, enhanced Autopilot, and a $5,000 performance upgrade package &mdash; will cost around $67,000, according to the company&rsquo;s Model 3 configurator online.</p>\r\n\r\n<p>Reducing the number of variations of Model S and X vehicles that Tesla has to make may help the company control costs, or reallocate materials and labor to other initiatives, such as Model 3 production at its packed car plant in Fremont, California or spare parts production in nearby Lathrop.</p>\r\n",
  "expire_date": ISODate("2020-03-29T18:30:00.0Z"),
  "status": NumberInt(1),
  "created_at": ISODate("2019-01-10T07:13:44.736Z"),
  "updated_at": ISODate("2019-08-08T22:53:05.549Z"),
  "image": "/Product/crawlahead/ca_01a.jpg"
});
db.getCollection("news").insert({
  "_id": ObjectId("5d394dd62a02f4259583721f"),
  "lang_content": [
    ObjectId("5d394dd62a02f42595837220"),
    ObjectId("5d394dd62a02f42595837222"),
    ObjectId("5d394dd62a02f42595837224"),
    ObjectId("5d394dd62a02f42595837226")
  ],
  "title": "LiteGait is Currently Scheduling Demonstrations ",
  "short_description": "what is this short",
  "description": "<p>and this 2</p>\r\n",
  "expire_date": ISODate("2019-10-17T18:30:00.0Z"),
  "image": "/accessories_img3.jpg",
  "status": NumberInt(1),
  "created_at": ISODate("2019-07-25T06:36:06.202Z"),
  "updated_at": ISODate("2019-10-02T04:42:55.386Z")
});

/** news_contents records **/
db.getCollection("news_contents").insert({
  "_id": ObjectId("5c36f055f315ad4925139789"),
  "news_id": ObjectId("5c36f055f315ad4925139788"),
  "lang": "en",
  "short_description": "Walk inside Paris’ Station F — dubbed the world’s largest start-up campus — and you might notice how far the concept goes beyond start-ups: Amazon, Facebook and Microsoft are all among the global companies that have set up inside. That’s all part of its fabric and mission to connect corporates, organizations and universities to more than 1,000 start-ups working there.",
  "description": "<p>Walk inside Paris&rsquo; Station F &mdash; dubbed the world&rsquo;s largest start-up campus &mdash; and you might notice how far the concept goes beyond start-ups:&nbsp;<a href=\"https://www.cnbc.com/quotes/?symbol=AMZN\" target=\"\">Amazon</a>,&nbsp;<a href=\"https://www.cnbc.com/quotes/?symbol=FB\" target=\"\">Facebook</a>&nbsp;and&nbsp;<a href=\"https://www.cnbc.com/quotes/?symbol=MSFT\" target=\"\">Microsoft</a>&nbsp;are all among the global companies that have set up inside.</p>\r\n\r\n<p>That&rsquo;s all part of its fabric and mission to connect corporates, organizations and universities to more than 1,000 start-ups working there.</p>\r\n\r\n<p>CNBC recently visited the campus, which opened in 2017 and for which start-ups must apply through a variety of incubators and programs. And it&rsquo;s not just a home for tech, either: The campus also includes the likes of&nbsp;<a href=\"https://www.cnbc.com/quotes/?symbol=OR-FR\" target=\"\">L&rsquo;Oreal</a>,&nbsp;<a href=\"https://www.cnbc.com/quotes/?symbol=MC-FR\" target=\"\">LVMH</a>&nbsp;and&nbsp;<a href=\"https://www.cnbc.com/quotes/?symbol=BNP-FR\" target=\"\">BNP Paribas</a>.</p>\r\n\r\n<p>&ldquo;They love being able to talk to people that have encountered the same difficulties, that have resources, that have contacts, so they&rsquo;re really sharing with each other and I think that&rsquo;s the main resource that you find here,&rdquo; Roxanne Varza, the program director for Station F, said of the start-ups and entrepreneurs there.</p>\r\n\r\n<p>Station F was backed by French telecom billionaire Xavier Niel, who poured $285 million into the project and was launched around the same time French President Emmanuel Macron took office. Macron has&nbsp;<a href=\"https://www.cnbc.com/2017/11/27/french-president-emmanuel-macron-wants-a-nation-of-internet-start-ups.html\" target=\"\">vowed</a>&nbsp;to make France a &ldquo;startup nation.&rdquo;</p>\r\n\r\n<p>At Station F, Benjamin Joffe, a partner at hardware accelerator HAX which is part of venture capital firm SOSV, praised France&rsquo;s recent improvements in its available talent, government support and access to early-stage capital.</p>\r\n\r\n<p>&ldquo;France now has a lot of the elements necessary to actually create new companies,&rdquo; he said.</p>\r\n\r\n<p>But, while the aim of Station F is to put France on the international start-up map, Joffe also still sees potential barriers.</p>\r\n\r\n<p>&ldquo;What&rsquo;s still missing in the ecosystem are things around skills for growth, maybe marketing skills as well, knowledge of international markets and the last thing that&rsquo;s also missing, I think, in the environment, is the habits and knowledge around exits,&rdquo; he added. &ldquo;A lot of start-ups are being created, but really the proof comes when you have an exit.&rdquo;</p>\r\n",
  "created_at": ISODate("2019-01-10T07:12:21.999Z"),
  "updated_at": ISODate("2019-07-29T10:42:58.239Z")
});
db.getCollection("news_contents").insert({
  "_id": ObjectId("5c36f056f315ad492513978b"),
  "news_id": ObjectId("5c36f055f315ad4925139788"),
  "lang": "fr",
  "short_description": "",
  "description": "",
  "created_at": ISODate("2019-01-10T07:12:22.183Z"),
  "updated_at": ISODate("2019-07-29T10:42:58.239Z")
});
db.getCollection("news_contents").insert({
  "_id": ObjectId("5c36f056f315ad492513978d"),
  "news_id": ObjectId("5c36f055f315ad4925139788"),
  "lang": "Gr",
  "short_description": "",
  "description": "",
  "created_at": ISODate("2019-01-10T07:12:22.186Z"),
  "updated_at": ISODate("2019-07-29T10:42:58.239Z")
});
db.getCollection("news_contents").insert({
  "_id": ObjectId("5c36f056f315ad492513978f"),
  "news_id": ObjectId("5c36f055f315ad4925139788"),
  "lang": "Sp",
  "short_description": "",
  "description": "",
  "created_at": ISODate("2019-01-10T07:12:22.188Z"),
  "updated_at": ISODate("2019-07-29T10:42:58.239Z")
});
db.getCollection("news_contents").insert({
  "_id": ObjectId("5c36f081f315ad4925139795"),
  "news_id": ObjectId("5c36f081f315ad4925139794"),
  "lang": "en",
  "short_description": "With more and more consumers looking to buy a midsize SUV, Ford is rolling out a fresh version of its iconic Explorer model. Unveiled Wednesday in New York, it’s the first all-new version of the Explorer in eight years.",
  "description": "<p>With more and more consumers looking to buy a midsize SUV,&nbsp;<a href=\"https://www.cnbc.com/quotes/?symbol=F\" target=\"\">Ford</a>&nbsp;is rolling out a fresh version of its iconic Explorer model. Unveiled Wednesday in New York, it&rsquo;s the first all-new version of the Explorer in eight years.</p>\r\n\r\n<p>&ldquo;The company thrives when we have fresh product, it is just as simple as that,&rdquo; Ford&rsquo;s president of global markets, Jim Farley, told CNBC after unveiling the new Explorer. &ldquo;With this new line-up of utilities from top to bottom, it is just really going to allow us to grow.&rdquo;</p>\r\n\r\n<p>The 2020 Explorer has been redesigned with special features, new technology and creature comforts SUV owners have come to expect. That includes Ford Co-Pilot360 which provides driver assistance to avoid collisions. In addition, Ford increased the towing capacity by 66 percent while making the Explorer lighter and more fuel efficient.</p>\r\n\r\n<p>Starting at $33,000, the new model goes on sale this summer, Ford said.</p>\r\n\r\n<p>&ldquo;The new Explorer is very important for Ford,&rdquo; said Jeff Schuster, analyst at automotive consulting firm LMC Automotive. &ldquo;Even though the Explorer is the top-selling midsize SUV, there&rsquo;s a lot more competition and new models coming in the next couple of years.&rdquo;</p>\r\n\r\n<p>While there were 21 midsize SUV&rsquo;s available in the U.S. last year, LMC Automotive expects that number to climb to 26 next year and to 30 models by 2022.</p>\r\n\r\n<p>That competition will be looking to cut into the Explorer&rsquo;s No. 1 position in the midsize SUV market. Last year, Ford sold more than 261,000 Explorers. An impressive number considering the model has not had a major makeover in years.</p>\r\n\r\n<p>&ldquo;The Explorer is really in the sweet spot of the market,&rdquo; said Schuster. &ldquo;Last year, midsize SUVs made up almost 14 percent of the market and we think the segment will keep growing.&rdquo;</p>\r\n\r\n<p>Whether or not the Explorer remains the leader of the SUV market remains to be seen, but the new model should position Ford to continue doing well in that segment. That&rsquo;s critical since Ford will be relying on its trucks and SUVs to generate profits while it restructures its business, investing billions in future technology like autonomous drive vehicles.</p>\r\n\r\n<p>&ldquo;We are on our toes again,&rdquo; said Farley. &ldquo;We will grow more revenue, better profitability, you know, this is when the car business is great.&rdquo;</p>\r\n",
  "created_at": ISODate("2019-01-10T07:13:05.469Z"),
  "updated_at": ISODate("2019-04-05T08:48:47.655Z")
});
db.getCollection("news_contents").insert({
  "_id": ObjectId("5c36f081f315ad4925139797"),
  "news_id": ObjectId("5c36f081f315ad4925139794"),
  "lang": "fr",
  "short_description": "",
  "description": "",
  "created_at": ISODate("2019-01-10T07:13:05.472Z"),
  "updated_at": ISODate("2019-04-05T08:48:47.655Z")
});
db.getCollection("news_contents").insert({
  "_id": ObjectId("5c36f081f315ad4925139799"),
  "news_id": ObjectId("5c36f081f315ad4925139794"),
  "lang": "Gr",
  "short_description": "",
  "description": "",
  "created_at": ISODate("2019-01-10T07:13:05.474Z"),
  "updated_at": ISODate("2019-04-05T08:48:47.655Z")
});
db.getCollection("news_contents").insert({
  "_id": ObjectId("5c36f081f315ad492513979b"),
  "news_id": ObjectId("5c36f081f315ad4925139794"),
  "lang": "Sp",
  "short_description": "",
  "description": "",
  "created_at": ISODate("2019-01-10T07:13:05.476Z"),
  "updated_at": ISODate("2019-04-05T08:48:47.655Z")
});
db.getCollection("news_contents").insert({
  "_id": ObjectId("5c36f0a8f315ad49251397a1"),
  "news_id": ObjectId("5c36f0a8f315ad49251397a0"),
  "lang": "en",
  "short_description": "The Model S and X are Tesla’s second and third major lines of electric vehicles, and are higher-priced lines compared to its newest Model 3. The 75 kWh version of the Model S and X that are being retired come with a battery that garners a range of 259 miles for the Model S, and a 237 miles for the Model X.",
  "description": "<p>The Model S and X are Tesla&rsquo;s second and third major lines of electric vehicles, and are higher-priced lines compared to its newest Model 3. The 75 kWh version of the Model S and X that are being retired come with a battery that garners a range of 259 miles for the Model S, and a 237 miles for the Model X.</p>\r\n\r\n<p>Higher-end versions of the S and X, equipped with a 100 kWh battery, will still be made and sold by Tesla, but at a higher price. Following the changes announced by Musk, the starting price for a new Model S will be $94,000 and the starting price for a Model X will be $97,000 from next week.</p>\r\n\r\n<p>Tesla did not respond to inquiries as to why the S and X offerings were pared back, or whether any team reviewed Musk&rsquo;s tweet before he shared it.</p>\r\n\r\n<p>Retiring the lower-range Model S and X vehicles may push prospective customers to buy Tesla&rsquo;s high-end Model 3 sedans instead.</p>\r\n\r\n<p><a href=\"https://www.cnbc.com/2018/05/19/elon-musk-teases-specifications-for-teslas-model-3-calling-it-amazing.html\" target=\"\">A Model 3 with bells and whistles</a>&nbsp;&mdash; like dual-motor all wheel drive, enhanced Autopilot, and a $5,000 performance upgrade package &mdash; will cost around $67,000, according to the company&rsquo;s Model 3 configurator online.</p>\r\n\r\n<p>Reducing the number of variations of Model S and X vehicles that Tesla has to make may help the company control costs, or reallocate materials and labor to other initiatives, such as Model 3 production at its packed car plant in Fremont, California or spare parts production in nearby Lathrop.</p>\r\n",
  "created_at": ISODate("2019-01-10T07:13:44.738Z"),
  "updated_at": ISODate("2019-08-08T22:53:05.553Z")
});
db.getCollection("news_contents").insert({
  "_id": ObjectId("5c36f0a8f315ad49251397a3"),
  "news_id": ObjectId("5c36f0a8f315ad49251397a0"),
  "lang": "fr",
  "short_description": "",
  "description": "",
  "created_at": ISODate("2019-01-10T07:13:44.740Z"),
  "updated_at": ISODate("2019-08-08T22:53:05.553Z")
});
db.getCollection("news_contents").insert({
  "_id": ObjectId("5c36f0a8f315ad49251397a5"),
  "news_id": ObjectId("5c36f0a8f315ad49251397a0"),
  "lang": "Gr",
  "short_description": "",
  "description": "",
  "created_at": ISODate("2019-01-10T07:13:44.743Z"),
  "updated_at": ISODate("2019-08-08T22:53:05.553Z")
});
db.getCollection("news_contents").insert({
  "_id": ObjectId("5c36f0a8f315ad49251397a7"),
  "news_id": ObjectId("5c36f0a8f315ad49251397a0"),
  "lang": "Sp",
  "short_description": "",
  "description": "",
  "created_at": ISODate("2019-01-10T07:13:44.745Z"),
  "updated_at": ISODate("2019-08-08T22:53:05.553Z")
});
db.getCollection("news_contents").insert({
  "_id": ObjectId("5d394dd62a02f42595837220"),
  "news_id": ObjectId("5d394dd62a02f4259583721f"),
  "lang": "en",
  "short_description": "what is this short",
  "description": "<p>and this 2</p>\r\n",
  "created_at": ISODate("2019-07-25T06:36:06.285Z"),
  "updated_at": ISODate("2019-10-02T04:42:55.438Z")
});
db.getCollection("news_contents").insert({
  "_id": ObjectId("5d394dd62a02f42595837222"),
  "news_id": ObjectId("5d394dd62a02f4259583721f"),
  "lang": "fr",
  "short_description": "",
  "description": "",
  "created_at": ISODate("2019-07-25T06:36:06.294Z"),
  "updated_at": ISODate("2019-10-02T04:42:55.438Z")
});
db.getCollection("news_contents").insert({
  "_id": ObjectId("5d394dd62a02f42595837224"),
  "news_id": ObjectId("5d394dd62a02f4259583721f"),
  "lang": "Gr",
  "short_description": "",
  "description": "",
  "created_at": ISODate("2019-07-25T06:36:06.303Z"),
  "updated_at": ISODate("2019-10-02T04:42:55.438Z")
});
db.getCollection("news_contents").insert({
  "_id": ObjectId("5d394dd62a02f42595837226"),
  "news_id": ObjectId("5d394dd62a02f4259583721f"),
  "lang": "Sp",
  "short_description": "",
  "description": "",
  "created_at": ISODate("2019-07-25T06:36:06.312Z"),
  "updated_at": ISODate("2019-10-02T04:42:55.438Z")
});

/** newsletters records **/
db.getCollection("newsletters").insert({
  "_id": ObjectId("5c36ef608ba4672adbd9406c"),
  "email": "anil@gmail.com",
  "status": NumberInt(1),
  "created_at": ISODate("2019-01-10T07:08:16.540Z"),
  "updated_at": ISODate("2019-01-10T07:08:16.540Z")
});

/** order_address records **/
db.getCollection("order_address").insert({
  "_id": ObjectId("5d8dde4d074034097ebc8ae3"),
  "order_id": ObjectId("5d8dde4d074034097ebc8ae2"),
  "billing_fname": "RAJ",
  "billing_lname": "VERMA",
  "billing_phone": "09950192509",
  "billing_email": "verma.rajkishore@gmail.com",
  "billing_add1": "WARD NO 17,",
  "billing_city": "JHUNJHUNU",
  "billing_state": "RAJASTHAN",
  "billing_country": "India",
  "billing_zipcode": "333022",
  "shipping_fname": "RAJ",
  "shipping_lname": "VERMA",
  "shipping_phone": "09950192509",
  "shipping_email": "verma.rajkishore@gmail.com",
  "shipping_add1": "WARD NO 17,",
  "shipping_city": "JHUNJHUNU",
  "shipping_state": "RAJASTHAN",
  "shipping_country": "India",
  "shipping_zipcode": "333022",
  "created_at": ISODate("2019-09-27T10:02:53.470Z"),
  "updated_at": ISODate("2019-09-27T10:02:53.470Z")
});
db.getCollection("order_address").insert({
  "_id": ObjectId("5d8e4bed074034097ebc8b26"),
  "order_id": ObjectId("5d8e4bed074034097ebc8b25"),
  "billing_fname": "mon",
  "billing_lname": "sss",
  "billing_phone": "244121212",
  "billing_email": "monserie@cox.net",
  "billing_add1": "ggg",
  "billing_city": "ddd",
  "billing_state": "az",
  "billing_country": "us",
  "billing_zipcode": "676767",
  "created_at": ISODate("2019-09-27T17:50:37.557Z"),
  "updated_at": ISODate("2019-09-27T17:50:37.557Z")
});
db.getCollection("order_address").insert({
  "_id": ObjectId("5d94f624df71be4b6137f2cb"),
  "order_id": ObjectId("5d94f624df71be4b6137f2ca"),
  "billing_fname": "monica",
  "billing_lname": "serie",
  "billing_phone": "4808291727",
  "billing_email": "monica@litegait.com",
  "billing_add1": "444",
  "billing_city": "tempe",
  "billing_state": "az",
  "billing_country": "us",
  "billing_zipcode": "85282",
  "shipping_fname": "monica",
  "shipping_lname": "serie",
  "shipping_phone": "4808291727",
  "shipping_email": "monica@litegait.com",
  "shipping_add1": "444",
  "shipping_city": "tempe",
  "shipping_state": "az",
  "shipping_country": "us",
  "shipping_zipcode": "85282",
  "created_at": ISODate("2019-10-02T19:10:28.521Z"),
  "updated_at": ISODate("2019-10-02T19:10:28.521Z")
});
db.getCollection("order_address").insert({
  "_id": ObjectId("5da6fe9211ece21ef75969f8"),
  "order_id": ObjectId("5da6fe9211ece21ef75969f7"),
  "billing_fname": "RAJ",
  "billing_lname": "VERMA",
  "billing_phone": "09950192509",
  "billing_email": "verma.rajkishore@gmail.com",
  "billing_add1": "WARD NO 17,",
  "billing_city": "JHUNJHUNU",
  "billing_state": "RAJASTHAN",
  "billing_country": "India",
  "billing_zipcode": "333022",
  "shipping_fname": "RAJ",
  "shipping_lname": "VERMA",
  "shipping_phone": "09950192509",
  "shipping_email": "verma.rajkishore@gmail.com",
  "shipping_add1": "WARD NO 17,",
  "shipping_city": "JHUNJHUNU",
  "shipping_state": "RAJASTHAN",
  "shipping_country": "India",
  "shipping_zipcode": "333022",
  "created_at": ISODate("2019-10-16T11:27:14.783Z"),
  "updated_at": ISODate("2019-10-16T11:27:14.783Z")
});
db.getCollection("order_address").insert({
  "_id": ObjectId("5de8a83d6fb4b60f0f7dd4bf"),
  "order_id": ObjectId("5de8a83d6fb4b60f0f7dd4be"),
  "billing_fname": "Kamal",
  "billing_lname": "Salinas",
  "billing_phone": "+1 (363) 299-7601",
  "billing_email": "daryqawow@mailinator.com",
  "billing_add1": "Quis autem pariatur",
  "billing_city": "Dolor dolorem amet ",
  "billing_state": "Non aliquip debitis ",
  "billing_country": "Ex aliquid eaque ull",
  "billing_zipcode": "36902",
  "shipping_fname": "Yardley",
  "shipping_lname": "Sullivan",
  "shipping_phone": "+1 (621) 116-7135",
  "shipping_email": "rifupeb@mailinator.com",
  "shipping_add1": "Dolorum laborum Odi",
  "shipping_city": "Dolore dolore odio d",
  "shipping_state": "Aliqua Labore beata",
  "shipping_country": "Tenetur qui exceptur",
  "shipping_zipcode": "29074",
  "created_at": ISODate("2019-12-05T06:48:29.515Z"),
  "updated_at": ISODate("2019-12-05T06:48:29.515Z")
});
db.getCollection("order_address").insert({
  "_id": ObjectId("5de8af3e95b70729ea6d4ade"),
  "order_id": ObjectId("5de8af3e95b70729ea6d4add"),
  "billing_fname": "Prescott",
  "billing_lname": "Greene",
  "billing_phone": "+1 (103) 211-5681",
  "billing_email": "viximy@mailinator.net",
  "billing_add1": "Unde Nam qui odio eu",
  "billing_city": "Delectus ex exercit",
  "billing_state": "Quam sit repudiandae",
  "billing_country": "Ut facilis qui sint ",
  "billing_zipcode": "62226",
  "shipping_fname": "Prescott",
  "shipping_lname": "Greene",
  "shipping_phone": "+1 (103) 211-5681",
  "shipping_email": "viximy@mailinator.net",
  "shipping_add1": "Unde Nam qui odio eu",
  "shipping_city": "Delectus ex exercit",
  "shipping_state": "Quam sit repudiandae",
  "shipping_country": "Ut facilis qui sint ",
  "shipping_zipcode": "62226",
  "created_at": ISODate("2019-12-05T07:18:22.122Z"),
  "updated_at": ISODate("2019-12-05T07:18:22.122Z")
});

/** order_products records **/
db.getCollection("order_products").insert({
  "_id": ObjectId("5d6918ac302a6771186cd085"),
  "order_id": ObjectId("5d6918ac302a6771186cd084"),
  "user_id": ObjectId("5badf3695115941ea80970ce"),
  "webinar_id": ObjectId("5c4027e1521f740b1cd20022"),
  "datetime": "date_time_1",
  "type": "webinar",
  "price": NumberInt(0),
  "qty": NumberInt(1),
  "total": NumberInt(0),
  "created_at": ISODate("2019-08-30T12:38:04.707Z"),
  "updated_at": ISODate("2019-08-30T12:38:04.707Z")
});
db.getCollection("order_products").insert({
  "_id": ObjectId("5d6918d2302a6771186cd08e"),
  "order_id": ObjectId("5d6918d2302a6771186cd08d"),
  "user_id": ObjectId("5badf3695115941ea80970ce"),
  "webinar_id": ObjectId("5c86557636c12022fc64521a"),
  "type": "webinar",
  "datetime": "date_time_1",
  "price": NumberInt(0),
  "qty": NumberInt(0),
  "total": NumberInt(0),
  "created_at": ISODate("2019-08-30T12:38:42.695Z"),
  "updated_at": ISODate("2019-08-30T12:38:42.695Z")
});
db.getCollection("order_products").insert({
  "_id": ObjectId("5d8dde4d074034097ebc8ae4"),
  "order_id": ObjectId("5d8dde4d074034097ebc8ae2"),
  "user_id": ObjectId("5d399c94c893b821ec976b99"),
  "part_id": ObjectId("5ccc104f06e67e44fa0eb244"),
  "type": "part",
  "price": NumberInt(2),
  "qty": NumberInt(1),
  "total": NumberInt(2),
  "created_at": ISODate("2019-09-27T10:02:53.498Z"),
  "updated_at": ISODate("2019-09-27T10:02:53.498Z")
});
db.getCollection("order_products").insert({
  "_id": ObjectId("5d8e4bed074034097ebc8b27"),
  "order_id": ObjectId("5d8e4bed074034097ebc8b25"),
  "user_id": ObjectId("5d34842eaee8f14fa304844d"),
  "webinar_id": ObjectId("5c8776632f029d24a8f06320"),
  "datetime": "date_time_1",
  "type": "webinar",
  "price": NumberInt(25),
  "qty": NumberInt(1),
  "total": NumberInt(25),
  "created_at": ISODate("2019-09-27T17:50:37.559Z"),
  "updated_at": ISODate("2019-09-27T17:50:37.559Z")
});
db.getCollection("order_products").insert({
  "_id": ObjectId("5d94f624df71be4b6137f2cc"),
  "order_id": ObjectId("5d94f624df71be4b6137f2ca"),
  "user_id": ObjectId("5c60e6618eaaa63ad1fdc477"),
  "part_id": ObjectId("5ccc104f06e67e44fa0eb244"),
  "type": "part",
  "price": NumberInt(2),
  "qty": NumberInt(1),
  "total": NumberInt(2),
  "created_at": ISODate("2019-10-02T19:10:28.535Z"),
  "updated_at": ISODate("2019-10-02T19:10:28.535Z")
});
db.getCollection("order_products").insert({
  "_id": ObjectId("5d9e2ada7dbdaa69d72e5eef"),
  "order_id": ObjectId("5d9e2ada7dbdaa69d72e5eee"),
  "user_id": ObjectId("5d34842eaee8f14fa304844d"),
  "webinar_id": ObjectId("5d9c155e7dbdaa69d72e5c77"),
  "datetime": "date_time_1",
  "type": "webinar",
  "price": NumberInt(0),
  "qty": NumberInt(1),
  "total": NumberInt(0),
  "created_at": ISODate("2019-10-09T18:45:46.304Z"),
  "updated_at": ISODate("2019-10-09T18:45:46.304Z")
});
db.getCollection("order_products").insert({
  "_id": ObjectId("5d9e2ada7dbdaa69d72e5ef1"),
  "order_id": ObjectId("5d9e2ada7dbdaa69d72e5eee"),
  "user_id": ObjectId("5d34842eaee8f14fa304844d"),
  "webinar_id": ObjectId("5c4027e1521f740b1cd20022"),
  "datetime": "date_time_2",
  "type": "webinar",
  "price": NumberInt(0),
  "qty": NumberInt(1),
  "total": NumberInt(0),
  "created_at": ISODate("2019-10-09T18:45:46.410Z"),
  "updated_at": ISODate("2019-10-09T18:45:46.410Z")
});
db.getCollection("order_products").insert({
  "_id": ObjectId("5d9e3e396b548c52a943a091"),
  "order_id": ObjectId("5d9e3e396b548c52a943a090"),
  "user_id": null,
  "webinar_id": ObjectId("5c88d08d36e25e2118009acb"),
  "type": "webinar",
  "datetime": "date_time_1",
  "price": NumberInt(0),
  "qty": NumberInt(0),
  "total": NumberInt(0),
  "created_at": ISODate("2019-10-09T20:08:25.628Z"),
  "updated_at": ISODate("2019-10-09T20:08:25.628Z")
});
db.getCollection("order_products").insert({
  "_id": ObjectId("5da6fe9211ece21ef75969f9"),
  "order_id": ObjectId("5da6fe9211ece21ef75969f7"),
  "user_id": ObjectId("5d34842eaee8f14fa304844d"),
  "product_id": ObjectId("5ccc103806e67e44fa0eb233"),
  "type": "product",
  "price": NumberInt(3),
  "qty": NumberInt(1),
  "total": NumberInt(3),
  "created_at": ISODate("2019-10-16T11:27:14.789Z"),
  "updated_at": ISODate("2019-10-16T11:27:14.789Z")
});
db.getCollection("order_products").insert({
  "_id": ObjectId("5da6fe9211ece21ef75969fb"),
  "order_id": ObjectId("5da6fe9211ece21ef75969f7"),
  "user_id": ObjectId("5d34842eaee8f14fa304844d"),
  "type": "parts",
  "price": NumberInt(130),
  "qty": NumberInt(1),
  "total": NumberInt(130),
  "created_at": ISODate("2019-10-16T11:27:14.795Z"),
  "updated_at": ISODate("2019-10-16T11:27:14.795Z")
});
db.getCollection("order_products").insert({
  "_id": ObjectId("5de8a83d6fb4b60f0f7dd4c0"),
  "order_id": ObjectId("5de8a83d6fb4b60f0f7dd4be"),
  "user_id": ObjectId("5d34842eaee8f14fa304844d"),
  "type": "parts",
  "price": NumberInt(180),
  "qty": NumberInt(1),
  "total": NumberInt(180),
  "created_at": ISODate("2019-12-05T06:48:29.521Z"),
  "updated_at": ISODate("2019-12-05T06:48:29.521Z")
});
db.getCollection("order_products").insert({
  "_id": ObjectId("5de8af3e95b70729ea6d4adf"),
  "order_id": ObjectId("5de8af3e95b70729ea6d4add"),
  "user_id": ObjectId("5d34842eaee8f14fa304844d"),
  "type": "part",
  "part_id": ObjectId("5c668a8fbc19ad187c839c88"),
  "price": NumberInt(130),
  "qty": NumberInt(1),
  "total": NumberInt(130),
  "created_at": ISODate("2019-12-05T07:18:22.127Z"),
  "updated_at": ISODate("2019-12-05T07:18:22.127Z")
});

/** orders records **/
db.getCollection("orders").insert({
  "_id": ObjectId("5d6918ac302a6771186cd084"),
  "order_product": [
    ObjectId("5d6918ac302a6771186cd085")
  ],
  "order_id": "201908301",
  "user_id": ObjectId("5badf3695115941ea80970ce"),
  "subtotal": NumberInt(0),
  "discount": NumberInt(0),
  "discountcode": "",
  "total": NumberInt(0),
  "status": NumberInt(1),
  "created_at": ISODate("2019-08-30T12:38:04.703Z"),
  "updated_at": ISODate("2019-08-30T12:38:04.710Z")
});
db.getCollection("orders").insert({
  "_id": ObjectId("5d6918d2302a6771186cd08d"),
  "order_product": [
    ObjectId("5d6918d2302a6771186cd08e")
  ],
  "order_id": "201908302",
  "user_id": ObjectId("5badf3695115941ea80970ce"),
  "subtotal": NumberInt(0),
  "discount": NumberInt(0),
  "total": NumberInt(0),
  "status": NumberInt(4),
  "created_at": ISODate("2019-08-30T12:38:42.693Z"),
  "updated_at": ISODate("2019-08-30T12:38:42.697Z")
});
db.getCollection("orders").insert({
  "_id": ObjectId("5d8dde4d074034097ebc8ae2"),
  "order_product": [
    ObjectId("5d8dde4d074034097ebc8ae4")
  ],
  "order_id": "201909271",
  "user_id": ObjectId("5d399c94c893b821ec976b99"),
  "subtotal": NumberInt(2),
  "discount": NumberInt(0),
  "discountcode": "",
  "total": NumberInt(2),
  "payment_method": "paypal",
  "payment_id": "Not Required",
  "status": NumberInt(1),
  "created_at": ISODate("2019-09-27T10:02:53.428Z"),
  "updated_at": ISODate("2019-09-27T10:02:53.499Z"),
  "order_address": ObjectId("5d8dde4d074034097ebc8ae3")
});
db.getCollection("orders").insert({
  "_id": ObjectId("5d8e4bed074034097ebc8b25"),
  "order_product": [
    ObjectId("5d8e4bed074034097ebc8b27")
  ],
  "order_id": "201909272",
  "user_id": ObjectId("5d34842eaee8f14fa304844d"),
  "subtotal": NumberInt(25),
  "discount": NumberInt(0),
  "discountcode": "",
  "total": NumberInt(25),
  "payment_method": "paypal",
  "payment_id": "Not Required",
  "status": NumberInt(1),
  "created_at": ISODate("2019-09-27T17:50:37.555Z"),
  "updated_at": ISODate("2019-09-27T17:50:37.561Z"),
  "order_address": ObjectId("5d8e4bed074034097ebc8b26")
});
db.getCollection("orders").insert({
  "_id": ObjectId("5d94f624df71be4b6137f2ca"),
  "order_product": [
    ObjectId("5d94f624df71be4b6137f2cc")
  ],
  "order_id": "201910031",
  "user_id": ObjectId("5c60e6618eaaa63ad1fdc477"),
  "subtotal": NumberInt(2),
  "discount": NumberInt(2),
  "discountcode": "montest1",
  "total": NumberInt(0),
  "payment_method": "paypal",
  "payment_id": "Not Required",
  "status": NumberInt(1),
  "created_at": ISODate("2019-10-02T19:10:28.511Z"),
  "updated_at": ISODate("2019-10-02T19:10:28.542Z"),
  "order_address": ObjectId("5d94f624df71be4b6137f2cb")
});
db.getCollection("orders").insert({
  "_id": ObjectId("5d9e2ada7dbdaa69d72e5eee"),
  "order_product": [
    ObjectId("5d9e2ada7dbdaa69d72e5eef"),
    ObjectId("5d9e2ada7dbdaa69d72e5ef1")
  ],
  "order_id": "201910101",
  "user_id": ObjectId("5d34842eaee8f14fa304844d"),
  "subtotal": NumberInt(0),
  "discount": NumberInt(0),
  "discountcode": "",
  "total": NumberInt(0),
  "status": NumberInt(1),
  "created_at": ISODate("2019-10-09T18:45:46.26Z"),
  "updated_at": ISODate("2019-10-09T18:45:46.414Z")
});
db.getCollection("orders").insert({
  "_id": ObjectId("5d9e3e396b548c52a943a090"),
  "order_product": [
    ObjectId("5d9e3e396b548c52a943a091")
  ],
  "order_id": "201910102",
  "user_id": null,
  "subtotal": NumberInt(0),
  "discount": NumberInt(0),
  "total": NumberInt(0),
  "status": NumberInt(4),
  "created_at": ISODate("2019-10-09T20:08:25.624Z"),
  "updated_at": ISODate("2019-10-09T20:08:25.633Z")
});
db.getCollection("orders").insert({
  "_id": ObjectId("5da6fe9211ece21ef75969f7"),
  "order_product": [
    ObjectId("5da6fe9211ece21ef75969f9"),
    ObjectId("5da6fe9211ece21ef75969fb")
  ],
  "order_id": "201910161",
  "user_id": ObjectId("5d34842eaee8f14fa304844d"),
  "subtotal": NumberInt(133),
  "discount": NumberInt(0),
  "discountcode": "",
  "total": NumberInt(133),
  "payment_method": "paypal",
  "payment_id": "Not Required",
  "status": NumberInt(1),
  "created_at": ISODate("2019-10-16T11:27:14.778Z"),
  "updated_at": ISODate("2019-10-16T11:27:14.799Z"),
  "order_address": ObjectId("5da6fe9211ece21ef75969f8")
});
db.getCollection("orders").insert({
  "_id": ObjectId("5de8a83d6fb4b60f0f7dd4be"),
  "order_product": [
    ObjectId("5de8a83d6fb4b60f0f7dd4c0")
  ],
  "order_id": "201912051",
  "user_id": ObjectId("5d34842eaee8f14fa304844d"),
  "subtotal": NumberInt(180),
  "discount": NumberInt(18),
  "discountcode": "PART10",
  "total": NumberInt(162),
  "payment_method": "paypal",
  "payment_id": "Not Required",
  "status": NumberInt(1),
  "created_at": ISODate("2019-12-05T06:48:29.508Z"),
  "updated_at": ISODate("2019-12-05T06:48:29.526Z"),
  "order_address": ObjectId("5de8a83d6fb4b60f0f7dd4bf")
});
db.getCollection("orders").insert({
  "_id": ObjectId("5de8af3e95b70729ea6d4add"),
  "order_product": [
    ObjectId("5de8af3e95b70729ea6d4adf")
  ],
  "order_id": "201912052",
  "user_id": ObjectId("5d34842eaee8f14fa304844d"),
  "subtotal": NumberInt(130),
  "discount": NumberInt(0),
  "discountcode": "",
  "total": NumberInt(130),
  "payment_method": "paypal",
  "payment_id": "Not Required",
  "status": NumberInt(1),
  "created_at": ISODate("2019-12-05T07:18:22.118Z"),
  "updated_at": ISODate("2019-12-05T07:18:22.130Z"),
  "order_address": ObjectId("5de8af3e95b70729ea6d4ade")
});

/** part_contents records **/
db.getCollection("part_contents").insert({
  "_id": ObjectId("5c668a67bc19ad187c839c7d"),
  "part_id": ObjectId("5c668a67bc19ad187c839c7c"),
  "lang": "en",
  "short_description": "<p>24V SLA Battery 1</p>\r\n",
  "description": "<p>Applicable Models:<br />\r\nLiteGait I 200E</p>\r\n\r\n<p><strong>Ground Shipping included in price for contiguous United States.</strong>&nbsp;F O B Tempe, Arizona.</p>\r\n\r\n<p>For expedited shipping or orders to Hawaii, Alaska or outside of the United States, please contact the Service &amp; Parts Department at 1-800-332-9255 Option 1.</p>\r\n\r\n<p>Purchase orders may be submitted via email to&nbsp;<a href=\"mailto:Orders@LiteGait.com\">Orders@LiteGait.com</a>.</p>\r\n",
  "created_at": ISODate("2019-02-15T09:46:15.824Z"),
  "updated_at": ISODate("2019-09-11T04:47:49.742Z")
});
db.getCollection("part_contents").insert({
  "_id": ObjectId("5c668a67bc19ad187c839c7f"),
  "part_id": ObjectId("5c668a67bc19ad187c839c7c"),
  "lang": "fr",
  "short_description": "",
  "description": "",
  "created_at": ISODate("2019-02-15T09:46:15.826Z"),
  "updated_at": ISODate("2019-09-11T04:47:49.742Z")
});
db.getCollection("part_contents").insert({
  "_id": ObjectId("5c668a67bc19ad187c839c81"),
  "part_id": ObjectId("5c668a67bc19ad187c839c7c"),
  "lang": "Gr",
  "short_description": "",
  "description": "",
  "created_at": ISODate("2019-02-15T09:46:15.828Z"),
  "updated_at": ISODate("2019-09-11T04:47:49.742Z")
});
db.getCollection("part_contents").insert({
  "_id": ObjectId("5c668a67bc19ad187c839c83"),
  "part_id": ObjectId("5c668a67bc19ad187c839c7c"),
  "lang": "Sp",
  "short_description": "",
  "description": "",
  "created_at": ISODate("2019-02-15T09:46:15.831Z"),
  "updated_at": ISODate("2019-09-11T04:47:49.742Z")
});
db.getCollection("part_contents").insert({
  "_id": ObjectId("5c668a8fbc19ad187c839c89"),
  "part_id": ObjectId("5c668a8fbc19ad187c839c88"),
  "lang": "en",
  "short_description": "<p>Single Channel Control Box for LiteGait&nbsp;I 200P (Version A &amp; B)</p>\r\n",
  "description": "<p>Applicable Models:<br />\r\nLiteGait I 200P</p>\r\n\r\n<p>Ground Shipping included in price for contiguous United States. FOB Tempe, Arizona.</p>\r\n\r\n<p>For expedited shipping or orders to Hawaii, Alaska or outside of the United States, please contact the Service &amp; Parts Department at 1-800-332-9255 Option 1.</p>\r\n\r\n<p>Purchase orders may be submitted via email to&nbsp;<a href=\"mailto:Orders@LiteGait.com\">Orders@LiteGait.com</a>.</p>\r\n",
  "created_at": ISODate("2019-02-15T09:46:55.94Z"),
  "updated_at": ISODate("2019-09-12T03:53:39.947Z")
});
db.getCollection("part_contents").insert({
  "_id": ObjectId("5c668a8fbc19ad187c839c8b"),
  "part_id": ObjectId("5c668a8fbc19ad187c839c88"),
  "lang": "fr",
  "short_description": "",
  "description": "",
  "created_at": ISODate("2019-02-15T09:46:55.97Z"),
  "updated_at": ISODate("2019-09-12T03:53:39.947Z")
});
db.getCollection("part_contents").insert({
  "_id": ObjectId("5c668a8fbc19ad187c839c8d"),
  "part_id": ObjectId("5c668a8fbc19ad187c839c88"),
  "lang": "Gr",
  "short_description": "",
  "description": "",
  "created_at": ISODate("2019-02-15T09:46:55.99Z"),
  "updated_at": ISODate("2019-09-12T03:53:39.947Z")
});
db.getCollection("part_contents").insert({
  "_id": ObjectId("5c668a8fbc19ad187c839c8f"),
  "part_id": ObjectId("5c668a8fbc19ad187c839c88"),
  "lang": "Sp",
  "short_description": "",
  "description": "",
  "created_at": ISODate("2019-02-15T09:46:55.102Z"),
  "updated_at": ISODate("2019-09-12T03:53:39.947Z")
});
db.getCollection("part_contents").insert({
  "_id": ObjectId("5c78f4ff8592290d03e2000c"),
  "part_id": ObjectId("5c78f4ff8592290d03e2000b"),
  "lang": "en",
  "short_description": "<p>Objective measurement provided by the amount of LiteGait support</p>\r\n",
  "description": "<p>One of our most popular upgrades is the new Tablet BiSym*, a unique option available with LiteGait&reg; to measure unilateral or bilateral support. LiteGait&reg;&rsquo;s special design includes a two-armed yoke which holds the patient above each shoulder. From these two points, the harness system can be adjusted to provide as much or as little support required to each side of the body. The range of support can vary on each side from full to no support.</p>\r\n\r\n<p>Sensors measure the weight bearing load changes from the right to left side during the gait cycle.&nbsp;The&nbsp;BiSym&nbsp;monitor&nbsp;displays the value of support provided by LiteGait&reg;&nbsp;in the selected weight units or a percentage of the patient&#39;s body weight.&nbsp;Toggle between weight and percentage by a touch of a button. These real time measurements can be used as biofeedback for the patient. The more weight bearing by the patient = better posture = less work by machine = smaller BiSym reading.</p>\r\n",
  "created_at": ISODate("2019-03-01T09:01:51.385Z"),
  "updated_at": ISODate("2019-09-27T22:53:10.563Z")
});
db.getCollection("part_contents").insert({
  "_id": ObjectId("5c78f4ff8592290d03e2000e"),
  "part_id": ObjectId("5c78f4ff8592290d03e2000b"),
  "lang": "fr",
  "short_description": "",
  "description": "",
  "created_at": ISODate("2019-03-01T09:01:51.388Z"),
  "updated_at": ISODate("2019-09-27T22:53:10.563Z")
});
db.getCollection("part_contents").insert({
  "_id": ObjectId("5c78f4ff8592290d03e20010"),
  "part_id": ObjectId("5c78f4ff8592290d03e2000b"),
  "lang": "Gr",
  "short_description": "",
  "description": "",
  "created_at": ISODate("2019-03-01T09:01:51.390Z"),
  "updated_at": ISODate("2019-09-27T22:53:10.563Z")
});
db.getCollection("part_contents").insert({
  "_id": ObjectId("5c78f4ff8592290d03e20012"),
  "part_id": ObjectId("5c78f4ff8592290d03e2000b"),
  "lang": "Sp",
  "short_description": "",
  "description": "",
  "created_at": ISODate("2019-03-01T09:01:51.392Z"),
  "updated_at": ISODate("2019-09-27T22:53:10.563Z")
});
db.getCollection("part_contents").insert({
  "_id": ObjectId("5cb660962a31271b4165f3c6"),
  "part_id": ObjectId("5cb660962a31271b4165f3c5"),
  "lang": "en",
  "short_description": "<p>24V SLA Battery</p>\r\n",
  "description": "<p>Rechargeable Battery for PS35E</p>\r\n\r\n<p>Applicable Models:<br />\r\nLiteGait&nbsp;I 360E (A)&nbsp; |&nbsp; LiteGait&nbsp;I 400E/ES&nbsp;&nbsp;|&nbsp; LiteGait I 500E/ES</p>\r\n\r\n<p>LiteGait is equipped with a 24 volt battery pack that needs to be charged <strong>overnight at least one per week.</strong>&nbsp; It is important to follow a weekly charging schedule to maximize the lifetime of the battery</p>\r\n\r\n<p><strong>Ground Shipping included in price for contiguous United States.</strong>&nbsp;FOB Tempe, Arizona.</p>\r\n\r\n<p>For expedited shipping or orders to Hawaii, Alaska or outside of the United States, please contact the Service &amp; Parts Department at 1-800-332-9255 Option 1</p>\r\n",
  "created_at": ISODate("2019-04-16T23:09:10.887Z"),
  "updated_at": ISODate("2019-09-11T04:48:37.903Z")
});
db.getCollection("part_contents").insert({
  "_id": ObjectId("5cb660962a31271b4165f3c8"),
  "part_id": ObjectId("5cb660962a31271b4165f3c5"),
  "lang": "fr",
  "short_description": "",
  "description": "",
  "created_at": ISODate("2019-04-16T23:09:10.891Z"),
  "updated_at": ISODate("2019-09-11T04:48:37.903Z")
});
db.getCollection("part_contents").insert({
  "_id": ObjectId("5cb660962a31271b4165f3ca"),
  "part_id": ObjectId("5cb660962a31271b4165f3c5"),
  "lang": "Gr",
  "short_description": "",
  "description": "",
  "created_at": ISODate("2019-04-16T23:09:10.893Z"),
  "updated_at": ISODate("2019-09-11T04:48:37.903Z")
});
db.getCollection("part_contents").insert({
  "_id": ObjectId("5cb660962a31271b4165f3cc"),
  "part_id": ObjectId("5cb660962a31271b4165f3c5"),
  "lang": "Sp",
  "short_description": "",
  "description": "",
  "created_at": ISODate("2019-04-16T23:09:10.895Z"),
  "updated_at": ISODate("2019-09-11T04:48:37.903Z")
});
db.getCollection("part_contents").insert({
  "_id": ObjectId("5ccc104f06e67e44fa0eb246"),
  "part_id": ObjectId("5ccc104f06e67e44fa0eb244"),
  "lang": "en",
  "short_description": "<p>Hand Switch</p>\r\n",
  "description": "<p>Hand SwitchHand SwitchHand SwitchHand SwitchHand SwitchHand SwitchHand SwitchHand SwitchHand SwitchHand SwitchHand SwitchHand SwitchHand SwitchHand SwitchHand SwitchHand SwitchHand SwitchHand SwitchHand SwitchHand SwitchHand SwitchHand SwitchHand SwitchHand SwitchHand Switch</p>\r\n",
  "created_at": ISODate("2019-03-01T09:01:51.385Z"),
  "updated_at": ISODate("2019-09-09T16:01:22.505Z")
});
db.getCollection("part_contents").insert({
  "_id": ObjectId("5ccc104f06e67e44fa0eb249"),
  "part_id": ObjectId("5ccc104f06e67e44fa0eb244"),
  "lang": "fr",
  "short_description": "",
  "description": "",
  "created_at": ISODate("2019-03-01T09:01:51.388Z"),
  "updated_at": ISODate("2019-09-09T16:01:22.505Z")
});
db.getCollection("part_contents").insert({
  "_id": ObjectId("5ccc104f06e67e44fa0eb24d"),
  "part_id": ObjectId("5ccc104f06e67e44fa0eb244"),
  "lang": "Gr",
  "short_description": "",
  "description": "",
  "created_at": ISODate("2019-03-01T09:01:51.390Z"),
  "updated_at": ISODate("2019-09-09T16:01:22.505Z")
});
db.getCollection("part_contents").insert({
  "_id": ObjectId("5ccc104f06e67e44fa0eb250"),
  "part_id": ObjectId("5ccc104f06e67e44fa0eb244"),
  "lang": "Sp",
  "short_description": "",
  "description": "",
  "created_at": ISODate("2019-03-01T09:01:51.392Z"),
  "updated_at": ISODate("2019-09-09T16:01:22.505Z")
});
db.getCollection("part_contents").insert({
  "_id": ObjectId("5ccc131106e67e44fa0eb275"),
  "part_id": ObjectId("5ccc131106e67e44fa0eb273"),
  "lang": "en",
  "short_description": "<p>Set of 2 rechargeable batteries</p>\r\n",
  "description": "<p>Rechargeable batteries&nbsp;</p>\r\n\r\n<p>Applicable Models:<br />\r\nLiteGait I&nbsp;w/PSCC Power System (set of 2)</p>\r\n\r\n<p>LiteGait is equipped with a 24 volt battery pack that needs to be charged <strong>overnight at least one per week.</strong>&nbsp; It is important to follow a weekly charging schedule to maximize the lifetime of the battery</p>\r\n\r\n<p><strong>Ground Shipping included in price for contiguous United States.</strong>&nbsp;FOB Tempe, Arizona.</p>\r\n\r\n<p>For expedited shipping or orders to Hawaii, Alaska or outside of the United States, please contact the Service &amp; Parts Department at 1-800-332-9255 Option 1</p>\r\n",
  "created_at": ISODate("2019-04-16T23:09:10.887Z"),
  "updated_at": ISODate("2019-09-11T15:26:51.476Z")
});
db.getCollection("part_contents").insert({
  "_id": ObjectId("5ccc131106e67e44fa0eb279"),
  "part_id": ObjectId("5ccc131106e67e44fa0eb273"),
  "lang": "fr",
  "short_description": "",
  "description": "",
  "created_at": ISODate("2019-04-16T23:09:10.891Z"),
  "updated_at": ISODate("2019-09-11T15:26:51.476Z")
});
db.getCollection("part_contents").insert({
  "_id": ObjectId("5ccc131106e67e44fa0eb27c"),
  "part_id": ObjectId("5ccc131106e67e44fa0eb273"),
  "lang": "Gr",
  "short_description": "",
  "description": "",
  "created_at": ISODate("2019-04-16T23:09:10.893Z"),
  "updated_at": ISODate("2019-09-11T15:26:51.476Z")
});
db.getCollection("part_contents").insert({
  "_id": ObjectId("5ccc131106e67e44fa0eb27f"),
  "part_id": ObjectId("5ccc131106e67e44fa0eb273"),
  "lang": "Sp",
  "short_description": "",
  "description": "",
  "created_at": ISODate("2019-04-16T23:09:10.895Z"),
  "updated_at": ISODate("2019-09-11T15:26:51.476Z")
});
db.getCollection("part_contents").insert({
  "_id": ObjectId("5d7878eaf3c6d306896b26f6"),
  "part_id": ObjectId("5d7878eaf3c6d306896b26f5"),
  "lang": "en",
  "short_description": "<p>24V SLA Battery</p>\r\n\r\n<p>Rechargeable Battery&nbsp;</p>\r\n\r\n<p>Applicable Models:<br />\r\nLiteGait&nbsp;I 200P</p>\r\n\r\n<p>LiteGait is equipped with a 24 volt battery pack that needs to be charged <strong>overnight at least one per week.</strong>&nbsp; It is important to follow a weekly charging schedule to maximize the lifetime&nbsp;of the battery</p>\r\n\r\n<p>&nbsp;</p>\r\n",
  "description": "<p><strong>Ground Shipping included in price for contiguous United States.</strong>&nbsp;FOB Tempe, Arizona.</p>\r\n\r\n<p>For expedited shipping or orders to Hawaii, Alaska or outside of the United States, please contact the Service &amp; Parts Department at 1-800-332-9255 Option 1</p>\r\n",
  "created_at": ISODate("2019-09-11T04:32:42.528Z"),
  "updated_at": ISODate("2019-10-10T05:26:53.804Z")
});
db.getCollection("part_contents").insert({
  "_id": ObjectId("5d7878eaf3c6d306896b26f8"),
  "part_id": ObjectId("5d7878eaf3c6d306896b26f5"),
  "lang": "fr",
  "short_description": "",
  "description": "",
  "created_at": ISODate("2019-09-11T04:32:42.531Z"),
  "updated_at": ISODate("2019-10-10T05:26:53.804Z")
});
db.getCollection("part_contents").insert({
  "_id": ObjectId("5d7878eaf3c6d306896b26fa"),
  "part_id": ObjectId("5d7878eaf3c6d306896b26f5"),
  "lang": "Gr",
  "short_description": "",
  "description": "",
  "created_at": ISODate("2019-09-11T04:32:42.532Z"),
  "updated_at": ISODate("2019-10-10T05:26:53.804Z")
});
db.getCollection("part_contents").insert({
  "_id": ObjectId("5d7878eaf3c6d306896b26fc"),
  "part_id": ObjectId("5d7878eaf3c6d306896b26f5"),
  "lang": "Sp",
  "short_description": "",
  "description": "",
  "created_at": ISODate("2019-09-11T04:32:42.534Z"),
  "updated_at": ISODate("2019-10-10T05:26:53.804Z")
});
db.getCollection("part_contents").insert({
  "_id": ObjectId("5d787a65f3c6d306896b270b"),
  "part_id": ObjectId("5d787a65f3c6d306896b2709"),
  "lang": "en",
  "short_description": "<p>24V SLA Battery</p>\r\n",
  "description": "<p>Rechargeable Battery for PS35E</p>\r\n\r\n<p>Applicable Models:<br />\r\nLiteGait&nbsp;I 200P | LiteGait&nbsp;I 260E | LiteGait I 300E | LiteGait I 300MX (Version B) |&nbsp;LiteGait I 200P<br />\r\nLiteGait I 250E |&nbsp;LiteGait&nbsp;I 350​ | LiteGait I 350E</p>\r\n\r\n<p>LiteGait is equipped with a 24 volt battery pack that needs to be charged <strong>overnight at least one per week.</strong>&nbsp; It is important to follow a weekly charging schedule to maximize the lifetime of the battery</p>\r\n\r\n<p><strong>Ground Shipping included in price for contiguous United States.</strong>&nbsp;FOB Tempe, Arizona.</p>\r\n\r\n<p>For expedited shipping or orders to Hawaii, Alaska or outside of the United States, please contact the Service &amp; Parts Department at 1-800-332-9255 Option 1</p>\r\n",
  "created_at": ISODate("2019-09-11T04:32:42.528Z"),
  "updated_at": ISODate("2019-09-11T15:12:27.638Z")
});
db.getCollection("part_contents").insert({
  "_id": ObjectId("5d787a65f3c6d306896b270e"),
  "part_id": ObjectId("5d787a65f3c6d306896b2709"),
  "lang": "fr",
  "short_description": "",
  "description": "",
  "created_at": ISODate("2019-09-11T04:32:42.531Z"),
  "updated_at": ISODate("2019-09-11T15:12:27.638Z")
});
db.getCollection("part_contents").insert({
  "_id": ObjectId("5d787a65f3c6d306896b2711"),
  "part_id": ObjectId("5d787a65f3c6d306896b2709"),
  "lang": "Gr",
  "short_description": "",
  "description": "",
  "created_at": ISODate("2019-09-11T04:32:42.532Z"),
  "updated_at": ISODate("2019-09-11T15:12:27.638Z")
});
db.getCollection("part_contents").insert({
  "_id": ObjectId("5d787a65f3c6d306896b2714"),
  "part_id": ObjectId("5d787a65f3c6d306896b2709"),
  "lang": "Sp",
  "short_description": "",
  "description": "",
  "created_at": ISODate("2019-09-11T04:32:42.534Z"),
  "updated_at": ISODate("2019-09-11T15:12:27.638Z")
});
db.getCollection("part_contents").insert({
  "_id": ObjectId("5d7ad71ff3c6d306896b28d1"),
  "part_id": ObjectId("5d7ad71ff3c6d306896b28d0"),
  "lang": "en",
  "short_description": "<p>Allows patient freedom of full 360&deg; rotation and locks in any position</p>\r\n",
  "description": "<p>The FreeDome* is an accessory that allows the patient/user to freely rotate in place beneath the yoke while remaining securely supported by the LiteGait&reg; harness.<br />\r\nFreeDome can easily be attached or removed from all LiteGait models. The FreeDome can be locked in any position allowing patient to walk sideways or backward on or off the treadmill, or simply walk with assistance when facing out of the LiteGai</p>\r\n",
  "created_at": ISODate("2019-09-12T23:39:11.655Z"),
  "updated_at": ISODate("2019-09-26T04:34:56.658Z")
});
db.getCollection("part_contents").insert({
  "_id": ObjectId("5d7ad71ff3c6d306896b28d3"),
  "part_id": ObjectId("5d7ad71ff3c6d306896b28d0"),
  "lang": "fr",
  "short_description": "",
  "description": "",
  "created_at": ISODate("2019-09-12T23:39:11.658Z"),
  "updated_at": ISODate("2019-09-26T04:34:56.658Z")
});
db.getCollection("part_contents").insert({
  "_id": ObjectId("5d7ad71ff3c6d306896b28d5"),
  "part_id": ObjectId("5d7ad71ff3c6d306896b28d0"),
  "lang": "Gr",
  "short_description": "",
  "description": "",
  "created_at": ISODate("2019-09-12T23:39:11.660Z"),
  "updated_at": ISODate("2019-09-26T04:34:56.658Z")
});
db.getCollection("part_contents").insert({
  "_id": ObjectId("5d7ad71ff3c6d306896b28d7"),
  "part_id": ObjectId("5d7ad71ff3c6d306896b28d0"),
  "lang": "Sp",
  "short_description": "",
  "description": "",
  "created_at": ISODate("2019-09-12T23:39:11.662Z"),
  "updated_at": ISODate("2019-09-26T04:34:56.658Z")
});
db.getCollection("part_contents").insert({
  "_id": ObjectId("5d7ada2af3c6d306896b28df"),
  "part_id": ObjectId("5d7ada2af3c6d306896b28de"),
  "lang": "en",
  "short_description": "<p>Integrated FreeDome paired with our&nbsp;<strong><a href=\"https://www.litegait.com/products/bisym-scale\" target=\"_self\">BiSym&nbsp;</a></strong>Tablet provides a real time display of the patient weight supported by the LiteGait&reg; on the Left, Right and Total</p>\r\n",
  "description": "<p>Integrated FreeDome paired with our&nbsp;<strong><a href=\"https://www.litegait.com/products/bisym-scale\" target=\"_self\">BiSym&nbsp;</a></strong>Tablet provides a real time display of the patient weight supported by the LiteGait&reg; on the Left, Right and Total.&nbsp;FreeDome allows the individual to freely rotate in place while remaining securely supported by the LiteGait. FreeDome can be locked into any position, allowing users to walk sideways with braiding, backward, on or off the treadmill, or simply walk with assistance when facing away from the LiteGait column.</p>\r\n",
  "created_at": ISODate("2019-09-12T23:52:10.926Z"),
  "updated_at": ISODate("2019-10-02T22:32:17.41Z")
});
db.getCollection("part_contents").insert({
  "_id": ObjectId("5d7ada2af3c6d306896b28e1"),
  "part_id": ObjectId("5d7ada2af3c6d306896b28de"),
  "lang": "fr",
  "short_description": "",
  "description": "",
  "created_at": ISODate("2019-09-12T23:52:10.928Z"),
  "updated_at": ISODate("2019-10-02T22:32:17.41Z")
});
db.getCollection("part_contents").insert({
  "_id": ObjectId("5d7ada2af3c6d306896b28e3"),
  "part_id": ObjectId("5d7ada2af3c6d306896b28de"),
  "lang": "Gr",
  "short_description": "",
  "description": "",
  "created_at": ISODate("2019-09-12T23:52:10.931Z"),
  "updated_at": ISODate("2019-10-02T22:32:17.41Z")
});
db.getCollection("part_contents").insert({
  "_id": ObjectId("5d7ada2af3c6d306896b28e5"),
  "part_id": ObjectId("5d7ada2af3c6d306896b28de"),
  "lang": "Sp",
  "short_description": "",
  "description": "",
  "created_at": ISODate("2019-09-12T23:52:10.933Z"),
  "updated_at": ISODate("2019-10-02T22:32:17.41Z")
});

/** part_gallery records **/
db.getCollection("part_gallery").insert({
  "_id": ObjectId("5d8ddbd4074034097ebc8ac6"),
  "part_id": ObjectId("5d7ada2af3c6d306896b28de"),
  "image": "/GKS20_01.jpg",
  "created_at": ISODate("2019-09-27T09:52:20.342Z"),
  "updated_at": ISODate("2019-09-27T09:52:20.342Z")
});
db.getCollection("part_gallery").insert({
  "_id": ObjectId("5d8ddbd4074034097ebc8ac5"),
  "part_id": ObjectId("5d7ada2af3c6d306896b28de"),
  "image": "/Home_featuired_img.png",
  "created_at": ISODate("2019-09-27T09:52:20.341Z"),
  "updated_at": ISODate("2019-09-27T09:52:20.341Z")
});

/** parts records **/
db.getCollection("parts").insert({
  "_id": ObjectId("5c668a67bc19ad187c839c7c"),
  "type": "parts",
  "lang_content": [
    ObjectId("5c668a67bc19ad187c839c7d"),
    ObjectId("5c668a67bc19ad187c839c7f"),
    ObjectId("5c668a67bc19ad187c839c81"),
    ObjectId("5c668a67bc19ad187c839c83")
  ],
  "category": ObjectId("5c6689bfbc19ad187c839c60"),
  "name": "PS20E-B",
  "slug": "ps20e-b",
  "sno": "PS12345",
  "image": "/parts/Batteries/PS20E-B.jpg",
  "short_description": "<p>24V SLA Battery 1</p>\r\n",
  "description": "<p>Applicable Models:<br />\r\nLiteGait I 200E</p>\r\n\r\n<p><strong>Ground Shipping included in price for contiguous United States.</strong>&nbsp;F O B Tempe, Arizona.</p>\r\n\r\n<p>For expedited shipping or orders to Hawaii, Alaska or outside of the United States, please contact the Service &amp; Parts Department at 1-800-332-9255 Option 1.</p>\r\n\r\n<p>Purchase orders may be submitted via email to&nbsp;<a href=\"mailto:Orders@LiteGait.com\">Orders@LiteGait.com</a>.</p>\r\n",
  "price": NumberInt(180),
  "price_request": NumberInt(0),
  "meta_title": "",
  "meta_description": "",
  "status": NumberInt(1),
  "related_product": [
    ""
  ],
  "created_at": ISODate("2019-02-15T09:46:15.822Z"),
  "updated_at": ISODate("2019-09-11T04:47:49.737Z"),
  "product_id": null,
  "schedule_service": NumberInt(0),
  "position": NumberInt(1),
  "testimonials": null,
  "banner_image": ""
});
db.getCollection("parts").insert({
  "_id": ObjectId("5c668a8fbc19ad187c839c88"),
  "type": "parts",
  "lang_content": [
    ObjectId("5c668a8fbc19ad187c839c89"),
    ObjectId("5c668a8fbc19ad187c839c8b"),
    ObjectId("5c668a8fbc19ad187c839c8d"),
    ObjectId("5c668a8fbc19ad187c839c8f")
  ],
  "category": ObjectId("5c6689d2bc19ad187c839c6c"),
  "name": "PS20P-C",
  "slug": "ps20p-c",
  "sno": "C2546",
  "image": "/parts/Control_Box/PS20P-C.jpg",
  "short_description": "<p>Single Channel Control Box for LiteGait&nbsp;I 200P (Version A &amp; B)</p>\r\n",
  "description": "<p>Applicable Models:<br />\r\nLiteGait I 200P</p>\r\n\r\n<p>Ground Shipping included in price for contiguous United States. FOB Tempe, Arizona.</p>\r\n\r\n<p>For expedited shipping or orders to Hawaii, Alaska or outside of the United States, please contact the Service &amp; Parts Department at 1-800-332-9255 Option 1.</p>\r\n\r\n<p>Purchase orders may be submitted via email to&nbsp;<a href=\"mailto:Orders@LiteGait.com\">Orders@LiteGait.com</a>.</p>\r\n",
  "price": NumberInt(130),
  "price_request": NumberInt(0),
  "meta_title": "",
  "meta_description": "",
  "status": NumberInt(1),
  "related_product": [
    "5c668a67bc19ad187c839c7c"
  ],
  "created_at": ISODate("2019-02-15T09:46:55.92Z"),
  "updated_at": ISODate("2019-09-12T03:53:39.942Z"),
  "product_id": null,
  "schedule_service": NumberInt(1),
  "position": NumberInt(1),
  "testimonials": [
    ObjectId("5bd16ea2b605151b7e93d894")
  ],
  "banner_image": ""
});
db.getCollection("parts").insert({
  "_id": ObjectId("5c78f4ff8592290d03e2000b"),
  "lang_content": [
    ObjectId("5c78f4ff8592290d03e2000c"),
    ObjectId("5c78f4ff8592290d03e2000e"),
    ObjectId("5c78f4ff8592290d03e20010"),
    ObjectId("5c78f4ff8592290d03e20012")
  ],
  "type": "accessories",
  "product_id": [
    ObjectId("5c24667cd4568d68ad108584"),
    ObjectId("5cd198ce06e67e44fa0eb2e4"),
    ObjectId("5cd1a2bb06e67e44fa0eb2f8")
  ],
  "category": ObjectId("5d7ad53ff3c6d306896b28b4"),
  "name": "BiSym",
  "slug": "bisym",
  "sno": "",
  "image": "/accessories_img1.jpg",
  "short_description": "<p>Objective measurement provided by the amount of LiteGait support</p>\r\n",
  "description": "<p>One of our most popular upgrades is the new Tablet BiSym*, a unique option available with LiteGait&reg; to measure unilateral or bilateral support. LiteGait&reg;&rsquo;s special design includes a two-armed yoke which holds the patient above each shoulder. From these two points, the harness system can be adjusted to provide as much or as little support required to each side of the body. The range of support can vary on each side from full to no support.</p>\r\n\r\n<p>Sensors measure the weight bearing load changes from the right to left side during the gait cycle.&nbsp;The&nbsp;BiSym&nbsp;monitor&nbsp;displays the value of support provided by LiteGait&reg;&nbsp;in the selected weight units or a percentage of the patient&#39;s body weight.&nbsp;Toggle between weight and percentage by a touch of a button. These real time measurements can be used as biofeedback for the patient. The more weight bearing by the patient = better posture = less work by machine = smaller BiSym reading.</p>\r\n",
  "price": null,
  "price_request": NumberInt(1),
  "schedule_service": NumberInt(0),
  "meta_title": "",
  "meta_description": "",
  "status": NumberInt(1),
  "related_product": [
    ""
  ],
  "created_at": ISODate("2019-03-01T09:01:51.384Z"),
  "updated_at": ISODate("2019-09-27T22:53:10.558Z"),
  "banner_image": "",
  "position": NumberInt(4),
  "testimonials": null
});
db.getCollection("parts").insert({
  "_id": ObjectId("5cb660962a31271b4165f3c5"),
  "lang_content": [
    ObjectId("5cb660962a31271b4165f3c6"),
    ObjectId("5cb660962a31271b4165f3c8"),
    ObjectId("5cb660962a31271b4165f3ca"),
    ObjectId("5cb660962a31271b4165f3cc")
  ],
  "type": "parts",
  "product_id": [
    ObjectId("5c24667cd4568d68ad108584")
  ],
  "category": ObjectId("5c6689bfbc19ad187c839c60"),
  "name": "PS50E-B",
  "slug": "ps50e-b",
  "sno": " PS50E-B123456",
  "image": "/parts/Batteries/PS50E-B.jpg",
  "short_description": "<p>24V SLA Battery</p>\r\n",
  "description": "<p>Rechargeable Battery for PS35E</p>\r\n\r\n<p>Applicable Models:<br />\r\nLiteGait&nbsp;I 360E (A)&nbsp; |&nbsp; LiteGait&nbsp;I 400E/ES&nbsp;&nbsp;|&nbsp; LiteGait I 500E/ES</p>\r\n\r\n<p>LiteGait is equipped with a 24 volt battery pack that needs to be charged <strong>overnight at least one per week.</strong>&nbsp; It is important to follow a weekly charging schedule to maximize the lifetime of the battery</p>\r\n\r\n<p><strong>Ground Shipping included in price for contiguous United States.</strong>&nbsp;FOB Tempe, Arizona.</p>\r\n\r\n<p>For expedited shipping or orders to Hawaii, Alaska or outside of the United States, please contact the Service &amp; Parts Department at 1-800-332-9255 Option 1</p>\r\n",
  "price": NumberInt(260),
  "price_request": NumberInt(0),
  "schedule_service": NumberInt(0),
  "meta_title": "",
  "meta_description": "",
  "status": NumberInt(1),
  "related_product": [
    ""
  ],
  "created_at": ISODate("2019-04-16T23:09:10.834Z"),
  "updated_at": ISODate("2019-09-11T04:48:37.896Z"),
  "position": NumberInt(4),
  "testimonials": [
    ObjectId("5bd16ea2b605151b7e93d894"),
    ObjectId("5c36edb38ba4672adbd9402e"),
    ObjectId("5c36ef538ba4672adbd94067")
  ],
  "banner_image": ""
});
db.getCollection("parts").insert({
  "_id": ObjectId("5ccc104f06e67e44fa0eb244"),
  "product_id": [
    ObjectId("5bd16edbb605151b7e93d899")
  ],
  "lang_content": [
    ObjectId("5ccc104f06e67e44fa0eb246"),
    ObjectId("5ccc104f06e67e44fa0eb249"),
    ObjectId("5ccc104f06e67e44fa0eb24d"),
    ObjectId("5ccc104f06e67e44fa0eb250")
  ],
  "testimonials": null,
  "type": "parts",
  "category": ObjectId("5d37ebfa2a02f425958371a8"),
  "name": "PS20X-E",
  "slug": "ps20x-e",
  "sno": "12345",
  "image": "/parts/Hand_Switch/PS20X-E.jpg",
  "short_description": "<p>Hand Switch</p>\r\n",
  "description": "<p>Hand SwitchHand SwitchHand SwitchHand SwitchHand SwitchHand SwitchHand SwitchHand SwitchHand SwitchHand SwitchHand SwitchHand SwitchHand SwitchHand SwitchHand SwitchHand SwitchHand SwitchHand SwitchHand SwitchHand SwitchHand SwitchHand SwitchHand SwitchHand SwitchHand Switch</p>\r\n",
  "price": NumberInt(2),
  "price_request": NumberInt(0),
  "schedule_service": NumberInt(0),
  "meta_title": "",
  "meta_description": "",
  "status": NumberInt(1),
  "related_product": [
    "5c668a67bc19ad187c839c7c",
    "5c668a8fbc19ad187c839c88"
  ],
  "created_at": ISODate("2019-03-01T09:01:51.384Z"),
  "updated_at": ISODate("2019-09-09T16:01:22.499Z"),
  "banner_image": "",
  "position": NumberInt(5)
});
db.getCollection("parts").insert({
  "_id": ObjectId("5ccc131106e67e44fa0eb273"),
  "product_id": [
    ObjectId("5c24667cd4568d68ad108584"),
    ObjectId("5cd198ce06e67e44fa0eb2e4")
  ],
  "lang_content": [
    ObjectId("5ccc131106e67e44fa0eb275"),
    ObjectId("5ccc131106e67e44fa0eb279"),
    ObjectId("5ccc131106e67e44fa0eb27c"),
    ObjectId("5ccc131106e67e44fa0eb27f")
  ],
  "testimonials": null,
  "type": "parts",
  "category": ObjectId("5c6689bfbc19ad187c839c60"),
  "name": "PSCC-B",
  "slug": "pscc-b",
  "sno": " PS50E-B",
  "image": "/parts/Batteries/PSCC.JPG",
  "short_description": "<p>Set of 2 rechargeable batteries</p>\r\n",
  "description": "<p>Rechargeable batteries&nbsp;</p>\r\n\r\n<p>Applicable Models:<br />\r\nLiteGait I&nbsp;w/PSCC Power System (set of 2)</p>\r\n\r\n<p>LiteGait is equipped with a 24 volt battery pack that needs to be charged <strong>overnight at least one per week.</strong>&nbsp; It is important to follow a weekly charging schedule to maximize the lifetime of the battery</p>\r\n\r\n<p><strong>Ground Shipping included in price for contiguous United States.</strong>&nbsp;FOB Tempe, Arizona.</p>\r\n\r\n<p>For expedited shipping or orders to Hawaii, Alaska or outside of the United States, please contact the Service &amp; Parts Department at 1-800-332-9255 Option 1</p>\r\n",
  "price": NumberInt(260),
  "price_request": NumberInt(0),
  "schedule_service": NumberInt(0),
  "meta_title": "",
  "meta_description": "",
  "status": NumberInt(1),
  "related_product": [
    ""
  ],
  "created_at": ISODate("2019-04-16T23:09:10.834Z"),
  "updated_at": ISODate("2019-09-11T15:26:51.471Z"),
  "position": NumberInt(5),
  "banner_image": ""
});
db.getCollection("parts").insert({
  "_id": ObjectId("5d7878eaf3c6d306896b26f5"),
  "product_id": [
    ObjectId("5d02700260b1b8748527ee74")
  ],
  "lang_content": [
    ObjectId("5d7878eaf3c6d306896b26f6"),
    ObjectId("5d7878eaf3c6d306896b26f8"),
    ObjectId("5d7878eaf3c6d306896b26fa"),
    ObjectId("5d7878eaf3c6d306896b26fc")
  ],
  "related_product": [
    ""
  ],
  "type": "parts",
  "category": ObjectId("5c6689bfbc19ad187c839c60"),
  "name": "PS20P-B",
  "slug": "ps20p-b",
  "sno": "",
  "image": "/parts/Batteries/PS20P-B.jpg",
  "banner_image": "",
  "short_description": "<p>24V SLA Battery</p>\r\n\r\n<p>Rechargeable Battery&nbsp;</p>\r\n\r\n<p>Applicable Models:<br />\r\nLiteGait&nbsp;I 200P</p>\r\n\r\n<p>LiteGait is equipped with a 24 volt battery pack that needs to be charged <strong>overnight at least one per week.</strong>&nbsp; It is important to follow a weekly charging schedule to maximize the lifetime&nbsp;of the battery</p>\r\n\r\n<p>&nbsp;</p>\r\n",
  "description": "<p><strong>Ground Shipping included in price for contiguous United States.</strong>&nbsp;FOB Tempe, Arizona.</p>\r\n\r\n<p>For expedited shipping or orders to Hawaii, Alaska or outside of the United States, please contact the Service &amp; Parts Department at 1-800-332-9255 Option 1</p>\r\n",
  "price": NumberInt(180),
  "price_request": NumberInt(0),
  "schedule_service": NumberInt(0),
  "position": NumberInt(2),
  "meta_title": "",
  "meta_description": "",
  "status": NumberInt(1),
  "created_at": ISODate("2019-09-11T04:32:42.526Z"),
  "updated_at": ISODate("2019-10-10T05:26:53.798Z"),
  "testimonials": null,
  "video": ""
});
db.getCollection("parts").insert({
  "_id": ObjectId("5d787a65f3c6d306896b2709"),
  "product_id": [
    ObjectId("5d02700260b1b8748527ee74")
  ],
  "lang_content": [
    ObjectId("5d787a65f3c6d306896b270b"),
    ObjectId("5d787a65f3c6d306896b270e"),
    ObjectId("5d787a65f3c6d306896b2711"),
    ObjectId("5d787a65f3c6d306896b2714")
  ],
  "testimonials": null,
  "related_product": [
    ""
  ],
  "type": "parts",
  "category": ObjectId("5c6689bfbc19ad187c839c60"),
  "name": "PS35E-B",
  "slug": "ps35e-b",
  "sno": "",
  "image": "/parts/Batteries/PS35E-B.jpg",
  "banner_image": "",
  "short_description": "<p>24V SLA Battery</p>\r\n",
  "description": "<p>Rechargeable Battery for PS35E</p>\r\n\r\n<p>Applicable Models:<br />\r\nLiteGait&nbsp;I 200P | LiteGait&nbsp;I 260E | LiteGait I 300E | LiteGait I 300MX (Version B) |&nbsp;LiteGait I 200P<br />\r\nLiteGait I 250E |&nbsp;LiteGait&nbsp;I 350​ | LiteGait I 350E</p>\r\n\r\n<p>LiteGait is equipped with a 24 volt battery pack that needs to be charged <strong>overnight at least one per week.</strong>&nbsp; It is important to follow a weekly charging schedule to maximize the lifetime of the battery</p>\r\n\r\n<p><strong>Ground Shipping included in price for contiguous United States.</strong>&nbsp;FOB Tempe, Arizona.</p>\r\n\r\n<p>For expedited shipping or orders to Hawaii, Alaska or outside of the United States, please contact the Service &amp; Parts Department at 1-800-332-9255 Option 1</p>\r\n",
  "price": NumberInt(260),
  "price_request": NumberInt(0),
  "schedule_service": NumberInt(0),
  "position": NumberInt(3),
  "meta_title": "",
  "meta_description": "",
  "status": NumberInt(1),
  "created_at": ISODate("2019-09-11T04:32:42.526Z"),
  "updated_at": ISODate("2019-09-11T15:12:27.632Z")
});
db.getCollection("parts").insert({
  "_id": ObjectId("5d7ad71ff3c6d306896b28d0"),
  "lang_content": [
    ObjectId("5d7ad71ff3c6d306896b28d1"),
    ObjectId("5d7ad71ff3c6d306896b28d3"),
    ObjectId("5d7ad71ff3c6d306896b28d5"),
    ObjectId("5d7ad71ff3c6d306896b28d7")
  ],
  "related_product": [
    ""
  ],
  "type": "accessories",
  "category": ObjectId("5d7ad51af3c6d306896b28a8"),
  "name": "FreeDome",
  "slug": "freedome",
  "sno": "",
  "image": "/accessories_img3.jpg",
  "banner_image": "",
  "short_description": "<p>Allows patient freedom of full 360&deg; rotation and locks in any position</p>\r\n",
  "description": "<p>The FreeDome* is an accessory that allows the patient/user to freely rotate in place beneath the yoke while remaining securely supported by the LiteGait&reg; harness.<br />\r\nFreeDome can easily be attached or removed from all LiteGait models. The FreeDome can be locked in any position allowing patient to walk sideways or backward on or off the treadmill, or simply walk with assistance when facing out of the LiteGai</p>\r\n",
  "price": null,
  "price_request": NumberInt(1),
  "schedule_service": NumberInt(0),
  "position": NumberInt(2),
  "meta_title": "",
  "meta_description": "",
  "status": NumberInt(1),
  "created_at": ISODate("2019-09-12T23:39:11.653Z"),
  "updated_at": ISODate("2019-09-26T04:34:56.582Z"),
  "product_id": null,
  "testimonials": null
});
db.getCollection("parts").insert({
  "_id": ObjectId("5d7ada2af3c6d306896b28de"),
  "lang_content": [
    ObjectId("5d7ada2af3c6d306896b28df"),
    ObjectId("5d7ada2af3c6d306896b28e1"),
    ObjectId("5d7ada2af3c6d306896b28e3"),
    ObjectId("5d7ada2af3c6d306896b28e5")
  ],
  "related_product": [
    "5c78f4ff8592290d03e2000b"
  ],
  "type": "accessories",
  "category": ObjectId("5d7ad51af3c6d306896b28a8"),
  "name": "iFreeDome",
  "slug": "ifreedome",
  "sno": "",
  "image": "/accessories/Y50E-FDBS(1).gif",
  "banner_image": "",
  "short_description": "<p>Integrated FreeDome paired with our&nbsp;<strong><a href=\"https://www.litegait.com/products/bisym-scale\" target=\"_self\">BiSym&nbsp;</a></strong>Tablet provides a real time display of the patient weight supported by the LiteGait&reg; on the Left, Right and Total</p>\r\n",
  "description": "<p>Integrated FreeDome paired with our&nbsp;<strong><a href=\"https://www.litegait.com/products/bisym-scale\" target=\"_self\">BiSym&nbsp;</a></strong>Tablet provides a real time display of the patient weight supported by the LiteGait&reg; on the Left, Right and Total.&nbsp;FreeDome allows the individual to freely rotate in place while remaining securely supported by the LiteGait. FreeDome can be locked into any position, allowing users to walk sideways with braiding, backward, on or off the treadmill, or simply walk with assistance when facing away from the LiteGait column.</p>\r\n",
  "price": null,
  "price_request": NumberInt(1),
  "schedule_service": NumberInt(0),
  "position": NumberInt(3),
  "meta_title": "",
  "meta_description": "",
  "status": NumberInt(1),
  "created_at": ISODate("2019-09-12T23:52:10.924Z"),
  "updated_at": ISODate("2019-10-02T22:32:17.21Z"),
  "product_id": [
    ObjectId("5c24667cd4568d68ad108584"),
    ObjectId("5cd198ce06e67e44fa0eb2e4"),
    ObjectId("5cd1a2bb06e67e44fa0eb2f8")
  ],
  "testimonials": null,
  "image_gallery": [
    ObjectId("5d8ddbd4074034097ebc8ac5"),
    ObjectId("5d8ddbd4074034097ebc8ac6")
  ],
  "video": "https://vimeo.com/304156637"
});

/** product_contents records **/
db.getCollection("product_contents").insert({
  "_id": ObjectId("5bd16edcb605151b7e93d89a"),
  "product_id": ObjectId("5bd16edbb605151b7e93d899"),
  "lang": "en",
  "short_description": "<p>18 oz. Premium Vinyl Banners</p>\r\n\r\n<p>18 oz. Premium Vinyl Banners&nbsp;</p>\r\n",
  "description": "<p>1The LGI300 P features 28&quot; of powered lift that can bring a patient up to 6&#39; 6&quot; tall and 300 pounds from sitting in a wheelchair to a full standing position over a treadmill. &nbsp;Additionally, the LGI&nbsp;300P Yoke can be manually adjusted to match your patient&#39;s height - making this one of our most versatile products.</p>\r\n",
  "created_at": ISODate("2018-10-25T07:21:00.18Z"),
  "updated_at": ISODate("2019-11-01T18:06:25.540Z")
});
db.getCollection("product_contents").insert({
  "_id": ObjectId("5bd16edcb605151b7e93d89c"),
  "product_id": ObjectId("5bd16edbb605151b7e93d899"),
  "lang": "fr",
  "short_description": "<p>18 oz. Premium Vinyl Banners18 oz. Premium Vinyl Banners</p>\r\n",
  "description": "<p>18 oz. Premium Vinyl Banners18 oz. Premium Vinyl Banners18 oz. Premium Vinyl Banners18 oz. Premium Vinyl Banners18 oz. Premium Vinyl Banners18 oz. Premium Vinyl Banners18 oz. Premium Vinyl Banners18 oz. Premium Vinyl Banners18 oz. Premium Vinyl Banners18 oz. Premium Vinyl Banners18 oz. Premium Vinyl Banners18 oz. Premium Vinyl Banners18 oz. Premium Vinyl Banners</p>\r\n",
  "created_at": ISODate("2018-10-25T07:21:00.844Z"),
  "updated_at": ISODate("2019-11-01T18:06:25.540Z")
});
db.getCollection("product_contents").insert({
  "_id": ObjectId("5c24667dd4568d68ad108585"),
  "product_id": ObjectId("5c24667cd4568d68ad108584"),
  "lang": "en",
  "short_description": "<p>Test Product</p>\r\n",
  "description": "<p>Text Here</p>\r\n",
  "created_at": ISODate("2018-12-27T05:43:25.20Z"),
  "updated_at": ISODate("2019-12-09T05:57:29.986Z")
});
db.getCollection("product_contents").insert({
  "_id": ObjectId("5c24667dd4568d68ad108587"),
  "product_id": ObjectId("5c24667cd4568d68ad108584"),
  "lang": "fr",
  "short_description": "",
  "description": "",
  "created_at": ISODate("2018-12-27T05:43:25.33Z"),
  "updated_at": ISODate("2019-12-09T05:57:29.986Z")
});
db.getCollection("product_contents").insert({
  "_id": ObjectId("5c24667dd4568d68ad108589"),
  "product_id": ObjectId("5c24667cd4568d68ad108584"),
  "lang": "Gr",
  "short_description": "",
  "description": "",
  "created_at": ISODate("2018-12-27T05:43:25.43Z"),
  "updated_at": ISODate("2019-12-09T05:57:29.986Z")
});
db.getCollection("product_contents").insert({
  "_id": ObjectId("5c24667dd4568d68ad10858b"),
  "product_id": ObjectId("5c24667cd4568d68ad108584"),
  "lang": "Sp",
  "short_description": "",
  "description": "",
  "created_at": ISODate("2018-12-27T05:43:25.52Z"),
  "updated_at": ISODate("2019-12-09T05:57:29.986Z")
});
db.getCollection("product_contents").insert({
  "_id": ObjectId("5ccc103806e67e44fa0eb235"),
  "product_id": ObjectId("5ccc103806e67e44fa0eb233"),
  "lang": "en",
  "short_description": "<p>18 oz. Premium Vinyl Banners</p>\r\n\r\n<p>18 oz. Premium Vinyl Banners&nbsp;</p>\r\n",
  "description": "<p>The 300MX model is a cost-effective option for patients up to 300 pounds. The innovative design and simplified lifting mechanism provides a reasonable alternative when cost is of primary concern.</p>\r\n",
  "created_at": ISODate("2018-10-25T07:21:00.18Z"),
  "updated_at": ISODate("2019-12-04T16:09:24.781Z")
});
db.getCollection("product_contents").insert({
  "_id": ObjectId("5ccc103806e67e44fa0eb239"),
  "product_id": ObjectId("5ccc103806e67e44fa0eb233"),
  "lang": "fr",
  "short_description": "<p>18 oz. Premium Vinyl Banners18 oz. Premium Vinyl Banners</p>\r\n",
  "description": "<p>18 oz. Premium Vinyl Banners18 oz. Premium Vinyl Banners18 oz. Premium Vinyl Banners18 oz. Premium Vinyl Banners18 oz. Premium Vinyl Banners18 oz. Premium Vinyl Banners18 oz. Premium Vinyl Banners18 oz. Premium Vinyl Banners18 oz. Premium Vinyl Banners18 oz. Premium Vinyl Banners18 oz. Premium Vinyl Banners18 oz. Premium Vinyl Banners18 oz. Premium Vinyl Banners</p>\r\n",
  "created_at": ISODate("2018-10-25T07:21:00.844Z"),
  "updated_at": ISODate("2019-12-04T16:09:24.781Z")
});
db.getCollection("product_contents").insert({
  "_id": ObjectId("5cd198ce06e67e44fa0eb2e5"),
  "product_id": ObjectId("5cd198ce06e67e44fa0eb2e4"),
  "lang": "en",
  "description": "<p>The LGI 400 features 32&quot; of powered lift that can quickly bring a patient up to 6&#39;8&quot; tall and 400 pounds from sitting in a wheelchair to a full standing position over a treadmill. The LG 400&nbsp;provides postural stability and bio-mechanically appropriate posture</p>\r\n",
  "created_at": ISODate("2019-05-07T14:40:14.401Z"),
  "updated_at": ISODate("2019-12-16T14:59:12.29Z")
});
db.getCollection("product_contents").insert({
  "_id": ObjectId("5cd198ce06e67e44fa0eb2e7"),
  "product_id": ObjectId("5cd198ce06e67e44fa0eb2e4"),
  "lang": "fr",
  "description": "",
  "created_at": ISODate("2019-05-07T14:40:14.411Z"),
  "updated_at": ISODate("2019-12-16T14:59:12.29Z")
});
db.getCollection("product_contents").insert({
  "_id": ObjectId("5cd198ce06e67e44fa0eb2e9"),
  "product_id": ObjectId("5cd198ce06e67e44fa0eb2e4"),
  "lang": "Gr",
  "description": "",
  "created_at": ISODate("2019-05-07T14:40:14.420Z"),
  "updated_at": ISODate("2019-12-16T14:59:12.29Z")
});
db.getCollection("product_contents").insert({
  "_id": ObjectId("5cd198ce06e67e44fa0eb2eb"),
  "product_id": ObjectId("5cd198ce06e67e44fa0eb2e4"),
  "lang": "Sp",
  "description": "",
  "created_at": ISODate("2019-05-07T14:40:14.426Z"),
  "updated_at": ISODate("2019-12-16T14:59:12.29Z")
});
db.getCollection("product_contents").insert({
  "_id": ObjectId("5cd1a2bb06e67e44fa0eb2f9"),
  "product_id": ObjectId("5cd1a2bb06e67e44fa0eb2f8"),
  "lang": "en",
  "description": "<p>The LGI300 features 28&quot; of powered lift that can bring a patient up to 6&#39; 9&quot; tall and 300 pounds from sitting in a wheelchair to a full standing position over a treadmill. The 300&nbsp;model provides postural stability and biomechanically appropriate posture. Includes 1 iHarness.</p>\r\n",
  "created_at": ISODate("2019-05-07T15:22:35.409Z"),
  "updated_at": ISODate("2019-11-01T18:04:05.184Z")
});
db.getCollection("product_contents").insert({
  "_id": ObjectId("5cd1a2bb06e67e44fa0eb2fb"),
  "product_id": ObjectId("5cd1a2bb06e67e44fa0eb2f8"),
  "lang": "fr",
  "description": "",
  "created_at": ISODate("2019-05-07T15:22:35.412Z"),
  "updated_at": ISODate("2019-11-01T18:04:05.184Z")
});
db.getCollection("product_contents").insert({
  "_id": ObjectId("5cd1a2bb06e67e44fa0eb2fd"),
  "product_id": ObjectId("5cd1a2bb06e67e44fa0eb2f8"),
  "lang": "Gr",
  "description": "",
  "created_at": ISODate("2019-05-07T15:22:35.414Z"),
  "updated_at": ISODate("2019-11-01T18:04:05.184Z")
});
db.getCollection("product_contents").insert({
  "_id": ObjectId("5cd1a2bb06e67e44fa0eb2ff"),
  "product_id": ObjectId("5cd1a2bb06e67e44fa0eb2f8"),
  "lang": "Sp",
  "description": "",
  "created_at": ISODate("2019-05-07T15:22:35.417Z"),
  "updated_at": ISODate("2019-11-01T18:04:05.184Z")
});
db.getCollection("product_contents").insert({
  "_id": ObjectId("5cf00a39fb42690c45d441d3"),
  "product_id": ObjectId("5cf00a39fb42690c45d441d2"),
  "lang": "en",
  "description": "<p>Therapy Mouse is a low cost, motion-sensing, wearable pointing device, that allows individuals who have difficulties using their hands to control a computer using any alternative body part. Simply plug into your computer and attach the sensor to the desired body part; the Therapy Mouse will translate user movements to precise, proportional computer control. &nbsp; Move the desired body part &ndash; and the computer pointer will move with it. &nbsp;Mouse clicks and selections can be performed either with the Therapy Mouse clicker, any other standard assistive switch, or with dwell software. &nbsp;Combine with our Gait Analysis software GaitSens&nbsp;to support&nbsp;<strong><a href=\"https://www.litegait.com/Blog-Dual-Tasking\">Dual Tasking</a></strong>&nbsp;during Gait Therapy!</p>\r\n",
  "created_at": ISODate("2019-05-30T16:52:09.14Z"),
  "updated_at": ISODate("2019-08-02T12:53:22.477Z")
});
db.getCollection("product_contents").insert({
  "_id": ObjectId("5cf00a39fb42690c45d441d5"),
  "product_id": ObjectId("5cf00a39fb42690c45d441d2"),
  "lang": "fr",
  "description": "",
  "created_at": ISODate("2019-05-30T16:52:09.16Z"),
  "updated_at": ISODate("2019-08-02T12:53:22.477Z")
});
db.getCollection("product_contents").insert({
  "_id": ObjectId("5cf00a39fb42690c45d441d7"),
  "product_id": ObjectId("5cf00a39fb42690c45d441d2"),
  "lang": "Gr",
  "description": "",
  "created_at": ISODate("2019-05-30T16:52:09.19Z"),
  "updated_at": ISODate("2019-08-02T12:53:22.477Z")
});
db.getCollection("product_contents").insert({
  "_id": ObjectId("5cf00a39fb42690c45d441d9"),
  "product_id": ObjectId("5cf00a39fb42690c45d441d2"),
  "lang": "Sp",
  "description": "",
  "created_at": ISODate("2019-05-30T16:52:09.21Z"),
  "updated_at": ISODate("2019-08-02T12:53:22.477Z")
});
db.getCollection("product_contents").insert({
  "_id": ObjectId("5cffd17c60b1b8748527ee2e"),
  "product_id": ObjectId("5cffd17c60b1b8748527ee2d"),
  "lang": "en",
  "description": "<p>The 100MX features an adjustable yoke for children up to 100 pounds. The adjustability built into these systems allows for growth potential in a single user case, and it allows facilities to easily treat all their patients with the same unit.</p>\r\n",
  "created_at": ISODate("2019-06-11T16:06:20.136Z"),
  "updated_at": ISODate("2019-12-04T16:13:09.532Z")
});
db.getCollection("product_contents").insert({
  "_id": ObjectId("5cffd17c60b1b8748527ee30"),
  "product_id": ObjectId("5cffd17c60b1b8748527ee2d"),
  "lang": "fr",
  "description": "",
  "created_at": ISODate("2019-06-11T16:06:20.139Z"),
  "updated_at": ISODate("2019-12-04T16:13:09.532Z")
});
db.getCollection("product_contents").insert({
  "_id": ObjectId("5cffd17c60b1b8748527ee32"),
  "product_id": ObjectId("5cffd17c60b1b8748527ee2d"),
  "lang": "Gr",
  "description": "",
  "created_at": ISODate("2019-06-11T16:06:20.142Z"),
  "updated_at": ISODate("2019-12-04T16:13:09.532Z")
});
db.getCollection("product_contents").insert({
  "_id": ObjectId("5cffd17c60b1b8748527ee34"),
  "product_id": ObjectId("5cffd17c60b1b8748527ee2d"),
  "lang": "Sp",
  "description": "",
  "created_at": ISODate("2019-06-11T16:06:20.146Z"),
  "updated_at": ISODate("2019-12-04T16:13:09.532Z")
});
db.getCollection("product_contents").insert({
  "_id": ObjectId("5d02700260b1b8748527ee75"),
  "product_id": ObjectId("5d02700260b1b8748527ee74"),
  "lang": "en",
  "description": "<p>The 200P is designed to meet the needs of younger patients, from children to teenagers. The 200P&#39;s innovative yoke can be manually adjusted to fit the patient, while 28&quot; of powered lift can quickly bring children and teenagers up to 6&#39; 0&quot; and 200 pounds from sitting in a wheelchair to a full standing position over a treadmill. Includes 2 iHarnesses.</p>\r\n",
  "created_at": ISODate("2019-06-13T15:47:14.645Z"),
  "updated_at": ISODate("2019-10-24T15:43:51.749Z")
});
db.getCollection("product_contents").insert({
  "_id": ObjectId("5d02700260b1b8748527ee77"),
  "product_id": ObjectId("5d02700260b1b8748527ee74"),
  "lang": "fr",
  "description": "",
  "created_at": ISODate("2019-06-13T15:47:14.648Z"),
  "updated_at": ISODate("2019-10-24T15:43:51.749Z")
});
db.getCollection("product_contents").insert({
  "_id": ObjectId("5d02700260b1b8748527ee79"),
  "product_id": ObjectId("5d02700260b1b8748527ee74"),
  "lang": "Gr",
  "description": "",
  "created_at": ISODate("2019-06-13T15:47:14.650Z"),
  "updated_at": ISODate("2019-10-24T15:43:51.749Z")
});
db.getCollection("product_contents").insert({
  "_id": ObjectId("5d02700260b1b8748527ee7b"),
  "product_id": ObjectId("5d02700260b1b8748527ee74"),
  "lang": "Sp",
  "description": "",
  "created_at": ISODate("2019-06-13T15:47:14.653Z"),
  "updated_at": ISODate("2019-10-24T15:43:51.749Z")
});
db.getCollection("product_contents").insert({
  "_id": ObjectId("5d02757a60b1b8748527ee96"),
  "product_id": ObjectId("5d02757a60b1b8748527ee95"),
  "lang": "en",
  "description": "<p>The GaitKeeper S20 is ideal for meeting your rehabilitation needs, designed with features preferred by clinicians while being simple for patients. The easy-to-read display shows distance in feet/meters and miles/kilometers and allows for programming of goals for session. The display console is removable to allow the clinician to monitor progress and adjust parameters from various positions while assisting or monitoring the patient. The S20 features a true zero start and 0.1 mph increments allow for safe use for patients of all levels. The 20&quot; by 56&quot; belt provides ample walking space, while the high torque motor keeps the belt running smooth, even at very low speeds. The unique, patent-pending &ldquo;Adjustabars&rdquo; have more range of adjustability than any handlebars currently available, allowing for easy height and width adjustment, with 20 secure, incremental positions to match patients of all sizes. The GaitKeeper S20 speeds up to 6 mph, inclines to 15% and can support up to 400 pounds.<br />\r\n<em><strong>Certifications</strong></em>: TUVc TUVus, Standard UL, CAN/CSA&nbsp;</p>\r\n",
  "created_at": ISODate("2019-06-13T16:10:34.836Z"),
  "updated_at": ISODate("2019-12-09T05:36:57.443Z")
});
db.getCollection("product_contents").insert({
  "_id": ObjectId("5d02757a60b1b8748527ee98"),
  "product_id": ObjectId("5d02757a60b1b8748527ee95"),
  "lang": "fr",
  "description": "",
  "created_at": ISODate("2019-06-13T16:10:34.839Z"),
  "updated_at": ISODate("2019-12-09T05:36:57.443Z")
});
db.getCollection("product_contents").insert({
  "_id": ObjectId("5d02757a60b1b8748527ee9a"),
  "product_id": ObjectId("5d02757a60b1b8748527ee95"),
  "lang": "Gr",
  "description": "",
  "created_at": ISODate("2019-06-13T16:10:34.841Z"),
  "updated_at": ISODate("2019-12-09T05:36:57.443Z")
});
db.getCollection("product_contents").insert({
  "_id": ObjectId("5d02757a60b1b8748527ee9c"),
  "product_id": ObjectId("5d02757a60b1b8748527ee95"),
  "lang": "Sp",
  "description": "",
  "created_at": ISODate("2019-06-13T16:10:34.844Z"),
  "updated_at": ISODate("2019-12-09T05:36:57.443Z")
});
db.getCollection("product_contents").insert({
  "_id": ObjectId("5d0bb37960b1b8748527ef98"),
  "product_id": ObjectId("5d0bb37960b1b8748527ef97"),
  "lang": "en",
  "description": "<p>Provide more treatment time and options &bull; All the benefits of LiteGait&reg; at home &bull; Multiple cost effective payment options</p>\r\n",
  "created_at": ISODate("2019-06-20T16:25:29.771Z"),
  "updated_at": ISODate("2019-12-09T05:33:06.426Z")
});
db.getCollection("product_contents").insert({
  "_id": ObjectId("5d0bb37960b1b8748527ef9a"),
  "product_id": ObjectId("5d0bb37960b1b8748527ef97"),
  "lang": "fr",
  "description": "",
  "created_at": ISODate("2019-06-20T16:25:29.776Z"),
  "updated_at": ISODate("2019-12-09T05:33:06.426Z")
});
db.getCollection("product_contents").insert({
  "_id": ObjectId("5d0bb37960b1b8748527ef9c"),
  "product_id": ObjectId("5d0bb37960b1b8748527ef97"),
  "lang": "Gr",
  "description": "",
  "created_at": ISODate("2019-06-20T16:25:29.779Z"),
  "updated_at": ISODate("2019-12-09T05:33:06.426Z")
});
db.getCollection("product_contents").insert({
  "_id": ObjectId("5d0bb37960b1b8748527ef9e"),
  "product_id": ObjectId("5d0bb37960b1b8748527ef97"),
  "lang": "Sp",
  "description": "",
  "created_at": ISODate("2019-06-20T16:25:29.782Z"),
  "updated_at": ISODate("2019-12-09T05:33:06.426Z")
});
db.getCollection("product_contents").insert({
  "_id": ObjectId("5d2debf9837f487c405e630e"),
  "product_id": ObjectId("5d2debf9837f487c405e630d"),
  "lang": "en",
  "description": "<ul>\r\n\t<li>Supported Ambulation - ample leg room for gait</li>\r\n\t<li>Sit to Stand</li>\r\n\t<li>Seated to Seated transfers? - in and out of wheelchair, toilet and bed</li>\r\n\t<li>Battery operated power lift</li>\r\n\t<li>Easy to unweight patients during ambulation</li>\r\n\t<li>Rolls easily in both home and therapy settings</li>\r\n\t<li>Small turn radius</li>\r\n\t<li>Legs swing open for wide chairs and close for narrow doorways (optional)</li>\r\n\t<li>Low base available to fit under 9&quot; or 7&quot; bed frames</li>\r\n\t<li>Small footprint for storage</li>\r\n</ul>\r\n",
  "created_at": ISODate("2019-07-16T15:23:37.36Z"),
  "updated_at": ISODate("2019-12-17T16:12:12.459Z")
});
db.getCollection("product_contents").insert({
  "_id": ObjectId("5d2debf9837f487c405e6310"),
  "product_id": ObjectId("5d2debf9837f487c405e630d"),
  "lang": "fr",
  "description": "",
  "created_at": ISODate("2019-07-16T15:23:37.39Z"),
  "updated_at": ISODate("2019-12-17T16:12:12.459Z")
});
db.getCollection("product_contents").insert({
  "_id": ObjectId("5d2debf9837f487c405e6312"),
  "product_id": ObjectId("5d2debf9837f487c405e630d"),
  "lang": "Gr",
  "description": "",
  "created_at": ISODate("2019-07-16T15:23:37.41Z"),
  "updated_at": ISODate("2019-12-17T16:12:12.459Z")
});
db.getCollection("product_contents").insert({
  "_id": ObjectId("5d2debf9837f487c405e6314"),
  "product_id": ObjectId("5d2debf9837f487c405e630d"),
  "lang": "Sp",
  "description": "",
  "created_at": ISODate("2019-07-16T15:23:37.44Z"),
  "updated_at": ISODate("2019-12-17T16:12:12.459Z")
});
db.getCollection("product_contents").insert({
  "_id": ObjectId("5d4287e182d43979936670ba"),
  "product_id": ObjectId("5d4287e182d43979936670b9"),
  "lang": "en",
  "description": "<p>Powerful, portable and versatile walking surface<br />\r\nPerfect for peds and adult at home or the clinic<br />\r\nEasily roll from room to room - bring to the patient<br />\r\n21&quot; W x 40&quot;L Walking Surface (18&quot;W available)<br />\r\nWeight: 350 lb Max Patient Weight</p>\r\n\r\n<p>The GaitKeeper Mini&reg; is truly a revolutionary portable walking surface designed for the busy clinic with limited floor space, or a home setting. &nbsp; &nbsp;Integrating with the world renown LiteGait&reg; &amp; Walkable&reg; (pediatric LiteGait), the GaitKeeper Mini is everything you need to get the best out of your LiteGait.<br />\r\nDo not be misled by the small size of this workhorse - this powerful treadmill performs. &nbsp;With a true zero start and steady 0.1 mph starting speed, the GaitKeeper Mini keeps a steady pace up to a maximum of 3 mph for all users from Pediatrics to a 350lb adult. &nbsp;The full featured handheld remote provides LED display of the belt speed and gives the clinician complete control of the treadmill regardless of location. Integrates with LiteGait.</p>\r\n\r\n<p>The small 40&quot; x 21&quot; (or 18&quot;W) &nbsp;walking surface fits perfectly under the LiteGait or Walkable; yet provides sufficient surface for most patients. &nbsp;With a footprint of only 42&rdquo; by 23&rdquo; &nbsp;or (21&quot;) and weighing less than 65 lbs, the GaitKeeper Mini fits in the smallest locations and is easily rolled or moved out of the way for storage when needed. &nbsp;You can even bring this treadmill with your LiteGait into your patient&rsquo;s room and it will fit under most beds! &nbsp;</p>\r\n\r\n<p>The GaitKeeper Mini continues Mobility Research&rsquo;s almost 20-year history of providing innovative rehabilitation therapy products to rehabilitation clinicians. &nbsp;</p>\r\n\r\n<p>US Patent# 9,339,683</p>\r\n",
  "created_at": ISODate("2019-08-01T06:34:09.161Z"),
  "updated_at": ISODate("2019-10-30T14:26:56.922Z")
});
db.getCollection("product_contents").insert({
  "_id": ObjectId("5d4287e182d43979936670bc"),
  "product_id": ObjectId("5d4287e182d43979936670b9"),
  "lang": "fr",
  "description": "",
  "created_at": ISODate("2019-08-01T06:34:09.163Z"),
  "updated_at": ISODate("2019-10-30T14:26:56.922Z")
});
db.getCollection("product_contents").insert({
  "_id": ObjectId("5d4287e182d43979936670be"),
  "product_id": ObjectId("5d4287e182d43979936670b9"),
  "lang": "Gr",
  "description": "",
  "created_at": ISODate("2019-08-01T06:34:09.166Z"),
  "updated_at": ISODate("2019-10-30T14:26:56.922Z")
});
db.getCollection("product_contents").insert({
  "_id": ObjectId("5d4287e182d43979936670c0"),
  "product_id": ObjectId("5d4287e182d43979936670b9"),
  "lang": "Sp",
  "description": "",
  "created_at": ISODate("2019-08-01T06:34:09.168Z"),
  "updated_at": ISODate("2019-10-30T14:26:56.923Z")
});
db.getCollection("product_contents").insert({
  "_id": ObjectId("5d4bb3d8a9421434c38e69bf"),
  "product_id": ObjectId("5d4bb3d8a9421434c38e69be"),
  "lang": "en",
  "description": "<h3 style=\"color:#000000; font-style:normal; margin-left:0px; margin-right:0px; text-align:left\">Easily provide objective gait data</h3>\r\n\r\n<p style=\"text-align:left\">Objective gait assessment with no setup. Sensors installed in GaitKeeper Treadmill send data 250 times a second to the supplied tablet. The GaitSens software determines gait events in real time. Plug in treadmill, run the software, system is ready to go! Initialize once and have clinical gait assessment tools and reports at your fingertips.</p>\r\n",
  "created_at": ISODate("2019-08-08T05:32:08.623Z"),
  "updated_at": ISODate("2019-12-06T20:45:59.595Z")
});
db.getCollection("product_contents").insert({
  "_id": ObjectId("5d4bb3d8a9421434c38e69c1"),
  "product_id": ObjectId("5d4bb3d8a9421434c38e69be"),
  "lang": "fr",
  "description": "",
  "created_at": ISODate("2019-08-08T05:32:08.626Z"),
  "updated_at": ISODate("2019-12-06T20:45:59.596Z")
});
db.getCollection("product_contents").insert({
  "_id": ObjectId("5d4bb3d8a9421434c38e69c3"),
  "product_id": ObjectId("5d4bb3d8a9421434c38e69be"),
  "lang": "Gr",
  "description": "",
  "created_at": ISODate("2019-08-08T05:32:08.629Z"),
  "updated_at": ISODate("2019-12-06T20:45:59.596Z")
});
db.getCollection("product_contents").insert({
  "_id": ObjectId("5d4bb3d8a9421434c38e69c5"),
  "product_id": ObjectId("5d4bb3d8a9421434c38e69be"),
  "lang": "Sp",
  "description": "",
  "created_at": ISODate("2019-08-08T05:32:08.631Z"),
  "updated_at": ISODate("2019-12-06T20:45:59.596Z")
});
db.getCollection("product_contents").insert({
  "_id": ObjectId("5d5b74df07ea1355fada742f"),
  "product_id": ObjectId("5d5b74df07ea1355fada742e"),
  "lang": "en",
  "description": "<p>The LG75 is a lightweight rehabilitation device for children up to 75 pounds. The design includes parallel handle bars and a supportive, nonpowered yoke that can be adjusted easily for more control of posture and balance.</p>\r\n",
  "created_at": ISODate("2019-08-20T04:19:43.694Z"),
  "updated_at": ISODate("2019-12-04T16:27:17.479Z")
});
db.getCollection("product_contents").insert({
  "_id": ObjectId("5d5b74df07ea1355fada7431"),
  "product_id": ObjectId("5d5b74df07ea1355fada742e"),
  "lang": "fr",
  "description": "",
  "created_at": ISODate("2019-08-20T04:19:43.698Z"),
  "updated_at": ISODate("2019-12-04T16:27:17.479Z")
});
db.getCollection("product_contents").insert({
  "_id": ObjectId("5d5b74df07ea1355fada7433"),
  "product_id": ObjectId("5d5b74df07ea1355fada742e"),
  "lang": "Gr",
  "description": "",
  "created_at": ISODate("2019-08-20T04:19:43.702Z"),
  "updated_at": ISODate("2019-12-04T16:27:17.479Z")
});
db.getCollection("product_contents").insert({
  "_id": ObjectId("5d5b74df07ea1355fada7435"),
  "product_id": ObjectId("5d5b74df07ea1355fada742e"),
  "lang": "Sp",
  "description": "",
  "created_at": ISODate("2019-08-20T04:19:43.704Z"),
  "updated_at": ISODate("2019-12-04T16:27:17.479Z")
});
db.getCollection("product_contents").insert({
  "_id": ObjectId("5d7a6662f3c6d306896b280f"),
  "product_id": ObjectId("5d7a6662f3c6d306896b280e"),
  "lang": "en",
  "description": "<p>The LiteGait&reg;-V model (LiteGait-Veterinary) can be used for quadrupeds weighing up to 200 pounds. The unit supports the trunk of the animal so that varying amounts of weight can be put on the limbs during gait training. Harnesses of different sizes are available for optimal fitting to individual animals. A battery operated lift mechanism allows for positioning for a wide range of body sizes from small to giant breed dogs.</p>\r\n\r\n<p>​Research on adults and children with neurological disorders has shown that utilizing the concepts of Body Weight Support treadmill training (also known as Partial Weight Bearing Treadmill Training) can be an effective method in training or retraining patients to walk. Several years ago an innovative doctor of veterinary medicine who was also trained as a physical therapist asked if these concepts could be transferred to companion animals. The answer has resulted in the LiteGait&reg;-V model (LiteGait-Veterinary). This unit can be used for quadrupeds weighing up to 200 pounds. The unit supports the trunk of the animal so that varying amounts of weight can be put on the limbs during gait training. Harnesses of different sizes are available for optimal fitting to individual animals. A battery operated lift mechanism allows for positioning for a wide range of body sizes from small to giant breed dogs.</p>\r\n\r\n<p>The unit can be utilized to assist the animal in moving over ground or over a slow moving treadmill. In addition, in veterinary practice, the LiteGait-V has the advantage of serving as a transfer device to assist handlers in lifting the animal into standing position</p>\r\n",
  "created_at": ISODate("2019-09-12T15:38:10.156Z"),
  "updated_at": ISODate("2019-10-11T03:47:22.714Z")
});
db.getCollection("product_contents").insert({
  "_id": ObjectId("5d7a6662f3c6d306896b2811"),
  "product_id": ObjectId("5d7a6662f3c6d306896b280e"),
  "lang": "fr",
  "description": "",
  "created_at": ISODate("2019-09-12T15:38:10.158Z"),
  "updated_at": ISODate("2019-10-11T03:47:22.714Z")
});
db.getCollection("product_contents").insert({
  "_id": ObjectId("5d7a6662f3c6d306896b2813"),
  "product_id": ObjectId("5d7a6662f3c6d306896b280e"),
  "lang": "Gr",
  "description": "",
  "created_at": ISODate("2019-09-12T15:38:10.162Z"),
  "updated_at": ISODate("2019-10-11T03:47:22.714Z")
});
db.getCollection("product_contents").insert({
  "_id": ObjectId("5d7a6662f3c6d306896b2815"),
  "product_id": ObjectId("5d7a6662f3c6d306896b280e"),
  "lang": "Sp",
  "description": "",
  "created_at": ISODate("2019-09-12T15:38:10.164Z"),
  "updated_at": ISODate("2019-10-11T03:47:22.714Z")
});
db.getCollection("product_contents").insert({
  "_id": ObjectId("5d7bca91e696962cf1b33b04"),
  "product_id": ObjectId("5d7bca91e696962cf1b33b03"),
  "lang": "en",
  "description": "<p>no</p>\r\n",
  "created_at": ISODate("2019-09-13T16:57:53.868Z"),
  "updated_at": ISODate("2019-10-25T15:48:11.666Z")
});
db.getCollection("product_contents").insert({
  "_id": ObjectId("5d7bca91e696962cf1b33b06"),
  "product_id": ObjectId("5d7bca91e696962cf1b33b03"),
  "lang": "fr",
  "description": "",
  "created_at": ISODate("2019-09-13T16:57:53.871Z"),
  "updated_at": ISODate("2019-10-25T15:48:11.666Z")
});
db.getCollection("product_contents").insert({
  "_id": ObjectId("5d7bca91e696962cf1b33b08"),
  "product_id": ObjectId("5d7bca91e696962cf1b33b03"),
  "lang": "Gr",
  "description": "",
  "created_at": ISODate("2019-09-13T16:57:53.874Z"),
  "updated_at": ISODate("2019-10-25T15:48:11.666Z")
});
db.getCollection("product_contents").insert({
  "_id": ObjectId("5d7bca91e696962cf1b33b0a"),
  "product_id": ObjectId("5d7bca91e696962cf1b33b03"),
  "lang": "Sp",
  "description": "",
  "created_at": ISODate("2019-09-13T16:57:53.876Z"),
  "updated_at": ISODate("2019-10-25T15:48:11.666Z")
});
db.getCollection("product_contents").insert({
  "_id": ObjectId("5d88f0db72c6024c7f2edd39"),
  "product_id": ObjectId("5d88f0db72c6024c7f2edd38"),
  "lang": "en",
  "description": "",
  "created_at": ISODate("2019-09-23T16:20:43.438Z"),
  "updated_at": ISODate("2019-11-06T16:07:42.399Z")
});
db.getCollection("product_contents").insert({
  "_id": ObjectId("5d88f0db72c6024c7f2edd3b"),
  "product_id": ObjectId("5d88f0db72c6024c7f2edd38"),
  "lang": "fr",
  "description": "",
  "created_at": ISODate("2019-09-23T16:20:43.462Z"),
  "updated_at": ISODate("2019-11-06T16:07:42.399Z")
});
db.getCollection("product_contents").insert({
  "_id": ObjectId("5d88f0db72c6024c7f2edd3d"),
  "product_id": ObjectId("5d88f0db72c6024c7f2edd38"),
  "lang": "Gr",
  "description": "",
  "created_at": ISODate("2019-09-23T16:20:43.466Z"),
  "updated_at": ISODate("2019-11-06T16:07:42.399Z")
});
db.getCollection("product_contents").insert({
  "_id": ObjectId("5d88f0db72c6024c7f2edd3f"),
  "product_id": ObjectId("5d88f0db72c6024c7f2edd38"),
  "lang": "Sp",
  "description": "",
  "created_at": ISODate("2019-09-23T16:20:43.468Z"),
  "updated_at": ISODate("2019-11-06T16:07:42.399Z")
});
db.getCollection("product_contents").insert({
  "_id": ObjectId("5d9e21cf7dbdaa69d72e5ed6"),
  "product_id": ObjectId("5d9e21cf7dbdaa69d72e5ed5"),
  "lang": "en",
  "description": "<p>Q-pads collorful and fun</p>\r\n",
  "created_at": ISODate("2019-10-09T18:07:11.99Z"),
  "updated_at": ISODate("2019-10-11T03:47:46.970Z")
});
db.getCollection("product_contents").insert({
  "_id": ObjectId("5d9e21cf7dbdaa69d72e5ed8"),
  "product_id": ObjectId("5d9e21cf7dbdaa69d72e5ed5"),
  "lang": "fr",
  "description": "",
  "created_at": ISODate("2019-10-09T18:07:11.184Z"),
  "updated_at": ISODate("2019-10-11T03:47:46.970Z")
});
db.getCollection("product_contents").insert({
  "_id": ObjectId("5d9e21cf7dbdaa69d72e5eda"),
  "product_id": ObjectId("5d9e21cf7dbdaa69d72e5ed5"),
  "lang": "Gr",
  "description": "",
  "created_at": ISODate("2019-10-09T18:07:11.196Z"),
  "updated_at": ISODate("2019-10-11T03:47:46.970Z")
});
db.getCollection("product_contents").insert({
  "_id": ObjectId("5d9e21cf7dbdaa69d72e5edc"),
  "product_id": ObjectId("5d9e21cf7dbdaa69d72e5ed5"),
  "lang": "Sp",
  "description": "",
  "created_at": ISODate("2019-10-09T18:07:11.204Z"),
  "updated_at": ISODate("2019-10-11T03:47:46.970Z")
});
db.getCollection("product_contents").insert({
  "_id": ObjectId("5de7dc82e3d0053b781ed363"),
  "product_id": ObjectId("5de7dc82e3d0053b781ed362"),
  "lang": "en",
  "description": "",
  "created_at": ISODate("2019-12-04T16:19:14.703Z"),
  "updated_at": ISODate("2019-12-04T16:19:46.699Z")
});
db.getCollection("product_contents").insert({
  "_id": ObjectId("5de7dc82e3d0053b781ed365"),
  "product_id": ObjectId("5de7dc82e3d0053b781ed362"),
  "lang": "fr",
  "description": "",
  "created_at": ISODate("2019-12-04T16:19:14.737Z"),
  "updated_at": ISODate("2019-12-04T16:19:46.699Z")
});
db.getCollection("product_contents").insert({
  "_id": ObjectId("5de7dc82e3d0053b781ed367"),
  "product_id": ObjectId("5de7dc82e3d0053b781ed362"),
  "lang": "Gr",
  "description": "",
  "created_at": ISODate("2019-12-04T16:19:14.747Z"),
  "updated_at": ISODate("2019-12-04T16:19:46.699Z")
});
db.getCollection("product_contents").insert({
  "_id": ObjectId("5de7dc82e3d0053b781ed369"),
  "product_id": ObjectId("5de7dc82e3d0053b781ed362"),
  "lang": "Sp",
  "description": "",
  "created_at": ISODate("2019-12-04T16:19:14.759Z"),
  "updated_at": ISODate("2019-12-04T16:19:46.699Z")
});
db.getCollection("product_contents").insert({
  "_id": ObjectId("5de7dd91e3d0053b781ed37f"),
  "product_id": ObjectId("5de7dd91e3d0053b781ed37e"),
  "lang": "en",
  "description": "",
  "created_at": ISODate("2019-12-04T16:23:45.690Z"),
  "updated_at": ISODate("2019-12-04T16:23:45.690Z")
});
db.getCollection("product_contents").insert({
  "_id": ObjectId("5de7dd91e3d0053b781ed381"),
  "product_id": ObjectId("5de7dd91e3d0053b781ed37e"),
  "lang": "fr",
  "description": "",
  "created_at": ISODate("2019-12-04T16:23:45.693Z"),
  "updated_at": ISODate("2019-12-04T16:23:45.693Z")
});
db.getCollection("product_contents").insert({
  "_id": ObjectId("5de7dd91e3d0053b781ed383"),
  "product_id": ObjectId("5de7dd91e3d0053b781ed37e"),
  "lang": "Gr",
  "description": "",
  "created_at": ISODate("2019-12-04T16:23:45.695Z"),
  "updated_at": ISODate("2019-12-04T16:23:45.695Z")
});
db.getCollection("product_contents").insert({
  "_id": ObjectId("5de7dd91e3d0053b781ed385"),
  "product_id": ObjectId("5de7dd91e3d0053b781ed37e"),
  "lang": "Sp",
  "description": "",
  "created_at": ISODate("2019-12-04T16:23:45.697Z"),
  "updated_at": ISODate("2019-12-04T16:23:45.697Z")
});
db.getCollection("product_contents").insert({
  "_id": ObjectId("5de9e9a395b70729ea6d4c59"),
  "product_id": ObjectId("5de9e9a395b70729ea6d4c58"),
  "lang": "en",
  "description": "<p>The GaitKeeper S22 is our premium rehabilitation treadmill. Its substantial 22&quot; by 56&quot; walking surface has a REVERSE function for more functional gait activities. The easy-to-read display shows distance in feet/meters and miles/kilometers and allows for programming of goals for session. The display console is removable to allow the clinician to monitor progress and adjust parameters from various positions while assisting or monitoring the patient. A true zero start and 0.1 mph increments allow for safe use for patients of all levels. The efficient AC motor produces high torque with cool and quiet operation, allowing smooth belt movement, even at very low speeds and when transitioning from forward to reverse. The unique, patent-pending &ldquo;Adjustabars&rdquo; have more range of adjustability than any handlebars currently available, allowing for easy height and width adjustment, with 20 secure, incremental positions to match patients of all sizes. . The GaitKeeper S22 speeds up to 10 mph forward and 3 mph in reverse, inclines up to 15% and can support up to 400 pounds.<br />\r\n<em><strong>Certifications</strong></em>: TUVc,&nbsp;TUVus, UL, CAN/CSA, MDD IEC60601-1-2&nbsp;(220V)</p>\r\n\r\n<p>GaitKeeper S22 is available installed with our&nbsp;<a href=\"https://www.litegait.com/products/GaitSens-2000\">GaitSens&nbsp;</a>software.&nbsp;&nbsp;GaitSens&nbsp; provides real-time temporal and spatial parameters of gait with objective reports and patient feedback functions.</p>\r\n",
  "created_at": ISODate("2019-12-06T05:39:47.907Z"),
  "updated_at": ISODate("2019-12-09T05:36:30.123Z")
});
db.getCollection("product_contents").insert({
  "_id": ObjectId("5de9e9a395b70729ea6d4c5b"),
  "product_id": ObjectId("5de9e9a395b70729ea6d4c58"),
  "lang": "fr",
  "description": "",
  "created_at": ISODate("2019-12-06T05:39:47.952Z"),
  "updated_at": ISODate("2019-12-09T05:36:30.123Z")
});
db.getCollection("product_contents").insert({
  "_id": ObjectId("5de9e9a395b70729ea6d4c5d"),
  "product_id": ObjectId("5de9e9a395b70729ea6d4c58"),
  "lang": "Gr",
  "description": "",
  "created_at": ISODate("2019-12-06T05:39:47.971Z"),
  "updated_at": ISODate("2019-12-09T05:36:30.123Z")
});
db.getCollection("product_contents").insert({
  "_id": ObjectId("5de9e9a495b70729ea6d4c5f"),
  "product_id": ObjectId("5de9e9a395b70729ea6d4c58"),
  "lang": "Sp",
  "description": "",
  "created_at": ISODate("2019-12-06T05:39:48.70Z"),
  "updated_at": ISODate("2019-12-09T05:36:30.123Z")
});
db.getCollection("product_contents").insert({
  "_id": ObjectId("5deada7495b70729ea6d4cc4"),
  "product_id": ObjectId("5deada7495b70729ea6d4cc3"),
  "lang": "en",
  "description": "<p>text</p>\r\n",
  "created_at": ISODate("2019-12-06T22:47:16.867Z"),
  "updated_at": ISODate("2019-12-17T16:11:08.292Z")
});
db.getCollection("product_contents").insert({
  "_id": ObjectId("5deada7495b70729ea6d4cc6"),
  "product_id": ObjectId("5deada7495b70729ea6d4cc3"),
  "lang": "fr",
  "description": "",
  "created_at": ISODate("2019-12-06T22:47:16.873Z"),
  "updated_at": ISODate("2019-12-17T16:11:08.292Z")
});
db.getCollection("product_contents").insert({
  "_id": ObjectId("5deada7495b70729ea6d4cc8"),
  "product_id": ObjectId("5deada7495b70729ea6d4cc3"),
  "lang": "Gr",
  "description": "",
  "created_at": ISODate("2019-12-06T22:47:16.876Z"),
  "updated_at": ISODate("2019-12-17T16:11:08.292Z")
});
db.getCollection("product_contents").insert({
  "_id": ObjectId("5deada7495b70729ea6d4cca"),
  "product_id": ObjectId("5deada7495b70729ea6d4cc3"),
  "lang": "Sp",
  "description": "",
  "created_at": ISODate("2019-12-06T22:47:16.878Z"),
  "updated_at": ISODate("2019-12-17T16:11:08.292Z")
});
db.getCollection("product_contents").insert({
  "_id": ObjectId("5deddc2695b70729ea6d4cee"),
  "product_id": ObjectId("5deddc2695b70729ea6d4ced"),
  "lang": "en",
  "description": "<p>330</p>\r\n",
  "created_at": ISODate("2019-12-09T05:31:18.685Z"),
  "updated_at": ISODate("2019-12-09T05:33:25.257Z")
});
db.getCollection("product_contents").insert({
  "_id": ObjectId("5deddc2695b70729ea6d4cf0"),
  "product_id": ObjectId("5deddc2695b70729ea6d4ced"),
  "lang": "fr",
  "description": "",
  "created_at": ISODate("2019-12-09T05:31:18.700Z"),
  "updated_at": ISODate("2019-12-09T05:33:25.257Z")
});
db.getCollection("product_contents").insert({
  "_id": ObjectId("5deddc2695b70729ea6d4cf2"),
  "product_id": ObjectId("5deddc2695b70729ea6d4ced"),
  "lang": "Gr",
  "description": "",
  "created_at": ISODate("2019-12-09T05:31:18.703Z"),
  "updated_at": ISODate("2019-12-09T05:33:25.257Z")
});
db.getCollection("product_contents").insert({
  "_id": ObjectId("5deddc2695b70729ea6d4cf4"),
  "product_id": ObjectId("5deddc2695b70729ea6d4ced"),
  "lang": "Sp",
  "description": "",
  "created_at": ISODate("2019-12-09T05:31:18.705Z"),
  "updated_at": ISODate("2019-12-09T05:33:25.257Z")
});
db.getCollection("product_contents").insert({
  "_id": ObjectId("5deddcf795b70729ea6d4d18"),
  "product_id": ObjectId("5deddcf795b70729ea6d4d17"),
  "lang": "en",
  "description": "<p>110</p>\r\n",
  "created_at": ISODate("2019-12-09T05:34:47.653Z"),
  "updated_at": ISODate("2019-12-09T05:34:47.653Z")
});
db.getCollection("product_contents").insert({
  "_id": ObjectId("5deddcf795b70729ea6d4d1a"),
  "product_id": ObjectId("5deddcf795b70729ea6d4d17"),
  "lang": "fr",
  "description": "",
  "created_at": ISODate("2019-12-09T05:34:47.655Z"),
  "updated_at": ISODate("2019-12-09T05:34:47.655Z")
});
db.getCollection("product_contents").insert({
  "_id": ObjectId("5deddcf795b70729ea6d4d1c"),
  "product_id": ObjectId("5deddcf795b70729ea6d4d17"),
  "lang": "Gr",
  "description": "",
  "created_at": ISODate("2019-12-09T05:34:47.658Z"),
  "updated_at": ISODate("2019-12-09T05:34:47.658Z")
});
db.getCollection("product_contents").insert({
  "_id": ObjectId("5deddcf795b70729ea6d4d1e"),
  "product_id": ObjectId("5deddcf795b70729ea6d4d17"),
  "lang": "Sp",
  "description": "",
  "created_at": ISODate("2019-12-09T05:34:47.663Z"),
  "updated_at": ISODate("2019-12-09T05:34:47.663Z")
});

/** product_featured records **/
db.getCollection("product_featured").insert({
  "_id": ObjectId("5ccc09c9a1a398298eb35189"),
  "product_id": ObjectId("5d9e21cf7dbdaa69d72e5ed5"),
  "created_at": ISODate("2019-05-03T09:28:41.735Z"),
  "updated_at": ISODate("2019-10-09T18:07:11.384Z"),
  "description": "therapy at home",
  "left_1_icon": "/icons/FontAwesome_f0c0(11)_256.png",
  "left_1_text": "Dual tasking this is the area for the text on the left",
  "left_1_title": "Benefits",
  "left_2_icon": "/icons/octicons_f042(9)_48.png",
  "left_2_text": "This product is very Reasonable in price",
  "left_2_title": "Reasonable",
  "left_3_icon": "/icons/FontAwesome_f110(2)_256.png",
  "left_3_text": "The text will  be here - I am thinking if there is not enough features on a certain product we should be able to hide what will not be in use",
  "left_3_title": "Title 3 on the left",
  "right_1_icon": "/icons/octicons_f037(8)_48.png",
  "right_1_text": "Very easy to use for user.",
  "right_1_title": "User Friendly",
  "right_2_icon": "/icons/linecons_e008(15)_256.png",
  "right_2_text": "and here",
  "right_2_title": "what?",
  "right_3_icon": "/icons/linecons_e02f(16)_256.png",
  "right_3_text": "here",
  "right_3_title": "what is this"
});

/** product_files records **/
db.getCollection("product_files").insert({
  "_id": ObjectId("5d53fd4fcfe3ee5c90a8bf14"),
  "product_id": ObjectId("5d4bb3d8a9421434c38e69be"),
  "accessory_manuals": "",
  "assembly_installation": "",
  "brochures": "/LGI360E_04.png",
  "created_at": ISODate("2019-08-14T12:23:43.443Z"),
  "gaitsens": "",
  "harness_instructions": "/LG300-Gkmini-04-4678-Trainer-Shadow.png",
  "maintenance_procedures": "",
  "product_datasheet": "/GKS20_01.jpg",
  "treadmill_maintenance": "",
  "treadmill_manuals": "/HNG_350_01-2s.gif",
  "updated_at": ISODate("2019-08-14T12:23:43.443Z"),
  "user_manual": "/0001.jpg"
});
db.getCollection("product_files").insert({
  "_id": ObjectId("5d53fd67cfe3ee5c90a8bf2d"),
  "product_id": ObjectId("5d2debf9837f487c405e630d"),
  "accessory_manuals": "/LG300DLX-01-4715-Shadows.png",
  "assembly_installation": "/GaitKeeper Banner - 1920x656 - B.jpg",
  "brochures": "",
  "created_at": ISODate("2019-08-14T12:24:07.576Z"),
  "gaitsens": "/WK_75_01-2s.gif",
  "harness_instructions": "",
  "maintenance_procedures": "/about_contact_bg.jpg",
  "product_datasheet": "",
  "treadmill_maintenance": "/LGI360E_04.png",
  "treadmill_manuals": "",
  "updated_at": ISODate("2019-08-14T12:24:07.576Z"),
  "user_manual": ""
});
db.getCollection("product_files").insert({
  "_id": ObjectId("5d53fd6ecfe3ee5c90a8bf3d"),
  "product_id": ObjectId("5cffd17c60b1b8748527ee2d"),
  "accessory_manuals": "",
  "assembly_installation": "",
  "brochures": "",
  "created_at": ISODate("2019-08-14T12:24:14.880Z"),
  "gaitsens": "/about_video.jpg",
  "harness_instructions": "/about_contact_bg.jpg",
  "maintenance_procedures": "/GaitKeeper Banner - 1920x656 - B.jpg",
  "product_datasheet": "",
  "treadmill_maintenance": "",
  "treadmill_manuals": "/gallery10.jpg",
  "updated_at": ISODate("2019-08-14T12:24:34.952Z"),
  "user_manual": ""
});
db.getCollection("product_files").insert({
  "_id": ObjectId("5d53fd92cfe3ee5c90a8bf6d"),
  "product_id": ObjectId("5cd198ce06e67e44fa0eb2e4"),
  "accessory_manuals": "",
  "assembly_installation": "",
  "brochures": "",
  "created_at": ISODate("2019-08-14T12:24:50.994Z"),
  "gaitsens": "",
  "harness_instructions": "/LiteGait_StickerPage500Ev2.pdf",
  "maintenance_procedures": "",
  "product_datasheet": [
    "/Product_Docs/Data_Sheets/400STND_US_032119.pdf"
  ],
  "treadmill_maintenance": "",
  "treadmill_manuals": "",
  "updated_at": ISODate("2019-10-28T16:42:23.971Z"),
  "user_manual": "",
  "product_datasheet_name": [
    "400 Standard"
  ]
});
db.getCollection("product_files").insert({
  "_id": ObjectId("5d542f4dcfe3ee5c90a93b18"),
  "product_id": ObjectId("5c24667cd4568d68ad108584"),
  "accessory_manuals": "",
  "assembly_installation": "",
  "brochures": "",
  "created_at": ISODate("2019-08-14T15:57:01.247Z"),
  "gaitsens": "",
  "harness_instructions": "",
  "maintenance_procedures": "",
  "product_datasheet": [
    "/Product_Docs/Data_Sheets/500STND_US_011619.pdf",
    "/Product_Docs/Data_Sheets/500DLX_US_082719.pdf"
  ],
  "treadmill_maintenance": "",
  "treadmill_manuals": "",
  "updated_at": ISODate("2019-10-28T16:39:53.648Z"),
  "user_manual": "",
  "product_datasheet_name": [
    "500 Standard",
    "500 Deluxe"
  ]
});

/** product_gallery records **/
db.getCollection("product_gallery").insert({
  "_id": ObjectId("5ccc103806e67e44fa0eb23c"),
  "product_id": ObjectId("5ccc103806e67e44fa0eb233"),
  "image": "/accessories_img1.jpg",
  "created_at": ISODate("2019-01-25T11:01:51.892Z"),
  "updated_at": ISODate("2019-01-25T11:01:51.892Z")
});
db.getCollection("product_gallery").insert({
  "_id": ObjectId("5ccc103806e67e44fa0eb23f"),
  "product_id": ObjectId("5ccc103806e67e44fa0eb233"),
  "image": "/accessories_img6.jpg",
  "created_at": ISODate("2019-01-25T11:01:51.892Z"),
  "updated_at": ISODate("2019-01-25T11:01:51.892Z")
});
db.getCollection("product_gallery").insert({
  "_id": ObjectId("5cd198ce06e67e44fa0eb2ed"),
  "product_id": ObjectId("5cd198ce06e67e44fa0eb2e4"),
  "image": "/gallery1.jpg",
  "created_at": ISODate("2019-05-07T14:40:14.433Z"),
  "updated_at": ISODate("2019-05-07T14:40:14.433Z")
});
db.getCollection("product_gallery").insert({
  "_id": ObjectId("5cd198ce06e67e44fa0eb2ee"),
  "product_id": ObjectId("5cd198ce06e67e44fa0eb2e4"),
  "image": "/product_img3.jpg",
  "created_at": ISODate("2019-05-07T14:40:14.433Z"),
  "updated_at": ISODate("2019-05-07T14:40:14.433Z")
});
db.getCollection("product_gallery").insert({
  "_id": ObjectId("5cd1a2bb06e67e44fa0eb301"),
  "product_id": ObjectId("5cd1a2bb06e67e44fa0eb2f8"),
  "image": "/accessories_img3.jpg",
  "created_at": ISODate("2019-05-07T15:22:35.420Z"),
  "updated_at": ISODate("2019-05-07T15:22:35.420Z")
});
db.getCollection("product_gallery").insert({
  "_id": ObjectId("5cd1a2bb06e67e44fa0eb302"),
  "product_id": ObjectId("5cd1a2bb06e67e44fa0eb2f8"),
  "image": "/accessories_img5.jpg",
  "created_at": ISODate("2019-05-07T15:22:35.420Z"),
  "updated_at": ISODate("2019-05-07T15:22:35.420Z")
});
db.getCollection("product_gallery").insert({
  "_id": ObjectId("5cd1abc906e67e44fa0eb332"),
  "product_id": ObjectId("5cd1a2bb06e67e44fa0eb2f8"),
  "image": "/LG300DLX-01-4715-Shadows.png",
  "created_at": ISODate("2019-05-07T16:01:13.702Z"),
  "updated_at": ISODate("2019-05-07T16:01:13.702Z")
});
db.getCollection("product_gallery").insert({
  "_id": ObjectId("5cd1abc906e67e44fa0eb334"),
  "product_id": ObjectId("5cd1a2bb06e67e44fa0eb2f8"),
  "image": "/LGI360E_04.png",
  "created_at": ISODate("2019-05-07T16:01:13.702Z"),
  "updated_at": ISODate("2019-05-07T16:01:13.702Z")
});
db.getCollection("product_gallery").insert({
  "_id": ObjectId("5cd1abfb06e67e44fa0eb33a"),
  "product_id": ObjectId("5cd1a2bb06e67e44fa0eb2f8"),
  "image": "/LG300DLX-01-4715-Shadows.png",
  "created_at": ISODate("2019-05-07T16:02:03.977Z"),
  "updated_at": ISODate("2019-05-07T16:02:03.977Z")
});
db.getCollection("product_gallery").insert({
  "_id": ObjectId("5cf00a39fb42690c45d441db"),
  "product_id": ObjectId("5cf00a39fb42690c45d441d2"),
  "image": "/gallery10.jpg",
  "created_at": ISODate("2019-05-30T16:52:09.24Z"),
  "updated_at": ISODate("2019-05-30T16:52:09.24Z")
});
db.getCollection("product_gallery").insert({
  "_id": ObjectId("5d436c1a82d439799366714d"),
  "product_id": ObjectId("5d2debf9837f487c405e630d"),
  "image": "/Product/HugNgo/hugngo_250_350_pair.jpg",
  "created_at": ISODate("2019-08-01T22:47:54.247Z"),
  "updated_at": ISODate("2019-08-01T22:47:54.247Z")
});
db.getCollection("product_gallery").insert({
  "_id": ObjectId("5d436c1a82d439799366714e"),
  "product_id": ObjectId("5d2debf9837f487c405e630d"),
  "image": "/Product/HugNgo/hugngo_250.jpg",
  "created_at": ISODate("2019-08-01T22:47:54.247Z"),
  "updated_at": ISODate("2019-08-01T22:47:54.247Z")
});
db.getCollection("product_gallery").insert({
  "_id": ObjectId("5d436c1a82d439799366714f"),
  "product_id": ObjectId("5d2debf9837f487c405e630d"),
  "image": "/Product/HugNgo/hugngo_350.jpg",
  "created_at": ISODate("2019-08-01T22:47:54.247Z"),
  "updated_at": ISODate("2019-08-01T22:47:54.247Z")
});
db.getCollection("product_gallery").insert({
  "_id": ObjectId("5d436c1a82d4397993667150"),
  "product_id": ObjectId("5d2debf9837f487c405e630d"),
  "image": "/Product/HugNgo/hugngo_350_device.jpg",
  "created_at": ISODate("2019-08-01T22:47:54.247Z"),
  "updated_at": ISODate("2019-08-01T22:47:54.247Z")
});
db.getCollection("product_gallery").insert({
  "_id": ObjectId("5d436ded82d4397993667162"),
  "product_id": ObjectId("5d0bb37960b1b8748527ef97"),
  "image": "/Product/LG4H/lg4h110_gkmini.jpg",
  "created_at": ISODate("2019-08-01T22:55:41.958Z"),
  "updated_at": ISODate("2019-08-01T22:55:41.958Z")
});
db.getCollection("product_gallery").insert({
  "_id": ObjectId("5d436eb382d4397993667179"),
  "product_id": ObjectId("5d2debf9837f487c405e630d"),
  "image": "/Product/HugNgo/hugngo_250_350_pair.jpg",
  "created_at": ISODate("2019-08-01T22:58:59.757Z"),
  "updated_at": ISODate("2019-08-01T22:58:59.757Z")
});
db.getCollection("product_gallery").insert({
  "_id": ObjectId("5d436ed882d439799366717f"),
  "product_id": ObjectId("5d2debf9837f487c405e630d"),
  "image": "/Product/HugNgo/hugngo_350_device.jpg",
  "created_at": ISODate("2019-08-01T22:59:36.104Z"),
  "updated_at": ISODate("2019-08-01T22:59:36.104Z")
});
db.getCollection("product_gallery").insert({
  "_id": ObjectId("5d43705182d439799366718c"),
  "product_id": ObjectId("5d02757a60b1b8748527ee95"),
  "image": "/Product/GaitKeeper/gk_s20_01.jpg",
  "created_at": ISODate("2019-08-01T23:05:53.926Z"),
  "updated_at": ISODate("2019-08-01T23:05:53.926Z")
});
db.getCollection("product_gallery").insert({
  "_id": ObjectId("5d4370d582d4397993667193"),
  "product_id": ObjectId("5d02757a60b1b8748527ee95"),
  "image": "/Product/GaitKeeper/gk_s20_02.jpg",
  "created_at": ISODate("2019-08-01T23:08:05.802Z"),
  "updated_at": ISODate("2019-08-01T23:08:05.802Z")
});
db.getCollection("product_gallery").insert({
  "_id": ObjectId("5d4370d582d4397993667194"),
  "product_id": ObjectId("5d02757a60b1b8748527ee95"),
  "image": "/Product/GaitKeeper/gk_s22_01.jpg",
  "created_at": ISODate("2019-08-01T23:08:05.802Z"),
  "updated_at": ISODate("2019-08-01T23:08:05.802Z")
});
db.getCollection("product_gallery").insert({
  "_id": ObjectId("5d43724782d43979936671a4"),
  "product_id": ObjectId("5d02700260b1b8748527ee74"),
  "image": "/Product/LG200P/lg200p_device.jpg",
  "created_at": ISODate("2019-08-01T23:14:15.448Z"),
  "updated_at": ISODate("2019-08-01T23:14:15.448Z")
});
db.getCollection("product_gallery").insert({
  "_id": ObjectId("5d43724782d43979936671a5"),
  "product_id": ObjectId("5d02700260b1b8748527ee74"),
  "image": "/Product/LG200P/lg200p_trampoline.jpg",
  "created_at": ISODate("2019-08-01T23:14:15.448Z"),
  "updated_at": ISODate("2019-08-01T23:14:15.448Z")
});
db.getCollection("product_gallery").insert({
  "_id": ObjectId("5d4372d282d43979936671b2"),
  "product_id": ObjectId("5d02700260b1b8748527ee74"),
  "image": "/Product/LG200P/lg200p_gkmini_toddler.jpg",
  "created_at": ISODate("2019-08-01T23:16:34.530Z"),
  "updated_at": ISODate("2019-08-01T23:16:34.530Z")
});
db.getCollection("product_gallery").insert({
  "_id": ObjectId("5d4373ac82d43979936671c0"),
  "product_id": ObjectId("5cffd17c60b1b8748527ee2d"),
  "image": "/Product/LG100mx/lg100mx_trampoline_therapist.jpg",
  "created_at": ISODate("2019-08-01T23:20:12.741Z"),
  "updated_at": ISODate("2019-08-01T23:20:12.741Z")
});
db.getCollection("product_gallery").insert({
  "_id": ObjectId("5d43751982d43979936671c8"),
  "product_id": ObjectId("5cffd17c60b1b8748527ee2d"),
  "image": "/Product/LG100mx/lg100mx_overground_family.jpg",
  "created_at": ISODate("2019-08-01T23:26:17.903Z"),
  "updated_at": ISODate("2019-08-01T23:26:17.903Z")
});
db.getCollection("product_gallery").insert({
  "_id": ObjectId("5d43760f82d43979936671d3"),
  "product_id": ObjectId("5cffd17c60b1b8748527ee2d"),
  "image": "/Product/LG100mx/lg200mx_100mx_pair.jpg",
  "created_at": ISODate("2019-08-01T23:30:23.925Z"),
  "updated_at": ISODate("2019-08-01T23:30:23.925Z")
});
db.getCollection("product_gallery").insert({
  "_id": ObjectId("5d43795482d43979936671e0"),
  "product_id": ObjectId("5c24667cd4568d68ad108584"),
  "image": "/Product/LG500/lg500_sdt_trio.jpg",
  "created_at": ISODate("2019-08-01T23:44:20.701Z"),
  "updated_at": ISODate("2019-08-01T23:44:20.701Z")
});
db.getCollection("product_gallery").insert({
  "_id": ObjectId("5d43795482d43979936671e1"),
  "product_id": ObjectId("5c24667cd4568d68ad108584"),
  "image": "/Product/LG500/lg500_dlx_lowered.jpg",
  "created_at": ISODate("2019-08-01T23:44:20.701Z"),
  "updated_at": ISODate("2019-08-01T23:44:20.701Z")
});
db.getCollection("product_gallery").insert({
  "_id": ObjectId("5d43795482d43979936671e2"),
  "product_id": ObjectId("5c24667cd4568d68ad108584"),
  "image": "/Product/LG500/lg500_dlx_harness_tablet.jpg",
  "created_at": ISODate("2019-08-01T23:44:20.701Z"),
  "updated_at": ISODate("2019-08-01T23:44:20.701Z")
});
db.getCollection("product_gallery").insert({
  "_id": ObjectId("5d437a0d82d43979936671eb"),
  "product_id": ObjectId("5c24667cd4568d68ad108584"),
  "image": "/Product/LG500/lg500_sdt_trio.jpg",
  "created_at": ISODate("2019-08-01T23:47:25.458Z"),
  "updated_at": ISODate("2019-08-01T23:47:25.458Z")
});
db.getCollection("product_gallery").insert({
  "_id": ObjectId("5d437a0d82d43979936671ec"),
  "product_id": ObjectId("5c24667cd4568d68ad108584"),
  "image": "/Product/LG500/lg500_tall_gks22_therapist_bars.jpg",
  "created_at": ISODate("2019-08-01T23:47:25.458Z"),
  "updated_at": ISODate("2019-08-01T23:47:25.458Z")
});
db.getCollection("product_gallery").insert({
  "_id": ObjectId("5d4bb3d8a9421434c38e69c7"),
  "product_id": ObjectId("5d4bb3d8a9421434c38e69be"),
  "image": "/gallery10.jpg",
  "created_at": ISODate("2019-08-08T05:32:08.633Z"),
  "updated_at": ISODate("2019-08-08T05:32:08.633Z")
});
db.getCollection("product_gallery").insert({
  "_id": ObjectId("5d51ecd1fbb7781db271cc06"),
  "product_id": ObjectId("5bd16edbb605151b7e93d899"),
  "image": "/Product/LG300P/lg300p_def.png",
  "created_at": ISODate("2019-08-12T22:48:49.871Z"),
  "updated_at": ISODate("2019-08-12T22:48:49.871Z")
});
db.getCollection("product_gallery").insert({
  "_id": ObjectId("5d51ecd1fbb7781db271cc07"),
  "product_id": ObjectId("5bd16edbb605151b7e93d899"),
  "image": "/Product/LG300P/lg300p_lower.jpg",
  "created_at": ISODate("2019-08-12T22:48:49.871Z"),
  "updated_at": ISODate("2019-08-12T22:48:49.871Z")
});
db.getCollection("product_gallery").insert({
  "_id": ObjectId("5d51edf9fbb7781db271cc1b"),
  "product_id": ObjectId("5d4bb3d8a9421434c38e69be"),
  "image": "/Product/GaitSens/gaitsens_live_screen_tablet.jpg",
  "created_at": ISODate("2019-08-12T22:53:45.307Z"),
  "updated_at": ISODate("2019-08-12T22:53:45.307Z")
});
db.getCollection("product_gallery").insert({
  "_id": ObjectId("5d51f027fbb7781db271cc26"),
  "product_id": ObjectId("5d4287e182d43979936670b9"),
  "image": "/Product/GaitKeeper/gkmini_handlebar.jpg",
  "created_at": ISODate("2019-08-12T23:03:03.249Z"),
  "updated_at": ISODate("2019-08-12T23:03:03.249Z")
});
db.getCollection("product_gallery").insert({
  "_id": ObjectId("5d51f027fbb7781db271cc28"),
  "product_id": ObjectId("5d4287e182d43979936670b9"),
  "image": "/Product/GaitKeeper/gkmini_200mx_therapist.jpg",
  "created_at": ISODate("2019-08-12T23:03:03.249Z"),
  "updated_at": ISODate("2019-08-12T23:03:03.249Z")
});
db.getCollection("product_gallery").insert({
  "_id": ObjectId("5d51f027fbb7781db271cc27"),
  "product_id": ObjectId("5d4287e182d43979936670b9"),
  "image": "/Product/GaitKeeper/gkmini_portable.jpg",
  "created_at": ISODate("2019-08-12T23:03:03.249Z"),
  "updated_at": ISODate("2019-08-12T23:03:03.249Z")
});
db.getCollection("product_gallery").insert({
  "_id": ObjectId("5d51f027fbb7781db271cc29"),
  "product_id": ObjectId("5d4287e182d43979936670b9"),
  "image": "/Product/GaitKeeper/gkmini_lg400t_therapist.jpg",
  "created_at": ISODate("2019-08-12T23:03:03.249Z"),
  "updated_at": ISODate("2019-08-12T23:03:03.249Z")
});
db.getCollection("product_gallery").insert({
  "_id": ObjectId("5d51f3b2fbb7781db271cc4a"),
  "product_id": ObjectId("5d02757a60b1b8748527ee95"),
  "image": "/Product/GaitKeeper/gk_s20_tm_lg400.jpg",
  "created_at": ISODate("2019-08-12T23:18:10.707Z"),
  "updated_at": ISODate("2019-08-12T23:18:10.707Z")
});
db.getCollection("product_gallery").insert({
  "_id": ObjectId("5d7a6662f3c6d306896b2817"),
  "product_id": ObjectId("5d7a6662f3c6d306896b280e"),
  "image": "/Product/LGV/lgv_treadmill_therapist.jpg",
  "created_at": ISODate("2019-09-12T15:38:10.166Z"),
  "updated_at": ISODate("2019-09-12T15:38:10.166Z")
});
db.getCollection("product_gallery").insert({
  "_id": ObjectId("5dbc7395720c200ccbf7acf5"),
  "product_id": ObjectId("5cd1a2bb06e67e44fa0eb2f8"),
  "image": "/Product/LG300/300D.png",
  "created_at": ISODate("2019-11-01T18:04:05.186Z"),
  "updated_at": ISODate("2019-11-01T18:04:05.186Z")
});
db.getCollection("product_gallery").insert({
  "_id": ObjectId("5dbc7421720c200ccbf7ad06"),
  "product_id": ObjectId("5bd16edbb605151b7e93d899"),
  "image": "/Product/LG300P/300PD.png",
  "created_at": ISODate("2019-11-01T18:06:25.541Z"),
  "updated_at": ISODate("2019-11-01T18:06:25.541Z")
});
db.getCollection("product_gallery").insert({
  "_id": ObjectId("5dbc74ec720c200ccbf7ad0f"),
  "product_id": ObjectId("5cd198ce06e67e44fa0eb2e4"),
  "image": "/Product/LG400/400S.png",
  "created_at": ISODate("2019-11-01T18:09:48.26Z"),
  "updated_at": ISODate("2019-11-01T18:09:48.26Z")
});
db.getCollection("product_gallery").insert({
  "_id": ObjectId("5dbc75a0720c200ccbf7ad1b"),
  "product_id": ObjectId("5c24667cd4568d68ad108584"),
  "image": "/Product/LG500/500D.png",
  "created_at": ISODate("2019-11-01T18:12:48.835Z"),
  "updated_at": ISODate("2019-11-01T18:12:48.835Z")
});
db.getCollection("product_gallery").insert({
  "_id": ObjectId("5deada7495b70729ea6d4ccd"),
  "product_id": ObjectId("5deada7495b70729ea6d4cc3"),
  "image": "/Product/HugNgo/hugngo_250_350_pair.jpg",
  "created_at": ISODate("2019-12-06T22:47:16.881Z"),
  "updated_at": ISODate("2019-12-06T22:47:16.881Z")
});
db.getCollection("product_gallery").insert({
  "_id": ObjectId("5deddc9295b70729ea6d4d04"),
  "product_id": ObjectId("5d0bb37960b1b8748527ef97"),
  "image": "/Product/LG4H/LG4H220-Gkmini-GaiterStool-04-1041-Isolated.png",
  "created_at": ISODate("2019-12-09T05:33:06.428Z"),
  "updated_at": ISODate("2019-12-09T05:33:06.428Z")
});

/** product_specification records **/
db.getCollection("product_specification").insert({
  "_id": ObjectId("5df7253122d6d8661dd452f4"),
  "model": "75W",
  "Market": "US",
  "Units": "in/lbs",
  "Max_user_weight": "75 lbs",
  "Overall_height": "5'-  1\"",
  "Max_user_height_overground": "4'- 10\"",
  "Max_user_height_over_6\"_treadmill": "4'- 4\"",
  "Removable_FreeDome": "Optional",
  "Max_user_height_overground_(rfd)": "4'- 4\"",
  "Max_user_height_over_6\"_treadmill_(rfd)": "3'- 10\"",
  "Integrated_FreeDome": "Not Available",
  "Max_user_height_overground_(ifd)": null,
  "Max_user_height_over_6\"_treadmill_(ifd)": null,
  "Powered_stroke_adjustment": "N/A",
  "Manual_stroke_adjustment": "28",
  "Base_options": "25/27/30",
  "Max_footprint_(base_x_width_x_length)": "33 x 32.5",
  "Standard_Base_width": NumberInt(25),
  "Max_treadmill_width_(sb)": "24",
  "Min_door_width_(sb)": "29",
  "Footprint_(width_x_length)_(sb)": "28 x 32.5",
  "Base_option_2_width": NumberInt(27),
  "Max_treadmill_width_(bo2)": "26",
  "Min_door_width_(bo2)": "31",
  "Footprint_(width_x_length)_(bo2)": "30 x 32.5",
  "Base_option_3_width": NumberInt(30),
  "Max_treadmill_width_(bo3)": "29",
  "Min_door_width_(bo3)": "34",
  "Footprint_(width_x_length)_(bo3)": "33 x 32.5",
  "Clearance_(floor_to_bottom_of_base_U)": "13",
  "Length_of_base_leg": "32.5",
  "Width_of_base_leg": "1.5",
  "Height_of_base_leg_from_ground": "5.5",
  "Caster_diameter": "3",
  "Harness_included": "1 Harness",
  "BiSym_Tablet": "Optional",
  "GaiterStool": "Optional",
  "3D_Handlebar": "Optional",
  "Parallel_Grab_Bar": "Included",
  "L-Shape_Handlears": "Not Available",
  "Q-Straps_-_3_resistances": "Optional",
  "Power_requirements": null,
  "created_at": ISODate("2019-12-16T06:33:21.721Z"),
  "updated_at": ISODate("2019-12-16T06:33:21.721Z")
});
db.getCollection("product_specification").insert({
  "_id": ObjectId("5df7253122d6d8661dd452f8"),
  "model": "100MX",
  "Market": "US",
  "Units": "in/lbs",
  "Max_user_weight": "100 lbs",
  "Overall_height": "5'-  5\"",
  "Max_user_height_overground": "5'- 2\"",
  "Max_user_height_over_6\"_treadmill": "4'- 8\"",
  "Removable_FreeDome": "Optional",
  "Max_user_height_overground_(rfd)": "4'- 8\"",
  "Max_user_height_over_6\"_treadmill_(rfd)": "4'- 2\"",
  "Integrated_FreeDome": "Not Available",
  "Max_user_height_overground_(ifd)": null,
  "Max_user_height_over_6\"_treadmill_(ifd)": null,
  "Powered_stroke_adjustment": "18",
  "Manual_stroke_adjustment": "27",
  "Base_options": "25/27/30",
  "Max_footprint_(base_x_width_x_length)": "33 x 32.5",
  "Standard_Base_width": NumberInt(25),
  "Max_treadmill_width_(sb)": "24",
  "Min_door_width_(sb)": "29",
  "Footprint_(width_x_length)_(sb)": "28 x 32.5",
  "Base_option_2_width": NumberInt(27),
  "Max_treadmill_width_(bo2)": "26",
  "Min_door_width_(bo2)": "31",
  "Footprint_(width_x_length)_(bo2)": "30 x 32.5",
  "Base_option_3_width": NumberInt(30),
  "Max_treadmill_width_(bo3)": "29",
  "Min_door_width_(bo3)": "34",
  "Footprint_(width_x_length)_(bo3)": "33 x 32.5",
  "Clearance_(floor_to_bottom_of_base_U)": "13",
  "Length_of_base_leg": "32.5",
  "Width_of_base_leg": "1.5",
  "Height_of_base_leg_from_ground": "5.5",
  "Caster_diameter": "3",
  "Harness_included": "1 Harness",
  "BiSym_Tablet": "Optional",
  "GaiterStool": "Optional",
  "3D_Handlebar": "Optional",
  "Parallel_Grab_Bar": "Included",
  "L-Shape_Handlears": "Not Available",
  "Q-Straps_-_3_resistances": "Optional",
  "Power_requirements": "110V-240V 50/60Hz",
  "created_at": ISODate("2019-12-16T06:33:21.721Z"),
  "updated_at": ISODate("2019-12-16T06:33:21.721Z")
});
db.getCollection("product_specification").insert({
  "_id": ObjectId("5df7253122d6d8661dd452fa"),
  "model": "100FX",
  "Market": "US",
  "Units": "in/lbs",
  "Max_user_weight": "100 lbs",
  "Overall_height": "5'-  8\"",
  "Max_user_height_overground": "5'- 2\"",
  "Max_user_height_over_6\"_treadmill": "4'- 8\"",
  "Removable_FreeDome": "Not Available",
  "Max_user_height_overground_(rfd)": "5'- 2\"",
  "Max_user_height_over_6\"_treadmill_(rfd)": "4'- 8\"",
  "Integrated_FreeDome": "Included",
  "Max_user_height_overground_(ifd)": "5'- 2\"",
  "Max_user_height_over_6\"_treadmill_(ifd)": "4'- 8\"",
  "Powered_stroke_adjustment": "18",
  "Manual_stroke_adjustment": "27",
  "Base_options": "25/27/30",
  "Max_footprint_(base_x_width_x_length)": "33 x 32.5",
  "Standard_Base_width": NumberInt(25),
  "Max_treadmill_width_(sb)": "24",
  "Min_door_width_(sb)": "29",
  "Footprint_(width_x_length)_(sb)": "28 x 32.5",
  "Base_option_2_width": NumberInt(27),
  "Max_treadmill_width_(bo2)": "26",
  "Min_door_width_(bo2)": "31",
  "Footprint_(width_x_length)_(bo2)": "30 x 32.5",
  "Base_option_3_width": NumberInt(30),
  "Max_treadmill_width_(bo3)": "29",
  "Min_door_width_(bo3)": "34",
  "Footprint_(width_x_length)_(bo3)": "33 x 32.5",
  "Clearance_(floor_to_bottom_of_base_U)": "13",
  "Length_of_base_leg": "32.5",
  "Width_of_base_leg": "1.5",
  "Height_of_base_leg_from_ground": "5.5",
  "Caster_diameter": "3",
  "Harness_included": "1 Harness",
  "BiSym_Tablet": "Optional",
  "GaiterStool": "Optional",
  "3D_Handlebar": "Optional",
  "Parallel_Grab_Bar": "Included",
  "L-Shape_Handlears": "Not Available",
  "Q-Straps_-_3_resistances": "Optional",
  "Power_requirements": "110V-240V 50/60Hz",
  "created_at": ISODate("2019-12-16T06:33:21.721Z"),
  "updated_at": ISODate("2019-12-16T06:33:21.721Z")
});
db.getCollection("product_specification").insert({
  "_id": ObjectId("5df7253122d6d8661dd452fc"),
  "model": "150JR",
  "Market": "US",
  "Units": "in/lbs",
  "Max_user_weight": "150 lbs",
  "Overall_height": "6'-  2\"",
  "Max_user_height_overground": "5'- 8\"",
  "Max_user_height_over_6\"_treadmill": "5'- 2\"",
  "Removable_FreeDome": "Optional",
  "Max_user_height_overground_(rfd)": "5'- 2\"",
  "Max_user_height_over_6\"_treadmill_(rfd)": "4'- 8\"",
  "Integrated_FreeDome": "Not Available",
  "Max_user_height_overground_(ifd)": null,
  "Max_user_height_over_6\"_treadmill_(ifd)": null,
  "Powered_stroke_adjustment": "N/A",
  "Manual_stroke_adjustment": "32",
  "Base_options": "27/30/34",
  "Max_footprint_(base_x_width_x_length)": "38 x 42.5",
  "Standard_Base_width": NumberInt(27),
  "Max_treadmill_width_(sb)": "26",
  "Min_door_width_(sb)": "32",
  "Footprint_(width_x_length)_(sb)": "31 x 42.5",
  "Base_option_2_width": NumberInt(30),
  "Max_treadmill_width_(bo2)": "29",
  "Min_door_width_(bo2)": "35",
  "Footprint_(width_x_length)_(bo2)": "34 x 42.5",
  "Base_option_3_width": NumberInt(34),
  "Max_treadmill_width_(bo3)": "33",
  "Min_door_width_(bo3)": "39",
  "Footprint_(width_x_length)_(bo3)": "38 x 42.5",
  "Clearance_(floor_to_bottom_of_base_U)": "18",
  "Length_of_base_leg": "42.5",
  "Width_of_base_leg": "2",
  "Height_of_base_leg_from_ground": "8",
  "Caster_diameter": "5",
  "Harness_included": "1 Harness",
  "BiSym_Tablet": "Optional",
  "GaiterStool": "Optional",
  "3D_Handlebar": "Optional",
  "Parallel_Grab_Bar": "Included",
  "L-Shape_Handlears": "Not Available",
  "Q-Straps_-_3_resistances": "Optional",
  "Power_requirements": null,
  "created_at": ISODate("2019-12-16T06:33:21.721Z"),
  "updated_at": ISODate("2019-12-16T06:33:21.721Z")
});
db.getCollection("product_specification").insert({
  "_id": ObjectId("5df7253122d6d8661dd452fe"),
  "model": "200JR",
  "Market": "US",
  "Units": "in/lbs",
  "Max_user_weight": "200 lbs",
  "Overall_height": "6'-  8\"",
  "Max_user_height_overground": "6'- 2\"",
  "Max_user_height_over_6\"_treadmill": "5'- 8\"",
  "Removable_FreeDome": "Optional",
  "Max_user_height_overground_(rfd)": "5'- 8\"",
  "Max_user_height_over_6\"_treadmill_(rfd)": "5'- 2\"",
  "Integrated_FreeDome": "Not Available",
  "Max_user_height_overground_(ifd)": null,
  "Max_user_height_over_6\"_treadmill_(ifd)": null,
  "Powered_stroke_adjustment": "N/A",
  "Manual_stroke_adjustment": "32",
  "Base_options": "30/27/34",
  "Max_footprint_(base_x_width_x_length)": "38 x 42.5",
  "Standard_Base_width": NumberInt(30),
  "Max_treadmill_width_(sb)": "29",
  "Min_door_width_(sb)": "35",
  "Footprint_(width_x_length)_(sb)": "31 x 42.5",
  "Base_option_2_width": NumberInt(27),
  "Max_treadmill_width_(bo2)": "26",
  "Min_door_width_(bo2)": "32",
  "Footprint_(width_x_length)_(bo2)": "34 x 42.5",
  "Base_option_3_width": NumberInt(34),
  "Max_treadmill_width_(bo3)": "33",
  "Min_door_width_(bo3)": "39",
  "Footprint_(width_x_length)_(bo3)": "38 x 42.5",
  "Clearance_(floor_to_bottom_of_base_U)": "18",
  "Length_of_base_leg": "42.5",
  "Width_of_base_leg": "2",
  "Height_of_base_leg_from_ground": "8",
  "Caster_diameter": "5",
  "Harness_included": "1 Harness",
  "BiSym_Tablet": "Optional",
  "GaiterStool": "Optional",
  "3D_Handlebar": "Optional",
  "Parallel_Grab_Bar": "Included",
  "L-Shape_Handlears": "Not Available",
  "Q-Straps_-_3_resistances": "Optional",
  "Power_requirements": null,
  "created_at": ISODate("2019-12-16T06:33:21.721Z"),
  "updated_at": ISODate("2019-12-16T06:33:21.721Z")
});
db.getCollection("product_specification").insert({
  "_id": ObjectId("5df7253122d6d8661dd45302"),
  "model": "200P",
  "Market": "US",
  "Units": "in/lbs",
  "Max_user_weight": "200 lbs",
  "Overall_height": "7'-  3\"",
  "Max_user_height_overground": "6'- 6\"",
  "Max_user_height_over_6\"_treadmill": "6'- 0\"",
  "Removable_FreeDome": "Optional",
  "Max_user_height_overground_(rfd)": "6'- 0\"",
  "Max_user_height_over_6\"_treadmill_(rfd)": "5'- 6\"",
  "Integrated_FreeDome": "Not Available",
  "Max_user_height_overground_(ifd)": null,
  "Max_user_height_over_6\"_treadmill_(ifd)": null,
  "Powered_stroke_adjustment": "28",
  "Manual_stroke_adjustment": "20",
  "Base_options": "30/27",
  "Max_footprint_(base_x_width_x_length)": "34 x 42.5",
  "Standard_Base_width": NumberInt(30),
  "Max_treadmill_width_(sb)": "29",
  "Min_door_width_(sb)": "35",
  "Footprint_(width_x_length)_(sb)": "31 x 42.5",
  "Base_option_2_width": NumberInt(27),
  "Max_treadmill_width_(bo2)": "26",
  "Min_door_width_(bo2)": "32",
  "Footprint_(width_x_length)_(bo2)": "34 x 42.5",
  "Base_option_3_width": null,
  "Max_treadmill_width_(bo3)": null,
  "Min_door_width_(bo3)": null,
  "Footprint_(width_x_length)_(bo3)": null,
  "Clearance_(floor_to_bottom_of_base_U)": "16",
  "Length_of_base_leg": "42.5",
  "Width_of_base_leg": "2",
  "Height_of_base_leg_from_ground": "8",
  "Caster_diameter": "5",
  "Harness_included": "2 Harness",
  "BiSym_Tablet": "Optional",
  "GaiterStool": "Optional",
  "3D_Handlebar": "Included",
  "Parallel_Grab_Bar": "Optional",
  "L-Shape_Handlears": "Optional",
  "Q-Straps_-_3_resistances": "Included",
  "Power_requirements": "110V-240V 50/60Hz",
  "created_at": ISODate("2019-12-16T06:33:21.722Z"),
  "updated_at": ISODate("2019-12-16T06:33:21.722Z")
});
db.getCollection("product_specification").insert({
  "_id": ObjectId("5df7253122d6d8661dd45304"),
  "model": "300P - Standard",
  "Market": "US",
  "Units": "in/lbs",
  "Max_user_weight": "300 lbs",
  "Overall_height": "7'-  6\"",
  "Max_user_height_overground": "7'- 0\"",
  "Max_user_height_over_6\"_treadmill": "6'- 6\"",
  "Removable_FreeDome": "Optional",
  "Max_user_height_overground_(rfd)": "6'- 6\"",
  "Max_user_height_over_6\"_treadmill_(rfd)": "6'- 0\"",
  "Integrated_FreeDome": "Not Available",
  "Max_user_height_overground_(ifd)": null,
  "Max_user_height_over_6\"_treadmill_(ifd)": null,
  "Powered_stroke_adjustment": "28",
  "Manual_stroke_adjustment": "20",
  "Base_options": "30/34",
  "Max_footprint_(base_x_width_x_length)": "38.5 x 45.5",
  "Standard_Base_width": NumberInt(30),
  "Max_treadmill_width_(sb)": "29",
  "Min_door_width_(sb)": "35.5",
  "Footprint_(width_x_length)_(sb)": "34.5 x 45.5",
  "Base_option_2_width": NumberInt(34),
  "Max_treadmill_width_(bo2)": "33",
  "Min_door_width_(bo2)": "39.5",
  "Footprint_(width_x_length)_(bo2)": "38.5 x 45.5",
  "Base_option_3_width": null,
  "Max_treadmill_width_(bo3)": null,
  "Min_door_width_(bo3)": null,
  "Footprint_(width_x_length)_(bo3)": null,
  "Clearance_(floor_to_bottom_of_base_U)": "19",
  "Length_of_base_leg": "45.5",
  "Width_of_base_leg": "2.5",
  "Height_of_base_leg_from_ground": "8",
  "Caster_diameter": "6",
  "Harness_included": "2 Harness",
  "BiSym_Tablet": "Wired",
  "GaiterStool": "Optional",
  "3D_Handlebar": "Included",
  "Parallel_Grab_Bar": "Optional",
  "L-Shape_Handlears": "Optional",
  "Q-Straps_-_3_resistances": "Included",
  "Power_requirements": "110V-240V 50/60Hz",
  "created_at": ISODate("2019-12-16T06:33:21.722Z"),
  "updated_at": ISODate("2019-12-16T06:33:21.722Z")
});
db.getCollection("product_specification").insert({
  "_id": ObjectId("5df7253122d6d8661dd45306"),
  "model": "300P - Deluxe",
  "Market": "US",
  "Units": "in/lbs",
  "Max_user_weight": "300 lbs",
  "Overall_height": "7'-  6\"",
  "Max_user_height_overground": "7'- 0\"",
  "Max_user_height_over_6\"_treadmill": "6'- 6\"",
  "Removable_FreeDome": "Included",
  "Max_user_height_overground_(rfd)": "6'- 6\"",
  "Max_user_height_over_6\"_treadmill_(rfd)": "6'- 0\"",
  "Integrated_FreeDome": "Not Available",
  "Max_user_height_overground_(ifd)": null,
  "Max_user_height_over_6\"_treadmill_(ifd)": null,
  "Powered_stroke_adjustment": "28",
  "Manual_stroke_adjustment": "20",
  "Base_options": "30/34",
  "Max_footprint_(base_x_width_x_length)": "38.5 x 45.5",
  "Standard_Base_width": NumberInt(30),
  "Max_treadmill_width_(sb)": "29",
  "Min_door_width_(sb)": "35.5",
  "Footprint_(width_x_length)_(sb)": "34.5 x 45.5",
  "Base_option_2_width": NumberInt(34),
  "Max_treadmill_width_(bo2)": "33",
  "Min_door_width_(bo2)": "39.5",
  "Footprint_(width_x_length)_(bo2)": "38.5 x 45.5",
  "Base_option_3_width": null,
  "Max_treadmill_width_(bo3)": null,
  "Min_door_width_(bo3)": null,
  "Footprint_(width_x_length)_(bo3)": null,
  "Clearance_(floor_to_bottom_of_base_U)": "19",
  "Length_of_base_leg": "45.5",
  "Width_of_base_leg": "2.5",
  "Height_of_base_leg_from_ground": "8",
  "Caster_diameter": "6",
  "Harness_included": "2 Harness",
  "BiSym_Tablet": "Wired",
  "GaiterStool": "Included",
  "3D_Handlebar": "Included",
  "Parallel_Grab_Bar": "Optional",
  "L-Shape_Handlears": "Optional",
  "Q-Straps_-_3_resistances": "Included",
  "Power_requirements": "110V-240V 50/60Hz",
  "created_at": ISODate("2019-12-16T06:33:21.722Z"),
  "updated_at": ISODate("2019-12-16T06:33:21.722Z")
});
db.getCollection("product_specification").insert({
  "_id": ObjectId("5df7253122d6d8661dd45308"),
  "model": "300 - Standard",
  "Market": "US",
  "Units": "in/lbs",
  "Max_user_weight": "300 lbs",
  "Overall_height": "7'- 11\"",
  "Max_user_height_overground": "7'- 5\"",
  "Max_user_height_over_6\"_treadmill": "6'- 11\"",
  "Removable_FreeDome": "Optional",
  "Max_user_height_overground_(rfd)": "6'- 11\"",
  "Max_user_height_over_6\"_treadmill_(rfd)": "6'- 5\"",
  "Integrated_FreeDome": "Optional",
  "Max_user_height_overground_(ifd)": null,
  "Max_user_height_over_6\"_treadmill_(ifd)": null,
  "Powered_stroke_adjustment": "28",
  "Manual_stroke_adjustment": "N/A",
  "Base_options": "30/34",
  "Max_footprint_(base_x_width_x_length)": "38.5 x 45.5",
  "Standard_Base_width": NumberInt(30),
  "Max_treadmill_width_(sb)": "29",
  "Min_door_width_(sb)": "35.5",
  "Footprint_(width_x_length)_(sb)": "34.5 x 45.5",
  "Base_option_2_width": NumberInt(34),
  "Max_treadmill_width_(bo2)": "33",
  "Min_door_width_(bo2)": "39.5",
  "Footprint_(width_x_length)_(bo2)": "38.5 x 45.5",
  "Base_option_3_width": null,
  "Max_treadmill_width_(bo3)": null,
  "Min_door_width_(bo3)": null,
  "Footprint_(width_x_length)_(bo3)": null,
  "Clearance_(floor_to_bottom_of_base_U)": "19",
  "Length_of_base_leg": "45.5",
  "Width_of_base_leg": "2.5",
  "Height_of_base_leg_from_ground": "8",
  "Caster_diameter": "6",
  "Harness_included": "1 Harness",
  "BiSym_Tablet": "Wired",
  "GaiterStool": "Optional",
  "3D_Handlebar": "Optional",
  "Parallel_Grab_Bar": "Optional",
  "L-Shape_Handlears": "Included",
  "Q-Straps_-_3_resistances": "Included",
  "Power_requirements": "110V-240V 50/60Hz",
  "created_at": ISODate("2019-12-16T06:33:21.722Z"),
  "updated_at": ISODate("2019-12-16T06:33:21.722Z")
});
db.getCollection("product_specification").insert({
  "_id": ObjectId("5df7253122d6d8661dd4530c"),
  "model": "400 - Standard",
  "Market": "US",
  "Units": "in/lbs",
  "Max_user_weight": "400 lbs",
  "Overall_height": "7'-  9\"",
  "Max_user_height_overground": "7'- 3\"",
  "Max_user_height_over_6\"_treadmill": "6'- 9\"",
  "Removable_FreeDome": "Optional",
  "Max_user_height_overground_(rfd)": "6'- 9\"",
  "Max_user_height_over_6\"_treadmill_(rfd)": "6'- 3\"",
  "Integrated_FreeDome": "Optional",
  "Max_user_height_overground_(ifd)": null,
  "Max_user_height_over_6\"_treadmill_(ifd)": null,
  "Powered_stroke_adjustment": "32",
  "Manual_stroke_adjustment": "N/A",
  "Base_options": "30/34/32",
  "Max_footprint_(base_x_width_x_length)": "36 x 48.5",
  "Standard_Base_width": NumberInt(30),
  "Max_treadmill_width_(sb)": "29",
  "Min_door_width_(sb)": "35",
  "Footprint_(width_x_length)_(sb)": "34 x 48.5",
  "Base_option_2_width": NumberInt(34),
  "Max_treadmill_width_(bo2)": "33",
  "Min_door_width_(bo2)": "39",
  "Footprint_(width_x_length)_(bo2)": "38 x 48.5",
  "Base_option_3_width": NumberInt(32),
  "Max_treadmill_width_(bo3)": "31",
  "Min_door_width_(bo3)": "37",
  "Footprint_(width_x_length)_(bo3)": "36 x 48.5",
  "Clearance_(floor_to_bottom_of_base_U)": "19.5",
  "Length_of_base_leg": "48.5",
  "Width_of_base_leg": "2",
  "Height_of_base_leg_from_ground": "8.5",
  "Caster_diameter": "5",
  "Harness_included": "2 Harness",
  "BiSym_Tablet": "Wireless",
  "GaiterStool": "Optional",
  "3D_Handlebar": "Included",
  "Parallel_Grab_Bar": "Optional",
  "L-Shape_Handlears": "Optional",
  "Q-Straps_-_3_resistances": "Included",
  "Power_requirements": "110V-240V 50/60Hz",
  "created_at": ISODate("2019-12-16T06:33:21.722Z"),
  "updated_at": ISODate("2019-12-16T06:33:21.722Z")
});
db.getCollection("product_specification").insert({
  "_id": ObjectId("5df7253122d6d8661dd4530e"),
  "model": "400 - Deluxe",
  "Market": "US",
  "Units": "in/lbs",
  "Max_user_weight": "400 lbs",
  "Overall_height": "7'-  9\"",
  "Max_user_height_overground": "6'- 11\"",
  "Max_user_height_over_6\"_treadmill": "6'- 5\"",
  "Removable_FreeDome": "Not Available",
  "Max_user_height_overground_(rfd)": "6'- 11\"",
  "Max_user_height_over_6\"_treadmill_(rfd)": "6'- 5\"",
  "Integrated_FreeDome": "Included",
  "Max_user_height_overground_(ifd)": "6'- 11\"",
  "Max_user_height_over_6\"_treadmill_(ifd)": "6'- 5\"",
  "Powered_stroke_adjustment": "32",
  "Manual_stroke_adjustment": "N/A",
  "Base_options": "30/34/32",
  "Max_footprint_(base_x_width_x_length)": "36 x 48.5",
  "Standard_Base_width": NumberInt(30),
  "Max_treadmill_width_(sb)": "29",
  "Min_door_width_(sb)": "35",
  "Footprint_(width_x_length)_(sb)": "34 x 48.5",
  "Base_option_2_width": NumberInt(34),
  "Max_treadmill_width_(bo2)": "33",
  "Min_door_width_(bo2)": "39",
  "Footprint_(width_x_length)_(bo2)": "38 x 48.5",
  "Base_option_3_width": NumberInt(32),
  "Max_treadmill_width_(bo3)": "31",
  "Min_door_width_(bo3)": "37",
  "Footprint_(width_x_length)_(bo3)": "36 x 48.5",
  "Clearance_(floor_to_bottom_of_base_U)": "19.5",
  "Length_of_base_leg": "48.5",
  "Width_of_base_leg": "2",
  "Height_of_base_leg_from_ground": "8.5",
  "Caster_diameter": "5",
  "Harness_included": "2 Harness",
  "BiSym_Tablet": "Wireless",
  "GaiterStool": "Included",
  "3D_Handlebar": "Included",
  "Parallel_Grab_Bar": "Optional",
  "L-Shape_Handlears": "Optional",
  "Q-Straps_-_3_resistances": "Included",
  "Power_requirements": "110V-240V 50/60Hz",
  "created_at": ISODate("2019-12-16T06:33:21.722Z"),
  "updated_at": ISODate("2019-12-16T06:33:21.722Z")
});
db.getCollection("product_specification").insert({
  "_id": ObjectId("5df7253122d6d8661dd45310"),
  "model": "400 - Tall",
  "Market": "US",
  "Units": "in/lbs",
  "Max_user_weight": "400 lbs",
  "Overall_height": "8'-  3\"",
  "Max_user_height_overground": "7'- 5\"",
  "Max_user_height_over_6\"_treadmill": "6'- 11\"",
  "Removable_FreeDome": "Not Available",
  "Max_user_height_overground_(rfd)": "7'- 5\"",
  "Max_user_height_over_6\"_treadmill_(rfd)": "6'- 11\"",
  "Integrated_FreeDome": "Included",
  "Max_user_height_overground_(ifd)": "7'- 5\"",
  "Max_user_height_over_6\"_treadmill_(ifd)": "6'- 11\"",
  "Powered_stroke_adjustment": "36",
  "Manual_stroke_adjustment": "N/A",
  "Base_options": "32/34/",
  "Max_footprint_(base_x_width_x_length)": "38.5 x 48",
  "Standard_Base_width": NumberInt(32),
  "Max_treadmill_width_(sb)": "31",
  "Min_door_width_(sb)": "37.5",
  "Footprint_(width_x_length)_(sb)": "36.5 x 48",
  "Base_option_2_width": NumberInt(34),
  "Max_treadmill_width_(bo2)": "33",
  "Min_door_width_(bo2)": "39.5",
  "Footprint_(width_x_length)_(bo2)": "38.5 x 48",
  "Base_option_3_width": null,
  "Max_treadmill_width_(bo3)": null,
  "Min_door_width_(bo3)": null,
  "Footprint_(width_x_length)_(bo3)": null,
  "Clearance_(floor_to_bottom_of_base_U)": "19",
  "Length_of_base_leg": "48",
  "Width_of_base_leg": "2.5",
  "Height_of_base_leg_from_ground": "8",
  "Caster_diameter": "6",
  "Harness_included": "2 Harness",
  "BiSym_Tablet": "Wireless",
  "GaiterStool": "Included",
  "3D_Handlebar": "Included",
  "Parallel_Grab_Bar": "Optional",
  "L-Shape_Handlears": "Optional",
  "Q-Straps_-_3_resistances": "Included",
  "Power_requirements": "110V-240V 50/60Hz",
  "created_at": ISODate("2019-12-16T06:33:21.722Z"),
  "updated_at": ISODate("2019-12-16T06:33:21.722Z")
});
db.getCollection("product_specification").insert({
  "_id": ObjectId("5df7253122d6d8661dd45312"),
  "model": "500 - Standard",
  "Market": "US",
  "Units": "in/lbs",
  "Max_user_weight": "500 lbs",
  "Overall_height": "7'- 11\"",
  "Max_user_height_overground": "7'- 5\"",
  "Max_user_height_over_6\"_treadmill": "6'- 11\"",
  "Removable_FreeDome": "Optional",
  "Max_user_height_overground_(rfd)": "6'- 11\"",
  "Max_user_height_over_6\"_treadmill_(rfd)": "6'- 5\"",
  "Integrated_FreeDome": "Optional",
  "Max_user_height_overground_(ifd)": null,
  "Max_user_height_over_6\"_treadmill_(ifd)": null,
  "Powered_stroke_adjustment": "32",
  "Manual_stroke_adjustment": "N/A",
  "Base_options": "30/34/32",
  "Max_footprint_(base_x_width_x_length)": "36 x 48.5",
  "Standard_Base_width": NumberInt(30),
  "Max_treadmill_width_(sb)": "29",
  "Min_door_width_(sb)": "35",
  "Footprint_(width_x_length)_(sb)": "34 x 48.5",
  "Base_option_2_width": NumberInt(34),
  "Max_treadmill_width_(bo2)": "33",
  "Min_door_width_(bo2)": "39",
  "Footprint_(width_x_length)_(bo2)": "38 x 48.5",
  "Base_option_3_width": NumberInt(32),
  "Max_treadmill_width_(bo3)": "31",
  "Min_door_width_(bo3)": "37",
  "Footprint_(width_x_length)_(bo3)": "36 x 48.5",
  "Clearance_(floor_to_bottom_of_base_U)": "21",
  "Length_of_base_leg": "48.5",
  "Width_of_base_leg": "2",
  "Height_of_base_leg_from_ground": "8.5",
  "Caster_diameter": "5",
  "Harness_included": "2 Harness",
  "BiSym_Tablet": "Wireless",
  "GaiterStool": "Optional",
  "3D_Handlebar": "Included",
  "Parallel_Grab_Bar": "Optional",
  "L-Shape_Handlears": "Optional",
  "Q-Straps_-_3_resistances": "Included",
  "Power_requirements": "110V-240V 50/60Hz",
  "created_at": ISODate("2019-12-16T06:33:21.722Z"),
  "updated_at": ISODate("2019-12-16T06:33:21.722Z")
});
db.getCollection("product_specification").insert({
  "_id": ObjectId("5df7253122d6d8661dd45316"),
  "model": "500 - Tall",
  "Market": "US",
  "Units": "in/lbs",
  "Max_user_weight": "500 lbs",
  "Overall_height": "8'-  5\"",
  "Max_user_height_overground": "7'- 7\"",
  "Max_user_height_over_6\"_treadmill": "7'- 1\"",
  "Removable_FreeDome": "Not Available",
  "Max_user_height_overground_(rfd)": "7'- 7\"",
  "Max_user_height_over_6\"_treadmill_(rfd)": "7'- 1\"",
  "Integrated_FreeDome": "Included",
  "Max_user_height_overground_(ifd)": "7'- 7\"",
  "Max_user_height_over_6\"_treadmill_(ifd)": "7'- 1\"",
  "Powered_stroke_adjustment": "36",
  "Manual_stroke_adjustment": "N/A",
  "Base_options": "32/34",
  "Max_footprint_(base_x_width_x_length)": "38.5 x 48",
  "Standard_Base_width": NumberInt(32),
  "Max_treadmill_width_(sb)": "31",
  "Min_door_width_(sb)": "37.5",
  "Footprint_(width_x_length)_(sb)": "36.5 x 48",
  "Base_option_2_width": NumberInt(34),
  "Max_treadmill_width_(bo2)": "33",
  "Min_door_width_(bo2)": "39.5",
  "Footprint_(width_x_length)_(bo2)": "38.5 x 48",
  "Base_option_3_width": null,
  "Max_treadmill_width_(bo3)": null,
  "Min_door_width_(bo3)": null,
  "Footprint_(width_x_length)_(bo3)": null,
  "Clearance_(floor_to_bottom_of_base_U)": "20.5",
  "Length_of_base_leg": "48",
  "Width_of_base_leg": "2.5",
  "Height_of_base_leg_from_ground": "8",
  "Caster_diameter": "6",
  "Harness_included": "2 Harness",
  "BiSym_Tablet": "Wireless",
  "GaiterStool": "Included",
  "3D_Handlebar": "Included",
  "Parallel_Grab_Bar": "Optional",
  "L-Shape_Handlears": "Optional",
  "Q-Straps_-_3_resistances": "Included",
  "Power_requirements": "110V-240V 50/60Hz",
  "created_at": ISODate("2019-12-16T06:33:21.722Z"),
  "updated_at": ISODate("2019-12-16T06:33:21.722Z")
});
db.getCollection("product_specification").insert({
  "_id": ObjectId("5df7253122d6d8661dd452f6"),
  "model": "100W",
  "Market": "US",
  "Units": "in/lbs",
  "Max_user_weight": "100 lbs",
  "Overall_height": "5'-  5\"",
  "Max_user_height_overground": "5'- 2\"",
  "Max_user_height_over_6\"_treadmill": "4'- 8\"",
  "Removable_FreeDome": "Optional",
  "Max_user_height_overground_(rfd)": "4'- 8\"",
  "Max_user_height_over_6\"_treadmill_(rfd)": "4'- 2\"",
  "Integrated_FreeDome": "Not Available",
  "Max_user_height_overground_(ifd)": null,
  "Max_user_height_over_6\"_treadmill_(ifd)": null,
  "Powered_stroke_adjustment": "N/A",
  "Manual_stroke_adjustment": "28",
  "Base_options": "25/27/30",
  "Max_footprint_(base_x_width_x_length)": "33 x 32.5",
  "Standard_Base_width": NumberInt(25),
  "Max_treadmill_width_(sb)": "24",
  "Min_door_width_(sb)": "29",
  "Footprint_(width_x_length)_(sb)": "28 x 32.5",
  "Base_option_2_width": NumberInt(27),
  "Max_treadmill_width_(bo2)": "26",
  "Min_door_width_(bo2)": "31",
  "Footprint_(width_x_length)_(bo2)": "30 x 32.5",
  "Base_option_3_width": NumberInt(30),
  "Max_treadmill_width_(bo3)": "29",
  "Min_door_width_(bo3)": "34",
  "Footprint_(width_x_length)_(bo3)": "33 x 32.5",
  "Clearance_(floor_to_bottom_of_base_U)": "13",
  "Length_of_base_leg": "32.5",
  "Width_of_base_leg": "1.5",
  "Height_of_base_leg_from_ground": "5.5",
  "Caster_diameter": "3",
  "Harness_included": "1 Harness",
  "BiSym_Tablet": "Optional",
  "GaiterStool": "Optional",
  "3D_Handlebar": "Optional",
  "Parallel_Grab_Bar": "Included",
  "L-Shape_Handlears": "Not Available",
  "Q-Straps_-_3_resistances": "Optional",
  "Power_requirements": null,
  "created_at": ISODate("2019-12-16T06:33:21.721Z"),
  "updated_at": ISODate("2019-12-16T06:33:21.721Z")
});
db.getCollection("product_specification").insert({
  "_id": ObjectId("5df7253122d6d8661dd4531a"),
  "model": "300MX",
  "Market": "US",
  "Units": "in/lbs",
  "Max_user_weight": "300 lbs",
  "Overall_height": "7'-  6\"",
  "Max_user_height_overground": "7'- 0\"",
  "Max_user_height_over_6\"_treadmill": "6'- 6\"",
  "Removable_FreeDome": "Optional",
  "Max_user_height_overground_(rfd)": "6'- 6\"",
  "Max_user_height_over_6\"_treadmill_(rfd)": "6'- 0\"",
  "Integrated_FreeDome": "Not Available",
  "Max_user_height_overground_(ifd)": null,
  "Max_user_height_over_6\"_treadmill_(ifd)": null,
  "Powered_stroke_adjustment": "26",
  "Manual_stroke_adjustment": "N/A",
  "Base_options": "30/34",
  "Max_footprint_(base_x_width_x_length)": "38 x 45.5",
  "Standard_Base_width": NumberInt(30),
  "Max_treadmill_width_(sb)": "29",
  "Min_door_width_(sb)": "35",
  "Footprint_(width_x_length)_(sb)": "34 x 45.5",
  "Base_option_2_width": NumberInt(34),
  "Max_treadmill_width_(bo2)": "33",
  "Min_door_width_(bo2)": "39",
  "Footprint_(width_x_length)_(bo2)": "38 x 45.5",
  "Base_option_3_width": null,
  "Max_treadmill_width_(bo3)": null,
  "Min_door_width_(bo3)": null,
  "Footprint_(width_x_length)_(bo3)": null,
  "Clearance_(floor_to_bottom_of_base_U)": "20.5",
  "Length_of_base_leg": "45.5",
  "Width_of_base_leg": "2",
  "Height_of_base_leg_from_ground": "8",
  "Caster_diameter": "6",
  "Harness_included": "1 Harness",
  "BiSym_Tablet": "Optional",
  "GaiterStool": "Optional",
  "3D_Handlebar": "Not Available",
  "Parallel_Grab_Bar": "Included",
  "L-Shape_Handlears": "Not Available",
  "Q-Straps_-_3_resistances": "Optional",
  "Power_requirements": "110V-240V 50/60Hz",
  "created_at": ISODate("2019-12-16T06:33:21.722Z"),
  "updated_at": ISODate("2019-12-16T06:33:21.722Z")
});
db.getCollection("product_specification").insert({
  "_id": ObjectId("5df7253122d6d8661dd45318"),
  "model": "650 - Standard",
  "Market": "US",
  "Units": "in/lbs",
  "Max_user_weight": "650 lbs",
  "Overall_height": "7'- 10\"",
  "Max_user_height_overground": "6'- 8\"",
  "Max_user_height_over_6\"_treadmill": "6'- 2\"",
  "Removable_FreeDome": "Optional",
  "Max_user_height_overground_(rfd)": "6'- 8\"",
  "Max_user_height_over_6\"_treadmill_(rfd)": "6'- 2\"",
  "Integrated_FreeDome": "Optional",
  "Max_user_height_overground_(ifd)": null,
  "Max_user_height_over_6\"_treadmill_(ifd)": null,
  "Powered_stroke_adjustment": "37",
  "Manual_stroke_adjustment": "N/A",
  "Base_options": "34",
  "Max_footprint_(base_x_width_x_length)": "38.5 x 48",
  "Standard_Base_width": NumberInt(34),
  "Max_treadmill_width_(sb)": "33",
  "Min_door_width_(sb)": "39.5",
  "Footprint_(width_x_length)_(sb)": "38.5 x 48",
  "Base_option_2_width": null,
  "Max_treadmill_width_(bo2)": null,
  "Min_door_width_(bo2)": null,
  "Footprint_(width_x_length)_(bo2)": null,
  "Base_option_3_width": null,
  "Max_treadmill_width_(bo3)": null,
  "Min_door_width_(bo3)": null,
  "Footprint_(width_x_length)_(bo3)": null,
  "Clearance_(floor_to_bottom_of_base_U)": "13.5",
  "Length_of_base_leg": "48",
  "Width_of_base_leg": "2.5",
  "Height_of_base_leg_from_ground": "14",
  "Caster_diameter": "6",
  "Harness_included": "3 Harness",
  "BiSym_Tablet": "Wired",
  "GaiterStool": "Included",
  "3D_Handlebar": "Included",
  "Parallel_Grab_Bar": "Optional",
  "L-Shape_Handlears": "Optional",
  "Q-Straps_-_3_resistances": "Included",
  "Power_requirements": "110V-240V 50/60Hz",
  "created_at": ISODate("2019-12-16T06:33:21.722Z"),
  "updated_at": ISODate("2019-12-16T06:33:21.722Z")
});
db.getCollection("product_specification").insert({
  "_id": ObjectId("5df7253122d6d8661dd45300"),
  "model": "200MX",
  "Market": "US",
  "Units": "in/lbs",
  "Max_user_weight": "200 lbs",
  "Overall_height": "6'-  7\"",
  "Max_user_height_overground": "6'- 1\"",
  "Max_user_height_over_6\"_treadmill": "5'- 7\"",
  "Removable_FreeDome": "Optional",
  "Max_user_height_overground_(rfd)": "5'- 7\"",
  "Max_user_height_over_6\"_treadmill_(rfd)": "5'- 1\"",
  "Integrated_FreeDome": "Not Available",
  "Max_user_height_overground_(ifd)": null,
  "Max_user_height_over_6\"_treadmill_(ifd)": null,
  "Powered_stroke_adjustment": "18",
  "Manual_stroke_adjustment": "32",
  "Base_options": "30/27/34",
  "Max_footprint_(base_x_width_x_length)": "38 x 42.5",
  "Standard_Base_width": NumberInt(30),
  "Max_treadmill_width_(sb)": "29",
  "Min_door_width_(sb)": "35",
  "Footprint_(width_x_length)_(sb)": "31 x 42.5",
  "Base_option_2_width": NumberInt(27),
  "Max_treadmill_width_(bo2)": "26",
  "Min_door_width_(bo2)": "32",
  "Footprint_(width_x_length)_(bo2)": "34 x 42.5",
  "Base_option_3_width": NumberInt(34),
  "Max_treadmill_width_(bo3)": "33",
  "Min_door_width_(bo3)": "39",
  "Footprint_(width_x_length)_(bo3)": "38 x 42.5",
  "Clearance_(floor_to_bottom_of_base_U)": "18",
  "Length_of_base_leg": "42.5",
  "Width_of_base_leg": "2",
  "Height_of_base_leg_from_ground": "8",
  "Caster_diameter": "5",
  "Harness_included": "1 Harness",
  "BiSym_Tablet": "Optional",
  "GaiterStool": "Optional",
  "3D_Handlebar": "Optional",
  "Parallel_Grab_Bar": "Included",
  "L-Shape_Handlears": "Not Available",
  "Q-Straps_-_3_resistances": "Included",
  "Power_requirements": "110V-240V 50/60Hz",
  "created_at": ISODate("2019-12-16T06:33:21.722Z"),
  "updated_at": ISODate("2019-12-16T06:33:21.722Z")
});
db.getCollection("product_specification").insert({
  "_id": ObjectId("5df7253122d6d8661dd4531c"),
  "model": "110 4Home",
  "Market": "US 4Home",
  "Units": "in/lbs",
  "Max_user_weight": "110 lbs",
  "Overall_height": "5'-  9\"",
  "Max_user_height_overground": "5'- 6\"",
  "Max_user_height_over_6\"_treadmill": "5'- 0\"",
  "Removable_FreeDome": "Optional",
  "Max_user_height_overground_(rfd)": "5'- 0\"",
  "Max_user_height_over_6\"_treadmill_(rfd)": "4'- 6\"",
  "Integrated_FreeDome": "Not Available",
  "Max_user_height_overground_(ifd)": null,
  "Max_user_height_over_6\"_treadmill_(ifd)": null,
  "Powered_stroke_adjustment": "18",
  "Manual_stroke_adjustment": "12",
  "Base_options": "25/27/30",
  "Max_footprint_(base_x_width_x_length)": "33 x 32.5",
  "Standard_Base_width": NumberInt(25),
  "Max_treadmill_width_(sb)": "24",
  "Min_door_width_(sb)": "28",
  "Footprint_(width_x_length)_(sb)": "28 x 32.5",
  "Base_option_2_width": NumberInt(27),
  "Max_treadmill_width_(bo2)": "26",
  "Min_door_width_(bo2)": "31",
  "Footprint_(width_x_length)_(bo2)": "30 x 32.5",
  "Base_option_3_width": NumberInt(30),
  "Max_treadmill_width_(bo3)": "29",
  "Min_door_width_(bo3)": "34",
  "Footprint_(width_x_length)_(bo3)": "33 x 32.5",
  "Clearance_(floor_to_bottom_of_base_U)": "13",
  "Length_of_base_leg": "32.5",
  "Width_of_base_leg": "1.5",
  "Height_of_base_leg_from_ground": "5.5",
  "Caster_diameter": "3",
  "Harness_included": "1 Harness",
  "BiSym_Tablet": "Optional",
  "GaiterStool": "Optional",
  "3D_Handlebar": "Optional",
  "Parallel_Grab_Bar": "Included",
  "L-Shape_Handlears": "Not Available",
  "Q-Straps_-_3_resistances": "Included",
  "Power_requirements": "110V-240V 50/60Hz",
  "created_at": ISODate("2019-12-16T06:33:21.722Z"),
  "updated_at": ISODate("2019-12-16T06:33:21.722Z")
});
db.getCollection("product_specification").insert({
  "_id": ObjectId("5df7253122d6d8661dd4530a"),
  "model": "300 - Deluxe",
  "Market": "US",
  "Units": "in/lbs",
  "Max_user_weight": "300 lbs",
  "Overall_height": "7'- 11\"",
  "Max_user_height_overground": "7'- 1\"",
  "Max_user_height_over_6\"_treadmill": "6'- 7\"",
  "Removable_FreeDome": "Not Available",
  "Max_user_height_overground_(rfd)": "7'- 1\"",
  "Max_user_height_over_6\"_treadmill_(rfd)": "6'- 7\"",
  "Integrated_FreeDome": "Included",
  "Max_user_height_overground_(ifd)": "7'- 1\"",
  "Max_user_height_over_6\"_treadmill_(ifd)": "6'- 7\"",
  "Powered_stroke_adjustment": "28",
  "Manual_stroke_adjustment": "N/A",
  "Base_options": "30/34",
  "Max_footprint_(base_x_width_x_length)": "38.5 x 45.5",
  "Standard_Base_width": NumberInt(30),
  "Max_treadmill_width_(sb)": "29",
  "Min_door_width_(sb)": "35.5",
  "Footprint_(width_x_length)_(sb)": "34.5 x 45.5",
  "Base_option_2_width": NumberInt(34),
  "Max_treadmill_width_(bo2)": "33",
  "Min_door_width_(bo2)": "39.5",
  "Footprint_(width_x_length)_(bo2)": "38.5 x 45.5",
  "Base_option_3_width": null,
  "Max_treadmill_width_(bo3)": null,
  "Min_door_width_(bo3)": null,
  "Footprint_(width_x_length)_(bo3)": null,
  "Clearance_(floor_to_bottom_of_base_U)": "19",
  "Length_of_base_leg": "45.5",
  "Width_of_base_leg": "2.5",
  "Height_of_base_leg_from_ground": "8",
  "Caster_diameter": "6",
  "Harness_included": "1 Harness",
  "BiSym_Tablet": "Wired",
  "GaiterStool": "Included",
  "3D_Handlebar": "Optional",
  "Parallel_Grab_Bar": "Optional",
  "L-Shape_Handlears": "Included",
  "Q-Straps_-_3_resistances": "Included",
  "Power_requirements": "110V-240V 50/60Hz",
  "created_at": ISODate("2019-12-16T06:33:21.722Z"),
  "updated_at": ISODate("2019-12-16T06:33:21.722Z")
});
db.getCollection("product_specification").insert({
  "_id": ObjectId("5df7253122d6d8661dd45314"),
  "model": "500 - Deluxe",
  "Market": "US",
  "Units": "in/lbs",
  "Max_user_weight": "500 lbs",
  "Overall_height": "7'- 11\"",
  "Max_user_height_overground": "7'- 1\"",
  "Max_user_height_over_6\"_treadmill": "6'- 7\"",
  "Removable_FreeDome": "Not Available",
  "Max_user_height_overground_(rfd)": "7'- 1\"",
  "Max_user_height_over_6\"_treadmill_(rfd)": "6'- 7\"",
  "Integrated_FreeDome": "Included",
  "Max_user_height_overground_(ifd)": "7'- 1\"",
  "Max_user_height_over_6\"_treadmill_(ifd)": "6'- 7\"",
  "Powered_stroke_adjustment": "32",
  "Manual_stroke_adjustment": "N/A",
  "Base_options": "30/34/32",
  "Max_footprint_(base_x_width_x_length)": "36 x 48.5",
  "Standard_Base_width": NumberInt(30),
  "Max_treadmill_width_(sb)": "29",
  "Min_door_width_(sb)": "35",
  "Footprint_(width_x_length)_(sb)": "34 x 48.5",
  "Base_option_2_width": NumberInt(34),
  "Max_treadmill_width_(bo2)": "33",
  "Min_door_width_(bo2)": "39",
  "Footprint_(width_x_length)_(bo2)": "38 x 48.5",
  "Base_option_3_width": NumberInt(32),
  "Max_treadmill_width_(bo3)": "31",
  "Min_door_width_(bo3)": "37",
  "Footprint_(width_x_length)_(bo3)": "36 x 48.5",
  "Clearance_(floor_to_bottom_of_base_U)": "21",
  "Length_of_base_leg": "48.5",
  "Width_of_base_leg": "2",
  "Height_of_base_leg_from_ground": "8.5",
  "Caster_diameter": "5",
  "Harness_included": "2 Harness",
  "BiSym_Tablet": "Wireless",
  "GaiterStool": "Included",
  "3D_Handlebar": "Included",
  "Parallel_Grab_Bar": "Optional",
  "L-Shape_Handlears": "Optional",
  "Q-Straps_-_3_resistances": "Included",
  "Power_requirements": "110V-240V 50/60Hz",
  "created_at": ISODate("2019-12-16T06:33:21.722Z"),
  "updated_at": ISODate("2019-12-16T06:33:21.722Z")
});
db.getCollection("product_specification").insert({
  "_id": ObjectId("5df7253122d6d8661dd4531e"),
  "model": "220 4Home",
  "Market": "US 4Home",
  "Units": "in/lbs",
  "Max_user_weight": "220 lbs",
  "Overall_height": "7'-  2\"",
  "Max_user_height_overground": "6'- 8\"",
  "Max_user_height_over_6\"_treadmill": "6'- 2\"",
  "Removable_FreeDome": "Optional",
  "Max_user_height_overground_(rfd)": "6'- 2\"",
  "Max_user_height_over_6\"_treadmill_(rfd)": "5'- 8\"",
  "Integrated_FreeDome": "Not Available",
  "Max_user_height_overground_(ifd)": null,
  "Max_user_height_over_6\"_treadmill_(ifd)": null,
  "Powered_stroke_adjustment": "24",
  "Manual_stroke_adjustment": "12",
  "Base_options": "27/30",
  "Max_footprint_(base_x_width_x_length)": "34.5 x 45.5",
  "Standard_Base_width": NumberInt(27),
  "Max_treadmill_width_(sb)": "26",
  "Min_door_width_(sb)": "31.5",
  "Footprint_(width_x_length)_(sb)": "31.5 x 45.5",
  "Base_option_2_width": NumberInt(30),
  "Max_treadmill_width_(bo2)": "29",
  "Min_door_width_(bo2)": "35.5",
  "Footprint_(width_x_length)_(bo2)": "34.5 x 45.5",
  "Base_option_3_width": null,
  "Max_treadmill_width_(bo3)": null,
  "Min_door_width_(bo3)": null,
  "Footprint_(width_x_length)_(bo3)": null,
  "Clearance_(floor_to_bottom_of_base_U)": "19",
  "Length_of_base_leg": "45.5",
  "Width_of_base_leg": "2.5",
  "Height_of_base_leg_from_ground": "8",
  "Caster_diameter": "6",
  "Harness_included": "1 Harness",
  "BiSym_Tablet": "Optional",
  "GaiterStool": "Optional",
  "3D_Handlebar": "Optional",
  "Parallel_Grab_Bar": "Included",
  "L-Shape_Handlears": "Not Available",
  "Q-Straps_-_3_resistances": "Included",
  "Power_requirements": "110V-240V 50/60Hz",
  "created_at": ISODate("2019-12-16T06:33:21.722Z"),
  "updated_at": ISODate("2019-12-16T06:33:21.722Z")
});
db.getCollection("product_specification").insert({
  "_id": ObjectId("5df7253122d6d8661dd45320"),
  "model": "330 4Home",
  "Market": "US 4Home",
  "Units": "in/lbs",
  "Max_user_weight": "330 lbs",
  "Overall_height": "7'-  6\"",
  "Max_user_height_overground": "7'- 0\"",
  "Max_user_height_over_6\"_treadmill": "6'- 6\"",
  "Removable_FreeDome": "Optional",
  "Max_user_height_overground_(rfd)": "6'- 6\"",
  "Max_user_height_over_6\"_treadmill_(rfd)": "6'- 0\"",
  "Integrated_FreeDome": "Not Available",
  "Max_user_height_overground_(ifd)": null,
  "Max_user_height_over_6\"_treadmill_(ifd)": null,
  "Powered_stroke_adjustment": "26",
  "Manual_stroke_adjustment": "12",
  "Base_options": "27/30/34",
  "Max_footprint_(base_x_width_x_length)": "38 x 45.5",
  "Standard_Base_width": NumberInt(27),
  "Max_treadmill_width_(sb)": "26",
  "Min_door_width_(sb)": "31",
  "Footprint_(width_x_length)_(sb)": "31 x 45.5",
  "Base_option_2_width": NumberInt(30),
  "Max_treadmill_width_(bo2)": "29",
  "Min_door_width_(bo2)": "35",
  "Footprint_(width_x_length)_(bo2)": "34 x 45.5",
  "Base_option_3_width": NumberInt(34),
  "Max_treadmill_width_(bo3)": "33",
  "Min_door_width_(bo3)": "39",
  "Footprint_(width_x_length)_(bo3)": "38 x 45.5",
  "Clearance_(floor_to_bottom_of_base_U)": "20",
  "Length_of_base_leg": "45.5",
  "Width_of_base_leg": "2",
  "Height_of_base_leg_from_ground": "8",
  "Caster_diameter": "6",
  "Harness_included": "1 Harness",
  "BiSym_Tablet": "Optional",
  "GaiterStool": "Optional",
  "3D_Handlebar": "Optional",
  "Parallel_Grab_Bar": "Included",
  "L-Shape_Handlears": "Not Available",
  "Q-Straps_-_3_resistances": "Included",
  "Power_requirements": "110V-240V 50/60Hz",
  "created_at": ISODate("2019-12-16T06:33:21.722Z"),
  "updated_at": ISODate("2019-12-16T06:33:21.722Z")
});
db.getCollection("product_specification").insert({
  "_id": ObjectId("5df7253122d6d8661dd45322"),
  "model": "35KW",
  "Market": "EU",
  "Units": "cm/kg",
  "Max_user_weight": "35 kg",
  "Overall_height": NumberInt(155),
  "Max_user_height_overground": NumberInt(147),
  "Max_user_height_over_6\"_treadmill": NumberInt(132),
  "Removable_FreeDome": "Optional",
  "Max_user_height_overground_(rfd)": NumberInt(147),
  "Max_user_height_over_6\"_treadmill_(rfd)": NumberInt(132),
  "Integrated_FreeDome": "Not Available",
  "Max_user_height_overground_(ifd)": null,
  "Max_user_height_over_6\"_treadmill_(ifd)": null,
  "Powered_stroke_adjustment": "N/A",
  "Manual_stroke_adjustment": NumberInt(60),
  "Base_options": "64/69/76",
  "Max_footprint_(base_x_width_x_length)": "84 x 82",
  "Standard_Base_width": NumberInt(64),
  "Max_treadmill_width_(sb)": NumberInt(62),
  "Min_door_width_(sb)": NumberInt(75),
  "Footprint_(width_x_length)_(sb)": "72 x 82",
  "Base_option_2_width": NumberInt(69),
  "Max_treadmill_width_(bo2)": NumberInt(67),
  "Min_door_width_(bo2)": NumberInt(80),
  "Footprint_(width_x_length)_(bo2)": "77 x 82",
  "Base_option_3_width": NumberInt(76),
  "Max_treadmill_width_(bo3)": NumberInt(74),
  "Min_door_width_(bo3)": NumberInt(87),
  "Footprint_(width_x_length)_(bo3)": "84 x 82",
  "Clearance_(floor_to_bottom_of_base_U)": NumberInt(33),
  "Length_of_base_leg": NumberInt(82),
  "Width_of_base_leg": NumberInt(4),
  "Height_of_base_leg_from_ground": NumberInt(14),
  "Caster_diameter": 7.6,
  "Harness_included": "1 Harness",
  "BiSym_Tablet": "Not Available",
  "GaiterStool": "Optional",
  "3D_Handlebar": "Not Available",
  "Parallel_Grab_Bar": "Included",
  "L-Shape_Handlears": "Not Available",
  "Q-Straps_-_3_resistances": "Optional",
  "Power_requirements": null,
  "created_at": ISODate("2019-12-16T06:33:21.722Z"),
  "updated_at": ISODate("2019-12-16T06:33:21.722Z")
});
db.getCollection("product_specification").insert({
  "_id": ObjectId("5df7253122d6d8661dd45324"),
  "model": "50KW",
  "Market": "EU",
  "Units": "cm/kg",
  "Max_user_weight": "50 kg",
  "Overall_height": NumberInt(167),
  "Max_user_height_overground": NumberInt(159),
  "Max_user_height_over_6\"_treadmill": NumberInt(144),
  "Removable_FreeDome": "Optional",
  "Max_user_height_overground_(rfd)": NumberInt(144),
  "Max_user_height_over_6\"_treadmill_(rfd)": NumberInt(129),
  "Integrated_FreeDome": "Not Available",
  "Max_user_height_overground_(ifd)": null,
  "Max_user_height_over_6\"_treadmill_(ifd)": null,
  "Powered_stroke_adjustment": "N/A",
  "Manual_stroke_adjustment": NumberInt(70),
  "Base_options": "64/69/76",
  "Max_footprint_(base_x_width_x_length)": "84 x 82",
  "Standard_Base_width": NumberInt(64),
  "Max_treadmill_width_(sb)": NumberInt(62),
  "Min_door_width_(sb)": NumberInt(75),
  "Footprint_(width_x_length)_(sb)": "72 x 82",
  "Base_option_2_width": NumberInt(69),
  "Max_treadmill_width_(bo2)": NumberInt(67),
  "Min_door_width_(bo2)": NumberInt(80),
  "Footprint_(width_x_length)_(bo2)": "77 x 82",
  "Base_option_3_width": NumberInt(76),
  "Max_treadmill_width_(bo3)": NumberInt(74),
  "Min_door_width_(bo3)": NumberInt(87),
  "Footprint_(width_x_length)_(bo3)": "84 x 82",
  "Clearance_(floor_to_bottom_of_base_U)": NumberInt(33),
  "Length_of_base_leg": NumberInt(82),
  "Width_of_base_leg": NumberInt(4),
  "Height_of_base_leg_from_ground": NumberInt(14),
  "Caster_diameter": 7.6,
  "Harness_included": "1 Harness",
  "BiSym_Tablet": "Not Available",
  "GaiterStool": "Optional",
  "3D_Handlebar": "Not Available",
  "Parallel_Grab_Bar": "Included",
  "L-Shape_Handlears": "Not Available",
  "Q-Straps_-_3_resistances": "Optional",
  "Power_requirements": null,
  "created_at": ISODate("2019-12-16T06:33:21.722Z"),
  "updated_at": ISODate("2019-12-16T06:33:21.722Z")
});
db.getCollection("product_specification").insert({
  "_id": ObjectId("5df7253122d6d8661dd45326"),
  "model": "50KX",
  "Market": "EU",
  "Units": "cm/kg",
  "Max_user_weight": "50 kg",
  "Overall_height": NumberInt(164),
  "Max_user_height_overground": NumberInt(146),
  "Max_user_height_over_6\"_treadmill": NumberInt(131),
  "Removable_FreeDome": "Optional",
  "Max_user_height_overground_(rfd)": NumberInt(146),
  "Max_user_height_over_6\"_treadmill_(rfd)": NumberInt(131),
  "Integrated_FreeDome": "Not Available",
  "Max_user_height_overground_(ifd)": null,
  "Max_user_height_over_6\"_treadmill_(ifd)": null,
  "Powered_stroke_adjustment": NumberInt(45),
  "Manual_stroke_adjustment": NumberInt(70),
  "Base_options": "64/69/76",
  "Max_footprint_(base_x_width_x_length)": "84 x 82",
  "Standard_Base_width": NumberInt(64),
  "Max_treadmill_width_(sb)": NumberInt(62),
  "Min_door_width_(sb)": NumberInt(75),
  "Footprint_(width_x_length)_(sb)": "72 x 82",
  "Base_option_2_width": NumberInt(69),
  "Max_treadmill_width_(bo2)": NumberInt(67),
  "Min_door_width_(bo2)": NumberInt(80),
  "Footprint_(width_x_length)_(bo2)": "77 x 82",
  "Base_option_3_width": NumberInt(76),
  "Max_treadmill_width_(bo3)": NumberInt(74),
  "Min_door_width_(bo3)": NumberInt(87),
  "Footprint_(width_x_length)_(bo3)": "84 x 82",
  "Clearance_(floor_to_bottom_of_base_U)": NumberInt(33),
  "Length_of_base_leg": NumberInt(82),
  "Width_of_base_leg": NumberInt(4),
  "Height_of_base_leg_from_ground": NumberInt(14),
  "Caster_diameter": 7.6,
  "Harness_included": "1 Harness",
  "BiSym_Tablet": "Not Available",
  "GaiterStool": "Optional",
  "3D_Handlebar": "Not Available",
  "Parallel_Grab_Bar": "Included",
  "L-Shape_Handlears": "Not Available",
  "Q-Straps_-_3_resistances": "Optional",
  "Power_requirements": "110V-240V 50/60Hz",
  "created_at": ISODate("2019-12-16T06:33:21.722Z"),
  "updated_at": ISODate("2019-12-16T06:33:21.722Z")
});
db.getCollection("product_specification").insert({
  "_id": ObjectId("5df7253122d6d8661dd45328"),
  "model": "90KX",
  "Market": "EU",
  "Units": "cm/kg",
  "Max_user_weight": "90 kg",
  "Overall_height": NumberInt(201),
  "Max_user_height_overground": NumberInt(186),
  "Max_user_height_over_6\"_treadmill": NumberInt(171),
  "Removable_FreeDome": "Optional",
  "Max_user_height_overground_(rfd)": NumberInt(171),
  "Max_user_height_over_6\"_treadmill_(rfd)": NumberInt(156),
  "Integrated_FreeDome": "Not Available",
  "Max_user_height_overground_(ifd)": null,
  "Max_user_height_over_6\"_treadmill_(ifd)": null,
  "Powered_stroke_adjustment": NumberInt(46),
  "Manual_stroke_adjustment": NumberInt(80),
  "Base_options": "76/69/86",
  "Max_footprint_(base_x_width_x_length)": "94 x 108",
  "Standard_Base_width": NumberInt(76),
  "Max_treadmill_width_(sb)": NumberInt(74),
  "Min_door_width_(sb)": NumberInt(87),
  "Footprint_(width_x_length)_(sb)": "77 x 108",
  "Base_option_2_width": NumberInt(69),
  "Max_treadmill_width_(bo2)": NumberInt(67),
  "Min_door_width_(bo2)": NumberInt(80),
  "Footprint_(width_x_length)_(bo2)": "84 x 108",
  "Base_option_3_width": NumberInt(86),
  "Max_treadmill_width_(bo3)": NumberInt(84),
  "Min_door_width_(bo3)": NumberInt(97),
  "Footprint_(width_x_length)_(bo3)": "94 x 108",
  "Clearance_(floor_to_bottom_of_base_U)": NumberInt(43),
  "Length_of_base_leg": NumberInt(108),
  "Width_of_base_leg": 5.5,
  "Height_of_base_leg_from_ground": NumberInt(20),
  "Caster_diameter": 12.7,
  "Harness_included": "1 Harness",
  "BiSym_Tablet": "Optional",
  "GaiterStool": "Optional",
  "3D_Handlebar": "Not Available",
  "Parallel_Grab_Bar": "Included",
  "L-Shape_Handlears": "Not Available",
  "Q-Straps_-_3_resistances": "Included",
  "Power_requirements": "110V-240V 50/60Hz",
  "created_at": ISODate("2019-12-16T06:33:21.722Z"),
  "updated_at": ISODate("2019-12-16T06:33:21.722Z")
});
db.getCollection("product_specification").insert({
  "_id": ObjectId("5df7253122d6d8661dd4532a"),
  "model": "100KP",
  "Market": "EU",
  "Units": "cm/kg",
  "Max_user_weight": "100 kg",
  "Overall_height": NumberInt(221),
  "Max_user_height_overground": NumberInt(206),
  "Max_user_height_over_6\"_treadmill": NumberInt(191),
  "Removable_FreeDome": "Optional",
  "Max_user_height_overground_(rfd)": NumberInt(191),
  "Max_user_height_over_6\"_treadmill_(rfd)": NumberInt(176),
  "Integrated_FreeDome": "Not Available",
  "Max_user_height_overground_(ifd)": null,
  "Max_user_height_over_6\"_treadmill_(ifd)": null,
  "Powered_stroke_adjustment": NumberInt(70),
  "Manual_stroke_adjustment": NumberInt(40),
  "Base_options": "69/76",
  "Max_footprint_(base_x_width_x_length)": "87 x 108",
  "Standard_Base_width": NumberInt(69),
  "Max_treadmill_width_(sb)": NumberInt(67),
  "Min_door_width_(sb)": NumberInt(82),
  "Footprint_(width_x_length)_(sb)": "80 x 108",
  "Base_option_2_width": NumberInt(76),
  "Max_treadmill_width_(bo2)": NumberInt(74),
  "Min_door_width_(bo2)": NumberInt(89),
  "Footprint_(width_x_length)_(bo2)": "87 x 108",
  "Base_option_3_width": null,
  "Max_treadmill_width_(bo3)": null,
  "Min_door_width_(bo3)": null,
  "Footprint_(width_x_length)_(bo3)": null,
  "Clearance_(floor_to_bottom_of_base_U)": NumberInt(43),
  "Length_of_base_leg": NumberInt(108),
  "Width_of_base_leg": 5.5,
  "Height_of_base_leg_from_ground": NumberInt(20),
  "Caster_diameter": 12.7,
  "Harness_included": "2 Harness",
  "BiSym_Tablet": "Optional",
  "GaiterStool": "Optional",
  "3D_Handlebar": "Not Available",
  "Parallel_Grab_Bar": "Included",
  "L-Shape_Handlears": "Optional",
  "Q-Straps_-_3_resistances": "Included",
  "Power_requirements": "110V-240V 50/60Hz",
  "created_at": ISODate("2019-12-16T06:33:21.722Z"),
  "updated_at": ISODate("2019-12-16T06:33:21.722Z")
});
db.getCollection("product_specification").insert({
  "_id": ObjectId("5df7253122d6d8661dd4532c"),
  "model": "125KX",
  "Market": "EU",
  "Units": "cm/kg",
  "Max_user_weight": "150 kg",
  "Overall_height": NumberInt(228),
  "Max_user_height_overground": NumberInt(213),
  "Max_user_height_over_6\"_treadmill": NumberInt(198),
  "Removable_FreeDome": "Optional",
  "Max_user_height_overground_(rfd)": NumberInt(198),
  "Max_user_height_over_6\"_treadmill_(rfd)": NumberInt(183),
  "Integrated_FreeDome": "Not Available",
  "Max_user_height_overground_(ifd)": null,
  "Max_user_height_over_6\"_treadmill_(ifd)": null,
  "Powered_stroke_adjustment": NumberInt(65),
  "Manual_stroke_adjustment": "N/A",
  "Base_options": "69/76/86",
  "Max_footprint_(base_x_width_x_length)": "97 x 198",
  "Standard_Base_width": NumberInt(69),
  "Max_treadmill_width_(sb)": NumberInt(67),
  "Min_door_width_(sb)": NumberInt(82),
  "Footprint_(width_x_length)_(sb)": "80 x 198",
  "Base_option_2_width": NumberInt(76),
  "Max_treadmill_width_(bo2)": NumberInt(74),
  "Min_door_width_(bo2)": NumberInt(89),
  "Footprint_(width_x_length)_(bo2)": "87 x 198",
  "Base_option_3_width": NumberInt(86),
  "Max_treadmill_width_(bo3)": NumberInt(84),
  "Min_door_width_(bo3)": NumberInt(99),
  "Footprint_(width_x_length)_(bo3)": "97 x 198",
  "Clearance_(floor_to_bottom_of_base_U)": NumberInt(54),
  "Length_of_base_leg": NumberInt(198),
  "Width_of_base_leg": 5.5,
  "Height_of_base_leg_from_ground": NumberInt(24),
  "Caster_diameter": 15.2,
  "Harness_included": "1 Harness",
  "BiSym_Tablet": "Optional",
  "GaiterStool": "Optional",
  "3D_Handlebar": "Optional",
  "Parallel_Grab_Bar": "Included",
  "L-Shape_Handlears": "Not Available",
  "Q-Straps_-_3_resistances": "Included",
  "Power_requirements": "110V-240V 50/60Hz",
  "created_at": ISODate("2019-12-16T06:33:21.722Z"),
  "updated_at": ISODate("2019-12-16T06:33:21.722Z")
});
db.getCollection("product_specification").insert({
  "_id": ObjectId("5df7253122d6d8661dd4532e"),
  "model": "135KS",
  "Market": "EU",
  "Units": "cm/kg",
  "Max_user_weight": "135 kg",
  "Overall_height": NumberInt(242),
  "Max_user_height_overground": NumberInt(227),
  "Max_user_height_over_6\"_treadmill": NumberInt(212),
  "Removable_FreeDome": "Not Available",
  "Max_user_height_overground_(rfd)": NumberInt(212),
  "Max_user_height_over_6\"_treadmill_(rfd)": NumberInt(197),
  "Integrated_FreeDome": "Included",
  "Max_user_height_overground_(ifd)": null,
  "Max_user_height_over_6\"_treadmill_(ifd)": null,
  "Powered_stroke_adjustment": NumberInt(70),
  "Manual_stroke_adjustment": "N/A",
  "Base_options": "76/86",
  "Max_footprint_(base_x_width_x_length)": "97 x 115",
  "Standard_Base_width": NumberInt(76),
  "Max_treadmill_width_(sb)": NumberInt(74),
  "Min_door_width_(sb)": NumberInt(89),
  "Footprint_(width_x_length)_(sb)": "87 x 115",
  "Base_option_2_width": NumberInt(86),
  "Max_treadmill_width_(bo2)": NumberInt(84),
  "Min_door_width_(bo2)": NumberInt(99),
  "Footprint_(width_x_length)_(bo2)": "97 x 115",
  "Base_option_3_width": null,
  "Max_treadmill_width_(bo3)": null,
  "Min_door_width_(bo3)": null,
  "Footprint_(width_x_length)_(bo3)": null,
  "Clearance_(floor_to_bottom_of_base_U)": NumberInt(48),
  "Length_of_base_leg": NumberInt(115),
  "Width_of_base_leg": 5.5,
  "Height_of_base_leg_from_ground": NumberInt(20),
  "Caster_diameter": 15.2,
  "Harness_included": "1 Harness",
  "BiSym_Tablet": "Optional",
  "GaiterStool": "Optional",
  "3D_Handlebar": "Included",
  "Parallel_Grab_Bar": "Optional",
  "L-Shape_Handlears": "Optional",
  "Q-Straps_-_3_resistances": "Included",
  "Power_requirements": "110V-240V 50/60Hz",
  "created_at": ISODate("2019-12-16T06:33:21.722Z"),
  "updated_at": ISODate("2019-12-16T06:33:21.722Z")
});
db.getCollection("product_specification").insert({
  "_id": ObjectId("5df7253122d6d8661dd45330"),
  "model": "135KD",
  "Market": "EU",
  "Units": "cm/kg",
  "Max_user_weight": "135 kg",
  "Overall_height": NumberInt(242),
  "Max_user_height_overground": NumberInt(217),
  "Max_user_height_over_6\"_treadmill": NumberInt(202),
  "Removable_FreeDome": "Not Available",
  "Max_user_height_overground_(rfd)": NumberInt(217),
  "Max_user_height_over_6\"_treadmill_(rfd)": NumberInt(202),
  "Integrated_FreeDome": "Included",
  "Max_user_height_overground_(ifd)": NumberInt(217),
  "Max_user_height_over_6\"_treadmill_(ifd)": NumberInt(202),
  "Powered_stroke_adjustment": NumberInt(70),
  "Manual_stroke_adjustment": "N/A",
  "Base_options": "76/86",
  "Max_footprint_(base_x_width_x_length)": "97 x 115",
  "Standard_Base_width": NumberInt(76),
  "Max_treadmill_width_(sb)": NumberInt(74),
  "Min_door_width_(sb)": NumberInt(89),
  "Footprint_(width_x_length)_(sb)": "87 x 115",
  "Base_option_2_width": NumberInt(86),
  "Max_treadmill_width_(bo2)": NumberInt(84),
  "Min_door_width_(bo2)": NumberInt(99),
  "Footprint_(width_x_length)_(bo2)": "97 x 115",
  "Base_option_3_width": null,
  "Max_treadmill_width_(bo3)": null,
  "Min_door_width_(bo3)": null,
  "Footprint_(width_x_length)_(bo3)": null,
  "Clearance_(floor_to_bottom_of_base_U)": NumberInt(48),
  "Length_of_base_leg": NumberInt(115),
  "Width_of_base_leg": 5.5,
  "Height_of_base_leg_from_ground": NumberInt(20),
  "Caster_diameter": 15.2,
  "Harness_included": "1 Harness",
  "BiSym_Tablet": "Wired",
  "GaiterStool": "Included",
  "3D_Handlebar": "Included",
  "Parallel_Grab_Bar": "Optional",
  "L-Shape_Handlears": "Optional",
  "Q-Straps_-_3_resistances": "Included",
  "Power_requirements": "110V-240V 50/60Hz",
  "created_at": ISODate("2019-12-16T06:33:21.722Z"),
  "updated_at": ISODate("2019-12-16T06:33:21.722Z")
});
db.getCollection("product_specification").insert({
  "_id": ObjectId("5df7253122d6d8661dd45334"),
  "model": "135KPD",
  "Market": "EU",
  "Units": "cm/kg",
  "Max_user_weight": "135 kg",
  "Overall_height": NumberInt(228),
  "Max_user_height_overground": NumberInt(213),
  "Max_user_height_over_6\"_treadmill": NumberInt(198),
  "Removable_FreeDome": "Included",
  "Max_user_height_overground_(rfd)": NumberInt(198),
  "Max_user_height_over_6\"_treadmill_(rfd)": NumberInt(183),
  "Integrated_FreeDome": "Not Available",
  "Max_user_height_overground_(ifd)": null,
  "Max_user_height_over_6\"_treadmill_(ifd)": null,
  "Powered_stroke_adjustment": NumberInt(70),
  "Manual_stroke_adjustment": NumberInt(50),
  "Base_options": "76/86",
  "Max_footprint_(base_x_width_x_length)": "97 x 115",
  "Standard_Base_width": NumberInt(76),
  "Max_treadmill_width_(sb)": NumberInt(74),
  "Min_door_width_(sb)": NumberInt(90),
  "Footprint_(width_x_length)_(sb)": "87 x 115",
  "Base_option_2_width": NumberInt(86),
  "Max_treadmill_width_(bo2)": NumberInt(84),
  "Min_door_width_(bo2)": NumberInt(100),
  "Footprint_(width_x_length)_(bo2)": "97 x 115",
  "Base_option_3_width": null,
  "Max_treadmill_width_(bo3)": null,
  "Min_door_width_(bo3)": null,
  "Footprint_(width_x_length)_(bo3)": null,
  "Clearance_(floor_to_bottom_of_base_U)": NumberInt(46),
  "Length_of_base_leg": NumberInt(115),
  "Width_of_base_leg": 5.5,
  "Height_of_base_leg_from_ground": NumberInt(18),
  "Caster_diameter": 12.7,
  "Harness_included": "2 Harness",
  "BiSym_Tablet": "Wired",
  "GaiterStool": "Included",
  "3D_Handlebar": "Included",
  "Parallel_Grab_Bar": "Optional",
  "L-Shape_Handlears": "Optional",
  "Q-Straps_-_3_resistances": "Included",
  "Power_requirements": "110V-240V 50/60Hz",
  "created_at": ISODate("2019-12-16T06:33:21.722Z"),
  "updated_at": ISODate("2019-12-16T06:33:21.722Z")
});
db.getCollection("product_specification").insert({
  "_id": ObjectId("5df7253122d6d8661dd45332"),
  "model": "135KP",
  "Market": "EU",
  "Units": "cm/kg",
  "Max_user_weight": "135 kg",
  "Overall_height": NumberInt(228),
  "Max_user_height_overground": NumberInt(213),
  "Max_user_height_over_6\"_treadmill": NumberInt(198),
  "Removable_FreeDome": "Included",
  "Max_user_height_overground_(rfd)": NumberInt(198),
  "Max_user_height_over_6\"_treadmill_(rfd)": NumberInt(183),
  "Integrated_FreeDome": "Not Available",
  "Max_user_height_overground_(ifd)": null,
  "Max_user_height_over_6\"_treadmill_(ifd)": null,
  "Powered_stroke_adjustment": NumberInt(70),
  "Manual_stroke_adjustment": NumberInt(50),
  "Base_options": "76/86",
  "Max_footprint_(base_x_width_x_length)": "97 x 115",
  "Standard_Base_width": NumberInt(76),
  "Max_treadmill_width_(sb)": NumberInt(74),
  "Min_door_width_(sb)": NumberInt(90),
  "Footprint_(width_x_length)_(sb)": "87 x 115",
  "Base_option_2_width": NumberInt(86),
  "Max_treadmill_width_(bo2)": NumberInt(84),
  "Min_door_width_(bo2)": NumberInt(100),
  "Footprint_(width_x_length)_(bo2)": "97 x 115",
  "Base_option_3_width": null,
  "Max_treadmill_width_(bo3)": null,
  "Min_door_width_(bo3)": null,
  "Footprint_(width_x_length)_(bo3)": null,
  "Clearance_(floor_to_bottom_of_base_U)": NumberInt(46),
  "Length_of_base_leg": NumberInt(115),
  "Width_of_base_leg": 5.5,
  "Height_of_base_leg_from_ground": NumberInt(18),
  "Caster_diameter": 12.7,
  "Harness_included": "2 Harness",
  "BiSym_Tablet": "Optional",
  "GaiterStool": "Optional",
  "3D_Handlebar": "Included",
  "Parallel_Grab_Bar": "Optional",
  "L-Shape_Handlears": "Optional",
  "Q-Straps_-_3_resistances": "Included",
  "Power_requirements": "110V-240V 50/60Hz",
  "created_at": ISODate("2019-12-16T06:33:21.722Z"),
  "updated_at": ISODate("2019-12-16T06:33:21.722Z")
});
db.getCollection("product_specification").insert({
  "_id": ObjectId("5df7253122d6d8661dd45336"),
  "model": "200KS",
  "Market": "EU",
  "Units": "cm/kg",
  "Max_user_weight": "200 kg",
  "Overall_height": NumberInt(242),
  "Max_user_height_overground": NumberInt(217),
  "Max_user_height_over_6\"_treadmill": NumberInt(202),
  "Removable_FreeDome": "Not Available",
  "Max_user_height_overground_(rfd)": NumberInt(217),
  "Max_user_height_over_6\"_treadmill_(rfd)": NumberInt(202),
  "Integrated_FreeDome": "Included",
  "Max_user_height_overground_(ifd)": NumberInt(217),
  "Max_user_height_over_6\"_treadmill_(ifd)": NumberInt(202),
  "Powered_stroke_adjustment": NumberInt(80),
  "Manual_stroke_adjustment": "N/A",
  "Base_options": "76/86",
  "Max_footprint_(base_x_width_x_length)": "97 x 123",
  "Standard_Base_width": NumberInt(76),
  "Max_treadmill_width_(sb)": NumberInt(74),
  "Min_door_width_(sb)": NumberInt(89),
  "Footprint_(width_x_length)_(sb)": "87 x 123",
  "Base_option_2_width": NumberInt(86),
  "Max_treadmill_width_(bo2)": NumberInt(84),
  "Min_door_width_(bo2)": NumberInt(99),
  "Footprint_(width_x_length)_(bo2)": "97 x 123",
  "Base_option_3_width": null,
  "Max_treadmill_width_(bo3)": null,
  "Min_door_width_(bo3)": null,
  "Footprint_(width_x_length)_(bo3)": null,
  "Clearance_(floor_to_bottom_of_base_U)": NumberInt(54),
  "Length_of_base_leg": NumberInt(123),
  "Width_of_base_leg": 5.5,
  "Height_of_base_leg_from_ground": NumberInt(22),
  "Caster_diameter": 12.7,
  "Harness_included": "1 Harness",
  "BiSym_Tablet": "Optional",
  "GaiterStool": "Optional",
  "3D_Handlebar": "Included",
  "Parallel_Grab_Bar": "Optional",
  "L-Shape_Handlears": "Optional",
  "Q-Straps_-_3_resistances": "Included",
  "Power_requirements": "110V-240V 50/60Hz",
  "created_at": ISODate("2019-12-16T06:33:21.722Z"),
  "updated_at": ISODate("2019-12-16T06:33:21.722Z")
});
db.getCollection("product_specification").insert({
  "_id": ObjectId("5df7253122d6d8661dd45338"),
  "model": "200KD",
  "Market": "EU",
  "Units": "cm/kg",
  "Max_user_weight": "200 kg",
  "Overall_height": NumberInt(242),
  "Max_user_height_overground": NumberInt(217),
  "Max_user_height_over_6\"_treadmill": NumberInt(202),
  "Removable_FreeDome": "Not Available",
  "Max_user_height_overground_(rfd)": NumberInt(217),
  "Max_user_height_over_6\"_treadmill_(rfd)": NumberInt(202),
  "Integrated_FreeDome": "Included",
  "Max_user_height_overground_(ifd)": NumberInt(217),
  "Max_user_height_over_6\"_treadmill_(ifd)": NumberInt(202),
  "Powered_stroke_adjustment": NumberInt(80),
  "Manual_stroke_adjustment": "N/A",
  "Base_options": "76/86",
  "Max_footprint_(base_x_width_x_length)": "97 x 123",
  "Standard_Base_width": NumberInt(76),
  "Max_treadmill_width_(sb)": NumberInt(74),
  "Min_door_width_(sb)": NumberInt(89),
  "Footprint_(width_x_length)_(sb)": "87 x 123",
  "Base_option_2_width": NumberInt(86),
  "Max_treadmill_width_(bo2)": NumberInt(84),
  "Min_door_width_(bo2)": NumberInt(99),
  "Footprint_(width_x_length)_(bo2)": "97 x 123",
  "Base_option_3_width": null,
  "Max_treadmill_width_(bo3)": null,
  "Min_door_width_(bo3)": null,
  "Footprint_(width_x_length)_(bo3)": null,
  "Clearance_(floor_to_bottom_of_base_U)": NumberInt(54),
  "Length_of_base_leg": NumberInt(123),
  "Width_of_base_leg": 5.5,
  "Height_of_base_leg_from_ground": NumberInt(22),
  "Caster_diameter": 12.7,
  "Harness_included": "2 Harness",
  "BiSym_Tablet": "Wireless",
  "GaiterStool": "Included",
  "3D_Handlebar": "Included",
  "Parallel_Grab_Bar": "Optional",
  "L-Shape_Handlears": "Optional",
  "Q-Straps_-_3_resistances": "Included",
  "Power_requirements": "110V-240V 50/60Hz",
  "created_at": ISODate("2019-12-16T06:33:21.723Z"),
  "updated_at": ISODate("2019-12-16T06:33:21.723Z")
});
db.getCollection("product_specification").insert({
  "_id": ObjectId("5df7253122d6d8661dd4533a"),
  "model": "200KT",
  "Market": "EU",
  "Units": "cm/kg",
  "Max_user_weight": "200 kg",
  "Overall_height": NumberInt(255),
  "Max_user_height_overground": NumberInt(230),
  "Max_user_height_over_6\"_treadmill": NumberInt(215),
  "Removable_FreeDome": "Not Available",
  "Max_user_height_overground_(rfd)": NumberInt(230),
  "Max_user_height_over_6\"_treadmill_(rfd)": NumberInt(215),
  "Integrated_FreeDome": "Included",
  "Max_user_height_overground_(ifd)": NumberInt(230),
  "Max_user_height_over_6\"_treadmill_(ifd)": NumberInt(215),
  "Powered_stroke_adjustment": NumberInt(90),
  "Manual_stroke_adjustment": "N/A",
  "Base_options": "86/81",
  "Max_footprint_(base_x_width_x_length)": "97 x 122",
  "Standard_Base_width": NumberInt(86),
  "Max_treadmill_width_(sb)": NumberInt(84),
  "Min_door_width_(sb)": NumberInt(100),
  "Footprint_(width_x_length)_(sb)": "92 x 122",
  "Base_option_2_width": NumberInt(81),
  "Max_treadmill_width_(bo2)": NumberInt(79),
  "Min_door_width_(bo2)": NumberInt(95),
  "Footprint_(width_x_length)_(bo2)": "97 x 122",
  "Base_option_3_width": null,
  "Max_treadmill_width_(bo3)": null,
  "Min_door_width_(bo3)": null,
  "Footprint_(width_x_length)_(bo3)": null,
  "Clearance_(floor_to_bottom_of_base_U)": NumberInt(52),
  "Length_of_base_leg": NumberInt(122),
  "Width_of_base_leg": 5.5,
  "Height_of_base_leg_from_ground": NumberInt(20),
  "Caster_diameter": 15.2,
  "Harness_included": "2 Harness",
  "BiSym_Tablet": "Wireless",
  "GaiterStool": "Optional",
  "3D_Handlebar": "Included",
  "Parallel_Grab_Bar": "Not Available",
  "L-Shape_Handlears": "Optional",
  "Q-Straps_-_3_resistances": "Included",
  "Power_requirements": "110V-240V 50/60Hz",
  "created_at": ISODate("2019-12-16T06:33:21.723Z"),
  "updated_at": ISODate("2019-12-16T06:33:21.723Z")
});
db.getCollection("product_specification").insert({
  "_id": ObjectId("5df7253122d6d8661dd4533c"),
  "model": "300K",
  "Market": "EU",
  "Units": "cm/kg",
  "Max_user_weight": "300 kg",
  "Overall_height": NumberInt(239),
  "Max_user_height_overground": NumberInt(193),
  "Max_user_height_over_6\"_treadmill": NumberInt(178),
  "Removable_FreeDome": "Not Available",
  "Max_user_height_overground_(rfd)": NumberInt(193),
  "Max_user_height_over_6\"_treadmill_(rfd)": NumberInt(178),
  "Integrated_FreeDome": "Included",
  "Max_user_height_overground_(ifd)": null,
  "Max_user_height_over_6\"_treadmill_(ifd)": null,
  "Powered_stroke_adjustment": NumberInt(80),
  "Manual_stroke_adjustment": "N/A",
  "Base_options": "86",
  "Max_footprint_(base_x_width_x_length)": "97 x 122",
  "Standard_Base_width": NumberInt(86),
  "Max_treadmill_width_(sb)": NumberInt(84),
  "Min_door_width_(sb)": NumberInt(100),
  "Footprint_(width_x_length)_(sb)": "97 x 122",
  "Base_option_2_width": null,
  "Max_treadmill_width_(bo2)": null,
  "Min_door_width_(bo2)": null,
  "Footprint_(width_x_length)_(bo2)": null,
  "Base_option_3_width": null,
  "Max_treadmill_width_(bo3)": null,
  "Min_door_width_(bo3)": null,
  "Footprint_(width_x_length)_(bo3)": null,
  "Clearance_(floor_to_bottom_of_base_U)": NumberInt(34),
  "Length_of_base_leg": NumberInt(122),
  "Width_of_base_leg": 5.5,
  "Height_of_base_leg_from_ground": NumberInt(20),
  "Caster_diameter": 15.2,
  "Harness_included": "3 Harness",
  "BiSym_Tablet": "Wireless",
  "GaiterStool": "Optional",
  "3D_Handlebar": "Included",
  "Parallel_Grab_Bar": "Not Available",
  "L-Shape_Handlears": "Optional",
  "Q-Straps_-_3_resistances": "Included",
  "Power_requirements": "110V-240V 50/60Hz",
  "created_at": ISODate("2019-12-16T06:33:21.723Z"),
  "updated_at": ISODate("2019-12-16T06:33:21.723Z")
});
db.getCollection("product_specification").insert({
  "_id": ObjectId("5df7253122d6d8661dd4533e"),
  "model": "LG4H 45K",
  "Market": "EU 4Home",
  "Units": "cm/kg",
  "Max_user_weight": "45 kg",
  "Overall_height": NumberInt(176),
  "Max_user_height_overground": NumberInt(168),
  "Max_user_height_over_6\"_treadmill": NumberInt(153),
  "Removable_FreeDome": "Optional",
  "Max_user_height_overground_(rfd)": NumberInt(153),
  "Max_user_height_over_6\"_treadmill_(rfd)": NumberInt(138),
  "Integrated_FreeDome": "Optional",
  "Max_user_height_overground_(ifd)": null,
  "Max_user_height_over_6\"_treadmill_(ifd)": null,
  "Powered_stroke_adjustment": NumberInt(47),
  "Manual_stroke_adjustment": "N/A",
  "Base_options": "64/69/76",
  "Max_footprint_(base_x_width_x_length)": "84 x 146",
  "Standard_Base_width": NumberInt(64),
  "Max_treadmill_width_(sb)": NumberInt(62),
  "Min_door_width_(sb)": NumberInt(75),
  "Footprint_(width_x_length)_(sb)": "72 x 146",
  "Base_option_2_width": NumberInt(69),
  "Max_treadmill_width_(bo2)": NumberInt(67),
  "Min_door_width_(bo2)": NumberInt(80),
  "Footprint_(width_x_length)_(bo2)": "77 x 146",
  "Base_option_3_width": NumberInt(76),
  "Max_treadmill_width_(bo3)": NumberInt(74),
  "Min_door_width_(bo3)": NumberInt(87),
  "Footprint_(width_x_length)_(bo3)": "84 x 146",
  "Clearance_(floor_to_bottom_of_base_U)": NumberInt(33),
  "Length_of_base_leg": NumberInt(146),
  "Width_of_base_leg": NumberInt(4),
  "Height_of_base_leg_from_ground": NumberInt(14),
  "Caster_diameter": 7.6,
  "Harness_included": "1 Harness",
  "BiSym_Tablet": "Optional",
  "GaiterStool": "Optional",
  "3D_Handlebar": "Optional",
  "Parallel_Grab_Bar": "Included",
  "L-Shape_Handlears": "Not Available",
  "Q-Straps_-_3_resistances": "Included",
  "Power_requirements": "110V-240V 50/60Hz",
  "created_at": ISODate("2019-12-16T06:33:21.723Z"),
  "updated_at": ISODate("2019-12-16T06:33:21.723Z")
});
db.getCollection("product_specification").insert({
  "_id": ObjectId("5df7253122d6d8661dd45340"),
  "model": "LG4H 95K",
  "Market": "EU 4Home",
  "Units": "cm/kg",
  "Max_user_weight": "95 kg",
  "Overall_height": NumberInt(214),
  "Max_user_height_overground": NumberInt(199),
  "Max_user_height_over_6\"_treadmill": NumberInt(184),
  "Removable_FreeDome": "Optional",
  "Max_user_height_overground_(rfd)": NumberInt(184),
  "Max_user_height_over_6\"_treadmill_(rfd)": NumberInt(169),
  "Integrated_FreeDome": "Not Available",
  "Max_user_height_overground_(ifd)": null,
  "Max_user_height_over_6\"_treadmill_(ifd)": null,
  "Powered_stroke_adjustment": NumberInt(61),
  "Manual_stroke_adjustment": "N/A",
  "Base_options": "69/76/86",
  "Max_footprint_(base_x_width_x_length)": "87 x 183",
  "Standard_Base_width": NumberInt(69),
  "Max_treadmill_width_(sb)": NumberInt(67),
  "Min_door_width_(sb)": NumberInt(82),
  "Footprint_(width_x_length)_(sb)": "80 x 183",
  "Base_option_2_width": NumberInt(76),
  "Max_treadmill_width_(bo2)": NumberInt(74),
  "Min_door_width_(bo2)": NumberInt(89),
  "Footprint_(width_x_length)_(bo2)": "87 x 183",
  "Base_option_3_width": NumberInt(86),
  "Max_treadmill_width_(bo3)": NumberInt(84),
  "Min_door_width_(bo3)": null,
  "Footprint_(width_x_length)_(bo3)": null,
  "Clearance_(floor_to_bottom_of_base_U)": NumberInt(46),
  "Length_of_base_leg": NumberInt(183),
  "Width_of_base_leg": 5.5,
  "Height_of_base_leg_from_ground": NumberInt(20),
  "Caster_diameter": 12.7,
  "Harness_included": "1 Harness",
  "BiSym_Tablet": "Optional",
  "GaiterStool": "Optional",
  "3D_Handlebar": "Optional",
  "Parallel_Grab_Bar": "Included",
  "L-Shape_Handlears": "Not Available",
  "Q-Straps_-_3_resistances": "Included",
  "Power_requirements": "110V-240V 50/60Hz",
  "created_at": ISODate("2019-12-16T06:33:21.723Z"),
  "updated_at": ISODate("2019-12-16T06:33:21.723Z")
});
db.getCollection("product_specification").insert({
  "_id": ObjectId("5df7253122d6d8661dd45342"),
  "model": "LG4H 150K",
  "Market": "EU 4Home",
  "Units": "cm/kg",
  "Max_user_weight": "150 kg",
  "Overall_height": NumberInt(228),
  "Max_user_height_overground": NumberInt(213),
  "Max_user_height_over_6\"_treadmill": NumberInt(198),
  "Removable_FreeDome": "Optional",
  "Max_user_height_overground_(rfd)": NumberInt(198),
  "Max_user_height_over_6\"_treadmill_(rfd)": NumberInt(183),
  "Integrated_FreeDome": "Not Available",
  "Max_user_height_overground_(ifd)": null,
  "Max_user_height_over_6\"_treadmill_(ifd)": null,
  "Powered_stroke_adjustment": NumberInt(65),
  "Manual_stroke_adjustment": "N/A",
  "Base_options": "69/76/86",
  "Max_footprint_(base_x_width_x_length)": "97 x 198",
  "Standard_Base_width": NumberInt(69),
  "Max_treadmill_width_(sb)": NumberInt(67),
  "Min_door_width_(sb)": NumberInt(82),
  "Footprint_(width_x_length)_(sb)": "80 x 198",
  "Base_option_2_width": NumberInt(76),
  "Max_treadmill_width_(bo2)": NumberInt(74),
  "Min_door_width_(bo2)": NumberInt(89),
  "Footprint_(width_x_length)_(bo2)": "87 x 198",
  "Base_option_3_width": NumberInt(86),
  "Max_treadmill_width_(bo3)": NumberInt(84),
  "Min_door_width_(bo3)": NumberInt(99),
  "Footprint_(width_x_length)_(bo3)": "97 x 198",
  "Clearance_(floor_to_bottom_of_base_U)": NumberInt(54),
  "Length_of_base_leg": NumberInt(198),
  "Width_of_base_leg": 5.5,
  "Height_of_base_leg_from_ground": NumberInt(24),
  "Caster_diameter": 15.2,
  "Harness_included": "1 Harness",
  "BiSym_Tablet": "Optional",
  "GaiterStool": "Optional",
  "3D_Handlebar": "Optional",
  "Parallel_Grab_Bar": "Included",
  "L-Shape_Handlears": "Not Available",
  "Q-Straps_-_3_resistances": "Included",
  "Power_requirements": "110V-240V 50/60Hz",
  "created_at": ISODate("2019-12-16T06:33:21.723Z"),
  "updated_at": ISODate("2019-12-16T06:33:21.723Z")
});
db.getCollection("product_specification").insert({
  "_id": ObjectId("5df7253122d6d8661dd45344"),
  "model": "LG50KXFD46",
  "Market": "MREU",
  "Units": "cm/kg",
  "Max_user_weight": "50 kg",
  "Overall_height": NumberInt(178),
  "Max_user_height_overground": NumberInt(160),
  "Max_user_height_over_6\"_treadmill": NumberInt(145),
  "Removable_FreeDome": "Not Available",
  "Max_user_height_overground_(rfd)": NumberInt(160),
  "Max_user_height_over_6\"_treadmill_(rfd)": NumberInt(145),
  "Integrated_FreeDome": "Included",
  "Max_user_height_overground_(ifd)": NumberInt(160),
  "Max_user_height_over_6\"_treadmill_(ifd)": NumberInt(145),
  "Powered_stroke_adjustment": NumberInt(45),
  "Manual_stroke_adjustment": NumberInt(70),
  "Base_options": "64/69/76",
  "Max_footprint_(base_x_width_x_length)": "84 x 82",
  "Standard_Base_width": NumberInt(64),
  "Max_treadmill_width_(sb)": NumberInt(62),
  "Min_door_width_(sb)": NumberInt(75),
  "Footprint_(width_x_length)_(sb)": "72 x 82",
  "Base_option_2_width": NumberInt(69),
  "Max_treadmill_width_(bo2)": NumberInt(67),
  "Min_door_width_(bo2)": NumberInt(80),
  "Footprint_(width_x_length)_(bo2)": "77 x 82",
  "Base_option_3_width": NumberInt(76),
  "Max_treadmill_width_(bo3)": NumberInt(74),
  "Min_door_width_(bo3)": NumberInt(87),
  "Footprint_(width_x_length)_(bo3)": "84 x 82",
  "Clearance_(floor_to_bottom_of_base_U)": NumberInt(39),
  "Length_of_base_leg": NumberInt(82),
  "Width_of_base_leg": NumberInt(4),
  "Height_of_base_leg_from_ground": NumberInt(20),
  "Caster_diameter": 12.7,
  "Harness_included": "2 Harness",
  "BiSym_Tablet": "Not Available",
  "GaiterStool": "Optional",
  "3D_Handlebar": "Not Available",
  "Parallel_Grab_Bar": "Included",
  "L-Shape_Handlears": "Not Available",
  "Q-Straps_-_3_resistances": "Optional",
  "Power_requirements": "110V-240V 50/60Hz",
  "created_at": ISODate("2019-12-16T06:33:21.723Z"),
  "updated_at": ISODate("2019-12-16T06:33:21.723Z")
});
db.getCollection("product_specification").insert({
  "_id": ObjectId("5df7253122d6d8661dd45346"),
  "model": "LG150K",
  "Market": "MREU",
  "Units": "cm/kg",
  "Max_user_weight": "150 kg",
  "Overall_height": NumberInt(242),
  "Max_user_height_overground": NumberInt(216),
  "Max_user_height_over_6\"_treadmill": NumberInt(201),
  "Removable_FreeDome": "Not Available",
  "Max_user_height_overground_(rfd)": NumberInt(217),
  "Max_user_height_over_6\"_treadmill_(rfd)": NumberInt(201),
  "Integrated_FreeDome": "Included",
  "Max_user_height_overground_(ifd)": NumberInt(217),
  "Max_user_height_over_6\"_treadmill_(ifd)": NumberInt(202),
  "Powered_stroke_adjustment": NumberInt(70),
  "Manual_stroke_adjustment": "N/A",
  "Base_options": "76/86",
  "Max_footprint_(base_x_width_x_length)": "97 x 115",
  "Standard_Base_width": NumberInt(76),
  "Max_treadmill_width_(sb)": NumberInt(74),
  "Min_door_width_(sb)": NumberInt(89),
  "Footprint_(width_x_length)_(sb)": "87 x 115",
  "Base_option_2_width": NumberInt(86),
  "Max_treadmill_width_(bo2)": NumberInt(84),
  "Min_door_width_(bo2)": NumberInt(99),
  "Footprint_(width_x_length)_(bo2)": "97 x 115",
  "Base_option_3_width": null,
  "Max_treadmill_width_(bo3)": null,
  "Min_door_width_(bo3)": null,
  "Footprint_(width_x_length)_(bo3)": null,
  "Clearance_(floor_to_bottom_of_base_U)": NumberInt(48),
  "Length_of_base_leg": NumberInt(115),
  "Width_of_base_leg": 5.5,
  "Height_of_base_leg_from_ground": NumberInt(20),
  "Caster_diameter": 15.2,
  "Harness_included": "1 Harness",
  "BiSym_Tablet": "Optional",
  "GaiterStool": "Optional",
  "3D_Handlebar": "Included",
  "Parallel_Grab_Bar": "Optional",
  "L-Shape_Handlears": "Optional",
  "Q-Straps_-_3_resistances": "Included",
  "Power_requirements": "110V-240V 50/60Hz",
  "created_at": ISODate("2019-12-16T06:33:21.723Z"),
  "updated_at": ISODate("2019-12-16T06:33:21.723Z")
});
db.getCollection("product_specification").insert({
  "_id": ObjectId("5df7253122d6d8661dd45348"),
  "model": "LG800",
  "Market": "MREU",
  "Units": "cm/kg",
  "Max_user_weight": "200 kg",
  "Overall_height": NumberInt(242),
  "Max_user_height_overground": NumberInt(216),
  "Max_user_height_over_6\"_treadmill": NumberInt(201),
  "Removable_FreeDome": "Not Available",
  "Max_user_height_overground_(rfd)": NumberInt(217),
  "Max_user_height_over_6\"_treadmill_(rfd)": NumberInt(201),
  "Integrated_FreeDome": "Included",
  "Max_user_height_overground_(ifd)": NumberInt(217),
  "Max_user_height_over_6\"_treadmill_(ifd)": NumberInt(202),
  "Powered_stroke_adjustment": NumberInt(80),
  "Manual_stroke_adjustment": "N/A",
  "Base_options": "86/81/76",
  "Max_footprint_(base_x_width_x_length)": "97 x 123",
  "Standard_Base_width": NumberInt(86),
  "Max_treadmill_width_(sb)": NumberInt(84),
  "Min_door_width_(sb)": NumberInt(99),
  "Footprint_(width_x_length)_(sb)": "87 x 123",
  "Base_option_2_width": NumberInt(81),
  "Max_treadmill_width_(bo2)": NumberInt(79),
  "Min_door_width_(bo2)": NumberInt(94),
  "Footprint_(width_x_length)_(bo2)": "92 x 123",
  "Base_option_3_width": NumberInt(76),
  "Max_treadmill_width_(bo3)": NumberInt(74),
  "Min_door_width_(bo3)": NumberInt(89),
  "Footprint_(width_x_length)_(bo3)": "97 x 123",
  "Clearance_(floor_to_bottom_of_base_U)": NumberInt(54),
  "Length_of_base_leg": NumberInt(123),
  "Width_of_base_leg": 5.5,
  "Height_of_base_leg_from_ground": NumberInt(22),
  "Caster_diameter": 12.7,
  "Harness_included": "1 Harness",
  "BiSym_Tablet": "Not Available",
  "GaiterStool": "Optional",
  "3D_Handlebar": "Included",
  "Parallel_Grab_Bar": "Optional",
  "L-Shape_Handlears": "Optional",
  "Q-Straps_-_3_resistances": "Optional",
  "Power_requirements": "110V-240V 50/60Hz",
  "created_at": ISODate("2019-12-16T06:33:21.723Z"),
  "updated_at": ISODate("2019-12-16T06:33:21.723Z")
});
db.getCollection("product_specification").insert({
  "_id": ObjectId("5df7253122d6d8661dd4534a"),
  "model": "LG900",
  "Market": "MREU",
  "Units": "cm/kg",
  "Max_user_weight": "225 kg",
  "Overall_height": NumberInt(255),
  "Max_user_height_overground": NumberInt(230),
  "Max_user_height_over_6\"_treadmill": NumberInt(215),
  "Removable_FreeDome": "Not Available",
  "Max_user_height_overground_(rfd)": NumberInt(230),
  "Max_user_height_over_6\"_treadmill_(rfd)": NumberInt(215),
  "Integrated_FreeDome": "Included",
  "Max_user_height_overground_(ifd)": NumberInt(230),
  "Max_user_height_over_6\"_treadmill_(ifd)": NumberInt(215),
  "Powered_stroke_adjustment": NumberInt(90),
  "Manual_stroke_adjustment": "N/A",
  "Base_options": "86/81/86",
  "Max_footprint_(base_x_width_x_length)": "97 x 122",
  "Standard_Base_width": NumberInt(86),
  "Max_treadmill_width_(sb)": NumberInt(84),
  "Min_door_width_(sb)": NumberInt(100),
  "Footprint_(width_x_length)_(sb)": "87 x 122",
  "Base_option_2_width": NumberInt(81),
  "Max_treadmill_width_(bo2)": NumberInt(79),
  "Min_door_width_(bo2)": NumberInt(95),
  "Footprint_(width_x_length)_(bo2)": "92 x 122",
  "Base_option_3_width": NumberInt(86),
  "Max_treadmill_width_(bo3)": NumberInt(84),
  "Min_door_width_(bo3)": NumberInt(100),
  "Footprint_(width_x_length)_(bo3)": "97 x 122",
  "Clearance_(floor_to_bottom_of_base_U)": NumberInt(52),
  "Length_of_base_leg": NumberInt(122),
  "Width_of_base_leg": 5.5,
  "Height_of_base_leg_from_ground": NumberInt(20),
  "Caster_diameter": 15.2,
  "Harness_included": "2 Harness",
  "BiSym_Tablet": "Wireless",
  "GaiterStool": "Optional",
  "3D_Handlebar": "Included",
  "Parallel_Grab_Bar": "Not Available",
  "L-Shape_Handlears": "Optional",
  "Q-Straps_-_3_resistances": "Included",
  "Power_requirements": "110V-240V 50/60Hz",
  "created_at": ISODate("2019-12-16T06:33:21.723Z"),
  "updated_at": ISODate("2019-12-16T06:33:21.723Z")
});
db.getCollection("product_specification").insert({
  "_id": ObjectId("5df7253122d6d8661dd4534c"),
  "model": "GKMINI 18",
  "Market": null,
  "Units": null,
  "Max_user_weight": null,
  "Overall_height": null,
  "Max_user_height_overground": null,
  "Max_user_height_over_6\"_treadmill": null,
  "Removable_FreeDome": null,
  "Max_user_height_overground_(rfd)": null,
  "Max_user_height_over_6\"_treadmill_(rfd)": null,
  "Integrated_FreeDome": null,
  "Max_user_height_overground_(ifd)": null,
  "Max_user_height_over_6\"_treadmill_(ifd)": null,
  "Powered_stroke_adjustment": null,
  "Manual_stroke_adjustment": null,
  "Base_options": null,
  "Max_footprint_(base_x_width_x_length)": null,
  "Standard_Base_width": null,
  "Max_treadmill_width_(sb)": null,
  "Min_door_width_(sb)": null,
  "Footprint_(width_x_length)_(sb)": null,
  "Base_option_2_width": null,
  "Max_treadmill_width_(bo2)": null,
  "Min_door_width_(bo2)": null,
  "Footprint_(width_x_length)_(bo2)": null,
  "Base_option_3_width": null,
  "Max_treadmill_width_(bo3)": null,
  "Min_door_width_(bo3)": null,
  "Footprint_(width_x_length)_(bo3)": null,
  "Clearance_(floor_to_bottom_of_base_U)": null,
  "Length_of_base_leg": null,
  "Width_of_base_leg": null,
  "Height_of_base_leg_from_ground": null,
  "Caster_diameter": null,
  "Harness_included": null,
  "BiSym_Tablet": null,
  "GaiterStool": null,
  "3D_Handlebar": null,
  "Parallel_Grab_Bar": null,
  "L-Shape_Handlears": null,
  "Q-Straps_-_3_resistances": null,
  "Power_requirements": null,
  "created_at": ISODate("2019-12-16T06:33:21.723Z"),
  "updated_at": ISODate("2019-12-16T06:33:21.723Z")
});
db.getCollection("product_specification").insert({
  "_id": ObjectId("5df7253122d6d8661dd4534e"),
  "model": "GKMINI 21",
  "Market": null,
  "Units": null,
  "Max_user_weight": null,
  "Overall_height": null,
  "Max_user_height_overground": null,
  "Max_user_height_over_6\"_treadmill": null,
  "Removable_FreeDome": null,
  "Max_user_height_overground_(rfd)": null,
  "Max_user_height_over_6\"_treadmill_(rfd)": null,
  "Integrated_FreeDome": null,
  "Max_user_height_overground_(ifd)": null,
  "Max_user_height_over_6\"_treadmill_(ifd)": null,
  "Powered_stroke_adjustment": null,
  "Manual_stroke_adjustment": null,
  "Base_options": null,
  "Max_footprint_(base_x_width_x_length)": null,
  "Standard_Base_width": null,
  "Max_treadmill_width_(sb)": null,
  "Min_door_width_(sb)": null,
  "Footprint_(width_x_length)_(sb)": null,
  "Base_option_2_width": null,
  "Max_treadmill_width_(bo2)": null,
  "Min_door_width_(bo2)": null,
  "Footprint_(width_x_length)_(bo2)": null,
  "Base_option_3_width": null,
  "Max_treadmill_width_(bo3)": null,
  "Min_door_width_(bo3)": null,
  "Footprint_(width_x_length)_(bo3)": null,
  "Clearance_(floor_to_bottom_of_base_U)": null,
  "Length_of_base_leg": null,
  "Width_of_base_leg": null,
  "Height_of_base_leg_from_ground": null,
  "Caster_diameter": null,
  "Harness_included": null,
  "BiSym_Tablet": null,
  "GaiterStool": null,
  "3D_Handlebar": null,
  "Parallel_Grab_Bar": null,
  "L-Shape_Handlears": null,
  "Q-Straps_-_3_resistances": null,
  "Power_requirements": null,
  "created_at": ISODate("2019-12-16T06:33:21.723Z"),
  "updated_at": ISODate("2019-12-16T06:33:21.723Z")
});
db.getCollection("product_specification").insert({
  "_id": ObjectId("5df7253122d6d8661dd45356"),
  "model": "Hug-NGo 250",
  "Market": null,
  "Units": null,
  "Max_user_weight": null,
  "Overall_height": null,
  "Max_user_height_overground": null,
  "Max_user_height_over_6\"_treadmill": null,
  "Removable_FreeDome": null,
  "Max_user_height_overground_(rfd)": null,
  "Max_user_height_over_6\"_treadmill_(rfd)": null,
  "Integrated_FreeDome": null,
  "Max_user_height_overground_(ifd)": null,
  "Max_user_height_over_6\"_treadmill_(ifd)": null,
  "Powered_stroke_adjustment": null,
  "Manual_stroke_adjustment": null,
  "Base_options": null,
  "Max_footprint_(base_x_width_x_length)": null,
  "Standard_Base_width": null,
  "Max_treadmill_width_(sb)": null,
  "Min_door_width_(sb)": null,
  "Footprint_(width_x_length)_(sb)": null,
  "Base_option_2_width": null,
  "Max_treadmill_width_(bo2)": null,
  "Min_door_width_(bo2)": null,
  "Footprint_(width_x_length)_(bo2)": null,
  "Base_option_3_width": null,
  "Max_treadmill_width_(bo3)": null,
  "Min_door_width_(bo3)": null,
  "Footprint_(width_x_length)_(bo3)": null,
  "Clearance_(floor_to_bottom_of_base_U)": null,
  "Length_of_base_leg": null,
  "Width_of_base_leg": null,
  "Height_of_base_leg_from_ground": null,
  "Caster_diameter": null,
  "Harness_included": null,
  "BiSym_Tablet": null,
  "GaiterStool": null,
  "3D_Handlebar": null,
  "Parallel_Grab_Bar": null,
  "L-Shape_Handlears": null,
  "Q-Straps_-_3_resistances": null,
  "Power_requirements": null,
  "created_at": ISODate("2019-12-16T06:33:21.723Z"),
  "updated_at": ISODate("2019-12-16T06:33:21.723Z")
});
db.getCollection("product_specification").insert({
  "_id": ObjectId("5df7253122d6d8661dd45358"),
  "model": "Hug-NGo 350",
  "Market": null,
  "Units": null,
  "Max_user_weight": null,
  "Overall_height": null,
  "Max_user_height_overground": null,
  "Max_user_height_over_6\"_treadmill": null,
  "Removable_FreeDome": null,
  "Max_user_height_overground_(rfd)": null,
  "Max_user_height_over_6\"_treadmill_(rfd)": null,
  "Integrated_FreeDome": null,
  "Max_user_height_overground_(ifd)": null,
  "Max_user_height_over_6\"_treadmill_(ifd)": null,
  "Powered_stroke_adjustment": null,
  "Manual_stroke_adjustment": null,
  "Base_options": null,
  "Max_footprint_(base_x_width_x_length)": null,
  "Standard_Base_width": null,
  "Max_treadmill_width_(sb)": null,
  "Min_door_width_(sb)": null,
  "Footprint_(width_x_length)_(sb)": null,
  "Base_option_2_width": null,
  "Max_treadmill_width_(bo2)": null,
  "Min_door_width_(bo2)": null,
  "Footprint_(width_x_length)_(bo2)": null,
  "Base_option_3_width": null,
  "Max_treadmill_width_(bo3)": null,
  "Min_door_width_(bo3)": null,
  "Footprint_(width_x_length)_(bo3)": null,
  "Clearance_(floor_to_bottom_of_base_U)": null,
  "Length_of_base_leg": null,
  "Width_of_base_leg": null,
  "Height_of_base_leg_from_ground": null,
  "Caster_diameter": null,
  "Harness_included": null,
  "BiSym_Tablet": null,
  "GaiterStool": null,
  "3D_Handlebar": null,
  "Parallel_Grab_Bar": null,
  "L-Shape_Handlears": null,
  "Q-Straps_-_3_resistances": null,
  "Power_requirements": null,
  "created_at": ISODate("2019-12-16T06:33:21.723Z"),
  "updated_at": ISODate("2019-12-16T06:33:21.723Z")
});
db.getCollection("product_specification").insert({
  "_id": ObjectId("5df86e5c22d6d8661dd45484"),
  "model": "HugN-Go\r\n 250",
  "Max_user_weight": "250 lbs",
  "Max_user_height": "6' 0\"",
  "Vertical_lift": "24\"",
  "Inside_width_-fixed": "25/27/30",
  "Inside_width_-adj": {
    "": "N/A"
  },
  "Bed_clearance": "8\"",
  "created_at": ISODate("2019-12-17T05:57:48.39Z"),
  "updated_at": ISODate("2019-12-17T05:57:48.39Z")
});
db.getCollection("product_specification").insert({
  "_id": ObjectId("5df86e5c22d6d8661dd45486"),
  "model": "HugN-Go\r\n 350",
  "Max_user_weight": "350 lbs",
  "Max_user_height": "6' 6\"",
  "Vertical_lift": "24\"",
  "Inside_width_-fixed": "30/34",
  "Inside_width_-adj": {
    "": "25\" - 34\""
  },
  "Bed_clearance": "7\"",
  "created_at": ISODate("2019-12-17T05:57:48.39Z"),
  "updated_at": ISODate("2019-12-17T05:57:48.39Z")
});

/** product_specification_group records **/
db.getCollection("product_specification_group").insert({
  "_id": ObjectId("5de11ab4bc40b337297c4101"),
  "name": "Weight",
  "specification_row": "Max_user_weight",
  "created_at": ISODate("2019-11-29T13:18:44.272Z"),
  "updated_at": ISODate("2019-12-11T06:12:11.690Z")
});
db.getCollection("product_specification_group").insert({
  "_id": ObjectId("5de11abfbc40b337297c4108"),
  "name": "Height",
  "specification_row": "Overall_height",
  "created_at": ISODate("2019-11-29T13:18:55.657Z"),
  "updated_at": ISODate("2019-12-11T06:12:17.413Z")
});
db.getCollection("product_specification_group").insert({
  "_id": ObjectId("5df7971b22d6d8661dd4537d"),
  "name": "Max height overground",
  "specification_row": "Max_user_height_overground",
  "created_at": ISODate("2019-12-16T14:39:23.372Z"),
  "updated_at": ISODate("2019-12-16T14:39:23.372Z")
});
db.getCollection("product_specification_group").insert({
  "_id": ObjectId("5df7973622d6d8661dd45384"),
  "name": "Max height over treadmill",
  "specification_row": "Max_user_height_over_6\"_treadmill",
  "created_at": ISODate("2019-12-16T14:39:50.433Z"),
  "updated_at": ISODate("2019-12-16T14:39:50.433Z")
});
db.getCollection("product_specification_group").insert({
  "_id": ObjectId("5df7976f22d6d8661dd4538b"),
  "name": "Removable FreeDome",
  "specification_row": "Removable_FreeDome,Max_user_height_overground_(rfd),Max_user_height_over_6\"_treadmill_(rfd)",
  "created_at": ISODate("2019-12-16T14:40:47.966Z"),
  "updated_at": ISODate("2019-12-16T14:41:16.878Z")
});
db.getCollection("product_specification_group").insert({
  "_id": ObjectId("5df797b322d6d8661dd45398"),
  "name": "Integrated FreeDome",
  "specification_row": "Integrated_FreeDome,Max_user_height_overground_(ifd),Max_user_height_over_6\"_treadmill_(ifd)",
  "created_at": ISODate("2019-12-16T14:41:55.412Z"),
  "updated_at": ISODate("2019-12-16T14:41:55.412Z")
});
db.getCollection("product_specification_group").insert({
  "_id": ObjectId("5df797d722d6d8661dd4539f"),
  "name": "Stroke powered",
  "specification_row": "Powered_stroke_adjustment",
  "created_at": ISODate("2019-12-16T14:42:31.20Z"),
  "updated_at": ISODate("2019-12-16T14:42:31.20Z")
});
db.getCollection("product_specification_group").insert({
  "_id": ObjectId("5df797eb22d6d8661dd453a6"),
  "name": "Stroke manual",
  "specification_row": "Manual_stroke_adjustment",
  "created_at": ISODate("2019-12-16T14:42:51.827Z"),
  "updated_at": ISODate("2019-12-16T14:42:51.827Z")
});
db.getCollection("product_specification_group").insert({
  "_id": ObjectId("5df7980422d6d8661dd453ad"),
  "name": "Base options",
  "specification_row": "Base_options",
  "created_at": ISODate("2019-12-16T14:43:16.108Z"),
  "updated_at": ISODate("2019-12-16T14:43:16.108Z")
});
db.getCollection("product_specification_group").insert({
  "_id": ObjectId("5df7981a22d6d8661dd453b4"),
  "name": "Footprint",
  "specification_row": "Max_footprint_(base_x_width_x_length)",
  "created_at": ISODate("2019-12-16T14:43:38.598Z"),
  "updated_at": ISODate("2019-12-16T14:43:38.598Z")
});
db.getCollection("product_specification_group").insert({
  "_id": ObjectId("5df7983c22d6d8661dd453bb"),
  "name": "Standard Base",
  "specification_row": "Standard_Base_width,Max_treadmill_width_(sb),Min_door_width_(sb),Footprint_(width_x_length)_(sb)",
  "created_at": ISODate("2019-12-16T14:44:12.764Z"),
  "updated_at": ISODate("2019-12-16T14:44:12.764Z")
});
db.getCollection("product_specification_group").insert({
  "_id": ObjectId("5df7986222d6d8661dd453c2"),
  "name": "Base option 2",
  "specification_row": "Base_option_2_width,Max_treadmill_width_(bo2),Min_door_width_(bo2),Footprint_(width_x_length)_(bo2)",
  "created_at": ISODate("2019-12-16T14:44:50.67Z"),
  "updated_at": ISODate("2019-12-16T14:44:50.67Z")
});
db.getCollection("product_specification_group").insert({
  "_id": ObjectId("5df7988022d6d8661dd453cb"),
  "name": "Base option 3",
  "specification_row": "Base_option_3_width,Max_treadmill_width_(bo3),Min_door_width_(bo3),Footprint_(width_x_length)_(bo3)",
  "created_at": ISODate("2019-12-16T14:45:20.191Z"),
  "updated_at": ISODate("2019-12-16T14:45:20.191Z")
});
db.getCollection("product_specification_group").insert({
  "_id": ObjectId("5df798b922d6d8661dd453d2"),
  "name": "Clearance",
  "specification_row": "Clearance_(floor_to_bottom_of_base_U)",
  "created_at": ISODate("2019-12-16T14:46:17.840Z"),
  "updated_at": ISODate("2019-12-16T14:46:17.840Z")
});
db.getCollection("product_specification_group").insert({
  "_id": ObjectId("5df798f622d6d8661dd453d9"),
  "name": "Length base leg",
  "specification_row": "Length_of_base_leg",
  "created_at": ISODate("2019-12-16T14:47:18.114Z"),
  "updated_at": ISODate("2019-12-16T14:47:18.114Z")
});
db.getCollection("product_specification_group").insert({
  "_id": ObjectId("5df7990a22d6d8661dd453e0"),
  "name": "Width base leg",
  "specification_row": "Width_of_base_leg",
  "created_at": ISODate("2019-12-16T14:47:38.37Z"),
  "updated_at": ISODate("2019-12-16T14:47:38.37Z")
});
db.getCollection("product_specification_group").insert({
  "_id": ObjectId("5df7991e22d6d8661dd453e7"),
  "name": "Height base leg",
  "specification_row": "Height_of_base_leg_from_ground",
  "created_at": ISODate("2019-12-16T14:47:58.30Z"),
  "updated_at": ISODate("2019-12-16T14:47:58.30Z")
});
db.getCollection("product_specification_group").insert({
  "_id": ObjectId("5df7993522d6d8661dd453ee"),
  "name": "Caster",
  "specification_row": "Caster_diameter",
  "created_at": ISODate("2019-12-16T14:48:21.331Z"),
  "updated_at": ISODate("2019-12-16T14:48:21.331Z")
});
db.getCollection("product_specification_group").insert({
  "_id": ObjectId("5df7994a22d6d8661dd453f5"),
  "name": "Harnesses",
  "specification_row": "Harness_included",
  "created_at": ISODate("2019-12-16T14:48:42.93Z"),
  "updated_at": ISODate("2019-12-16T14:48:42.93Z")
});
db.getCollection("product_specification_group").insert({
  "_id": ObjectId("5df7995a22d6d8661dd453fc"),
  "name": "BiSym",
  "specification_row": "BiSym_Tablet",
  "created_at": ISODate("2019-12-16T14:48:58.810Z"),
  "updated_at": ISODate("2019-12-16T14:48:58.810Z")
});
db.getCollection("product_specification_group").insert({
  "_id": ObjectId("5df7997c22d6d8661dd45403"),
  "name": "GaiterStool",
  "specification_row": "GaiterStool",
  "created_at": ISODate("2019-12-16T14:49:32.883Z"),
  "updated_at": ISODate("2019-12-16T14:49:32.883Z")
});
db.getCollection("product_specification_group").insert({
  "_id": ObjectId("5df7999422d6d8661dd4540a"),
  "name": "3D Handlebars",
  "specification_row": "3D_Handlebar",
  "created_at": ISODate("2019-12-16T14:49:56.804Z"),
  "updated_at": ISODate("2019-12-16T14:51:16.989Z")
});
db.getCollection("product_specification_group").insert({
  "_id": ObjectId("5df799d722d6d8661dd45411"),
  "name": "Parallel Grab Bars",
  "specification_row": "Parallel_Grab_Bar",
  "created_at": ISODate("2019-12-16T14:51:03.161Z"),
  "updated_at": ISODate("2019-12-16T14:51:03.161Z")
});
db.getCollection("product_specification_group").insert({
  "_id": ObjectId("5df79a0122d6d8661dd4541e"),
  "name": "L-Shape Handlebars",
  "specification_row": "L-Shape_Handlears",
  "created_at": ISODate("2019-12-16T14:51:45.477Z"),
  "updated_at": ISODate("2019-12-16T14:51:45.477Z")
});
db.getCollection("product_specification_group").insert({
  "_id": ObjectId("5df79a1722d6d8661dd45425"),
  "name": "Q-Straps",
  "specification_row": "Q-Straps_-_3_resistances",
  "created_at": ISODate("2019-12-16T14:52:07.264Z"),
  "updated_at": ISODate("2019-12-16T14:52:07.264Z")
});
db.getCollection("product_specification_group").insert({
  "_id": ObjectId("5df79a2422d6d8661dd4542c"),
  "name": "Power",
  "specification_row": "Power_requirements",
  "created_at": ISODate("2019-12-16T14:52:20.628Z"),
  "updated_at": ISODate("2019-12-16T14:52:20.628Z")
});
db.getCollection("product_specification_group").insert({
  "_id": ObjectId("5df8fbfe22d6d8661dd454a8"),
  "name": "Vertical lift",
  "specification_row": "Vertical_lift",
  "created_at": ISODate("2019-12-17T16:02:06.941Z"),
  "updated_at": ISODate("2019-12-17T16:02:06.941Z")
});
db.getCollection("product_specification_group").insert({
  "_id": ObjectId("5df8fc1c22d6d8661dd454af"),
  "name": "Inside fixed",
  "specification_row": "Inside_width_-fixed",
  "created_at": ISODate("2019-12-17T16:02:36.826Z"),
  "updated_at": ISODate("2019-12-17T16:02:36.826Z")
});
db.getCollection("product_specification_group").insert({
  "_id": ObjectId("5df8fc3922d6d8661dd454b6"),
  "name": "Inside adj",
  "specification_row": "Inside_width_-adj",
  "created_at": ISODate("2019-12-17T16:03:05.928Z"),
  "updated_at": ISODate("2019-12-17T16:03:05.928Z")
});
db.getCollection("product_specification_group").insert({
  "_id": ObjectId("5df8fc5322d6d8661dd454bd"),
  "name": "Clearance",
  "specification_row": "Bed_clearance",
  "created_at": ISODate("2019-12-17T16:03:31.610Z"),
  "updated_at": ISODate("2019-12-17T16:03:31.610Z")
});

/** products records **/
db.getCollection("products").insert({
  "_id": ObjectId("5bd16edbb605151b7e93d899"),
  "lang_content": [
    ObjectId("5bd16edcb605151b7e93d89a"),
    ObjectId("5bd16edcb605151b7e93d89c")
  ],
  "image_gallery": [
    ObjectId("5d51ecd1fbb7781db271cc06"),
    ObjectId("5d51ecd1fbb7781db271cc07"),
    ObjectId("5dbc7421720c200ccbf7ad06")
  ],
  "name": "LG 300P",
  "slug": "lg-300p",
  "region": "NA",
  "image": "/Product/LG300P/lg300p_gkmini_walking.jpg",
  "short_description": "<p>18 oz. Premium Vinyl Banners</p>\r\n\r\n<p>18 oz. Premium Vinyl Banners&nbsp;</p>\r\n",
  "description": "<p>1The LGI300 P features 28&quot; of powered lift that can bring a patient up to 6&#39; 6&quot; tall and 300 pounds from sitting in a wheelchair to a full standing position over a treadmill. &nbsp;Additionally, the LGI&nbsp;300P Yoke can be manually adjusted to match your patient&#39;s height - making this one of our most versatile products.</p>\r\n",
  "video": "/video/video.mp4",
  "stock": NumberInt(100),
  "tags": "Sale",
  "price": NumberInt(0),
  "price_request": NumberInt(1),
  "meta_title": "Large Mesh Banners | Custom Vinyl Printing | Full Color Banner",
  "meta_description": "Large Mesh Banners | Custom Vinyl Printing | Full Color Banner",
  "status": NumberInt(1),
  "created_at": ISODate("2018-10-25T07:21:00.15Z"),
  "updated_at": ISODate("2019-11-01T18:06:25.544Z"),
  "category": ObjectId("5c5d422055a6cc290aae530b"),
  "featured_product": NumberInt(1),
  "featured_product_main": NumberInt(0),
  "related_specification": "",
  "specification_column": "ground,ground_fd,treadmill,treadmill_fd,treadmill_fdi,units",
  "schedule_service": NumberInt(0),
  "position": NumberInt(4),
  "testimonials": [
    ObjectId("5c36edb38ba4672adbd9402e")
  ],
  "banner_image": "",
  "related_product": "",
  "related_product_text": "",
  "serial_no": "",
  "service_description": "",
  "service_image": "",
  "specification_default": "",
  "specification_related": "",
  "specification_row": "",
  "specification_row_default": ""
});
db.getCollection("products").insert({
  "_id": ObjectId("5c24667cd4568d68ad108584"),
  "lang_content": [
    ObjectId("5c24667dd4568d68ad108585"),
    ObjectId("5c24667dd4568d68ad108587"),
    ObjectId("5c24667dd4568d68ad108589"),
    ObjectId("5c24667dd4568d68ad10858b")
  ],
  "image_gallery": [
    ObjectId("5d43795482d43979936671e2"),
    ObjectId("5d43795482d43979936671e1"),
    ObjectId("5d437a0d82d43979936671ec"),
    ObjectId("5d437a0d82d43979936671eb"),
    ObjectId("5dbc75a0720c200ccbf7ad1b")
  ],
  "name": "LG 500",
  "slug": "lg-500",
  "category": ObjectId("5c5d422055a6cc290aae530b"),
  "region": "NA",
  "image": "/Product/LG500/lg500_tall_gks22_therapist_def.jpg",
  "short_description": "<p>Test Product</p>\r\n",
  "description": "<p>Text Here</p>\r\n",
  "video": "/video/video.mp4",
  "stock": null,
  "tags": "dog",
  "price": null,
  "price_request": NumberInt(1),
  "meta_title": "test Product ",
  "meta_description": "Test Product",
  "status": NumberInt(1),
  "created_at": ISODate("2018-12-27T05:43:25.13Z"),
  "updated_at": ISODate("2019-12-09T05:57:29.981Z"),
  "featured_product": NumberInt(0),
  "featured_product_main": NumberInt(0),
  "related_specification": "100W,100FX",
  "specification_column": "footprint,ground,ground_fd,treadmill,treadmill_fd,treadmill_fdi",
  "schedule_service": NumberInt(1),
  "position": NumberInt(1),
  "testimonials": [
    ObjectId("5bd16ea2b605151b7e93d894"),
    ObjectId("5c36edb38ba4672adbd9402e")
  ],
  "banner_image": "",
  "related_product": "5d02757a60b1b8748527ee95,5d4bb3d8a9421434c38e69be",
  "related_product_text": "",
  "serial_no": "",
  "service_description": "",
  "service_image": "",
  "specification_default": "650 - Standard",
  "specification_related": "400 - Standard,500 - Deluxe,400 - Deluxe,400 - Tall,500 - Standard,650 - Standard",
  "specification_row": "Height,FreeDome,Standard Base width",
  "specification_row_default": "Height,Standard Base width"
});
db.getCollection("products").insert({
  "_id": ObjectId("5ccc103806e67e44fa0eb233"),
  "lang_content": [
    ObjectId("5ccc103806e67e44fa0eb235"),
    ObjectId("5ccc103806e67e44fa0eb239")
  ],
  "image_gallery": [
    ObjectId("5ccc103806e67e44fa0eb23c"),
    ObjectId("5ccc103806e67e44fa0eb23f")
  ],
  "testimonials": null,
  "name": "LG 300MX",
  "slug": "lg-300mx",
  "region": "NA",
  "image": "/Product/LG300mx/lg300mx_overground_therapist.jpg",
  "short_description": "<p>18 oz. Premium Vinyl Banners</p>\r\n\r\n<p>18 oz. Premium Vinyl Banners&nbsp;</p>\r\n",
  "description": "<p>The 300MX model is a cost-effective option for patients up to 300 pounds. The innovative design and simplified lifting mechanism provides a reasonable alternative when cost is of primary concern.</p>\r\n",
  "video": "/video/video.mp4",
  "stock": null,
  "tags": "New, Sale",
  "price": null,
  "price_request": NumberInt(1),
  "meta_title": "Large Mesh Banners | Custom Vinyl Printing | Full Color Banner",
  "meta_description": "Large Mesh Banners | Custom Vinyl Printing | Full Color Banner",
  "status": NumberInt(1),
  "created_at": ISODate("2018-10-25T07:21:00.15Z"),
  "updated_at": ISODate("2019-12-04T16:09:24.778Z"),
  "category": ObjectId("5c5d422055a6cc290aae530b"),
  "featured_product": NumberInt(1),
  "featured_product_main": NumberInt(0),
  "related_specification": "",
  "specification_column": "ground,ground_fd,treadmill,treadmill_fd,treadmill_fdi,units",
  "schedule_service": NumberInt(0),
  "position": NumberInt(6),
  "banner_image": "",
  "related_product": "5c24667cd4568d68ad108584,5cd198ce06e67e44fa0eb2e4",
  "related_product_text": "Can be used  With",
  "serial_no": "",
  "service_description": "",
  "service_image": "",
  "specification_default": "",
  "specification_related": "",
  "specification_row": "",
  "specification_row_default": ""
});
db.getCollection("products").insert({
  "_id": ObjectId("5cd198ce06e67e44fa0eb2e4"),
  "lang_content": [
    ObjectId("5cd198ce06e67e44fa0eb2e5"),
    ObjectId("5cd198ce06e67e44fa0eb2e7"),
    ObjectId("5cd198ce06e67e44fa0eb2e9"),
    ObjectId("5cd198ce06e67e44fa0eb2eb")
  ],
  "image_gallery": [
    ObjectId("5cd198ce06e67e44fa0eb2ed"),
    ObjectId("5cd198ce06e67e44fa0eb2ee"),
    ObjectId("5dbc74ec720c200ccbf7ad0f")
  ],
  "name": "LG 400",
  "slug": "lg-400",
  "category": ObjectId("5c5d422055a6cc290aae530b"),
  "region": "NA",
  "image": "/Product/LG400/lg400_def.png",
  "description": "<p>The LGI 400 features 32&quot; of powered lift that can quickly bring a patient up to 6&#39;8&quot; tall and 400 pounds from sitting in a wheelchair to a full standing position over a treadmill. The LG 400&nbsp;provides postural stability and bio-mechanically appropriate posture</p>\r\n",
  "video": "https://vimeo.com/334982174",
  "stock": null,
  "tags": "popular",
  "price": null,
  "price_request": NumberInt(1),
  "featured_product_main": NumberInt(0),
  "featured_product": NumberInt(0),
  "schedule_service": NumberInt(0),
  "related_specification": "300 - Standard,400 - Standard,400 - Deluxe,500 - Standard",
  "specification_column": "ground,ground_fd,leg_height,treadmill,treadmill_fd",
  "position": NumberInt(2),
  "meta_title": "",
  "meta_description": "",
  "status": NumberInt(1),
  "created_at": ISODate("2019-05-07T14:40:14.395Z"),
  "updated_at": ISODate("2019-12-16T14:59:12.25Z"),
  "testimonials": null,
  "banner_image": "",
  "related_product": "5cf00a39fb42690c45d441d2",
  "related_product_text": "Features and Add Ons",
  "specification_default": "300 - Standard,400 - Standard,400 - Deluxe,500 - Standard",
  "specification_related": "300P - Standard,300P - Deluxe,300 - Standard,400 - Standard,400 - Deluxe,400 - Tall,500 - Standard,500 - Tall,300 - Deluxe,500 - Deluxe",
  "specification_row": "Weight,Height,Max height overground,Max height over treadmill,Removable FreeDome,Integrated FreeDome,Stroke powered,Stroke manual,Base options,Footprint,Standard Base,Base option 2,Base option 3,Clearance,Length base leg,Width base leg,Height base leg,Caster,Harnesses,BiSym,GaiterStool,3D Handlebars,Parallel Grab Bars,L-Shape Handlebars,Q-Straps,Power",
  "serial_no": "",
  "service_description": "",
  "service_image": "",
  "specification_row_default": "Weight,Height,Max height overground,Max height over treadmill,Removable FreeDome,Integrated FreeDome,Base options,Standard Base,Clearance,Caster,BiSym,GaiterStool"
});
db.getCollection("products").insert({
  "_id": ObjectId("5cd1a2bb06e67e44fa0eb2f8"),
  "lang_content": [
    ObjectId("5cd1a2bb06e67e44fa0eb2f9"),
    ObjectId("5cd1a2bb06e67e44fa0eb2fb"),
    ObjectId("5cd1a2bb06e67e44fa0eb2fd"),
    ObjectId("5cd1a2bb06e67e44fa0eb2ff")
  ],
  "image_gallery": [
    ObjectId("5cd1a2bb06e67e44fa0eb301"),
    ObjectId("5cd1a2bb06e67e44fa0eb302"),
    ObjectId("5cd1abc906e67e44fa0eb334"),
    ObjectId("5cd1abfb06e67e44fa0eb33a"),
    ObjectId("5dbc7395720c200ccbf7acf5")
  ],
  "name": "LG 300",
  "slug": "lg-300",
  "category": ObjectId("5c5d422055a6cc290aae530b"),
  "region": "NA",
  "image": "/Product/LG300/lg300_def.png",
  "description": "<p>The LGI300 features 28&quot; of powered lift that can bring a patient up to 6&#39; 9&quot; tall and 300 pounds from sitting in a wheelchair to a full standing position over a treadmill. The 300&nbsp;model provides postural stability and biomechanically appropriate posture. Includes 1 iHarness.</p>\r\n",
  "video": "",
  "stock": null,
  "tags": "versatile",
  "price": null,
  "price_request": NumberInt(1),
  "featured_product_main": NumberInt(0),
  "featured_product": NumberInt(0),
  "schedule_service": NumberInt(0),
  "related_specification": "5d441057cfe3ee5c90880be7,5d441057cfe3ee5c90880bec,5d441057cfe3ee5c90880bed",
  "specification_column": "ground,ground_fd,standard,treadmill,treadmill_fd,treadmill_fdi",
  "position": NumberInt(3),
  "meta_title": "",
  "meta_description": "",
  "status": NumberInt(1),
  "created_at": ISODate("2019-05-07T15:22:35.407Z"),
  "updated_at": ISODate("2019-11-01T18:04:05.211Z"),
  "testimonials": null,
  "banner_image": "",
  "related_product": "",
  "related_product_text": "",
  "serial_no": "",
  "service_description": "",
  "service_image": "",
  "specification_default": "300P - Standard,300 - Standard,300 - Deluxe,400 - Standard",
  "specification_related": "",
  "specification_row": "ground,ground_fd",
  "specification_row_default": "units,max_wt,overall_height,ground,ground_fd"
});
db.getCollection("products").insert({
  "_id": ObjectId("5cf00a39fb42690c45d441d2"),
  "lang_content": [
    ObjectId("5cf00a39fb42690c45d441d3"),
    ObjectId("5cf00a39fb42690c45d441d5"),
    ObjectId("5cf00a39fb42690c45d441d7"),
    ObjectId("5cf00a39fb42690c45d441d9")
  ],
  "image_gallery": [
    ObjectId("5cf00a39fb42690c45d441db")
  ],
  "name": "Therapy Mouse",
  "slug": "therapy-mouse",
  "category": ObjectId("5cf0096dfb42690c45d441be"),
  "region": "NA",
  "image": "/Product/TherapyMouse/tm_def.png",
  "description": "<p>Therapy Mouse is a low cost, motion-sensing, wearable pointing device, that allows individuals who have difficulties using their hands to control a computer using any alternative body part. Simply plug into your computer and attach the sensor to the desired body part; the Therapy Mouse will translate user movements to precise, proportional computer control. &nbsp; Move the desired body part &ndash; and the computer pointer will move with it. &nbsp;Mouse clicks and selections can be performed either with the Therapy Mouse clicker, any other standard assistive switch, or with dwell software. &nbsp;Combine with our Gait Analysis software GaitSens&nbsp;to support&nbsp;<strong><a href=\"https://www.litegait.com/Blog-Dual-Tasking\">Dual Tasking</a></strong>&nbsp;during Gait Therapy!</p>\r\n",
  "video": "",
  "stock": null,
  "tags": "dual tasking, therapy mouse, ",
  "price": null,
  "price_request": NumberInt(0),
  "featured_product_main": NumberInt(0),
  "schedule_service": NumberInt(0),
  "related_specification": "5d441057cfe3ee5c90880be7,5d441057cfe3ee5c90880bec",
  "specification_column": "base_1_wt,base_2_wt,base_3_wt,base_options,bisym_include,clearance",
  "position": null,
  "meta_title": "",
  "meta_description": "",
  "status": NumberInt(1),
  "created_at": ISODate("2019-05-30T16:52:09.12Z"),
  "updated_at": ISODate("2019-10-11T03:47:47.11Z"),
  "banner_image": "/Banners/banner_therapymouse.png",
  "related_product": "",
  "related_product_text": "",
  "testimonials": [
    ""
  ]
});
db.getCollection("products").insert({
  "_id": ObjectId("5cffd17c60b1b8748527ee2d"),
  "lang_content": [
    ObjectId("5cffd17c60b1b8748527ee2e"),
    ObjectId("5cffd17c60b1b8748527ee30"),
    ObjectId("5cffd17c60b1b8748527ee32"),
    ObjectId("5cffd17c60b1b8748527ee34")
  ],
  "image_gallery": [
    ObjectId("5d4373ac82d43979936671c0"),
    ObjectId("5d43751982d43979936671c8"),
    ObjectId("5d43760f82d43979936671d3")
  ],
  "name": "LG 100MX",
  "slug": "lg-100mx",
  "category": ObjectId("5c748820092aa62529ff4d70"),
  "region": "NA",
  "image": "/Product/LG100mx/lg100mx_def.jpg",
  "description": "<p>The 100MX features an adjustable yoke for children up to 100 pounds. The adjustability built into these systems allows for growth potential in a single user case, and it allows facilities to easily treat all their patients with the same unit.</p>\r\n",
  "video": "",
  "stock": null,
  "tags": "pediatric, ",
  "price": null,
  "price_request": NumberInt(1),
  "featured_product_main": NumberInt(0),
  "schedule_service": NumberInt(0),
  "related_specification": "5c595721d6e8c821bc97c4f3,5c595721d6e8c821bc97c4f5,5c595721d6e8c821bc97c4f6,5c595721d6e8c821bc97c4f9,5c595721d6e8c821bc97c4fb",
  "specification_column": "maxwt,ground,ground_fd,treadmill,maxfootprint,footprint",
  "position": NumberInt(2),
  "meta_title": "",
  "meta_description": "",
  "status": NumberInt(1),
  "created_at": ISODate("2019-06-11T16:06:20.134Z"),
  "updated_at": ISODate("2019-12-04T16:13:09.527Z"),
  "testimonials": null,
  "banner_image": "",
  "related_product": "",
  "related_product_text": "",
  "serial_no": "",
  "service_description": "",
  "service_image": "",
  "specification_default": "",
  "specification_related": "",
  "specification_row": "",
  "specification_row_default": ""
});
db.getCollection("products").insert({
  "_id": ObjectId("5d02700260b1b8748527ee74"),
  "lang_content": [
    ObjectId("5d02700260b1b8748527ee75"),
    ObjectId("5d02700260b1b8748527ee77"),
    ObjectId("5d02700260b1b8748527ee79"),
    ObjectId("5d02700260b1b8748527ee7b")
  ],
  "image_gallery": [
    ObjectId("5d43724782d43979936671a4"),
    ObjectId("5d43724782d43979936671a5"),
    ObjectId("5d4372d282d43979936671b2")
  ],
  "name": "LG 200P",
  "slug": "lg-200p",
  "category": ObjectId("5c748820092aa62529ff4d70"),
  "region": "NA",
  "image": "/Product/LG200P/lg200p_def.jpg",
  "description": "<p>The 200P is designed to meet the needs of younger patients, from children to teenagers. The 200P&#39;s innovative yoke can be manually adjusted to fit the patient, while 28&quot; of powered lift can quickly bring children and teenagers up to 6&#39; 0&quot; and 200 pounds from sitting in a wheelchair to a full standing position over a treadmill. Includes 2 iHarnesses.</p>\r\n",
  "video": "",
  "stock": null,
  "tags": "pediatric, ",
  "price": null,
  "price_request": NumberInt(0),
  "featured_product_main": NumberInt(0),
  "schedule_service": NumberInt(0),
  "related_specification": "",
  "specification_column": "ground,ground_fd,treadmill",
  "position": NumberInt(6),
  "meta_title": "",
  "meta_description": "",
  "status": NumberInt(1),
  "created_at": ISODate("2019-06-13T15:47:14.644Z"),
  "updated_at": ISODate("2019-10-24T15:43:51.745Z"),
  "banner_image": "",
  "related_product": "",
  "related_product_text": "",
  "testimonials": null,
  "serial_no": "",
  "service_description": "",
  "service_image": "",
  "specification_default": "",
  "specification_related": "",
  "specification_row": "",
  "specification_row_default": ""
});
db.getCollection("products").insert({
  "_id": ObjectId("5d02757a60b1b8748527ee95"),
  "lang_content": [
    ObjectId("5d02757a60b1b8748527ee96"),
    ObjectId("5d02757a60b1b8748527ee98"),
    ObjectId("5d02757a60b1b8748527ee9a"),
    ObjectId("5d02757a60b1b8748527ee9c")
  ],
  "image_gallery": [
    ObjectId("5d43705182d439799366718c"),
    ObjectId("5d4370d582d4397993667194"),
    ObjectId("5d4370d582d4397993667193"),
    ObjectId("5d51f3b2fbb7781db271cc4a")
  ],
  "name": "GK-S20",
  "slug": "gk-s20",
  "category": ObjectId("5c78f6ae8592290d03e2004c"),
  "region": "NA",
  "image": "/Product/GaitKeeper/gk_s22_def.jpg",
  "description": "<p>The GaitKeeper S20 is ideal for meeting your rehabilitation needs, designed with features preferred by clinicians while being simple for patients. The easy-to-read display shows distance in feet/meters and miles/kilometers and allows for programming of goals for session. The display console is removable to allow the clinician to monitor progress and adjust parameters from various positions while assisting or monitoring the patient. The S20 features a true zero start and 0.1 mph increments allow for safe use for patients of all levels. The 20&quot; by 56&quot; belt provides ample walking space, while the high torque motor keeps the belt running smooth, even at very low speeds. The unique, patent-pending &ldquo;Adjustabars&rdquo; have more range of adjustability than any handlebars currently available, allowing for easy height and width adjustment, with 20 secure, incremental positions to match patients of all sizes. The GaitKeeper S20 speeds up to 6 mph, inclines to 15% and can support up to 400 pounds.<br />\r\n<em><strong>Certifications</strong></em>: TUVc TUVus, Standard UL, CAN/CSA&nbsp;</p>\r\n",
  "video": "",
  "stock": null,
  "tags": "new",
  "price": null,
  "price_request": NumberInt(0),
  "featured_product_main": NumberInt(0),
  "schedule_service": NumberInt(0),
  "related_specification": "",
  "specification_column": "",
  "position": NumberInt(2),
  "meta_title": "",
  "meta_description": "",
  "status": NumberInt(1),
  "created_at": ISODate("2019-06-13T16:10:34.834Z"),
  "updated_at": ISODate("2019-12-09T05:36:57.440Z"),
  "banner_image": "",
  "related_product": "",
  "related_product_text": "",
  "testimonials": null,
  "serial_no": "",
  "service_description": "",
  "service_image": "",
  "specification_default": "GK-S22,GK-S20",
  "specification_related": "GK-S22,GK-S20",
  "specification_row": "Weight,Height",
  "specification_row_default": ""
});
db.getCollection("products").insert({
  "_id": ObjectId("5d0bb37960b1b8748527ef97"),
  "lang_content": [
    ObjectId("5d0bb37960b1b8748527ef98"),
    ObjectId("5d0bb37960b1b8748527ef9a"),
    ObjectId("5d0bb37960b1b8748527ef9c"),
    ObjectId("5d0bb37960b1b8748527ef9e")
  ],
  "image_gallery": [
    ObjectId("5deddc9295b70729ea6d4d04")
  ],
  "name": "LG4H 220",
  "slug": "lg4h-220",
  "category": ObjectId("5c78f6718592290d03e20040"),
  "region": "NA",
  "image": "/Product/LG4H/lg4h_220_gkmini.jpg",
  "description": "<p>Provide more treatment time and options &bull; All the benefits of LiteGait&reg; at home &bull; Multiple cost effective payment options</p>\r\n",
  "video": "",
  "stock": null,
  "tags": "home",
  "price": 8.495,
  "price_request": NumberInt(0),
  "featured_product_main": NumberInt(0),
  "schedule_service": NumberInt(0),
  "related_specification": "",
  "specification_column": "treadmill,treadmill_fd,units",
  "position": NumberInt(2),
  "meta_title": "",
  "meta_description": "",
  "status": NumberInt(1),
  "created_at": ISODate("2019-06-20T16:25:29.767Z"),
  "updated_at": ISODate("2019-12-09T05:33:06.482Z"),
  "banner_image": "",
  "related_product": "",
  "related_product_text": "",
  "testimonials": null,
  "serial_no": "",
  "service_description": "",
  "service_image": "",
  "specification_default": "",
  "specification_related": "",
  "specification_row": "",
  "specification_row_default": ""
});
db.getCollection("products").insert({
  "_id": ObjectId("5d2debf9837f487c405e630d"),
  "lang_content": [
    ObjectId("5d2debf9837f487c405e630e"),
    ObjectId("5d2debf9837f487c405e6310"),
    ObjectId("5d2debf9837f487c405e6312"),
    ObjectId("5d2debf9837f487c405e6314")
  ],
  "image_gallery": [
    ObjectId("5d436c1a82d439799366714e"),
    ObjectId("5d436c1a82d439799366714f"),
    ObjectId("5d436eb382d4397993667179"),
    ObjectId("5d436ed882d439799366717f")
  ],
  "name": "HugN-Go 350",
  "slug": "hugn-go-350",
  "category": ObjectId("5d2de90b837f487c405e62ee"),
  "region": "NA",
  "image": "/Product/HugNgo/hugngo_def.jpg",
  "description": "<ul>\r\n\t<li>Supported Ambulation - ample leg room for gait</li>\r\n\t<li>Sit to Stand</li>\r\n\t<li>Seated to Seated transfers? - in and out of wheelchair, toilet and bed</li>\r\n\t<li>Battery operated power lift</li>\r\n\t<li>Easy to unweight patients during ambulation</li>\r\n\t<li>Rolls easily in both home and therapy settings</li>\r\n\t<li>Small turn radius</li>\r\n\t<li>Legs swing open for wide chairs and close for narrow doorways (optional)</li>\r\n\t<li>Low base available to fit under 9&quot; or 7&quot; bed frames</li>\r\n\t<li>Small footprint for storage</li>\r\n</ul>\r\n",
  "video": "",
  "stock": null,
  "price": null,
  "price_request": NumberInt(1),
  "featured_product_main": NumberInt(0),
  "schedule_service": NumberInt(0),
  "related_specification": "5c595721d6e8c821bc97c4f7",
  "specification_column": "",
  "position": NumberInt(1),
  "related_product_text": "",
  "banner_image": "",
  "meta_title": "",
  "meta_description": "",
  "status": NumberInt(1),
  "created_at": ISODate("2019-07-16T15:23:37.34Z"),
  "updated_at": ISODate("2019-12-17T16:12:12.455Z"),
  "related_product": "",
  "testimonials": null,
  "serial_no": "",
  "service_description": "",
  "service_image": "",
  "specification_default": "HugN-Go\r\n 250,HugN-Go\r\n 350",
  "specification_related": "",
  "specification_row": "Clearance,Inside fixed,Inside adj,Clearance",
  "specification_row_default": "Weight,Height,Vertical lift"
});
db.getCollection("products").insert({
  "_id": ObjectId("5d4287e182d43979936670b9"),
  "lang_content": [
    ObjectId("5d4287e182d43979936670ba"),
    ObjectId("5d4287e182d43979936670bc"),
    ObjectId("5d4287e182d43979936670be"),
    ObjectId("5d4287e182d43979936670c0")
  ],
  "image_gallery": [
    ObjectId("5d51f027fbb7781db271cc26"),
    ObjectId("5d51f027fbb7781db271cc28"),
    ObjectId("5d51f027fbb7781db271cc27"),
    ObjectId("5d51f027fbb7781db271cc29")
  ],
  "name": "GK mini",
  "slug": "gk-mini",
  "category": ObjectId("5c78f6ae8592290d03e2004c"),
  "region": "NA",
  "image": "/Product/GaitKeeper/gkmini_handlebars.jpg",
  "description": "<p>Powerful, portable and versatile walking surface<br />\r\nPerfect for peds and adult at home or the clinic<br />\r\nEasily roll from room to room - bring to the patient<br />\r\n21&quot; W x 40&quot;L Walking Surface (18&quot;W available)<br />\r\nWeight: 350 lb Max Patient Weight</p>\r\n\r\n<p>The GaitKeeper Mini&reg; is truly a revolutionary portable walking surface designed for the busy clinic with limited floor space, or a home setting. &nbsp; &nbsp;Integrating with the world renown LiteGait&reg; &amp; Walkable&reg; (pediatric LiteGait), the GaitKeeper Mini is everything you need to get the best out of your LiteGait.<br />\r\nDo not be misled by the small size of this workhorse - this powerful treadmill performs. &nbsp;With a true zero start and steady 0.1 mph starting speed, the GaitKeeper Mini keeps a steady pace up to a maximum of 3 mph for all users from Pediatrics to a 350lb adult. &nbsp;The full featured handheld remote provides LED display of the belt speed and gives the clinician complete control of the treadmill regardless of location. Integrates with LiteGait.</p>\r\n\r\n<p>The small 40&quot; x 21&quot; (or 18&quot;W) &nbsp;walking surface fits perfectly under the LiteGait or Walkable; yet provides sufficient surface for most patients. &nbsp;With a footprint of only 42&rdquo; by 23&rdquo; &nbsp;or (21&quot;) and weighing less than 65 lbs, the GaitKeeper Mini fits in the smallest locations and is easily rolled or moved out of the way for storage when needed. &nbsp;You can even bring this treadmill with your LiteGait into your patient&rsquo;s room and it will fit under most beds! &nbsp;</p>\r\n\r\n<p>The GaitKeeper Mini continues Mobility Research&rsquo;s almost 20-year history of providing innovative rehabilitation therapy products to rehabilitation clinicians. &nbsp;</p>\r\n\r\n<p>US Patent# 9,339,683</p>\r\n",
  "video": "",
  "stock": null,
  "price": null,
  "price_request": NumberInt(0),
  "featured_product_main": NumberInt(0),
  "schedule_service": NumberInt(0),
  "related_specification": "",
  "specification_column": "units",
  "position": NumberInt(3),
  "related_product_text": "",
  "banner_image": "",
  "meta_title": "",
  "meta_description": "",
  "status": NumberInt(1),
  "created_at": ISODate("2019-08-01T06:34:09.159Z"),
  "updated_at": ISODate("2019-10-30T14:26:56.913Z"),
  "related_product": "",
  "testimonials": null,
  "serial_no": "",
  "service_description": "",
  "service_image": "",
  "specification_default": "",
  "specification_related": "",
  "specification_row": "",
  "specification_row_default": ""
});
db.getCollection("products").insert({
  "_id": ObjectId("5d4bb3d8a9421434c38e69be"),
  "lang_content": [
    ObjectId("5d4bb3d8a9421434c38e69bf"),
    ObjectId("5d4bb3d8a9421434c38e69c1"),
    ObjectId("5d4bb3d8a9421434c38e69c3"),
    ObjectId("5d4bb3d8a9421434c38e69c5")
  ],
  "image_gallery": [
    ObjectId("5d4bb3d8a9421434c38e69c7"),
    ObjectId("5d51edf9fbb7781db271cc1b")
  ],
  "name": "GaitSens 2.0",
  "slug": "gaitsens-20",
  "category": ObjectId("5d14f1540a2c2e561c73512b"),
  "region": "NA",
  "image": "/Product/GaitSens/gaitsens_def.jpg",
  "description": "<h3 style=\"color:#000000; font-style:normal; margin-left:0px; margin-right:0px; text-align:left\">Easily provide objective gait data</h3>\r\n\r\n<p style=\"text-align:left\">Objective gait assessment with no setup. Sensors installed in GaitKeeper Treadmill send data 250 times a second to the supplied tablet. The GaitSens software determines gait events in real time. Plug in treadmill, run the software, system is ready to go! Initialize once and have clinical gait assessment tools and reports at your fingertips.</p>\r\n",
  "video": "",
  "stock": null,
  "price": null,
  "price_request": NumberInt(1),
  "featured_product_main": NumberInt(0),
  "schedule_service": NumberInt(0),
  "related_specification": "",
  "specification_column": "base_1_wt",
  "position": null,
  "related_product_text": "",
  "banner_image": "",
  "meta_title": "",
  "meta_description": "",
  "status": NumberInt(1),
  "created_at": ISODate("2019-08-08T05:32:08.621Z"),
  "updated_at": ISODate("2019-12-06T20:45:59.591Z"),
  "related_product": "",
  "testimonials": [
    ObjectId("5bd16ea2b605151b7e93d894")
  ],
  "serial_no": "",
  "service_description": "",
  "service_image": "",
  "specification_default": "",
  "specification_related": "",
  "specification_row": "",
  "specification_row_default": ""
});
db.getCollection("products").insert({
  "_id": ObjectId("5d5b74df07ea1355fada742e"),
  "lang_content": [
    ObjectId("5d5b74df07ea1355fada742f"),
    ObjectId("5d5b74df07ea1355fada7431"),
    ObjectId("5d5b74df07ea1355fada7433"),
    ObjectId("5d5b74df07ea1355fada7435")
  ],
  "image_gallery": [
    
  ],
  "name": "LG 75",
  "slug": "lg-75",
  "category": ObjectId("5c748820092aa62529ff4d70"),
  "region": "NA",
  "image": "/WK_75_01-2s.gif",
  "description": "<p>The LG75 is a lightweight rehabilitation device for children up to 75 pounds. The design includes parallel handle bars and a supportive, nonpowered yoke that can be adjusted easily for more control of posture and balance.</p>\r\n",
  "video": "",
  "stock": null,
  "price": null,
  "price_request": NumberInt(1),
  "featured_product_main": NumberInt(0),
  "schedule_service": NumberInt(0),
  "related_specification": "",
  "specification_column": "",
  "position": NumberInt(1),
  "related_product_text": "",
  "related_product": "",
  "banner_image": "",
  "meta_title": "",
  "meta_description": "",
  "status": NumberInt(1),
  "created_at": ISODate("2019-08-20T04:19:43.692Z"),
  "updated_at": ISODate("2019-12-04T16:27:17.475Z"),
  "testimonials": null,
  "specification_default": "75W,100FX,100W,100MX",
  "specification_related": "75W,100FX,100W,100MX",
  "specification_row": "",
  "serial_no": "HP101,HP102",
  "service_description": "Service page data of The LG75 is a lightweight rehabilitation device for children up to 75 pounds. The design includes parallel handle bars and a supportive, nonpowered yoke that can be adjusted easily for more control of posture and balance",
  "service_image": "/LG200P_300P_4606.png",
  "specification_row_default": ""
});
db.getCollection("products").insert({
  "_id": ObjectId("5d7a6662f3c6d306896b280e"),
  "lang_content": [
    ObjectId("5d7a6662f3c6d306896b280f"),
    ObjectId("5d7a6662f3c6d306896b2811"),
    ObjectId("5d7a6662f3c6d306896b2813"),
    ObjectId("5d7a6662f3c6d306896b2815")
  ],
  "image_gallery": [
    ObjectId("5d7a6662f3c6d306896b2817")
  ],
  "name": "LGV",
  "slug": "lgv",
  "category": ObjectId("5d37d4302a02f4259583713f"),
  "region": "NA",
  "image": "/Product/LGV/lgv_def.jpg",
  "description": "<p>The LiteGait&reg;-V model (LiteGait-Veterinary) can be used for quadrupeds weighing up to 200 pounds. The unit supports the trunk of the animal so that varying amounts of weight can be put on the limbs during gait training. Harnesses of different sizes are available for optimal fitting to individual animals. A battery operated lift mechanism allows for positioning for a wide range of body sizes from small to giant breed dogs.</p>\r\n\r\n<p>​Research on adults and children with neurological disorders has shown that utilizing the concepts of Body Weight Support treadmill training (also known as Partial Weight Bearing Treadmill Training) can be an effective method in training or retraining patients to walk. Several years ago an innovative doctor of veterinary medicine who was also trained as a physical therapist asked if these concepts could be transferred to companion animals. The answer has resulted in the LiteGait&reg;-V model (LiteGait-Veterinary). This unit can be used for quadrupeds weighing up to 200 pounds. The unit supports the trunk of the animal so that varying amounts of weight can be put on the limbs during gait training. Harnesses of different sizes are available for optimal fitting to individual animals. A battery operated lift mechanism allows for positioning for a wide range of body sizes from small to giant breed dogs.</p>\r\n\r\n<p>The unit can be utilized to assist the animal in moving over ground or over a slow moving treadmill. In addition, in veterinary practice, the LiteGait-V has the advantage of serving as a transfer device to assist handlers in lifting the animal into standing position</p>\r\n",
  "video": "https://vimeo.com/352357004",
  "stock": null,
  "price": null,
  "price_request": NumberInt(1),
  "featured_product_main": NumberInt(0),
  "schedule_service": NumberInt(0),
  "specification_related": "",
  "specification_default": "",
  "specification_row": "",
  "position": NumberInt(1),
  "related_product_text": "",
  "related_product": "",
  "banner_image": "",
  "serial_no": "",
  "service_image": "",
  "service_description": "",
  "meta_title": "",
  "meta_description": "",
  "status": NumberInt(1),
  "created_at": ISODate("2019-09-12T15:38:10.154Z"),
  "updated_at": ISODate("2019-10-11T03:47:47.11Z"),
  "testimonials": null,
  "specification_row_default": ""
});
db.getCollection("products").insert({
  "_id": ObjectId("5d7bca91e696962cf1b33b03"),
  "lang_content": [
    ObjectId("5d7bca91e696962cf1b33b04"),
    ObjectId("5d7bca91e696962cf1b33b06"),
    ObjectId("5d7bca91e696962cf1b33b08"),
    ObjectId("5d7bca91e696962cf1b33b0a")
  ],
  "image_gallery": [
    
  ],
  "name": "LG I 250 / 350 (General)",
  "slug": "lg-i-250--350-general",
  "category": ObjectId("5c5d422055a6cc290aae530b"),
  "region": "NA",
  "image": "",
  "description": "<p>no</p>\r\n",
  "video": "",
  "stock": null,
  "price": null,
  "price_request": NumberInt(0),
  "featured_product_main": NumberInt(0),
  "schedule_service": NumberInt(1),
  "specification_related": "",
  "specification_default": "",
  "specification_row": "",
  "position": null,
  "related_product_text": "",
  "related_product": "",
  "banner_image": "",
  "serial_no": "",
  "service_image": "",
  "service_description": "Original unit, built over 25 years ago and is still being utilized in many facilities",
  "meta_title": "",
  "meta_description": "",
  "status": NumberInt(0),
  "created_at": ISODate("2019-09-13T16:57:53.866Z"),
  "updated_at": ISODate("2019-10-28T16:30:58.388Z"),
  "specification_row_default": "",
  "testimonials": null
});
db.getCollection("products").insert({
  "_id": ObjectId("5d88f0db72c6024c7f2edd38"),
  "lang_content": [
    ObjectId("5d88f0db72c6024c7f2edd39"),
    ObjectId("5d88f0db72c6024c7f2edd3b"),
    ObjectId("5d88f0db72c6024c7f2edd3d"),
    ObjectId("5d88f0db72c6024c7f2edd3f")
  ],
  "image_gallery": [
    
  ],
  "name": "LG II / LG Jr.",
  "slug": "lg-ii--lg-jr",
  "category": ObjectId("5c748820092aa62529ff4d70"),
  "region": "NA",
  "image": "",
  "description": "",
  "video": "",
  "stock": null,
  "price": null,
  "price_request": NumberInt(0),
  "featured_product_main": NumberInt(0),
  "schedule_service": NumberInt(0),
  "specification_related": "",
  "specification_default": "",
  "specification_row": "",
  "position": null,
  "related_product_text": "",
  "related_product": "",
  "banner_image": "",
  "serial_no": "",
  "service_image": "/accessories_img6.jpg",
  "service_description": "need history text, how many sentences show up on the home page\r\nas opposed to after clicking on the product link and going to the individual page. Why are the links different on the landing page as opposed to after I choose the model. and they are not really different, they just dont show the same on both pages. I am not sure what  is the reason that is. are  the links defaulted on the second page of the fist page. plus the contact support page is only on the second page but unfortunately is goes to the contact us page instead of  service support.\r\nadding more text to check the disable button, I think it changes if i edit",
  "meta_title": "",
  "meta_description": "",
  "status": NumberInt(0),
  "created_at": ISODate("2019-09-23T16:20:43.436Z"),
  "updated_at": ISODate("2019-11-06T16:08:17.349Z"),
  "specification_row_default": "",
  "testimonials": null
});
db.getCollection("products").insert({
  "_id": ObjectId("5d9e21cf7dbdaa69d72e5ed5"),
  "lang_content": [
    ObjectId("5d9e21cf7dbdaa69d72e5ed6"),
    ObjectId("5d9e21cf7dbdaa69d72e5ed8"),
    ObjectId("5d9e21cf7dbdaa69d72e5eda"),
    ObjectId("5d9e21cf7dbdaa69d72e5edc")
  ],
  "image_gallery": [
    
  ],
  "name": "Q-pads",
  "slug": "q-pads",
  "category": ObjectId("5d2de7f4837f487c405e62e2"),
  "region": "NA",
  "image": "/accessories_img2.jpg",
  "description": "<p>Q-pads collorful and fun</p>\r\n",
  "video": "",
  "stock": null,
  "price": NumberInt(1299),
  "price_request": NumberInt(0),
  "featured_product_main": NumberInt(1),
  "schedule_service": NumberInt(1),
  "specification_related": "",
  "specification_default": "",
  "specification_row": "",
  "specification_row_default": "",
  "position": null,
  "related_product_text": "goes with",
  "related_product": "",
  "banner_image": "",
  "serial_no": "",
  "service_image": "",
  "service_description": "",
  "meta_title": "",
  "meta_description": "",
  "status": NumberInt(1),
  "created_at": ISODate("2019-10-09T18:07:11.97Z"),
  "updated_at": ISODate("2019-10-11T03:47:46.966Z"),
  "testimonials": null
});
db.getCollection("products").insert({
  "_id": ObjectId("5de7dc82e3d0053b781ed362"),
  "lang_content": [
    ObjectId("5de7dc82e3d0053b781ed363"),
    ObjectId("5de7dc82e3d0053b781ed365"),
    ObjectId("5de7dc82e3d0053b781ed367"),
    ObjectId("5de7dc82e3d0053b781ed369")
  ],
  "image_gallery": [
    
  ],
  "name": "LG 100FX",
  "slug": "lg-100fx",
  "category": ObjectId("5c748820092aa62529ff4d70"),
  "region": "NA",
  "image": "/Product/LG100mx/LG100MX-GkMini-01-4509.jpg",
  "description": "",
  "video": "",
  "stock": null,
  "price": null,
  "price_request": NumberInt(1),
  "featured_product_main": NumberInt(0),
  "schedule_service": NumberInt(0),
  "specification_related": "",
  "specification_default": "",
  "specification_row": "",
  "specification_row_default": "",
  "position": NumberInt(3),
  "related_product_text": "",
  "related_product": "",
  "banner_image": "",
  "serial_no": "",
  "service_image": "",
  "service_description": "",
  "meta_title": "",
  "meta_description": "",
  "status": NumberInt(1),
  "created_at": ISODate("2019-12-04T16:19:14.698Z"),
  "updated_at": ISODate("2019-12-04T16:19:46.694Z"),
  "testimonials": null
});
db.getCollection("products").insert({
  "_id": ObjectId("5de7dd91e3d0053b781ed37e"),
  "lang_content": [
    ObjectId("5de7dd91e3d0053b781ed37f"),
    ObjectId("5de7dd91e3d0053b781ed381"),
    ObjectId("5de7dd91e3d0053b781ed383"),
    ObjectId("5de7dd91e3d0053b781ed385")
  ],
  "image_gallery": [
    
  ],
  "name": "LG 200MX",
  "slug": "lg-200mx",
  "category": ObjectId("5c748820092aa62529ff4d70"),
  "region": "NA",
  "image": "/Product/LG100mx/lg200mx_100mx_pair.jpg",
  "description": "",
  "video": "",
  "stock": null,
  "price": null,
  "price_request": NumberInt(0),
  "featured_product_main": NumberInt(0),
  "schedule_service": NumberInt(0),
  "specification_related": "",
  "specification_default": "",
  "specification_row": "",
  "specification_row_default": "",
  "position": NumberInt(4),
  "related_product_text": "",
  "related_product": "",
  "banner_image": "",
  "serial_no": "",
  "service_image": "",
  "service_description": "",
  "meta_title": "",
  "meta_description": "",
  "status": NumberInt(1),
  "created_at": ISODate("2019-12-04T16:23:45.688Z"),
  "updated_at": ISODate("2019-12-04T16:23:45.699Z")
});
db.getCollection("products").insert({
  "_id": ObjectId("5de9e9a395b70729ea6d4c58"),
  "lang_content": [
    ObjectId("5de9e9a395b70729ea6d4c59"),
    ObjectId("5de9e9a395b70729ea6d4c5b"),
    ObjectId("5de9e9a395b70729ea6d4c5d"),
    ObjectId("5de9e9a495b70729ea6d4c5f")
  ],
  "image_gallery": [
    
  ],
  "name": "GK-S22",
  "slug": "gk-s22",
  "category": ObjectId("5c78f6ae8592290d03e2004c"),
  "region": "NA",
  "image": "/Product/GaitKeeper/gk_s22_01.jpg",
  "description": "<p>The GaitKeeper S22 is our premium rehabilitation treadmill. Its substantial 22&quot; by 56&quot; walking surface has a REVERSE function for more functional gait activities. The easy-to-read display shows distance in feet/meters and miles/kilometers and allows for programming of goals for session. The display console is removable to allow the clinician to monitor progress and adjust parameters from various positions while assisting or monitoring the patient. A true zero start and 0.1 mph increments allow for safe use for patients of all levels. The efficient AC motor produces high torque with cool and quiet operation, allowing smooth belt movement, even at very low speeds and when transitioning from forward to reverse. The unique, patent-pending &ldquo;Adjustabars&rdquo; have more range of adjustability than any handlebars currently available, allowing for easy height and width adjustment, with 20 secure, incremental positions to match patients of all sizes. . The GaitKeeper S22 speeds up to 10 mph forward and 3 mph in reverse, inclines up to 15% and can support up to 400 pounds.<br />\r\n<em><strong>Certifications</strong></em>: TUVc,&nbsp;TUVus, UL, CAN/CSA, MDD IEC60601-1-2&nbsp;(220V)</p>\r\n\r\n<p>GaitKeeper S22 is available installed with our&nbsp;<a href=\"https://www.litegait.com/products/GaitSens-2000\">GaitSens&nbsp;</a>software.&nbsp;&nbsp;GaitSens&nbsp; provides real-time temporal and spatial parameters of gait with objective reports and patient feedback functions.</p>\r\n",
  "video": "https://vimeo.com/332036369",
  "stock": null,
  "price": null,
  "price_request": NumberInt(0),
  "featured_product_main": NumberInt(0),
  "schedule_service": NumberInt(0),
  "specification_related": "",
  "specification_default": "",
  "specification_row": "",
  "specification_row_default": "",
  "position": NumberInt(1),
  "related_product_text": "",
  "related_product": "",
  "banner_image": "",
  "serial_no": "",
  "service_image": "",
  "service_description": "",
  "meta_title": "",
  "meta_description": "",
  "status": NumberInt(1),
  "created_at": ISODate("2019-12-06T05:39:47.868Z"),
  "updated_at": ISODate("2019-12-09T05:36:30.118Z"),
  "testimonials": null
});
db.getCollection("products").insert({
  "_id": ObjectId("5deada7495b70729ea6d4cc3"),
  "lang_content": [
    ObjectId("5deada7495b70729ea6d4cc4"),
    ObjectId("5deada7495b70729ea6d4cc6"),
    ObjectId("5deada7495b70729ea6d4cc8"),
    ObjectId("5deada7495b70729ea6d4cca")
  ],
  "image_gallery": [
    ObjectId("5deada7495b70729ea6d4ccd")
  ],
  "name": "HugN-Go 250",
  "slug": "hugn-go-250",
  "category": ObjectId("5d2de90b837f487c405e62ee"),
  "region": "NA",
  "image": "/Product/HugNgo/hugngo_03.png",
  "description": "<p>text</p>\r\n",
  "video": "",
  "stock": null,
  "price": NumberInt(0),
  "price_request": NumberInt(0),
  "featured_product_main": NumberInt(0),
  "schedule_service": NumberInt(0),
  "specification_related": "HugN-Go\r\n 250,HugN-Go\r\n 350",
  "specification_default": "HugN-Go\r\n 250,HugN-Go\r\n 350",
  "specification_row": "Weight,Height,Clearance,Vertical lift,Inside fixed,Inside adj,Clearance",
  "specification_row_default": "Weight,Height,Vertical lift",
  "position": NumberInt(1),
  "related_product_text": "",
  "related_product": "",
  "banner_image": "",
  "serial_no": "",
  "service_image": "/Product/HugNgo/hugngo_250_350_pair.jpg",
  "service_description": "",
  "meta_title": "",
  "meta_description": "",
  "status": NumberInt(1),
  "created_at": ISODate("2019-12-06T22:47:16.843Z"),
  "updated_at": ISODate("2019-12-17T16:11:08.288Z"),
  "testimonials": null
});
db.getCollection("products").insert({
  "_id": ObjectId("5deddc2695b70729ea6d4ced"),
  "lang_content": [
    ObjectId("5deddc2695b70729ea6d4cee"),
    ObjectId("5deddc2695b70729ea6d4cf0"),
    ObjectId("5deddc2695b70729ea6d4cf2"),
    ObjectId("5deddc2695b70729ea6d4cf4")
  ],
  "image_gallery": [
    
  ],
  "name": "LG4H 330",
  "slug": "lg4h-330",
  "category": ObjectId("5c78f6718592290d03e20040"),
  "region": "NA",
  "image": "/Product/LG4H/LG4H330.png",
  "description": "<p>330</p>\r\n",
  "video": "",
  "stock": null,
  "price": null,
  "price_request": NumberInt(1),
  "featured_product_main": NumberInt(0),
  "schedule_service": NumberInt(0),
  "specification_related": "",
  "specification_default": "",
  "specification_row": "",
  "specification_row_default": "",
  "position": NumberInt(1),
  "related_product_text": "",
  "related_product": "",
  "banner_image": "",
  "serial_no": "",
  "service_image": "",
  "service_description": "",
  "meta_title": "",
  "meta_description": "",
  "status": NumberInt(1),
  "created_at": ISODate("2019-12-09T05:31:18.683Z"),
  "updated_at": ISODate("2019-12-09T05:33:25.253Z"),
  "testimonials": null
});
db.getCollection("products").insert({
  "_id": ObjectId("5deddcf795b70729ea6d4d17"),
  "lang_content": [
    ObjectId("5deddcf795b70729ea6d4d18"),
    ObjectId("5deddcf795b70729ea6d4d1a"),
    ObjectId("5deddcf795b70729ea6d4d1c"),
    ObjectId("5deddcf795b70729ea6d4d1e")
  ],
  "image_gallery": [
    
  ],
  "name": "LG4H 110",
  "slug": "lg4h-110",
  "category": ObjectId("5c78f6718592290d03e20040"),
  "region": "NA",
  "image": "/Product/LG4H/LG4H110-Og-02-0943-Isolated-Clean.png",
  "description": "<p>110</p>\r\n",
  "video": "",
  "stock": null,
  "price": null,
  "price_request": NumberInt(0),
  "featured_product_main": NumberInt(0),
  "schedule_service": NumberInt(0),
  "specification_related": "",
  "specification_default": "",
  "specification_row": "",
  "specification_row_default": "",
  "position": NumberInt(3),
  "related_product_text": "",
  "related_product": "",
  "banner_image": "",
  "serial_no": "",
  "service_image": "",
  "service_description": "",
  "meta_title": "",
  "meta_description": "",
  "status": NumberInt(1),
  "created_at": ISODate("2019-12-09T05:34:47.651Z"),
  "updated_at": ISODate("2019-12-09T05:34:47.664Z")
});

/** quote_forms records **/
db.getCollection("quote_forms").insert({
  "_id": ObjectId("5c668c3fbc19ad187c839cbd"),
  "product_id": "5c668a8fbc19ad187c839c88",
  "product_type": "parts",
  "type": "textbox",
  "title": "How much Quantity You want",
  "slug": "how-much-quantity-you-want",
  "options": "",
  "placeholder": "How much Quantity You want",
  "required": NumberInt(1),
  "position": NumberInt(5),
  "status": NumberInt(1),
  "created_at": ISODate("2019-02-15T09:54:07.254Z"),
  "updated_at": ISODate("2019-02-15T09:54:07.254Z")
});
db.getCollection("quote_forms").insert({
  "_id": ObjectId("5d9eae416b548c52a943a0c0"),
  "product_id": "5cd198ce06e67e44fa0eb2e4",
  "product_type": "products",
  "type": "textbox",
  "title": "How much Quantity You want",
  "options": "",
  "placeholder": "",
  "required": NumberInt(0),
  "position": null,
  "status": NumberInt(1),
  "created_at": ISODate("2019-10-10T04:06:25.375Z"),
  "updated_at": ISODate("2019-10-10T04:06:25.375Z")
});

/** regions records **/
db.getCollection("regions").insert({
  "_id": ObjectId("5bd16e0cb605151b7e93d86b"),
  "code": "NA",
  "name": "North America",
  "status": NumberInt(1),
  "created_at": ISODate("2018-10-25T07:17:32.207Z"),
  "updated_at": ISODate("2019-01-18T11:41:21.929Z"),
  "image": "/flag/us_flag.png"
});
db.getCollection("regions").insert({
  "_id": ObjectId("5bd16e16b605151b7e93d86f"),
  "code": "uk",
  "name": "UK & Ireland",
  "status": NumberInt(1),
  "created_at": ISODate("2018-10-25T07:17:42.822Z"),
  "updated_at": ISODate("2019-01-18T11:43:57.316Z"),
  "image": "/flag/ireland.png"
});
db.getCollection("regions").insert({
  "_id": ObjectId("5c0acef2b52d1035f4b1418f"),
  "code": "Eu",
  "name": "Europe",
  "status": NumberInt(1),
  "created_at": ISODate("2018-12-07T19:50:10.97Z"),
  "updated_at": ISODate("2019-01-18T11:41:33.663Z"),
  "image": "/flag/eu_flag.png"
});
db.getCollection("regions").insert({
  "_id": ObjectId("5c0acf35b52d1035f4b14193"),
  "code": "Wd",
  "name": "World",
  "status": NumberInt(1),
  "created_at": ISODate("2018-12-07T19:51:17.210Z"),
  "updated_at": ISODate("2019-07-29T10:48:40.304Z"),
  "image": "/flag/world.jpeg"
});

/** requests records **/
db.getCollection("requests").insert({
  "_id": ObjectId("5c668c4bbc19ad187c839cc2"),
  "part_id": ObjectId("5c668a8fbc19ad187c839c88"),
  "type": "part",
  "name": "RAJ KISHORE VERMA",
  "email": "verma.rajkishore@gmail.com",
  "phone": "9950192509",
  "message": "Test Message",
  "customData": "{\"how-much-quantity-you-want\":\"2\"}",
  "status": NumberInt(1),
  "created_at": ISODate("2019-02-15T09:54:19.932Z"),
  "updated_at": ISODate("2019-02-15T09:54:19.932Z")
});
db.getCollection("requests").insert({
  "_id": ObjectId("5c668d8a092aa62529ff4bff"),
  "product_id": ObjectId("5c24667cd4568d68ad108584"),
  "type": "product",
  "name": "RAJ KISHORE VERMA",
  "email": "verma.rajkishore@gmail.com",
  "phone": "9950192509",
  "message": "fsfsdf",
  "customData": "{}",
  "status": NumberInt(1),
  "created_at": ISODate("2019-02-15T09:59:38.725Z"),
  "updated_at": ISODate("2019-02-15T09:59:38.725Z")
});
db.getCollection("requests").insert({
  "_id": ObjectId("5cb7b2122a31271b4165f3f8"),
  "part_id": ObjectId("5c668a8fbc19ad187c839c88"),
  "type": "part",
  "name": "Monica Serie",
  "email": "monica@litegait.com",
  "phone": "7327792949",
  "message": "where do I go",
  "customData": "{\"How much Quantity You want\":\"1\"}",
  "status": NumberInt(1),
  "created_at": ISODate("2019-04-17T23:09:06.850Z"),
  "updated_at": ISODate("2019-04-17T23:09:06.850Z")
});
db.getCollection("requests").insert({
  "_id": ObjectId("5d79c1bdf3c6d306896b2803"),
  "product_id": ObjectId("5c24667cd4568d68ad108584"),
  "type": "product",
  "name": "Colt Barry",
  "email": "kafyvafil@mailinator.com",
  "phone": "+1 (294) 548-4291",
  "message": "Voluptas a expedita ",
  "customData": "{}",
  "status": NumberInt(1),
  "created_at": ISODate("2019-09-12T03:55:41.49Z"),
  "updated_at": ISODate("2019-09-12T03:55:41.49Z")
});

/** seminar_contents records **/
db.getCollection("seminar_contents").insert({
  "_id": ObjectId("5bf3a7cebfe6552a98582b00"),
  "lang": "en",
  "description": "<p>This is a basic to intermediate course designed to present current concepts of weight bearing assisted gait training, neuroplasticity, motor learning, and principles of effective therapeutic exercise and teach how to apply them to patient treatment. Current therapeutic interventions that utilize these concepts are presented and analyzed as to their strengths and weaknesses. Objective tests and measurements for documentation and reimbursement are taught and practiced. This course facilitates integration of theory and current practice techniques and is a mixture of lecture, demonstration, discussion, videotape case presentations and hands-on practical work. At the end of this workshop therapists will have new skills, ideas and ample encouragement to apply these skills immediately to patient care.</p>\r\n",
  "created_at": ISODate("2018-11-20T06:21:02.615Z"),
  "updated_at": ISODate("2019-11-13T16:24:00.868Z")
});
db.getCollection("seminar_contents").insert({
  "_id": ObjectId("5bf3a7cebfe6552a98582b02"),
  "lang": "fr",
  "description": "<p>php seminar</p>\r\n",
  "created_at": ISODate("2018-11-20T06:21:02.622Z"),
  "updated_at": ISODate("2019-11-13T16:24:00.868Z")
});
db.getCollection("seminar_contents").insert({
  "_id": ObjectId("5c875f19bde8c9097ce08305"),
  "lang": "en",
  "description": "<p>This course has a little something for everyone. It is an excellent introductory course for the clinician wishing to gain exposure to several approaches before pursuing certification in any one area, for the clinician or clinic wishing to gain a better understanding of the evidence-base for (and against) the use of various treatment techniques, or for consensus-building in the clinic with several therapists treating from a wide range of seemingly conflicting treatment philosophies. Dr. Ball&rsquo;s course is a one-day combined lab and lecture guided-tour through several traditional and non-traditional physical therapy treatment techniques (including therapeutic handing, sensory processing, several pediatric manual therapy techniques, and Body-Weight Supported Gait Training) while concurrently reviewing relevant studies and gaining an awareness of the literature supporting or challenging use of each treatment technique. Dr. Ball contends that this foundation is an essential part of understanding the value of the LiteGait system as a treatment environment (as opposed to a treatment alternative) that when combined with the treatment philosophy of the evidence-based therapist&rsquo;s choice, further enhances therapeutic outcomes</p>\r\n",
  "created_at": ISODate("2019-03-12T07:26:17.676Z"),
  "updated_at": ISODate("2019-11-13T05:18:46.538Z")
});
db.getCollection("seminar_contents").insert({
  "_id": ObjectId("5c875f19bde8c9097ce08307"),
  "lang": "fr",
  "description": "",
  "created_at": ISODate("2019-03-12T07:26:17.682Z"),
  "updated_at": ISODate("2019-11-13T05:18:46.538Z")
});
db.getCollection("seminar_contents").insert({
  "_id": ObjectId("5c93ea14eabf3931ff289194"),
  "lang": "en",
  "description": "<p>This course is designed to enable physical and occupational therapists to devise systemic applications of ultrasound, electrical stimulation and kinesio taping for specific impairments of the movement system. Impairments discussed in the course will include inflammation and edema, spasticity and contractures, pain, and muscle weakness. Resolution of these musculoskeletal and neurological impairments will promote optimal participation in Body Weight Support training for functional task performance and mobility. Participants will learn to improve the identified impairments and accelerate achievement of functional rehabilitation goals. The course will be presented through powerpoint with handouts along with hands on learning</p>\r\n",
  "created_at": ISODate("2019-03-21T19:46:28.735Z"),
  "updated_at": ISODate("2019-11-13T05:18:29.30Z")
});
db.getCollection("seminar_contents").insert({
  "_id": ObjectId("5c93ea14eabf3931ff289196"),
  "lang": "fr",
  "description": "",
  "created_at": ISODate("2019-03-21T19:46:28.741Z"),
  "updated_at": ISODate("2019-11-13T05:18:29.30Z")
});
db.getCollection("seminar_contents").insert({
  "_id": ObjectId("5c93ea14eabf3931ff289198"),
  "lang": "Gr",
  "description": "",
  "created_at": ISODate("2019-03-21T19:46:28.744Z"),
  "updated_at": ISODate("2019-11-13T05:18:29.30Z")
});
db.getCollection("seminar_contents").insert({
  "_id": ObjectId("5c93ea14eabf3931ff28919a"),
  "lang": "Sp",
  "description": "",
  "created_at": ISODate("2019-03-21T19:46:28.747Z"),
  "updated_at": ISODate("2019-11-13T05:18:29.30Z")
});
db.getCollection("seminar_contents").insert({
  "_id": ObjectId("5d7280e3d4748747ea5758fa"),
  "lang": "en",
  "description": "<p><strong>PHP: Hypertext Preprocessor</strong>&nbsp;(or simply&nbsp;<strong>PHP</strong>) is a&nbsp;<a href=\"https://en.wikipedia.org/wiki/General-purpose_programming_language\">general-purpose programming language</a>&nbsp;originally designed for&nbsp;<a href=\"https://en.wikipedia.org/wiki/Web_development\">web development</a>. It was originally created by&nbsp;<a href=\"https://en.wikipedia.org/wiki/Rasmus_Lerdorf\">Rasmus Lerdorf</a>&nbsp;in 1994;<a href=\"https://en.wikipedia.org/wiki/PHP#cite_note-History_of_PHP-4\">[4]</a>&nbsp;the PHP&nbsp;<a href=\"https://en.wikipedia.org/wiki/Reference_implementation\">reference implementation</a>&nbsp;is now produced by The PHP Group.<a href=\"https://en.wikipedia.org/wiki/PHP#cite_note-about_PHP-5\">[5]</a>&nbsp;PHP originally stood for&nbsp;<em>Personal Home Page</em>,<a href=\"https://en.wikipedia.org/wiki/PHP#cite_note-History_of_PHP-4\">[4]</a>&nbsp;but it now stands for the&nbsp;<a href=\"https://en.wikipedia.org/wiki/Recursive_initialism\">recursive initialism</a>&nbsp;<em>PHP: Hypertext Preprocessor</em>.<a href=\"https://en.wikipedia.org/wiki/PHP#cite_note-6\">[6]</a></p>\r\n\r\n<p>PHP code may be executed with a&nbsp;<a href=\"https://en.wikipedia.org/wiki/Command-line_interface\">command line interface</a>&nbsp;(CLI), embedded into&nbsp;<a href=\"https://en.wikipedia.org/wiki/HTML\">HTML</a>&nbsp;code, or it can be used in combination with various&nbsp;<a href=\"https://en.wikipedia.org/wiki/Web_template_system\">web template systems</a>, web content management systems, and&nbsp;<a href=\"https://en.wikipedia.org/wiki/Web_framework\">web frameworks</a>. PHP code is usually processed by a PHP&nbsp;<a href=\"https://en.wikipedia.org/wiki/Interpreter_(computing)\">interpreter</a>&nbsp;implemented as a&nbsp;<a href=\"https://en.wikipedia.org/wiki/Plugin_(computing)\">module</a>&nbsp;in a web server or as a&nbsp;<a href=\"https://en.wikipedia.org/wiki/Common_Gateway_Interface\">Common Gateway Interface</a>&nbsp;(CGI) executable. The web server combines the results of the interpreted and executed PHP code, which may be any type of data, including images, with the generated web page. PHP can be used for many programming tasks outside of the web context, such as&nbsp;<a href=\"https://en.wikipedia.org/wiki/Computer_software\">standalone</a>&nbsp;<a href=\"https://en.wikipedia.org/wiki/Graphical_user_interface\">graphical applications</a><a href=\"https://en.wikipedia.org/wiki/PHP#cite_note-7\">[7]</a>&nbsp;and robotic&nbsp;<a href=\"https://en.wikipedia.org/wiki/Unmanned_aerial_vehicle\">drone</a>&nbsp;control<a href=\"https://en.wikipedia.org/wiki/PHP#cite_note-8\">[8]</a>.</p>\r\n\r\n<p>The standard PHP interpreter, powered by the&nbsp;<a href=\"https://en.wikipedia.org/wiki/Zend_Engine\">Zend Engine</a>, is&nbsp;<a href=\"https://en.wikipedia.org/wiki/Free_software\">free software</a>&nbsp;released under the&nbsp;<a href=\"https://en.wikipedia.org/wiki/PHP_License\">PHP License</a>. PHP has been widely ported and can be deployed on most web servers on almost every&nbsp;<a href=\"https://en.wikipedia.org/wiki/Operating_system\">operating system</a>&nbsp;and&nbsp;<a href=\"https://en.wikipedia.org/wiki/Computing_platform\">platform</a>, free of charge.<a href=\"https://en.wikipedia.org/wiki/PHP#cite_note-foundations-9\">[9]</a></p>\r\n\r\n<p>The PHP language evolved without a written&nbsp;<a href=\"https://en.wikipedia.org/wiki/Formal_specification\">formal specification</a>&nbsp;or standard until 2014, with the original implementation acting as the&nbsp;<em><a href=\"https://en.wikipedia.org/wiki/De_facto\">de facto</a></em>&nbsp;standard which other implementations aimed to follow. Since 2014 work has gone on to create a formal PHP specification.<a href=\"https://en.wikipedia.org/wiki/PHP#cite_note-10\">[10]</a></p>\r\n",
  "created_at": ISODate("2018-11-20T06:21:02.615Z"),
  "updated_at": ISODate("2019-11-13T05:17:22.959Z"),
  "seminar_id": ObjectId("5d7280e3d4748747ea5758f8")
});
db.getCollection("seminar_contents").insert({
  "_id": ObjectId("5d7280e3d4748747ea5758fd"),
  "lang": "fr",
  "description": "<p>php seminar</p>\r\n",
  "created_at": ISODate("2018-11-20T06:21:02.622Z"),
  "updated_at": ISODate("2019-11-13T05:17:22.959Z"),
  "seminar_id": ObjectId("5d7280e3d4748747ea5758f8")
});
db.getCollection("seminar_contents").insert({
  "_id": ObjectId("5dcb95726256f133e1771f20"),
  "seminar_id": ObjectId("5dcb95726256f133e1771f1f"),
  "lang": "en",
  "description": "<p>Perfect to get everyone started or re-acquainted with using LiteGait<sup>&reg;</sup>&nbsp;.&nbsp; Customized 90 minute LIVE online refresher training &ndash; designed to be interactive using lecture &amp; videos, focused on meeting your clinical needs.&nbsp; The package includes: 90 min presentation, certificate to use for one Clinical webinar (up to $35 value), access to all recordings of LiteGait<sup>&reg;</sup>&nbsp;Tips and Tricks, Journal Club, and Case Studies .&nbsp; *May qualify for 1 hr CE or .1ceu in some locations- ask for details.</p>\r\n",
  "created_at": ISODate("2019-11-13T05:32:34.230Z"),
  "updated_at": ISODate("2019-11-13T05:32:34.230Z")
});
db.getCollection("seminar_contents").insert({
  "_id": ObjectId("5dcb95726256f133e1771f22"),
  "seminar_id": ObjectId("5dcb95726256f133e1771f1f"),
  "lang": "fr",
  "description": "",
  "created_at": ISODate("2019-11-13T05:32:34.238Z"),
  "updated_at": ISODate("2019-11-13T05:32:34.238Z")
});
db.getCollection("seminar_contents").insert({
  "_id": ObjectId("5dcb95726256f133e1771f24"),
  "seminar_id": ObjectId("5dcb95726256f133e1771f1f"),
  "lang": "Gr",
  "description": "",
  "created_at": ISODate("2019-11-13T05:32:34.242Z"),
  "updated_at": ISODate("2019-11-13T05:32:34.242Z")
});
db.getCollection("seminar_contents").insert({
  "_id": ObjectId("5dcb95726256f133e1771f26"),
  "seminar_id": ObjectId("5dcb95726256f133e1771f1f"),
  "lang": "Sp",
  "description": "",
  "created_at": ISODate("2019-11-13T05:32:34.245Z"),
  "updated_at": ISODate("2019-11-13T05:32:34.245Z")
});

/** seminar_request records **/
db.getCollection("seminar_request").insert({
  "_id": ObjectId("5c94a7709b495718363c0b07"),
  "seminar_id": ObjectId("5c93ea14eabf3931ff289193"),
  "name": "RAJ KISHORE VERMA",
  "email": "verma.rajkishore@gmail.com",
  "phone": "9950192509",
  "address": "WARD NO 17,",
  "message": "test",
  "status": NumberInt(1),
  "created_at": ISODate("2019-03-22T09:14:25.1Z"),
  "updated_at": ISODate("2019-03-22T09:14:25.1Z")
});

/** seminars records **/
db.getCollection("seminars").insert({
  "_id": ObjectId("5bf3a7cebfe6552a98582aff"),
  "lang_content": [
    ObjectId("5bf3a7cebfe6552a98582b00"),
    ObjectId("5bf3a7cebfe6552a98582b02")
  ],
  "title": "Potion for Locomotion ",
  "slug": "potion-for-locomotion-",
  "speaker_id": [
    ObjectId("5c7cd37bfdbb9436784ca0fb")
  ],
  "description": "<p>This is a basic to intermediate course designed to present current concepts of weight bearing assisted gait training, neuroplasticity, motor learning, and principles of effective therapeutic exercise and teach how to apply them to patient treatment. Current therapeutic interventions that utilize these concepts are presented and analyzed as to their strengths and weaknesses. Objective tests and measurements for documentation and reimbursement are taught and practiced. This course facilitates integration of theory and current practice techniques and is a mixture of lecture, demonstration, discussion, videotape case presentations and hands-on practical work. At the end of this workshop therapists will have new skills, ideas and ample encouragement to apply these skills immediately to patient care.</p>\r\n",
  "price": NumberInt(75),
  "meta_title": "php seminar 1",
  "meta_description": "php seminar 1",
  "status": NumberInt(1),
  "created_at": ISODate("2018-11-20T06:21:02.611Z"),
  "updated_at": ISODate("2019-11-13T16:24:00.859Z"),
  "date_time": ISODate("2019-11-12T02:30:00.0Z"),
  "location": "Malviya Nagar\r\n aipur 123 frontage lane\r\n Chicago, IL",
  "subtitle": "Principles of Neuroplasticity, Motor Learning, and Gait Training",
  "image": "/seminar/seminar_05.jpg",
  "position": NumberInt(100),
  "testimonials": null
});
db.getCollection("seminars").insert({
  "_id": ObjectId("5c875f19bde8c9097ce08304"),
  "speaker_id": [
    ObjectId("5c7cd37bfdbb9436784ca0fb")
  ],
  "lang_content": [
    ObjectId("5c875f19bde8c9097ce08305"),
    ObjectId("5c875f19bde8c9097ce08307")
  ],
  "title": "Pediatric Physical Therapy Treatments: ",
  "slug": "pediatric-physical-therapy-treatments-",
  "description": "<p>This course has a little something for everyone. It is an excellent introductory course for the clinician wishing to gain exposure to several approaches before pursuing certification in any one area, for the clinician or clinic wishing to gain a better understanding of the evidence-base for (and against) the use of various treatment techniques, or for consensus-building in the clinic with several therapists treating from a wide range of seemingly conflicting treatment philosophies. Dr. Ball&rsquo;s course is a one-day combined lab and lecture guided-tour through several traditional and non-traditional physical therapy treatment techniques (including therapeutic handing, sensory processing, several pediatric manual therapy techniques, and Body-Weight Supported Gait Training) while concurrently reviewing relevant studies and gaining an awareness of the literature supporting or challenging use of each treatment technique. Dr. Ball contends that this foundation is an essential part of understanding the value of the LiteGait system as a treatment environment (as opposed to a treatment alternative) that when combined with the treatment philosophy of the evidence-based therapist&rsquo;s choice, further enhances therapeutic outcomes</p>\r\n",
  "date_time": null,
  "location": "",
  "price": NumberInt(700),
  "meta_title": "",
  "meta_description": "",
  "status": NumberInt(1),
  "created_at": ISODate("2019-03-12T07:26:17.669Z"),
  "updated_at": ISODate("2019-11-13T05:18:46.528Z"),
  "subtitle": "History, Evidence, and Outcomes",
  "testimonials": null,
  "image": "/sunflower-3792914_1920.jpg",
  "position": NumberInt(103)
});
db.getCollection("seminars").insert({
  "_id": ObjectId("5c93ea14eabf3931ff289193"),
  "speaker_id": [
    ObjectId("5c93e8f7eabf3931ff289184")
  ],
  "lang_content": [
    ObjectId("5c93ea14eabf3931ff289194"),
    ObjectId("5c93ea14eabf3931ff289196"),
    ObjectId("5c93ea14eabf3931ff289198"),
    ObjectId("5c93ea14eabf3931ff28919a")
  ],
  "title": "Optimizing Outcomes for Body Weight Supported Therapies ",
  "slug": "optimizing-outcomes-for-body-weight-supported-therapies-",
  "description": "<p>This course is designed to enable physical and occupational therapists to devise systemic applications of ultrasound, electrical stimulation and kinesio taping for specific impairments of the movement system. Impairments discussed in the course will include inflammation and edema, spasticity and contractures, pain, and muscle weakness. Resolution of these musculoskeletal and neurological impairments will promote optimal participation in Body Weight Support training for functional task performance and mobility. Participants will learn to improve the identified impairments and accelerate achievement of functional rehabilitation goals. The course will be presented through powerpoint with handouts along with hands on learning</p>\r\n",
  "date_time": null,
  "location": "",
  "price": NumberInt(350),
  "meta_title": "",
  "meta_description": "",
  "status": NumberInt(1),
  "created_at": ISODate("2019-03-21T19:46:28.730Z"),
  "updated_at": ISODate("2019-11-13T05:18:29.14Z"),
  "subtitle": "Theory, Evidence and Practical Application",
  "testimonials": null,
  "image": "/LG Greece Cyprus.jpg",
  "position": NumberInt(102)
});
db.getCollection("seminars").insert({
  "_id": ObjectId("5d7280e3d4748747ea5758f8"),
  "speaker_id": [
    ObjectId("5c7cd37bfdbb9436784ca0fb")
  ],
  "lang_content": [
    ObjectId("5d7280e3d4748747ea5758fa"),
    ObjectId("5d7280e3d4748747ea5758fd")
  ],
  "testimonials": [
    ObjectId("5bd16ea2b605151b7e93d894"),
    ObjectId("5c36edb38ba4672adbd9402e")
  ],
  "title": "php seminar1- copy",
  "slug": "php-seminar1- copy",
  "description": "<p><strong>PHP: Hypertext Preprocessor</strong>&nbsp;(or simply&nbsp;<strong>PHP</strong>) is a&nbsp;<a href=\"https://en.wikipedia.org/wiki/General-purpose_programming_language\">general-purpose programming language</a>&nbsp;originally designed for&nbsp;<a href=\"https://en.wikipedia.org/wiki/Web_development\">web development</a>. It was originally created by&nbsp;<a href=\"https://en.wikipedia.org/wiki/Rasmus_Lerdorf\">Rasmus Lerdorf</a>&nbsp;in 1994;<a href=\"https://en.wikipedia.org/wiki/PHP#cite_note-History_of_PHP-4\">[4]</a>&nbsp;the PHP&nbsp;<a href=\"https://en.wikipedia.org/wiki/Reference_implementation\">reference implementation</a>&nbsp;is now produced by The PHP Group.<a href=\"https://en.wikipedia.org/wiki/PHP#cite_note-about_PHP-5\">[5]</a>&nbsp;PHP originally stood for&nbsp;<em>Personal Home Page</em>,<a href=\"https://en.wikipedia.org/wiki/PHP#cite_note-History_of_PHP-4\">[4]</a>&nbsp;but it now stands for the&nbsp;<a href=\"https://en.wikipedia.org/wiki/Recursive_initialism\">recursive initialism</a>&nbsp;<em>PHP: Hypertext Preprocessor</em>.<a href=\"https://en.wikipedia.org/wiki/PHP#cite_note-6\">[6]</a></p>\r\n\r\n<p>PHP code may be executed with a&nbsp;<a href=\"https://en.wikipedia.org/wiki/Command-line_interface\">command line interface</a>&nbsp;(CLI), embedded into&nbsp;<a href=\"https://en.wikipedia.org/wiki/HTML\">HTML</a>&nbsp;code, or it can be used in combination with various&nbsp;<a href=\"https://en.wikipedia.org/wiki/Web_template_system\">web template systems</a>, web content management systems, and&nbsp;<a href=\"https://en.wikipedia.org/wiki/Web_framework\">web frameworks</a>. PHP code is usually processed by a PHP&nbsp;<a href=\"https://en.wikipedia.org/wiki/Interpreter_(computing)\">interpreter</a>&nbsp;implemented as a&nbsp;<a href=\"https://en.wikipedia.org/wiki/Plugin_(computing)\">module</a>&nbsp;in a web server or as a&nbsp;<a href=\"https://en.wikipedia.org/wiki/Common_Gateway_Interface\">Common Gateway Interface</a>&nbsp;(CGI) executable. The web server combines the results of the interpreted and executed PHP code, which may be any type of data, including images, with the generated web page. PHP can be used for many programming tasks outside of the web context, such as&nbsp;<a href=\"https://en.wikipedia.org/wiki/Computer_software\">standalone</a>&nbsp;<a href=\"https://en.wikipedia.org/wiki/Graphical_user_interface\">graphical applications</a><a href=\"https://en.wikipedia.org/wiki/PHP#cite_note-7\">[7]</a>&nbsp;and robotic&nbsp;<a href=\"https://en.wikipedia.org/wiki/Unmanned_aerial_vehicle\">drone</a>&nbsp;control<a href=\"https://en.wikipedia.org/wiki/PHP#cite_note-8\">[8]</a>.</p>\r\n\r\n<p>The standard PHP interpreter, powered by the&nbsp;<a href=\"https://en.wikipedia.org/wiki/Zend_Engine\">Zend Engine</a>, is&nbsp;<a href=\"https://en.wikipedia.org/wiki/Free_software\">free software</a>&nbsp;released under the&nbsp;<a href=\"https://en.wikipedia.org/wiki/PHP_License\">PHP License</a>. PHP has been widely ported and can be deployed on most web servers on almost every&nbsp;<a href=\"https://en.wikipedia.org/wiki/Operating_system\">operating system</a>&nbsp;and&nbsp;<a href=\"https://en.wikipedia.org/wiki/Computing_platform\">platform</a>, free of charge.<a href=\"https://en.wikipedia.org/wiki/PHP#cite_note-foundations-9\">[9]</a></p>\r\n\r\n<p>The PHP language evolved without a written&nbsp;<a href=\"https://en.wikipedia.org/wiki/Formal_specification\">formal specification</a>&nbsp;or standard until 2014, with the original implementation acting as the&nbsp;<em><a href=\"https://en.wikipedia.org/wiki/De_facto\">de facto</a></em>&nbsp;standard which other implementations aimed to follow. Since 2014 work has gone on to create a formal PHP specification.<a href=\"https://en.wikipedia.org/wiki/PHP#cite_note-10\">[10]</a></p>\r\n",
  "price": NumberInt(700),
  "meta_title": "php seminar 1",
  "meta_description": "php seminar 1",
  "status": NumberInt(1),
  "created_at": ISODate("2018-11-20T06:21:02.611Z"),
  "updated_at": ISODate("2019-11-13T05:17:22.955Z"),
  "date_time": null,
  "location": "",
  "subtitle": "php seminar",
  "image": "/seminar/seminar_08-2H.jpg",
  "position": NumberInt(101)
});
db.getCollection("seminars").insert({
  "_id": ObjectId("5dcb95726256f133e1771f1f"),
  "speaker_id": [
    ObjectId("5c93e8f7eabf3931ff289184")
  ],
  "lang_content": [
    ObjectId("5dcb95726256f133e1771f20"),
    ObjectId("5dcb95726256f133e1771f22"),
    ObjectId("5dcb95726256f133e1771f24"),
    ObjectId("5dcb95726256f133e1771f26")
  ],
  "title": "OL1: Online Refresher Training",
  "subtitle": "Total Hours 1.5 | Online 1.5 | Onsite 0 | Ceus ",
  "slug": "ol1-online-refresher-training",
  "image": "/seminar/seminar_02.jpg",
  "description": "<p>Perfect to get everyone started or re-acquainted with using LiteGait<sup>&reg;</sup>&nbsp;.&nbsp; Customized 90 minute LIVE online refresher training &ndash; designed to be interactive using lecture &amp; videos, focused on meeting your clinical needs.&nbsp; The package includes: 90 min presentation, certificate to use for one Clinical webinar (up to $35 value), access to all recordings of LiteGait<sup>&reg;</sup>&nbsp;Tips and Tricks, Journal Club, and Case Studies .&nbsp; *May qualify for 1 hr CE or .1ceu in some locations- ask for details.</p>\r\n",
  "date_time": null,
  "location": "",
  "price": NumberInt(100),
  "position": NumberInt(10),
  "meta_title": "",
  "meta_description": "",
  "status": NumberInt(1),
  "created_at": ISODate("2019-11-13T05:32:34.206Z"),
  "updated_at": ISODate("2019-11-13T05:32:34.247Z")
});

/** services records **/
db.getCollection("services").insert({
  "_id": ObjectId("5d79c17df3c6d306896b27fe"),
  "part_id": ObjectId("5c668a8fbc19ad187c839c88"),
  "type": "part",
  "name": "Monica",
  "email": "monic@gmail.com",
  "phone": "9887654321",
  "use": "home_use",
  "address": "My Home",
  "comments": "tets comment",
  "status": NumberInt(1),
  "created_at": ISODate("2019-09-12T03:54:37.445Z"),
  "updated_at": ISODate("2019-09-12T03:54:37.445Z")
});
db.getCollection("services").insert({
  "_id": ObjectId("5d9e25e97dbdaa69d72e5ee7"),
  "product_id": ObjectId("5c24667cd4568d68ad108584"),
  "type": "product",
  "name": "Amir",
  "email": "amir@litegait.com",
  "phone": "4808291727",
  "use": "facility_use",
  "address": "gfgf",
  "comments": "need your help",
  "status": NumberInt(1),
  "created_at": ISODate("2019-10-09T18:24:41.300Z"),
  "updated_at": ISODate("2019-10-09T18:24:41.300Z")
});

/** speaker_contents records **/
db.getCollection("speaker_contents").insert({
  "_id": ObjectId("5c2f06ca31fdef2dc05a51e9"),
  "speaker_id": ObjectId("5c2f06ca31fdef2dc05a51e8"),
  "lang": "en",
  "bio": "<p>Lorem Ipsum</p>\r\n",
  "created_at": ISODate("2019-01-04T07:10:02.996Z"),
  "updated_at": ISODate("2019-08-30T18:58:30.251Z")
});
db.getCollection("speaker_contents").insert({
  "_id": ObjectId("5c2f06cb31fdef2dc05a51eb"),
  "speaker_id": ObjectId("5c2f06ca31fdef2dc05a51e8"),
  "lang": "fr",
  "bio": "",
  "created_at": ISODate("2019-01-04T07:10:03.3Z"),
  "updated_at": ISODate("2019-08-30T18:58:30.251Z")
});
db.getCollection("speaker_contents").insert({
  "_id": ObjectId("5c7cd37bfdbb9436784ca0fc"),
  "speaker_id": ObjectId("5c7cd37bfdbb9436784ca0fb"),
  "lang": "en",
  "bio": "<p>Karen Gage Bensley, PT, DPT, MS, PCS has been a PT for 30 years, is a board certified (APTA) clinical specialist in pediatrics, and has both an advanced Masters and a clinical doctorate with a focus in pediatrics. She currently works at a nonprofit community hospital on a pedi rehab team, seeing folks from birth to age 30 with neuro developmental disabilities. She is also an adjunct faculty at two universities, University of New England (doctoral program) and University of New Hampshire (grad program in LEND), and enjoys teaching.</p>\r\n",
  "created_at": ISODate("2019-03-04T07:27:55.227Z"),
  "updated_at": ISODate("2019-08-30T19:00:23.196Z")
});
db.getCollection("speaker_contents").insert({
  "_id": ObjectId("5c7cd37bfdbb9436784ca0fe"),
  "speaker_id": ObjectId("5c7cd37bfdbb9436784ca0fb"),
  "lang": "fr",
  "bio": "",
  "created_at": ISODate("2019-03-04T07:27:55.234Z"),
  "updated_at": ISODate("2019-08-30T19:00:23.196Z")
});
db.getCollection("speaker_contents").insert({
  "_id": ObjectId("5c93e8f7eabf3931ff289185"),
  "speaker_id": ObjectId("5c93e8f7eabf3931ff289184"),
  "lang": "en",
  "bio": "<p>Keith Khoo has been a clinician since 1985 after graduating from the University of Oklahoma.&nbsp; He has held a variety of offices within the Oklahoma Physical Therapy Association for 10 years.&nbsp; He was recognized with the Chapter Founder&rsquo;s Award in 1996.&nbsp;&nbsp; His practice has been varied from regional director of operations to being a treating therapist in hospitals, has evaluated and treated geriatics to pediatrics, and practiced in varied environments, from long term care and outpatient sports medicine, industrial medicine departments to presently at the Oklahoma Neurological Center of Excellence where he evaluates and subsequently treats primarily movement disorder patients like Parkinsons and Multiple Sclerosis.&nbsp; He also treats neuropathic patients,&nbsp; balance as well as vestibular disorders, provides pain management, and neuromuscular re-education.&nbsp; Keith has taught courses not only on balance and vestibular rehabilitation, but also on skincare, debridement certification, and incontinence management.&nbsp; He has also provided continuing education courses on modality applications (electrical stimulation, ultrasound, laser and light) for varied neuromusculoskeletal dysfunctions both here in the United States and abroad.&nbsp; He is a clinical specialist with Richmar.&nbsp; He is an instructor for LiteGait unweighting dynamic suspension system to use with Parkinsons, Multiple Sclerosis and Neuropathic patients to correct gait deviations and initiate gait in early stages of rehabilitation</p>\r\n",
  "created_at": ISODate("2019-03-21T19:41:43.409Z"),
  "updated_at": ISODate("2019-08-30T23:10:45.300Z")
});
db.getCollection("speaker_contents").insert({
  "_id": ObjectId("5c93e8f7eabf3931ff289187"),
  "speaker_id": ObjectId("5c93e8f7eabf3931ff289184"),
  "lang": "fr",
  "bio": "",
  "created_at": ISODate("2019-03-21T19:41:43.412Z"),
  "updated_at": ISODate("2019-08-30T23:10:45.300Z")
});
db.getCollection("speaker_contents").insert({
  "_id": ObjectId("5c93e8f7eabf3931ff289189"),
  "speaker_id": ObjectId("5c93e8f7eabf3931ff289184"),
  "lang": "Gr",
  "bio": "",
  "created_at": ISODate("2019-03-21T19:41:43.414Z"),
  "updated_at": ISODate("2019-08-30T23:10:45.300Z")
});
db.getCollection("speaker_contents").insert({
  "_id": ObjectId("5c93e8f7eabf3931ff28918b"),
  "speaker_id": ObjectId("5c93e8f7eabf3931ff289184"),
  "lang": "Sp",
  "bio": "",
  "created_at": ISODate("2019-03-21T19:41:43.415Z"),
  "updated_at": ISODate("2019-08-30T23:10:45.301Z")
});
db.getCollection("speaker_contents").insert({
  "_id": ObjectId("5cd4bb0906e67e44fa0eb3af"),
  "speaker_id": ObjectId("5cd4bb0906e67e44fa0eb3ae"),
  "lang": "en",
  "bio": "<p>Nechama Karman, PT, MS, PCS, is a board certified pediatric physical therapist in private practice in Great Neck, NY.&nbsp; She has extensive experience in pediatric and adult rehabilitation settings, including inpatient, outpatient, acute and long-term rehab as well as home-based environments. She has been a LiteGait clinical trainer since 2008, and since 2010 has been responsible for ongoing clinical education programs for Mobility Research &ndash; including the facilitation of the online Journal Club, Case Studies &amp; other clinical webinars.&nbsp; She has used LiteGait extensively across populations for both gait training and advanced skills training, primarily in individuals with acquired brain injuries and other neurological deficits. She was formerly on faculty at the School of Health Professions, Behavioral and Life Sciences of New York Institute of Technology, in Old Westbury, NY and School of Health Sciences, Hunter College of the City University of New York.</p>\r\n",
  "created_at": ISODate("2019-05-09T23:43:05.616Z"),
  "updated_at": ISODate("2019-09-06T14:57:21.73Z")
});
db.getCollection("speaker_contents").insert({
  "_id": ObjectId("5cd4bb0906e67e44fa0eb3b1"),
  "speaker_id": ObjectId("5cd4bb0906e67e44fa0eb3ae"),
  "lang": "fr",
  "bio": "",
  "created_at": ISODate("2019-05-09T23:43:05.626Z"),
  "updated_at": ISODate("2019-09-06T14:57:21.73Z")
});
db.getCollection("speaker_contents").insert({
  "_id": ObjectId("5cd4bb0906e67e44fa0eb3b3"),
  "speaker_id": ObjectId("5cd4bb0906e67e44fa0eb3ae"),
  "lang": "Gr",
  "bio": "",
  "created_at": ISODate("2019-05-09T23:43:05.633Z"),
  "updated_at": ISODate("2019-09-06T14:57:21.73Z")
});
db.getCollection("speaker_contents").insert({
  "_id": ObjectId("5cd4bb0906e67e44fa0eb3b5"),
  "speaker_id": ObjectId("5cd4bb0906e67e44fa0eb3ae"),
  "lang": "Sp",
  "bio": "",
  "created_at": ISODate("2019-05-09T23:43:05.639Z"),
  "updated_at": ISODate("2019-09-06T14:57:21.73Z")
});
db.getCollection("speaker_contents").insert({
  "_id": ObjectId("5d696ef1e401c22327431158"),
  "speaker_id": ObjectId("5d696ef1e401c22327431157"),
  "lang": "en",
  "bio": "<p>Dr. Andrea Batt has been passionate about&nbsp;the physical therapy profession since 1980. She initially became an LPTA in 1980, earned her BSPT in 1986 and her DPT in 2011. Over the years pediatric therapy became&nbsp; a special interest, with an emphasis on individuals with long term developmental disabilities across the age span and she has&nbsp;been a board certified pediatric physical therapist since 2000.<br />\r\nCradle to Rocker &nbsp;provides PT services for all family members - birth to adult. She has strong hands on skills, but uses outcomes based/evidence based practice.<br />\r\nDr. Batt has provided outpatient PT services to individuals of all ages with acute problems, short and long-term disabilities.&nbsp; She enjoys teaching and training&nbsp; patients, families, physicians, therapists and others. She has provided home based services, worked in day habilitation centers, school systems,group homes, inpatient and outpatient rehabilitation, skilled nursing facilities and a long time ago in acute care.&nbsp; She has successfully treated people of all ages with&nbsp;a wide variety of issues from low back pain to post operative rehabilitation.&nbsp; All of her sessions are individual one:one intervention.br /&gt; Dr. Batt recognizes the critical importance of collaborating with all therapy disciplines, educators, caregivers, DME providers, orthotists and medical specialists to ensure the best outcomes for her clients of any age.</p>\r\n",
  "created_at": ISODate("2019-08-30T18:46:09.185Z"),
  "updated_at": ISODate("2019-10-03T01:07:36.283Z")
});
db.getCollection("speaker_contents").insert({
  "_id": ObjectId("5d696ef1e401c2232743115a"),
  "speaker_id": ObjectId("5d696ef1e401c22327431157"),
  "lang": "fr",
  "bio": "",
  "created_at": ISODate("2019-08-30T18:46:09.191Z"),
  "updated_at": ISODate("2019-10-03T01:07:36.283Z")
});
db.getCollection("speaker_contents").insert({
  "_id": ObjectId("5d696ef1e401c2232743115c"),
  "speaker_id": ObjectId("5d696ef1e401c22327431157"),
  "lang": "Gr",
  "bio": "",
  "created_at": ISODate("2019-08-30T18:46:09.193Z"),
  "updated_at": ISODate("2019-10-03T01:07:36.284Z")
});
db.getCollection("speaker_contents").insert({
  "_id": ObjectId("5d696ef1e401c2232743115e"),
  "speaker_id": ObjectId("5d696ef1e401c22327431157"),
  "lang": "Sp",
  "bio": "",
  "created_at": ISODate("2019-08-30T18:46:09.195Z"),
  "updated_at": ISODate("2019-10-03T01:07:36.284Z")
});
db.getCollection("speaker_contents").insert({
  "_id": ObjectId("5d696ff9e401c2232743116b"),
  "speaker_id": ObjectId("5d696ff9e401c2232743116a"),
  "lang": "en",
  "bio": "<p>Yocheved Bensinger-Brody PT, PhD, PCS is a board certified pediatric physical therapist. She is an Assistant Professor in Touro College&rsquo;s Doctor of Physical Therapy program in NYC, and she is the clinician owner of a boutique private pediatric practice in Northern NJ. Yocheved graduated with a BS in Physical Therapy from Florida International University, an MA in Movement Sciences from Teachers College, Columbia University, and a PhD in Psychology from the Graduate Center at CUNY. Yocheved has extensive experience working with individuals diagnosed with neurodevelopmental disorders, and she has specific expertise evaluating neonates and infants. Yocheved&rsquo;s research interests include the investigation of early trajectories of neurodevelopmental disorders, and the relationship between cognitive and motor behaviors.&nbsp;</p>\r\n",
  "created_at": ISODate("2019-08-30T18:50:33.595Z"),
  "updated_at": ISODate("2019-08-30T18:50:33.595Z")
});
db.getCollection("speaker_contents").insert({
  "_id": ObjectId("5d696ff9e401c2232743116d"),
  "speaker_id": ObjectId("5d696ff9e401c2232743116a"),
  "lang": "fr",
  "bio": "",
  "created_at": ISODate("2019-08-30T18:50:33.599Z"),
  "updated_at": ISODate("2019-08-30T18:50:33.599Z")
});
db.getCollection("speaker_contents").insert({
  "_id": ObjectId("5d696ff9e401c2232743116f"),
  "speaker_id": ObjectId("5d696ff9e401c2232743116a"),
  "lang": "Gr",
  "bio": "",
  "created_at": ISODate("2019-08-30T18:50:33.601Z"),
  "updated_at": ISODate("2019-08-30T18:50:33.601Z")
});
db.getCollection("speaker_contents").insert({
  "_id": ObjectId("5d696ff9e401c22327431171"),
  "speaker_id": ObjectId("5d696ff9e401c2232743116a"),
  "lang": "Sp",
  "bio": "",
  "created_at": ISODate("2019-08-30T18:50:33.603Z"),
  "updated_at": ISODate("2019-08-30T18:50:33.603Z")
});
db.getCollection("speaker_contents").insert({
  "_id": ObjectId("5d697077e401c22327431179"),
  "speaker_id": ObjectId("5d697077e401c22327431178"),
  "lang": "en",
  "bio": "<p>Amanda Hall, PT, MPT, PCS, ATP received her Master of Physical Therapy from the University of Washington in 2001. She was certified as a Pediatric Clinical Specialist through the ABPTS in 2011, and as an Assistive Technology Professional (ATP) through the Rehabilitation Engineering and Assistive Technology Society of North America in 2017. She has built her knowledge base in orthotics, casting, and splinting over 15+ years of clinical practice, which has included a wide range of pediatric and neurologic settings, including outpatient, NICU, inpatient rehabilitation, complex care, and orthotic/equipment clinic. She developed a framework for therapeutic casting based on evidence-informed practice with a focus on differential diagnosis, manual therapy, developmental kinesiopathology, and biomechanical alignment of casts, with specific use of casting materials. Her framework has a strong focus in patient-centered treatment and adaptive design. As a result, she has received mainstream media attention as the &ldquo;Madcaster&rdquo;, including features in MTV, UsWeekly, NBC, and ABC. At the Orthotics, Prosthetics, and Equipment Department of the HSC Pediatric Center in Washington, D.C., she provides serial casting for upper and lower extremities as well as orthotic, equipment, and assistive technology evaluations. Amanda teaches physical and occupational therapists casting techniques, encouraging individualized treatment and a developmental kinesiopathological approach. At CSM 2019, Amanda presented Therapeutic Casting: A Modern Clinical Pathway to Improve Outcomes as a pre-conference course for the Academy of Pediatric Physical Therapy.</p>\r\n",
  "created_at": ISODate("2019-08-30T18:52:39.784Z"),
  "updated_at": ISODate("2019-08-30T18:52:39.784Z")
});
db.getCollection("speaker_contents").insert({
  "_id": ObjectId("5d697077e401c2232743117b"),
  "speaker_id": ObjectId("5d697077e401c22327431178"),
  "lang": "fr",
  "bio": "",
  "created_at": ISODate("2019-08-30T18:52:39.787Z"),
  "updated_at": ISODate("2019-08-30T18:52:39.787Z")
});
db.getCollection("speaker_contents").insert({
  "_id": ObjectId("5d697077e401c2232743117d"),
  "speaker_id": ObjectId("5d697077e401c22327431178"),
  "lang": "Gr",
  "bio": "",
  "created_at": ISODate("2019-08-30T18:52:39.789Z"),
  "updated_at": ISODate("2019-08-30T18:52:39.789Z")
});
db.getCollection("speaker_contents").insert({
  "_id": ObjectId("5d697077e401c2232743117f"),
  "speaker_id": ObjectId("5d697077e401c22327431178"),
  "lang": "Sp",
  "bio": "",
  "created_at": ISODate("2019-08-30T18:52:39.793Z"),
  "updated_at": ISODate("2019-08-30T18:52:39.793Z")
});
db.getCollection("speaker_contents").insert({
  "_id": ObjectId("5d69715fe401c2232743118a"),
  "speaker_id": ObjectId("5d69715fe401c22327431189"),
  "lang": "en",
  "bio": "<p>Susan Hastings, PT, DPT, PCS is a Board-Certified Pediatric Certified Specialist, who graduated from Rocky Mountain University of Health Professions (RMUoHP) with a tDPT, specializing in Pediatrics in 2010. Upon graduation, she opened her own practice, utilizing mainly FES and WBV in her treatment. She has used functional electrical stimulation (FES) in children with upper motor neuron lesions for the past 20 years, developing new applications of stimulation for treatment in children with CP, with the focus on motor learning. She has presented her findings at Combined Sections Meeting 2016 and at APTA SoPAC 2016, where she co-taught a pre-conference course with Beverly Cusick. Susan was invited to present at the 2017 California APTA Annual Conference, as well as 2016 New Mexico APTA Annual Conference, and has given courses in numerous other cities, including Rio de Janeiro. Most recently she was asked to present her findings at AACPDM 2018, where she gave a short presentation to the NMES -Gait-Assisted Group. Currently, she is collaborating with Dr. V R Edgerton and Dr. P Gad from UCLA, in order to help children with CP through novel FES applications. Previous to opening her practice, she was Sr. Therapist for Santa Clara County California Children Services (CCS) for a staff of 45 therapists. During that time she was asked to assist in courses taught by notable pediatric therapists, including Beverly Cusick (biomechanics and casting), Judy Carmick (FES), Mary Massery (function and breathing), Lois Bly (NDT baby treatment), and Lauren Beeler (NDT).</p>\r\n",
  "created_at": ISODate("2019-08-30T18:56:31.39Z"),
  "updated_at": ISODate("2019-08-30T18:56:31.39Z")
});
db.getCollection("speaker_contents").insert({
  "_id": ObjectId("5d69715fe401c2232743118c"),
  "speaker_id": ObjectId("5d69715fe401c22327431189"),
  "lang": "fr",
  "bio": "",
  "created_at": ISODate("2019-08-30T18:56:31.42Z"),
  "updated_at": ISODate("2019-08-30T18:56:31.42Z")
});
db.getCollection("speaker_contents").insert({
  "_id": ObjectId("5d69715fe401c2232743118e"),
  "speaker_id": ObjectId("5d69715fe401c22327431189"),
  "lang": "Gr",
  "bio": "",
  "created_at": ISODate("2019-08-30T18:56:31.44Z"),
  "updated_at": ISODate("2019-08-30T18:56:31.44Z")
});
db.getCollection("speaker_contents").insert({
  "_id": ObjectId("5d69715fe401c22327431190"),
  "speaker_id": ObjectId("5d69715fe401c22327431189"),
  "lang": "Sp",
  "bio": "",
  "created_at": ISODate("2019-08-30T18:56:31.46Z"),
  "updated_at": ISODate("2019-08-30T18:56:31.46Z")
});

/** speakers records **/
db.getCollection("speakers").insert({
  "_id": ObjectId("5c2f06ca31fdef2dc05a51e8"),
  "lang_content": [
    ObjectId("5c2f06ca31fdef2dc05a51e9"),
    ObjectId("5c2f06cb31fdef2dc05a51eb")
  ],
  "name": "Omega Apple",
  "slug": "omega",
  "image": "/TrainerSpeaker/batt_andrea_circle.png",
  "bio": "<p>Lorem Ipsum</p>\r\n",
  "category": "both",
  "trainers": "not_talk",
  "instructors": "high_end_professor",
  "webinars": "speak_person",
  "meta_title": "introduction",
  "meta_description": "introduction",
  "status": NumberInt(1),
  "created_at": ISODate("2019-01-04T07:10:02.989Z"),
  "updated_at": ISODate("2019-08-30T18:58:30.246Z"),
  "tag_line": "PT, DPT, MS, PCS",
  "testimonials": null,
  "fname": "Omega",
  "lname": "Apple"
});
db.getCollection("speakers").insert({
  "_id": ObjectId("5c7cd37bfdbb9436784ca0fb"),
  "lang_content": [
    ObjectId("5c7cd37bfdbb9436784ca0fc"),
    ObjectId("5c7cd37bfdbb9436784ca0fe")
  ],
  "name": "Karen Gage Bensley",
  "slug": "karen-gage-bensley",
  "tag_line": "PT, DPT, MS, PCS",
  "image": "/TrainerSpeaker/yocheved_circle.png",
  "bio": "<p>Karen Gage Bensley, PT, DPT, MS, PCS has been a PT for 30 years, is a board certified (APTA) clinical specialist in pediatrics, and has both an advanced Masters and a clinical doctorate with a focus in pediatrics. She currently works at a nonprofit community hospital on a pedi rehab team, seeing folks from birth to age 30 with neuro developmental disabilities. She is also an adjunct faculty at two universities, University of New England (doctoral program) and University of New Hampshire (grad program in LEND), and enjoys teaching.</p>\r\n",
  "category": "instructor",
  "trainers": "lecture",
  "instructors": "high_end_professor",
  "webinars": "profession",
  "meta_title": "",
  "meta_description": "",
  "status": NumberInt(1),
  "created_at": ISODate("2019-03-04T07:27:55.170Z"),
  "updated_at": ISODate("2019-08-30T19:00:23.190Z"),
  "fname": "Karen Gage",
  "lname": "Bensley",
  "testimonials": [
    ObjectId("5bd16ea2b605151b7e93d894")
  ]
});
db.getCollection("speakers").insert({
  "_id": ObjectId("5c93e8f7eabf3931ff289184"),
  "lang_content": [
    ObjectId("5c93e8f7eabf3931ff289185"),
    ObjectId("5c93e8f7eabf3931ff289187"),
    ObjectId("5c93e8f7eabf3931ff289189"),
    ObjectId("5c93e8f7eabf3931ff28918b")
  ],
  "name": "Keith Khoo",
  "slug": "keith-khoo",
  "tag_line": "PT",
  "image": "/TrainerSpeaker/karman_nechama_circle.png",
  "bio": "<p>Keith Khoo has been a clinician since 1985 after graduating from the University of Oklahoma.&nbsp; He has held a variety of offices within the Oklahoma Physical Therapy Association for 10 years.&nbsp; He was recognized with the Chapter Founder&rsquo;s Award in 1996.&nbsp;&nbsp; His practice has been varied from regional director of operations to being a treating therapist in hospitals, has evaluated and treated geriatics to pediatrics, and practiced in varied environments, from long term care and outpatient sports medicine, industrial medicine departments to presently at the Oklahoma Neurological Center of Excellence where he evaluates and subsequently treats primarily movement disorder patients like Parkinsons and Multiple Sclerosis.&nbsp; He also treats neuropathic patients,&nbsp; balance as well as vestibular disorders, provides pain management, and neuromuscular re-education.&nbsp; Keith has taught courses not only on balance and vestibular rehabilitation, but also on skincare, debridement certification, and incontinence management.&nbsp; He has also provided continuing education courses on modality applications (electrical stimulation, ultrasound, laser and light) for varied neuromusculoskeletal dysfunctions both here in the United States and abroad.&nbsp; He is a clinical specialist with Richmar.&nbsp; He is an instructor for LiteGait unweighting dynamic suspension system to use with Parkinsons, Multiple Sclerosis and Neuropathic patients to correct gait deviations and initiate gait in early stages of rehabilitation</p>\r\n",
  "category": "both",
  "trainers": "lecture",
  "instructors": "high_end_professor",
  "webinars": "profession",
  "meta_title": "",
  "meta_description": "",
  "status": NumberInt(1),
  "created_at": ISODate("2019-03-21T19:41:43.407Z"),
  "updated_at": ISODate("2019-08-30T23:10:45.296Z"),
  "fname": "Keith",
  "lname": "Khoo",
  "testimonials": null
});
db.getCollection("speakers").insert({
  "_id": ObjectId("5cd4bb0906e67e44fa0eb3ae"),
  "lang_content": [
    ObjectId("5cd4bb0906e67e44fa0eb3af"),
    ObjectId("5cd4bb0906e67e44fa0eb3b1"),
    ObjectId("5cd4bb0906e67e44fa0eb3b3"),
    ObjectId("5cd4bb0906e67e44fa0eb3b5")
  ],
  "name": "Nechama Karman",
  "slug": "nechama-karman",
  "tag_line": "PT, MS, PCS",
  "image": "/TrainerSpeaker/karman_nechama.jpg",
  "bio": "<p>Nechama Karman, PT, MS, PCS, is a board certified pediatric physical therapist in private practice in Great Neck, NY.&nbsp; She has extensive experience in pediatric and adult rehabilitation settings, including inpatient, outpatient, acute and long-term rehab as well as home-based environments. She has been a LiteGait clinical trainer since 2008, and since 2010 has been responsible for ongoing clinical education programs for Mobility Research &ndash; including the facilitation of the online Journal Club, Case Studies &amp; other clinical webinars.&nbsp; She has used LiteGait extensively across populations for both gait training and advanced skills training, primarily in individuals with acquired brain injuries and other neurological deficits. She was formerly on faculty at the School of Health Professions, Behavioral and Life Sciences of New York Institute of Technology, in Old Westbury, NY and School of Health Sciences, Hunter College of the City University of New York.</p>\r\n",
  "category": "both",
  "meta_title": "",
  "meta_description": "",
  "status": NumberInt(1),
  "created_at": ISODate("2019-05-09T23:43:05.520Z"),
  "updated_at": ISODate("2019-09-06T14:57:21.69Z"),
  "fname": "Nechama",
  "lname": "Karman",
  "testimonials": [
    ObjectId("5c36edb38ba4672adbd9402e")
  ]
});
db.getCollection("speakers").insert({
  "_id": ObjectId("5d696ef1e401c22327431157"),
  "lang_content": [
    ObjectId("5d696ef1e401c22327431158"),
    ObjectId("5d696ef1e401c2232743115a"),
    ObjectId("5d696ef1e401c2232743115c"),
    ObjectId("5d696ef1e401c2232743115e")
  ],
  "fname": "Andrea",
  "lname": "Batt ",
  "name": "Andrea Batt ",
  "slug": "andrea-batt-",
  "tag_line": "PT, DPT ",
  "image": "/TrainerSpeaker/batt_circle.jpg",
  "bio": "<p>Dr. Andrea Batt has been passionate about&nbsp;the physical therapy profession since 1980. She initially became an LPTA in 1980, earned her BSPT in 1986 and her DPT in 2011. Over the years pediatric therapy became&nbsp; a special interest, with an emphasis on individuals with long term developmental disabilities across the age span and she has&nbsp;been a board certified pediatric physical therapist since 2000.<br />\r\nCradle to Rocker &nbsp;provides PT services for all family members - birth to adult. She has strong hands on skills, but uses outcomes based/evidence based practice.<br />\r\nDr. Batt has provided outpatient PT services to individuals of all ages with acute problems, short and long-term disabilities.&nbsp; She enjoys teaching and training&nbsp; patients, families, physicians, therapists and others. She has provided home based services, worked in day habilitation centers, school systems,group homes, inpatient and outpatient rehabilitation, skilled nursing facilities and a long time ago in acute care.&nbsp; She has successfully treated people of all ages with&nbsp;a wide variety of issues from low back pain to post operative rehabilitation.&nbsp; All of her sessions are individual one:one intervention.br /&gt; Dr. Batt recognizes the critical importance of collaborating with all therapy disciplines, educators, caregivers, DME providers, orthotists and medical specialists to ensure the best outcomes for her clients of any age.</p>\r\n",
  "category": "trainer",
  "meta_title": "",
  "meta_description": "",
  "status": NumberInt(1),
  "created_at": ISODate("2019-08-30T18:46:09.182Z"),
  "updated_at": ISODate("2019-10-03T01:07:36.279Z"),
  "testimonials": [
    ObjectId("5bd16ea2b605151b7e93d894")
  ]
});
db.getCollection("speakers").insert({
  "_id": ObjectId("5d696ff9e401c2232743116a"),
  "lang_content": [
    ObjectId("5d696ff9e401c2232743116b"),
    ObjectId("5d696ff9e401c2232743116d"),
    ObjectId("5d696ff9e401c2232743116f"),
    ObjectId("5d696ff9e401c22327431171")
  ],
  "fname": "Yocheved",
  "lname": "Bensinger-Brody",
  "name": "Yocheved Bensinger-Brody",
  "slug": "yocheved-bensinger-brody",
  "tag_line": "PT, PhD, PCS ",
  "image": "/TrainerSpeaker/yocheved.jpg",
  "bio": "<p>Yocheved Bensinger-Brody PT, PhD, PCS is a board certified pediatric physical therapist. She is an Assistant Professor in Touro College&rsquo;s Doctor of Physical Therapy program in NYC, and she is the clinician owner of a boutique private pediatric practice in Northern NJ. Yocheved graduated with a BS in Physical Therapy from Florida International University, an MA in Movement Sciences from Teachers College, Columbia University, and a PhD in Psychology from the Graduate Center at CUNY. Yocheved has extensive experience working with individuals diagnosed with neurodevelopmental disorders, and she has specific expertise evaluating neonates and infants. Yocheved&rsquo;s research interests include the investigation of early trajectories of neurodevelopmental disorders, and the relationship between cognitive and motor behaviors.&nbsp;</p>\r\n",
  "category": "instructor",
  "meta_title": "",
  "meta_description": "",
  "status": NumberInt(1),
  "created_at": ISODate("2019-08-30T18:50:33.594Z"),
  "updated_at": ISODate("2019-08-30T18:50:33.604Z")
});
db.getCollection("speakers").insert({
  "_id": ObjectId("5d697077e401c22327431178"),
  "lang_content": [
    ObjectId("5d697077e401c22327431179"),
    ObjectId("5d697077e401c2232743117b"),
    ObjectId("5d697077e401c2232743117d"),
    ObjectId("5d697077e401c2232743117f")
  ],
  "fname": "Amanda",
  "lname": "Hall ",
  "name": "Amanda Hall ",
  "slug": "amanda-hall-",
  "tag_line": "PT, MPT, PCS, ATP ",
  "image": "/TrainerSpeaker/hall_amanda_circle.png",
  "bio": "<p>Amanda Hall, PT, MPT, PCS, ATP received her Master of Physical Therapy from the University of Washington in 2001. She was certified as a Pediatric Clinical Specialist through the ABPTS in 2011, and as an Assistive Technology Professional (ATP) through the Rehabilitation Engineering and Assistive Technology Society of North America in 2017. She has built her knowledge base in orthotics, casting, and splinting over 15+ years of clinical practice, which has included a wide range of pediatric and neurologic settings, including outpatient, NICU, inpatient rehabilitation, complex care, and orthotic/equipment clinic. She developed a framework for therapeutic casting based on evidence-informed practice with a focus on differential diagnosis, manual therapy, developmental kinesiopathology, and biomechanical alignment of casts, with specific use of casting materials. Her framework has a strong focus in patient-centered treatment and adaptive design. As a result, she has received mainstream media attention as the &ldquo;Madcaster&rdquo;, including features in MTV, UsWeekly, NBC, and ABC. At the Orthotics, Prosthetics, and Equipment Department of the HSC Pediatric Center in Washington, D.C., she provides serial casting for upper and lower extremities as well as orthotic, equipment, and assistive technology evaluations. Amanda teaches physical and occupational therapists casting techniques, encouraging individualized treatment and a developmental kinesiopathological approach. At CSM 2019, Amanda presented Therapeutic Casting: A Modern Clinical Pathway to Improve Outcomes as a pre-conference course for the Academy of Pediatric Physical Therapy.</p>\r\n",
  "category": "instructor",
  "meta_title": "",
  "meta_description": "",
  "status": NumberInt(1),
  "created_at": ISODate("2019-08-30T18:52:39.783Z"),
  "updated_at": ISODate("2019-08-30T18:52:39.794Z")
});
db.getCollection("speakers").insert({
  "_id": ObjectId("5d69715fe401c22327431189"),
  "lang_content": [
    ObjectId("5d69715fe401c2232743118a"),
    ObjectId("5d69715fe401c2232743118c"),
    ObjectId("5d69715fe401c2232743118e"),
    ObjectId("5d69715fe401c22327431190")
  ],
  "fname": "Susan",
  "lname": "Hastings ",
  "name": "Susan Hastings ",
  "slug": "susan-hastings-",
  "tag_line": "PT, DPT, PCS",
  "image": "/TrainerSpeaker/hastings_susan_circle.png",
  "bio": "<p>Susan Hastings, PT, DPT, PCS is a Board-Certified Pediatric Certified Specialist, who graduated from Rocky Mountain University of Health Professions (RMUoHP) with a tDPT, specializing in Pediatrics in 2010. Upon graduation, she opened her own practice, utilizing mainly FES and WBV in her treatment. She has used functional electrical stimulation (FES) in children with upper motor neuron lesions for the past 20 years, developing new applications of stimulation for treatment in children with CP, with the focus on motor learning. She has presented her findings at Combined Sections Meeting 2016 and at APTA SoPAC 2016, where she co-taught a pre-conference course with Beverly Cusick. Susan was invited to present at the 2017 California APTA Annual Conference, as well as 2016 New Mexico APTA Annual Conference, and has given courses in numerous other cities, including Rio de Janeiro. Most recently she was asked to present her findings at AACPDM 2018, where she gave a short presentation to the NMES -Gait-Assisted Group. Currently, she is collaborating with Dr. V R Edgerton and Dr. P Gad from UCLA, in order to help children with CP through novel FES applications. Previous to opening her practice, she was Sr. Therapist for Santa Clara County California Children Services (CCS) for a staff of 45 therapists. During that time she was asked to assist in courses taught by notable pediatric therapists, including Beverly Cusick (biomechanics and casting), Judy Carmick (FES), Mary Massery (function and breathing), Lois Bly (NDT baby treatment), and Lauren Beeler (NDT).</p>\r\n",
  "category": "instructor",
  "meta_title": "",
  "meta_description": "",
  "status": NumberInt(1),
  "created_at": ISODate("2019-08-30T18:56:31.37Z"),
  "updated_at": ISODate("2019-08-30T18:56:31.47Z")
});

/** states records **/
db.getCollection("states").insert({
  "_id": ObjectId("5bd16de7b605151b7e93d85d"),
  "name": "California",
  "status": NumberInt(1),
  "created_at": ISODate("2018-10-25T07:16:55.174Z"),
  "updated_at": ISODate("2019-10-10T03:54:41.593Z"),
  "default_message": ""
});
db.getCollection("states").insert({
  "_id": ObjectId("5c0acd52b52d1035f4b14181"),
  "name": "Arizona",
  "status": NumberInt(1),
  "created_at": ISODate("2018-12-07T19:43:14.205Z"),
  "updated_at": ISODate("2019-10-08T23:12:09.40Z"),
  "default_message": "This course is approved in Arizona by virtue of being approved by the Arkansas Physical Therapy Association "
});
db.getCollection("states").insert({
  "_id": ObjectId("5ccc139006e67e44fa0eb28c"),
  "name": "Pennsylvania",
  "status": NumberInt(1),
  "created_at": ISODate("2019-05-03T10:10:24.37Z"),
  "updated_at": ISODate("2019-10-08T22:30:23.420Z"),
  "default_message": ""
});
db.getCollection("states").insert({
  "_id": ObjectId("5ccc139606e67e44fa0eb290"),
  "name": "SA",
  "status": NumberInt(1),
  "created_at": ISODate("2019-05-03T10:10:30.67Z"),
  "updated_at": ISODate("2019-05-03T10:10:30.67Z")
});
db.getCollection("states").insert({
  "_id": ObjectId("5d9cdde67dbdaa69d72e5cb5"),
  "name": "New Jersey",
  "default_message": "Sorry this course in not approved by New Jersey",
  "status": NumberInt(1),
  "created_at": ISODate("2019-10-08T19:05:10.991Z"),
  "updated_at": ISODate("2019-10-08T19:29:58.670Z")
});
db.getCollection("states").insert({
  "_id": ObjectId("5d9cde337dbdaa69d72e5cb9"),
  "name": "Alabama",
  "default_message": "",
  "status": NumberInt(1),
  "created_at": ISODate("2019-10-08T19:06:27.46Z"),
  "updated_at": ISODate("2019-10-08T22:35:22.979Z")
});
db.getCollection("states").insert({
  "_id": ObjectId("5d9cde437dbdaa69d72e5cbd"),
  "name": "Alaska",
  "default_message": "This course is approved in Alaska by virtue of being approved by the Arkansas Physical Therapy Association ",
  "status": NumberInt(1),
  "created_at": ISODate("2019-10-08T19:06:43.872Z"),
  "updated_at": ISODate("2019-10-08T23:12:26.139Z")
});
db.getCollection("states").insert({
  "_id": ObjectId("5d9cde8c7dbdaa69d72e5cc1"),
  "name": "Arkansas",
  "default_message": "",
  "status": NumberInt(1),
  "created_at": ISODate("2019-10-08T19:07:56.301Z"),
  "updated_at": ISODate("2019-10-08T22:37:06.550Z")
});
db.getCollection("states").insert({
  "_id": ObjectId("5d9cdea47dbdaa69d72e5cc5"),
  "name": "Colorado",
  "default_message": "",
  "status": NumberInt(1),
  "created_at": ISODate("2019-10-08T19:08:20.420Z"),
  "updated_at": ISODate("2019-10-08T22:37:44.868Z")
});
db.getCollection("states").insert({
  "_id": ObjectId("5d9cdeae7dbdaa69d72e5cc9"),
  "name": "Connecticut",
  "default_message": "",
  "status": NumberInt(1),
  "created_at": ISODate("2019-10-08T19:08:30.736Z"),
  "updated_at": ISODate("2019-10-08T22:38:08.239Z")
});
db.getCollection("states").insert({
  "_id": ObjectId("5d9cdeb77dbdaa69d72e5ccd"),
  "name": "Delaware",
  "default_message": "This course is approved in Delaware by virtue of being approved by the Arkansas Physical Therapy Association ",
  "status": NumberInt(1),
  "created_at": ISODate("2019-10-08T19:08:39.789Z"),
  "updated_at": ISODate("2019-10-08T23:12:38.216Z")
});
db.getCollection("states").insert({
  "_id": ObjectId("5d9cdec17dbdaa69d72e5cd1"),
  "name": "Florida",
  "default_message": "",
  "status": NumberInt(1),
  "created_at": ISODate("2019-10-08T19:08:49.130Z"),
  "updated_at": ISODate("2019-10-08T22:38:46.274Z")
});
db.getCollection("states").insert({
  "_id": ObjectId("5d9cdecb7dbdaa69d72e5cd5"),
  "name": "Georgia",
  "default_message": "This course is approved in Georgia by virtue of being approved by the Arkansas Physical Therapy Association ",
  "status": NumberInt(1),
  "created_at": ISODate("2019-10-08T19:08:59.485Z"),
  "updated_at": ISODate("2019-10-08T23:12:53.101Z")
});
db.getCollection("states").insert({
  "_id": ObjectId("5d9cded57dbdaa69d72e5cd9"),
  "name": "Hawaii",
  "default_message": "",
  "status": NumberInt(1),
  "created_at": ISODate("2019-10-08T19:09:09.674Z"),
  "updated_at": ISODate("2019-10-08T22:39:39.818Z")
});
db.getCollection("states").insert({
  "_id": ObjectId("5d9cdede7dbdaa69d72e5cdd"),
  "name": "Idaho",
  "default_message": "",
  "status": NumberInt(1),
  "created_at": ISODate("2019-10-08T19:09:18.608Z"),
  "updated_at": ISODate("2019-10-08T22:40:09.982Z")
});
db.getCollection("states").insert({
  "_id": ObjectId("5d9cdee77dbdaa69d72e5ce1"),
  "name": "Illinois",
  "default_message": "",
  "status": NumberInt(1),
  "created_at": ISODate("2019-10-08T19:09:27.72Z"),
  "updated_at": ISODate("2019-10-08T22:40:48.651Z")
});
db.getCollection("states").insert({
  "_id": ObjectId("5d9cdeef7dbdaa69d72e5ce5"),
  "name": "Indiana",
  "default_message": "This course is approved in Indiana by virtue of being approved by the Arkansas Physical Therapy Association ",
  "status": NumberInt(1),
  "created_at": ISODate("2019-10-08T19:09:35.900Z"),
  "updated_at": ISODate("2019-10-08T23:13:07.275Z")
});
db.getCollection("states").insert({
  "_id": ObjectId("5d9cdef87dbdaa69d72e5ce9"),
  "name": "Iowa",
  "default_message": "",
  "status": NumberInt(1),
  "created_at": ISODate("2019-10-08T19:09:44.598Z"),
  "updated_at": ISODate("2019-10-08T22:41:25.801Z")
});
db.getCollection("states").insert({
  "_id": ObjectId("5d9cdf037dbdaa69d72e5ced"),
  "name": "Kansas",
  "default_message": "This course is approved in Kansas by virtue of being approved by the Arkansas Physical Therapy Association ",
  "status": NumberInt(1),
  "created_at": ISODate("2019-10-08T19:09:55.15Z"),
  "updated_at": ISODate("2019-10-08T23:13:51.948Z")
});
db.getCollection("states").insert({
  "_id": ObjectId("5d9cdf0f7dbdaa69d72e5cf1"),
  "name": "Kentucky",
  "default_message": "",
  "status": NumberInt(1),
  "created_at": ISODate("2019-10-08T19:10:07.542Z"),
  "updated_at": ISODate("2019-10-08T22:42:02.497Z")
});
db.getCollection("states").insert({
  "_id": ObjectId("5d9cdf197dbdaa69d72e5cf5"),
  "name": "Louisiana",
  "default_message": "",
  "status": NumberInt(1),
  "created_at": ISODate("2019-10-08T19:10:17.22Z"),
  "updated_at": ISODate("2019-10-08T22:42:22.43Z")
});
db.getCollection("states").insert({
  "_id": ObjectId("5d9cdf217dbdaa69d72e5cf9"),
  "name": "Massachusetts",
  "default_message": "",
  "status": NumberInt(1),
  "created_at": ISODate("2019-10-08T19:10:25.778Z"),
  "updated_at": ISODate("2019-10-08T22:43:22.559Z")
});
db.getCollection("states").insert({
  "_id": ObjectId("5d9cdf337dbdaa69d72e5cfd"),
  "name": "Maryland",
  "default_message": "",
  "status": NumberInt(1),
  "created_at": ISODate("2019-10-08T19:10:43.655Z"),
  "updated_at": ISODate("2019-10-08T22:43:01.511Z")
});
db.getCollection("states").insert({
  "_id": ObjectId("5d9cdf457dbdaa69d72e5d01"),
  "name": "Louisiana",
  "default_message": "",
  "status": NumberInt(1),
  "created_at": ISODate("2019-10-08T19:11:01.84Z"),
  "updated_at": ISODate("2019-10-08T22:42:40.634Z")
});
db.getCollection("states").insert({
  "_id": ObjectId("5d9cdf4c7dbdaa69d72e5d05"),
  "name": "Michigan",
  "default_message": "This course is approved in Michigan by virtue of being approved by the Arkansas",
  "status": NumberInt(1),
  "created_at": ISODate("2019-10-08T19:11:08.961Z"),
  "updated_at": ISODate("2019-10-08T22:52:30.36Z")
});
db.getCollection("states").insert({
  "_id": ObjectId("5d9cdf657dbdaa69d72e5d09"),
  "name": "Mississippi",
  "default_message": "This course is approved in Mississippi by virtue of being approved by the Arkansas",
  "status": NumberInt(1),
  "created_at": ISODate("2019-10-08T19:11:33.517Z"),
  "updated_at": ISODate("2019-10-08T22:53:02.347Z")
});
db.getCollection("states").insert({
  "_id": ObjectId("5d9cdf717dbdaa69d72e5d0d"),
  "name": "Missouri",
  "default_message": "This course is approved in Missouri by virtue of being approved by the Arkansas Physical Therapy Association ",
  "status": NumberInt(1),
  "created_at": ISODate("2019-10-08T19:11:45.447Z"),
  "updated_at": ISODate("2019-10-08T23:11:54.384Z")
});
db.getCollection("states").insert({
  "_id": ObjectId("5d9cdf7b7dbdaa69d72e5d11"),
  "name": "Montana",
  "default_message": "This course is approved in Montana by virtue of being approved by the Arkansas Physical Therapy Association ",
  "status": NumberInt(1),
  "created_at": ISODate("2019-10-08T19:11:55.344Z"),
  "updated_at": ISODate("2019-10-08T23:11:43.615Z")
});
db.getCollection("states").insert({
  "_id": ObjectId("5d9cdf857dbdaa69d72e5d15"),
  "name": "Nebraska",
  "default_message": "",
  "status": NumberInt(1),
  "created_at": ISODate("2019-10-08T19:12:05.26Z"),
  "updated_at": ISODate("2019-10-08T22:33:24.606Z")
});
db.getCollection("states").insert({
  "_id": ObjectId("5d9cdf927dbdaa69d72e5d19"),
  "name": "Nevada",
  "default_message": "",
  "status": NumberInt(1),
  "created_at": ISODate("2019-10-08T19:12:18.135Z"),
  "updated_at": ISODate("2019-10-08T22:33:12.547Z")
});
db.getCollection("states").insert({
  "_id": ObjectId("5d9cdf9d7dbdaa69d72e5d1d"),
  "name": "New Hampshire",
  "default_message": "This course is approved in New Hampshire by virtue of being approved by the Arkansas Physical Therapy Association ",
  "status": NumberInt(1),
  "created_at": ISODate("2019-10-08T19:12:29.278Z"),
  "updated_at": ISODate("2019-10-08T23:11:20.491Z")
});
db.getCollection("states").insert({
  "_id": ObjectId("5d9cdfcf7dbdaa69d72e5d21"),
  "name": "New Mexico",
  "default_message": "",
  "status": NumberInt(1),
  "created_at": ISODate("2019-10-08T19:13:19.869Z"),
  "updated_at": ISODate("2019-10-08T22:32:41.11Z")
});
db.getCollection("states").insert({
  "_id": ObjectId("5d9cdfd97dbdaa69d72e5d25"),
  "name": "New York",
  "default_message": "",
  "status": NumberInt(1),
  "created_at": ISODate("2019-10-08T19:13:29.730Z"),
  "updated_at": ISODate("2019-10-08T22:31:48.853Z")
});
db.getCollection("states").insert({
  "_id": ObjectId("5d9cdfe37dbdaa69d72e5d29"),
  "name": "North Carolina",
  "default_message": "",
  "status": NumberInt(1),
  "created_at": ISODate("2019-10-08T19:13:39.788Z"),
  "updated_at": ISODate("2019-10-08T22:31:26.12Z")
});
db.getCollection("states").insert({
  "_id": ObjectId("5d9cdfed7dbdaa69d72e5d2d"),
  "name": "North Dakota",
  "default_message": "",
  "status": NumberInt(1),
  "created_at": ISODate("2019-10-08T19:13:49.593Z"),
  "updated_at": ISODate("2019-10-08T22:31:12.738Z")
});
db.getCollection("states").insert({
  "_id": ObjectId("5d9cdff67dbdaa69d72e5d31"),
  "name": "Ohio",
  "default_message": "",
  "status": NumberInt(1),
  "created_at": ISODate("2019-10-08T19:13:58.502Z"),
  "updated_at": ISODate("2019-10-08T22:31:00.708Z")
});
db.getCollection("states").insert({
  "_id": ObjectId("5d9cdfff7dbdaa69d72e5d35"),
  "name": "Oklahoma",
  "default_message": "",
  "status": NumberInt(1),
  "created_at": ISODate("2019-10-08T19:14:07.708Z"),
  "updated_at": ISODate("2019-10-08T22:30:51.189Z")
});
db.getCollection("states").insert({
  "_id": ObjectId("5d9ce00b7dbdaa69d72e5d39"),
  "name": "Oregon",
  "default_message": "",
  "status": NumberInt(1),
  "created_at": ISODate("2019-10-08T19:14:19.98Z"),
  "updated_at": ISODate("2019-10-08T22:30:36.611Z")
});
db.getCollection("states").insert({
  "_id": ObjectId("5d9ce02b7dbdaa69d72e5d3d"),
  "name": "Rhode Island",
  "default_message": "This course is approved in Rhode Island by virtue of being approved by the Arkansas Physical Therapy Association ",
  "status": NumberInt(1),
  "created_at": ISODate("2019-10-08T19:14:51.880Z"),
  "updated_at": ISODate("2019-10-08T23:11:07.865Z")
});
db.getCollection("states").insert({
  "_id": ObjectId("5d9ce0377dbdaa69d72e5d41"),
  "name": "South Carolina",
  "default_message": "",
  "status": NumberInt(1),
  "created_at": ISODate("2019-10-08T19:15:03.277Z"),
  "updated_at": ISODate("2019-10-08T22:28:32.15Z")
});
db.getCollection("states").insert({
  "_id": ObjectId("5d9ce0407dbdaa69d72e5d45"),
  "name": "South Dakota",
  "default_message": "",
  "status": NumberInt(1),
  "created_at": ISODate("2019-10-08T19:15:12.839Z"),
  "updated_at": ISODate("2019-10-08T22:28:14.73Z")
});
db.getCollection("states").insert({
  "_id": ObjectId("5d9ce04b7dbdaa69d72e5d49"),
  "name": "Tennessee",
  "default_message": "This course is approved in Tennessee by virtue of being approved by the Arkansas Physical Therapy Association ",
  "status": NumberInt(1),
  "created_at": ISODate("2019-10-08T19:15:23.211Z"),
  "updated_at": ISODate("2019-10-08T23:10:58.293Z")
});
db.getCollection("states").insert({
  "_id": ObjectId("5d9ce0547dbdaa69d72e5d4d"),
  "name": "Texas",
  "default_message": "",
  "status": NumberInt(1),
  "created_at": ISODate("2019-10-08T19:15:32.325Z"),
  "updated_at": ISODate("2019-10-08T22:01:03.168Z")
});
db.getCollection("states").insert({
  "_id": ObjectId("5d9ce0727dbdaa69d72e5d51"),
  "name": "Utah",
  "default_message": "This course is approved in Utah by virtue of being approved by the Arkansas Physical Therapy Association ",
  "status": NumberInt(1),
  "created_at": ISODate("2019-10-08T19:16:02.278Z"),
  "updated_at": ISODate("2019-10-08T23:10:49.625Z")
});
db.getCollection("states").insert({
  "_id": ObjectId("5d9ce0837dbdaa69d72e5d55"),
  "name": "Vermont",
  "default_message": "",
  "status": NumberInt(1),
  "created_at": ISODate("2019-10-08T19:16:19.936Z"),
  "updated_at": ISODate("2019-10-08T22:00:40.265Z")
});
db.getCollection("states").insert({
  "_id": ObjectId("5d9ce08e7dbdaa69d72e5d59"),
  "name": "Virginia",
  "default_message": "This course is approved in Virginia by virtue of being approved by the Arkansas Physical Therapy Association ",
  "status": NumberInt(1),
  "created_at": ISODate("2019-10-08T19:16:30.188Z"),
  "updated_at": ISODate("2019-10-08T23:10:38.206Z")
});
db.getCollection("states").insert({
  "_id": ObjectId("5d9ce0987dbdaa69d72e5d5d"),
  "name": "Washington",
  "default_message": "",
  "status": NumberInt(1),
  "created_at": ISODate("2019-10-08T19:16:40.437Z"),
  "updated_at": ISODate("2019-10-08T22:00:27.78Z")
});
db.getCollection("states").insert({
  "_id": ObjectId("5d9ce0a47dbdaa69d72e5d61"),
  "name": "West Virginia",
  "default_message": "",
  "status": NumberInt(1),
  "created_at": ISODate("2019-10-08T19:16:52.174Z"),
  "updated_at": ISODate("2019-10-08T20:04:35.803Z")
});
db.getCollection("states").insert({
  "_id": ObjectId("5d9ce0ae7dbdaa69d72e5d65"),
  "name": "Wisconsin",
  "default_message": "This course is approved in Wisconsin by virtue of being approved by the Arkansas Physical Therapy Association ",
  "status": NumberInt(1),
  "created_at": ISODate("2019-10-08T19:17:02.839Z"),
  "updated_at": ISODate("2019-10-08T23:10:28.106Z")
});
db.getCollection("states").insert({
  "_id": ObjectId("5d9ce0b87dbdaa69d72e5d69"),
  "name": "Wyoming",
  "default_message": "",
  "status": NumberInt(1),
  "created_at": ISODate("2019-10-08T19:17:12.252Z"),
  "updated_at": ISODate("2019-10-08T20:03:59.448Z")
});
db.getCollection("states").insert({
  "_id": ObjectId("5d9d0f077dbdaa69d72e5dd0"),
  "name": "Minnesota",
  "default_message": "",
  "status": NumberInt(1),
  "created_at": ISODate("2019-10-08T22:34:47.320Z"),
  "updated_at": ISODate("2019-10-08T22:34:47.320Z")
});

/** static_page_contents records **/
db.getCollection("static_page_contents").insert({
  "_id": ObjectId("5bcd6e066c5a3c2a6cffaf48"),
  "static_page_id": ObjectId("5bcd6e066c5a3c2a6cffaf47"),
  "lang": "en",
  "body": "<div class=\"video_section about_video\">\r\n<div class=\"video\"><span><embed src=\"http://182.156.245.83:5003/gallery/video/video.mp4\" style=\"width:100%;height:400px\"></embed></span></div>\r\n</div>\r\n\r\n<div class=\"our_mission about_content\">\r\n<div class=\"container\">\r\n<h4 class=\"about_sub_head\"><span><span><span><span><span><span>Our Mission</span></span></span></span></span></span></h4>\r\n\r\n<div class=\"col-md-12 mobile_no_pt\">\r\n<h4><span><span><span><span><span>The standard Lorem Ipsum passage, used since the 1500s</span></span></span></span></span></h4>\r\n\r\n<p><span><span><span><span><span>The standard Lorem Ipsum passage, used since the 1500s &quot;Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.&quot;</span></span></span></span></span></p>\r\n\r\n<ul class=\"about_list\">\r\n\t<li><span><span><span><span><span>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</span></span></span></span></span></li>\r\n\t<li><span><span><span><span><span>Fusce bibendum libero in neque vehicula, in pharetra enim aliquam.</span></span></span></span></span></li>\r\n\t<li><span><span><span><span><span>Ut sit amet mauris vel sapien dapibus congue eu ut turpis.</span></span></span></span></span></li>\r\n\t<li><span><span><span><span><span>Curabitur mattis erat sit amet egestas scelerisque.</span></span></span></span></span></li>\r\n\t<li><span><span><span><span><span>Donec id libero eu leo facilisis commodo et a nulla.</span></span></span></span></span></li>\r\n</ul>\r\n</div>\r\n</div>\r\n</div>\r\n\r\n<div class=\"about_content\">\r\n<div class=\"container\">\r\n<h4 class=\"about_sub_head\"><span><span><span><span><span><span>Values</span></span></span></span></span></span></h4>\r\n\r\n<div class=\"col-md-12 mobile_no_pt\">\r\n<ul class=\"about_list\">\r\n\t<li><span><span><span><span><span>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</span></span></span></span></span></li>\r\n\t<li><span><span><span><span><span>Fusce bibendum libero in neque vehicula, in pharetra enim aliquam.</span></span></span></span></span></li>\r\n\t<li><span><span><span><span><span>Ut sit amet mauris vel sapien dapibus congue eu ut turpis.</span></span></span></span></span></li>\r\n\t<li><span><span><span><span><span>Curabitur mattis erat sit amet egestas scelerisque.</span></span></span></span></span></li>\r\n\t<li><span><span><span><span><span>Donec id libero eu leo facilisis commodo et a nulla.</span></span></span></span></span></li>\r\n</ul>\r\n</div>\r\n</div>\r\n</div>\r\n\r\n<div class=\"about_content purpose\">\r\n<div class=\"container\">\r\n<h4 class=\"about_sub_head\"><span><span><span><span><span>Purpose </span></span></span></span></span></h4>\r\n\r\n<div class=\"col-md-12 mobile_no_pt\">\r\n<h4><span><span><span><span><span>Why do we use it?</span></span></span></span></span></h4>\r\n\r\n<p><span><span><span><span><span>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using &#39;Content here, content here&#39;, making it look like readable English.</span></span></span></span></span></p>\r\n\r\n<p><span><span><span><span><span>Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for &#39;lorem ipsum&#39; will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like). </span></span></span></span></span></p>\r\n\r\n<p><span><span><span><span><span>Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for &#39;lorem ipsum&#39; will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like). </span></span></span></span></span></p>\r\n\r\n<ul class=\"about_list\">\r\n\t<li><span><span><span><span><span>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</span></span></span></span></span></li>\r\n\t<li><span><span><span><span><span>Fusce bibendum libero in neque vehicula, in pharetra enim aliquam.</span></span></span></span></span></li>\r\n\t<li><span><span><span><span><span>Ut sit amet mauris vel sapien dapibus congue eu ut turpis.</span></span></span></span></span></li>\r\n\t<li><span><span><span><span><span>Curabitur mattis erat sit amet egestas scelerisque.</span></span></span></span></span></li>\r\n\t<li><span><span><span><span><span>Donec id libero eu leo facilisis commodo et a nulla.</span></span></span></span></span></li>\r\n</ul>\r\n</div>\r\n</div>\r\n</div>\r\n",
  "created_at": ISODate("2018-10-22T06:28:22.99Z"),
  "updated_at": ISODate("2019-08-19T03:06:23.513Z")
});
db.getCollection("static_page_contents").insert({
  "_id": ObjectId("5bcd6e066c5a3c2a6cffaf4a"),
  "static_page_id": ObjectId("5bcd6e066c5a3c2a6cffaf47"),
  "lang": "hi",
  "body": null,
  "created_at": ISODate("2018-10-22T06:28:22.153Z"),
  "updated_at": ISODate("2019-08-19T03:06:23.513Z")
});
db.getCollection("static_page_contents").insert({
  "_id": ObjectId("5bf53d41c798c41b04a9f4d1"),
  "static_page_id": ObjectId("5bf53d41c798c41b04a9f4d0"),
  "lang": "en",
  "body": "<p><strong>What makes&nbsp;LiteGait&nbsp;the Most Advanced Solution for Gait Therapy?</strong></p>\r\n\r\n<p><strong>1 : Yoke</strong><br />\r\n<em>Rigid FlexAble</em><br />\r\nThe Y-shaped yoke supports the patient from directly over each shoulder maintaining posture and balance. This unique design allows control over the partial weight bearing status of each side of the body independently.&nbsp;FlexAble?&nbsp;Yoke easily transitions from rigid support to allow controlled vertical displacement, ideal for challenging posture and, balance during gait as therapy progresses.</p>\r\n\r\n<p><strong>2 : Straps</strong><br />\r\n<em>Right - Left - Front - Back</em><br />\r\nFour overhead adjustable straps allow correction of asymmetric upper body posture. Each of the straps can be individually adjusted to correct posture for proper walking.<br />\r\n&nbsp;</p>\r\n\r\n<p><img alt=\"\" src=\"https://www.litegait.com/sites/files/styles/large/public/inset_features.png?itok=6Q6E8cay\" style=\"float:right; height:412px; width:259px\" /></p>\r\n\r\n<p><strong>3 : Harness</strong><br />\r\n<em>Ideal &nbsp;&bull; &nbsp;Adjustable &nbsp;&bull; &nbsp;Washable</em><br />\r\nThe harness securely wraps around the patient&#39;s trunk and comfortably transfers load to the lower abdomen, over the hip, and through the groin. While the optional thigh straps will avoid loads to the groin area, the standard groin straps prevent interference with limb movement. This promotes full leg extension and prevents sitting.</p>\r\n\r\n<p><strong>4 : Actuator</strong><br />\r\n<em>No Compressors &nbsp;&bull; &nbsp;No Pumps &nbsp;&bull; &nbsp;No Noise</em><br />\r\nThe lift mechanism is a rechargeable, battery powered actuator which permits the incremental adjustment of weight bearing. This allows patients weighing up to&nbsp;500lbs&nbsp;to be brought to an upright posture by a single operator. The handheld control allows therapists to conveniently bring patient to a standing position and adjust the amount of weight supported.</p>\r\n\r\n<p><strong>5 : Base</strong><br />\r\n<em>Mobile &nbsp;&bull; &nbsp;Stable &nbsp;&bull; &nbsp;Versatile</em><br />\r\nThe wheeled base, designed for over-ground walking, fits through most doorways, over most treadmills, and under most&nbsp;hospital&nbsp;beds.&nbsp;LiteGait&reg; casters lock the device securely in place for standing activities or treadmill training. Then, when patients are ready to practice the skills they have learned,&nbsp;LiteGait&nbsp;can be smoothly pushed over ground, while preventing falls and eliminating the fear of falling.</p>\r\n",
  "created_at": ISODate("2018-11-21T11:10:57.918Z"),
  "updated_at": ISODate("2019-07-18T14:58:31.209Z")
});
db.getCollection("static_page_contents").insert({
  "_id": ObjectId("5bf53d41c798c41b04a9f4d3"),
  "static_page_id": ObjectId("5bf53d41c798c41b04a9f4d0"),
  "lang": "fr",
  "body": "<p>The unique design of&nbsp;LiteGait&nbsp;provides benefits to the Patient, the facility and to the therapist.&nbsp;The unique design of&nbsp;LiteGait&nbsp;provides benefits to the Patient, the facility and to the therapist.The unique design of&nbsp;LiteGait&nbsp;provides benefits to the Patient, the facility and to the therapist.The unique design of&nbsp;LiteGait&nbsp;provides benefits to the Patient, the facility and to the therapist.The unique design of&nbsp;LiteGait&nbsp;provides benefits to the Patient, the facility and to the therapist.The unique design of&nbsp;LiteGait&nbsp;provides benefits to the Patient, the facility and to the therapist.The unique design of&nbsp;LiteGait&nbsp;provides benefits to the Patient, the facility and to the therapist.</p>\r\n",
  "created_at": ISODate("2018-11-21T11:10:57.976Z"),
  "updated_at": ISODate("2019-07-18T14:58:31.209Z")
});
db.getCollection("static_page_contents").insert({
  "_id": ObjectId("5c346a14f0c69030a0af90f5"),
  "static_page_id": ObjectId("5c346a14f0c69030a0af90f4"),
  "lang": "en",
  "body": "<p>test</p>\r\n",
  "created_at": ISODate("2019-01-08T09:15:00.172Z"),
  "updated_at": ISODate("2019-01-08T09:15:00.172Z")
});
db.getCollection("static_page_contents").insert({
  "_id": ObjectId("5c346a14f0c69030a0af90f7"),
  "static_page_id": ObjectId("5c346a14f0c69030a0af90f4"),
  "lang": "fr",
  "body": "",
  "created_at": ISODate("2019-01-08T09:15:00.178Z"),
  "updated_at": ISODate("2019-01-08T09:15:00.178Z")
});
db.getCollection("static_page_contents").insert({
  "_id": ObjectId("5c346a25f0c69030a0af90fd"),
  "static_page_id": ObjectId("5c346a25f0c69030a0af90fc"),
  "lang": "en",
  "body": "<ul>\r\n\t<li><a href=\"http://localhost:4200/#\">Info Request</a></li>\r\n\t<li><a href=\"http://localhost:4200/#\">Info Request</a></li>\r\n\t<li><a href=\"http://localhost:4200/#\">Info Request</a></li>\r\n\t<li>&nbsp;</li>\r\n\t<li>&nbsp;</li>\r\n</ul>\r\n",
  "created_at": ISODate("2019-01-08T09:15:17.28Z"),
  "updated_at": ISODate("2019-01-08T09:15:17.28Z")
});
db.getCollection("static_page_contents").insert({
  "_id": ObjectId("5c346a25f0c69030a0af90ff"),
  "static_page_id": ObjectId("5c346a25f0c69030a0af90fc"),
  "lang": "fr",
  "body": "",
  "created_at": ISODate("2019-01-08T09:15:17.34Z"),
  "updated_at": ISODate("2019-01-08T09:15:17.34Z")
});
db.getCollection("static_page_contents").insert({
  "_id": ObjectId("5c346a33f0c69030a0af9105"),
  "static_page_id": ObjectId("5c346a33f0c69030a0af9104"),
  "lang": "en",
  "body": "<ul>\r\n\t<li><a href=\"http://localhost:4200/#\">Instructors</a></li>\r\n\t<li><a href=\"http://localhost:4200/#\">Instructors</a></li>\r\n\t<li><a href=\"http://localhost:4200/#\">Instructors</a></li>\r\n\t<li><a href=\"http://localhost:4200/#\">Instructors</a></li>\r\n\t<li><a href=\"http://localhost:4200/#\">Instructors</a></li>\r\n\t<li><a href=\"http://localhost:4200/#\">Instructors</a></li>\r\n\t<li>&nbsp;</li>\r\n\t<li>&nbsp;</li>\r\n\t<li>&nbsp;</li>\r\n\t<li>&nbsp;</li>\r\n\t<li>&nbsp;</li>\r\n</ul>\r\n",
  "created_at": ISODate("2019-01-08T09:15:31.278Z"),
  "updated_at": ISODate("2019-01-08T09:15:31.278Z")
});
db.getCollection("static_page_contents").insert({
  "_id": ObjectId("5c346a33f0c69030a0af9107"),
  "static_page_id": ObjectId("5c346a33f0c69030a0af9104"),
  "lang": "fr",
  "body": "",
  "created_at": ISODate("2019-01-08T09:15:31.283Z"),
  "updated_at": ISODate("2019-01-08T09:15:31.283Z")
});
db.getCollection("static_page_contents").insert({
  "_id": ObjectId("5c346a3cf0c69030a0af910d"),
  "static_page_id": ObjectId("5c346a3cf0c69030a0af910c"),
  "lang": "en",
  "body": "<ul>\r\n\t<li><a href=\"http://localhost:4200/#\">Clinicians</a></li>\r\n\t<li><a href=\"http://localhost:4200/#\">Clinicians</a></li>\r\n\t<li><a href=\"http://localhost:4200/#\">Clinicians</a></li>\r\n\t<li><a href=\"http://localhost:4200/#\">Clinicians</a></li>\r\n\t<li>&nbsp;</li>\r\n\t<li>&nbsp;</li>\r\n\t<li>&nbsp;</li>\r\n</ul>\r\n",
  "created_at": ISODate("2019-01-08T09:15:40.683Z"),
  "updated_at": ISODate("2019-01-08T09:15:40.683Z")
});
db.getCollection("static_page_contents").insert({
  "_id": ObjectId("5c346a3cf0c69030a0af910f"),
  "static_page_id": ObjectId("5c346a3cf0c69030a0af910c"),
  "lang": "fr",
  "body": "",
  "created_at": ISODate("2019-01-08T09:15:40.687Z"),
  "updated_at": ISODate("2019-01-08T09:15:40.687Z")
});
db.getCollection("static_page_contents").insert({
  "_id": ObjectId("5c61cb54aa4fee7be94b60e4"),
  "static_page_id": ObjectId("5c61cb54aa4fee7be94b60e3"),
  "lang": "en",
  "body": "<p>Eligible&nbsp;for 1.0&nbsp;Contact Hour</p>\r\n\r\n<p><strong>Abstract:</strong></p>\r\n\r\n<p>One of the most debilitation aspects of many neurological and musculoskeletal disorders is loss of the ability to ambulate. Much of therapeutic practice involves faciltation of the recovery of ambulation and its components, namely posture, balance, weight bearing, endurance and coordination of lower limb movement.</p>\r\n",
  "created_at": ISODate("2019-02-11T19:21:56.214Z"),
  "updated_at": ISODate("2019-02-11T19:21:56.214Z")
});
db.getCollection("static_page_contents").insert({
  "_id": ObjectId("5c61cb54aa4fee7be94b60e6"),
  "static_page_id": ObjectId("5c61cb54aa4fee7be94b60e3"),
  "lang": "fr",
  "body": "",
  "created_at": ISODate("2019-02-11T19:21:56.221Z"),
  "updated_at": ISODate("2019-02-11T19:21:56.221Z")
});
db.getCollection("static_page_contents").insert({
  "_id": ObjectId("5c61cb54aa4fee7be94b60e8"),
  "static_page_id": ObjectId("5c61cb54aa4fee7be94b60e3"),
  "lang": "Gr",
  "body": "",
  "created_at": ISODate("2019-02-11T19:21:56.224Z"),
  "updated_at": ISODate("2019-02-11T19:21:56.224Z")
});
db.getCollection("static_page_contents").insert({
  "_id": ObjectId("5c61cb54aa4fee7be94b60ea"),
  "static_page_id": ObjectId("5c61cb54aa4fee7be94b60e3"),
  "lang": "Sp",
  "body": "",
  "created_at": ISODate("2019-02-11T19:21:56.227Z"),
  "updated_at": ISODate("2019-02-11T19:21:56.227Z")
});
db.getCollection("static_page_contents").insert({
  "_id": ObjectId("5d405c57e7f8521544da144c"),
  "static_page_id": ObjectId("5d405c57e7f8521544da144b"),
  "lang": "en",
  "body": "<div class=\"education_summary_content\">\r\n<div class=\"container\">\r\n<h4><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span>The standard Lorem Ipsum passage, used since the 1500s</span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></h4>\r\n\r\n<p><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span>Mobility Research was created to bring rehabilitation solutions to clinicians, to bridge the gap between laboratory and clinical arenas, and to serve the educational needs of rehabilitation professionals. From providing our onsite educational&nbsp;inservices, equipment training sessions, and accredited seminars to organizing national and international accredited courses, we have committed extensive resources to the education of clinicians. Our hope is to prepare today&#39;s clinicians for tomorrow&#39;s challenges.</span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></p>\r\n\r\n<p><span><span><span><span><span><span><span><span><span><span><span><span>&nbsp;</span></span></span></span></span></span></span></span></span></span></span></span></p>\r\n</div>\r\n</div>\r\n\r\n<p><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><a href=\"http://182.156.245.83:2030/litegaitweb/webinars\"><img alt=\"\" src=\"http://14.98.110.245:5003/gallery//Banners/education/banner_edu_webinars.jpg\" style=\"width: 1402px; height: 300px;\" /></a> </span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></p>\r\n\r\n<div class=\"education_summary_content\">\r\n<div class=\"container\">\r\n<div class=\"col-md-12 mobile_no_pt\">\r\n<p style=\"text-align: center;\"><span><span><span><span><span><span><span><span><span><span><span><span><span><span>Short 30 - 90 minute presentations are packed with useful information augmented with instructional videos on a multitude of topics</span></span></span></span></span></span></span></span></span></span></span></span></span></span></p>\r\n\r\n<hr /></div>\r\n</div>\r\n</div>\r\n\r\n<p><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><img alt=\"\" src=\"http://14.98.110.245:5003/gallery//Banners/education/banner_edu_trainings.jpg\" style=\"width: 1402px; height: 300px;\" /> </span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></p>\r\n\r\n<div class=\"education_summary_content\">\r\n<div class=\"container\">\r\n<div class=\"col-md-12 mobile_no_pt\">\r\n<p style=\"text-align: center;\"><span><span><span><span><span><span><span><span><span><span><span><span><span><span>training and workshop brief description</span></span></span></span></span></span></span></span></span></span></span></span></span></span></p>\r\n\r\n<hr /></div>\r\n</div>\r\n</div>\r\n\r\n<p style=\"text-align: center;\"><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><img alt=\"\" src=\"http://14.98.110.245:5003/gallery//Banners/education/banner_edu_seminar1.jpg\" style=\"width: 1402px; height: 300px;\" /> </span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></p>\r\n\r\n<p style=\"text-align: center;\"><span><span><span><span><span><span><span><span><span><span><span><span><span><span>seminars brief description</span></span></span></span></span></span></span></span></span></span></span></span></span></span></p>\r\n\r\n<hr />\r\n<p style=\"text-align: center;\"><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><img alt=\"\" src=\"http://http://14.98.110.245:5003/gallery//Banners/education/banner_edu_clinicalsupport.jpg\" style=\"width: 1402px; height: 300px;\" /><img alt=\"\" src=\"http://14.98.110.245:5003/gallery//Banners/education/banner_edu_clinicalsupport.jpg\" style=\"width: 1402px; height: 300px;\" /> </span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></p>\r\n\r\n<p style=\"text-align: center;\"><span><span><span><span><span><span><span><span><span><span><span><span><span><span>Mobility Research provides exceptional clinical support ensuring your needs are met in order for you to help your clients achieve their goals using LiteGait.</span></span></span></span></span></span></span></span></span></span></span></span></span></span></p>\r\n\r\n<hr />",
  "created_at": ISODate("2019-07-30T15:03:51.902Z"),
  "updated_at": ISODate("2019-09-19T05:02:25.871Z")
});
db.getCollection("static_page_contents").insert({
  "_id": ObjectId("5d405c57e7f8521544da144e"),
  "static_page_id": ObjectId("5d405c57e7f8521544da144b"),
  "lang": "fr",
  "body": "",
  "created_at": ISODate("2019-07-30T15:03:51.906Z"),
  "updated_at": ISODate("2019-09-19T05:02:25.871Z")
});
db.getCollection("static_page_contents").insert({
  "_id": ObjectId("5d405c57e7f8521544da1450"),
  "static_page_id": ObjectId("5d405c57e7f8521544da144b"),
  "lang": "Gr",
  "body": "",
  "created_at": ISODate("2019-07-30T15:03:51.908Z"),
  "updated_at": ISODate("2019-09-19T05:02:25.871Z")
});
db.getCollection("static_page_contents").insert({
  "_id": ObjectId("5d405c57e7f8521544da1452"),
  "static_page_id": ObjectId("5d405c57e7f8521544da144b"),
  "lang": "Sp",
  "body": "",
  "created_at": ISODate("2019-07-30T15:03:51.910Z"),
  "updated_at": ISODate("2019-09-19T05:02:25.871Z")
});
db.getCollection("static_page_contents").insert({
  "_id": ObjectId("5d53e5dbfbb7781db271cd99"),
  "static_page_id": ObjectId("5d53e5dbfbb7781db271cd98"),
  "lang": "en",
  "body": "<p><span>Mobility Research provides exceptional clinical support ensuring your needs are met in order for you to help your clients achieve their goals using LiteGait. We give you direct access to our clinical educators who can provide guidance on LiteGait use with particular clients and specific treatment concerns via &ldquo;Ask a PT&rdquo;. One of our in-house clinicians produces a valuable and free quarterly webinar &ldquo;LiteGait Clinical Tips and Tricks&rdquo; that instructs new users, updates veteran users, and re-energizes occasional users. In conjunction with our onsite demonstration travel schedule, we are available for a visit to help refresh a new staff or update you on the latest advancements the LiteGait product line has brought to the rehabilitation arena.</span></p>\r\n\r\n<table border=\"0\" cellpadding=\"1\" cellspacing=\"1\" style=\"width:900px;\">\r\n\t<tbody>\r\n\t\t<tr>\r\n\t\t\t<td>\r\n\t\t\t<p>ffd dsf df dfs trt r rt rt rt ert&nbsp;</p>\r\n\r\n\t\t\t<p>fgdfgdfgdfgd</p>\r\n\r\n\t\t\t<p>hjghj</p>\r\n\t\t\t</td>\r\n\t\t\t<td><img alt=\"\" src=\"http://14.98.110.245:5003/gallery//sunflower-3792914_1920.jpg\" style=\"width: 200px; height: 93px; float: right;\" /></td>\r\n\t\t</tr>\r\n\t\t<tr>\r\n\t\t\t<td>&nbsp;</td>\r\n\t\t\t<td>&nbsp;</td>\r\n\t\t</tr>\r\n\t\t<tr>\r\n\t\t\t<td>&nbsp;</td>\r\n\t\t\t<td>&nbsp;</td>\r\n\t\t</tr>\r\n\t</tbody>\r\n</table>\r\n\r\n<hr />",
  "created_at": ISODate("2019-08-14T10:43:39.808Z"),
  "updated_at": ISODate("2019-10-04T04:41:06.867Z")
});
db.getCollection("static_page_contents").insert({
  "_id": ObjectId("5d53e5dbfbb7781db271cd9b"),
  "static_page_id": ObjectId("5d53e5dbfbb7781db271cd98"),
  "lang": "fr",
  "body": "",
  "created_at": ISODate("2019-08-14T10:43:39.810Z"),
  "updated_at": ISODate("2019-10-04T04:41:06.867Z")
});
db.getCollection("static_page_contents").insert({
  "_id": ObjectId("5d53e5dbfbb7781db271cd9d"),
  "static_page_id": ObjectId("5d53e5dbfbb7781db271cd98"),
  "lang": "Gr",
  "body": "",
  "created_at": ISODate("2019-08-14T10:43:39.812Z"),
  "updated_at": ISODate("2019-10-04T04:41:06.867Z")
});
db.getCollection("static_page_contents").insert({
  "_id": ObjectId("5d53e5dbfbb7781db271cd9f"),
  "static_page_id": ObjectId("5d53e5dbfbb7781db271cd98"),
  "lang": "Sp",
  "body": "",
  "created_at": ISODate("2019-08-14T10:43:39.813Z"),
  "updated_at": ISODate("2019-10-04T04:41:06.867Z")
});
db.getCollection("static_page_contents").insert({
  "_id": ObjectId("5d53e5f7fbb7781db271cda7"),
  "static_page_id": ObjectId("5d53e5f7fbb7781db271cda6"),
  "lang": "en",
  "body": "<h4 _ngcontent-c4=\"\">Software Update Information page</h4>\r\n",
  "created_at": ISODate("2019-08-14T10:44:07.130Z"),
  "updated_at": ISODate("2019-08-14T10:44:07.130Z")
});
db.getCollection("static_page_contents").insert({
  "_id": ObjectId("5d53e5f7fbb7781db271cda9"),
  "static_page_id": ObjectId("5d53e5f7fbb7781db271cda6"),
  "lang": "fr",
  "body": "",
  "created_at": ISODate("2019-08-14T10:44:07.133Z"),
  "updated_at": ISODate("2019-08-14T10:44:07.133Z")
});
db.getCollection("static_page_contents").insert({
  "_id": ObjectId("5d53e5f7fbb7781db271cdab"),
  "static_page_id": ObjectId("5d53e5f7fbb7781db271cda6"),
  "lang": "Gr",
  "body": "",
  "created_at": ISODate("2019-08-14T10:44:07.135Z"),
  "updated_at": ISODate("2019-08-14T10:44:07.135Z")
});
db.getCollection("static_page_contents").insert({
  "_id": ObjectId("5d53e5f7fbb7781db271cdad"),
  "static_page_id": ObjectId("5d53e5f7fbb7781db271cda6"),
  "lang": "Sp",
  "body": "",
  "created_at": ISODate("2019-08-14T10:44:07.137Z"),
  "updated_at": ISODate("2019-08-14T10:44:07.137Z")
});
db.getCollection("static_page_contents").insert({
  "_id": ObjectId("5d53fbbe257ed64e047dd0b7"),
  "static_page_id": ObjectId("5d53fbbe257ed64e047dd0b6"),
  "lang": "en",
  "body": "<p>Please fill the following form, we will address the issue soon.</p>\r\n",
  "created_at": ISODate("2019-08-14T12:17:02.676Z"),
  "updated_at": ISODate("2019-08-14T12:17:02.676Z")
});
db.getCollection("static_page_contents").insert({
  "_id": ObjectId("5d53fbbe257ed64e047dd0b9"),
  "static_page_id": ObjectId("5d53fbbe257ed64e047dd0b6"),
  "lang": "fr",
  "body": "",
  "created_at": ISODate("2019-08-14T12:17:02.681Z"),
  "updated_at": ISODate("2019-08-14T12:17:02.681Z")
});
db.getCollection("static_page_contents").insert({
  "_id": ObjectId("5d53fbbe257ed64e047dd0bb"),
  "static_page_id": ObjectId("5d53fbbe257ed64e047dd0b6"),
  "lang": "Gr",
  "body": "",
  "created_at": ISODate("2019-08-14T12:17:02.684Z"),
  "updated_at": ISODate("2019-08-14T12:17:02.684Z")
});
db.getCollection("static_page_contents").insert({
  "_id": ObjectId("5d53fbbe257ed64e047dd0bd"),
  "static_page_id": ObjectId("5d53fbbe257ed64e047dd0b6"),
  "lang": "Sp",
  "body": "",
  "created_at": ISODate("2019-08-14T12:17:02.686Z"),
  "updated_at": ISODate("2019-08-14T12:17:02.686Z")
});
db.getCollection("static_page_contents").insert({
  "_id": ObjectId("5d973e8b7dbdaa69d72e5c3f"),
  "static_page_id": ObjectId("5d973e8b7dbdaa69d72e5c3e"),
  "lang": "en",
  "body": "<p><span><span><span style=\"font-size:10px;\"><em>Host your own Clinical CEU seminar for your staff and your community.Our accredited CEU seminars are taught by independent clinical instructors, considered experts in the field of gait therapy and the treatment of children and adults with neurological and motor impairments. Our instructors range from owners of independent clinics for children and adults to PT school professors. They each bring their individual expertise and research experience, and seminar topics range from FES &amp; BWS to the Neuro &amp; Geriatric population, as well as from basic to advanced. Our courses are designed for clinicians to take away real knowledge that can be applied with their patients the very next day.</em></span></span></span></p>\r\n",
  "created_at": ISODate("2019-10-04T12:43:55.553Z"),
  "updated_at": ISODate("2019-10-04T12:43:55.553Z")
});
db.getCollection("static_page_contents").insert({
  "_id": ObjectId("5d973e8b7dbdaa69d72e5c41"),
  "static_page_id": ObjectId("5d973e8b7dbdaa69d72e5c3e"),
  "lang": "fr",
  "body": "",
  "created_at": ISODate("2019-10-04T12:43:55.558Z"),
  "updated_at": ISODate("2019-10-04T12:43:55.558Z")
});
db.getCollection("static_page_contents").insert({
  "_id": ObjectId("5d973e8b7dbdaa69d72e5c43"),
  "static_page_id": ObjectId("5d973e8b7dbdaa69d72e5c3e"),
  "lang": "Gr",
  "body": "",
  "created_at": ISODate("2019-10-04T12:43:55.560Z"),
  "updated_at": ISODate("2019-10-04T12:43:55.560Z")
});
db.getCollection("static_page_contents").insert({
  "_id": ObjectId("5d973e8b7dbdaa69d72e5c45"),
  "static_page_id": ObjectId("5d973e8b7dbdaa69d72e5c3e"),
  "lang": "Sp",
  "body": "",
  "created_at": ISODate("2019-10-04T12:43:55.562Z"),
  "updated_at": ISODate("2019-10-04T12:43:55.562Z")
});
db.getCollection("static_page_contents").insert({
  "_id": ObjectId("5da0713cbddc25177441bb4d"),
  "static_page_id": ObjectId("5da0713cbddc25177441bb4c"),
  "lang": "en",
  "body": "<p>Service check video will come here.&nbsp;Service check video will come hereService check video will come hereService check video will come hereService check video will come hereService check video will come hereService check video will come hereService check video will come here</p>\r\n",
  "created_at": ISODate("2019-10-11T12:10:36.18Z"),
  "updated_at": ISODate("2019-10-11T12:10:36.18Z")
});
db.getCollection("static_page_contents").insert({
  "_id": ObjectId("5da0713cbddc25177441bb4f"),
  "static_page_id": ObjectId("5da0713cbddc25177441bb4c"),
  "lang": "fr",
  "body": "",
  "created_at": ISODate("2019-10-11T12:10:36.24Z"),
  "updated_at": ISODate("2019-10-11T12:10:36.24Z")
});
db.getCollection("static_page_contents").insert({
  "_id": ObjectId("5da0713cbddc25177441bb51"),
  "static_page_id": ObjectId("5da0713cbddc25177441bb4c"),
  "lang": "Gr",
  "body": "",
  "created_at": ISODate("2019-10-11T12:10:36.26Z"),
  "updated_at": ISODate("2019-10-11T12:10:36.26Z")
});
db.getCollection("static_page_contents").insert({
  "_id": ObjectId("5da0713cbddc25177441bb53"),
  "static_page_id": ObjectId("5da0713cbddc25177441bb4c"),
  "lang": "Sp",
  "body": "",
  "created_at": ISODate("2019-10-11T12:10:36.28Z"),
  "updated_at": ISODate("2019-10-11T12:10:36.28Z")
});
db.getCollection("static_page_contents").insert({
  "_id": ObjectId("5da498b9bddc25177441bbb3"),
  "static_page_id": ObjectId("5da498b9bddc25177441bbb2"),
  "lang": "en",
  "body": "<div class=\"center\"><span><iframe allowfullscreen=\"\" frameborder=\"0\" height=\"540\" src=\"https://spark.adobe.com/video/1h2cwm5RqUkIr/embed\" width=\"960\"></iframe></span></div>\r\n\r\n<div formid=\"e4fc10b8-6d57-4028-85f4-ab8d6d85007e\" id=\"hubspotForm\" portalid=\"5543041\"><span><span><span>&nbsp;</span></span></span></div>\r\n",
  "created_at": ISODate("2019-10-14T15:48:09.435Z"),
  "updated_at": ISODate("2019-10-15T07:07:01.586Z")
});
db.getCollection("static_page_contents").insert({
  "_id": ObjectId("5da498b9bddc25177441bbb5"),
  "static_page_id": ObjectId("5da498b9bddc25177441bbb2"),
  "lang": "fr",
  "body": "",
  "created_at": ISODate("2019-10-14T15:48:09.446Z"),
  "updated_at": ISODate("2019-10-15T07:07:01.586Z")
});
db.getCollection("static_page_contents").insert({
  "_id": ObjectId("5da498b9bddc25177441bbb7"),
  "static_page_id": ObjectId("5da498b9bddc25177441bbb2"),
  "lang": "Gr",
  "body": "",
  "created_at": ISODate("2019-10-14T15:48:09.449Z"),
  "updated_at": ISODate("2019-10-15T07:07:01.586Z")
});
db.getCollection("static_page_contents").insert({
  "_id": ObjectId("5da498b9bddc25177441bbb9"),
  "static_page_id": ObjectId("5da498b9bddc25177441bbb2"),
  "lang": "Sp",
  "body": "",
  "created_at": ISODate("2019-10-14T15:48:09.453Z"),
  "updated_at": ISODate("2019-10-15T07:07:01.586Z")
});
db.getCollection("static_page_contents").insert({
  "_id": ObjectId("5da59eeb11ece21ef7596950"),
  "static_page_id": ObjectId("5da59eeb11ece21ef759694f"),
  "lang": "en",
  "body": "\r\n\r\n<div formid=\"e4fc10b8-6d57-4028-85f4-ab8d6d85007e\" id=\"hubspotForm\" portalid=\"5543041\"><span><span>&nbsp;</span></span></div>\r\n",
  "created_at": ISODate("2019-10-15T10:26:51.682Z"),
  "updated_at": ISODate("2019-10-15T15:28:39.468Z")
});
db.getCollection("static_page_contents").insert({
  "_id": ObjectId("5da59eeb11ece21ef7596952"),
  "static_page_id": ObjectId("5da59eeb11ece21ef759694f"),
  "lang": "fr",
  "body": "",
  "created_at": ISODate("2019-10-15T10:26:51.686Z"),
  "updated_at": ISODate("2019-10-15T15:28:39.468Z")
});
db.getCollection("static_page_contents").insert({
  "_id": ObjectId("5da59eeb11ece21ef7596954"),
  "static_page_id": ObjectId("5da59eeb11ece21ef759694f"),
  "lang": "Gr",
  "body": "",
  "created_at": ISODate("2019-10-15T10:26:51.689Z"),
  "updated_at": ISODate("2019-10-15T15:28:39.468Z")
});
db.getCollection("static_page_contents").insert({
  "_id": ObjectId("5da59eeb11ece21ef7596956"),
  "static_page_id": ObjectId("5da59eeb11ece21ef759694f"),
  "lang": "Sp",
  "body": "",
  "created_at": ISODate("2019-10-15T10:26:51.691Z"),
  "updated_at": ISODate("2019-10-15T15:28:39.469Z")
});
db.getCollection("static_page_contents").insert({
  "_id": ObjectId("5da59ef611ece21ef7596961"),
  "static_page_id": ObjectId("5da59ef611ece21ef7596960"),
  "lang": "en",
  "body": "<p>Rent This Device</p>\r\n",
  "created_at": ISODate("2019-10-15T10:27:02.778Z"),
  "updated_at": ISODate("2019-10-15T10:27:02.778Z")
});
db.getCollection("static_page_contents").insert({
  "_id": ObjectId("5da59ef611ece21ef7596963"),
  "static_page_id": ObjectId("5da59ef611ece21ef7596960"),
  "lang": "fr",
  "body": "",
  "created_at": ISODate("2019-10-15T10:27:02.781Z"),
  "updated_at": ISODate("2019-10-15T10:27:02.781Z")
});
db.getCollection("static_page_contents").insert({
  "_id": ObjectId("5da59ef611ece21ef7596965"),
  "static_page_id": ObjectId("5da59ef611ece21ef7596960"),
  "lang": "Gr",
  "body": "",
  "created_at": ISODate("2019-10-15T10:27:02.784Z"),
  "updated_at": ISODate("2019-10-15T10:27:02.784Z")
});
db.getCollection("static_page_contents").insert({
  "_id": ObjectId("5da59ef611ece21ef7596967"),
  "static_page_id": ObjectId("5da59ef611ece21ef7596960"),
  "lang": "Sp",
  "body": "",
  "created_at": ISODate("2019-10-15T10:27:02.785Z"),
  "updated_at": ISODate("2019-10-15T10:27:02.785Z")
});
db.getCollection("static_page_contents").insert({
  "_id": ObjectId("5da59f0111ece21ef7596972"),
  "static_page_id": ObjectId("5da59f0111ece21ef7596971"),
  "lang": "en",
  "body": "<p><span><span>Request a Demonstration</span></span></p>\r\n\r\n<div formid=\"e4fc10b8-6d57-4028-85f4-ab8d6d85007e\" id=\"hubspotForm\" portalid=\"5543041\"><span><span><span>&nbsp;</span></span></span></div>\r\n",
  "created_at": ISODate("2019-10-15T10:27:13.241Z"),
  "updated_at": ISODate("2019-10-15T15:31:23.998Z")
});
db.getCollection("static_page_contents").insert({
  "_id": ObjectId("5da59f0111ece21ef7596974"),
  "static_page_id": ObjectId("5da59f0111ece21ef7596971"),
  "lang": "fr",
  "body": "",
  "created_at": ISODate("2019-10-15T10:27:13.243Z"),
  "updated_at": ISODate("2019-10-15T15:31:23.998Z")
});
db.getCollection("static_page_contents").insert({
  "_id": ObjectId("5da59f0111ece21ef7596976"),
  "static_page_id": ObjectId("5da59f0111ece21ef7596971"),
  "lang": "Gr",
  "body": "",
  "created_at": ISODate("2019-10-15T10:27:13.245Z"),
  "updated_at": ISODate("2019-10-15T15:31:23.998Z")
});
db.getCollection("static_page_contents").insert({
  "_id": ObjectId("5da59f0111ece21ef7596978"),
  "static_page_id": ObjectId("5da59f0111ece21ef7596971"),
  "lang": "Sp",
  "body": "",
  "created_at": ISODate("2019-10-15T10:27:13.246Z"),
  "updated_at": ISODate("2019-10-15T15:31:23.998Z")
});
db.getCollection("static_page_contents").insert({
  "_id": ObjectId("5da6f7e211ece21ef75969e7"),
  "static_page_id": ObjectId("5da6f7e211ece21ef75969e6"),
  "lang": "en",
  "body": "<div class=\"container\">\r\n<div class=\"col-xs-3\"><br />\r\n<br />\r\n<br />\r\n<br />\r\n<span><span><span><span>Left Data will come here</span></span></span></span></div>\r\n\r\n<div class=\"col-xs-9\">\r\n<div class=\"select_product_outer_heading\">\r\n<h3><span><span><span><span><span><span>Submit a Service Assistance Request Ticket</span></span></span></span></span></span></h3>\r\n</div>\r\n\r\n<div formid=\"ca763588-787c-41e0-a40d-ec8300c532c1\" id=\"hubspotForm\" portalid=\"5543041\"><span><span><span><span><span><span><span><span><span><span><span>&nbsp;</span></span></span></span></span></span></span></span></span></span></span></div>\r\n</div>\r\n</div>\r\n",
  "created_at": ISODate("2019-10-16T10:58:42.59Z"),
  "updated_at": ISODate("2019-11-22T05:50:50.310Z")
});
db.getCollection("static_page_contents").insert({
  "_id": ObjectId("5da6f7e211ece21ef75969e9"),
  "static_page_id": ObjectId("5da6f7e211ece21ef75969e6"),
  "lang": "fr",
  "body": "",
  "created_at": ISODate("2019-10-16T10:58:42.67Z"),
  "updated_at": ISODate("2019-11-22T05:50:50.310Z")
});
db.getCollection("static_page_contents").insert({
  "_id": ObjectId("5da6f7e211ece21ef75969eb"),
  "static_page_id": ObjectId("5da6f7e211ece21ef75969e6"),
  "lang": "Gr",
  "body": "",
  "created_at": ISODate("2019-10-16T10:58:42.72Z"),
  "updated_at": ISODate("2019-11-22T05:50:50.310Z")
});
db.getCollection("static_page_contents").insert({
  "_id": ObjectId("5da6f7e211ece21ef75969ed"),
  "static_page_id": ObjectId("5da6f7e211ece21ef75969e6"),
  "lang": "Sp",
  "body": "",
  "created_at": ISODate("2019-10-16T10:58:42.76Z"),
  "updated_at": ISODate("2019-11-22T05:50:50.310Z")
});

/** static_pages records **/
db.getCollection("static_pages").insert({
  "_id": ObjectId("5bcd6e066c5a3c2a6cffaf47"),
  "lang_content": [
    ObjectId("5bcd6e066c5a3c2a6cffaf48"),
    ObjectId("5bcd6e066c5a3c2a6cffaf4a")
  ],
  "slug": "about-us",
  "title": "About US",
  "body": "<div class=\"video_section about_video\">\r\n<div class=\"video\"><span><embed src=\"http://182.156.245.83:5003/gallery/video/video.mp4\" style=\"width:100%;height:400px\"></embed></span></div>\r\n</div>\r\n\r\n<div class=\"our_mission about_content\">\r\n<div class=\"container\">\r\n<h4 class=\"about_sub_head\"><span><span><span><span><span><span>Our Mission</span></span></span></span></span></span></h4>\r\n\r\n<div class=\"col-md-12 mobile_no_pt\">\r\n<h4><span><span><span><span><span>The standard Lorem Ipsum passage, used since the 1500s</span></span></span></span></span></h4>\r\n\r\n<p><span><span><span><span><span>The standard Lorem Ipsum passage, used since the 1500s &quot;Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.&quot;</span></span></span></span></span></p>\r\n\r\n<ul class=\"about_list\">\r\n\t<li><span><span><span><span><span>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</span></span></span></span></span></li>\r\n\t<li><span><span><span><span><span>Fusce bibendum libero in neque vehicula, in pharetra enim aliquam.</span></span></span></span></span></li>\r\n\t<li><span><span><span><span><span>Ut sit amet mauris vel sapien dapibus congue eu ut turpis.</span></span></span></span></span></li>\r\n\t<li><span><span><span><span><span>Curabitur mattis erat sit amet egestas scelerisque.</span></span></span></span></span></li>\r\n\t<li><span><span><span><span><span>Donec id libero eu leo facilisis commodo et a nulla.</span></span></span></span></span></li>\r\n</ul>\r\n</div>\r\n</div>\r\n</div>\r\n\r\n<div class=\"about_content\">\r\n<div class=\"container\">\r\n<h4 class=\"about_sub_head\"><span><span><span><span><span><span>Values</span></span></span></span></span></span></h4>\r\n\r\n<div class=\"col-md-12 mobile_no_pt\">\r\n<ul class=\"about_list\">\r\n\t<li><span><span><span><span><span>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</span></span></span></span></span></li>\r\n\t<li><span><span><span><span><span>Fusce bibendum libero in neque vehicula, in pharetra enim aliquam.</span></span></span></span></span></li>\r\n\t<li><span><span><span><span><span>Ut sit amet mauris vel sapien dapibus congue eu ut turpis.</span></span></span></span></span></li>\r\n\t<li><span><span><span><span><span>Curabitur mattis erat sit amet egestas scelerisque.</span></span></span></span></span></li>\r\n\t<li><span><span><span><span><span>Donec id libero eu leo facilisis commodo et a nulla.</span></span></span></span></span></li>\r\n</ul>\r\n</div>\r\n</div>\r\n</div>\r\n\r\n<div class=\"about_content purpose\">\r\n<div class=\"container\">\r\n<h4 class=\"about_sub_head\"><span><span><span><span><span>Purpose </span></span></span></span></span></h4>\r\n\r\n<div class=\"col-md-12 mobile_no_pt\">\r\n<h4><span><span><span><span><span>Why do we use it?</span></span></span></span></span></h4>\r\n\r\n<p><span><span><span><span><span>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using &#39;Content here, content here&#39;, making it look like readable English.</span></span></span></span></span></p>\r\n\r\n<p><span><span><span><span><span>Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for &#39;lorem ipsum&#39; will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like). </span></span></span></span></span></p>\r\n\r\n<p><span><span><span><span><span>Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for &#39;lorem ipsum&#39; will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like). </span></span></span></span></span></p>\r\n\r\n<ul class=\"about_list\">\r\n\t<li><span><span><span><span><span>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</span></span></span></span></span></li>\r\n\t<li><span><span><span><span><span>Fusce bibendum libero in neque vehicula, in pharetra enim aliquam.</span></span></span></span></span></li>\r\n\t<li><span><span><span><span><span>Ut sit amet mauris vel sapien dapibus congue eu ut turpis.</span></span></span></span></span></li>\r\n\t<li><span><span><span><span><span>Curabitur mattis erat sit amet egestas scelerisque.</span></span></span></span></span></li>\r\n\t<li><span><span><span><span><span>Donec id libero eu leo facilisis commodo et a nulla.</span></span></span></span></span></li>\r\n</ul>\r\n</div>\r\n</div>\r\n</div>\r\n",
  "meta_title": "about us title",
  "meta_description": "about us desc",
  "created_at": ISODate("2018-10-22T06:28:22.94Z"),
  "updated_at": ISODate("2019-08-19T03:06:23.509Z"),
  "testimonials": null,
  "banner_image": "",
  "page_video": null
});
db.getCollection("static_pages").insert({
  "_id": ObjectId("5bf53d41c798c41b04a9f4d0"),
  "lang_content": [
    ObjectId("5bf53d41c798c41b04a9f4d1"),
    ObjectId("5bf53d41c798c41b04a9f4d3")
  ],
  "slug": "why-litegait",
  "title": "Why LiteGait",
  "body": "<p><strong>What makes&nbsp;LiteGait&nbsp;the Most Advanced Solution for Gait Therapy?</strong></p>\r\n\r\n<p><strong>1 : Yoke</strong><br />\r\n<em>Rigid FlexAble</em><br />\r\nThe Y-shaped yoke supports the patient from directly over each shoulder maintaining posture and balance. This unique design allows control over the partial weight bearing status of each side of the body independently.&nbsp;FlexAble?&nbsp;Yoke easily transitions from rigid support to allow controlled vertical displacement, ideal for challenging posture and, balance during gait as therapy progresses.</p>\r\n\r\n<p><strong>2 : Straps</strong><br />\r\n<em>Right - Left - Front - Back</em><br />\r\nFour overhead adjustable straps allow correction of asymmetric upper body posture. Each of the straps can be individually adjusted to correct posture for proper walking.<br />\r\n&nbsp;</p>\r\n\r\n<p><img alt=\"\" src=\"https://www.litegait.com/sites/files/styles/large/public/inset_features.png?itok=6Q6E8cay\" style=\"float:right; height:412px; width:259px\" /></p>\r\n\r\n<p><strong>3 : Harness</strong><br />\r\n<em>Ideal &nbsp;&bull; &nbsp;Adjustable &nbsp;&bull; &nbsp;Washable</em><br />\r\nThe harness securely wraps around the patient&#39;s trunk and comfortably transfers load to the lower abdomen, over the hip, and through the groin. While the optional thigh straps will avoid loads to the groin area, the standard groin straps prevent interference with limb movement. This promotes full leg extension and prevents sitting.</p>\r\n\r\n<p><strong>4 : Actuator</strong><br />\r\n<em>No Compressors &nbsp;&bull; &nbsp;No Pumps &nbsp;&bull; &nbsp;No Noise</em><br />\r\nThe lift mechanism is a rechargeable, battery powered actuator which permits the incremental adjustment of weight bearing. This allows patients weighing up to&nbsp;500lbs&nbsp;to be brought to an upright posture by a single operator. The handheld control allows therapists to conveniently bring patient to a standing position and adjust the amount of weight supported.</p>\r\n\r\n<p><strong>5 : Base</strong><br />\r\n<em>Mobile &nbsp;&bull; &nbsp;Stable &nbsp;&bull; &nbsp;Versatile</em><br />\r\nThe wheeled base, designed for over-ground walking, fits through most doorways, over most treadmills, and under most&nbsp;hospital&nbsp;beds.&nbsp;LiteGait&reg; casters lock the device securely in place for standing activities or treadmill training. Then, when patients are ready to practice the skills they have learned,&nbsp;LiteGait&nbsp;can be smoothly pushed over ground, while preventing falls and eliminating the fear of falling.</p>\r\n",
  "meta_title": "Why LiteGait",
  "meta_description": "Why LiteGait",
  "created_at": ISODate("2018-11-21T11:10:57.848Z"),
  "updated_at": ISODate("2019-07-18T14:58:31.205Z"),
  "testimonials": null,
  "banner_image": ""
});
db.getCollection("static_pages").insert({
  "_id": ObjectId("5c346a14f0c69030a0af90f4"),
  "lang_content": [
    ObjectId("5c346a14f0c69030a0af90f5"),
    ObjectId("5c346a14f0c69030a0af90f7")
  ],
  "slug": "support",
  "title": "Support",
  "body": "<p>test</p>\r\n",
  "meta_title": "",
  "meta_description": "",
  "created_at": ISODate("2019-01-08T09:15:00.163Z"),
  "updated_at": ISODate("2019-01-08T09:15:00.179Z")
});
db.getCollection("static_pages").insert({
  "_id": ObjectId("5c346a25f0c69030a0af90fc"),
  "lang_content": [
    ObjectId("5c346a25f0c69030a0af90fd"),
    ObjectId("5c346a25f0c69030a0af90ff")
  ],
  "slug": "info-request",
  "title": "Info Request",
  "body": "<ul>\r\n\t<li><a href=\"http://localhost:4200/#\">Info Request</a></li>\r\n\t<li><a href=\"http://localhost:4200/#\">Info Request</a></li>\r\n\t<li><a href=\"http://localhost:4200/#\">Info Request</a></li>\r\n\t<li>&nbsp;</li>\r\n\t<li>&nbsp;</li>\r\n</ul>\r\n",
  "meta_title": "",
  "meta_description": "",
  "created_at": ISODate("2019-01-08T09:15:17.26Z"),
  "updated_at": ISODate("2019-01-08T09:15:17.35Z")
});
db.getCollection("static_pages").insert({
  "_id": ObjectId("5c346a33f0c69030a0af9104"),
  "lang_content": [
    ObjectId("5c346a33f0c69030a0af9105"),
    ObjectId("5c346a33f0c69030a0af9107")
  ],
  "slug": "instructors",
  "title": "Instructors",
  "body": "<ul>\r\n\t<li><a href=\"http://localhost:4200/#\">Instructors</a></li>\r\n\t<li><a href=\"http://localhost:4200/#\">Instructors</a></li>\r\n\t<li><a href=\"http://localhost:4200/#\">Instructors</a></li>\r\n\t<li><a href=\"http://localhost:4200/#\">Instructors</a></li>\r\n\t<li><a href=\"http://localhost:4200/#\">Instructors</a></li>\r\n\t<li><a href=\"http://localhost:4200/#\">Instructors</a></li>\r\n\t<li>&nbsp;</li>\r\n\t<li>&nbsp;</li>\r\n\t<li>&nbsp;</li>\r\n\t<li>&nbsp;</li>\r\n\t<li>&nbsp;</li>\r\n</ul>\r\n",
  "meta_title": "",
  "meta_description": "",
  "created_at": ISODate("2019-01-08T09:15:31.276Z"),
  "updated_at": ISODate("2019-01-08T09:15:31.284Z")
});
db.getCollection("static_pages").insert({
  "_id": ObjectId("5c346a3cf0c69030a0af910c"),
  "lang_content": [
    ObjectId("5c346a3cf0c69030a0af910d"),
    ObjectId("5c346a3cf0c69030a0af910f")
  ],
  "slug": "clinicians",
  "title": "Clinicians",
  "body": "<ul>\r\n\t<li><a href=\"http://localhost:4200/#\">Clinicians</a></li>\r\n\t<li><a href=\"http://localhost:4200/#\">Clinicians</a></li>\r\n\t<li><a href=\"http://localhost:4200/#\">Clinicians</a></li>\r\n\t<li><a href=\"http://localhost:4200/#\">Clinicians</a></li>\r\n\t<li>&nbsp;</li>\r\n\t<li>&nbsp;</li>\r\n\t<li>&nbsp;</li>\r\n</ul>\r\n",
  "meta_title": "",
  "meta_description": "",
  "created_at": ISODate("2019-01-08T09:15:40.681Z"),
  "updated_at": ISODate("2019-01-08T09:15:40.688Z")
});
db.getCollection("static_pages").insert({
  "_id": ObjectId("5c61cb54aa4fee7be94b60e3"),
  "lang_content": [
    ObjectId("5c61cb54aa4fee7be94b60e4"),
    ObjectId("5c61cb54aa4fee7be94b60e6"),
    ObjectId("5c61cb54aa4fee7be94b60e8"),
    ObjectId("5c61cb54aa4fee7be94b60ea")
  ],
  "slug": "litegait-therapy",
  "title": "LiteGait Therapy- Clinical Foundation & Demonstration",
  "body": "<p>Eligible&nbsp;for 1.0&nbsp;Contact Hour</p>\r\n\r\n<p><strong>Abstract:</strong></p>\r\n\r\n<p>One of the most debilitation aspects of many neurological and musculoskeletal disorders is loss of the ability to ambulate. Much of therapeutic practice involves faciltation of the recovery of ambulation and its components, namely posture, balance, weight bearing, endurance and coordination of lower limb movement.</p>\r\n",
  "meta_title": "",
  "meta_description": "",
  "created_at": ISODate("2019-02-11T19:21:56.185Z"),
  "updated_at": ISODate("2019-02-11T19:21:56.230Z")
});
db.getCollection("static_pages").insert({
  "_id": ObjectId("5d405c57e7f8521544da144b"),
  "lang_content": [
    ObjectId("5d405c57e7f8521544da144c"),
    ObjectId("5d405c57e7f8521544da144e"),
    ObjectId("5d405c57e7f8521544da1450"),
    ObjectId("5d405c57e7f8521544da1452")
  ],
  "slug": "education",
  "title": "Education",
  "body": "<div class=\"education_summary_content\">\r\n<div class=\"container\">\r\n<h4><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span>The standard Lorem Ipsum passage, used since the 1500s</span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></h4>\r\n\r\n<p><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span>Mobility Research was created to bring rehabilitation solutions to clinicians, to bridge the gap between laboratory and clinical arenas, and to serve the educational needs of rehabilitation professionals. From providing our onsite educational&nbsp;inservices, equipment training sessions, and accredited seminars to organizing national and international accredited courses, we have committed extensive resources to the education of clinicians. Our hope is to prepare today&#39;s clinicians for tomorrow&#39;s challenges.</span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></p>\r\n\r\n<p><span><span><span><span><span><span><span><span><span><span><span><span>&nbsp;</span></span></span></span></span></span></span></span></span></span></span></span></p>\r\n</div>\r\n</div>\r\n\r\n<p><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><a href=\"http://182.156.245.83:2030/litegaitweb/webinars\"><img alt=\"\" src=\"http://14.98.110.245:5003/gallery//Banners/education/banner_edu_webinars.jpg\" style=\"width: 1402px; height: 300px;\" /></a> </span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></p>\r\n\r\n<div class=\"education_summary_content\">\r\n<div class=\"container\">\r\n<div class=\"col-md-12 mobile_no_pt\">\r\n<p style=\"text-align: center;\"><span><span><span><span><span><span><span><span><span><span><span><span><span><span>Short 30 - 90 minute presentations are packed with useful information augmented with instructional videos on a multitude of topics</span></span></span></span></span></span></span></span></span></span></span></span></span></span></p>\r\n\r\n<hr /></div>\r\n</div>\r\n</div>\r\n\r\n<p><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><img alt=\"\" src=\"http://14.98.110.245:5003/gallery//Banners/education/banner_edu_trainings.jpg\" style=\"width: 1402px; height: 300px;\" /> </span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></p>\r\n\r\n<div class=\"education_summary_content\">\r\n<div class=\"container\">\r\n<div class=\"col-md-12 mobile_no_pt\">\r\n<p style=\"text-align: center;\"><span><span><span><span><span><span><span><span><span><span><span><span><span><span>training and workshop brief description</span></span></span></span></span></span></span></span></span></span></span></span></span></span></p>\r\n\r\n<hr /></div>\r\n</div>\r\n</div>\r\n\r\n<p style=\"text-align: center;\"><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><img alt=\"\" src=\"http://14.98.110.245:5003/gallery//Banners/education/banner_edu_seminar1.jpg\" style=\"width: 1402px; height: 300px;\" /> </span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></p>\r\n\r\n<p style=\"text-align: center;\"><span><span><span><span><span><span><span><span><span><span><span><span><span><span>seminars brief description</span></span></span></span></span></span></span></span></span></span></span></span></span></span></p>\r\n\r\n<hr />\r\n<p style=\"text-align: center;\"><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><span><img alt=\"\" src=\"http://http://14.98.110.245:5003/gallery//Banners/education/banner_edu_clinicalsupport.jpg\" style=\"width: 1402px; height: 300px;\" /><img alt=\"\" src=\"http://14.98.110.245:5003/gallery//Banners/education/banner_edu_clinicalsupport.jpg\" style=\"width: 1402px; height: 300px;\" /> </span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></p>\r\n\r\n<p style=\"text-align: center;\"><span><span><span><span><span><span><span><span><span><span><span><span><span><span>Mobility Research provides exceptional clinical support ensuring your needs are met in order for you to help your clients achieve their goals using LiteGait.</span></span></span></span></span></span></span></span></span></span></span></span></span></span></p>\r\n\r\n<hr />",
  "banner_image": "",
  "meta_title": "",
  "meta_description": "",
  "created_at": ISODate("2019-07-30T15:03:51.900Z"),
  "updated_at": ISODate("2019-09-19T05:02:25.867Z"),
  "page_video": null,
  "testimonials": [
    ObjectId("5c36edb38ba4672adbd9402e")
  ]
});
db.getCollection("static_pages").insert({
  "_id": ObjectId("5d53e5dbfbb7781db271cd98"),
  "lang_content": [
    ObjectId("5d53e5dbfbb7781db271cd99"),
    ObjectId("5d53e5dbfbb7781db271cd9b"),
    ObjectId("5d53e5dbfbb7781db271cd9d"),
    ObjectId("5d53e5dbfbb7781db271cd9f")
  ],
  "slug": "clinical-support",
  "title": "Clinical Support",
  "body": "<p><span>Mobility Research provides exceptional clinical support ensuring your needs are met in order for you to help your clients achieve their goals using LiteGait. We give you direct access to our clinical educators who can provide guidance on LiteGait use with particular clients and specific treatment concerns via &ldquo;Ask a PT&rdquo;. One of our in-house clinicians produces a valuable and free quarterly webinar &ldquo;LiteGait Clinical Tips and Tricks&rdquo; that instructs new users, updates veteran users, and re-energizes occasional users. In conjunction with our onsite demonstration travel schedule, we are available for a visit to help refresh a new staff or update you on the latest advancements the LiteGait product line has brought to the rehabilitation arena.</span></p>\r\n\r\n<table border=\"0\" cellpadding=\"1\" cellspacing=\"1\" style=\"width:900px;\">\r\n\t<tbody>\r\n\t\t<tr>\r\n\t\t\t<td>\r\n\t\t\t<p>ffd dsf df dfs trt r rt rt rt ert&nbsp;</p>\r\n\r\n\t\t\t<p>fgdfgdfgdfgd</p>\r\n\r\n\t\t\t<p>hjghj</p>\r\n\t\t\t</td>\r\n\t\t\t<td><img alt=\"\" src=\"http://14.98.110.245:5003/gallery//sunflower-3792914_1920.jpg\" style=\"width: 200px; height: 93px; float: right;\" /></td>\r\n\t\t</tr>\r\n\t\t<tr>\r\n\t\t\t<td>&nbsp;</td>\r\n\t\t\t<td>&nbsp;</td>\r\n\t\t</tr>\r\n\t\t<tr>\r\n\t\t\t<td>&nbsp;</td>\r\n\t\t\t<td>&nbsp;</td>\r\n\t\t</tr>\r\n\t</tbody>\r\n</table>\r\n\r\n<hr />",
  "banner_image": "/Banners/homepg_support/clinical support_homepg-03.jpg",
  "meta_title": "",
  "meta_description": "",
  "created_at": ISODate("2019-08-14T10:43:39.806Z"),
  "updated_at": ISODate("2019-10-04T04:41:06.863Z"),
  "page_video": null,
  "testimonials": null
});
db.getCollection("static_pages").insert({
  "_id": ObjectId("5d53e5f7fbb7781db271cda6"),
  "lang_content": [
    ObjectId("5d53e5f7fbb7781db271cda7"),
    ObjectId("5d53e5f7fbb7781db271cda9"),
    ObjectId("5d53e5f7fbb7781db271cdab"),
    ObjectId("5d53e5f7fbb7781db271cdad")
  ],
  "slug": "software-update-information",
  "title": "Software Update Information",
  "body": "<h4 _ngcontent-c4=\"\">Software Update Information page</h4>\r\n",
  "banner_image": "",
  "meta_title": "",
  "meta_description": "",
  "created_at": ISODate("2019-08-14T10:44:07.129Z"),
  "updated_at": ISODate("2019-08-14T10:44:07.138Z")
});
db.getCollection("static_pages").insert({
  "_id": ObjectId("5d53fbbe257ed64e047dd0b6"),
  "lang_content": [
    ObjectId("5d53fbbe257ed64e047dd0b7"),
    ObjectId("5d53fbbe257ed64e047dd0b9"),
    ObjectId("5d53fbbe257ed64e047dd0bb"),
    ObjectId("5d53fbbe257ed64e047dd0bd")
  ],
  "slug": "contact-us",
  "title": "Contact Us",
  "body": "<p>Please fill the following form, we will address the issue soon.</p>\r\n",
  "banner_image": "",
  "meta_title": "",
  "meta_description": "",
  "created_at": ISODate("2019-08-14T12:17:02.672Z"),
  "updated_at": ISODate("2019-08-14T12:17:02.687Z")
});
db.getCollection("static_pages").insert({
  "_id": ObjectId("5d973e8b7dbdaa69d72e5c3e"),
  "lang_content": [
    ObjectId("5d973e8b7dbdaa69d72e5c3f"),
    ObjectId("5d973e8b7dbdaa69d72e5c41"),
    ObjectId("5d973e8b7dbdaa69d72e5c43"),
    ObjectId("5d973e8b7dbdaa69d72e5c45")
  ],
  "slug": "seminar-page-top-text",
  "title": "Seminar Page Top Text",
  "body": "<p><span><span><span style=\"font-size:10px;\"><em>Host your own Clinical CEU seminar for your staff and your community.Our accredited CEU seminars are taught by independent clinical instructors, considered experts in the field of gait therapy and the treatment of children and adults with neurological and motor impairments. Our instructors range from owners of independent clinics for children and adults to PT school professors. They each bring their individual expertise and research experience, and seminar topics range from FES &amp; BWS to the Neuro &amp; Geriatric population, as well as from basic to advanced. Our courses are designed for clinicians to take away real knowledge that can be applied with their patients the very next day.</em></span></span></span></p>\r\n",
  "banner_image": "",
  "meta_title": "",
  "meta_description": "",
  "created_at": ISODate("2019-10-04T12:43:55.549Z"),
  "updated_at": ISODate("2019-10-04T12:43:55.563Z")
});
db.getCollection("static_pages").insert({
  "_id": ObjectId("5da0713cbddc25177441bb4c"),
  "lang_content": [
    ObjectId("5da0713cbddc25177441bb4d"),
    ObjectId("5da0713cbddc25177441bb4f"),
    ObjectId("5da0713cbddc25177441bb51"),
    ObjectId("5da0713cbddc25177441bb53")
  ],
  "slug": "service-check-video",
  "title": "Service check video",
  "body": "<p>Service check video will come here.&nbsp;Service check video will come hereService check video will come hereService check video will come hereService check video will come hereService check video will come hereService check video will come hereService check video will come here</p>\r\n",
  "banner_image": "",
  "meta_title": "",
  "meta_description": "",
  "created_at": ISODate("2019-10-11T12:10:36.14Z"),
  "updated_at": ISODate("2019-10-11T12:10:36.29Z")
});
db.getCollection("static_pages").insert({
  "_id": ObjectId("5da498b9bddc25177441bbb2"),
  "lang_content": [
    ObjectId("5da498b9bddc25177441bbb3"),
    ObjectId("5da498b9bddc25177441bbb5"),
    ObjectId("5da498b9bddc25177441bbb7"),
    ObjectId("5da498b9bddc25177441bbb9")
  ],
  "slug": "request-a-demo",
  "title": "Request A Demo",
  "body": "<div class=\"center\"><span><iframe allowfullscreen=\"\" frameborder=\"0\" height=\"540\" src=\"https://spark.adobe.com/video/1h2cwm5RqUkIr/embed\" width=\"960\"></iframe></span></div>\r\n\r\n<div formid=\"e4fc10b8-6d57-4028-85f4-ab8d6d85007e\" id=\"hubspotForm\" portalid=\"5543041\"><span><span><span>&nbsp;</span></span></span></div>\r\n",
  "banner_image": "",
  "meta_title": "",
  "meta_description": "",
  "created_at": ISODate("2019-10-14T15:48:09.407Z"),
  "updated_at": ISODate("2019-10-15T07:07:01.582Z"),
  "page_video": null,
  "testimonials": null
});
db.getCollection("static_pages").insert({
  "_id": ObjectId("5da59eeb11ece21ef759694f"),
  "lang_content": [
    ObjectId("5da59eeb11ece21ef7596950"),
    ObjectId("5da59eeb11ece21ef7596952"),
    ObjectId("5da59eeb11ece21ef7596954"),
    ObjectId("5da59eeb11ece21ef7596956")
  ],
  "slug": "find-a-unit-near-me",
  "title": "Find A Unit Near Me",
  "body": "\r\n\r\n<div formid=\"e4fc10b8-6d57-4028-85f4-ab8d6d85007e\" id=\"hubspotForm\" portalid=\"5543041\"><span><span>&nbsp;</span></span></div>\r\n",
  "banner_image": "",
  "meta_title": "",
  "meta_description": "",
  "created_at": ISODate("2019-10-15T10:26:51.679Z"),
  "updated_at": ISODate("2019-10-15T15:28:39.465Z"),
  "page_video": null,
  "testimonials": null
});
db.getCollection("static_pages").insert({
  "_id": ObjectId("5da59ef611ece21ef7596960"),
  "lang_content": [
    ObjectId("5da59ef611ece21ef7596961"),
    ObjectId("5da59ef611ece21ef7596963"),
    ObjectId("5da59ef611ece21ef7596965"),
    ObjectId("5da59ef611ece21ef7596967")
  ],
  "slug": "rent-this-device",
  "title": "Rent This Device",
  "body": "<p>Rent This Device</p>\r\n",
  "banner_image": "",
  "meta_title": "",
  "meta_description": "",
  "created_at": ISODate("2019-10-15T10:27:02.777Z"),
  "updated_at": ISODate("2019-10-15T10:27:02.786Z")
});
db.getCollection("static_pages").insert({
  "_id": ObjectId("5da59f0111ece21ef7596971"),
  "lang_content": [
    ObjectId("5da59f0111ece21ef7596972"),
    ObjectId("5da59f0111ece21ef7596974"),
    ObjectId("5da59f0111ece21ef7596976"),
    ObjectId("5da59f0111ece21ef7596978")
  ],
  "slug": "request-a-demonstration",
  "title": "Request a Demonstration",
  "body": "<p><span><span>Request a Demonstration</span></span></p>\r\n\r\n<div formid=\"e4fc10b8-6d57-4028-85f4-ab8d6d85007e\" id=\"hubspotForm\" portalid=\"5543041\"><span><span><span>&nbsp;</span></span></span></div>\r\n",
  "banner_image": "",
  "meta_title": "",
  "meta_description": "",
  "created_at": ISODate("2019-10-15T10:27:13.239Z"),
  "updated_at": ISODate("2019-10-15T15:31:23.994Z"),
  "page_video": null,
  "testimonials": null
});
db.getCollection("static_pages").insert({
  "_id": ObjectId("5da6f7e211ece21ef75969e6"),
  "lang_content": [
    ObjectId("5da6f7e211ece21ef75969e7"),
    ObjectId("5da6f7e211ece21ef75969e9"),
    ObjectId("5da6f7e211ece21ef75969eb"),
    ObjectId("5da6f7e211ece21ef75969ed")
  ],
  "slug": "service-contact-us",
  "title": "Service Contact ",
  "body": "<div class=\"container\">\r\n<div class=\"col-xs-3\"><br />\r\n<br />\r\n<br />\r\n<br />\r\n<span><span><span><span>Left Data will come here</span></span></span></span></div>\r\n\r\n<div class=\"col-xs-9\">\r\n<div class=\"select_product_outer_heading\">\r\n<h3><span><span><span><span><span><span>Submit a Service Assistance Request Ticket</span></span></span></span></span></span></h3>\r\n</div>\r\n\r\n<div formid=\"ca763588-787c-41e0-a40d-ec8300c532c1\" id=\"hubspotForm\" portalid=\"5543041\"><span><span><span><span><span><span><span><span><span><span><span>&nbsp;</span></span></span></span></span></span></span></span></span></span></span></div>\r\n</div>\r\n</div>\r\n",
  "banner_image": "",
  "meta_title": "",
  "meta_description": "",
  "created_at": ISODate("2019-10-16T10:58:42.54Z"),
  "updated_at": ISODate("2019-11-22T05:50:50.294Z"),
  "page_video": null,
  "testimonials": null
});

/** tags records **/
db.getCollection("tags").insert({
  "_id": ObjectId("5bd16e3cb605151b7e93d87d"),
  "name": "LG400",
  "status": NumberInt(1),
  "created_at": ISODate("2018-10-25T07:18:20.919Z"),
  "updated_at": ISODate("2019-08-06T04:21:08.523Z")
});
db.getCollection("tags").insert({
  "_id": ObjectId("5bd16e42b605151b7e93d881"),
  "name": "LG300",
  "status": NumberInt(1),
  "created_at": ISODate("2018-10-25T07:18:26.45Z"),
  "updated_at": ISODate("2019-08-06T04:20:35.651Z")
});
db.getCollection("tags").insert({
  "_id": ObjectId("5d4c567444b42b41a30ed278"),
  "name": "LiteGait Overground",
  "status": NumberInt(1),
  "created_at": ISODate("2019-08-08T17:05:56.105Z"),
  "updated_at": ISODate("2019-08-08T17:05:56.105Z")
});
db.getCollection("tags").insert({
  "_id": ObjectId("5d4d9051bf4f45672e99b449"),
  "name": "harness application adult",
  "status": NumberInt(1),
  "created_at": ISODate("2019-08-09T15:25:05.369Z"),
  "updated_at": ISODate("2019-08-09T15:25:05.369Z")
});
db.getCollection("tags").insert({
  "_id": ObjectId("5d4d9064bf4f45672e99b44d"),
  "name": "harness application peds",
  "status": NumberInt(1),
  "created_at": ISODate("2019-08-09T15:25:24.22Z"),
  "updated_at": ISODate("2019-08-09T15:25:24.22Z")
});
db.getCollection("tags").insert({
  "_id": ObjectId("5d4d9099bf4f45672e99b451"),
  "name": "Q-pad",
  "status": NumberInt(1),
  "created_at": ISODate("2019-08-09T15:26:17.268Z"),
  "updated_at": ISODate("2019-08-09T15:26:17.268Z")
});
db.getCollection("tags").insert({
  "_id": ObjectId("5d81b3c1e696962cf1b33b43"),
  "name": "Service Check",
  "status": NumberInt(1),
  "created_at": ISODate("2019-09-18T04:34:09.988Z"),
  "updated_at": ISODate("2019-09-18T04:34:09.988Z")
});
db.getCollection("tags").insert({
  "_id": ObjectId("5d81b3e0e696962cf1b33b47"),
  "name": "LiteGait adult",
  "status": NumberInt(1),
  "created_at": ISODate("2019-09-18T04:34:40.805Z"),
  "updated_at": ISODate("2019-09-18T04:34:40.805Z")
});
db.getCollection("tags").insert({
  "_id": ObjectId("5d81b3f1e696962cf1b33b4b"),
  "name": "LiteGait peds",
  "status": NumberInt(1),
  "created_at": ISODate("2019-09-18T04:34:57.507Z"),
  "updated_at": ISODate("2019-09-18T04:34:57.507Z")
});

/** testimonials records **/
db.getCollection("testimonials").insert({
  "_id": ObjectId("5bd16ea2b605151b7e93d894"),
  "name": "kelly",
  "feedback": "Very Good Site. Impress a lot. Very Good Site. Impress a lot.Very Good Site. Impress a lot.",
  "image": "/testimonials/woman-3368245.jpg",
  "status": NumberInt(1),
  "created_at": ISODate("2018-10-25T07:20:02.900Z"),
  "updated_at": ISODate("2019-10-07T15:16:33.289Z"),
  "type": "inservices",
  "facility": "Golden Living- TX",
  "home_page": NumberInt(1)
});
db.getCollection("testimonials").insert({
  "_id": ObjectId("5c36edb38ba4672adbd9402e"),
  "name": "Matt",
  "feedback": "We Impress it with this too much. its help me very much to improve my health.We Impress it with this too much. its help me very much to improve my health.",
  "image": "/webinars/webclass-05.jpg",
  "status": NumberInt(1),
  "created_at": ISODate("2019-01-10T07:01:07.808Z"),
  "updated_at": ISODate("2019-10-04T12:45:50.619Z"),
  "type": "trainings",
  "facility": "HCR Manor Care, TN",
  "home_page": NumberInt(1)
});
db.getCollection("testimonials").insert({
  "_id": ObjectId("5c36ef538ba4672adbd94067"),
  "name": "Jule",
  "feedback": "In promotion and of advertising, a testimonial or show consists of a person's written or spoken statement extolling the virtue of a product. The term \"testimonial\" most commonly applies to the sales-pitches attributed to ordinary citizens.",
  "image": "/testimonial1.jpg",
  "status": NumberInt(1),
  "created_at": ISODate("2019-01-10T07:08:03.477Z"),
  "updated_at": ISODate("2019-10-04T12:45:45.717Z"),
  "type": "products",
  "facility": "JuleF",
  "home_page": NumberInt(1)
});

/** topics records **/
db.getCollection("topics").insert({
  "_id": ObjectId("5d398477667cf16bdd7a0107"),
  "topic": "Adult",
  "slug": "adult",
  "status": NumberInt(1),
  "created_at": ISODate("2019-07-25T10:29:11.624Z"),
  "updated_at": ISODate("2019-09-05T15:29:20.149Z")
});
db.getCollection("topics").insert({
  "_id": ObjectId("5d39848c667cf16bdd7a010f"),
  "topic": "Neuro",
  "slug": "neuro",
  "status": NumberInt(1),
  "created_at": ISODate("2019-07-25T10:29:32.421Z"),
  "updated_at": ISODate("2019-09-05T15:28:40.883Z")
});
db.getCollection("topics").insert({
  "_id": ObjectId("5d7129e23fbef23b14b59601"),
  "topic": "Pediatric",
  "slug": "pediatric",
  "status": NumberInt(1),
  "created_at": ISODate("2019-09-05T15:29:38.721Z"),
  "updated_at": ISODate("2019-09-05T15:29:38.721Z")
});

/** users records **/
db.getCollection("users").insert({
  "_id": ObjectId("5badf16c4883432ea4a93935"),
  "role_id": NumberInt(1),
  "name": "LiteGait Admin",
  "email": "admin@mailinator.com",
  "phone": "09950192509",
  "password": "sha1$88f4b935$1$9bffb42580384fa430c4d3131fce7b8a668c31e5",
  "status": NumberInt(1),
  "created_at": ISODate("2018-09-28T09:16:28.866Z"),
  "updated_at": ISODate("2019-10-11T12:08:16.118Z"),
  "role_permission": "{\"blog\":\"1\",\"category\":\"1\"}"
});
db.getCollection("users").insert({
  "_id": ObjectId("5badf3695115941ea80970ce"),
  "role_id": NumberInt(3),
  "name": "anil yadav",
  "email": "anil.1@mailinator.com",
  "phone": "09950192509",
  "password": "sha1$ed2e058b$1$541cb55444ac8b84acd8b9085abe67a49b91cc54",
  "status": NumberInt(1),
  "created_at": ISODate("2018-09-28T09:24:57.764Z"),
  "updated_at": ISODate("2019-07-23T04:32:23.182Z"),
  "elp_expire": ISODate("2020-07-23T04:32:23.181Z"),
  "elp_id": ObjectId("5cb05e0e2a31271b4165f35b"),
  "elp_payment_id": ObjectId("5d368dd781421734b162fba4"),
  "elp_purchase": ISODate("2019-07-23T04:32:23.181Z")
});
db.getCollection("users").insert({
  "_id": ObjectId("5c60e6618eaaa63ad1fdc477"),
  "role_id": NumberInt(3),
  "name": "Monica Serie",
  "email": "monica@litegait.com",
  "phone": "4808291727",
  "password": "sha1$5b07a7dd$1$d83022478f2c6a9171a9df22e71b274f74089858",
  "status": NumberInt(1),
  "created_at": ISODate("2019-02-11T03:05:05.834Z"),
  "updated_at": ISODate("2019-09-27T05:16:20.738Z"),
  "elp_expire": ISODate("2020-09-27T05:16:20.738Z"),
  "elp_id": ObjectId("5cb05e002a31271b4165f357"),
  "elp_payment_id": null,
  "elp_provider_payment_id": ObjectId("5d368dd781421734b162fba4"),
  "elp_purchase": ISODate("2019-09-27T05:16:20.738Z")
});
db.getCollection("users").insert({
  "_id": ObjectId("5d273333017cd5612df4449f"),
  "role_id": NumberInt(3),
  "name": "testone",
  "email": "testone@mailinator.com",
  "phone": "8761234563",
  "password": "sha1$2b2fa84a$1$b5f18c3f1d58be1e3aac0a2ea65a40078eeb908e",
  "status": NumberInt(1),
  "created_at": ISODate("2019-07-11T13:01:39.216Z"),
  "updated_at": ISODate("2019-08-12T12:38:17.975Z"),
  "elp_expire": ISODate("2020-07-11T13:04:26.128Z"),
  "elp_id": ObjectId("5cb05e0e2a31271b4165f35b"),
  "elp_payment_id": ObjectId("5d2733dacfcece7c92e90423"),
  "elp_purchase": ISODate("2019-07-11T13:04:26.128Z")
});
db.getCollection("users").insert({
  "_id": ObjectId("5d34842eaee8f14fa304844d"),
  "role_id": NumberInt(3),
  "name": "Monica",
  "email": "monserie@cox.net",
  "password": "sha1$0624554d$1$6ec0ceb212ccac200a83b7939ce40e1091587582",
  "status": NumberInt(1),
  "created_at": ISODate("2019-07-21T15:26:38.534Z"),
  "updated_at": ISODate("2019-09-27T17:40:43.17Z"),
  "address": "",
  "disipline": "",
  "facility": "",
  "license_number": "",
  "phone": ""
});
db.getCollection("users").insert({
  "_id": ObjectId("5d368e5881421734b162fbab"),
  "role_id": NumberInt(3),
  "name": "Mohan",
  "email": "anil.2@mailinator.com",
  "phone": "9887231253",
  "password": "sha1$506a9c4d$1$5453e5985c206b42e932aca45e22eab491e38a93",
  "status": NumberInt(1),
  "created_at": ISODate("2019-07-23T04:34:32.205Z"),
  "updated_at": ISODate("2019-07-23T04:35:07.372Z"),
  "elp_expire": ISODate("2020-07-23T04:35:07.372Z"),
  "elp_id": ObjectId("5cb05e002a31271b4165f357"),
  "elp_purchase": ISODate("2019-07-23T04:35:07.372Z")
});
db.getCollection("users").insert({
  "_id": ObjectId("5d3971dc2a02f4259583724a"),
  "role_id": NumberInt(3),
  "name": "testtwo",
  "email": "testtwo@mailinator.com",
  "phone": "8888888123",
  "password": "sha1$b498d784$1$f58ba2b79041af29df6fe4a4fdb5287503a7884b",
  "status": NumberInt(1),
  "created_at": ISODate("2019-07-25T09:09:48.300Z"),
  "updated_at": ISODate("2019-07-25T11:25:25.973Z"),
  "elp_expire": ISODate("2020-07-25T11:25:25.972Z"),
  "elp_id": ObjectId("5cb05e0e2a31271b4165f35b"),
  "elp_payment_id": ObjectId("5d3991a5667cf16bdd7a0161"),
  "elp_provider_payment_id": null,
  "elp_purchase": ISODate("2019-07-25T11:25:25.972Z")
});
db.getCollection("users").insert({
  "_id": ObjectId("5d39921e667cf16bdd7a0163"),
  "role_id": NumberInt(3),
  "name": "testthree",
  "email": "testthree@mailinator.com",
  "phone": "7777123123",
  "password": "sha1$b9641e0b$1$d2f3a5b2f0417709a1efb659a3fc7555f0377766",
  "status": NumberInt(1),
  "created_at": ISODate("2019-07-25T11:27:26.643Z"),
  "updated_at": ISODate("2019-07-25T11:59:47.289Z"),
  "elp_expire": ISODate("2020-07-25T11:59:47.289Z"),
  "elp_id": ObjectId("5cb05e002a31271b4165f357"),
  "elp_payment_id": null,
  "elp_provider_payment_id": ObjectId("5d3991a5667cf16bdd7a0161"),
  "elp_purchase": ISODate("2019-07-25T11:59:47.289Z")
});
db.getCollection("users").insert({
  "_id": ObjectId("5d399c94c893b821ec976b99"),
  "role_id": NumberInt(3),
  "name": "Krishan",
  "email": "anil.3@mailinator.com",
  "phone": "9950192509",
  "password": "sha1$7f74887e$1$c084187815d7d8a41b0b803cd9049e4ba602cd9e",
  "status": NumberInt(1),
  "created_at": ISODate("2019-07-25T12:12:04.52Z"),
  "updated_at": ISODate("2019-08-13T03:57:30.3Z"),
  "elp_expire": ISODate("2020-08-13T03:57:30.3Z"),
  "elp_id": ObjectId("5cb05e002a31271b4165f357"),
  "elp_payment_id": null,
  "elp_provider_payment_id": ObjectId("5d368dd781421734b162fba4"),
  "elp_purchase": ISODate("2019-08-13T03:57:30.3Z")
});
db.getCollection("users").insert({
  "_id": ObjectId("5d39de103f139a23a9e72ab0"),
  "role_id": NumberInt(3),
  "name": "Guest",
  "email": "litegaitmonica@gmail.com",
  "password": "sha1$6c97b829$1$61f5662384435058e22689c02c0c054378df21b4",
  "status": NumberInt(1),
  "created_at": ISODate("2019-07-25T16:51:28.615Z"),
  "updated_at": ISODate("2019-07-25T16:51:28.615Z")
});
db.getCollection("users").insert({
  "_id": ObjectId("5da06f657a580a0f7a021e16"),
  "role_id": NumberInt(2),
  "name": "Sub Admin",
  "email": "subadmin@mailinator.com",
  "phone": "123456",
  "password": "sha1$9c0397b6$1$46cee0c5ad6fdee55eb06986197e8b74b3b29baa",
  "address": "",
  "facility": "",
  "license_number": "",
  "disipline": "",
  "status": NumberInt(1),
  "created_at": ISODate("2019-10-11T12:02:45.652Z"),
  "updated_at": ISODate("2019-10-11T12:02:45.652Z")
});

/** webinar_contents records **/
db.getCollection("webinar_contents").insert({
  "_id": ObjectId("5c4027e1521f740b1cd20023"),
  "webinar_id": ObjectId("5c4027e1521f740b1cd20022"),
  "lang": "en",
  "description": "<p>Health awairness programHealth awairness programHealth awairness programHealth awairness programHealth awairness programHealth awairness programHealth awairness program</p>\r\n",
  "objective": "<p>no</p>\r\n",
  "created_at": ISODate("2019-01-17T06:59:45.82Z"),
  "updated_at": ISODate("2019-10-08T17:21:33.479Z"),
  "topics": null
});
db.getCollection("webinar_contents").insert({
  "_id": ObjectId("5c4027e1521f740b1cd20025"),
  "webinar_id": ObjectId("5c4027e1521f740b1cd20022"),
  "lang": "fr",
  "description": "<p>French&nbsp;Health awairness program.&nbsp;French&nbsp;Health awairness program.French&nbsp;Health awairness program.French&nbsp;Health awairness program.French&nbsp;Health awairness program.French&nbsp;Health awairness program.French&nbsp;Health awairness program.French&nbsp;Health awairness program.French&nbsp;Health awairness program.French&nbsp;Health awairness program.</p>\r\n",
  "objective": "<p>French&nbsp;To improve your health awareness.&nbsp;To improve your health awareness.&nbsp;French&nbsp;To improve your health awareness.&nbsp;To improve your health awareness.&nbsp;French&nbsp;To improve your health awareness.&nbsp;To improve your health awareness.&nbsp;French&nbsp;To improve your health awareness.&nbsp;To improve your health awareness.&nbsp;French&nbsp;To improve your health awareness.&nbsp;To improve your health awareness.&nbsp;</p>\r\n",
  "created_at": ISODate("2019-01-17T06:59:45.125Z"),
  "updated_at": ISODate("2019-10-08T17:21:33.479Z"),
  "topics": null
});
db.getCollection("webinar_contents").insert({
  "_id": ObjectId("5c86557636c12022fc64521b"),
  "webinar_id": ObjectId("5c86557636c12022fc64521a"),
  "lang": "en",
  "description": "<p>Php WebinarPhp WebinarPhp WebinarPhp WebinarPhp Webinar</p>\r\n",
  "objective": "<p>Php WebinarPhp WebinarPhp WebinarPhp WebinarPhp WebinarPhp Webinar</p>\r\n",
  "created_at": ISODate("2019-03-11T12:32:54.111Z"),
  "updated_at": ISODate("2019-09-23T04:31:33.378Z"),
  "topics": null
});
db.getCollection("webinar_contents").insert({
  "_id": ObjectId("5c86557636c12022fc64521d"),
  "webinar_id": ObjectId("5c86557636c12022fc64521a"),
  "lang": "fr",
  "description": "",
  "objective": "",
  "created_at": ISODate("2019-03-11T12:32:54.119Z"),
  "updated_at": ISODate("2019-09-23T04:31:33.378Z"),
  "topics": null
});
db.getCollection("webinar_contents").insert({
  "_id": ObjectId("5c8776632f029d24a8f06321"),
  "webinar_id": ObjectId("5c8776632f029d24a8f06320"),
  "lang": "en",
  "description": "<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n",
  "objective": "<p>About New Generation</p>\r\n",
  "created_at": ISODate("2019-03-12T09:05:39.379Z"),
  "updated_at": ISODate("2019-10-08T17:34:07.520Z"),
  "topics": null
});
db.getCollection("webinar_contents").insert({
  "_id": ObjectId("5c8776632f029d24a8f06323"),
  "webinar_id": ObjectId("5c8776632f029d24a8f06320"),
  "lang": "fr",
  "description": "",
  "objective": "",
  "created_at": ISODate("2019-03-12T09:05:39.387Z"),
  "updated_at": ISODate("2019-10-08T17:34:07.520Z"),
  "topics": null
});
db.getCollection("webinar_contents").insert({
  "_id": ObjectId("5c88d08d36e25e2118009acc"),
  "webinar_id": ObjectId("5c88d08d36e25e2118009acb"),
  "lang": "en",
  "description": "<p>It might seem contradictory, but putting physical stress on your body through exercise can relieve mental stress.</p>\r\n\r\n<p>The benefits are strongest when you exercise regularly. People who exercise regularly are less likely to experience anxiety than those who don&#39;t exercise (<a href=\"http://www.sciencedirect.com/science/article/pii/S0091743505002331\" target=\"_blank\">1</a>).</p>\r\n\r\n<p>There are a few reasons behind this:</p>\r\n\r\n<ul>\r\n\t<li><strong>Stress hormones:</strong>&nbsp;Exercise lowers your body&#39;s stress hormones &mdash; such as cortisol &mdash; in the long run. It also helps release endorphins, which are chemicals that improve your mood and act as natural painkillers.</li>\r\n\t<li><strong>Sleep:</strong>&nbsp;Exercise can also&nbsp;<a href=\"https://www.healthline.com/nutrition/17-tips-to-sleep-better\">improve your sleep quality</a>, which can be negatively affected by stress and anxiety.</li>\r\n\t<li><strong>Confidence:</strong>&nbsp;When you exercise regularly, you may feel more competent and confident in your body, which in turn promotes mental wellbeing.</li>\r\n</ul>\r\n\r\n<p>Try to find an&nbsp;<a href=\"https://www.healthline.com/nutrition/how-to-start-exercising\">exercise routine</a>&nbsp;or activity you enjoy, such as walking, dancing, rock climbing or yoga.</p>\r\n\r\n<p>Activities &mdash; such as walking or jogging &mdash; that involve repetitive movements of large muscle groups can be particularly stress relieving.</p>\r\n",
  "objective": "<p>Several supplements promote stress and anxiety reduction. Here is a brief overview of some of the most common ones:</p>\r\n\r\n<ul>\r\n\t<li><strong>Lemon balm:</strong>&nbsp;Lemon balm is a member of the mint family that has been studied for its anti-anxiety effects (<a href=\"https://www.ncbi.nlm.nih.gov/pubmed/19865069\" target=\"_blank\">2</a>).</li>\r\n\t<li><strong>Omega-3 fatty acids:</strong>&nbsp;One study showed that medical students who received&nbsp;<a href=\"https://www.healthline.com/nutrition/omega-3-supplement-guide\">omega-3 supplements</a>&nbsp;experienced a 20% reduction in anxiety symptoms (<a href=\"https://www.ncbi.nlm.nih.gov/pubmed/21784145\" target=\"_blank\">3</a>).</li>\r\n\t<li><strong>Ashwagandha:</strong>&nbsp;<a href=\"https://www.healthline.com/nutrition/12-proven-ashwagandha-benefits\">Ashwagandha</a>&nbsp;is an herb used in Ayurvedic medicine to treat stress and anxiety. Several studies suggest that it&#39;s effective (<a href=\"https://www.ncbi.nlm.nih.gov/pubmed/25405876\" target=\"_blank\">4</a>).</li>\r\n\t<li><strong>Green tea:</strong>&nbsp;<a href=\"https://www.healthline.com/nutrition/top-10-evidence-based-health-benefits-of-green-tea\">Green tea</a>&nbsp;contains many polyphenol antioxidants which provide health benefits. It may lower stress and anxiety by increasing serotonin levels (<a href=\"https://www.ncbi.nlm.nih.gov/pubmed/23625424\" target=\"_blank\">5</a>).</li>\r\n\t<li><strong>Valerian:</strong>&nbsp;Valerian root is a popular sleep aid due to its tranquilizing effect. It contains valerenic acid, which alters gamma-aminobutyric acid (GABA) receptors to lower anxiety.</li>\r\n\t<li><strong>Kava kava:</strong>&nbsp;Kava kava is a psychoactive member of the pepper family. Long used as a sedative in the South Pacific, it is increasingly used in Europe and the US to treat mild stress and anxiety (<a href=\"https://www.ncbi.nlm.nih.gov/pubmed/12672148\" target=\"_blank\">6</a>).</li>\r\n</ul>\r\n\r\n<p>Some supplements can interact with medications or have side effects, so you may want to consult with a doctor if you have a medical condition.</p>\r\n",
  "created_at": ISODate("2019-03-13T09:42:37.79Z"),
  "updated_at": ISODate("2019-09-05T12:44:35.716Z"),
  "topics": null
});
db.getCollection("webinar_contents").insert({
  "_id": ObjectId("5c88d08d36e25e2118009ace"),
  "webinar_id": ObjectId("5c88d08d36e25e2118009acb"),
  "lang": "fr",
  "description": "",
  "objective": "",
  "created_at": ISODate("2019-03-13T09:42:37.133Z"),
  "updated_at": ISODate("2019-09-05T12:44:35.716Z"),
  "topics": null
});
db.getCollection("webinar_contents").insert({
  "_id": ObjectId("5d9c155e7dbdaa69d72e5c78"),
  "webinar_id": ObjectId("5d9c155e7dbdaa69d72e5c77"),
  "lang": "en",
  "description": "<p><strong>Back On Track:&nbsp; Implementation of a multi-system approach to re-start developmental progress in a young child with DS post remission from Leukemia: A Case Report.</strong></p>\r\n\r\n<p>Children diagnosed with Down Syndrome are expected to achieve developmental milestones at delayed intervals with trajectories that are well documented in the literature. However, when a young child with DS is then diagnosed with and treated for leukemia, the developmental expectations are more ambiguous. In this case study we will discuss a 5.8-year-old girl diagnosed with DS and post remission for leukemia whose developmental progress was stalled before the age of 2, but is now back on track at the age of 5 following the implementation of a multi-system approach.</p>\r\n",
  "objective": "",
  "created_at": ISODate("2019-10-08T04:49:34.817Z"),
  "updated_at": ISODate("2019-11-21T16:16:24.675Z"),
  "topics": null
});
db.getCollection("webinar_contents").insert({
  "_id": ObjectId("5d9c155e7dbdaa69d72e5c7a"),
  "webinar_id": ObjectId("5d9c155e7dbdaa69d72e5c77"),
  "lang": "fr",
  "description": "",
  "objective": "",
  "created_at": ISODate("2019-10-08T04:49:34.826Z"),
  "updated_at": ISODate("2019-11-21T16:16:24.675Z"),
  "topics": null
});
db.getCollection("webinar_contents").insert({
  "_id": ObjectId("5d9c155e7dbdaa69d72e5c7c"),
  "webinar_id": ObjectId("5d9c155e7dbdaa69d72e5c77"),
  "lang": "Gr",
  "description": "",
  "objective": "",
  "created_at": ISODate("2019-10-08T04:49:34.833Z"),
  "updated_at": ISODate("2019-11-21T16:16:24.675Z"),
  "topics": null
});
db.getCollection("webinar_contents").insert({
  "_id": ObjectId("5d9c155e7dbdaa69d72e5c7e"),
  "webinar_id": ObjectId("5d9c155e7dbdaa69d72e5c77"),
  "lang": "Sp",
  "description": "",
  "objective": "",
  "created_at": ISODate("2019-10-08T04:49:34.841Z"),
  "updated_at": ISODate("2019-11-21T16:16:24.675Z"),
  "topics": null
});

/** webinars records **/
db.getCollection("webinars").insert({
  "_id": ObjectId("5c4027e1521f740b1cd20022"),
  "lang_content": [
    ObjectId("5c4027e1521f740b1cd20023"),
    ObjectId("5c4027e1521f740b1cd20025")
  ],
  "title": "Health awairness program",
  "slug": "health-awairness-program",
  "type": "webinar",
  "description": "<p>Health awairness programHealth awairness programHealth awairness programHealth awairness programHealth awairness programHealth awairness programHealth awairness program</p>\r\n",
  "objective": "<p>no</p>\r\n",
  "states": [
    "CA",
    "AZ"
  ],
  "pre_requisite": "none",
  "education_level": "secondary",
  "price": NumberInt(0),
  "date_time": ISODate("2019-01-30T05:30:00.0Z"),
  "location": "Pratap Nagar Jaipur",
  "contact_hours": "1.5",
  "meta_title": "Health awairness program",
  "meta_description": "Health awairness program desc",
  "status": NumberInt(1),
  "created_at": ISODate("2019-01-17T06:59:45.76Z"),
  "updated_at": ISODate("2019-10-08T17:21:33.469Z"),
  "date_time_1": ISODate("2019-10-30T19:30:00.0Z"),
  "date_time_2": ISODate("2019-10-31T13:30:00.0Z"),
  "url": "http://www.selfridges.com/GB/en/cat/womens/shoes/boots/?cm_sp=MegaMenu-_-Women-_-Shoes-Boots",
  "speaker_id": [
    ObjectId("5d696ef1e401c22327431157")
  ],
  "category": ObjectId("5c88fdde8592290d03e200fa"),
  "topics": [
    "Adult"
  ],
  "recorded_url": null,
  "state_msg": "{\"CA\":\"\",\"AZ\":\"\"}",
  "image": "/service_circle1.jpg",
  "testimonials": null
});
db.getCollection("webinars").insert({
  "_id": ObjectId("5c86557636c12022fc64521a"),
  "speaker_id": [
    ObjectId("5cd4bb0906e67e44fa0eb3ae")
  ],
  "lang_content": [
    ObjectId("5c86557636c12022fc64521b"),
    ObjectId("5c86557636c12022fc64521d")
  ],
  "title": "Php Webinar",
  "slug": "php-webinar",
  "description": "<p>Php WebinarPhp WebinarPhp WebinarPhp WebinarPhp Webinar</p>\r\n",
  "objective": "<p>Php WebinarPhp WebinarPhp WebinarPhp WebinarPhp WebinarPhp Webinar</p>\r\n",
  "states": [
    "CA",
    "AZ"
  ],
  "pre_requisite": "should be educated",
  "education_level": "secondary",
  "price": NumberInt(3),
  "date_time_1": ISODate("2019-10-03T07:30:00.0Z"),
  "date_time_2": ISODate("2019-10-04T13:30:00.0Z"),
  "url": "http://www.selfridges.com/GB/en/cat/womens/shoes/boots/?cm_sp=MegaMenu-_-Women-_-Shoes-Boots",
  "location": "Malviya Nagar Jaipur",
  "contact_hours": "0.1",
  "meta_title": "",
  "meta_description": "",
  "status": NumberInt(1),
  "created_at": ISODate("2019-03-11T12:32:54.88Z"),
  "updated_at": ISODate("2019-09-23T04:31:33.372Z"),
  "category": ObjectId("5c88fdde8592290d03e200fa"),
  "topics": [
    "Adult",
    "Neuro"
  ],
  "recorded_url": null,
  "image": "/webinars/webclass-06.jpg",
  "state_msg": "{\"CA\":\"\",\"AZ\":\"\"}",
  "testimonials": null
});
db.getCollection("webinars").insert({
  "_id": ObjectId("5c8776632f029d24a8f06320"),
  "speaker_id": [
    ObjectId("5c2f06ca31fdef2dc05a51e8")
  ],
  "lang_content": [
    ObjectId("5c8776632f029d24a8f06321"),
    ObjectId("5c8776632f029d24a8f06323")
  ],
  "title": "About New Generation",
  "slug": "about-new-generation",
  "description": "<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n",
  "objective": "<p>About New Generation</p>\r\n",
  "states": [
    "CA",
    "AZ",
    "PA"
  ],
  "pre_requisite": "should be educated",
  "education_level": "secondary",
  "price": NumberInt(25),
  "date_time_1": ISODate("2019-09-30T08:30:00.0Z"),
  "date_time_2": ISODate("2019-09-30T11:30:00.0Z"),
  "url": "https://mobilityresearch.adobeconnect.com/p40c72sg5gas/",
  "contact_hours": "1.5",
  "meta_title": "",
  "meta_description": "",
  "status": NumberInt(1),
  "created_at": ISODate("2019-03-12T09:05:39.374Z"),
  "updated_at": ISODate("2019-10-08T17:34:07.515Z"),
  "category": ObjectId("5c6c5e15092aa62529ff4cee"),
  "topics": [
    "Neuro"
  ],
  "recorded_url": null,
  "state_msg": "{\"CA\":\"\",\"AZ\":\"\",\"PA\":\"\"}",
  "image": "/webinars/webclass-03.jpg",
  "testimonials": null
});
db.getCollection("webinars").insert({
  "_id": ObjectId("5c88d08d36e25e2118009acb"),
  "speaker_id": [
    ObjectId("5c7cd37bfdbb9436784ca0fb")
  ],
  "lang_content": [
    ObjectId("5c88d08d36e25e2118009acc"),
    ObjectId("5c88d08d36e25e2118009ace")
  ],
  "title": "Remove Stress",
  "slug": "remove-stress",
  "category": ObjectId("5c6c5e15092aa62529ff4cee"),
  "description": "<p>It might seem contradictory, but putting physical stress on your body through exercise can relieve mental stress.</p>\r\n\r\n<p>The benefits are strongest when you exercise regularly. People who exercise regularly are less likely to experience anxiety than those who don&#39;t exercise (<a href=\"http://www.sciencedirect.com/science/article/pii/S0091743505002331\" target=\"_blank\">1</a>).</p>\r\n\r\n<p>There are a few reasons behind this:</p>\r\n\r\n<ul>\r\n\t<li><strong>Stress hormones:</strong>&nbsp;Exercise lowers your body&#39;s stress hormones &mdash; such as cortisol &mdash; in the long run. It also helps release endorphins, which are chemicals that improve your mood and act as natural painkillers.</li>\r\n\t<li><strong>Sleep:</strong>&nbsp;Exercise can also&nbsp;<a href=\"https://www.healthline.com/nutrition/17-tips-to-sleep-better\">improve your sleep quality</a>, which can be negatively affected by stress and anxiety.</li>\r\n\t<li><strong>Confidence:</strong>&nbsp;When you exercise regularly, you may feel more competent and confident in your body, which in turn promotes mental wellbeing.</li>\r\n</ul>\r\n\r\n<p>Try to find an&nbsp;<a href=\"https://www.healthline.com/nutrition/how-to-start-exercising\">exercise routine</a>&nbsp;or activity you enjoy, such as walking, dancing, rock climbing or yoga.</p>\r\n\r\n<p>Activities &mdash; such as walking or jogging &mdash; that involve repetitive movements of large muscle groups can be particularly stress relieving.</p>\r\n",
  "objective": "<p>Several supplements promote stress and anxiety reduction. Here is a brief overview of some of the most common ones:</p>\r\n\r\n<ul>\r\n\t<li><strong>Lemon balm:</strong>&nbsp;Lemon balm is a member of the mint family that has been studied for its anti-anxiety effects (<a href=\"https://www.ncbi.nlm.nih.gov/pubmed/19865069\" target=\"_blank\">2</a>).</li>\r\n\t<li><strong>Omega-3 fatty acids:</strong>&nbsp;One study showed that medical students who received&nbsp;<a href=\"https://www.healthline.com/nutrition/omega-3-supplement-guide\">omega-3 supplements</a>&nbsp;experienced a 20% reduction in anxiety symptoms (<a href=\"https://www.ncbi.nlm.nih.gov/pubmed/21784145\" target=\"_blank\">3</a>).</li>\r\n\t<li><strong>Ashwagandha:</strong>&nbsp;<a href=\"https://www.healthline.com/nutrition/12-proven-ashwagandha-benefits\">Ashwagandha</a>&nbsp;is an herb used in Ayurvedic medicine to treat stress and anxiety. Several studies suggest that it&#39;s effective (<a href=\"https://www.ncbi.nlm.nih.gov/pubmed/25405876\" target=\"_blank\">4</a>).</li>\r\n\t<li><strong>Green tea:</strong>&nbsp;<a href=\"https://www.healthline.com/nutrition/top-10-evidence-based-health-benefits-of-green-tea\">Green tea</a>&nbsp;contains many polyphenol antioxidants which provide health benefits. It may lower stress and anxiety by increasing serotonin levels (<a href=\"https://www.ncbi.nlm.nih.gov/pubmed/23625424\" target=\"_blank\">5</a>).</li>\r\n\t<li><strong>Valerian:</strong>&nbsp;Valerian root is a popular sleep aid due to its tranquilizing effect. It contains valerenic acid, which alters gamma-aminobutyric acid (GABA) receptors to lower anxiety.</li>\r\n\t<li><strong>Kava kava:</strong>&nbsp;Kava kava is a psychoactive member of the pepper family. Long used as a sedative in the South Pacific, it is increasingly used in Europe and the US to treat mild stress and anxiety (<a href=\"https://www.ncbi.nlm.nih.gov/pubmed/12672148\" target=\"_blank\">6</a>).</li>\r\n</ul>\r\n\r\n<p>Some supplements can interact with medications or have side effects, so you may want to consult with a doctor if you have a medical condition.</p>\r\n",
  "states": [
    "CA",
    "SA"
  ],
  "pre_requisite": "None",
  "education_level": "secondary",
  "price": NumberInt(2),
  "date_time_1": ISODate("2019-08-20T09:30:00.0Z"),
  "date_time_2": ISODate("2019-08-20T10:30:00.0Z"),
  "url": "https://www.mybag.com/women/bags/backpacks.list?productsPerPage=66",
  "contact_hours": "1.5",
  "meta_title": "",
  "meta_description": "",
  "status": NumberInt(1),
  "created_at": ISODate("2019-03-13T09:42:37.42Z"),
  "updated_at": ISODate("2019-09-05T12:44:35.711Z"),
  "topics": [
    "Health Awareness"
  ],
  "recorded_url": null,
  "image": "/gallery11.jpg",
  "state_msg": "{\"CA\":\"\",\"SA\":\"\"}",
  "testimonials": null
});
db.getCollection("webinars").insert({
  "_id": ObjectId("5d9c155e7dbdaa69d72e5c77"),
  "speaker_id": [
    ObjectId("5d696ff9e401c2232743116a")
  ],
  "topics": [
    "Pediatric"
  ],
  "lang_content": [
    ObjectId("5d9c155e7dbdaa69d72e5c78"),
    ObjectId("5d9c155e7dbdaa69d72e5c7a"),
    ObjectId("5d9c155e7dbdaa69d72e5c7c"),
    ObjectId("5d9c155e7dbdaa69d72e5c7e")
  ],
  "states": [
    "California",
    "Ohio"
  ],
  "title": "Case Report 2019-08",
  "slug": "case-report-2019-08",
  "category": ObjectId("5c88fdde8592290d03e200fa"),
  "image": "/webinars/webclass-07.jpg",
  "description": "<p><strong>Back On Track:&nbsp; Implementation of a multi-system approach to re-start developmental progress in a young child with DS post remission from Leukemia: A Case Report.</strong></p>\r\n\r\n<p>Children diagnosed with Down Syndrome are expected to achieve developmental milestones at delayed intervals with trajectories that are well documented in the literature. However, when a young child with DS is then diagnosed with and treated for leukemia, the developmental expectations are more ambiguous. In this case study we will discuss a 5.8-year-old girl diagnosed with DS and post remission for leukemia whose developmental progress was stalled before the age of 2, but is now back on track at the age of 5 following the implementation of a multi-system approach.</p>\r\n",
  "objective": "",
  "pre_requisite": "n",
  "education_level": "",
  "price": NumberInt(5),
  "date_time_1": ISODate("2019-10-23T19:30:00.0Z"),
  "date_time_2": ISODate("2019-10-24T01:30:00.0Z"),
  "url": "",
  "contact_hours": ".05",
  "meta_title": "",
  "meta_description": "",
  "status": NumberInt(1),
  "state_msg": "{\"California\":\"This course has been approved by CERS, an approval agency for the California Board of Physical Therapy or 1.5 hours of continuing education through 11/06/2020. Approval CERS #19-30633-2\",\"Ohio\":\"Ohio Physical Therapy Association (OPTA) for 1.5 hours through 8/21/2020 - Approval # 19S1430\"}",
  "created_at": ISODate("2019-10-08T04:49:34.785Z"),
  "updated_at": ISODate("2019-11-21T16:16:24.667Z"),
  "recorded_url": null,
  "testimonials": null
});
