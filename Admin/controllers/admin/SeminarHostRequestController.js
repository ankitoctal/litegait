var SeminarRequest = include("models/SeminarRequest.js");

exports.list = function(req, res) {
   	var query = SeminarRequest.find().populate({ path: 'seminar_id', select: 'title' });    
    query.sort({ created_at: -1 });
    query.exec(function (err, result) {
    	console.log(result);    	
  		res.render('admin/seminarhostrequest/list',{ title: 'Seminar Host Request List',requests:result});       	
	}); 
	 	    
};


exports.delete = function(req, res) {	
	SeminarRequest.remove({_id:req.params.id}).exec();
	req.flash('success', 'Seminar Host Request Deleted Successfully');
	res.redirect('/admin/seminar_host_request/list'); 
    
};

exports.view = function(req, res) {	
    var query = SeminarRequest.findOne({'_id' : req.params.id}).populate({ path: 'seminar_id', select: 'title' });    
	query.exec(function (err, result) {		
		res.render('admin/seminarhostrequest/view',{ title: 'View Seminar Host Request',reqBody:result});       	
	});  
     
};

exports.status = function(req, res) {	
	var request = {};					
	request.status = req.params.status;			
	SeminarRequest.findByIdAndUpdate( req.params.id, request , function (err) {
		req.flash('success', 'Seminar Host Request Status Changed Successfully');
		res.redirect('/admin/seminar_host_request/list');
	});	
    
};
