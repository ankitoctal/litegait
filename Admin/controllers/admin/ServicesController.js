var Service = include("models/Service.js");

exports.list = function(req, res) {
   	var query = Service.find().populate({ path: 'product_id', select: 'name' }).populate({ path: 'part_id', select: 'name' });    
    query.sort({ created_at: -1 });
    query.exec(function (err, result) {    	
  		res.render('admin/services/list',{ title: 'Service Request List',requests:result});       	
	}); 
	 	    
};


exports.delete = function(req, res) {	
	Service.remove({_id:req.params.id}).exec();
	req.flash('success', 'Service Request Deleted Successfully');
	res.redirect('/admin/services/list'); 
    
};

exports.view = function(req, res) {	
    var query = Service.findOne({'_id' : req.params.id}).populate({ path: 'product_id', select: 'name' }).populate({ path: 'part_id', select: 'name' }); 
	query.exec(function (err, result) {		
		res.render('admin/services/view',{ title: 'View Request',reqBody:result});       	
	});  
     
};

exports.status = function(req, res) {	
	var request = {};					
	request.status = req.params.status;			
	Service.findByIdAndUpdate( req.params.id, request , function (err) {
		req.flash('success', 'Service Request Status Changed Successfully');
		res.redirect('/admin/services/list');
	});	
    
};
