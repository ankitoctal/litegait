var Menu = include("models/Menu.js");
var StaticPage = include("models/StaticPage.js");
var Category = include("models/Category.js");
var Product = include("models/Product.js");

exports.list = async function(req, res) {
	var menu = new Menu();				
	var menus = await menu.getAllNestedMenu(req.params.menuname);		

   	res.render('admin/menu/list',{ title: 'Menu List ('+req.params.menuname+')','menuname':req.params.menuname,menus:menus});       	
	
	 	    
};

exports.add = async function(req, res) {

	var menu = new Menu();				
	var menus = await menu.getAllNestedMenu(req.params.menuname);
	var category = new Category();			
	var categories = await category.getAllNestedCategory();	
	var static_pages = await StaticPage.find().select('_id title').exec();
	var products = await Product.find().select('_id name').exec();	

    if(req.method == 'POST')
    {
    	req.checkBody('type', 'Type is required').notEmpty();
    	req.checkBody('name', 'Name is required').notEmpty();
    	if(req.body.type=='page')		
    		req.checkBody('page_id', 'Page is required').notEmpty();    	
    	else if(req.body.type=='category')		
    		req.checkBody('category_id', 'Category is required').notEmpty(); 
    	else if(req.body.type=='product')		
    		req.checkBody('product_id', 'Product is required').notEmpty();
    	else if(req.body.type=='static_url')		
    		req.checkBody('static_url', 'Static URL is required').notEmpty();		   	

		var errors = req.validationErrors();
		if(errors){						
			res.render('admin/menu/add', {title: 'Add Category','menuname':req.params.menuname, errors: errors,categories:categories,static_pages:static_pages,products:products,menus:menus,reqBody:req.body});			  		
		}
		else
		{
			var menu = new Menu();
			menu.menuname = req.params.menuname;		
			menu.type =req.body.type;
			menu.name =req.body.name;			
			menu.parent_id = 0;	
			menu.page_id =req.body.page_id;
			menu.category_id =req.body.category_id;
			menu.product_id =req.body.product_id;
			menu.static_url =req.body.static_url;			
			if(typeof req.body.parent_id !== "undefined" && req.body.parent_id !="")
			{
				menu.parent_id =req.body.parent_id;	
			}			
			menu.status = 1;
			menu.save(function (err) {				
				if(err)
				{
					req.flash('success', err.message);
					res.render('admin/menu/add', {title: 'Add Menu','menuname':req.params.menuname,categories:categories,static_pages:static_pages,products:products,menus:menus, reqBody:req.body});			  		
				}
				else
				{
					req.flash('success', 'Menu Added Successfully');			
    				return res.redirect('/admin/menu/list/' + req.params.menuname);	
				}			
					  			
			});  		
		}
    }
    else
    {
    	res.render('admin/menu/add',{ title: 'Add Menu','menuname':req.params.menuname,categories:categories,static_pages:static_pages,products:products,menus:menus});
    }
     
};

exports.edit = async function(req, res) {
	
	var cmenu = await Menu.findOne({'_id' : req.params.id});
	var menu = new Menu();			
	var menus = await menu.getAllNestedMenu(cmenu.menuname);
	var category = new Category();			
	var categories = await category.getAllNestedCategory();	
	var static_pages = await StaticPage.find().select('_id title').exec();
	var products = await Product.find().select('_id name').exec();	

    if(req.method == 'POST')
    {
    	req.checkBody('type', 'Type is required').notEmpty();
    	req.checkBody('name', 'Name is required').notEmpty();
    	if(req.body.type=='page')		
    		req.checkBody('page_id', 'Page is required').notEmpty();    	
    	else if(req.body.type=='category')		
    		req.checkBody('category_id', 'Category is required').notEmpty(); 
    	else if(req.body.type=='product')		
    		req.checkBody('product_id', 'Product is required').notEmpty();
    	else if(req.body.type=='static_url')		
    		req.checkBody('static_url', 'Static URL is required').notEmpty();

		var errors = req.validationErrors();
		if(errors){						
			res.render('admin/menu/edit', {title: 'Edit Category','menuname':cmenu.menuname, errors: errors,categories:categories,static_pages:static_pages,products:products,menus:menus,reqBody:req.body});			  		
		}
		else
		{			
			var menu = {};										
			menu.type =req.body.type;
			menu.name =req.body.name;			
			menu.parent_id = 0;	
			menu.page_id =req.body.page_id;
			menu.category_id =req.body.category_id;
			menu.product_id =req.body.product_id;
			menu.static_url =req.body.static_url;			
			if(typeof req.body.parent_id !== "undefined" && req.body.parent_id !="")
			{
				menu.parent_id =req.body.parent_id;	
			}										
			Menu.findByIdAndUpdate( req.params.id, menu , function (err) {				
				if(err)
				{
					req.flash('success', err.message);
					res.render('admin/menu/edit', {title: 'Edit Menu','menuname':cmenu.menuname,categories:categories,static_pages:static_pages,products:products,menus:menus, reqBody:req.body});			  		
				}
				else
				{
					req.flash('success', 'Menu Updated Successfully');			
    				return res.redirect('/admin/menu/list/' + cmenu.menuname);	
				}			
					  			
			});  		
		}
    }
    else
    {
    	var query = Menu.findOne({'_id' : req.params.id});        	
    	query.exec(function (err, result) {
  			res.render('admin/menu/edit',{ title: 'Edit Menu','menuname':result.menuname,categories:categories,static_pages:static_pages,products:products,menus:menus ,reqBody:result});       	
		});
    	
    }
     
};

exports.delete = function(req, res) {	
	var query = Menu.findOne({'_id' : req.params.id});        	
    query.exec(function (err, result) {
    	Menu.remove({_id:req.params.id}).exec();
		req.flash('success', 'Menu Deleted Successfully');
		res.redirect('/admin/menu/list/'+result.menuname);
    });	
    
};


exports.sorting = async function(req, res) {	
	
	var data  = req.body.data;

	for(val in data){
		var id = data[val]['id'];
		var menu = {};					
		menu.parent_id = 0;
		menu.weight = val;
		await Menu.findByIdAndUpdate( id, menu);

		for(i in data[val]['children']){			
			var id2 = data[val]['children'][i]['id'];
			var menu = {};					
			menu.parent_id = id;
			menu.weight = i;
			await Menu.findByIdAndUpdate( id2, menu);

			for(j in data[val]['children'][i]['children']){			
				var id3 = data[val]['children'][i]['children'][j]['id'];
				var menu = {};					
				menu.parent_id = id2;
				menu.weight = j;
				await Menu.findByIdAndUpdate( id3, menu);

			}

		}
	}	

	res.send(req.body);
	/*var menu = {};					
	menu.status = req.params.status;			
	Menu.findByIdAndUpdate( req.params.id, menu , function (err) {
		req.flash('success', 'Menu Status Changed Successfully');
		res.redirect('/admin/menu/list');
	});	 */
    
};
