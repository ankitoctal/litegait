var State = include("models/State.js");

exports.list = function(req, res) {
   	var query = State.find();    
    query.sort({ created_at: -1 });
    query.exec(function (err, result) {
  		res.render('admin/states/list',{ title: 'States List',states:result});       	
	}); 
	 	    
};

exports.add = function(req, res) {	
    if(req.method == 'POST')
    {
    	req.checkBody('name', 'Name is required').notEmpty();   		
		var errors = req.validationErrors();
		if(errors){						
			res.render('admin/states/add', {title: 'Add State', errors: errors,reqBody:req.body});			  		
		}
		else
		{
			var state = new State();		
			state.name =req.body.name;
			state.default_message = req.body.default_message; 
			state.status = 1;
			state.save(function (err) {				
				if(err)
				{
					req.flash('success', err.message);
					res.render('admin/states/add', {title: 'Add State', reqBody:req.body});			  		
				}
				else
				{
					req.flash('success', 'State Added Successfully');			
    				return res.redirect('/admin/states/list');	
				}			
					  			
			});  		
		}
    }
    else
    {
    	res.render('admin/states/add',{ title: 'Add State'});
    }
     
};

exports.edit = function(req, res) {	
    if(req.method == 'POST')
    {
    	req.checkBody('name', 'Name is required').notEmpty();   		
		var errors = req.validationErrors();
		if(errors){						
			res.render('admin/states/edit', {title: 'Edit State', errors: errors,reqBody:req.body});			  		
		}
		else
		{			
			var state = {};					
			state.name =req.body.name;		
			state.default_message = req.body.default_message; 		
			State.findByIdAndUpdate( req.params.id, state , function (err) {				
				if(err)
				{
					req.flash('success', err.message);
					res.render('admin/states/edit', {title: 'Edit State', reqBody:req.body});			  		
				}
				else
				{
					req.flash('success', 'State Updated Successfully');			
    				return res.redirect('/admin/states/list');	
				}			
					  			
			});  		
		}
    }
    else
    {
    	var query = State.findOne({'_id' : req.params.id});        	
    	query.exec(function (err, result) {
  			res.render('admin/states/edit',{ title: 'Edit State',reqBody:result});       	
		});
    	
    }
     
};

exports.delete = function(req, res) {	
	State.remove({_id:req.params.id}).exec();
	req.flash('success', 'State Deleted Successfully');
	res.redirect('/admin/states/list');
    
};

exports.view = function(req, res) {	
    var query = StaticPage.findOne({'_id' : req.params.id});        	
	query.exec(function (err, result) {
			res.render('admin/states/view',{ title: 'View State',reqBody:result});       	
	});  
     
};

exports.status = function(req, res) {	
	var state = {};					
	state.status = req.params.status;			
	State.findByIdAndUpdate( req.params.id, state , function (err) {
		req.flash('success', 'State Status Changed Successfully');
		res.redirect('/admin/states/list');
	});	
    
};
