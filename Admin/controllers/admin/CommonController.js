const config = include('configs/config.js');
const nodemailer = require("nodemailer");

const Email = include("models/Email.js");

async function sendMail(params) {
	var query = Email.findOne({'slug' : params.slug});        	
	query.exec(function (err, result) {
		var message = result.body;
		
		params.params.site_url = config.site_url;
		params.params.admin_mail = config.admin_mail;
		params.params.site_name = config.site_name;
		
		for (prop in params.params){
			var val= params.params[prop];
			//message = message.replace("{{"+prop+"}}",val);	
			message = message.split("{{"+prop+"}}").join(val);
		}
		
		var smtpTransport = nodemailer.createTransport({
			host: config.senderHost,
			port: config.senderPort,
			secure: false, // true for 465, false for other ports
			auth: {
				user: config.senderUsername,
				pass: config.senderPassword
			}
		});

		var mail = {
			from: config.senderFrom,
			to: params.to,
			subject: result.subject,
			//text: "Node.js New world for me",
			html: message
		}
		
		var res = {};
		res.status = null;
		res.msg = null;
		
		smtpTransport.sendMail(mail, function(error, response){
			if(error){
				res.msg = error;
			}else{
				res.status = 'success';
				res.msg = response.message;
			}
			smtpTransport.close();			
			return res;	
		});
	});
}

exports.sendMail = sendMail;