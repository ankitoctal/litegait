const QuoteForm = include("models/QuoteForm.js");

exports.view = function(req, res) {	
    var query = QuoteForm.find({'product_id' : req.params.id}).sort({ position: 1 });     	
	query.exec(function (err, result) {			
			res.render('admin/quote_form/view',{ title: 'View Quote Form',product_id:req.params.id,type:req.params.type,results:result});       	
	});       	
     
};

exports.add = async function(req, res) {	

    if(req.method == 'POST')
    {
    	req.checkBody('type', 'Type is required').notEmpty();   		
    	req.checkBody('title', 'Title is required').notEmpty();
    	//req.checkBody('slug', 'Slug is required').notEmpty();
    	if(req.body.type=='selectbox')  		
    	{
    		req.checkBody('options', 'Options is required').notEmpty();
    	}
		var errors = req.validationErrors();
		if(errors){						
			res.render('admin/quote_form/add', {title: 'Add Input', errors: errors,product_id:req.params.id,type:req.params.type,reqBody:req.body});			  		
		}
		else
		{
			var quoteform = new QuoteForm();		
			quoteform.product_id =req.params.id;
			quoteform.product_type = req.params.type;
			quoteform.type =req.body.type;
			quoteform.title =req.body.title;
			quoteform.slug =(new Date()).getTime().toString(36);
			quoteform.options =req.body.options;
			quoteform.placeholder =req.body.placeholder;
			quoteform.required = 0;
			if(typeof req.body.required !== "undefined" && req.body.required == 1)
    		{
				quoteform.required = 1;
			}
			quoteform.position =req.body.position;
			quoteform.status = 1;		
			quoteform.save(function (err) {				
				if(err)
				{
					req.flash('success', err.message);
					res.render('admin/quote_form/add', {title: 'Add Input',product_id:req.params.id,type:req.params.type, reqBody:req.body});			  		
				}
				else
				{
					req.flash('success', 'Input Added Successfully');			
    				return res.redirect('/admin/quote_form/view/'+req.params.id+'/'+req.params.type);	
				}			
					  			
			});  		
		}
    }
    else
    {
    	res.render('admin/quote_form/add',{ title: 'Add New Input',product_id:req.params.id,type:req.params.type});
    }
     
};

exports.edit = async function(req, res) {	
	
	if(req.method == 'POST')
    {
    	req.checkBody('type', 'Type is required').notEmpty();   		
    	req.checkBody('title', 'Title is required').notEmpty();
    	//req.checkBody('slug', 'Slug is required').notEmpty();
    	if(req.body.type=='selectbox')  		
    	{
    		req.checkBody('options', 'Options is required').notEmpty();
    	}
		var errors = req.validationErrors();
		if(errors){						
			res.render('admin/quote_form/add', {title: 'Add Input', errors: errors,product_id:req.body.product_id,type:req.params.type,reqBody:req.body});			  		
		}
		else
		{			
			var quoteform = {};								
			quoteform.type =req.body.type;
			quoteform.title =req.body.title;
			//quoteform.slug =req.body.slug;
			quoteform.options =req.body.options;
			quoteform.placeholder =req.body.placeholder;
			quoteform.required = 0;
			if(typeof req.body.required !== "undefined" && req.body.required == 1)
    		{
				quoteform.required = 1;
			}
			quoteform.position =req.body.position;
			quoteform.status = 1;								
			QuoteForm.findByIdAndUpdate( req.params.id, quoteform , function (err) {								
				if(err)
				{
					req.flash('success', err.message);
					res.render('admin/quote_form/edit', {title: 'Edit Form Input', product_id:req.body.product_id,type:req.params.type, reqBody:req.body});			  		
				}
				else
				{
					req.flash('success', 'Form Input Updated Successfully');			
    				return res.redirect('/admin/quote_form/view/'+req.body.product_id+'/'+req.params.type);	
				}			
					  			
			});  		
		}
    }
    else
    {
    	var query = QuoteForm.findOne({'_id' : req.params.id});        	
    	query.exec(function (err, result) {
  			res.render('admin/quote_form/edit',{ title: 'Edit Form Input',reqBody:result,product_id:result.product_id,type:req.params.type});       	
		});
    	
    }
     
};

exports.delete = function(req, res) {	
	QuoteForm.remove({_id:req.params.fid}).exec();
	req.flash('success', 'Form Input Deleted Successfully');
	res.redirect('/admin/quote_form/view/'+req.params.id+'/'+req.params.type);
    
};