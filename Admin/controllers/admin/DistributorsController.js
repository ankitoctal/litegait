const Region = include("models/Region.js");
const Distributor = include("models/Distributor.js");

exports.list = function(req, res) {
   	var query = Distributor.find();    
    query.sort({ created_at: -1 });
    query.exec(function (err, result) {
  		res.render('admin/distributors/list',{ title: 'Distributors List',distributors:result});       	
	}); 
	 	    
};

exports.add = async function(req, res) {
	const regions = await Region.find();	
    if(req.method == 'POST')
    {
    	req.checkBody('name', 'Name is required').notEmpty(); 
    	req.checkBody('address', 'Address is required').notEmpty(); 
    	req.checkBody('region', 'Region is required').notEmpty();    	

		var errors = req.validationErrors();
		if(errors){						
			res.render('admin/distributors/add', {title: 'Add Distributor', errors: errors,regions:regions,reqBody:req.body});			  		
		}
		else
		{
			var distributor = new Distributor();		
			distributor.region =req.body.region;			
			distributor.name =req.body.name;			
			distributor.address =req.body.address;
			distributor.national =req.body.national;
			distributor.certified =req.body.certified;
			distributor.high_level_product =req.body.high_level_product;
			distributor.status = 1;
			distributor.save(function (err) {				
				if(err)
				{
					req.flash('success', err.message);
					res.render('admin/distributors/add', {title: 'Add Distributor', regions:regions,reqBody:req.body});			  		
				}
				else
				{
					req.flash('success', 'Distributor Added Successfully');			
    				return res.redirect('/admin/distributors/list');	
				}			
					  			
			});  		
		}
    }
    else
    {
    	res.render('admin/distributors/add',{ title: 'Add Distributor',regions:regions});
    }
     
};

exports.edit = async function(req, res) {	
	const regions = await Region.find();
    if(req.method == 'POST')
    {
    	req.checkBody('name', 'Name is required').notEmpty(); 
    	req.checkBody('address', 'Address is required').notEmpty(); 
    	req.checkBody('region', 'Region is required').notEmpty();	
		var errors = req.validationErrors();
		if(errors){						
			res.render('admin/distributors/edit', {title: 'Edit Faq', errors: errors,regions:regions,reqBody:req.body});			  		
		}
		else
		{	
			var distributor = {};					
			distributor.region =req.body.region;			
			distributor.name =req.body.name;			
			distributor.address =req.body.address;
			distributor.national =req.body.national;
			distributor.certified =req.body.certified;
			distributor.high_level_product =req.body.high_level_product;

			Distributor.findByIdAndUpdate( req.params.id, distributor , function (err) {				
				if(err)
				{
					req.flash('success', err.message);
					res.render('admin/distributors/edit', {title: 'Edit Distributor',regions:regions, reqBody:req.body});			  		
				}
				else
				{
					req.flash('success', 'Distributor Updated Successfully');			
    				return res.redirect('/admin/distributors/list');	
				}			
					  			
			});  		
		}
    }
    else
    {
    	var query = Distributor.findOne({'_id' : req.params.id});        	
    	query.exec(function (err, result) {
  			res.render('admin/distributors/edit',{ title: 'Edit Distributor',regions:regions,reqBody:result});       	
		});
    	
    }
     
};

exports.delete = function(req, res) {	
	Distributor.remove({_id:req.params.id}).exec();
	req.flash('success', 'Distributor Deleted Successfully');
	res.redirect('/admin/distributors/list');
    
};

exports.view = function(req, res) {	
    var query = Category.findOne({'_id' : req.params.id});        	
	query.exec(function (err, result) {
			res.render('admin/distributors/view',{ title: 'View Category',reqBody:result});       	
	});  
     
};

exports.status = function(req, res) {	
	var distributor = {};					
	distributor.status = req.params.status;			
	Distributor.findByIdAndUpdate( req.params.id, distributor , function (err) {
		req.flash('success', 'Distributor Status Changed Successfully');
		res.redirect('/admin/distributors/list');
	});	
    
};
