var Request = include("models/Request.js");

exports.list = function(req, res) {
   	var query = Request.find().populate({ path: 'product_id', select: 'name' }).populate({ path: 'part_id', select: 'name' });    
    query.sort({ created_at: -1 });
    query.exec(function (err, result) {    	
  		res.render('admin/requests/list',{ title: 'Price Quote Request List',requests:result});       	
	}); 
	 	    
};


exports.delete = function(req, res) {	
	Request.remove({_id:req.params.id}).exec();
	req.flash('success', 'Price Quote Request Deleted Successfully');
	res.redirect('/admin/requests/list'); 
    
};

exports.view = function(req, res) {	
    var query = Request.findOne({'_id' : req.params.id}).populate({ path: 'product_id', select: 'name' }).populate({ path: 'part_id', select: 'name' }); 
	query.exec(function (err, result) {		
		res.render('admin/requests/view',{ title: 'View Price Quote  Request',reqBody:result});       	
	});  
     
};

exports.status = function(req, res) {	
	var request = {};					
	request.status = req.params.status;			
	Request.findByIdAndUpdate( req.params.id, request , function (err) {
		req.flash('success', 'Price Quote Request Status Changed Successfully');
		res.redirect('/admin/requests/list');
	});	
    
};
