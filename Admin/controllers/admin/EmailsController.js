var Email = include("models/Email.js");

exports.list = function(req, res) {
   	var query = Email.find();    
    query.sort({ created_at: -1 });
    query.exec(function (err, result) {
  		res.render('admin/emails/list',{ title: 'Emails List',users:result});       	
	}); 
	 	    
};

exports.add = function(req, res) {	
    if(req.method == 'POST')
    {
    	req.checkBody('slug', 'Slug is required').notEmpty();
   		req.checkBody('subject', 'Subject is required').notEmpty();
   		req.checkBody('body', 'Body is required').notEmpty();
		var errors = req.validationErrors();
		if(errors){						
			res.render('admin/emails/add', {title: 'Add Email', errors: errors,reqBody:req.body});			  		
		}
		else
		{
			var email = new Email();		
			email.slug =req.body.slug;
			email.subject =req.body.subject;
			email.body =req.body.body;	

			email.save(function (err) {				
				if(err)
				{
					req.flash('success', err.message);
					res.render('admin/emails/add', {title: 'Add Email', reqBody:req.body});			  		
				}
				else
				{
					req.flash('success', 'Email Added Successfully');			
    				return res.redirect('/admin/emails/list');	
				}			
					  			
			});  		
		}
    }
    else
    {
    	res.render('admin/emails/add',{ title: 'Add Email'});
    }
     
};

exports.edit = function(req, res) {	
    if(req.method == 'POST')
    {
    	req.checkBody('slug', 'Slug is required').notEmpty();
   		req.checkBody('subject', 'Subject is required').notEmpty();
   		req.checkBody('body', 'Body is required').notEmpty();
		var errors = req.validationErrors();
		if(errors){						
			res.render('admin/emails/edit', {title: 'Edit Email', errors: errors,reqBody:req.body});			  		
		}
		else
		{			
			var email = {};					
			email.slug =req.body.slug;
			email.subject =req.body.subject;
			email.body =req.body.body;			
			Email.findByIdAndUpdate( req.params.id, email , function (err) {				
				if(err)
				{
					req.flash('success', err.message);
					res.render('admin/emails/edit', {title: 'Edit Email', reqBody:req.body});			  		
				}
				else
				{
					req.flash('success', 'Email Updated Successfully');			
    				return res.redirect('/admin/emails/list');	
				}			
					  			
			});  		
		}
    }
    else
    {
    	var query = Email.findOne({'_id' : req.params.id});        	
    	query.exec(function (err, result) {
  			res.render('admin/emails/edit',{ title: 'Edit Email',reqBody:result});       	
		});
    	
    }
     
};

exports.delete = function(req, res) {	
	Email.remove({_id:req.params.id}).exec();
	req.flash('success', 'Email Deleted Successfully');
	res.redirect('/admin/emails/list');
    
};