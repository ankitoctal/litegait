exports.statusChange = async function(req, res) {
	if(req.method == 'POST')
    {
		var id = req.body.id;
		var status = req.body.status;
		var moduleName = req.body.moduleName;
		
		var objTable = include("models/"+moduleName+".js");
		var detail = await objTable.findOne({'_id' : id}).exec();
		
		detail.status = status;
		
		detail.save(function (err) {
			if(err){
				res.json({'status':''});
			}
			else{
				res.json({'status':'success'});
			}	  			
		});
    }
};

exports.deleterecord = async function(req, res) {
	if(req.method == 'POST')
    {
		//res.json(req.body);
		
		var id = req.body.id;
		var moduleName = req.body.moduleName;
		
		var objTable = include("models/"+moduleName+".js");
		
		//State.remove({_id:req.params.id}).exec();
		
		objTable.deleteOne({_id:id},function(err){
			if(err){
				res.json({'status':''});
			}
			else{
				res.json({'status':'success'});
			}	
		});
    }
};
