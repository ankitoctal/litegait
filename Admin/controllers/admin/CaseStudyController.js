const Speaker = include("models/Speaker.js");
const State = include("models/State.js");
const CaseStudy = include("models/CaseStudy.js");
const CaseStudyContent = include("models/CaseStudyContent.js"); 

exports.list = async function(req, res) {

	const speakers = await Speaker.where('status',1).select('_id name').find();

   	var query = CaseStudy.find();    
    query.sort({ created_at: -1 });
    query.exec(function (err, result) {
    	
  		res.render('admin/casestudy/list',{ title: 'Case Study List',casestudies:result,speakers: speakers});       	
	}); 
	 	    
};

exports.add = async function(req, res) {	
	
    if(req.method == 'POST')
    {    	
    	req.body.description = {};    	
    	res.locals.activeLanguage.forEach(function(language) {  			  			
  			req.body.description[language.code] =req.body['description['+language.code+']'] ;  			
		});    	
    	req.checkBody('slug', 'Slug is required').notEmpty();
   		req.checkBody('title', 'Title is required').notEmpty();
   		req.checkBody('synopsis', 'Synopsis is required').notEmpty();   		
   		req.checkBody('description.en', 'Description is required').notEmpty();     		   				   		  		
		var errors = req.validationErrors();
		if(errors){						
			res.render('admin/casestudy/add', {title: 'Add Case Study', errors: errors,reqBody:req.body});
		}
		else
		{
			var casestudy = new CaseStudy();					
			casestudy.title =req.body.title;
			casestudy.slug =req.body.slug;						
			casestudy.synopsis =req.body.synopsis;			
			casestudy.description =req.body.description.en;						
			casestudy.meta_title =req.body.meta_title;	
			casestudy.meta_description =req.body.meta_description;
			casestudy.status = 1;	
			casestudy.save(async function (err,newcasestudy) {				
				if(err)
				{
					req.flash('success', err.message);
					res.render('admin/casestudy/add', {title: 'Add Case Study', errors: errors,reqBody:req.body});
				}
				else
				{
					for (var i = 0; i < res.locals.activeLanguage.length; i++) {
						var langCode = res.locals.activeLanguage[i].code;
						var casestudycontent = new CaseStudyContent();
						casestudycontent.case_study_id =newcasestudy._id;
						casestudycontent.lang=langCode;												
						casestudycontent.description=req.body.description[langCode];						
						await casestudycontent.save();

						newcasestudy.lang_content.push(casestudycontent);
						await newcasestudy.save();
					}

					req.flash('success', 'Case Study Added Successfully');			
    				return res.redirect('/admin/casestudy/list');	
				}			
					  			
			});  		
		}
    }
    else
    {
    	res.render('admin/casestudy/add',{ title: 'Add Case Study'});
    }
     
};

exports.edit = async function(req, res) {	

    if(req.method == 'POST')
    {
    	req.body.description = {};    	
    	res.locals.activeLanguage.forEach(function(language) {  			  			
  			req.body.description[language.code] =req.body['description['+language.code+']'] ;  			
		});    	
    	req.checkBody('slug', 'Slug is required').notEmpty();
   		req.checkBody('title', 'Title is required').notEmpty();   		
   		req.checkBody('description.en', 'Description is required').notEmpty();    		
   		req.checkBody('synopsis', 'Synopsis is required').notEmpty();  		   		
		var errors = req.validationErrors();
		if(errors){			
			//console.log(errors);
			res.render('admin/casestudy/add', {title: 'Add Case Study', errors: errors,reqBody:req.body});
		}
		else
		{			
			var casestudy = {};					
			casestudy.title =req.body.title;
			casestudy.slug =req.body.slug;						
			casestudy.synopsis =req.body.synopsis;								
			casestudy.description =req.body.description.en;						
			casestudy.meta_title =req.body.meta_title;	
			casestudy.meta_description =req.body.meta_description;							
			CaseStudy.findByIdAndUpdate( req.params.id, casestudy , function (err) {
				//console.log(err);				
				if(err)
				{
					req.flash('success', err.message);
					res.render('admin/casestudy/add', {title: 'Add Case Study', errors: errors,reqBody:req.body});
				}
				else
				{
					var query = CaseStudy.findOne({'_id' : req.params.id}).populate('lang_content');      	
			    	query.exec(function (err, result) {    						    		
			    		for (var i = 0; i < result.lang_content.length; i++) {
			    			var k = result.lang_content[i];
			    			var langCode = k.lang; 
			    			var casestudycontent = {};				    						    			
			    			casestudycontent.description=req.body.description[langCode];			    			
			    			CaseStudyContent.findByIdAndUpdate( k._id, casestudycontent , function (err) {
			    			});	
						}
					});	

					req.flash('success', 'Case Study Updated Successfully');			
    				return res.redirect('/admin/casestudy/list');	
				}			
					  			
			});  		
		}
    }
    else
    {
    	var query = CaseStudy.findOne({'_id' : req.params.id}).populate('lang_content');        	
    	query.lean().exec(function (err, result) {    		
    		
    		result.description = {};

	    	result.lang_content.forEach(function(content) {  	    		
	    		result.description[content.lang] =content.description ;	    		
			});
			
  			res.render('admin/casestudy/edit',{ title: 'Edit Case Study',reqBody:result});       	
		});
    	
    }
     
};

exports.delete = function(req, res) {
	//console.log(req.params.user_id);
	var query = CaseStudy.findOne({'_id' : req.params.id}).populate('lang_content');        	
	query.exec(function (err, result) {
		for (var i = 0; i < result.lang_content.length; i++) {
			var k = result.lang_content[i];			
			CaseStudyContent.remove({_id:k._id}).exec();			
		}
	});
	CaseStudy.remove({_id:req.params.id}).exec();
	req.flash('success', 'Case Study Deleted Successfully');	
	res.redirect('/admin/casestudy/list');
    
};

exports.view = function(req, res) {	
    var query = Category.findOne({'_id' : req.params.id});        	
	query.exec(function (err, result) {
			res.render('admin/speakers/view',{ title: 'View Category',reqBody:result});       	
	});  
     
};

exports.status = function(req, res) {	
	var casestudy = {};					
	casestudy.status = req.params.status;			
	CaseStudy.findByIdAndUpdate( req.params.id, casestudy , function (err) {
		req.flash('success', 'Case Study Status Changed Successfully');
		res.redirect('/admin/casestudy/list');
	});	
    
};
