var News = include("models/News.js");
var NewsContent = include("models/NewsContent.js");
var Bulletin = include("models/Bulletin.js");

exports.list = function(req, res) {
   	var query = Bulletin.find();    
    query.sort({ created_at: -1 });
    query.exec(function (err, result) {
  		res.render('admin/bulletin/list',{ title: 'Bulletin List',bulletins:result});       	
	});     
};

exports.add = async function(req, res) {
	if(req.method == 'POST')
    {  
   		req.checkBody('title', 'Title is required').notEmpty();		    	    	   		
   		req.checkBody('publish_date', 'Publish Date is required').notEmpty();    	    	
   		//req.checkBody('htmlfile', 'HTML File is required').notEmpty();   		    	    	
		var errors = req.validationErrors();
		if(errors){		
			res.render('admin/bulletin/add', {title: 'Add Bulletin', errors: errors,reqBody:req.body});				  		
		}
		else
		{
			var htmlFile = req.files.htmlfile;
           	var fileName = req.files.htmlfile.name; 
           	htmlFile.mv('public/uploads/bulletin/' + fileName  , function(err) {

           		if(err)
				{
					req.flash('success', err.message);
					res.render('admin/bulletin/add', {title: 'Add Bulletin', reqBody:req.body});			  		
				}           	 

				var bulletin = new Bulletin();		
				bulletin.title =req.body.title;
				bulletin.description =req.body.description;				
				bulletin.publish_date =req.body.publish_date;
				bulletin.image =req.body.image;
				bulletin.htmlfile = fileName;
				bulletin.status = 1;			
				bulletin.save(async function (err, newbulletin) {
								
					if(err)
					{
						req.flash('success', err.message);
						res.render('admin/bulletin/add', {title: 'Add Bulletin', reqBody:req.body});			  		
					}
					else
					{		
						req.flash('success', 'Bulletin Added Successfully');			
	    				return res.redirect('/admin/bulletin/list');		
					}			
						  			
				}); 
			});	 		
		}
    }
    else
    {
		res.render('admin/bulletin/add',{ title: 'Add Bulletin'});
    }
     
};
exports.edit = async function(req, res) {	
    if(req.method == 'POST')
    {
		req.checkBody('title', 'Title is required').notEmpty();		    	    	   		
   		req.checkBody('publish_date', 'Publish Date is required').notEmpty();    	    	
   		//req.checkBody('link', 'Link is required').notEmpty();  

		var errors = req.validationErrors();
		if(errors){			
			
			res.render('admin/bulletin/edit', {title: 'Edit Bulletin', errors: errors,reqBody:req.body});			  		
		}
		else
		{	
			
           	var bulletin = {};					
			bulletin.title =req.body.title;
			bulletin.description =req.body.description;				
			bulletin.publish_date =req.body.publish_date;
			if(req.body.image){
				bulletin.image =req.body.image;
			}
			

           	if(!isEmpty(req.files)) 
           	{ console.log("nahi aana chahiye");
           		var htmlFile = req.files.htmlfile;
           		var fileName = req.files.htmlfile.name; 
           		bulletin.htmlfile = fileName;

           		await htmlFile.mv('public/uploads/bulletin/' + fileName);
           	}					

			Bulletin.findByIdAndUpdate( req.params.id, bulletin , function (err) {			
				if(err)
				{
					req.flash('success', err.message);
					res.render('admin/bulletin/edit', {title: 'Edit Bulletin', reqBody:req.body});			  		
				}
				else 
				{
					req.flash('success', 'Bulletin Updated Successfully');			
    				return res.redirect('/admin/bulletin/list');	
				}			
					  			
			});
			  		
		}
    }
    else
    {
    	var query = Bulletin.findOne({'_id' : req.params.id});
    	query.lean().exec(function (err, result) { 							  		
  			res.render('admin/bulletin/edit',{ title: 'Edit Bulletin',reqBody:result});       	
		});
    	
    }
     
};
function isEmpty(obj) {
    for(var key in obj) {
        if(obj.hasOwnProperty(key))
            return false;
    }
    return true;
}
exports.delete = function(req, res) {	
	Bulletin.remove({_id:req.params.id}).exec();
	req.flash('success', 'Bulletin Deleted Successfully');
	res.redirect('/admin/bulletin/list'); 
};


exports.status = function(req, res) {	
	var bulletin = {};					
	bulletin.status = req.params.status;			
	Bulletin.findByIdAndUpdate( req.params.id, bulletin , function (err) {
		req.flash('success', 'Bulletin Status Changed Successfully');
		res.redirect('/admin/bulletin/list');
	});	
    
};
