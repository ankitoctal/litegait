var Gallery = include("models/Gallery.js");
const GalleryContent = include("models/GalleryContent.js"); 
var Tag = include("models/Tag.js");

exports.list = function(req, res) {
   	var query = Gallery.find();    
    query.sort({ created_at: -1 });
    query.exec(function (err, result) {
  		res.render('admin/gallery/list',{ title: 'Gallery List',galleries:result});       	
	}); 
	 	    
};


exports.shareGalleryUrl= async function(req, res){
	res.render('admin/gallery/share_gallery_url',{ title: 'Share gallery URL'});
}


exports.add = async function(req, res) {
	const tags = await Tag.find();
    if(req.method == 'POST')
    {  
		req.body.description = {};
    	res.locals.activeLanguage.forEach(function(language) {  		
			req.body.description[language.code] =req.body['description['+language.code+']'] ;
		});

		req.checkBody('title', 'Title is required').notEmpty();
		req.checkBody('type', 'Type is required').notEmpty();
		if(req.body.type == "photo" || req.body.type == "video"){ 
			req.checkBody('file', 'File is required').notEmpty();
		}
		else if(req.body.type == "vimeo_video"){ 
			req.checkBody('vimeo_url', 'Vimeo video URL is required').notEmpty();
		}   		
   		req.checkBody('tag', 'Tag is required').notEmpty();
   		req.checkBody('description.en', 'Description is required').notEmpty();  
   		  	    	
		var errors = req.validationErrors();
		if(errors){		
			res.render('admin/gallery/add', {title: 'Add Gallery', errors: errors,reqBody:req.body,tags: tags});				  		
		}
		else
		{
			var gallery = new Gallery();		
			gallery.title =req.body.title;  
			gallery.position =req.body.position;
			gallery.type =req.body.type;
			gallery.file =req.body.file;
			gallery.vimeo_url =req.body.vimeo_url;
			gallery.tag =req.body.tag;
			gallery.description =req.body.description.en;	
			gallery.status = 1;			
			gallery.save(async function (err, newgallery) {
							
				if(err)
				{
					req.flash('success', err.message);
					res.render('admin/gallery/add', {title: 'Add Gallery', reqBody:req.body,tags: tags});			  		
				}
				else
				{					
					for (var i = 0; i < res.locals.activeLanguage.length; i++) {
						var langCode = res.locals.activeLanguage[i].code;
						var gallerycontent = new GalleryContent();
						gallerycontent.gallery_id =newgallery._id;
						gallerycontent.description=req.body.description[langCode];
						gallerycontent.lang=langCode;						
						await gallerycontent.save();
						newgallery.lang_content.push(gallerycontent);
						await newgallery.save();
					}		
					req.flash('success', 'Gallery Added Successfully');			
    				return res.redirect('/admin/gallery/list');		
				}			
					  			
			});  		
		}
    }
    else
    {
		res.render('admin/gallery/add',{ title: 'Add Gallery', tags: tags});
    }
     
};
exports.edit = async function(req, res) {	
	const tags = await Tag.find();
    if(req.method == 'POST')
    {
    	req.body.description = {};
    	res.locals.activeLanguage.forEach(function(language) {  		
			req.body.description[language.code] =req.body['description['+language.code+']'] ;
		});
		
    	req.checkBody('title', 'Title is required').notEmpty();
		req.checkBody('type', 'Type is required').notEmpty();
		if(req.body.type == "photo" || req.body.type == "video"){ 
			req.checkBody('file', 'File is required').notEmpty();
		}
		else if(req.body.type == "vimeo_video"){ 
			req.checkBody('vimeo_url', 'Vimeo Video URL is required').notEmpty();
		}   		
   		req.checkBody('tag', 'Tag is required').notEmpty();
   		req.checkBody('description.en', 'Description is required').notEmpty();    	    	
		var errors = req.validationErrors();
		if(errors){			
			
			res.render('admin/gallery/edit', {title: 'Edit Gallery', errors: errors,reqBody:req.body,tags: tags});			  		
		}
		else
		{			
			var gallery = {};	
			gallery.title =req.body.title;	
			gallery.position =req.body.position;			
			gallery.type =req.body.type;
			if(req.body.type == "photo" || req.body.type == "video"){ 
				gallery.file =req.body.file;
				gallery.vimeo_url ='';
			}
			else if(req.body.type == "vimeo_video"){ 
				gallery.file ='';
				gallery.vimeo_url =req.body.vimeo_url;
			} 			
			gallery.tag =req.body.tag;
			gallery.description =req.body.description.en;		
			Gallery.findByIdAndUpdate( req.params.id, gallery , function (err) {			
				if(err)
				{
					req.flash('success', err.message);
					res.render('admin/gallery/edit', {title: 'Edit Gallery', reqBody:req.body,tags: tags});			  		
				}
				else 
				{
					var query = Gallery.findOne({'_id' : req.params.id}).populate('lang_content');      	
			    	query.exec(async function (err, result) {    						    		
			    		for (var i = 0; i < result.lang_content.length; i++) {
			    			var k = result.lang_content[i];
			    			var langCode = k.lang;
			    			var gallerycontent = {};				    			
			    			gallerycontent.description=req.body.description[langCode];
			    			GalleryContent.findByIdAndUpdate( k._id, gallerycontent , function (err) {
			    			});	
						}			  			
					});
					req.flash('success', 'Gallery Updated Successfully');			
    				return res.redirect('/admin/gallery/list');	
				}			
					  			
			});  		
		}
    }
    else
    {
    	var query = Gallery.findOne({'_id' : req.params.id}).populate('lang_content').populate('image_gallery');
    	query.lean().exec(function (err, result) { 		
		
    		result.description = {};

	    	result.lang_content.forEach(function(content) {  
	    		result.description[content.lang] =content.description ;	  			
			});    		  		
  			res.render('admin/gallery/edit',{ title: 'Edit Gallery',reqBody:result, tags: tags});       	
		});
    	
    }
     
};
exports.delete = function(req, res) {
	var query = Gallery.findOne({'_id' : req.params.id}).populate('lang_content');        	
	query.exec(function (err, result) {
		for (var i = 0; i < result.lang_content.length; i++) {
			var k = result.lang_content[i];			
			GalleryContent.remove({_id:k._id}).exec();			
		}
	});
	Gallery.remove({_id:req.params.id}).exec();
	req.flash('success', 'Gallery Deleted Successfully');
	res.redirect('/admin/gallery/list');  
    
};
exports.status = function(req, res) {	
	var nws = {};					
	nws.status = req.params.status;			
	Gallery.findByIdAndUpdate( req.params.id, nws , function (err) {
		req.flash('success', 'Gallery Status Changed Successfully');
		res.redirect('/admin/gallery/list');
	});	
    
};
