var Region = include("models/Region.js");

exports.list = function(req, res) {
   	var query = Region.find();    
    query.sort({ created_at: -1 });
    query.exec(function (err, result) {
  		res.render('admin/regions/list',{ title: 'Region List',regions:result});       	
	}); 
	 	    
};

exports.add = function(req, res) {	
    if(req.method == 'POST')
    {
    	req.checkBody('code', 'Region Code is required').notEmpty(); 
    	req.checkBody('name', 'Name is required').notEmpty(); 
    	req.checkBody('image', 'Image is required').notEmpty();     	    	 		
		var errors = req.validationErrors();
		if(errors){						
			res.render('admin/regions/add', {title: 'Add Region', errors: errors,reqBody:req.body});			  		
		}
		else
		{
			var region = new Region();		
			region.code =req.body.code;
			region.name =req.body.name;
			region.image =req.body.image;		
			region.status = 1;
			region.save(function (err) {				
				if(err)
				{
					req.flash('success', err.message);
					res.render('admin/regions/add', {title: 'Add Region', reqBody:req.body});			  		
				}
				else
				{
					req.flash('success', 'Region Added Successfully');			
    				return res.redirect('/admin/regions/list');	
				}			
					  			
			});  		
		}
    }
    else
    {
    	res.render('admin/regions/add',{ title: 'Add Region'});
    }
     
};

exports.edit = function(req, res) {	
    if(req.method == 'POST')
    {
    	req.checkBody('code', 'Region Code is required').notEmpty(); 
    	req.checkBody('name', 'Name is required').notEmpty();
    	req.checkBody('image', 'Image is required').notEmpty();     	
		var errors = req.validationErrors();
		if(errors){						
			res.render('admin/regions/edit', {title: 'Edit Region', errors: errors,reqBody:req.body});			  		
		}
		else
		{			
			var region = {};					
			region.code =req.body.code;
			region.name =req.body.name;
			region.image =req.body.image;			
			Region.findByIdAndUpdate( req.params.id, region , function (err) {				
				if(err)
				{
					req.flash('success', err.message);
					res.render('admin/regions/edit', {title: 'Edit Region', reqBody:req.body});			  		
				}
				else
				{
					req.flash('success', 'Region Updated Successfully');			
    				return res.redirect('/admin/regions/list');	
				}			
					  			
			});  		
		}
    }
    else
    {     	
    	var query = Region.findOne({'_id' : req.params.id});        	
    	query.exec(function (err, result) {    			
  			res.render('admin/regions/edit',{ title: 'Edit Region',reqBody:result});       	
		}); 
    }
     
};

exports.delete = function(req, res) {	
	Region.remove({_id:req.params.id}).exec();
	req.flash('success', 'Region Deleted Successfully');
	res.redirect('/admin/regions/list');
    
};

exports.status = function(req, res) {	
	var region = {};					
	region.status = req.params.status;			
	Region.findByIdAndUpdate( req.params.id, region , function (err) {
		req.flash('success', 'Region Status Changed Successfully');
		res.redirect('/admin/regions/list');
	});
    
};