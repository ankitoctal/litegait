const Order = include("models/Order.js");
const OrderProduct = include("models/OrderProduct.js"); 
const OrderAddress = include("models/OrderAddress.js");
const config = include('configs/config.js');
const excel = require('node-excel-export');

var CommonController = require('./CommonController');

exports.list = function(req, res) {
	var query_user = req.query.user; 
   	var query = Order.find({total: { $gt: 0 }}).populate('user_id').populate({path: 'order_product',model: 'OrderProduct',populate:[{path: 'product_id',model: 'Product',select :'name slug image'},{path: 'part_id',model: 'Part',select :'name slug image'},{path: 'webinar_id',model: 'Webinar',select :'title slug date_time_1 date_time_2'},{path: 'seminar_id',model: 'Seminar',select :'title slug date_time'}] }); 
    query.sort({ created_at: -1 });
    query.exec(function (err, result) {  	

		res.render('admin/orders/list',{ title: 'Order List',statuses:config.order_statuses,orders:result, query_user: query_user});       	
	}); 
	 	    
};


exports.view = function(req, res) {	
    var query = Order.findOne({'_id' : req.params.id}).populate('user_id','name email phone').populate('order_address');
	//query.populate({path: 'order_product',model: 'OrderProduct',populate: {path: 'product_id',model: 'Product',select :'name slug image'} });
	query.populate({path: 'order_product',model: 'OrderProduct',populate:[{path: 'product_id',model: 'Product',select :'name slug image'},{path: 'part_id',model: 'Part',select :'name slug image'},{path: 'webinar_id',model: 'Webinar',select :'title slug date_time_1 date_time_2'},{path: 'seminar_id',model: 'Seminar',select :'title slug date_time'}] });
	query.exec(function (err, result) {	
		//console.log(result);	
		res.render('admin/orders/view',{ title: 'View Order',order:result});       	
	});  
     
};

exports.packingSlip = function(req, res) {	
    var query = Order.findOne({'_id' : req.params.id}).populate('user_id','name email phone').populate('order_address');
	//query.populate({path: 'order_product',model: 'OrderProduct',populate: {path: 'product_id',model: 'Product',select :'name slug image'} });
	query.populate({path: 'order_product',model: 'OrderProduct',populate:[{path: 'product_id',model: 'Product',select :'name slug image'},{path: 'part_id',model: 'Part',select :'name slug image'},{path: 'webinar_id',model: 'Webinar',select :'title slug date_time_1 date_time_2'},{path: 'seminar_id',model: 'Seminar',select :'title slug date_time'}] });
	query.exec(function (err, result) {	
		//console.log(result);	
		res.render('admin/orders/packing_slip',{ title: 'Packing Slip for #'+result.order_id,order:result});       	
	});  
     
};

exports.statusChange = async function(req, res) {
	if(req.method == 'POST')
    {
		var id = req.body.id;
		var status = req.body.status;
		
		var detail = await Order.findOne({'_id' : id}).exec();
		
		detail.status = status;
		
		detail.save(function (err) {
			if(err){
				res.json({'status':'','err':err,'data':req.body});
			}
			else{
				res.json({'status':'success'});
			}	  			
		});
    }
};

exports.mail = async function(req, res) {	
    var query = Order.findOne({'_id' : req.params.id}).populate('user_id','name email phone').populate('order_address');
	query.populate({path: 'order_product',model: 'OrderProduct',populate: {path: 'product_id',model: 'Product',select :'name slug image'} });

	query.exec(function (err, result) {
		var params =  {};
		params.slug = 'order-success';	
		params.to = result.user_id.email;				
		params.params = {};	
		params.params.name = result.user_id.name;		
		params.params.site_name = config.site_name;
		params.params.details = order_details(result);		
		params.params.admin_mail = config.admin_mail;
		
		CommonController.sendMail(params);
		
		/* var response = {};
		response.status = "success";
		response.msg = "Mail Send Successfully.";
		return res.json(response); */
		
		backURL = req.header('Referer') || '/';
		res.redirect(backURL);
	});     
};

function order_details(result) {
	var strDetails = '<table border="1" style="width:100%">';
		
	strDetails += '<tr>';
		strDetails += '<th>Order Id</th><td>'+result._id+'</td>';
	strDetails += '</tr>';
	
	strDetails += '<tr>';
		strDetails += '<th>Customer</th><td>';
			strDetails += '<b>Name : </b>'+result.user_id.name+'<br/>';
			strDetails += '<b>Email : </b>'+result.user_id.email+'<br/>';
			strDetails += '<b>Phone : </b>'+result.user_id.phone+'<br/>';
		strDetails += '</td>';
	strDetails += '</tr>';
	
	strDetails += '<tr>';
		strDetails += '<th>Order Product</th><td>';
			strDetails += '<table border="1" style="width: 90%;margin: 18px;">';
				strDetails += '<tr>';
					strDetails += '<th>Sr.No</th><th>Name</th><th>Price</th><th>Qty</th><th>Total</th>';
				strDetails += '</tr>';
				
				var i = 1;
				result.order_product.forEach(function(product) {  
					strDetails += '<tr>';
						strDetails += '<td>'+i+'</td>';
						strDetails += '<td>'+product.product_id.name+'</td>';
						strDetails += '<td>$'+product.price+'</td>';
						strDetails += '<td>$'+product.qty+'</td>';
						strDetails += '<td>$'+product.total+'</td>';
					strDetails += '</tr>';
					i++;
				});
				
			strDetails += '</table>';
		strDetails += '</td>';
	strDetails += '</tr>';
	
	strDetails += '<tr>';
		strDetails += '<th>Order Address</th><td>';
			strDetails += '<div style="width:40%;float:left;">';
				strDetails += '<b>Billing Address</b><br/><div style="border: 1px solid #cccc;margin: 5px;padding: 10px;min-height: 180px;">';
					strDetails += '<b>'+result.order_address.billing_fname+' '+result.order_address.billing_lname+'</b><br/>';
					strDetails += result.order_address.billing_add1+'<br/>';
					strDetails += '<b>City : </b>'+result.order_address.billing_city+'<br/>';
					strDetails += '<b>State : </b>'+result.order_address.billing_state+'<br/>';
					strDetails += '<b>Country : </b>'+result.order_address.billing_country+'<br/>';
					strDetails += '<b>Zipcode : </b>'+result.order_address.billing_zipcode+'<br/>';
			strDetails += '</div></div>';
			
			strDetails += '<div style="width:40%;float:left;">';
				strDetails += '<b>Shipping Address</b><br/><div style="border: 1px solid #cccc;margin: 5px;padding: 10px;min-height: 180px;">';
					strDetails += '<b>'+result.order_address.shipping_fname+' '+result.order_address.shipping_lname+'</b><br/>';
					strDetails += result.order_address.shipping_add1+'<br/>';
					strDetails += '<b>City : </b>'+result.order_address.shipping_city+'<br/>';
					strDetails += '<b>State : </b>'+result.order_address.shipping_state+'<br/>';
					strDetails += '<b>Country : </b>'+result.order_address.shipping_country+'<br/>';
					strDetails += '<b>Zipcode : </b>'+result.order_address.shipping_zipcode+'<br/>';
			strDetails += '</div></div>';
		strDetails += '</td>';
	strDetails += '</tr>';
		
	strDetails += '<tr>';
		strDetails += '<th>Payment Method</th><td>'+result.payment_method+'</td>';
	strDetails += '</tr>';
		
	strDetails += '<tr>';
		strDetails += '<th>Payment Id</th><td>'+result.payment_id+'</td>';
	strDetails += '</tr>';
		
	strDetails += '<tr>';
		strDetails += '<th>Sub Total</th><td>$'+result.sub_total+'</td>';
	strDetails += '</tr>';

	if(result.discount > 0){
		strDetails += '<tr>';
			strDetails += '<th>Discount</th><td>$'+result.discount+'</td>';
		strDetails += '</tr>';
	}
	
	strDetails += '<tr>';
		strDetails += '<th>Total</th><td>$'+result.total+'</td>';
	strDetails += '</tr>';
	
	strDetails += '</table>';
	
	return strDetails;
}

exports.editAddress = async function(req, res) {

	var order = await Order.findOne({'_id' : req.params.id}).populate('user_id','name email phone').populate('order_address');

	if(req.method == 'POST'){
		req.checkBody('billing_fname', 'Billing First Name is required').notEmpty();
		req.checkBody('billing_phone', 'Billing Phone is required').notEmpty();
		req.checkBody('billing_email', 'Billing Email is required').notEmpty();
		req.checkBody('billing_add1', 'Billing Address Line 1 is required').notEmpty();
		req.checkBody('billing_city', 'Billing City is required').notEmpty();
		req.checkBody('billing_state', 'Billing State is required').notEmpty();
		req.checkBody('billing_country', 'Billing Country is required').notEmpty();
		req.checkBody('billing_zipcode', 'Billing ZipCode is required').notEmpty();
		req.checkBody('shipping_fname', 'Shipping First Name is required').notEmpty();
		req.checkBody('shipping_phone', 'Shipping Phone is required').notEmpty();
		req.checkBody('shipping_email', 'Shipping Email is required').notEmpty();
		req.checkBody('shipping_add1', 'Shipping Address Line 1 is required').notEmpty();
		req.checkBody('shipping_city', 'Shipping City is required').notEmpty();
		req.checkBody('shipping_state', 'Shipping State is required').notEmpty();
		req.checkBody('shipping_country', 'Shipping Country is required').notEmpty();
		req.checkBody('shipping_zipcode', 'Shipping ZipCode is required').notEmpty();
		var errors = req.validationErrors();
		if(errors){		
			res.render('admin/orders/edit_address',{errors:errors, title: 'Edit Order Address For #'+order._id,order:order,reqBody:order.order_address});
		}
		else
		{
			var orderaddress = {};					
			orderaddress.billing_fname = req.body.billing_fname;
			orderaddress.billing_lname = req.body.billing_lname;
			orderaddress.billing_phone = req.body.billing_phone;
			orderaddress.billing_email = req.body.billing_email;
			orderaddress.billing_add1 = req.body.billing_add1;
			orderaddress.billing_add2 = req.body.billing_add2;
			orderaddress.billing_city = req.body.billing_city;
			orderaddress.billing_state = req.body.billing_state;
			orderaddress.billing_country = req.body.billing_country;
			orderaddress.billing_zipcode = req.body.billing_zipcode;
			orderaddress.shipping_fname = req.body.shipping_fname;
			orderaddress.shipping_lname = req.body.shipping_lname;
			orderaddress.shipping_phone = req.body.shipping_phone;
			orderaddress.shipping_email = req.body.shipping_email;
			orderaddress.shipping_add1 = req.body.shipping_add1;
			orderaddress.shipping_add2 = req.body.shipping_add2;
			orderaddress.shipping_city = req.body.shipping_city;
			orderaddress.shipping_state = req.body.shipping_state;
			orderaddress.shipping_country = req.body.shipping_country;
			orderaddress.shipping_zipcode = req.body.shipping_zipcode;

			OrderAddress.findByIdAndUpdate( order.order_address._id, orderaddress , function (err) {
				if(err)
				{
					req.flash('success', err.message);
					res.render('admin/orders/edit_address',{errors:errors, title: 'Edit Order Address For #'+order._id,order:order,reqBody:order.order_address});
				}
				else 
				{
					req.flash('success', 'Order Address Updated Successfully');			
    				return res.redirect('/admin/order/view/'+req.params.id);	
				}
			});	
			
		}	
    } 
    else
    {
    	res.render('admin/orders/edit_address',{ title: 'Edit Order Address For #'+order._id,order:order,reqBody:order.order_address});
    }

};

exports.editShipment = async function(req, res) {

	var order = await Order.findOne({'_id' : req.params.id});

	if(req.method == 'POST'){
		req.checkBody('ship_method', 'Shipment Method is required').notEmpty();
		req.checkBody('ship_id', 'Shipment ID is required').notEmpty();		
		var errors = req.validationErrors();
		if(errors){	
			res.render('admin/orders/edit_shipment',{errors:errors, title: 'Edit Order Shipment For #'+order._id,order:order,reqBody:order});	
		}
		else
		{
			var neworder = {};					
			neworder.ship_method = req.body.ship_method;
			neworder.ship_id = req.body.ship_id;
			Order.findByIdAndUpdate( req.params.id, neworder , function (err) {
				if(err)
				{
					req.flash('success', err.message);
					res.render('admin/orders/edit_shipment',{errors:errors, title: 'Edit Order Shipment For #'+order._id,order:order,reqBody:order});
				}
				else 
				{
					req.flash('success', 'Order Shipment Details Updated Successfully');			
    				return res.redirect('/admin/order/view/'+req.params.id);	
				}
			});	
			
		}	
    } 
    else
    {
    	res.render('admin/orders/edit_shipment',{ title: 'Edit Order Shipment For #'+order._id,order:order,reqBody:order});
    }

};



exports.export_in_excel = function(req, res) {	


	var query_user = req.query.user; 
   	var query = Order.find({total: { $gt: 0 }}).populate('user_id').populate({path: 'order_product',model: 'OrderProduct',populate:[{path: 'product_id',model: 'Product',select :'name slug image'},{path: 'part_id',model: 'Part',select :'name slug image'},{path: 'webinar_id',model: 'Webinar',select :'title slug date_time_1 date_time_2'},{path: 'seminar_id',model: 'Seminar',select :'title slug date_time'}] }); 
    query.sort({ created_at: -1 });
    query.exec(function (err, result) {  

		const specification = {
		  id: { 
		    displayName: 'ID', 
		    headerStyle: {}, 
		    width: 220 
		  },
		  name: {
		    displayName: 'NAME',
		    headerStyle: {},
		    width: 220
		  },
		  products: {
		    displayName: 'PRODUCTS',
		    headerStyle: {},
		    width: 220
		  },
		  email: {
		    displayName: 'EMAIL',
		    headerStyle: {},
		    width: 220
		  },		  
		  total: {
		    displayName: 'TOTAL ($)',
		    headerStyle:{},
		    width: 220
		  },
		  status: {
		    displayName: 'STATUS',
		    headerStyle:{},
		    width: 220
		  },
		  created: {
		    displayName: 'CREATED',
		    headerStyle:{},
		    width: 220
		  },
		}
		 
	
        var temp_arr = []; 
		for (var i = 0;i<result.length;i++){
 
			var tempobj = {}; 
			tempobj.id =  '#'+result[i].order_id;
			tempobj.name =  result[i].user_id ? result[i].user_id.name : ''

			var temp_op_prod = '';
			if (result[i].order_product){
				result[i].order_product.forEach(function (op) {
				    if(op.product_id)
				    	temp_op_prod += op.product_id.name + ", "
				    if(op.part_id)
				    	temp_op_prod += op.part_id.name + ", "
				    if(op.webinar_id)
				    	temp_op_prod += op.webinar_id.title + ", "
				    if(op.seminar_id)
				    	temp_op_prod += op.seminar_id.title + ", "
				});
				temp_op_prod = temp_op_prod.slice(0, -2);
			}
			tempobj.products = temp_op_prod ; 


			tempobj.email =  result[i].user_id ? result[i].user_id.email : ''
			tempobj.total =  result[i].total;
			tempobj.status =  config.order_statuses[result[i].status]  ;
			tempobj.created =  result[i].created_at ? result[i].created_at.toDateString() : '';
			temp_arr[i] = tempobj; 
		}		

		const dataset = temp_arr
		const report = excel.buildExport(
		  [ 
		    {
		      name: 'Orders Report', 
		      specification: specification, 
		      data: dataset 
		    }
		  ]
		);
		 
		
		res.attachment('order_report.xlsx'); 
		return res.send(report);
		
	}); 




};
