const Form = include("models/Form.js");
const FormInput = include("models/FormInput.js");
const StaticPage = include("models/StaticPage.js");

exports.list = function(req, res) {	
	var query = Form.find();   	
    query.sort({ created_at: -1 });
    query.exec(function (err, result) {    	
  		res.render('admin/forms/list',{ title: 'Forms List',forms:result});       	
	}); 
	 	    
};

exports.add = async function(req, res) {

    if(req.method == 'POST')
    {
    	req.checkBody('slug', 'Slug is required').notEmpty();
   		req.checkBody('title', 'Title is required').notEmpty();
   		req.checkBody('body.en', 'Body is required').notEmpty();
		var errors = req.validationErrors();
		if(errors){						
			res.render('admin/forms/add', {title: 'Add Static Page', errors: errors,reqBody:req.body});			  		
		}
		else
		{
			var staticpage = new StaticPage();		
			staticpage.slug =req.body.slug;
			staticpage.title =req.body.title;
			staticpage.body =req.body.body.en;
			staticpage.banner_image =req.body.banner_image;	
			staticpage.testimonials =req.body.testimonials;	
			staticpage.meta_title =req.body.meta_title;	
			staticpage.meta_description =req.body.meta_description;	

			staticpage.save(async function (err, newstaticpage) {				
				if(err)
				{
					req.flash('success', err.message);
					res.render('admin/forms/add', {title: 'Add Static Page', reqBody:req.body});			  		
				}
				else
				{					
					for (var i = 0; i < res.locals.activeLanguage.length; i++) {
						var langCode = res.locals.activeLanguage[i].code;
						var staticpagecontent = new StaticPageContent();
						staticpagecontent.static_page_id =newstaticpage._id;
						staticpagecontent.lang=langCode;						
						staticpagecontent.body=req.body.body[langCode];							
						await staticpagecontent.save();

						newstaticpage.lang_content.push(staticpagecontent);
						await newstaticpage.save();
					}				

					req.flash('success', 'Static Page Added Successfully');			
    				return res.redirect('/admin/forms/list');	
				}			
					  			
			});  		
		}
    }
    else
    {
    	res.render('admin/forms/add',{ title: 'Add Form'});
    }
     
};

exports.edit = async function(req, res) {
	
	var testimonial = new Testimonial();
	var testimonials = await testimonial.getAll();
	console.log(testimonials);

    if(req.method == 'POST')
    {    	
    	req.checkBody('slug', 'Slug is required').notEmpty();
   		req.checkBody('title', 'Title is required').notEmpty();
   		req.checkBody('body', 'Body is required').notEmpty();
		var errors = req.validationErrors();
		if(errors){						
			res.render('admin/forms/edit', {title: 'Edit Static Page', errors: errors,reqBody:req.body,testimonials:testimonials});			  		
		}
		else
		{			
			var staticpage = {};					
			staticpage.slug =req.body.slug;
			staticpage.title =req.body.title; 			
			staticpage.body =req.body.body.en;	
			staticpage.banner_image =req.body.banner_image;	
			staticpage.page_video =req.body.page_video;	
			staticpage.testimonials =req.body.testimonials;	
			staticpage.meta_title =req.body.meta_title;	
			staticpage.meta_description =req.body.meta_description;		
			StaticPage.findByIdAndUpdate( req.params.id, staticpage , function (err) {				
				if(err)
				{
					req.flash('success', err.message);
					res.render('admin/forms/edit', {title: 'Edit Static Page', reqBody:req.body,testimonials:testimonials});			  		
				}
				else
				{
					var query = StaticPage.findOne({'_id' : req.params.id}).populate('lang_content');      	
			    	query.exec(function (err, result) {    						    		
			    		for (var i = 0; i < result.lang_content.length; i++) {
			    			var k = result.lang_content[i];
			    			var langCode = k.lang;
			    			var staticpagecontent = {};				    			
			    			staticpagecontent.body=req.body.body[langCode];		    						    			
			    			StaticPageContent.findByIdAndUpdate( k._id, staticpagecontent , function (err) {
			    			});	
						}				  			
					});

					req.flash('success', 'Static Page Updated Successfully');			
    				return res.redirect('/admin/forms/list');	
				}			
					  			
			});  		
		}
    }
    else
    {
    	var query = StaticPage.findOne({'_id' : req.params.id}).populate('lang_content');      	
    	query.exec(function (err, result) {     		  		    		
  			res.render('admin/forms/edit',{ title: 'Edit Static Page',reqBody:result,testimonials:testimonials});       	
		});
    	
    }
     
};

exports.delete = function(req, res) {	
	var query = StaticPage.findOne({'_id' : req.params.id}).populate('lang_content');        	
	query.exec(function (err, result) {
		for (var i = 0; i < result.lang_content.length; i++) {
			var k = result.lang_content[i];			
			StaticPageContent.remove({_id:k._id}).exec();			
		}
	});
	StaticPage.remove({_id:req.params.id}).exec();
	req.flash('success', 'Static Page Deleted Successfully');
	res.redirect('/admin/forms/list');
    
};

exports.view = function(req, res) {	
    var query = QuoteForm.find({'product_id' : req.params.id}).sort({ position: 1 });     	
	query.exec(function (err, result) {			
			res.render('admin/quote_form/view',{ title: 'View Quote Form',product_id:req.params.id,type:req.params.type,results:result});       	
	});       	
     
};

exports.input_add = async function(req, res) {	

    if(req.method == 'POST')
    {
    	req.checkBody('type', 'Type is required').notEmpty();   		
    	req.checkBody('title', 'Title is required').notEmpty();
    	//req.checkBody('slug', 'Slug is required').notEmpty();
    	if(req.body.type=='selectbox')  		
    	{
    		req.checkBody('options', 'Options is required').notEmpty();
    	}
		var errors = req.validationErrors();
		if(errors){						
			res.render('admin/quote_form/add', {title: 'Add Input', errors: errors,product_id:req.params.id,type:req.params.type,reqBody:req.body});			  		
		}
		else
		{
			var quoteform = new QuoteForm();		
			quoteform.product_id =req.params.id;
			quoteform.product_type = req.params.type;
			quoteform.type =req.body.type;
			quoteform.title =req.body.title;
			//quoteform.slug =req.body.slug;
			quoteform.options =req.body.options;
			quoteform.placeholder =req.body.placeholder;
			quoteform.required = 0;
			if(typeof req.body.required !== "undefined" && req.body.required == 1)
    		{
				quoteform.required = 1;
			}
			quoteform.position =req.body.position;
			quoteform.status = 1;		
			quoteform.save(function (err) {				
				if(err)
				{
					req.flash('success', err.message);
					res.render('admin/quote_form/add', {title: 'Add Input',product_id:req.params.id,type:req.params.type, reqBody:req.body});			  		
				}
				else
				{
					req.flash('success', 'Input Added Successfully');			
    				return res.redirect('/admin/quote_form/view/'+req.params.id+'/'+req.params.type);	
				}			
					  			
			});  		
		}
    }
    else
    {
    	res.render('admin/quote_form/add',{ title: 'Add New Input',product_id:req.params.id,type:req.params.type});
    }
     
};

exports.input_edit = async function(req, res) {	
	
	if(req.method == 'POST')
    {
    	req.checkBody('type', 'Type is required').notEmpty();   		
    	req.checkBody('title', 'Title is required').notEmpty();
    	//req.checkBody('slug', 'Slug is required').notEmpty();
    	if(req.body.type=='selectbox')  		
    	{
    		req.checkBody('options', 'Options is required').notEmpty();
    	}
		var errors = req.validationErrors();
		if(errors){						
			res.render('admin/quote_form/add', {title: 'Add Input', errors: errors,product_id:req.body.product_id,type:req.params.type,reqBody:req.body});			  		
		}
		else
		{			
			var quoteform = {};								
			quoteform.type =req.body.type;
			quoteform.title =req.body.title;
			//quoteform.slug =req.body.slug;
			quoteform.options =req.body.options;
			quoteform.placeholder =req.body.placeholder;
			quoteform.required = 0;
			if(typeof req.body.required !== "undefined" && req.body.required == 1)
    		{
				quoteform.required = 1;
			}
			quoteform.position =req.body.position;
			quoteform.status = 1;								
			QuoteForm.findByIdAndUpdate( req.params.id, quoteform , function (err) {								
				if(err)
				{
					req.flash('success', err.message);
					res.render('admin/quote_form/edit', {title: 'Edit Form Input', product_id:req.body.product_id,type:req.params.type, reqBody:req.body});			  		
				}
				else
				{
					req.flash('success', 'Form Input Updated Successfully');			
    				return res.redirect('/admin/quote_form/view/'+req.body.product_id+'/'+req.params.type);	
				}			
					  			
			});  		
		}
    }
    else
    {
    	var query = QuoteForm.findOne({'_id' : req.params.id});        	
    	query.exec(function (err, result) {
  			res.render('admin/quote_form/edit',{ title: 'Edit Form Input',reqBody:result,product_id:result.product_id,type:req.params.type});       	
		});
    	
    }
     
};

exports.input_delete = function(req, res) {	
	QuoteForm.remove({_id:req.params.fid}).exec();
	req.flash('success', 'Form Input Deleted Successfully');
	res.redirect('/admin/quote_form/view/'+req.params.id+'/'+req.params.type);
    
};