var Testimonial = include("models/Testimonial.js");

exports.list = function(req, res) {
   	var query = Testimonial.find();    
    query.sort({ created_at: -1 });
    query.exec(function (err, result) {
  		res.render('admin/testimonials/list',{ title: 'Testimonials List',testimonials:result});       	
	}); 
	 	    
};

exports.add = function(req, res) {	
    if(req.method == 'POST')
    {
    	//req.checkBody('name', 'Name is required').notEmpty(); 
    	req.checkBody('feedback', 'Feedback is required').notEmpty(); 
    	req.checkBody('image', 'Image is required').notEmpty();
    	req.checkBody('type', 'Type is required').notEmpty();

		var errors = req.validationErrors();
		if(errors){						
			res.render('admin/testimonials/add', {title: 'Add Testimonials', errors: errors,reqBody:req.body});			  		
		}
		else
		{
			var testimonial = new Testimonial();		
			testimonial.name =req.body.name;
			testimonial.facility =req.body.facility;
			testimonial.feedback =req.body.feedback;
			testimonial.image="";
			testimonial.status = 1;
			testimonial.image =req.body.image;
			testimonial.type =req.body.type;
			testimonial.home_page =req.body.home_page;

			testimonial.save(function (err) {				
				if(err)
				{
					req.flash('success', err.message);
					res.render('admin/testimonials/add', {title: 'Add Testimonials', reqBody:req.body});			  		
				}
				else
				{
					req.flash('success', 'Testimonials Added Successfully');			
    				return res.redirect('/admin/testimonials/list');	
				}			
					  			
			});  		
		}
    }
    else
    {
    	res.render('admin/testimonials/add',{ title: 'Add Testimonials'});
    }
     
};

exports.edit = function(req, res) {	
    if(req.method == 'POST')
    {
    	//req.checkBody('name', 'Name is required').notEmpty(); 
    	req.checkBody('type', 'Type is required').notEmpty();
    	req.checkBody('feedback', 'Feedback is required').notEmpty();	
		var errors = req.validationErrors();
		if(errors){						
			res.render('admin/testimonials/edit', {title: 'Edit Faq', errors: errors,reqBody:req.body});			  		
		}
		else
		{	
			var testimonial = {};					
			testimonial.name =req.body.name;
			testimonial.facility =req.body.facility;
			testimonial.feedback =req.body.feedback;				

			if(req.body.image != "")
			{
				testimonial.image =req.body.image;				
			}
			testimonial.type =req.body.type;
			testimonial.home_page =req.body.home_page;

			Testimonial.findByIdAndUpdate( req.params.id, testimonial , function (err) {				
				if(err)
				{
					req.flash('success', err.message);
					res.render('admin/testimonials/edit', {title: 'Edit Testimonial', reqBody:req.body});			  		
				}
				else
				{
					req.flash('success', 'Testimonial Updated Successfully');			
    				return res.redirect('/admin/testimonials/list');	
				}			
					  			
			});  		
		}
    }
    else
    {
    	var query = Testimonial.findOne({'_id' : req.params.id});        	
    	query.exec(function (err, result) {
  			res.render('admin/testimonials/edit',{ title: 'Edit Testimonial',reqBody:result});       	
		});
    	
    }
     
};

exports.delete = function(req, res) {	
	Testimonial.remove({_id:req.params.id}).exec();
	req.flash('success', 'Testimonial Deleted Successfully');
	res.redirect('/admin/testimonials/list');
    
};

exports.view = function(req, res) {	
    var query = Category.findOne({'_id' : req.params.id});        	
	query.exec(function (err, result) {
			res.render('admin/testimonials/view',{ title: 'View Category',reqBody:result});       	
	});  
     
};

exports.status = function(req, res) {	
	var testimonial = {};					
	testimonial.status = req.params.status;			
	Testimonial.findByIdAndUpdate( req.params.id, testimonial , function (err) {
		req.flash('success', 'Testimonial Status Changed Successfully');
		res.redirect('/admin/testimonials/list');
	});	
    
};
