var Topic = include("models/Topic.js");

exports.list = function(req, res) {
   	var query = Topic.find();    
    query.sort({ created_at: -1 });
    query.exec(function (err, result) {
  		res.render('admin/topics/list',{ title: 'Topic List',topics:result});       	
	}); 
};

exports.add = function(req, res) {	
    if(req.method == 'POST')
    {
    	req.checkBody('topic', 'Topic is required').notEmpty();   	
    	req.checkBody('slug', 'Slug is required').notEmpty();   	
		var errors = req.validationErrors();
		if(errors){						
			res.render('admin/topics/add', {title: 'Add Topic', errors: errors,reqBody:req.body});			  		
		}
		else
		{
			var topicdata = new Topic();		
			topicdata.topic =req.body.topic;
			topicdata.slug =req.body.slug;	
			topicdata.status = 1;
			topicdata.save(function (err) {				
				if(err)
				{
					req.flash('success', err.message);
					res.render('admin/topics/add', {title: 'Add Topic', reqBody:req.body});			  		
				}
				else
				{
					req.flash('success', 'Topic Added Successfully');			
    				return res.redirect('/admin/topics/list');	
				}			
					  			
			});  		
		}
    }
    else
    {
    	res.render('admin/topics/add',{ title: 'Add Topic'});
    }
};

exports.edit = function(req, res) {	
    if(req.method == 'POST')
    {
    	req.checkBody('topic', 'Topic is required').notEmpty();   	
    	req.checkBody('slug', 'Slug is required').notEmpty();   
		var errors = req.validationErrors();
		if(errors){						
			res.render('admin/topics/edit', {title: 'Edit Topic', errors: errors,reqBody:req.body});			  		
		}
		else
		{			
			var topicdata = {};					
			topicdata.topic =req.body.topic;		
			topicdata.slug =req.body.slug;		
			Topic.findByIdAndUpdate( req.params.id, topicdata , function (err) {				
				if(err)
				{
					req.flash('success', err.message);
					res.render('admin/topics/edit', {title: 'Edit Topic', reqBody:req.body});			  		
				}
				else
				{
					req.flash('success', 'Topic Updated Successfully');			
    				return res.redirect('/admin/topics/list');	
				}			
			});  		
		}
    }
    else
    {       	
    	var query = Topic.findOne({'_id' : req.params.id});        	
    	query.exec(function (err, result) {    		
  			res.render('admin/topics/edit',{ title: 'Edit Topic',reqBody:result});       	
		}); 
    }
};

exports.delete = function(req, res) {	
	Topic.remove({_id:req.params.id}).exec();
	req.flash('success', 'Topic Deleted Successfully');
	res.redirect('/admin/topics/list');
};

exports.status = function(req, res) {	
	var topicdata = {};					
	topicdata.status = req.params.status;			
	Topic.findByIdAndUpdate( req.params.id, topicdata , function (err) {
		req.flash('success', 'Topic Status Changed Successfully');
		res.redirect('/admin/topics/list');
	});
};