const Banner = include("models/Banner.js");
const BannerPage = include("models/BannerPage.js");
const fs = require('fs');
exports.list = async function(req, res) {	 
	let requestSegments = req.path.split('/');
	var query = Banner.find(); 
    query.sort({ created_at: -1 });
    query.exec(function (err, result) {
  		res.render('admin/banners/list',{ title: 'Banner List',banners:result});       	
	}); 
	 	    
};

exports.add = async function(req, res) {

    if(req.method == 'POST')
    {
    	req.checkBody('type', 'Type is required').notEmpty();
   		//req.checkBody('title', 'Title is required').notEmpty();
		//req.checkBody('banner_text_one', 'Banner text line 1 is required').notEmpty();
		//req.checkBody('image', 'Image is required').notEmpty();
		var errors = req.validationErrors();
		if(errors){			
			res.render('admin/banners/add', {title: 'Add Banner',errors: errors,reqBody:req.body});			  		
		}
		else
		{
			var banner = new Banner();		
			banner.type =req.body.type;	
			banner.title =req.body.title;	
			banner.banner_text_one =req.body.banner_text_one;
			banner.banner_text_two =req.body.banner_text_two;
			banner.image =req.body.image;	
			if(req.body.order != "")
			{
				banner.order =req.body.order;
			}			
			banner.status = 1;			
			banner.save(async function (err, newbanner) {		
				if(err)
				{
					req.flash('success', err.message);
					res.render('admin/banners/add', {title: 'Add Banner', reqBody:req.body});			  		
				}
				else
				{					

					req.flash('success', 'Banner Added Successfully');			
    				return res.redirect('/admin/banners/list');	
				}			
					  			
			});  		
		}
    }
    else
    {
    	res.render('admin/banners/add',{ title: 'Add Banner'});
    }
     
};

exports.edit = function(req, res) {	
    if(req.method == 'POST') 
    {
    	req.checkBody('type', 'Type is required').notEmpty();
   		//req.checkBody('banner_text_one', 'Banner Text One is required').notEmpty();
		var errors = req.validationErrors();
		if(errors){			
			res.render('admin/banners/edit', {title: 'Edit Banner', errors: errors,reqBody:req.body});			  		
		}
		else
		{			
			var banner = {};					
			banner.type =req.body.type;
			banner.title =req.body.title;
			
			banner.banner_text_one =req.body.banner_text_one;
			banner.banner_text_two =req.body.banner_text_two;
			if(req.body.order != "")
			{
				banner.order =req.body.order;
			}
			if(req.body.image != "")
			{
				banner.image =req.body.image;				
  			}			
			Banner.findByIdAndUpdate( req.params.id, banner , function (err) {				
				if(err)
				{
					req.flash('success', err.message);
					res.render('admin/banners/edit', {title: 'Edit Banner', reqBody:req.body,req:req});			  		
				}
				else
				{
					req.flash('success', 'Banner Updated Successfully');			
    				return res.redirect('/admin/banners/list');	
				}			
					  			
			});  		
		}
    }
    else
    {
    	var query = Banner.findOne({'_id' : req.params.id});        	
    	query.exec(function (err, result) {
  			res.render('admin/banners/edit',{ title: 'Edit Banner',reqBody:result});     	
		});
    	
    }
     
};

exports.delete = function(req, res) {	
	Banner.remove({_id:req.params.id}).exec();
	req.flash('success', 'Banner Deleted Successfully');
	res.redirect('/admin/banners/list');
    
};

exports.status = function(req, res) {	
	var banner = {};					
	banner.status = req.params.status;			
	Banner.findByIdAndUpdate( req.params.id, banner , function (err) {
		req.flash('success', 'Banner Status Changed Successfully');
		res.redirect('/admin/banners/list');
	});
    
};

exports.view = async function(req, res) {	
    var query = Banner.findOne({'_id' : req.params.id}).populate('lang_content');        	
	query.exec(function (err, result) {
			res.render('admin/banners/view',{ title: 'View Banner',reqBody:result});       	
	});  
     
};

exports.page = async function(req, res) {

    if(req.method == 'POST'){    	
		//req.checkBody('banner.static_page', 'Static Page Banner is required').notEmpty();		
		var errors = req.validationErrors();
		if(errors){						
			res.render('admin/banners/page',{ title: 'Page Banner Setting',errors: errors,reqBody:req.body});			  		
		}
		else
		{	
			BannerPage.remove({}, function(err) {	            
        	});

			for (var name in req.body){
				var image = req.body[name];

				var banner = new BannerPage();		
				banner.name = name;				
				banner.image = image;							
				banner.save();
			}			

			req.flash('success', 'Banner Saved Successfully');
			return res.redirect('/admin/banners/page');
		}
    }
    else
    {
    	var query = BannerPage.find();        	
    	query.exec(function (err, result) {    		
    		var banner=[];
    		result.forEach(function(data) {
    			banner[data.name] = data.image;
    		});    		
  			res.render('admin/banners/page',{ title: 'Page Banner Setting',reqBody:banner});     	
		});    	
    }
     
};