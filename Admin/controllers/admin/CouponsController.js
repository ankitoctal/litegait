var Coupon = include("models/Coupon.js");
var User = include("models/User.js");
var Product = include("models/Product.js");
var Part = include("models/Part.js");
var Webinar = include("models/Webinar.js");
var Seminar = include("models/Seminar.js");
const Category = include("models/Category.js");

var mongoose = require('mongoose');

exports.list = function(req, res) {
   	var query = Coupon.find();    
    query.sort({ created_at: -1 });
    query.exec(function (err, result) {
  		res.render('admin/coupons/list',{ title: 'Discount Coupon List',coupons:result});       	
	}); 
	 	    
};

exports.add = async function(req, res) {	
	
	const users = await User.find().select('_id name email');
	const products = await Product.find().select('_id name');
	const parts = await Part.find().select('_id name');	
	const webinars = await Webinar.find().select('_id title');	
	const seminars = await Seminar.find().select('_id title');	

	var allproducts = products.concat(parts,webinars,seminars);

	const categories = await Category.where({$or:[{'type':'product'},{'type':'parts'},{'type':'accessories'},{'type':'webinar'}]}).select('name type parent_id').find();

    if(req.method == 'POST')
    {    	
    	req.checkBody('title', 'Title is required').notEmpty(); 
    	req.checkBody('code', 'Code is required').notEmpty(); 
    	req.checkBody('type', 'Discount Type is required').notEmpty(); 
    	req.checkBody('expire', 'Expire Date is required').notEmpty();     	

    	if(typeof req.body.type !== "undefined" && req.body.type=='fix')
    	{    		
    		req.checkBody('amount', 'Discount Amount is required').notEmpty(); 
    	}
    	else if(typeof req.body.type !== "undefined" && req.body.type=='percent')
    	{    		
    		req.checkBody('percent', 'Discount Percenteage is required').notEmpty(); 
    	}

		var errors = req.validationErrors();
		if(errors){						
			res.render('admin/coupons/add', {title: 'Add Discount Coupon', errors: errors,users:users,allproducts:allproducts,reqBody:req.body});			  		
		}
		else
		{
			var coupon = new Coupon();		
			coupon.title =req.body.title;
			coupon.code =req.body.code;
			coupon.type =req.body.type;
			coupon.amount =req.body.amount;
			coupon.percent =req.body.percent;
			coupon.min =req.body.min;
			coupon.max =req.body.max;
			coupon.expire =req.body.expire;
			coupon.single_use = 0 ;
			//coupon.users = "";
			coupon.applied_on = req.body.applied_on;
			coupon.category = req.body.category;
			coupon.users = req.body.users;
			//coupon.products = "";

			if(typeof req.body.single_use !== "undefined" )
    		{    		
    			coupon.single_use = 1;
    		}
    		/* if(typeof req.body.users !== "undefined" )
    		{    		
    			coupon.users = req.body.users;
    		} */
    		/*if(typeof req.body.products !== "undefined" )
    		{    		
    			coupon.products = req.body.products;
    		}*/

			coupon.status = 1;
			coupon.save(function (err) {				
				if(err)
				{
					req.flash('success', err.message);
					res.render('admin/coupons/add', {title: 'Add Discount Coupon',users:users,allproducts:allproducts,categories:categories, reqBody:req.body});			  		
				}
				else
				{
					req.flash('success', 'Discount Coupon Added Successfully');			
    				return res.redirect('/admin/coupons/list');	
				}			
					  			
			});  		
		}
    }
    else
    {
    	res.render('admin/coupons/add',{ title: 'Add Discount Coupon',users:users,allproducts:allproducts,categories:categories});
    }
     
};

exports.edit = async function(req, res) {
	
	const users = await User.find().select('_id name email');
	const products = await Product.find().select('_id name');
	const parts = await Part.find().select('_id name');	
	const webinars = await Webinar.find().select('_id title');	
	const seminars = await Seminar.find().select('_id title');	
	var allproducts = products.concat(parts,webinars,seminars);	

	const categories = await Category.where({$or:[{'type':'product'},{'type':'parts'},{'type':'accessories'},{'type':'webinar'}]}).select('name type parent_id').find();			

    if(req.method == 'POST')
    {
    	req.checkBody('title', 'Title is required').notEmpty(); 
    	req.checkBody('code', 'Code is required').notEmpty(); 
    	req.checkBody('type', 'Discount Type is required').notEmpty(); 
    	req.checkBody('expire', 'Expire Date is required').notEmpty();     	

    	if(typeof req.body.type !== "undefined" && req.body.type=='fix')
    	{    		
    		req.checkBody('amount', 'Discount Amount is required').notEmpty(); 
    	}
    	else if(typeof req.body.type !== "undefined" && req.body.type=='percent')
    	{    		
    		req.checkBody('percent', 'Discount Percenteage is required').notEmpty(); 
    	}   		
		var errors = req.validationErrors();
		if(errors){						
			res.render('admin/coupons/edit', {title: 'Edit Discount Coupon', errors: errors,users:users,allproducts:allproducts,categories:categories,reqBody:req.body});			  		
		}
		else
		{			
			var coupon = {};					
			coupon.title =req.body.title;
			coupon.code =req.body.code;
			coupon.type =req.body.type;
			coupon.amount =req.body.amount;
			coupon.percent =req.body.percent;
			coupon.min =req.body.min;
			coupon.max =req.body.max;
			coupon.expire =req.body.expire;
			coupon.single_use = 0 ;
			//coupon.users = "";
			coupon.applied_on = req.body.applied_on;
			coupon.category = req.body.category;
			coupon.users = req.body.users;
			//coupon.products = "";

			if(typeof req.body.single_use !== "undefined" )
    		{    		
    			coupon.single_use = 1;
    		}
    		/* if(typeof req.body.users !== "undefined" )
    		{    		
    			coupon.users = req.body.users instanceof Array ? req.body.users.toString() : '';
    		}*/ 
    		/*if(typeof req.body.products !== "undefined" )
    		{    		
    			coupon.products = req.body.products;
    		}*/

			var id = mongoose.Types.ObjectId(req.params.id);
			Coupon.findByIdAndUpdate( id, coupon , function (err) {		
				if(err)
				{
					req.flash('success', err.message);
					res.render('admin/coupons/edit', {title: 'Edit Discount Coupon',users:users,allproducts:allproducts,categories:categories, reqBody:req.body});			  		
				}
				else
				{
					req.flash('success', 'Discount Coupon Updated Successfully');							
    				return res.redirect('/admin/coupons/list');	
				}			
			});  		
		}
    }
    else
    {
    	var query = Coupon.findOne({'_id' : req.params.id});        	
    	query.lean().exec(function (err, result) {    		
    		result.expire = result.expire ? result.expire.getMonth()+1+'/'+ result.expire.getDate() +'/'+ result.expire.getFullYear() : '';		
  			res.render('admin/coupons/edit',{ title: 'Edit Discount Coupon',users:users,allproducts:allproducts,categories:categories,reqBody:result});       	
		});
    	
    }
     
};

exports.delete = function(req, res) {	
	Coupon.remove({_id:req.params.id}).exec();
	req.flash('success', 'Discount Coupon Deleted Successfully');
	res.redirect('/admin/coupons/list');
    
};

exports.view = function(req, res) {	
    var query = StaticPage.findOne({'_id' : req.params.id});        	
	query.exec(function (err, result) {
			res.render('admin/coupons/view',{ title: 'View State',reqBody:result});       	
	});  
     
};

exports.status = function(req, res) {	
	var coupon = {};					
	coupon.status = req.params.status;			
	Coupon.findByIdAndUpdate( req.params.id, coupon , function (err) {
		req.flash('success', 'Discount Coupon Status Changed Successfully');
		res.redirect('/admin/coupons/list');
	});	
    
};
