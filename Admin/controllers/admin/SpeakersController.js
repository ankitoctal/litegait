const Speaker = include("models/Speaker.js");
const SpeakerContent = include("models/SpeakerContent.js");
const Testimonial = include("models/Testimonial.js"); 

exports.list = async function(req, res) {
	
   	var query = Speaker.find();    
    query.sort({ lname: 1 });
    query.exec(function (err, result) {
    	
  		res.render('admin/speakers/list',{ title: 'Speaker List',speakers:result});       	
	}); 
	 	    
};

exports.add = async function(req, res) {

	var testimonial = new Testimonial();
	var testimonials = await testimonial.getAll();

    if(req.method == 'POST')
    {    	
    	req.body.bio = {};
    	res.locals.activeLanguage.forEach(function(language) {  			  			
  			req.body.bio[language.code] =req.body['bio['+language.code+']'] ;
		});    	
    	req.checkBody('slug', 'Slug is required').notEmpty();
    	req.checkBody('tag_line', 'Tag Line is required').notEmpty();
   		req.checkBody('fname', 'Name is required').notEmpty();   		
   		req.checkBody('bio.en', 'Bio is required').notEmpty();  
   		req.checkBody('category', 'Category is required').notEmpty();   		
   		//req.checkBody('trainers', 'Trainers is required').notEmpty();   		
   		//req.checkBody('instructors', 'Instructors is required').notEmpty();   		
   		//req.checkBody('webinars', 'Webinars is required').notEmpty();   		
   		req.checkBody('image', 'Image is required').notEmpty();  		
		var errors = req.validationErrors();
		if(errors){						
			res.render('admin/speakers/add', {title: 'Add Speaker', errors: errors,reqBody:req.body,testimonials:testimonials});			  		
		}
		else
		{
			var speaker = new Speaker();					
			//speaker.name =req.body.name;
			speaker.fname =req.body.fname;
			speaker.lname =req.body.lname;
			speaker.name =req.body.fname+' '+req.body.lname;
			speaker.slug =req.body.slug;			
			speaker.tag_line =req.body.tag_line;			
			speaker.image =req.body.image;			
			speaker.bio =req.body.bio.en;					
			speaker.category =req.body.category;	
			//speaker.trainers =req.body.trainers;
			//speaker.instructors =req.body.instructors;
			//speaker.webinars =req.body.webinars;
			speaker.testimonials =req.body.testimonials;			
			speaker.meta_title =req.body.meta_title;	
			speaker.meta_description =req.body.meta_description;
			speaker.status = 1;			
			speaker.save(async function (err,newspeaker) {				
				if(err)
				{
					req.flash('success', err.message);
					res.render('admin/speakers/add', {title: 'Add Speaker', reqBody:req.body,testimonials:testimonials});			  		
				}
				else
				{
					for (var i = 0; i < res.locals.activeLanguage.length; i++) {
						var langCode = res.locals.activeLanguage[i].code;
						var speakercontent = new SpeakerContent();
						speakercontent.speaker_id =newspeaker._id;
						speakercontent.lang=langCode;												
						speakercontent.bio=req.body.bio[langCode];
						await speakercontent.save();

						newspeaker.lang_content.push(speakercontent);
						await newspeaker.save();
					}

					req.flash('success', 'Speaker Added Successfully');			
    				return res.redirect('/admin/speakers/list');	
				}			
					  			
			});  		
		}
    }
    else
    {
    	res.render('admin/speakers/add',{ title: 'Add Speaker',testimonials:testimonials});
    }
     
};

exports.edit = async function(req, res) {	

	var testimonial = new Testimonial();
	var testimonials = await testimonial.getAll();

    if(req.method == 'POST')
    {
    	req.body.bio = {};
    	res.locals.activeLanguage.forEach(function(language) {  			  			
  			req.body.bio[language.code] =req.body['bio['+language.code+']'] ;
		});    	
    	req.checkBody('slug', 'Slug is required').notEmpty();
    	req.checkBody('tag_line', 'Tag Line is required').notEmpty();
   		req.checkBody('fname', 'Name is required').notEmpty();   		
   		req.checkBody('bio.en', 'Bio is required').notEmpty();  
   		req.checkBody('category', 'Category is required').notEmpty();   		
   		//req.checkBody('trainers', 'Trainers is required').notEmpty();   		
   		//req.checkBody('instructors', 'Instructors is required').notEmpty();   		
   		//req.checkBody('webinars', 'Webinars is required').notEmpty();   		     		
		var errors = req.validationErrors();
		if(errors){						
			res.render('admin/speakers/edit', {title: 'Edit Speaker', errors: errors,reqBody:req.body,testimonials:testimonials});			  		
		}
		else
		{			
			var speaker = {};					
			//speaker.name =req.body.name;
			speaker.fname =req.body.fname;
			speaker.lname =req.body.lname;
			speaker.name =req.body.fname+' '+req.body.lname;
			speaker.slug =req.body.slug;			
			speaker.tag_line =req.body.tag_line;			
			if(req.body.image != "")
			{
				speaker.image =req.body.image;				
			}			
			speaker.bio =req.body.bio.en;					
			speaker.category =req.body.category;	
			//speaker.trainers =req.body.trainers;
			//speaker.instructors =req.body.instructors;
			//speaker.webinars =req.body.webinars;
			speaker.testimonials =req.body.testimonials;						
			speaker.meta_title =req.body.meta_title;	
			speaker.meta_description =req.body.meta_description;				
			Speaker.findByIdAndUpdate( req.params.id, speaker , function (err) {				
				if(err)
				{
					req.flash('success', err.message);
					res.render('admin/speakers/edit', {title: 'Edit Speaker', reqBody:req.body,testimonials:testimonials});			  		
				}
				else
				{
					var query = Speaker.findOne({'_id' : req.params.id}).populate('lang_content');      	
			    	query.exec(function (err, result) {    						    		
			    		for (var i = 0; i < result.lang_content.length; i++) {
			    			var k = result.lang_content[i];
			    			var langCode = k.lang;
			    			var speakercontent = {};				    						    			
			    			speakercontent.bio=req.body.bio[langCode];
			    			SpeakerContent.findByIdAndUpdate( k._id, speakercontent , function (err) {
			    			});	
						}
					});	

					req.flash('success', 'Speaker Updated Successfully');			
    				return res.redirect('/admin/speakers/list');	
				}			
					  			
			});  		
		}
    }
    else
    {
    	var query = Speaker.findOne({'_id' : req.params.id}).populate('lang_content');        	
    	query.lean().exec(function (err, result) {
    		
    		result.bio = {};

	    	result.lang_content.forEach(function(content) {  	    		
	    		result.bio[content.lang] =content.bio ;	  			
			});
			
  			res.render('admin/speakers/edit',{ title: 'Edit Speaker',reqBody:result,testimonials:testimonials});       	
		});
    	
    }
     
};

exports.delete = function(req, res) {	
	var query = Speaker.findOne({'_id' : req.params.id}).populate('lang_content');        	
	query.exec(function (err, result) {
		for (var i = 0; i < result.lang_content.length; i++) {
			var k = result.lang_content[i];			
			SpeakerContent.remove({_id:k._id}).exec();			
		}
	});
	Speaker.remove({_id:req.params.id}).exec();
	req.flash('success', 'Speaker Deleted Successfully');	
	res.redirect('/admin/speakers/list');
    
};

exports.view = function(req, res) {	
    var query = Category.findOne({'_id' : req.params.id});        	
	query.exec(function (err, result) {
			res.render('admin/speakers/view',{ title: 'View Category',reqBody:result});       	
	});  
     
};

exports.status = function(req, res) {	
	var speaker = {};					
	speaker.status = req.params.status;			
	Speaker.findByIdAndUpdate( req.params.id, speaker , function (err) {
		req.flash('success', 'Speaker Changed Successfully');
		res.redirect('/admin/speakers/list');
	});	
    
};
