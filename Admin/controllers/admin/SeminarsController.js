const mongoose = require('mongoose');
const Speaker = include("models/Speaker.js");
const State = include("models/State.js");
const Seminar = include("models/Seminar.js");
const SeminarContent = include("models/SeminarContent.js"); 
const Testimonial = include("models/Testimonial.js");

exports.list = async function(req, res) {

	const speakers = await Speaker.where('status',1).select('_id name').find();

   	var query = Seminar.find();    
    query.sort({ created_at: -1 }).populate('speaker_id');
    query.exec(function (err, result) {
    	
  		res.render('admin/seminars/list',{ title: 'Seminar List',seminars:result,speakers: speakers});       	
	}); 
	 	    
};

exports.add = async function(req, res) {

	const speakers = await Speaker.where('status',1).select('_id name').find();	
	var testimonial = new Testimonial();
	var testimonials = await testimonial.getAll();
	
    if(req.method == 'POST')
    {    	
    	req.body.description = {};    	
    	res.locals.activeLanguage.forEach(function(language) {  			  			
  			req.body.description[language.code] =req.body['description['+language.code+']'] ;  			
		});    	
    	req.checkBody('slug', 'Slug is required').notEmpty();
   		req.checkBody('title', 'Title is required').notEmpty();   		
   		//req.checkBody('subtitle', 'Sub Title is required').notEmpty();   		
   		req.checkBody('description.en', 'Description is required').notEmpty();     		
   		req.checkBody('speaker_id', 'Speaker is required').notEmpty();   		
   		req.checkBody('image', 'Image is required').notEmpty();   		
   		req.checkBody('price', 'Price is required').notEmpty(); 		   		  		
		var errors = req.validationErrors();
		if(errors){						
			res.render('admin/seminars/add', {title: 'Add Seminar', errors: errors,reqBody:req.body,speakers:speakers,testimonials:testimonials});
		}
		else
		{
			var seminar = new Seminar();					
			seminar.title =req.body.title;
			seminar.subtitle =req.body.subtitle;
			seminar.slug =req.body.slug;						
			seminar.speaker_id =req.body.speaker_id;			
			seminar.image =req.body.image;			
			seminar.description =req.body.description.en;
			seminar.date_time =req.body.date_time;			
			seminar.location =req.body.location;						
			seminar.price =req.body.price;	
			seminar.position =req.body.position;	
			seminar.testimonials =req.body.testimonials;		
			seminar.meta_title =req.body.meta_title;	
			seminar.meta_description =req.body.meta_description;
			seminar.status = 1;	
			seminar.save(async function (err,newseminar) {				
				if(err)
				{
					req.flash('success', err.message);
					res.render('admin/seminars/add', {title: 'Add Seminar', errors: errors,reqBody:req.body,speakers:speakers,testimonials:testimonials});
				}
				else
				{
					for (var i = 0; i < res.locals.activeLanguage.length; i++) {
						var langCode = res.locals.activeLanguage[i].code;
						var seminarcontent = new SeminarContent();
						seminarcontent.seminar_id =newseminar._id;
						seminarcontent.lang=langCode;												
						seminarcontent.description=req.body.description[langCode];						
						await seminarcontent.save();

						newseminar.lang_content.push(seminarcontent);
						await newseminar.save();
					}

					req.flash('success', 'Seminar Added Successfully');			
    				return res.redirect('/admin/seminars/list');	
				}			
					  			
			});  		
		}
    }
    else
    {
    	res.render('admin/seminars/add',{ title: 'Add Seminar',speakers:speakers,testimonials:testimonials});
    }
     
};

exports.edit = async function(req, res) {

	const speakers = await Speaker.where('status',1).select('_id name').find();
	var testimonial = new Testimonial();
	var testimonials = await testimonial.getAll();		

    if(req.method == 'POST')
    {
    	req.body.description = {};    	
    	res.locals.activeLanguage.forEach(function(language) {  			  			
  			req.body.description[language.code] =req.body['description['+language.code+']'] ;  			
		});    	
    	req.checkBody('slug', 'Slug is required').notEmpty();
   		req.checkBody('title', 'Title is required').notEmpty(); 
   		//req.checkBody('subtitle', 'Sub Title is required').notEmpty();   		
   		req.checkBody('description.en', 'Description is required').notEmpty();    		
   		req.checkBody('speaker_id', 'Speaker is required').notEmpty();  		
   		req.checkBody('image', 'Image is required').notEmpty();  		
   		req.checkBody('price', 'Price is required').notEmpty();   		   		
		var errors = req.validationErrors();
		if(errors){						
			res.render('admin/seminars/edit', {title: 'Add Seminar', errors: errors,reqBody:req.body,speakers:speakers,testimonials:testimonials});
		}
		else
		{			
			var seminar = {};					
			seminar.title =req.body.title;
			seminar.subtitle =req.body.subtitle;
			seminar.slug =req.body.slug;						
			seminar.speaker_id =req.body.speaker_id;								
			seminar.image =req.body.image;								
			seminar.description =req.body.description.en;			
			seminar.date_time =req.body.date_time;			
			seminar.location =req.body.location;			
			seminar.price =req.body.price;
			seminar.position =req.body.position;
			seminar.testimonials =req.body.testimonials;
			seminar.meta_title =req.body.meta_title;	
			seminar.meta_description =req.body.meta_description;							
			Seminar.findByIdAndUpdate( req.params.id, seminar , function (err) {				
				if(err)
				{
					req.flash('success', err.message);
					res.render('admin/seminars/edit', {title: 'Add Seminar', errors: errors,reqBody:req.body,speakers:speakers,testimonials:testimonials});
				}
				else
				{
					var query = Seminar.findOne({'_id' : req.params.id}).populate('lang_content');      	
			    	query.exec(function (err, result) {    						    		
			    		for (var i = 0; i < result.lang_content.length; i++) {
			    			var k = result.lang_content[i];
			    			var langCode = k.lang; 
			    			var seminarcontent = {};				    						    			
			    			seminarcontent.description=req.body.description[langCode];			    			
			    			SeminarContent.findByIdAndUpdate( k._id, seminarcontent , function (err) {
			    			});	
						}
					});	

					req.flash('success', 'Seminar Updated Successfully');			
    				return res.redirect('/admin/seminars/list');	
				}			
					  			
			});  		
		}
    }
    else
    {
    	var query = Seminar.findOne({'_id' : req.params.id}).populate('lang_content');        	
    	query.lean().exec(function (err, result) {    		
    		
    		result.description = {};

	    	result.lang_content.forEach(function(content) {  	    		
	    		result.description[content.lang] =content.description ;	    		
			});
			
  			res.render('admin/seminars/edit',{ title: 'Edit Seminar',reqBody:result,speakers:speakers,testimonials:testimonials});       	
		});
    	
    }
     
};

exports.delete = function(req, res) {	
	var query = Seminar.findOne({'_id' : req.params.id}).populate('lang_content');        	
	query.exec(function (err, result) {
		for (var i = 0; i < result.lang_content.length; i++) {
			var k = result.lang_content[i];			
			SeminarContent.remove({_id:k._id}).exec();			
		}
	});
	Seminar.remove({_id:req.params.id}).exec();
	req.flash('success', 'Seminar Deleted Successfully');	
	res.redirect('/admin/seminars/list');
    
};

exports.view = function(req, res) {	
    var query = Category.findOne({'_id' : req.params.id});        	
	query.exec(function (err, result) {
			res.render('admin/speakers/view',{ title: 'View Category',reqBody:result});       	
	});  
};

exports.status = function(req, res) {	
	var seminar = {};					
	seminar.status = req.params.status;			
	Seminar.findByIdAndUpdate( req.params.id, seminar , function (err) {
		req.flash('success', 'Seminar Status Changed Successfully');
		res.redirect('/admin/seminars/list');
	});
};


exports.copy = function(req, res) {	
    var query = Seminar.findOne({'_id' : req.params.id}).populate('lang_content');        	
	query.lean().exec(function (err, result) {    		
		
		var doc = new Seminar(result);
        doc._id = mongoose.Types.ObjectId();
        doc.isNew = true;
        doc.lang_content = [];        
        doc.title = doc.title + '- copy';
        doc.slug = doc.slug + '- copy';
        doc.save(async function(err, newdoc) {
            for (var i = 0; i < result.lang_content.length; i++) {
                var subdoc = new SeminarContent(result.lang_content[i]);
                subdoc._id = mongoose.Types.ObjectId();
                subdoc.seminar_id = newdoc._id;
                subdoc.isNew = true;
                await subdoc.save();
                newdoc.lang_content.push(subdoc);
                await newdoc.save();
            }
        });
        req.flash('success', 'Seminar Copied Successfully');
        res.redirect('/admin/seminars/list');
		
		
	}); 
};