const mongoose = require('mongoose');
const Region = include("models/Region.js");
const Category = include("models/Category.js");
const Product = include("models/Product.js");
const ProductContent = include("models/ProductContent.js");
const ProductGallery = include("models/ProductGallery.js");
const ProductFeatured = include("models/ProductFeatured.js");
const ProductSpecification = include("models/ProductSpecification.js");
const ProductSerialno = include("models/ProductSerialno.js");
const Testimonial = include("models/Testimonial.js");
const ProductFiles = include("models/ProductFiles.js");
const ProductSpecificationGroup = include("models/ProductSpecificationGroup.js");

const readXlsxFile = require('read-excel-file/node');
var multer = require('multer')
var path = require("path");

exports.list = async function(req, res) {
    //const categories = await Category.find();	  

    var query = Product.find().populate('category');
    //query.populate('lang_content');   
    query.sort({ created_at: -1 });
    query.exec(function(err, result) {
        res.render('admin/products/list', { title: 'Product List', products: result });
    });
};

exports.add = async function(req, res) {

    const regions = await Region.find();
    const categories = await Category.where('type', 'product').find();
    const productSpecification = await ProductSpecification.find().sort({ created_at: 1, _id: 1 });
    const products = await Product.find().select('_id name').exec();
    var testimonial = new Testimonial();
    var testimonials = await testimonial.getAll();
    /*var specificationRow = {};
    productSpecification.forEach(async function(specification) {    	   	
    	var keys = Object.keys(specification.toObject());
    	keys.forEach(async function(key) {    		
    		specificationRow[key]=key;
    	}); 
    }); */
    var specificationRow = await ProductSpecificationGroup.find();

    if (req.method == 'POST') { 
        req.body.description = {};
        res.locals.activeLanguage.forEach(function(language) {
            req.body.description[language.code] = req.body['description[' + language.code + ']'];
        });

        req.checkBody('slug', 'Slug is required').notEmpty();
        req.checkBody('name', 'Name is required').notEmpty();
        req.checkBody('category', 'Category is required').notEmpty();
        req.checkBody('region', 'Region is required').notEmpty();

        var errors = req.validationErrors();
        if (errors) {
            res.render('admin/products/add', { title: 'Add Product', products: products, regions: regions, categories: categories, productSpecification: productSpecification, specificationRow: specificationRow, errors: errors, reqBody: req.body, testimonials: testimonials });
        } else {
            var product = new Product();
            product.name = req.body.name;
            product.slug = req.body.slug;
            product.category = req.body.category;
            product.region = req.body.region;
            product.image = req.body.image;
            product.description = req.body.description.en;
            product.video = req.body.video;
            product.stock = req.body.stock;
            product.price = req.body.price;
            product.price_request = req.body.price_request;
            product.featured_product_main = req.body.featured_product_main;
            product.schedule_service = req.body.schedule_service;
            if (typeof product.price_request == "undefined") {
                product.price_request = 0;
            }
            if (typeof product.featured_product_main == "undefined") {
                product.featured_product_main = 0;
            }
            if (typeof product.schedule_service == "undefined") {
                product.schedule_service = 0;
            }
            product.specification_related = req.body.specification_related ? req.body.specification_related.toString() : '';
            product.specification_default = req.body.specification_default ? req.body.specification_default.toString() : '';
            product.specification_row = req.body.specification_row ? req.body.specification_row.toString() : '';
            product.specification_row_default = req.body.specification_row_default ? req.body.specification_row_default.toString() : '';
            product.position = req.body.position;
            product.testimonials = req.body.testimonials;
            product.related_product_text = req.body.related_product_text;
            product.related_product = req.body.related_product ? req.body.related_product.toString() : '';
            product.banner_image = req.body.banner_image;
            product.serial_no = req.body.serial_no;
            product.service_image = req.body.service_image;
            product.service_description = req.body.service_description;
            product.meta_title = req.body.meta_title;
            product.meta_description = req.body.meta_description;
            product.status = 1;

            product.save(async function(err, newproduct) {
                if (err) {
                    req.flash('success', err.message);
                    res.render('admin/products/add', { title: 'Add Product', products: products, regions: regions, categories: categories, productSpecification: productSpecification, specificationRow: specificationRow, errors: errors, reqBody: req.body, testimonials: testimonials });
                } else {
                    for (var i = 0; i < res.locals.activeLanguage.length; i++) {
                        var langCode = res.locals.activeLanguage[i].code;
                        var productcontent = new ProductContent();
                        productcontent.product_id = newproduct._id;
                        productcontent.lang = langCode;
                        productcontent.description = req.body.description[langCode];
                        await productcontent.save();

                        newproduct.lang_content.push(productcontent);
                        await newproduct.save();
                    }

                    /*if(typeof req.files.gallery_image !== "undefined")
					{
						if(Array.isArray(req.files.gallery_image))
						{
							req.files.gallery_image.forEach(async function(image) {										
								image.mv('public/uploads/product/'+image.name, async function(err) {
				    				if (err)
				      				return res.status(500).send(err); 

				      				var productgallery = new ProductGallery();
									productgallery.product_id =newproduct._id;
									productgallery.image=image.name;														
									await productgallery.save();

									newproduct.image_gallery.push(productgallery);
									await newproduct.save();   		
								});	
							});
						}
						else
						{
							var image = req.files.gallery_image; 
							image.mv('public/uploads/product/'+image.name, async function(err) {
			    				if (err)
			      				return res.status(500).send(err); 

			      				var productgallery = new ProductGallery();
								productgallery.product_id =newproduct._id;
								productgallery.image=image.name;														
								await productgallery.save();

								newproduct.image_gallery.push(productgallery);
								await newproduct.save();   		
							});
						}	
					}*/

                    /*Gallery Images section */
                    if (req.body.gallery_image !== "") {
                        if (Array.isArray(req.body.gallery_image)) {
                            req.body.gallery_image.forEach(async function(image) {
                                var productgallery = new ProductGallery();
                                productgallery.product_id = newproduct._id;
                                productgallery.image = image;
                                await productgallery.save();

                                newproduct.image_gallery.push(productgallery);
                                await newproduct.save();
                            });
                        } else {
                            var image = req.body.gallery_image;
                            var productgallery = new ProductGallery();
                            productgallery.product_id = newproduct._id;
                            productgallery.image = image
                            await productgallery.save();
                            newproduct.image_gallery.push(productgallery);
                            await newproduct.save();
                        }
                    }
                    /*Gallery Images section */


                    if (newproduct.featured_product_main) {
                        ProductFeatured.find().count(async function(err, count) {
                            if (count) {
                                var pfresult = await ProductFeatured.findOne().exec();
                                await ProductFeatured.deleteMany({ '_id': { $ne: pfresult._id } });
                                pfresult.product_id = newproduct._id;
                                await pfresult.save();

                                await Product.updateMany({ '_id': { $ne: newproduct._id } }, { $set: { featured_product_main: 0 } });

                            }
                        });
                    }

                    req.flash('success', 'Product Added Successfully');
                    return res.redirect('/admin/products/list');
                }

            });
        }
    } else {
        res.render('admin/products/add', { title: 'Add Product', products: products, regions: regions, categories: categories, productSpecification: productSpecification, specificationRow: specificationRow, testimonials: testimonials });
    }
};

exports.edit = async function(req, res) {

    const regions = await Region.find();
    const categories = await Category.where('type', 'product').find();
    const products = await Product.find().select('_id name').exec();
    const productSpecification = await ProductSpecification.find().sort({ created_at: 1, _id: 1 });
    /* var specificationRow = {};
    productSpecification.forEach(async function(specification) {    	   	
    	var keys = Object.keys(specification.toObject());
    	keys.forEach(async function(key) {    		
    		specificationRow[key]=key;
    	});
    });  */

    var specificationRow = await ProductSpecificationGroup.find();

    var testimonial = new Testimonial();
    var testimonials = await testimonial.getAll();

    if (req.method == 'POST') {
        req.body.description = {};

        res.locals.activeLanguage.forEach(function(language) {
            req.body.description[language.code] = req.body['description[' + language.code + ']'];
        });
        req.checkBody('slug', 'Slug is required').notEmpty();
        req.checkBody('name', 'Name is required').notEmpty();
        req.checkBody('category', 'Category is required').notEmpty();
        req.checkBody('region', 'Region is required').notEmpty();

        var errors = req.validationErrors();
        if (errors) {
            res.render('admin/products/edit', { title: 'Edit Product', products: products, regions: regions, categories: categories, productSpecification: productSpecification, specificationRow: specificationRow, errors: errors, reqBody: req.body, testimonials: testimonials });
        } else {
            var product = {};
            product.name = req.body.name;
            product.slug = req.body.slug;
            product.category = req.body.category;
            product.region = req.body.region;
            product.image = req.body.image;
            product.description = req.body.description.en;
            product.video = req.body.video;
            product.stock = req.body.stock;
            product.price = req.body.price;
            product.price_request = req.body.price_request;
            product.featured_product_main = req.body.featured_product_main;
            product.schedule_service = req.body.schedule_service;
            if (typeof product.price_request == "undefined") {
                product.price_request = 0;
            }
            if (typeof product.featured_product_main == "undefined") {
                product.featured_product_main = 0;
            }

            if (typeof product.schedule_service == "undefined") {
                product.schedule_service = 0;
            }
            product.specification_related = req.body.specification_related ? req.body.specification_related.toString() : '';
            product.specification_default = req.body.specification_default ? req.body.specification_default.toString() : '';
            product.specification_row = req.body.specification_row ? req.body.specification_row.toString() : '';
            product.specification_row_default = req.body.specification_row_default ? req.body.specification_row_default.toString() : '';
            product.position = req.body.position;
            product.testimonials = req.body.testimonials;
            product.related_product_text = req.body.related_product_text;
            product.related_product = req.body.related_product ? req.body.related_product.toString() : '';
            product.banner_image = req.body.banner_image;
            product.serial_no = req.body.serial_no;
            product.service_image = req.body.service_image;
            product.service_description = req.body.service_description;
            product.meta_title = req.body.meta_title;
            product.meta_description = req.body.meta_description;
            //product.status = 1;            
            Product.findByIdAndUpdate(req.params.id, product, function(err) {
                if (err) {
                    req.flash('success', err.message);
                    res.render('admin/products/edit', { title: 'Edit Product', products: products, regions: regions, categories: categories, productSpecification: productSpecification, specificationRow: specificationRow, errors: errors, reqBody: req.body, testimonials: testimonials });
                } else {
                    var query = Product.findOne({ '_id': req.params.id }).populate('lang_content');
                    query.exec(async function(err, result) {
                        for (var i = 0; i < result.lang_content.length; i++) {
                            var k = result.lang_content[i];
                            var langCode = k.lang;
                            var productcontent = {};
                            //productcontent.short_description=req.body.short_description[langCode];
                            productcontent.description = req.body.description[langCode];
                            ProductContent.findByIdAndUpdate(k._id, productcontent, function(err) {});
                        }

                        /*Gallery Images section */
                        if (req.body.gallery_image !== "") {
                            if (Array.isArray(req.body.gallery_image)) {
                                req.body.gallery_image.forEach(async function(image) {
                                    var productgallery = new ProductGallery();
                                    productgallery.product_id = result._id;
                                    productgallery.image = image;
                                    await productgallery.save();

                                    result.image_gallery.push(productgallery);
                                    await result.save();
                                });
                            } else {
                                var image = req.body.gallery_image;
                                var productgallery = new ProductGallery();
                                productgallery.product_id = result._id;
                                productgallery.image = image
                                await productgallery.save();

                                result.image_gallery.push(productgallery);
                                await result.save();
                            }
                        }
                        /*Gallery Images section */

                        /* if(typeof req.files.gallery_image !== "undefined")
						{
							if(Array.isArray(req.files.gallery_image))
							{
								req.files.gallery_image.forEach(async function(image) {

									image.mv('public/uploads/product/'+image.name, async function(err) {
					    				if (err)
					      				return res.status(500).send(err); 

					      				var productgallery = new ProductGallery();
										productgallery.product_id =result._id;
										productgallery.image=image.name;														
										await productgallery.save();

										result.image_gallery.push(productgallery);
										await result.save();   		
									});	
								});
							}
							else
							{
								var image = req.files.gallery_image; 
								image.mv('public/uploads/product/'+image.name, async function(err) {
				    				if (err)
				      				return res.status(500).send(err); 

				      				var productgallery = new ProductGallery();
									productgallery.product_id =result._id;
									productgallery.image=image.name;														
									await productgallery.save();

									result.image_gallery.push(productgallery);
									await result.save();   		
								});
							}	
						} */
                        if (result.featured_product_main) {
                            ProductFeatured.find().count(async function(err, count) {
                                if (count) {
                                    var pfresult = await ProductFeatured.findOne().exec();
                                    await ProductFeatured.deleteMany({ '_id': { $ne: pfresult._id } });
                                    pfresult.product_id = result._id;
                                    await pfresult.save();

                                    await Product.updateMany({ '_id': { $ne: result._id } }, { $set: { featured_product_main: 0 } });

                                }
                            });
                        }
                        req.flash('success', 'Product Updated Successfully');
                        return res.redirect('/admin/products/list');
                    });


                }

            });
        }         
    } else {

        var query = Product.findOne({ '_id': req.params.id }).populate('lang_content').populate({path: 'image_gallery',options: { sort: '_id' }});
        query.lean().exec(function(err, result) {
            if (result) {
                result.description = {};
                result.lang_content.forEach(function(content) {
                    result.description[content.lang] = content.description;
                });
            }
            res.render('admin/products/edit', { title: 'Edit Product', products: products, regions: regions, categories: categories, productSpecification: productSpecification, specificationRow: specificationRow, reqBody: result, testimonials: testimonials });
        });

    }
};

exports.delete = function(req, res) {
    var query = Product.findOne({ '_id': req.params.id }).populate('lang_content');
    query.exec(function(err, result) {
        for (var i = 0; i < result.lang_content.length; i++) {
            var k = result.lang_content[i];
            ProductContent.remove({ _id: k._id }).exec();
        }
    });
    Product.remove({ _id: req.params.id }).exec();
    req.flash('success', 'Product Deleted Successfully');
    res.redirect('/admin/products/list');
};

exports.status = function(req, res) {
    var product = {};
    product.status = req.params.status;
    Product.findByIdAndUpdate(req.params.id, product, function(err) {
        req.flash('success', 'Product Status Changed Successfully');
        res.redirect('/admin/products/list');
    });
};

exports.copy = function(req, res) {
    var query = Product.findOne({ '_id': req.params.id }).populate('lang_content').populate('image_gallery');
    query.lean().exec(function(err, result) {

        var doc = new Product(result);
        doc._id = mongoose.Types.ObjectId();
        doc.isNew = true;
        doc.lang_content = [];
        doc.image_gallery = [];
        doc.name = doc.name + '- copy';
        doc.slug = doc.slug + '- copy';
        doc.save(async function(err, newdoc) {
            for (var i = 0; i < result.lang_content.length; i++) {
                var subdoc = new ProductContent(result.lang_content[i]);
                subdoc._id = mongoose.Types.ObjectId();
                subdoc.product_id = newdoc._id;
                subdoc.isNew = true;
                await subdoc.save();
                newdoc.lang_content.push(subdoc);
                await newdoc.save();
            }

            for (var i = 0; i < result.image_gallery.length; i++) {
                var subdoc = new ProductGallery(result.image_gallery[i]);
                subdoc._id = mongoose.Types.ObjectId();
                subdoc.product_id = newdoc._id;
                subdoc.isNew = true;
                await subdoc.save();
                newdoc.image_gallery.push(subdoc);
                await newdoc.save();
            }
        });
        req.flash('success', 'Product Copied Successfully');
        res.redirect('/admin/products/list');
    });
};


exports.view = async function(req, res) {
    var query = StaticPage.findOne({ '_id': req.params.id }).populate('lang_content');
    query.exec(function(err, result) {
        res.render('admin/products/view', { title: 'View Product', reqBody: result });
    });
};

exports.gallery_delete = function(req, res) {

    var query = ProductGallery.findOne({ '_id': req.params.id });
    query.exec(function(err, result) {
        pid = result.product_id;

        Product.findByIdAndUpdate(pid, { $pull: { image_gallery: req.params.id } }, function(err) {
            ProductGallery.remove({ _id: req.params.id }).exec();
        });
    });

    res.send('deleted');
};


exports.featured_product = async function(req, res) {

    if (req.method == 'POST') {
        var result = await ProductFeatured.findOne({ 'product_id': req.params.id }).exec();
        if (result == null) {
            var productfeatured = new ProductFeatured();
            productfeatured.product_id = req.params.id;
            await productfeatured.save();
        }

        var productfeatured = {};
        productfeatured.description = req.body.description;
        productfeatured.left_1_title = req.body.left_1_title;
        productfeatured.left_1_icon = req.body.left_1_icon;
        productfeatured.left_1_text = req.body.left_1_text;
        productfeatured.left_2_title = req.body.left_2_title;
        productfeatured.left_2_icon = req.body.left_2_icon;
        productfeatured.left_2_text = req.body.left_2_text;
        productfeatured.left_3_title = req.body.left_3_title;
        productfeatured.left_3_icon = req.body.left_3_icon;
        productfeatured.left_3_text = req.body.left_3_text;
        productfeatured.right_1_title = req.body.right_1_title;
        productfeatured.right_1_icon = req.body.right_1_icon;
        productfeatured.right_1_text = req.body.right_1_text;
        productfeatured.right_2_title = req.body.right_2_title;
        productfeatured.right_2_icon = req.body.right_2_icon;
        productfeatured.right_2_text = req.body.right_2_text;
        productfeatured.right_3_title = req.body.right_3_title;
        productfeatured.right_3_icon = req.body.right_3_icon;
        productfeatured.right_3_text = req.body.right_3_text;
        ProductFeatured.update({ 'product_id': req.params.id }, productfeatured, function(err) {
            if (err) {
                req.flash('success', err.message);
                res.render('admin/products/featured_product', { title: 'Edit Featured Product Properties', reqBody: req.body });
            } else {
                req.flash('success', 'Featured Product Properties Updated Successfully');
                return res.redirect('/admin/products/list');
            }

        });
    } else {
        var query = ProductFeatured.findOne({ 'product_id': req.params.id });
        query.exec(function(err, result) {
            res.render('admin/products/featured_product', { title: 'Edit Featured Product Properties', reqBody: result });
        });

    }

};

exports.specification_list = async function(req, res) {

    var query = ProductSpecification.find().sort({ created_at: 1, _id: 1 });
    query.sort({ created_at: 1, _id: 1 });
    query.exec(function(err, result) {
        res.render('admin/products/specification_list', { title: 'Product Specification List', productSpecifications: result });
    });
};

exports.specification_delete = async function(req, res) {

    ProductSpecification.remove({ _id: req.params.id }).exec();
    req.flash('success', 'Product Specification Deleted Successfully');
    res.redirect('/admin/products/specification_list');
};

exports.specification_view = async function(req, res) {

    var query = ProductSpecification.findOne({ '_id': req.params.id });
    query.exec(function(err, result) {

        result1 = JSON.stringify(result);
        result2 = JSON.parse(result1);
        var keysarr = [];
        var valsarr = [];
        var i = 0;
        for (var key in result2) {
            if (result2.hasOwnProperty(key)) {
                keysarr[i] = key;
                valsarr[i] = result2[key];
                i++;
            }
        }
        res.render('admin/products/specification_view', { title: 'View Product Specification', keysarr: keysarr, valsarr: valsarr });
    });
};


exports.specification_upload = async function(req, res) {

    const productSpecification = await ProductSpecification.find().select('model');

    if (req.method == 'POST') {

        restLogo = typeof req.files['testfile'] !== "undefined" ? req.files['testfile'].name : '';
        req.checkBody('testfile', 'Please upload .xls and .xlsx file only.').isExcel(restLogo);
        var errors = req.validationErrors();
        if (errors) {
            res.render('admin/products/specification_upload', { title: 'Upload Product Specification', errors: errors, reqBody: req.body });
        } else {
            var time = new Date().getTime();
            var startup_image = req.files.testfile;
            var fileName = time + '_' + req.files.testfile.name;
            startup_image.mv('public/uploads/specification/' + fileName, function(err) {

                if (err) {
                    req.flash('success', 'Failed!, please try again.');
                    res.render('admin/products/specification_upload', { title: 'Upload Product Specification' });
                } else {
                    var absolutePath = path.resolve("public/uploads/specification/" + fileName);
                    readXlsxFile(absolutePath).then((rows, errors) => {
                        if (!errors) {
                            var a = rows
                            rows1 = Object.keys(a[0]).map(function(c) {
                                return a.map(function(r) { return r[c]; });
                            });
                            var flg = true;
                            var colval = '';
                            for (let val of rows1) {
                                if (flg == true) {
                                    colval = val;
                                    flg = false;
                                } else {
                                    var i = 0;
                                    var tempdoc = {};
                                    for (let dataval of val) {
                                        var key = colval[i];
                                        //key = key.replace(/[^a-zA-Z0-9 ]/g, "");
                                        key = key.replace(/\s/g, "_");
                                        tempdoc[key] = dataval;
                                        //tempdoc[colval[i]] = dataval; 
                                        i++;
                                    }
                                    //var exists = false;
                                    for (let ps of productSpecification) {
                                        if (ps.model == val[0]) {
                                            //exists = ps._id;
                                            ProductSpecification.remove({ _id: ps._id }).exec();
                                        }
                                    }
                                    //console.log(tempdoc.model);                                   
                                    ProductSpecification.create(tempdoc, { upsert: true }, function(err) {});

                                    /* if(exists == false)                                                                    
                                    {
                                    	ProductSpecification.create(tempdoc, {upsert: true}, function (err) {});
                                    }
                                    else
                                    {
                                    	ProductSpecification.findByIdAndUpdate(exists, tempdoc, function (err) {});
                                    } */

                                }
                            }
                            req.flash('success', 'File Imported successfully.');
                            res.redirect('/admin/products/specification_list');
                        } else {
                            req.flash('success', 'Failed!, please try again.');
                            res.render('admin/products/specification_upload', { title: 'Upload Product Specification' });
                        }
                    });
                }
            });
        }
    } else {
        res.render('admin/products/specification_upload', { title: 'Upload Product Specification' });
    }
};

exports.files = async function(req, res) {

    if (req.method == 'POST') {

        //console.log(req.body);

        var productfiles = {};
        productfiles.product_id = req.params.id;
        productfiles.user_manual = req.body.user_manual;
        productfiles.maintenance_procedures = req.body.maintenance_procedures;
        //productfiles.product_datasheet = req.body.product_datasheet;                              
        productfiles.product_datasheet_name = req.body.gallery_image_name;
        productfiles.product_datasheet = req.body.gallery_image;
        productfiles.accessory_manuals = req.body.accessory_manuals;
        productfiles.harness_instructions = req.body.harness_instructions;
        productfiles.assembly_installation = req.body.assembly_installation;
        productfiles.treadmill_manuals = req.body.treadmill_manuals;
        productfiles.treadmill_maintenance = req.body.treadmill_maintenance;
        productfiles.brochures = req.body.brochures;
        productfiles.gaitsens = req.body.gaitsens;
        ProductFiles.findOneAndUpdate({ 'product_id': req.params.id }, productfiles, { new: true, upsert: true }, function(err) {
            if (err) {
                req.flash('success', err.message);
                res.render('admin/products/files', { title: 'Product Supported Files', reqBody: result });
            } else {
                req.flash('success', 'Product Updated Successfully');
                return res.redirect('/admin/products/list');
            }
        });

    } else {
        var query = ProductFiles.findOne({ 'product_id': req.params.id });
        query.lean().exec(function(err, result) {
            res.render('admin/products/files', { title: 'Product Supported Files', reqBody: result });
        });
    }
};

exports.serialno_upload = async function(req, res) {
    if (req.method == 'POST') {

        restLogo = typeof req.files['testfile'] !== "undefined" ? req.files['testfile'].name : '';
        req.checkBody('testfile', 'Please upload .xls and .xlsx file only.').isExcel(restLogo);
        var errors = req.validationErrors();
        if (errors) {
            res.render('admin/products/serialno_upload', { title: 'Upload Product Serial No', errors: errors, reqBody: req.body });
        } else {
            var time = new Date().getTime();
            var startup_image = req.files.testfile;
            var fileName = time + '_' + req.files.testfile.name;
            startup_image.mv('public/uploads/specification/' + fileName, function(err) {

                if (err) {
                    req.flash('success', 'Failed!, please try again.');
                    res.render('admin/products/serialno_upload', { title: 'Upload Product Serial No' });
                } else {
                    var absolutePath = path.resolve("public/uploads/specification/" + fileName);
                    readXlsxFile(absolutePath).then(async(rows, errors) => {
                        if (!errors) {
                            await ProductSerialno.deleteMany({}).exec();

                            for (let val of rows) {
                                if (val[0] != '') {
                                    var tempdoc = {};
                                    tempdoc.name = val[0];
                                    tempdoc.serialno = val[1];
                                    ProductSerialno.create(tempdoc, { upsert: true }, function(err) {});
                                }
                            }
                            req.flash('success', 'Serial No uploaded successfully.');
                            res.render('admin/products/serialno_upload', { title: 'Upload Product Serial No' });
                        } else {
                            req.flash('success', 'Failed!, please try again.');
                            res.render('admin/products/serialno_upload', { title: 'Upload Product Serial No' });
                        }
                    });
                }
            });
        }
    } else {
        res.render('admin/products/serialno_upload', { title: 'Upload Product Serial No' });
    }
};

exports.specification_group_list = async function(req, res) {

    var query = ProductSpecificationGroup.find();
    query.sort({ created_at: 1 });
    query.exec(function(err, result) {
        res.render('admin/products/specification_group_list', { title: 'Product Specification Group List', productSpecificationGroup: result });
    });
};

exports.specification_group_add = async function(req, res) {
    const productSpecification = await ProductSpecification.find().sort({ created_at: 1, _id: 1 });
    const specificationRow = {};
    productSpecification.forEach(async function(specification) {
        var keys = Object.keys(specification.toObject());
        keys.forEach(async function(key) {
            specificationRow[key] = key;
        });
    });

    if (req.method == 'POST') {

        req.checkBody('name', 'Group Name is required').notEmpty();
        req.checkBody('specification_row', 'Specification Row is required').notEmpty();
        var errors = req.validationErrors();
        if (errors) {
            res.render('admin/products/specification_group_add', { title: 'Add Product Specification Group', errors: errors, reqBody: req.body, specificationRow: specificationRow });
        } else {
            var productSpecificationGroup = new ProductSpecificationGroup();
            productSpecificationGroup.name = req.body.name;
            productSpecificationGroup.specification_row = req.body.specification_row ? req.body.specification_row.toString() : '';
            productSpecificationGroup.save(async function(err, result) {

                if (err) {
                    req.flash('success', err.message);
                    res.render('admin/products/specification_group_add', { title: 'Add Product Specification Group', reqBody: req.body, specificationRow: specificationRow });
                } else {
                    req.flash('success', 'Product Specification Group Added Successfully');
                    return res.redirect('/admin/products/specification_group_list');
                }

            });
        }
    } else {
        res.render('admin/products/specification_group_add', { title: 'Add Product Specification Group', specificationRow: specificationRow });
    }
};

exports.specification_group_edit = async function(req, res) {

    const productSpecification = await ProductSpecification.find().sort({ created_at: 1, _id: 1 });
    const specificationRow = {};
    productSpecification.forEach(async function(specification) {
        var keys = Object.keys(specification.toObject());
        keys.forEach(async function(key) {
            specificationRow[key] = key;
        });
    });

    if (req.method == 'POST') {
        req.checkBody('name', 'Group Name is required').notEmpty();
        req.checkBody('specification_row', 'Specification Row is required').notEmpty();

        var errors = req.validationErrors();
        if (errors) {

            res.render('admin/blogs/edit', { title: 'Edit Blog', errors: errors, reqBody: req.body, specificationRow: specificationRow });
        } else {
            var productSpecificationGroup = {};
            productSpecificationGroup.name = req.body.name;
            productSpecificationGroup.specification_row = req.body.specification_row ? req.body.specification_row.toString() : '';
            ProductSpecificationGroup.findByIdAndUpdate(req.params.id, productSpecificationGroup, function(err) {
                if (err) {
                    req.flash('success', err.message);
                    res.render('admin/blogs/edit', { title: 'Edit Product Specification Group', reqBody: req.body, specificationRow: specificationRow });
                } else {
                    req.flash('success', 'Product Specification Group Added Successfully');
                    return res.redirect('/admin/products/specification_group_list');
                }

            });
        }
    } else {
        var query = ProductSpecificationGroup.findOne({ '_id': req.params.id });
        query.exec(function(err, result) {
            res.render('admin/products/specification_group_edit', { title: 'Edit Product Specification Group', reqBody: result, specificationRow: specificationRow });
        });

    }
};

exports.specification_group_delete = async function(req, res) {

    ProductSpecificationGroup.remove({ _id: req.params.id }).exec();
    req.flash('success', 'Product Specification Group Deleted Successfully');
    res.redirect('/admin/products/specification_group_list');
};