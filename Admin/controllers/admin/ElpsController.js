var Elp = include("models/Elp.js");
var ElpPayment = include("models/ElpPayment.js");
var Coupon = include("models/Coupon.js");
var User = include("models/User.js");

var mongoose = require('mongoose'); 


exports.list = function(req, res) {
   	var query = Elp.find();    
    query.sort({ display_number : 1 });
    query.exec(function (err, result) {
  		res.render('admin/elps/list',{ title: 'Elp List',elps:result});       	
	});    
};

exports.add = function(req, res) {	
    if(req.method == 'POST')
    {
    	req.checkBody('name', 'Name is required').notEmpty();  
    	req.checkBody('display_name', 'Display Name is required').notEmpty();  
    	req.checkBody('display_sub_name', 'Display Sub Name is required').notEmpty();
    	req.checkBody('price', 'Price is required').notEmpty();  
    	req.checkBody('price', 'Price is required numeric value').isNumeric();
		req.checkBody('display_number', 'Display Number is required').notEmpty();  
    	req.checkBody('display_number', 'Display Number is required numeric value').isNumeric(); 
		req.checkBody('duration', 'Duration is required').notEmpty();  
    	req.checkBody('duration', 'Duration is required numeric value').isNumeric();  
    	req.checkBody('duration', 'Duration is required numeric decimal value').isDecimal(); 
    	req.checkBody('persons', 'No. Of Person is required').notEmpty(); 
 		
		var errors = req.validationErrors();
		if(errors){						
			res.render('admin/elps/add', {title: 'Add Elp', errors: errors,reqBody:req.body});			  		
		}
		else
		{
			var elp = new Elp();		
			elp.name =req.body.name;
			elp.display_name =req.body.display_name; 
			elp.display_sub_name =req.body.display_sub_name;
			elp.display_number =req.body.display_number;
			elp.price =req.body.price;
			elp.duration =req.body.duration;
			elp.persons =req.body.persons;
			elp.status = 1;
			
			elp.save(function (err) {				
				if(err)
				{
					req.flash('success', err.message);
					res.render('admin/elps/add', {title: 'Add Elps', reqBody:req.body});			  		
				}
				else
				{
					req.flash('success', 'Elp Added Successfully');			
    				return res.redirect('/admin/elp/list');	
				}	  			
			});  		
		}
    }
    else
    {
    	res.render('admin/elps/add',{ title: 'Add Elp'});
    }
     
};

exports.edit = function(req, res) {	
    if(req.method == 'POST')
    {
    	req.checkBody('name', 'Name is required').notEmpty();
    	req.checkBody('display_name', 'Display Name is required').notEmpty();  
    	req.checkBody('display_sub_name', 'Display Sub Name is required').notEmpty();
		req.checkBody('price', 'Price is required').notEmpty();  
    	req.checkBody('price', 'Price is required numeric value').isNumeric(); 
    	req.checkBody('display_number', 'Display Number is required').notEmpty();  
    	req.checkBody('display_number', 'Display Number is required numeric value').isNumeric(); 
    	req.checkBody('duration', 'Duration is required').notEmpty();  
    	req.checkBody('duration', 'Duration is required numeric value').isNumeric();  
    	req.checkBody('duration', 'Duration is required numeric decimal value').isDecimal();
    	req.checkBody('persons', 'No. Of Person is required').notEmpty();  
 		  		
		var errors = req.validationErrors();
		if(errors){						
			res.render('admin/elps/edit', {title: 'Edit Elp', errors: errors,reqBody:req.body});			  		
		}
		else
		{			
			var elp = {};					
			elp.name =req.body.name;
			elp.display_name =req.body.display_name;  
			elp.display_sub_name =req.body.display_sub_name; 
			elp.price =req.body.price;
			elp.display_number =req.body.display_number;
			elp.duration =req.body.duration;
			elp.persons =req.body.persons;
			
			Elp.findByIdAndUpdate( req.params.id, elp , function (err) {				
				if(err)
				{
					req.flash('success', err.message);
					res.render('admin/elps/edit', {title: 'Edit Elp', reqBody:req.body});	  		
				}
				else
				{
					req.flash('success', 'Elp Updated Successfully');			
    				return res.redirect('/admin/elp/list');	
				}					  			
			});  		
		}
    }
    else
    {
    	var query = Elp.findOne({'_id' : req.params.id});        	
    	query.exec(function (err, result) {
  			res.render('admin/elps/edit',{ title: 'Edit Elp',reqBody:result});       	
		});
    	
    }
     
};

exports.delete = function(req, res) {	
	Elp.remove({_id:req.params.id}).exec();
	req.flash('success', 'Elp Deleted Successfully');
	res.redirect('/admin/elp/list');
    
};


exports.status = function(req, res) {	
	var elp = {};					
	elp.status = req.params.status;			
	Elp.findByIdAndUpdate( req.params.id, elp , function (err) {
		req.flash('success', 'Elp Status Changed Successfully');
		res.redirect('/admin/elp/list');
	});	
    
};

exports.payments = function(req, res) {
   	var query = ElpPayment.find().populate('elp_id').populate('user_id');    
    query.sort({ created_at : -1 });
    query.exec(function (err, result) {
    	// console.log(result);  
    	// process.exit(); 
  		res.render('admin/elps/payments',{ title: 'Elp Payments List',payments:result});       	
	});    
};


exports.view_coupon_user = async function(req, res) {
   	var query = Coupon.findOne({'code' : req.params.id});        	
    query.exec( await function (err, result) {
		User.find({
		    '_id': { $in: result.users.split(",")}
		}, function(err, docs){
		     if(!err){
		     	res.render('admin/elps/view_coupon_user',{ title: 'Used Coupon',users:docs});     
		     }else{
		     	res.render('admin/elps/view_coupon_user',{ title: 'Used Coupon',users:''});     
		     }
		});   	
	});    
};

