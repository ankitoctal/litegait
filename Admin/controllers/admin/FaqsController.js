var Faq = include("models/Faq.js");
var FaqContent = include("models/FaqContent.js");
const Category = include("models/Category.js");

exports.list = async function(req, res) {
   	//var query = Faq.find().populate('category');
   	const categories = await Category.where('type','faqs').find();    
   	var query = Faq.find();    
    query.sort({ created_at: -1 });
    query.exec(function (err, result) {
  		res.render('admin/faqs/list',{ title: 'Faqs List',faqs:result,categories:categories});       	
	}); 
	 	    
};

exports.add = async function(req, res) {
	
	const categories = await Category.where('type','faqs').find();

    if(req.method == 'POST')
    {  
    	req.body.question = {};
		req.body.answer = {};
    	res.locals.activeLanguage.forEach(function(language) {  		
  			req.body.question[language.code] =req.body['question['+language.code+']'] ;
			req.body.answer[language.code] =req.body['answer['+language.code+']'] ;
		});
		
		req.checkBody('category', 'Category is required').notEmpty(); 
		req.checkBody('question.en', 'Question is required').notEmpty(); 
   		req.checkBody('answer.en', 'Answer is required').notEmpty();    	    	
		var errors = req.validationErrors();
		if(errors){		
			res.render('admin/faqs/add', {title: 'Add Faq',categories:categories, errors: errors,reqBody:req.body});				  		
		}
		else
		{
			var faq = new Faq();	
			// faq.category =req.body.category;	
			faq.category =req.body.category ? req.body.category.toString() : '';;	
			faq.question =req.body.question.en;	
			faq.answer =req.body.answer.en;	
			faq.status = 1;			
			faq.save(async function (err, newfaq) {
							
				if(err)
				{
					req.flash('success', err.message);
					res.render('admin/faqs/add', {title: 'Add Faq',categories:categories, reqBody:req.body});			  		
				}
				else
				{					
					for (var i = 0; i < res.locals.activeLanguage.length; i++) {
						var langCode = res.locals.activeLanguage[i].code;
						var faqcontent = new FaqContent();
						faqcontent.faq_id =newfaq._id;
						faqcontent.lang=langCode;						
						faqcontent.question=req.body.question[langCode];
						faqcontent.answer=req.body.answer[langCode];
						await faqcontent.save();
						newfaq.lang_content.push(faqcontent);
						await newfaq.save();
					}		
					req.flash('success', 'Faq Added Successfully');			
    				return res.redirect('/admin/faqs/list');		
				}			
					  			
			});  		
		}
    }
    else
    {
		res.render('admin/faqs/add',{ title: 'Add Faq',categories:categories});
    }
     
};

exports.edit = async function(req, res) {

	const categories = await Category.where('type','faqs').find();

    if(req.method == 'POST')
    {
    	req.body.question = {};
    	req.body.answer = {};
    	res.locals.activeLanguage.forEach(function(language) {  			
  			req.body.question[language.code] =req.body['question['+language.code+']'] ;
  			req.body.answer[language.code] =req.body['answer['+language.code+']'] ;
		});
		req.checkBody('category', 'Category is required').notEmpty(); 
   		req.checkBody('question.en', 'Question is required').notEmpty();
   		req.checkBody('answer.en', 'Answer is required').notEmpty();

		var errors = req.validationErrors();
		if(errors){			
			
			res.render('admin/faqs/edit', {title: 'Edit Faq',categories:categories, errors: errors,reqBody:req.body});			  		
		}
		else
		{	 console.log(req.body.category)		
			var faq = {};					
			faq.category = req.body.category ? req.body.category.toString() : '';
			faq.question =req.body.question.en;	
			faq.answer =req.body.answer.en;			
			Faq.findByIdAndUpdate( req.params.id, faq , function (err) {			
				if(err)
				{
					req.flash('success', err.message);
					res.render('admin/faqs/edit', {title: 'Edit Faq',categories:categories, reqBody:req.body});			  		
				}
				else 
				{
					var query = Faq.findOne({'_id' : req.params.id}).populate('lang_content');      	
			    	query.exec(async function (err, result) {    						    		
			    		for (var i = 0; i < result.lang_content.length; i++) {
			    			var k = result.lang_content[i];
			    			var langCode = k.lang;
			    			var faqcontent = {};				    			
			    			faqcontent.question=req.body.question[langCode];
			    			faqcontent.answer=req.body.answer[langCode];
			    			FaqContent.findByIdAndUpdate( k._id, faqcontent , function (err) {
			    			});	
						}			  			
					});

					req.flash('success', 'Faq Updated Successfully');			
    				return res.redirect('/admin/faqs/list');	
				}			
					  			
			});  		
		}
    }
    else
    {
    	var query = Faq.findOne({'_id' : req.params.id}).populate('lang_content');
    	query.lean().exec(function (err, result) { 		
			
    		result.question = {};
    		result.answer = {};

	    	result.lang_content.forEach(function(contents) {  
	    		result.question[contents.lang] =contents.question ;
	    		result.answer[contents.lang] =contents.answer ;	  			
			});   		  		
  			res.render('admin/faqs/edit',{ title: 'Edit Faq',categories:categories, reqBody:result});       	
		});
    	
    }
     
};

exports.delete = function(req, res) {
	var query = Faq.findOne({'_id' : req.params.id}).populate('lang_content');        	
	query.exec(function (err, result) {
		for (var i = 0; i < result.lang_content.length; i++) {
			var k = result.lang_content[i];			
			FaqContent.remove({_id:k._id}).exec();			
		}
	});
	Faq.remove({_id:req.params.id}).exec();
	req.flash('success', 'Faq Deleted Successfully');
	res.redirect('/admin/faqs/list');    
};

exports.view = function(req, res) {	
    var query = Category.findOne({'_id' : req.params.id});        	
	query.exec(function (err, result) {
			res.render('admin/faqs/view',{ title: 'View Category',reqBody:result});       	
	});  
     
};

exports.status = function(req, res) {	
	var faq = {};					
	faq.status = req.params.status;			
	Faq.findByIdAndUpdate( req.params.id, faq , function (err) {
		req.flash('success', 'Faq Status Changed Successfully');
		res.redirect('/admin/faqs/list');
	});	
    
};
