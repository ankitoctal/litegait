var Category = include("models/Category.js");
var CategoryContent = include("models/CategoryContent.js");

exports.list = function(req, res) {
   	var query = Category.find();    
    query.sort({ type: 1 });
    query.exec(function (err, result) {
    	
  		res.render('admin/category/list',{ title: 'Category List',categories:result});       	
	}); 
	 	    
};

exports.add = async function(req, res) {
	
	var categories = await Category.where('parent_id','0').find().exec();	
	var subcat = (req.query.subcat)?req.query.subcat:0;	

    if(req.method == 'POST')
    {
    	req.checkBody('type', 'Type is required').notEmpty();
    	req.checkBody('name', 'Name is required').notEmpty(); 
    	req.checkBody('slug', 'Slug is required').notEmpty();
    	//req.checkBody('image', 'Image is required').notEmpty();
    	//req.checkBody('feature.en', 'Feature is required').notEmpty();
    	req.checkBody('short_description.en', 'Short Description is required').notEmpty();  		
		var errors = req.validationErrors();
		if(errors){						
			res.render('admin/category/add', {title: 'Add Category', errors: errors,categories:categories,subcat:subcat,reqBody:req.body});			  		
		}
		else
		{
			var category = new Category();		
			category.type =req.body.type;
			category.name =req.body.name;
			category.slug =req.body.slug;
			category.parent_id = 0;	
			if(typeof req.body.parent_id !== "undefined" && req.body.parent_id !="")
			{
				category.parent_id =req.body.parent_id;	
			}	
			category.image =req.body.image;	
			category.banner_image =req.body.banner_image;	
			category.feature =req.body.feature.en;
			category.short_description =req.body.short_description.en;	
			category.position =req.body.position;	
			category.status = 1;
			category.save(async function (err,newcategory) {				
				if(err)
				{
					req.flash('success', err.message);
					res.render('admin/category/add', {title: 'Add Category', reqBody:req.body});			  		
				}
				else
				{
					for (var i = 0; i < res.locals.activeLanguage.length; i++) {
						var langCode = res.locals.activeLanguage[i].code;
						var categorycontent = new CategoryContent();
						categorycontent.product_id =newcategory._id;
						categorycontent.lang=langCode;						
						categorycontent.feature=req.body.feature[langCode];
						categorycontent.short_description=req.body.short_description[langCode];
						//categorycontent.description=req.body.description[langCode];
						await categorycontent.save();

						newcategory.lang_content.push(categorycontent);
						await newcategory.save();
					}

					req.flash('success', 'Category Added Successfully');			
    				return res.redirect('/admin/categories/list');	
				}			
					  			
			});  		
		}
    }
    else
    {
    	res.render('admin/category/add',{ title: 'Add Category',categories:categories,subcat:subcat});
    }
     
};

exports.edit = async function(req, res) {	
	var categories = await Category.where('parent_id','0').find().exec();
    
    if(req.method == 'POST')
    {
    	req.checkBody('type', 'Type is required').notEmpty();
    	req.checkBody('name', 'Name is required').notEmpty();
    	req.checkBody('slug', 'Slug is required').notEmpty();
    	//req.checkBody('image', 'Image is required').notEmpty();
    	//req.checkBody('feature.en', 'Feature is required').notEmpty();
    	req.checkBody('short_description.en', 'Short Description is required').notEmpty();
		var errors = req.validationErrors();
		if(errors){						
			res.render('admin/category/edit', {title: 'Edit Category', errors: errors,reqBody:req.body,categories:categories});			  		
		}
		else
		{			
			var category = {};
			category.type = req.body.type;	
			category.parent_id = 0;	
			if(typeof req.body.parent_id !== "undefined" && req.body.parent_id !="")
			{
				category.parent_id =req.body.parent_id;	
			}				
			category.name =req.body.name;				
			category.slug =req.body.slug;
			category.image =req.body.image;	
			category.banner_image =req.body.banner_image;	
			category.feature =req.body.feature.en;
			category.short_description =req.body.short_description.en;
			category.position =req.body.position;
			Category.findByIdAndUpdate( req.params.id, category , function (err) {				
				if(err)
				{
					req.flash('success', err.message);
					res.render('admin/category/edit', {title: 'Edit Category', reqBody:req.body,categories:categories});			  		
				}
				else
				{
					var query = Category.findOne({'_id' : req.params.id}).populate('lang_content');      	
			    	query.exec(async function (err, result) {    						    		
			    		for (var i = 0; i < result.lang_content.length; i++) {
			    			var k = result.lang_content[i];
			    			var langCode = k.lang;
			    			var categorycontent = {};				    			
			    			categorycontent.feature=req.body.feature[langCode];
							categorycontent.short_description=req.body.short_description[langCode];
			    			CategoryContent.findByIdAndUpdate( k._id, categorycontent , function (err) {
			    			});	
						}
					});
						
					req.flash('success', 'Category Updated Successfully');			
    				return res.redirect('/admin/categories/list');	
				}			
					  			
			});  		
		}
    }
    else
    {
    	var query = Category.findOne({'_id' : req.params.id}).populate('lang_content');        	
    	query.lean().exec(function (err, result) {
    		result.feature = {};
    		result.short_description = {};
	    	result.lang_content.forEach(function(content) {  
	    		result.feature[content.lang] =content.feature ;	  			
	    		result.short_description[content.lang] =content.short_description ;	    		
			});			
  			res.render('admin/category/edit',{ title: 'Edit Category',reqBody:result,categories:categories});       	
		});
    	
    }
     
};

exports.delete = function(req, res) {	
	Category.remove({_id:req.params.id}).exec();
	req.flash('success', 'Category Deleted Successfully');
	res.redirect('/admin/categories/list');
    
};

exports.view = function(req, res) {	
    var query = Category.findOne({'_id' : req.params.id});        	
	query.exec(function (err, result) {
			res.render('admin/category/view',{ title: 'View Category',reqBody:result});       	
	});  
     
};

exports.status = function(req, res) {	
	var category = {};					
	category.status = req.params.status;			
	Category.findByIdAndUpdate( req.params.id, category , function (err) {
		req.flash('success', 'Category Status Changed Successfully');
		res.redirect('/admin/categories/list');
	});	
    
};
