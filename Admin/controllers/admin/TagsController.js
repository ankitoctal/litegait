var Tag = include("models/Tag.js");
const excel = require('node-excel-export');

exports.list = function(req, res) {
   	var query = Tag.find();    
    query.sort({ created_at: -1 });
    query.exec(function (err, result) {
  		res.render('admin/tags/list',{ title: 'Tags List',tags:result});       	
	}); 
	 	    
};

exports.add = function(req, res) {	
    if(req.method == 'POST')
    {
    	req.checkBody('name', 'Name is required').notEmpty();   		
		var errors = req.validationErrors();
		if(errors){						
			res.render('admin/tags/add', {title: 'Add Tag', errors: errors,reqBody:req.body});			  		
		}
		else
		{
			var tag = new Tag();		
			tag.name =req.body.name;
			tag.status = 1;
			tag.save(function (err) {				
				if(err)
				{
					req.flash('success', err.message);
					res.render('admin/tags/add', {title: 'Add Tag', reqBody:req.body});			  		
				}
				else
				{
					req.flash('success', 'Tag Added Successfully');			
    				return res.redirect('/admin/tags/list');	
				}			
					  			
			});  		
		}
    }
    else
    {
    	res.render('admin/tags/add',{ title: 'Add Tag'});
    }
     
};

exports.edit = function(req, res) {	
    if(req.method == 'POST')
    {
    	req.checkBody('name', 'Name is required').notEmpty();   		
		var errors = req.validationErrors();
		if(errors){						
			res.render('admin/tags/edit', {title: 'Edit Tag', errors: errors,reqBody:req.body});			  		
		}
		else
		{			
			var tag = {};					
			tag.name =req.body.name;				
			Tag.findByIdAndUpdate( req.params.id, tag , function (err) {				
				if(err)
				{
					req.flash('success', err.message);
					res.render('admin/tags/edit', {title: 'Edit Tag', reqBody:req.body});			  		
				}
				else
				{
					req.flash('success', 'Tag Updated Successfully');			
    				return res.redirect('/admin/tags/list');	
				}			
					  			
			});  		
		}
    }
    else
    {
    	var query = Tag.findOne({'_id' : req.params.id});        	
    	query.exec(function (err, result) {
  			res.render('admin/tags/edit',{ title: 'Edit Tag',reqBody:result});       	
		});
    	
    }
     
};

exports.delete = function(req, res) {	
	Tag.remove({_id:req.params.id}).exec();
	req.flash('success', 'Tag Deleted Successfully');
	res.redirect('/admin/tags/list');
    
};

exports.view = function(req, res) {	
    var query = Category.findOne({'_id' : req.params.id});        	
	query.exec(function (err, result) {
			res.render('admin/tags/view',{ title: 'View Category',reqBody:result});       	
	});  
     
};

exports.status = function(req, res) {	
	var tag = {};					
	tag.status = req.params.status;			
	Tag.findByIdAndUpdate( req.params.id, tag , function (err) {
		req.flash('success', 'Tag Status Changed Successfully');
		res.redirect('/admin/tags/list');
	});	
    
};




exports.export_in_excel = function(req, res) {	


   	var query = Tag.find();    
    query.sort({ created_at: -1 });

    query.exec(function (err, result) {
		const specification = {

		  name: {
		    displayName: 'NAME',
		    headerStyle: {},
		    width: 220
		  },
		  status: {
		    displayName: 'STATUS',
		    headerStyle:{},
		    width: 220
		  },
		  created: {
		    displayName: 'CREATED',
		    headerStyle:{},
		    width: 220
		  },
		}
		 
	
        var temp_arr = []; 
		for (var i = 0;i<result.length;i++){
			var tempobj = {}; 
			tempobj.name =  result[i].name;
			tempobj.status =  result[i].status == 1 ? 'Active' : 'Inactive';
			tempobj.created =  result[i].created_at ? result[i].created_at.toDateString() : '';
			temp_arr[i] = tempobj; 
		}			
		const dataset = temp_arr
		const report = excel.buildExport(
		  [ 
		    {
		      name: 'Tags Report', 
		      specification: specification, 
		      data: dataset 
		    }
		  ]
		);
		 
		
		res.attachment('tags_report.xlsx'); 
		return res.send(report);
		
	}); 




};

