const mongoose = require('mongoose');
const Part = include("models/Part.js");
const PartContent = include("models/PartContent.js"); 
const PartGallery = include("models/PartGallery.js"); 
const Product = include("models/Product.js");
const Category = include("models/Category.js");
const Testimonial = include("models/Testimonial.js");

exports.list = async function(req, res) {
	
   	var query = Part.find().populate('category').populate({path: 'product_id',model: 'Product',select:'name'});    
    query.sort({ created_at: -1 });
    query.exec(function (err, result) {    	
  		res.render('admin/parts/list',{ title: 'Part/Accessories List',parts:result});       	
	}); 
	 	    
};

exports.add = async function(req, res) {
	
	const products = await Product.find().select('_id name').exec();
	const categories = await Category.where({$or:[{'type':'parts'},{'type':'accessories'}]}).find();	
	const allparts = await Part.find().select('_id name').exec();	
	var testimonial = new Testimonial();
	var testimonials = await testimonial.getAll();

    if(req.method == 'POST')
    {    	
    	req.body.description = {};
    	req.body.short_description = {};
    	res.locals.activeLanguage.forEach(function(language) {  			  			
  			req.body.description[language.code] =req.body['description['+language.code+']'] ;
  			req.body.short_description[language.code] =req.body['short_description['+language.code+']'] ;
		});

    	req.checkBody('type', 'Type is required').notEmpty();
    	//req.checkBody('product_id', 'Product is required').notEmpty();
    	req.checkBody('category', 'Category is required').notEmpty();
    	req.checkBody('slug', 'Slug is required').notEmpty();
   		req.checkBody('name', 'Name is required').notEmpty();   		
   		req.checkBody('short_description.en', 'Short Description is required').notEmpty();   		
   		req.checkBody('description.en', 'Description is required').notEmpty();   		
   		req.checkBody('image', 'Image is required').notEmpty();  		
		var errors = req.validationErrors();
		if(errors){						
			res.render('admin/parts/add', {title: 'Add Part/Accessories', errors: errors,products:products,categories:categories,allparts:allparts,reqBody:req.body,testimonials:testimonials});			  		
		}
		else
		{
			var part = new Part();		
			part.type =req.body.type;
			part.product_id =req.body.product_id;
			part.category =req.body.category;
			part.name =req.body.name;
			part.slug =req.body.slug;
			part.sno =req.body.sno;
			part.image =req.body.image;	
			part.banner_image = req.body.banner_image;		
			part.short_description =req.body.short_description.en;
			part.description =req.body.description.en;
			part.video = req.body.video;					
			part.price =req.body.price;	
			part.price_request =req.body.price_request;
			if(typeof part.price_request == "undefined")
    		{	
    			part.price_request = 0;
    		}
    		part.schedule_service =req.body.schedule_service;
			if(typeof part.schedule_service == "undefined")
    		{	
    			part.schedule_service = 0;
    		}	
			part.position =req.body.position;	
			part.testimonials =req.body.testimonials;	
			part.meta_title =req.body.meta_title;	
			part.meta_description =req.body.meta_description;
			part.status = 1;
			part.related_product = req.body.related_product;		    		
    		if(typeof part.related_product == "undefined")
    		{	
    			part.related_product = "";
    		}				
			part.save(async function (err,newpart) {				
				if(err)
				{
					req.flash('success', err.message);
					res.render('admin/parts/add', {title: 'Add Part/Accessories', products:products, categories:categories, reqBody:req.body});			  		
				}
				else
				{
					for (var i = 0; i < res.locals.activeLanguage.length; i++) {
						var langCode = res.locals.activeLanguage[i].code;
						var partcontent = new PartContent();
						partcontent.part_id =newpart._id;
						partcontent.lang=langCode;												
						partcontent.short_description=req.body.short_description[langCode];
						partcontent.description=req.body.description[langCode];
						await partcontent.save();

						newpart.lang_content.push(partcontent);
						await newpart.save();
					}

					/*Gallery Images section */
                    if (req.body.gallery_image !== "") {
                        if (Array.isArray(req.body.gallery_image)) {
                            req.body.gallery_image.forEach(async function(image,index) {
                                var partgallery = new PartGallery();
                                partgallery.part_id = newpart._id;
                                partgallery.image = image;	                                
                                await partgallery.save();

                                newpart.image_gallery.push(partgallery);	                                	                                
                                if(index == req.body.gallery_image.length-1)
                                {	                               	
                                	await newpart.save();
                                }		                                
                            });
                        } else {
                            var image = req.body.gallery_image;
                            var partgallery = new PartGallery();
                            partgallery.product_id = newpart._id;
                            partgallery.image = image
                            await partgallery.save();

                            newpart.image_gallery.push(partgallery);
                            await newpart.save();
                        }
                    }
                    /*Gallery Images section */

					req.flash('success', 'Part/Accessories Added Successfully');			
    				return res.redirect('/admin/parts/list');	
				}			
					  			
			});  		
		}
    }
    else
    {
    	res.render('admin/parts/add',{ title: 'Add Part/Accessories',products:products, categories:categories,allparts:allparts,testimonials:testimonials});
    }
     
};

exports.edit = async function(req, res) {	
	
	const products = await Product.find().select('_id name').exec();		
	const categories = await Category.where({$or:[{'type':'parts'},{'type':'accessories'}]}).find();	
	const allparts = await Part.find().select('_id name').exec();
	var testimonial = new Testimonial();
	var testimonials = await testimonial.getAll();	

    if(req.method == 'POST')
    { 
    	req.body.description = {};
    	req.body.short_description = {};
    	res.locals.activeLanguage.forEach(function(language) {  			  			
  			req.body.short_description[language.code] =req.body['short_description['+language.code+']'] ;
  			req.body.description[language.code] =req.body['description['+language.code+']'] ;
		});
    	
    	req.checkBody('type', 'Type is required').notEmpty();
    	//req.checkBody('product_id', 'Product is required').notEmpty();
    	req.checkBody('category', 'Category is required').notEmpty();
    	req.checkBody('slug', 'Slug is required').notEmpty();
   		req.checkBody('name', 'Name is required').notEmpty();
   		//req.checkBody('sno', 'Serial Number is required').notEmpty();   		
   		req.checkBody('short_description.en', 'Short Description is required').notEmpty();   		  		
   		req.checkBody('description.en', 'Description is required').notEmpty();   		  		
		var errors = req.validationErrors();
		if(errors){						
			res.render('admin/parts/edit', {title: 'Edit Part/Accessories', errors: errors,products:products,categories:categories,allparts:allparts,reqBody:req.body,testimonials:testimonials});			  		
		}
		else
		{			
			var part = {};								
			part.type =req.body.type;
			part.product_id =req.body.product_id;
			part.category =req.body.category;
			part.name =req.body.name;
			part.slug =req.body.slug;
			part.sno =req.body.sno;
			part.image =req.body.image;
			part.banner_image = req.body.banner_image;			
			part.short_description =req.body.short_description.en;					
			part.description =req.body.description.en;
			part.video = req.body.video;					
			part.price =req.body.price;	
			part.price_request =req.body.price_request;
			if(typeof part.price_request == "undefined")
    		{	
    			part.price_request = 0;
    		}
    		part.schedule_service =req.body.schedule_service;
			if(typeof part.schedule_service == "undefined")
    		{	
    			part.schedule_service = 0;
    		}
    		part.position =req.body.position;	
			part.testimonials =req.body.testimonials;	
			part.meta_title =req.body.meta_title;	
			part.meta_description =req.body.meta_description;
			part.status = 1;
			part.related_product = req.body.related_product;		    		
    		if(typeof part.related_product == "undefined")
    		{	
    			part.related_product = "";
    		}																    		
			Part.findByIdAndUpdate( req.params.id, part , function (err) {				
				if(err)
				{
					req.flash('success', err.message);
					res.render('admin/parts/edit', {title: 'Edit Part/Accessories', reqBody:req.body});			  		
				}
				else
				{				

					var query = Part.findOne({'_id' : req.params.id}).populate('lang_content');      	
			    	query.exec(async function (err, result) {    						    		
			    		for (var i = 0; i < result.lang_content.length; i++) {
			    			var k = result.lang_content[i];
			    			var langCode = k.lang;
			    			var partcontent = {};				    						    			
			    			partcontent.short_description=req.body.short_description[langCode];
			    			partcontent.description=req.body.description[langCode];
			    			PartContent.findByIdAndUpdate( k._id, partcontent , function (err) {
			    			});	
						}

						/*Gallery Images section */
	                    if (req.body.gallery_image !== "") {
	                        if (Array.isArray(req.body.gallery_image)) {
	                            req.body.gallery_image.forEach(async function(image,index) {
	                                var partgallery = new PartGallery();
	                                partgallery.part_id = result._id;
	                                partgallery.image = image;	                                
	                                await partgallery.save();

	                                result.image_gallery.push(partgallery);	                                	                                
	                                if(index == req.body.gallery_image.length-1)
	                                {	                               	
	                                	await result.save();
	                                }		                                
	                            });
	                        } else {
	                            var image = req.body.gallery_image;
	                            var partgallery = new PartGallery();
	                            partgallery.product_id = result._id;
	                            partgallery.image = image
	                            await partgallery.save();

	                            result.image_gallery.push(partgallery);
	                            await result.save();
	                        }
	                    }
	                    /*Gallery Images section */
					});	

					req.flash('success', 'Part/Accessories Updated Successfully');			
    				return res.redirect('/admin/parts/list');	
				}			
					  			
			});  		
		}
    }
    else
    {
    	var query = Part.findOne({'_id' : req.params.id}).populate('lang_content').populate('image_gallery');;        	
    	query.lean().exec(function (err, result) {

    		result.description = {};
    		result.short_description = {};

	    	result.lang_content.forEach(function(content) {  	    		
	    		result.short_description[content.lang] =content.short_description ;	  			
	    		result.description[content.lang] =content.description ;	  			
			});
			
  			res.render('admin/parts/edit',{ title: 'Edit Part/Accessories',categories:categories,products:products,allparts:allparts,reqBody:result,testimonials:testimonials});       	
		});
    	
    }
     
};

exports.delete = function(req, res) {	
	var query = Part.findOne({'_id' : req.params.id}).populate('lang_content');        	
	query.exec(function (err, result) {
		for (var i = 0; i < result.lang_content.length; i++) {
			var k = result.lang_content[i];			
			PartContent.remove({_id:k._id}).exec();			
		}
	});
	Part.remove({_id:req.params.id}).exec();
	req.flash('success', 'Part/Accessories Deleted Successfully');
	res.redirect('/admin/parts/list');    
};

exports.view = function(req, res) {	
    var query = Category.findOne({'_id' : req.params.id});        	
	query.exec(function (err, result) {
			res.render('admin/parts/view',{ title: 'View Part/Accessories',reqBody:result});       	
	});  
     
};

exports.status = function(req, res) {	
	var part = {};					
	part.status = req.params.status;			
	Part.findByIdAndUpdate( req.params.id, part , function (err) {
		req.flash('success', 'Part/Accessories Status Changed Successfully');
		res.redirect('/admin/parts/list');
	});	
    
};

exports.copy = function(req, res) {	
	var query = Part.findOne({'_id' : req.params.id}).populate('lang_content');        	
	query.lean().exec(function (err, result) {
		
		var doc = new Part(result);
		doc._id = mongoose.Types.ObjectId();		
        doc.isNew = true; 
        doc.lang_content=[];
        doc.name = doc.name+'- copy';        
        doc.slug = doc.slug+'- copy';                               
        doc.save(async function (err,newdoc) {
	        for (var i = 0; i < result.lang_content.length; i++) {				
				var subdoc = new PartContent(result.lang_content[i]);
				subdoc._id = mongoose.Types.ObjectId();	
				subdoc.part_id =newdoc._id;	
		        subdoc.isNew = true; 	        		         
		        await subdoc.save();
				newdoc.lang_content.push(subdoc);
				await newdoc.save();
			}							
		});	
		req.flash('success', 'Part/Accessories Copied Successfully');
		res.redirect('/admin/parts/list'); 
	});	   
};

exports.gallery_delete = function(req, res) {

    var query = PartGallery.findOne({ '_id': req.params.id });
    query.exec(function(err, result) {
        pid = result.part_id;

        Part.findByIdAndUpdate(pid, { $pull: { image_gallery: req.params.id } }, function(err) {
            PartGallery.remove({ _id: req.params.id }).exec();
        });
    });

    res.send('deleted');
};