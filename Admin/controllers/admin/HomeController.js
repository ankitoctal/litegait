const db = include('configs/db.js');
const config = include('configs/config.js');
const passwordHash = require('password-hash');

exports.dashboard = function(req, res) {	
    res.render('admin/home/dashboard',{ title: 'Admin Dashboard' });
};

exports.logout = function(req, res) {	
	req.session.user =null; 
	req.session.destroy(); 
	return res.redirect('/admin/login');  
};

exports.login = function(req, res) {
	if(req.method == 'POST')
    {
    	req.checkBody('email', 'Email is required').notEmpty();
	   	req.checkBody('email', 'Email is not valid').isEmail();
	   	req.checkBody('password', 'Password is required').notEmpty();

		var errors = req.validationErrors();
		if(errors){			
		  	res.render('admin/home/login', {title: 'Admin Login', errors: errors });
		}
		else{
			//var hashedPassword = passwordHash.generate(req.body.password);

			var newuser = {email: req.body.email, $or:[{role_id: 1},{role_id: 2}]};
		  	db.collection("users").findOne(newuser, function(err, result) {
	   			if (err) throw err;      			
	   			if(result != null && passwordHash.verify(req.body.password, result.password))
	   			{
	   				req.session.user = result;    				
	   				return res.redirect('/admin/users/list');
	   			}
	   			else
	   			{
	   				req.flash('success', 'Error in Login');			
	    			return res.redirect('/admin/login'); 	
	   			}
	   			       			
	  		});
		}
    }
    else
    {
    	res.render('admin/home/login',{ title: 'Admin Login' });
    }
    
};