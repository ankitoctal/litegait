var News = include("models/News.js");
var NewsContent = include("models/NewsContent.js");

exports.list = function(req, res) {
   	var query = News.find();    
    query.sort({ created_at: -1 });
    query.exec(function (err, result) {
  		res.render('admin/news/list',{ title: 'News List',news:result});       	
	}); 
	 	    
};

exports.add = async function(req, res) {
	if(req.method == 'POST')
    {  
    	req.body.short_description = {};
		req.body.description = {};
    	res.locals.activeLanguage.forEach(function(language) {  		
  			req.body.short_description[language.code] =req.body['short_description['+language.code+']'] ;
			req.body.description[language.code] =req.body['description['+language.code+']'] ;
		});
		
   		req.checkBody('title', 'Title is required').notEmpty();
		req.checkBody('short_description.en', 'Short Description is required').notEmpty(); 
   		req.checkBody('description.en', 'Description is required').notEmpty();    	    	
   		req.checkBody('expire_date', 'Expiry Date is required').notEmpty();    	    	
		var errors = req.validationErrors();
		if(errors){		
			res.render('admin/news/add', {title: 'Add News', errors: errors,reqBody:req.body});				  		
		}
		else
		{
			var news = new News();		
			news.title =req.body.title;
			news.short_description =req.body.short_description.en;	
			news.description =req.body.description.en;	
			news.expire_date =req.body.expire_date;
			news.image =req.body.image;
			news.status = 1;			
			news.save(async function (err, newnews) {
							
				if(err)
				{
					req.flash('success', err.message);
					res.render('admin/news/add', {title: 'Add News', reqBody:req.body});			  		
				}
				else
				{					
					for (var i = 0; i < res.locals.activeLanguage.length; i++) {
						var langCode = res.locals.activeLanguage[i].code;
						var newscontent = new NewsContent();
						newscontent.news_id =newnews._id;
						newscontent.lang=langCode;						
						newscontent.short_description=req.body.short_description[langCode];
						newscontent.description=req.body.description[langCode];
						await newscontent.save();						
						newnews.lang_content.push(newscontent);
						await newnews.save();
					}		
					req.flash('success', 'News Added Successfully');			
    				return res.redirect('/admin/news/list');		
				}			
					  			
			});  		
		}
    }
    else
    {
		res.render('admin/news/add',{ title: 'Add News'});
    }
     
};
exports.edit = async function(req, res) {	
    if(req.method == 'POST')
    {
		req.body.short_description = {};
    	req.body.description = {};
    	res.locals.activeLanguage.forEach(function(language) {  			
  			req.body.short_description[language.code] =req.body['short_description['+language.code+']'] ;
  			req.body.description[language.code] =req.body['description['+language.code+']'] ;
		});
   		req.checkBody('title', 'Title is required').notEmpty();
   		req.checkBody('short_description.en', 'Short Description is required').notEmpty();
   		req.checkBody('description.en', 'description is required').notEmpty();

		var errors = req.validationErrors();
		if(errors){			
			
			res.render('admin/news/edit', {title: 'Edit News', errors: errors,reqBody:req.body});			  		
		}
		else
		{			
			var nws = {};					
			nws.title =req.body.title;
			nws.short_description =req.body.short_description.en;	
			nws.description =req.body.description.en;
			nws.expire_date =req.body.expire_date;
			if(req.body.image != "")
			{
				nws.image =req.body.image;				
  			}			
			News.findByIdAndUpdate( req.params.id, nws , function (err) {			
				if(err)
				{
					req.flash('success', err.message);
					res.render('admin/news/edit', {title: 'Edit News', reqBody:req.body});			  		
				}
				else 
				{
					var query = News.findOne({'_id' : req.params.id}).populate('lang_content');      	
			    	query.exec(async function (err, result) {    						    		
			    		for (var i = 0; i < result.lang_content.length; i++) {
			    			var k = result.lang_content[i];
			    			var langCode = k.lang;
			    			var newscontent = {};				    			
			    			newscontent.short_description=req.body.short_description[langCode];
			    			newscontent.description=req.body.description[langCode];
			    			NewsContent.findByIdAndUpdate( k._id, newscontent , function (err) {
			    			});	
						}			  			
					});

					req.flash('success', 'News Updated Successfully');			
    				return res.redirect('/admin/news/list');	
				}			
					  			
			});  		
		}
    }
    else
    {
    	var query = News.findOne({'_id' : req.params.id}).populate('lang_content');
    	query.lean().exec(function (err, result) { 		
			
    		result.short_description = {};
    		result.description = {};

	    	result.lang_content.forEach(function(content) {  
	    		result.short_description[content.lang] =content.short_description ;
	    		result.description[content.lang] =content.description ;	  			
			});   		  		
  			res.render('admin/news/edit',{ title: 'Edit News',reqBody:result});       	
		});
    	
    }
     
};

exports.delete = function(req, res) {
	var query = News.findOne({'_id' : req.params.id}).populate('lang_content');        	
	query.exec(function (err, result) {
		for (var i = 0; i < result.lang_content.length; i++) {
			var k = result.lang_content[i];			
			NewsContent.remove({_id:k._id}).exec();			
		}
	});
	News.remove({_id:req.params.id}).exec();
	req.flash('success', 'News Deleted Successfully');
	res.redirect('/admin/news/list'); 
};

exports.view = function(req, res) {	
    var query = Category.findOne({'_id' : req.params.id});        	
	query.exec(function (err, result) {
			res.render('admin/news/view',{ title: 'View Category',reqBody:result});       	
	});  
     
};

exports.status = function(req, res) {	
	var nws = {};					
	nws.status = req.params.status;			
	News.findByIdAndUpdate( req.params.id, nws , function (err) {
		req.flash('success', 'News Status Changed Successfully');
		res.redirect('/admin/news/list');
	});	
    
};
