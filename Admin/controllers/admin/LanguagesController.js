var Language = include("models/Language.js");

exports.list = function(req, res) {
   	var query = Language.find();    
    query.sort({ created_at: -1 });
    query.exec(function (err, result) {
  		res.render('admin/languages/list',{ title: 'Language List',languages:result});       	
	}); 
	 	    
};

exports.add = function(req, res) {	
    if(req.method == 'POST')
    {
    	req.checkBody('code', 'Language Code is required').notEmpty(); 
    	req.checkBody('name', 'Name is required').notEmpty();     	
		var errors = req.validationErrors();
		if(errors){						
			res.render('admin/languages/add', {title: 'Add Language', errors: errors,reqBody:req.body});			  		
		}
		else
		{
			var language = new Language();		
			language.code =req.body.code;
			language.name =req.body.name;			
			language.status = 1;
			language.save(function (err) {				
				if(err)
				{
					req.flash('success', err.message);
					res.render('admin/languages/add', {title: 'Add Language', reqBody:req.body});			  		
				}
				else
				{
					req.flash('success', 'Language Added Successfully');			
    				return res.redirect('/admin/languages/list');	
				}			
					  			
			});  		
		}
    }
    else
    {
    	res.render('admin/languages/add',{ title: 'Add Language'});
    }
     
};

exports.edit = function(req, res) {	
    if(req.method == 'POST')
    {
    	req.checkBody('code', 'Language Code is required').notEmpty(); 
    	req.checkBody('name', 'Name is required').notEmpty();     	
		var errors = req.validationErrors();
		if(errors){						
			res.render('admin/languages/edit', {title: 'Edit Language', errors: errors,reqBody:req.body});			  		
		}
		else
		{			
			var language = {};					
			language.code =req.body.code;
			language.name =req.body.name;			
			Language.findByIdAndUpdate( req.params.id, language , function (err) {				
				if(err)
				{
					req.flash('success', err.message);
					res.render('admin/languages/edit', {title: 'Edit Language', reqBody:req.body});			  		
				}
				else
				{
					req.flash('success', 'Language Updated Successfully');			
    				return res.redirect('/admin/languages/list');	
				}			
					  			
			});  		
		}
    }
    else
    {       	
    	var query = Language.findOne({'_id' : req.params.id});        	
    	query.exec(function (err, result) {    		
  			res.render('admin/languages/edit',{ title: 'Edit Language',reqBody:result});       	
		}); 
    }
     
};

exports.delete = function(req, res) {	
	Language.remove({_id:req.params.id}).exec();
	req.flash('success', 'Language Deleted Successfully');
	res.redirect('/admin/languages/list');
    
};

exports.status = function(req, res) {	
	var language = {};					
	language.status = req.params.status;			
	Language.findByIdAndUpdate( req.params.id, language , function (err) {
		req.flash('success', 'Language Status Changed Successfully');
		res.redirect('/admin/languages/list');
	});
    
};