const Speaker = include("models/Speaker.js");
const State = include("models/State.js");
const Topic = include("models/Topic.js");
const Webinar = include("models/Webinar.js");
const WebinarContent = include("models/WebinarContent.js");
const Category = include("models/Category.js");
var Testimonial = include("models/Testimonial.js");
const Order = include("models/Order.js");
const OrderProduct = include("models/OrderProduct.js");
const User = include("models/User.js");

const excel = require('node-excel-export');

exports.list = async function(req, res) {

    const speakers = await Speaker.where('status', 1).select('_id name').find();
    //const orderproduct = await OrderProduct.where({'webinar_id':{ $ne: null }}).find().exec();
    const orderproducts = await OrderProduct.where({ 'type': 'webinar' }).find().exec();
    var query = Webinar.find();
    query.sort({ created_at: -1 }).populate('speaker_id').populate('category');
    query.exec(function(err, result) {

        res.render('admin/webinars/list', { title: 'Webinar List', webinars: result, speakers: speakers, orderproducts: orderproducts });
    });

};

exports.add = async function(req, res) {

    const speakers = await Speaker.where('status', 1).select('_id name').find();
    const states = await State.where('status', 1).find();
    const topics = await Topic.where('status', 1).find();
    const categories = await Category.where('type', 'webinar').find();
    var testimonial = new Testimonial();
    var testimonials = await testimonial.getAll();

    if (req.method == 'POST') {
        req.body.description = {};
        req.body.objective = {};
        // req.body.topics = {};
        res.locals.activeLanguage.forEach(function(language) {
            req.body.description[language.code] = req.body['description[' + language.code + ']'];
            req.body.objective[language.code] = req.body['objective[' + language.code + ']'];
            // req.body.topics[language.code] =req.body['topics['+language.code+']'] ;
        });
       //  console.log(req.body); process.exit();  	
        req.checkBody('slug', 'Slug is required').notEmpty();
        req.checkBody('title', 'Title is required').notEmpty();
        req.checkBody('category', 'Category is required').notEmpty();
        //req.checkBody('description.en', 'Description is required').notEmpty(); topic
        // req.checkBody('objective.en', 'Objective is required').notEmpty();  
        // req.checkBody('topics.en', 'Topics is required').notEmpty();   date_time_1
        //req.checkBody('type', 'Type is required').notEmpty();   		
        //req.checkBody('speaker_id', 'Speaker is required').notEmpty();
       // req.checkBody('image', 'Image is required').notEmpty();
        //req.checkBody('states', 'States is required').notEmpty();
        //req.checkBody('topics', 'Topic is required').notEmpty();
        //req.checkBody('pre_requisite', 'Pre Requisite is required').notEmpty();
        //req.checkBody('education_level', 'Education Level is required').notEmpty(); length
        //req.checkBody('price', 'Price is required').notEmpty();
       // req.checkBody('date_time_1', 'Date & Time is required').notEmpty();
        //req.checkBody('location', 'Location is required').notEmpty();
        //req.checkBody('contact_hours', 'Contact Hours is required').notEmpty();
        //req.checkBody('image', 'Image is required').notEmpty();		
        var errors = req.validationErrors();
        if (errors) {
            res.render('admin/webinars/add', { title: 'Add Webinar', errors: errors, reqBody: req.body, speakers: speakers, states: states, topics: topics, categories: categories, testimonials: testimonials });
        } else {
            var webinar = new Webinar();
            webinar.title = req.body.title; 
            webinar.slug = req.body.slug;
            webinar.category = req.body.category;
           
            webinar.image = req.body.image;
            webinar.description = req.body.description.en;
            webinar.objective = req.body.objective.en;
            // webinar.topics =req.body.topics.en;  console.log() 
            webinar.states = req.body.states;                     
            webinar.pre_requisite = req.body.pre_requisite;
            webinar.education_level = req.body.education_level;
            webinar.price = req.body.price;
            webinar.date_time_1 = req.body.date_time_1;
            webinar.date_time_2 = req.body.date_time_2;
            webinar.url = req.body.url;
            webinar.recorded_url = req.body.recorded_url;
            webinar.contact_hours = req.body.contact_hours;
            webinar.testimonials = req.body.testimonials;
            webinar.meta_title = req.body.meta_title;
            webinar.meta_description = req.body.meta_description;
            webinar.status = 1;
             webinar.speaker_id = req.body.speaker_id;
            var statemsg = {};
            var webtopics = []; states
            if((typeof req.body.states != 'undefined')){
                if (typeof req.body.states == 'string') {
                webinar.states = req.body.states;
                statemsg[req.body.states] = req.body['state_msg[' + req.body.states + ']'];
                } else {
                    for (var i = 0; i < req.body.states.length; i++) {
                        var k = req.body.states[i];
                        statemsg[k] = req.body['state_msg[' + k + ']'];
                    }
                }
            }
            if((typeof req.body.topics != 'undefined')){
                if (typeof req.body.topics == 'string') {
                    webinar.topics = req.body.topics; 
                }
            }
            // if((typeof req.body.speaker_id != 'undefined')){
            //     if (typeof req.body.speaker_id == 'string') {
            //         webinar.speaker_id = req.body.speaker_id;
            //     }
            // }

            


            webinar.state_msg = JSON.stringify(statemsg);



            webinar.save(async function(err, newwebinar) {
                if (err) {
                    req.flash('success', err.message);
                    res.render('admin/webinars/add', { title: 'Add Webinar', errors: errors, reqBody: req.body, speakers: speakers, states: states, topics: topics, categories: categories, testimonials: testimonials });
                } else {
                    for (var i = 0; i < res.locals.activeLanguage.length; i++) {
                        var langCode = res.locals.activeLanguage[i].code;
                        var webinarcontent = new WebinarContent();
                        webinarcontent.webinar_id = newwebinar._id;
                        webinarcontent.lang = langCode;
                        webinarcontent.description = req.body.description[langCode];
                        webinarcontent.objective = req.body.objective[langCode];
                        if(typeof req.body.topics !="undefined"){
                            webinarcontent.topics = req.body.topics[langCode];
                        }
                        await webinarcontent.save();
                        newwebinar.lang_content.push(webinarcontent);
                        await newwebinar.save();
                    }

                    /* if(typeof req.body.speaker_id == 'string')
                    {							  console
                    	newwebinar.speaker_id.push(req.body.speaker_id);
                    	await newwebinar.save();
                    }
                    else
                    {							
                    	for (var i = 0; i < req.body.speaker_id.length; i++) {
                    		var speaker_id = req.body.speaker_id[i];
                    		newwebinar.speaker_id.push(speaker_id);
                    		await newwebinar.save();
                    		
                    	}							
                    }*/

                    req.flash('success', 'Webinar Added Successfully');
                    return res.redirect('/admin/webinars/list');
                }

            });
        }
    } else {
        res.render('admin/webinars/add', { title: 'Add Webinar', speakers: speakers, states: states, topics: topics, categories: categories, testimonials: testimonials });
    }

};

exports.edit = async function(req, res) {

    const speakers = await Speaker.where('status', 1).select('_id name').find();
    const states = await State.where('status', 1).find();
    const topics = await Topic.where('status', 1).find();
    const categories = await Category.where('type', 'webinar').find();
    var testimonial = new Testimonial();
    var testimonials = await testimonial.getAll();
 
    if (req.method == 'POST') {
        req.body.description = {};
        req.body.objective = {};
        // req.body.topics = {};
        res.locals.activeLanguage.forEach(function(language) {
            req.body.description[language.code] = req.body['description[' + language.code + ']'];
            req.body.objective[language.code] = req.body['objective[' + language.code + ']'];
            // req.body.topics[language.code] =req.body['topics['+language.code+']'] ;
        });
        req.checkBody('slug', 'Slug is required').notEmpty();
        req.checkBody('title', 'Title is required').notEmpty();
        req.checkBody('category', 'Category is required').notEmpty();  
        //req.checkBody('description.en', 'Description is required').notEmpty();  date_time_1
        // req.checkBody('objective.en', 'Objective is required').notEmpty();     				
        // req.checkBody('topics.en', 'Topics is required').notEmpty();     				
        req.checkBody('speaker_id', 'Speaker is required').notEmpty();
        //req.checkBody('image', 'Image is required').notEmpty();
        //req.checkBody('states', 'States is required').notEmpty();
        //req.checkBody('topics', 'Topic is required').notEmpty();
        //req.checkBody('pre_requisite', 'Pre Requisite is required').notEmpty();
        //req.checkBody('education_level', 'Education Level is required').notEmpty();
        //req.checkBody('price', 'Price is required').notEmpty();
       // req.checkBody('date_time_1', 'Date & Time is required').notEmpty();
       // req.checkBody('contact_hours', 'Contact Hours is required').notEmpty();
        var errors = req.validationErrors();
        if (errors) {  
            res.render('admin/webinars/edit', { title: 'Add Webinar', errors: errors, reqBody: req.body, speakers: speakers, states: states, topics: topics, categories: categories, testimonials: testimonials });
        } else {
            var date_time_1 = req.body.date_time_1;
            var date_time_2 = req.body.date_time_2;
            var webinar = {};				
            webinar.title = req.body.title;
            webinar.slug = req.body.slug;
            webinar.category = req.body.category;
            webinar.type = req.body.type;
            
            webinar.image = req.body.image;
            webinar.description = req.body.description.en;
            webinar.objective = req.body.objective.en;
            // webinar.topics =req.body.topics.en;
            //webinar.states = req.body.states; // req.body.states instanceof Array ? req.body.states.toString() : req.body.states;	
           // webinar.topics = req.body.topics; //  req.body.topics instanceof Array ? req.body.topics.toString() : req.body.topics;
            webinar.pre_requisite = req.body.pre_requisite;
            webinar.education_level = req.body.education_level;
            webinar.price = req.body.price;
            webinar.date_time_1 = date_time_1.trim();;
            webinar.date_time_2 = date_time_2.trim();;
            webinar.url = req.body.url;
            webinar.recorded_url = req.body.recorded_url;
            webinar.contact_hours = req.body.contact_hours;
            webinar.testimonials = req.body.testimonials;
            webinar.meta_title = req.body.meta_title;
            webinar.meta_description = req.body.meta_description;
            webinar.speaker_id = req.body.speaker_id;
            var webstates = [];
            var webtopics = [];
            webinar.states = req.body.states;
            webinar.topics = webtopics;
            
            var statemsg = {};
            if((typeof req.body.states != 'undefined')){
                if (typeof req.body.states == 'string') {
                    webinar.states = req.body.states;
                    statemsg[req.body.states] = req.body['state_msg[' + req.body.states + ']'];
                } else {
                    for (var i = 0; i < req.body.states.length; i++) {
                        var k = req.body.states[i];
                        statemsg[k] = req.body['state_msg[' + k + ']'];
                    }
                }
            }
            if((typeof req.body.topics != 'undefined')){
                if (typeof req.body.topics == 'string') {
                    webinar.topics = req.body.topics; 
                }
            }
      
            webinar.state_msg = JSON.stringify(statemsg);


            Webinar.findByIdAndUpdate(req.params.id, webinar, async function(err) {
                if (err) {
                    req.flash('success', err.message);
                    res.render('admin/webinars/edit', { title: 'Add Webinar', errors: errors, reqBody: req.body, speakers: speakers, states: states, topics: topics, categories: categories, testimonials: testimonials });
                } else {
                    var query = Webinar.findOne({ '_id': req.params.id }).populate('lang_content');
                    query.exec(async function(err, result) {
                        for (var i = 0; i < result.lang_content.length; i++) {
                            var k = result.lang_content[i];
                            var langCode = k.lang;
                            var webinarcontent = {};
                            webinarcontent.description = req.body.description[langCode];
                            webinarcontent.objective = req.body.objective[langCode];
                            webinarcontent.topics = req.body.topics[langCode];
                            WebinarContent.findByIdAndUpdate(k._id, webinarcontent, function(err) {});
                        }

                        //await Webinar.findByIdAndUpdate(req.params.id,{$pullAll: {speaker_id: result.speaker_id } });

                        /* if(typeof req.body.speaker_id == 'string')
                        {							
                        	result.speaker_id.push(req.body.speaker_id);
                        	await result.save();
                        }
                        else
                        {							
                        	for (var i = 0; i < req.body.speaker_id.length; i++) {
                        		var speaker_id = req.body.speaker_id[i];
                        		result.speaker_id.push(speaker_id);
                        		await result.save();									
                        	}							
                        }*/

                    });

                    req.flash('success', 'Webinar Updated Successfully');
                    return res.redirect('/admin/webinars/list');
                }

            });
        }
    } else {
        var query = Webinar.findOne({ '_id': req.params.id }).populate('lang_content');
        query.lean().exec(function(err, result) {

            result.description = {};
            result.objective = {};
            // result.topics = {};

            result.lang_content.forEach(function(content) {
                result.description[content.lang] = content.description;
                result.objective[content.lang] = content.objective;
                // result.topics[content.lang] =content.topics ;	  			
            });

            if (result.state_msg)
                result.state_msg = JSON.parse(result.state_msg);

            res.render('admin/webinars/edit', { title: 'Edit Webinar', reqBody: result, speakers: speakers, states: states, topics: topics, categories: categories, testimonials: testimonials });
        });

    }

};

exports.delete = function(req, res) {
    var query = Webinar.findOne({ '_id': req.params.id }).populate('lang_content');
    query.exec(function(err, result) {
        for (var i = 0; i < result.lang_content.length; i++) {
            var k = result.lang_content[i];
            WebinarContent.remove({ _id: k._id }).exec();
        }
    });
    Webinar.remove({ _id: req.params.id }).exec();
    req.flash('success', 'Webinar Deleted Successfully');
    res.redirect('/admin/webinars/list');

};

exports.view = function(req, res) {
    var query = Category.findOne({ '_id': req.params.id });
    query.exec(function(err, result) {
        res.render('admin/speakers/view', { title: 'View Category', reqBody: result });
    });

};

exports.status = function(req, res) {
    var webinar = {};
    webinar.status = req.params.status;
    Webinar.findByIdAndUpdate(req.params.id, webinar, function(err) {
        req.flash('success', 'Webinar Status Changed Successfully');
        res.redirect('/admin/webinars/list');
    });

};

exports.attendies = async function(req, res) {

    var users = await User.find({}).exec();

    var query = Webinar.findOne({ '_id': req.params.id });
    query.exec(async function(err, result) {
        const orderproducts = await OrderProduct.where({ 'webinar_id': result._id }).find().exec();
        
        res.render('admin/webinars/attendies', { title: 'View attendies of ' + result.title, webinar: result, orderproducts: orderproducts, users: users });
    });
};

exports.attendiesExport = async function(req, res) {

    var users = await User.find({}).exec();

    var query = Webinar.findOne({ '_id': req.params.id });
    query.exec(async function(err, result) {
        const orderproducts = await OrderProduct.where({ 'webinar_id': result._id }).find().exec();
        const specification = {
            id: {
                displayName: 'S.No',
                headerStyle: {},
                width: 220
            },
            name: {
                displayName: 'NAME',
                headerStyle: {},
                width: 220
            },
            email: {
                displayName: 'EMAIL',
                headerStyle: {},
                width: 220
            },
            phone: {
                displayName: 'PHONE',
                headerStyle: {},
                width: 220
            },
            date_time: {
                displayName: 'Date & Time',
                headerStyle: {},
                width: 220
            },
            created: {
                displayName: 'Booking Date',
                headerStyle: {},
                width: 220
            },
        }

        var temp_arr = [];
        var i = 0;
        orderproducts.forEach(function(orderproduct) {
            var tempobj = {};
            tempobj.id = i + 1;
            users.forEach(function(user) {
                if (orderproduct.user_id && user._id == orderproduct.user_id.toString()) {
                    tempobj.name = user.name;
                    tempobj.email = user.email;
                    tempobj.phone = user.phone;
                }
            });

            tempobj.date_time = orderproduct.datetime;
            tempobj.created = orderproduct.created_at.toLocaleString();
            temp_arr[i] = tempobj;
            i++;
        });


        const dataset = temp_arr;
        const report = excel.buildExport(
            [{
                name: 'Webinar Report',
                specification: specification,
                data: dataset
            }]
        );

        res.attachment('webinar_report.xlsx');
        return res.send(report);

    });
};