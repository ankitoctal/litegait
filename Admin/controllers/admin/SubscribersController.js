const Newsletter = include("models/Newsletter.js");
const sharp = require('sharp');
const fs = require('fs');
exports.list = async function(req, res) {	 
	let requestSegments = req.path.split('/');
	var query = Newsletter.find(); 
    query.sort({ created_at: -1 });
    query.exec(function (err, result) {
  		res.render('admin/subscribers/list',{ title: 'Subscribers List',subscribers:result});       	
	}); 
	 	    
};

exports.delete = function(req, res) {
	Newsletter.remove({_id:req.params.id}).exec();
	req.flash('success', 'Subscriber Deleted Successfully');
	res.redirect('/admin/subscribers/list');
    
};
