var User = include("models/User.js");
var Order = include("models/Order.js");
var ElpPayment = include("models/ElpPayment.js");
var OrderProduct = include("models/OrderProduct.js");
var passwordHash = require('password-hash');
const config = include('configs/config.js');
const excel = require('node-excel-export');
const readXlsxFile = require('read-excel-file/node');
const multer = require('multer')
const path = require("path");

exports.list = function(req, res) {
    var query = User.find({ $or: [{ 'role_id': 3 }, { 'role_id': 2 }] }).populate('elp_id');
    query.sort({ created_at: -1 });
    query.exec(function(err, result) {
        res.render('admin/users/list', { title: 'User List', users: result });
    });

};

exports.add = function(req, res) {
    if (req.method == 'POST') {
        req.checkBody('role_id', 'Role ID is required').notEmpty();
        req.checkBody('name', 'Name is required').notEmpty();
        req.checkBody('email', 'email is required').notEmpty();
        req.checkBody('email', 'email is not valid').isEmail();
        req.checkBody('password', 'Password is required').notEmpty();
        req.checkBody('password', 'Password must be 6 character long ').isLength({ min: 6, max: 20 });
        req.checkBody('cpassword', 'Confirm Password is required').notEmpty();
        req.checkBody('cpassword', 'Passwords do not match.').equals(req.body.password);

        var errors = req.validationErrors();
        if (errors) {
            res.render('admin/users/add', { title: 'Add User', errors: errors, reqBody: req.body });
        } else {
            var hashedPassword = passwordHash.generate(req.body.password);

            var user = new User();
            user.role_id = req.body.role_id;
            user.name = req.body.name;
            user.email = req.body.email;
            user.phone = req.body.phone;
            user.password = hashedPassword;
            user.address = req.body.address;
            user.facility = req.body.facility;
            user.license_number = req.body.license_number;
            user.disipline = req.body.disipline;
            user.status = 1;

            user.save(function(err) {
                if (err) {
                    req.flash('success', err.message);
                    res.render('admin/users/add', { title: 'Add User', reqBody: req.body });
                } else {
                    req.flash('success', 'User Added Successfully');
                    return res.redirect('/admin/users/list');
                }

            });
        }
    } else {
        res.render('admin/users/add', { title: 'Add User' });
    }

};



exports.view = async function(req, res) {
    var query = User.findOne({ '_id': req.params.user_id });

    var query_user = req.query.user;

    query.exec(async function(err, result) {

        var orders = await Order.find({ user_id: result._id }).populate('user_id').populate({ path: 'order_product', model: 'OrderProduct', populate: [{ path: 'product_id', model: 'Product', select: 'name slug image' }, { path: 'part_id', model: 'Part', select: 'name slug image' }, { path: 'webinar_id', model: 'Webinar', select: 'title slug date_time_1 date_time_2' }, { path: 'seminar_id', model: 'Seminar', select: 'title slug date_time' }] }).exec();
        var elpPayments = await ElpPayment.find({ user_id: result._id }).populate('elp_id').populate('user_id').sort({ created_at: -1 }).exec();

        res.render('admin/users/view', { title: 'View User', result: result, orders: orders, statuses: config.order_statuses, elpPayments: elpPayments });
    });
};





exports.edit = function(req, res) {
    if (req.method == 'POST') {
        req.checkBody('name', 'Name is required').notEmpty();
        req.checkBody('email', 'email is required').notEmpty();
        req.checkBody('email', 'email is not valid').isEmail();
         req.checkBody('role_id', 'Role ID is required').notEmpty();
        if (req.body.password || req.body.cpassword) {
            req.checkBody('password', 'Password is required').notEmpty();
            req.checkBody('password', 'Password must be 6 character long ').isLength({ min: 6, max: 20 });
            req.checkBody('cpassword', 'Confirm Password is required').notEmpty();
            req.checkBody('cpassword', 'Passwords do not match.').equals(req.body.password);
        }


        var errors = req.validationErrors();
        if (errors) {
            res.render('admin/users/edit', { title: 'Edit User', errors: errors, reqBody: req.body });
        } else {
            var user = {};
            user.name = req.body.name;
            user.role_id = req.body.role_id;
            user.email = req.body.email;
            user.phone = req.body.phone;
            user.address = req.body.address;
            user.facility = req.body.facility;
            user.license_number = req.body.license_number;
            user.disipline = req.body.disipline;

            if (req.body.password && req.body.cpassword) {
                var hashedPassword = passwordHash.generate(req.body.password);
                user.password = hashedPassword;
            }


            User.findByIdAndUpdate(req.params.user_id, user, function(err) {
                if (err) {
                    req.flash('success', err.message);
                    res.render('admin/users/edit', { title: 'Edit User', reqBody: req.body });
                } else {
                    req.flash('success', 'User Updated Successfully');
                    return res.redirect('/admin/users/list');
                }

            });
        }
    } else {
        var query = User.findOne({ '_id': req.params.user_id });
        query.exec(function(err, result) {
            result.password = '';
            console.log(result);
            res.render('admin/users/edit', { title: 'Edit User', reqBody: result });
        });``

    }

};

exports.delete = function(req, res) {
    User.remove({ _id: req.params.user_id }).exec();
    req.flash('success', 'User Deleted Successfully');
    res.redirect('/admin/users/list');

};

exports.export_in_excel = function(req, res) {

    var query = User.find({ 'role_id': 3 }).populate('elp_id');
    query.sort({ created_at: -1 });
    query.exec(function(err, result) {
        const specification = {
            id: {
                displayName: 'ID',
                headerStyle: {},
                width: 220
            },
            name: {
                displayName: 'NAME',
                headerStyle: {},
                width: 220
            },
            email: {
                displayName: 'EMAIL',
                headerStyle: {},
                width: 220
            },
            phone: {
                displayName: 'PHONE',
                headerStyle: {},
                width: 220
            },
            elp_plan: {
                displayName: 'ELP PLAN',
                headerStyle: {},
                width: 220
            },
            status: {
                displayName: 'STATUS',
                headerStyle: {},
                width: 220
            },
            created: {
                displayName: 'CREATED',
                headerStyle: {},
                width: 220
            },
        }


        var temp_arr = [];
        for (var i = 0; i < result.length; i++) {
            var tempobj = {};
            tempobj.id = result[i]._id;
            tempobj.name = result[i].name;
            tempobj.email = result[i].email;
            tempobj.phone = result[i].phone;
            tempobj.elp_plan = result[i].elp_id ? result[i].elp_id.name : '';
            tempobj.status = result[i].status == 1 ? 'Active' : 'Inactive';
            tempobj.created = result[i].created_at ? result[i].created_at.toDateString() : '';
            temp_arr[i] = tempobj;
        }
        const dataset = temp_arr
        const report = excel.buildExport(
            [{
                name: 'User Report',
                specification: specification,
                data: dataset
            }]
        );


        res.attachment('user_report.xlsx');
        return res.send(report);

    });
};


exports.role_permission = function(req, res) {

    if (req.method == 'POST') {
        User.findOne({ 'role_id': '1' }, function(err, user) {
            user.role_permission = JSON.stringify(req.body);
            user.save(function(err) {
                req.flash('success', 'Role Permission Updated Successfully');
                return res.redirect('/admin/users/role_permission');
            });

        });

    } else {
        var query = User.findOne({ 'role_id': '1' });
        query.exec(function(err, result) {
            if (result.role_permission == undefined)
                result.role_permission = "";

            res.render('admin/users/role_permission', { title: 'Role Permission', reqBody: result });
        });

    }

};

exports.import = function(req, res) {
	var hashedPassword = passwordHash.generate('123456');
	
	var absolutePath = path.resolve("public/uploads/mytable.xlsx" );
	readXlsxFile(absolutePath).then(async(rows, errors) => {
		if (!errors) {			
			for (let val of rows) {				
				if (val[1] != '') {										
					var user = {};
					user.role_id = 3;
					user.name = val[0];
					user.email = val[1];
					user.phone = val[2];
					user.address = val[4];
					user.license_number = val[3];
                    user.zip_code = val[5];
                    user.title = val[10];
                    user.organization = val[6];
                    user.country = val[7];
                    user.state = val[8];
                    user.city = val[9];
					user.facility = '';
					user.disipline = '';
					user.password = hashedPassword;
					user.status = 1;					
					User.create(user, { upsert: true }, function(err) {});										
				}
			}
		}
		else{
			res.send('Error in User Import');
		}
	});
	res.send('User Imported Successfully');
};