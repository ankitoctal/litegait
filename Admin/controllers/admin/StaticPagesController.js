var Language = include("models/Language.js");
var StaticPage = include("models/StaticPage.js");
var Testimonial = include("models/Testimonial.js");
var StaticPageContent = include("models/StaticPageContent.js");


exports.list = function(req, res) {	
	var query = StaticPage.find();
   	query.populate('lang_content');
    query.sort({ created_at: -1 });
    query.exec(function (err, result) {    	
  		res.render('admin/staticpages/list',{ title: 'Static Pages List',staticpages:result});       	
	}); 
	 	    
};

exports.add = async function(req, res) {

	var testimonial = new Testimonial();
	var testimonials = await testimonial.getAll();

    if(req.method == 'POST')
    {
    	req.checkBody('slug', 'Slug is required').notEmpty();
   		req.checkBody('title', 'Title is required').notEmpty();
   		req.checkBody('body.en', 'Body is required').notEmpty();
		var errors = req.validationErrors();
		if(errors){						
			res.render('admin/staticpages/add', {title: 'Add Static Page', errors: errors,reqBody:req.body});			  		
		}
		else
		{
			var staticpage = new StaticPage();		
			staticpage.slug =req.body.slug;
			staticpage.title =req.body.title;
			staticpage.body =req.body.body.en;
			staticpage.banner_image =req.body.banner_image;	
			staticpage.testimonials =req.body.testimonials;	
			staticpage.meta_title =req.body.meta_title;	
			staticpage.meta_description =req.body.meta_description;	

			staticpage.save(async function (err, newstaticpage) {				
				if(err)
				{
					req.flash('success', err.message);
					res.render('admin/staticpages/add', {title: 'Add Static Page', reqBody:req.body});			  		
				}
				else
				{					
					for (var i = 0; i < res.locals.activeLanguage.length; i++) {
						var langCode = res.locals.activeLanguage[i].code;
						var staticpagecontent = new StaticPageContent();
						staticpagecontent.static_page_id =newstaticpage._id;
						staticpagecontent.lang=langCode;						
						staticpagecontent.body=req.body.body[langCode];							
						await staticpagecontent.save();

						newstaticpage.lang_content.push(staticpagecontent);
						await newstaticpage.save();
					}				

					req.flash('success', 'Static Page Added Successfully');			
    				return res.redirect('/admin/staticpages/list');	
				}			
					  			
			});  		
		}
    }
    else
    {
    	res.render('admin/staticpages/add',{ title: 'Add Static Page',testimonials:testimonials});
    }
     
};

exports.edit = async function(req, res) {
	
	var testimonial = new Testimonial();
	var testimonials = await testimonial.getAll();
    if(req.method == 'POST')
    {    	
    	req.checkBody('slug', 'Slug is required').notEmpty();
   		req.checkBody('title', 'Title is required').notEmpty();
   		req.checkBody('body', 'Body is required').notEmpty();
		var errors = req.validationErrors();
		if(errors){						
			res.render('admin/staticpages/edit', {title: 'Edit Static Page', errors: errors,reqBody:req.body,testimonials:testimonials});			  		
		}
		else
		{			
			var staticpage = {};					
			staticpage.slug =req.body.slug;
			staticpage.title =req.body.title; 			
			staticpage.body =req.body.body.en;	
			staticpage.banner_image =req.body.banner_image;	
			staticpage.page_video =req.body.page_video;	
			staticpage.testimonials =req.body.testimonials;	
			staticpage.meta_title =req.body.meta_title;	
			staticpage.meta_description =req.body.meta_description;		
			StaticPage.findByIdAndUpdate( req.params.id, staticpage , function (err) {				
				if(err)
				{
					req.flash('success', err.message);
					res.render('admin/staticpages/edit', {title: 'Edit Static Page', reqBody:req.body,testimonials:testimonials});			  		
				}
				else
				{
					var query = StaticPage.findOne({'_id' : req.params.id}).populate('lang_content');      	
			    	query.exec(function (err, result) {    						    		
			    		for (var i = 0; i < result.lang_content.length; i++) {
			    			var k = result.lang_content[i];
			    			var langCode = k.lang;
			    			var staticpagecontent = {};				    			
			    			staticpagecontent.body=req.body.body[langCode];		    						    			
			    			StaticPageContent.findByIdAndUpdate( k._id, staticpagecontent , function (err) {
			    			});	
						}				  			
					});

					req.flash('success', 'Static Page Updated Successfully');			
    				return res.redirect('/admin/staticpages/list');	
				}			
					  			
			});  		
		}
    }
    else
    {
    	var query = StaticPage.findOne({'_id' : req.params.id}).populate('lang_content');      	
    	query.exec(function (err, result) {     		  		    		
  			res.render('admin/staticpages/edit',{ title: 'Edit Static Page',reqBody:result,testimonials:testimonials});       	
		});
    	
    }
     
};

exports.delete = function(req, res) {	
	var query = StaticPage.findOne({'_id' : req.params.id}).populate('lang_content');        	
	query.exec(function (err, result) {
		for (var i = 0; i < result.lang_content.length; i++) {
			var k = result.lang_content[i];			
			StaticPageContent.remove({_id:k._id}).exec();			
		}
	});
	StaticPage.remove({_id:req.params.id}).exec();
	req.flash('success', 'Static Page Deleted Successfully');
	res.redirect('/admin/staticpages/list');
    
};

exports.view = async function(req, res) {	
    var query = StaticPage.findOne({'_id' : req.params.id}).populate('lang_content');        	
	query.exec(function (err, result) {
			res.render('admin/staticpages/view',{ title: 'View Static Page',reqBody:result});       	
	});  
     
};