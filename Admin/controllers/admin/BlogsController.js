var Blog = include("models/Blog.js");
const BlogContent = include("models/BlogContent.js"); 

exports.list = function(req, res) {
   	var query = Blog.find();    
    query.sort({ created_at: -1 });
    query.exec(function (err, result) {
  		res.render('admin/blogs/list',{ title: 'Blog List',blogs:result});       	
	}); 
	 	    
};

exports.add = async function(req, res) {
    if(req.method == 'POST')
    {  
    	req.body.short_description = {};
		req.body.content = {};
    	res.locals.activeLanguage.forEach(function(language) {  		
  			req.body.short_description[language.code] =req.body['short_description['+language.code+']'] ;
			req.body.content[language.code] =req.body['content['+language.code+']'] ;
		});
		
   		req.checkBody('title', 'Title is required').notEmpty();
		req.checkBody('short_description.en', 'Short Description is required').notEmpty(); 
   		req.checkBody('content.en', 'Content is required').notEmpty();    	    	
		var errors = req.validationErrors();
		if(errors){		
			res.render('admin/blogs/add', {title: 'Add Blog', errors: errors,reqBody:req.body});				  		
		}
		else
		{
			var blog = new Blog();		
			blog.title =req.body.title;
			blog.image =req.body.image;
			blog.short_description =req.body.short_description.en;	
			blog.content =req.body.content.en;	
			blog.status = 1;			
			blog.save(async function (err, newblog) {
							
				if(err)
				{
					req.flash('success', err.message);
					res.render('admin/blogs/add', {title: 'Add Blog', reqBody:req.body});			  		
				}
				else
				{					
					for (var i = 0; i < res.locals.activeLanguage.length; i++) {
						var langCode = res.locals.activeLanguage[i].code;
						var blogcontent = new BlogContent();
						blogcontent.blog_id =newblog._id;
						blogcontent.lang=langCode;						
						blogcontent.short_description=req.body.short_description[langCode];
						blogcontent.content=req.body.content[langCode];
						await blogcontent.save();

						newblog.lang_content.push(blogcontent);
						await newblog.save();
					}		
					req.flash('success', 'Blog Added Successfully');			
    				return res.redirect('/admin/blogs/list');		
				}			
					  			
			});  		
		}
    }
    else
    {
		res.render('admin/blogs/add',{ title: 'Add Blog'});
    }
     
};
exports.edit = async function(req, res) {	
    if(req.method == 'POST')
    {
    	req.body.short_description = {};
    	req.body.content = {};
    	res.locals.activeLanguage.forEach(function(language) {  			
  			req.body.short_description[language.code] =req.body['short_description['+language.code+']'] ;
  			req.body.content[language.code] =req.body['content['+language.code+']'] ;
		});
   		req.checkBody('title', 'Title is required').notEmpty();
   		req.checkBody('short_description.en', 'Short Description is required').notEmpty();
   		req.checkBody('content.en', 'Content is required').notEmpty();

		var errors = req.validationErrors();
		if(errors){			
			
			res.render('admin/blogs/edit', {title: 'Edit Blog', errors: errors,reqBody:req.body});			  		
		}
		else
		{			
			var blog = {};					
			blog.title =req.body.title;
  			if(req.body.image != "")
			{
				blog.image =req.body.image;				
  			}
			blog.short_description =req.body.short_description.en;	
			blog.content =req.body.content.en;			
			Blog.findByIdAndUpdate( req.params.id, blog , function (err) {			
				if(err)
				{
					req.flash('success', err.message);
					res.render('admin/blogs/edit', {title: 'Edit Blog', reqBody:req.body});			  		
				}
				else 
				{
					var query = Blog.findOne({'_id' : req.params.id}).populate('lang_content');      	
			    	query.exec(async function (err, result) {    						    		
			    		for (var i = 0; i < result.lang_content.length; i++) {
			    			var k = result.lang_content[i];
			    			var langCode = k.lang;
			    			var blogcontent = {};				    			
			    			blogcontent.short_description=req.body.short_description[langCode];
			    			blogcontent.content=req.body.content[langCode];
			    			BlogContent.findByIdAndUpdate( k._id, blogcontent , function (err) {
			    			});	
						}			  			
					});

					req.flash('success', 'Blog Updated Successfully');			
    				return res.redirect('/admin/blogs/list');	
				}			
					  			
			});  		
		}
    }
    else
    {
    	var query = Blog.findOne({'_id' : req.params.id}).populate('lang_content');
    	query.lean().exec(function (err, result) { 		
			
    		result.short_description = {};
    		result.content = {};

	    	result.lang_content.forEach(function(contents) {  
	    		result.short_description[contents.lang] =contents.short_description ;
	    		result.content[contents.lang] =contents.content ;	  			
			});   		  		
  			res.render('admin/blogs/edit',{ title: 'Edit Blog',reqBody:result});       	
		});
    	
    }
     
};
exports.delete = function(req, res) {
	var query = Blog.findOne({'_id' : req.params.id}).populate('lang_content');        	
	query.exec(function (err, result) {
		for (var i = 0; i < result.lang_content.length; i++) {
			var k = result.lang_content[i];			
			BlogContent.remove({_id:k._id}).exec();			
		}
	});
	Blog.remove({_id:req.params.id}).exec();
	req.flash('success', 'Blog Deleted Successfully');
	res.redirect('/admin/blogs/list');
    
};
exports.status = function(req, res) {	
	var nws = {};					
	nws.status = req.params.status;			
	Blog.findByIdAndUpdate( req.params.id, nws , function (err) {
		req.flash('success', 'Blog Status Changed Successfully');
		res.redirect('/admin/blogs/list');
	});	
    
};
