const config = include('configs/config.js');
const User = include("models/User.js");
const Order = include("models/Order.js");
const OrderProduct = include("models/OrderProduct.js");
const passwordHash = require('password-hash');
const Product = include("models/Product.js");
const Webinar = include("models/Webinar.js");
const Seminar = include("models/Seminar.js");
const ElpPayment = include("models/ElpPayment.js"); 
const Elp = include("models/Elp.js");
const Coupon = include("models/Coupon.js");

const CommonController=require('./CommonController');
const moment = require('moment');


exports.check_email = function(req, res) {	

   	var query = User.findOne({'email':req.params.email});    
    query.sort({ created_at: -1 });
    query.exec(function (err, result) {
		if(result)
		{
			var response = {};				
			response.isEmailUnique =true;								
			return res.json(response);			
		}	
		else
		{
			var response = {};			
			return res.json(response);
		}  		
	}); 
	 	    
};

exports.add = function(req, res) {
	
	if(req.body.email !="")
	{
		var hashedPassword = passwordHash.generate(req.body.password);
		var user = new User();		
		user.role_id = 3;
		user.name =req.body.name;
		user.email =req.body.email;
		user.phone =req.body.phone;
		user.password =hashedPassword;
		user.status = 1;
		user.save(function (err) {							
			if(err)
			{
				var response = {};
				response.status ='error';
				response.msg = err.message;
				return res.json(response);			  		
			}
			else
			{
				var params =  {};
				params.slug='user_register';	
				params.to=req.body.email;				
				params.params = {};	
				params.params.name=req.body.name;		
				CommonController.sendMail(params);
	
				var response = {};
				response.status ='success';
				response.msg = 'Thank you for your registration';
				return res.json(response);	
			}			
					  			
		});		
	}
	else
	{
		var response = {};
		response.status ='error';
		response.msg = 'Email is required';
		return res.json(response);
	}     
};




exports.sendForgotPasswordMail = function(req, res) {
	
		if(req.body.email !="")
		{
			var user = new User();	
			User.findOne({email: req.body.email}, function(err,obj) {
				if(obj){
					var reset_pwd_link = config.site_url+"reset_password/"+Buffer.from(obj._id.toString()).toString('base64'); 
					var params =  {};
					params.slug='forget_password';	
					params.to=req.body.email;				
					params.params = {};	
					params.params.name=obj.name;	
					params.params.reset_pwd_link= reset_pwd_link;		
					CommonController.sendMail(params);
					var response = {};
					response.status ='success';
					response.msg = 'Please check your email to reset your password.';
					return res.json(response);		

				}else{
				 console.log(obj); 
				}
	 		});	
		}
		else
		{
			var response = {};
			response.status ='error';
			response.msg = 'Email is required';
			return res.json(response);
		}     
};




exports.resetUserPassword = function(req, res) {
	if(req.body.password !="" &&  req.body.password == req.body.confirm_password)
	{
		var hashedPassword = passwordHash.generate(req.body.password);
		User.update({_id: req.body.userid}, {
		    password: hashedPassword
		}, function(err, affected, resp) {
			if(err)
			{
				var response = {};
				response.status ='error';
				response.msg = err.message;
				return res.json(response);			  		
			}
			else
			{
				var response = {};
				response.status ='success';
				response.msg = 'Password reset successfully.';
				return res.json(response);	
			}	
		}); 
	}
	else
	{
		var response = {};
		response.status ='error';
		response.msg = 'Email is required';
		return res.json(response);
	}     
};


exports.addGuest = function(req, res) {
	if(req.body.email !="")
	{
		var hashedPassword = passwordHash.generate('123456');
		var user = new User();	
		User.findOne({email: req.body.email}, function(err,obj) { 
			if(obj){
						var response = {};
						response.status ='success';
						// response.msg = 'Thank you for your registration';
						response.data = obj;
						return res.json(response);
			}else{
				user.role_id = 3;
				user.name = 'Guest';
				user.email =req.body.email;
				user.password =hashedPassword;
				user.status = 1;
				user.save(function (err, result) {		
					if(err)
					{
						var response = {};
						response.status ='error';
						response.msg = err.message;
						return res.json(response);			  		
					}
					else
					{	
						var response = {};
						response.status ='success';
						// response.msg = 'Thank you for your registration';
						response.data = result;
						return res.json(response);
					}			
				});	
			}
		});
	}
	else
	{
		var response = {};
		response.status ='error';
		response.msg = 'Email is required';
		return res.json(response);
	}     
};





exports.login = function(req, res) {	
	
	var user = {};		
	user.role_id = 3;	
	user.email =req.body.email;		
	user.status = 1;
	
	var query = User.findOne(user).populate('elp_id');        	
	query.lean().exec(function (err, result) {		 

		if(result != null && passwordHash.verify(req.body.password, result.password))
		{			
			result.elp_plan = 0;
			if(result.elp_id && result.elp_id.persons > 1 && new Date(result.elp_expire).getTime() > new Date().getTime() ){
				result.elp_plan = 1;
			}
			else if(new Date(result.elp_expire).getTime() > new Date().getTime() ){
				result.elp_plan = 2;
			}			
			var response = {};
			response.status ='success';			
			response.msg = 'Thank you for login';
			response.data = result;
			return res.json(response);
		}
		else
		{
			var response = {};
			response.status ='error';			
			response.msg = 'Email and Password is not correct.';
			return res.json(response);	
		}	
	});		
};

exports.getUser = function(req, res) {	

   	var query = User.findOne({'_id':req.params.user_id});        
    query.exec(function (err, result) {
		var response = {};				
		response.status ='success';						
		response.data = result;
		return res.json(response);	  		
	}); 	    
};

exports.saveUser = function(req, res) {
	
	var user = {};			
	user.name =req.body.name;	
	user.phone =req.body.phone;	
	User.findByIdAndUpdate( req.body._id, user , function (err) {
		var response = {};
		response.status ='success';
		response.msg = 'Profile Updated Successfully';
		return res.json(response);
	});		
};

exports.changePassword = function(req, res) {	
	
	
	var query = User.findOne({'_id':req.body.id});        	
	query.exec(function (err, result) {
		if(result != null && passwordHash.verify(req.body.old_password, result.password))
		{
			var user = {};							
			user.password =passwordHash.generate(req.body.password);	
			User.findByIdAndUpdate( req.body.id, user , function (err) {
				var response = {};
				response.status ='success';
				response.msg = 'Password Changed Successfully';
				return res.json(response);
			});
		}
		else
		{
			var response = {};
			response.status ='error';			
			response.msg = 'Old Password is not correct.';
			return res.json(response);	
		}
	});		
};

exports.orderList = function(req, res) {	

   	var query = Order.find({'user_id':req.params.user_id}).populate('user_id');
   	query.populate({path: 'order_product',model: 'OrderProduct',populate:[{path: 'product_id',model: 'Product',select :'name slug image'},{path: 'part_id',model: 'Part',select :'name slug image'},{path: 'webinar_id',model: 'Webinar',select :'title slug date_time_1 date_time_2'},{path: 'seminar_id',model: 'Seminar',select :'title slug date_time'}] });
   	query.populate('order_address');
   	query.sort({ created_at: -1 });
    query.exec(function (err, result) {
		var response = {};				
		response.status ='success';						
		response.data = result;
		return res.json(response);	  		
	}); 	    
};

exports.orderView = function(req, res) {

   	var query = Order.findOne({'_id':req.params.order_id}).populate('user_id');
   	//query.populate({path: 'order_product',model: 'OrderProduct',populate: {path: 'product_id',model: 'Product',select :'name slug'} });
   	query.populate({path: 'order_product',model: 'OrderProduct',populate:[{path: 'product_id',model: 'Product',select :'name slug image'},{path: 'part_id',model: 'Part',select :'name slug image'},{path: 'webinar_id',model: 'Webinar',select :'title slug date_time_1 date_time_2'},{path: 'seminar_id',model: 'Seminar',select :'title slug date_time'}] });
   	query.populate('order_address');
    query.exec(function (err, result) {
		var response = {};				
		response.status ='success';						
		response.data = result;
		return res.json(response);	  		
	}); 	    
};	

exports.webinarList = function(req, res) {	

   	var query = Order.find({'user_id':req.params.user_id}).populate('user_id');
   	query.populate({path: 'order_product',model: 'OrderProduct',populate:[{path: 'webinar_id',model: 'Webinar',populate:[{path: 'speaker_id',model: 'Speaker'}]}] });
   	query.populate('order_address');
   	query.sort({ created_at: -1 });
    query.exec(function (err, result) {
		var response = {};				
		response.status ='success';	
		var data=[];	
		var i=0;
		result.forEach(function(content) {
			content.order_product.forEach(function(content2) {
				if(content2.webinar_id)
				{
					data[i]=content;
					i++;
				}
			})	
			
		});				
		response.data = data;
		return res.json(response);	  		
	}); 	    
};

exports.seminarList = function(req, res) {	

   	var query = Order.find({'user_id':req.params.user_id}).populate('user_id');
   	query.populate({path: 'order_product',model: 'OrderProduct',populate:[{path: 'seminar_id',model: 'Seminar',populate:[{path: 'speaker_id',model: 'Speaker'}]}] });
   	query.populate('order_address');
   	query.sort({ created_at: -1 });
    query.exec(function (err, result) {
		var response = {};				
		response.status ='success';	
		var data=[];	
		var i=0;
		result.forEach(function(content) {
			content.order_product.forEach(function(content2) {
				if(content2.seminar_id)
				{
					data[i]=content;
					i++;
				}
			})	
			
		});				
		response.data = data;
		return res.json(response);	  		
	});	    
};



exports.getUserPlan = function(req, res) {	

	var query = User.findOne({'_id': req.params.user_id}).populate('elp_id').populate('elp_payment_id').populate('elp_provider_payment_id'); 
    query.exec(async function (err, result) {    	
		if(err)
		{
			var response = {};
			response.status ='error';
			response.msg = err.message;
			return res.json(response);			  		
		}
		else
		{
			var response = {};
			response.status ='success';
			response.data = result; 
			response.msg = '';
			return res.json(response);	
		}	
	});	   

};