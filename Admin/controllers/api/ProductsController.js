const config = include('configs/config.js');
const Region = include("models/Region.js");
const Category = include("models/Category.js");
const Product = include("models/Product.js");
const ProductContent = include("models/ProductContent.js");
const ProductGallery = include("models/ProductGallery.js");
const ProductFeatured = include("models/ProductFeatured.js");
const ProductFiles = include("models/ProductFiles.js");
const ProductSpecification = include("models/ProductSpecification.js");
const ProductSerialno = include("models/ProductSerialno.js");
const QuoteForm = include("models/QuoteForm.js");
const Request = include("models/Request.js");
const Service = include("models/Service.js");
const Part = include("models/Part.js");
const ProductSpecificationGroup = include("models/ProductSpecificationGroup.js");

const readXlsxFile = require('read-excel-file/node');
const multer = require('multer')
const path = require("path");

exports.home_page_featured_list = async function(req, res) {
    const categories = await Category.find();
    var productfeatured = await ProductFeatured.find();

    var query = Product.find({ $or: [{ "featured_product_main": 1 }, { "featured_product": 1 }] });
    query.select('_id name slug image short_description description featured_product_main featured_product')
    query.populate('lang_content');
    query.sort({ created_at: -1 });
    query.lean().exec(async function(err, result) {
        var featured_product_main;
        var featured_product = [];
        result.forEach(async function(content) {
            if (content.featured_product_main == 1) {
                productfeatured.forEach(function(content2) {
                    if (content._id == content2.product_id.toString()) {
                        content.featured_product = content2;
                    }
                });
                featured_product_main = content;
            } else
                featured_product.push(content);
        });
        var response = {};
        response.status = 'success';
        response.featured_product_main = featured_product_main;
        response.featured_product = featured_product;
        res.json(response);
    });
};

exports.list = async function(req, res) {
    //const categories = await Category.find();	  

    var category = await Category.findOne({ 'slug': req.body.category });

    var query = Product.find({ 'status': 1, 'region': req.body.regionCode, 'category': category._id }).sort({ position: 1 }).populate('lang_content');
    //query.sort({ created_at: -1 });
    query.exec(function(err, result) {
        var response = {};
        response.status = 'success';
        response.languageCode = req.body.languageCode;
        response.regionCode = req.body.regionCode;
        response.category = category;
        result.forEach(function(content, index) {
            content.lang_content.forEach(function(content2) {
                if (content2.lang == req.body.languageCode && content2.description != "") {
                    result[index].short_description = content2.short_description;
                    result[index].description = content2.description;
                }
            });
        });
        response.data = result;
        res.json(response);
    });
};

exports.view = async function(req, res) {

    var query = Product.findOne({ 'slug': req.body.slug }).sort({ modified_at: -1 }).populate('category').populate('lang_content').populate({path: 'image_gallery',options: { sort: '_id' }});
    query.lean().exec(async function(err, result) {
        var response = {};
        response.status = 'success';
        response.languageCode = req.body.languageCode;
        response.regionCode = req.body.regionCode;
        result.lang_content.forEach(function(content) {
            if (content.lang == req.body.languageCode && content.description != "") {
                result.short_description = content.short_description;
                result.description = content.description;
            }
        });
        response.data = result;

        // var category_products = await Product.find({ 'status': 1, 'region': req.body.regionCode, 'category': result.category, '_id': { $ne: result._id } }).sort({ position: 1 }).select('slug name image');

        var category_products = await Product.find({ 'status': 1, 'region': req.body.regionCode, 'category': result.category }).sort({ position: 1 }).select('slug name image');
        response.data.category_products = category_products;
        rel_products = new Array();
        var r = 0;
        var rel_products_list = await Product.find({ 'status': 1 }).sort({ position: 1 }).select('slug name image').exec();
        if (rel_products_list && result.related_product) {
            rel_products_list.forEach(function(content) {
                if (result.related_product.includes(content._id))
                    rel_products[r++] = content;
            });
        }
        response.data.related_product_list = rel_products;

        //const productSpecification = await ProductSpecification.find({'model':{$in: result.specification_related.split(',')}});       
        const productSpecification = await ProductSpecification.find().sort({ created_at: 1, _id: 1 });
        response.data.productSpecification = productSpecification;
        const productSpecificationGroup = await ProductSpecificationGroup.find();
        response.data.productSpecificationGroup = productSpecificationGroup;

        if (result.specification_row_default) {
            var group_row_arr = result.specification_row.split(',');
            var group_row_default_arr = result.specification_row_default.split(',');

            var specificationRow = {};
            var specificationRowDefault = {};
            productSpecificationGroup.forEach(async function(group) {

                if (group_row_arr.includes(group.name)) {
                    specificationRow[group.name] = group.specification_row;
                }

                if (group_row_default_arr.includes(group.name)) {
                    specificationRowDefault[group.name] = group.specification_row;
                }
            });
            response.data.specification_row = specificationRow;
            response.data.specification_row_default = specificationRowDefault;
        }

        /* var group_row_arr = result.specification_row.split(',');
        if (result.specification_row_default) {
            var group_row_default_arr = result.specification_row_default.split(',');

            var specificationRow = new Array();
            var specificationRowDefault = new Array();
            productSpecificationGroup.forEach(async function(group) {

                if (group_row_arr.includes(group.name)) {
                    var keys = group.specification_row.split(',');
                    keys.forEach(async function(key) {
                        specificationRow.push(key);
                    });
                }

                if (group_row_default_arr.includes(group.name)) {
                    var keys = group.specification_row.split(',');
                    keys.forEach(async function(key) {
                        specificationRowDefault.push(key);
                    });
                }
            });
            response.data.specification_row = specificationRow.toString();
            response.data.specification_row_default = specificationRowDefault.toString();
        } */

        var productFiles = await ProductFiles.findOne({ 'product_id': result._id });
        response.data.productFiles = productFiles;
        res.json(response);
    });
};


exports.getQuoteForm = async function(req, res) {

    var product = '';

    if (req.body.type == 'product')
        product = await Product.findOne({ 'slug': req.body.slug }).select('name slug');
    else if (req.body.type == 'part')
        product = await Part.findOne({ 'slug': req.body.slug }).select('name slug');

    var query = QuoteForm.find({ 'product_id': product._id }).sort({ position: 1 });
    query.lean().exec(async function(err, result) {
        var response = {};
        response.status = 'success';
        response.languageCode = req.body.languageCode;
        response.regionCode = req.body.regionCode;
        response.product = product;
        response.data = result;
        res.json(response);
    });

};

exports.saveQuoteForm = async function(req, res) {

    if (req.body != "") {
        var request = new Request();
        if (req.body.type == 'product')
            request.product_id = req.body.product_id;
        else if (req.body.type == 'part')
            request.part_id = req.body.product_id;

        request.type = req.body.type;
        request.name = req.body.name;
        request.email = req.body.email;
        request.phone = req.body.phone;
        request.message = req.body.message;
        request.customData = JSON.stringify(req.body.customData);
        request.status = 1;
        request.save(function(err) {
            if (err) {
                var response = {};
                response.status = 'error';
                response.msg = 'Error in saving Request';
                response.error = err;
                res.json(response);
            } else {
                var response = {};
                response.status = 'success';
                response.msg = 'Request saved successfully';
                res.json(response);
            }
        });
    } else {
        var response = {};
        response.status = 'error';
        response.msg = 'Request Data not available';
        res.json(response);
    }
};

exports.saveServiceForm = async function(req, res) {

    if (req.body != "") {
        var service = new Service();
        if (req.body.type == 'product')
            service.product_id = req.body.product_id;
        else if (req.body.type == 'part')
            service.part_id = req.body.product_id;

        service.type = req.body.type;
        service.name = req.body.name;
        service.email = req.body.email;
        service.phone = req.body.phone;
        service.use = req.body.use;
        service.address = req.body.address;
        service.comments = req.body.comments;
        service.status = 1;
        service.save(function(err) {
            if (err) {
                var response = {};
                response.status = 'error';
                response.msg = 'Error in saving Request';
                response.error = err;
                res.json(response);
            } else {
                var response = {};
                response.status = 'success';
                response.msg = 'Schedule Service Request saved successfully';
                res.json(response);
            }
        });
    } else {
        var response = {};
        response.status = 'error';
        response.msg = 'Request Data not available';
        res.json(response);
    }
};


exports.searchBySerialNo = async function(req, res) {

    var serialno = req.body.serialno.toUpperCase();
    //var serialno='LG20X18-E003';
    /* var arr= serialno.split("-");	
    if(arr.length > 1)
    {	
    	serialno = arr[0]+'-'+arr[1][0];
    }	*/
    var response = {};
    response.status = 'error';
    response.msg = 'Sorry we are unable to find the product for that serial number pleas <a href="' + config.site_url + 'page/service-contact-us">Contact Service</a>.';
    response.serialno = serialno;

    //var query = ProductSerialno.findOne({ 'serialno' : { '$regex' : serialno, '$options' : 'i' } });
    var query = ProductSerialno.findOne({ 'serialno': serialno });
    query.exec(async function(err, result) {
        if (result) {
            response.serial = result;
            //var query2 = Product.find({ 'name' : { '$regex' : result.name, '$options' : 'i' } });    
            var query2 = Product.find({ 'name': result.name });
            query2.exec(function(err, result2) {
                response.product = result2;
                if (result2 && result2.length == 1) {
                    response.status = 'success';
                    response.msg = 'Product Found!';
                    response.data = result2[0];
                    res.json(response);
                } else if (result2 && result2.length > 1) {
                    var rdata = result2[0];
                    result2.forEach(function(content) {
                        if (content.name.toLowerCase() == result.name.toLowerCase()) {
                            rdata = content;
                        }
                    });
                    response.status = 'success';
                    response.msg = 'Product Found!';
                    response.data = rdata;
                    res.json(response);
                } else {
                    res.json(response);
                }

            });
        } else {
            res.json(response);
        }
    });
}