const Region = include("models/Region.js");
const Category = include("models/Category.js");
const CategoryContent = include("models/CategoryContent.js");
const Product = include("models/Product.js");

exports.list = async function(req, res) {	    

	var catlist=[];
	var query = Category.find({'status' : 1,'type' : req.params.type,'parent_id':'0'}).sort({ position: 1 }).populate('lang_content');   	
    query.sort({ created_at: -1 });
    query.exec(async function (err, result) {
    	var response = {};
		response.status ='success';		
		response.languageCode =req.body.languageCode;
		response.regionCode =req.body.regionCode;		
		result.forEach(function(content,index) {
			catlist.push(content._id);
			content.lang_content.forEach(function(content2) {				
				if(content2.lang == req.body.languageCode && content2.description !="")
				{	
					result[index].short_description = content2.short_description;
					result[index].description = content2.description;
				}
			});	
		});		
		response.data = result;
		var result2 = await Product.find({'status' : 1,'region' : req.body.regionCode,'category':{ $in: catlist} }).sort({ position: 1 }).select('name slug category position').exec(); 
		response.products = result2;
		res.json(response);
	}); 	 	    
};