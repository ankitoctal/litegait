const Category = include("models/Category.js");
const Product = include("models/Product.js");
const ProductFiles = include("models/ProductFiles.js");
const Banner = include("models/Banner.js");


exports.document_list = async function(req, res) {	    
   	
   	var documents = await ProductFiles.find(); 
   	var categories = await Category.find({'status' : 1,'type' : 'product','parent_id':'0'}).sort({ position: 1 }); 

    var query = Product.aggregate([

        {
          $lookup:
          {
              from: 'parts',
              localField: "_id",
              foreignField: "product_id",
              as: 'parts'
          }
      },
  ]);    
    query.exec(function (err, result) {    	
    	var response = {};
		response.status ='success';		
		response.languageCode =req.body.languageCode;				
		response.data = result;	
		response.documents = documents;	
		response.categories = categories;	
		res.json(response);      	
	}); 	 	    
};

exports.get_service_banner = async function(req, res) {	   

	Banner.findOne({type: 'service', status: 1}, function(err,obj) { 	

        if(err){
            var response = {};
            response.status ='error';
            response.msg = "Some Internal error occured!";
            return res.json(response);  
        }else{
            var response = {};
            response.data = obj; 
            response.status ='success';
            return res.json(response);
        }
	});
}
