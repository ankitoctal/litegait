const config = include('configs/config.js');
const nodemailer = require("nodemailer");
var mongoose = require('mongoose');
const StaticPage = include("models/StaticPage.js");
const Product = include("models/Product.js");
const Part = include("models/Part.js");
const Category = include("models/Category.js");
const Email = include("models/Email.js");
const Menu = include("models/Menu.js");
const Language = include("models/Language.js");
const Region = include("models/Region.js");
const Banner = include("models/Banner.js");
const BannerPage = include("models/BannerPage.js");
const Testimonial = include("models/Testimonial.js");
const Newsletter = include("models/Newsletter.js");
const News = include("models/News.js");
const Faq = include("models/Faq.js");
const Gallery = include("models/Gallery.js");
const Blog = include("models/Blog.js");
const Elp = include("models/Elp.js");
const ElpPayment = include("models/ElpPayment.js");
const User = include("models/User.js");
const Coupon = include("models/Coupon.js");
const Tag = include("models/Tag.js");
const Speaker = include("models/Speaker.js");
const Webinar = include("models/Webinar.js");
const Seminar = include("models/Seminar.js");
const Distributor = include("models/Distributor.js");
const Bulletin = include("models/Bulletin.js");

const moment = require('moment');

exports.static_pages = function(req, res) {

    var query = StaticPage.findOne({ 'slug': req.params.slug }).populate('lang_content');
    query.exec(function(err, result) {
        var response = {};
        response.status = 'success';
        response.languageCode = req.body.languageCode;
        result.lang_content.forEach(function(content) {
            if (content.lang == req.body.languageCode) {
                if (content.body != "") {
                    result.body = content.body;
                }

            }
        });

        if (result.meta_title == "") {
            result.meta_title = result.title;
        }

        response.data = result;
        res.json(response);
    });
};

exports.menu_list = async function(req, res) {

    var allPages = {};
    var allProduct = {};
    var allCategory = {};

    var result = await StaticPage.find().select('slug title').exec();
    result.forEach(function(content) {
        allPages[content._id] = content.slug;
    });
    var result = await Product.find().select('slug title').exec();
    result.forEach(function(content) {
        allProduct[content._id] = content.slug;
    });
    var result = await Category.find().select('slug name').exec();
    result.forEach(function(content) {
        allCategory[content._id] = content.slug;
    });

    var menu = new Menu();
    var menus = await menu.getAllNestedMenu(req.params.menutype, allPages, allProduct, allCategory);

    var finalmenu = {};
    var i = 0;
    Object.keys(menus).forEach(function(key) {

        finalmenu[i] = menus[key];
        finalmenu[i]['childs2'] = finalmenu[i]['childs'];
        finalmenu[i]['childs'] = {};
        var j = 0;
        Object.keys(menus[key]['childs2']).forEach(function(key2) {
            finalmenu[i]['childs'][j] = menus[key]['childs2'][key2];
            j++;
        });
        delete finalmenu[i]['childs2'];
        i++;
    });

    var response = {};
    response.status = 'success';

    response.data = finalmenu;
    res.json(response);
};

exports.language_list = async function(req, res) {

    var query = Language.find({ 'status': 1 }).select('code name');
    query.exec(function(err, result) {
        var response = {};
        response.status = 'success';
        response.data = result;
        res.json(response);
    });
};

exports.region_list = async function(req, res) {

    var query = Region.find({ 'status': 1 }).select('code name image');
    query.exec(function(err, result) {
        var response = {};
        response.status = 'success';
        response.data = result;
        res.json(response);
    });
};

exports.banner_list = function(req, res) {

    var query = Banner.find({ 'status': 1 }).sort({ order: 1 });
    query.exec(function(err, result) {
        var response = {};
        response.status = 'success';
        response.data = result;
        res.json(response);
    });
};

exports.banner_page = function(req, res) {

    var query = BannerPage.find();
    query.exec(function(err, result) {
        var banner = {};
        result.forEach(function(data) {
            banner[data.name] = data.image;
        });
        var response = {};
        response.status = 'success';
        response.data = banner;
        res.json(response);
    });
};

exports.testimonail_list = function(req, res) {

    var query = Testimonial.find({ 'status': 1 });
    if(req.body.page=='home_page')
    {
    	query.where({ 'home_page': 1 })
    }
    query.exec(function(err, result) {
        var response = {};
        response.status = 'success';
        response.data = result;
        res.json(response);
    });
};

exports.newsletter_subscribe = function(req, res) {

    var user = new Newsletter();
    user.email = req.body.email;
    user.status = 1;
    user.save(function(err) {
        var response = {};
        response.status = 'success';
        res.json(response);
    });
};

function sendMail(params) {

    var query = Email.findOne({ 'slug': params.slug });
    query.exec(function(err, result) {
        var message = result.body;

        params.params.site_url = config.site_url;
        params.params.admin_mail = config.admin_mail;
        params.params.site_name = config.site_name;

        for (prop in params.params) {
            var val = params.params[prop];
            //message = message.replace("{{"+prop+"}}",val);	
            message = message.replace(new RegExp("{{" + prop + "}}", 'gi'), val);
        }

        var smtpTransport = nodemailer.createTransport({
            host: config.senderHost,
            port: config.senderPort,
            secure: false, // true for 465, false for other ports
            auth: {
                user: config.senderUsername,
                pass: config.senderPassword
            }
        });

        var mail = {
            from: config.senderFrom,
            to: params.to,
            subject: result.subject,
            //text: "Node.js New world for me",
            html: message
        }

        smtpTransport.sendMail(mail, function(error, response) {
             smtpTransport.close();
            if (error) {
                return true; 
                //console.log(error);
            } else {
                return false ; 
                //console.log("Message sent: " + response.message);
            }

           
        });

    });
}

exports.sendMail = sendMail;

exports.testmail = function(req, res) {

    var params = {};
    params.slug = 'user_register';
    params.to = 'anil.kumar@octalsoftware.net';
    params.subject = 'Send Email Using Node.js';
    params.params = {};
    params.params.name = 'anil';
    sendMail(params);

    var response = {};
    response.status = 'success';
    response.msg = 'mail sent successfully';
    return res.json(response);
};

exports.latest_news = function(req, res) {

    var query = News.find({ 'status': 1, 'expire_date': { $gte: new Date() } }).sort({ created_at: -1 }).limit(3).populate('lang_content');
    query.lean().exec(function(err, result) {
        var response = {};
        response.status = 'success';
        response.languageCode = req.body.languageCode;
        result.forEach(function(content, index) {
            content.lang_content.forEach(function(content2) {
                if (content2.lang == req.body.languageCode && content2.description != "") {
                    result[index].short_description = content2.short_description;
                    result[index].description = content2.description;
                }
            });
        });

        response.data = result;
        res.json(response);
    });
};

exports.news_list = function(req, res) {

    var query = News.find({ 'status': 1, 'expire_date': { $gte: new Date() } }).sort({ modified_at: -1 }).populate('lang_content');;
    query.lean().exec(function(err, result) {
        var response = {};
        response.status = 'success';
        response.languageCode = req.body.languageCode;
        result.forEach(function(content, index) {
            content.lang_content.forEach(function(content2) {
                if (content2.lang == req.body.languageCode && content2.description != "") {
                    result[index].short_description = content2.short_description;
                    result[index].description = content2.description;
                }
            });
        });

        response.data = result;
        res.json(response);
    });
};

exports.news_view = function(req, res) {

    var query = News.findOne({ '_id': req.params.id }).populate('lang_content');
    query.lean().exec(function(err, result) {
        var response = {};
        response.status = 'success';
        response.languageCode = req.body.languageCode;
        result.lang_content.forEach(function(content) {
            if (content.lang == req.body.languageCode && content.description != "") {
                result.short_description = content.short_description;
                result.description = content.description;
            }
        });

        response.data = result;
        res.json(response);
    });
};

exports.faq_list = async function(req, res) {
    const categories = await Category.where({ 'type': 'faqs' }).find().sort('parent_id').select('name slug parent_id');
    var final_result = {};
    var final_result1 = [];
    var n = 0;
    categories.forEach(function(content, index) {
        n = index;
        if (content.parent_id == 0) {
            final_result[content._id] = { name: content.name, slug: content.slug, childs: 0, 'childstr': '' };
            
            categories.forEach(function(content2, index2) { 
                if (content._id == content2.parent_id) {
                    final_result[content._id]['childstr'] = final_result[content._id]['childstr'] + content2.name + ', ';

                }
            });
        }
    });

    var result = Object.keys(final_result).map(function (key) { 
        // Using Number() to convert key to number type 
        // Using obj[key] to retrieve key value 
        return [Number(key), final_result[key]]; 
    }); 
      
    // Printing values 
    for(var i = 0; i < result.length; i++) { 
        for(var z = 0; z < result[i].length; z++) { 
            final_result1[i] = result[i][z];
        } 
    } 
    var response = {};
    response.categories = final_result1;
    res.json(response);
}

exports.faq_list_old = async function(req, res) {
    const categories = await Category.where({ 'type': 'faqs', 'parent_id': "0" }).find().select('name slug');

    var final_result = {};
    categories.forEach(function(content, index) {
        final_result[content._id] = { name: content.name, slug: content.slug, childs: 0 };
    });

    var query = Faq.find({ 'status': 1 }).sort({ created_at: -1 }).populate('lang_content');
    query.exec(function(err, result) {
        var response = {};
        response.status = 'success';
        response.categories = final_result;
        response.languageCode = req.body.languageCode;
        result.forEach(function(content, index) {
            var cats = content.category.split(',');
            cats.forEach(function(cat) {
                final_result[cat]['childs'] = final_result[cat]['childs'] + 1;
            });
            //final_result[content.category]['childs'] = final_result[content.category]['childs'] + 1;
            content.lang_content.forEach(function(content2) {
                if (content2.lang == req.body.languageCode && content2.question != "") {
                    result[index].question = content2.question;
                    result[index].answer = content2.answer;
                }
            });
        });
        //console.log(result);
        response.data = result;
        res.json(response);
    });
}
exports.faq_category_list = async function(req, res) {

    var slug = req.body.slug;
    //slug='account-settings';

    const mainCategory = await Category.where({ 'slug': slug }).findOne();

    const categories = await Category.where({ 'type': 'faqs', 'parent_id': mainCategory._id }).find().select('name slug');

    var final_result = {};
    categories.forEach(function(content, index) {
        final_result[content._id] = { name: content.name, slug: content.slug, childs: 0 };
    });

    var faqresult = [];
    var query = Faq.find({ 'status': 1 }).sort({ created_at: -1 }).populate('lang_content');
    query.exec(function(err, result) {
        var response = {};
        response.status = 'success';
        response.categories = final_result;
        response.languageCode = req.body.languageCode;
        result.forEach(function(content, index) {
            var cats = content.category.split(',');
            cats.forEach(function(cat) {
                if (final_result[cat]) {
                    final_result[cat]['childs'] = final_result[cat]['childs'] + 1;
                }
            });
            content.lang_content.forEach(function(content2) {
                if (content2.lang == req.body.languageCode && content2.question != "") {
                    result[index].question = content2.question;
                    result[index].answer = content2.answer;
                }
            });
        });
        response.data = result;
        res.json(response);
    });
}

exports.faq_view = async function(req, res) {
    var query = Faq.findOne({ '_id': req.params.id }).populate('lang_content');
    query.exec(async function(err, result) {

        var str = result.category.split(',');
        var related = await Faq.find({ '_id': { $ne: result._id }, 'status': 1, 'category': { $regex: str[0], $options: 'i' } }).sort({ created_at: -1 });

        var response = {};
        response.status = 'success';
        response.data = result;
        response.related = related;
        response.languageCode = req.body.languageCode;
        result.lang_content.forEach(function(content) {
            if (content.lang == req.body.languageCode && content.question != "") {
                result.question = content.question;
                result.answer = content.answer;
            }
        });
        res.json(response);
    });
}

exports.gallery_list_old = async function(req, res) {

    /* var perPage = 10;
    var page = (req.body.page >= 1) ? req.body.page: 1;	
    var skip = (page-1) * perPage;
    //var query = Gallery.find({'status' : 1}).sort({ modified_at: -1 }).populate('lang_content');
    var total = await Gallery.count({'status' : 1}).exec();
    var query = Gallery.find({'status' : 1}).sort({ modified_at: -1 }).limit(perPage).skip(skip); */

    var query = Gallery.find({ 'status': 1 }).sort({ modified_at: -1 }).populate('lang_content');
    query.exec(function(err, result) {
        var response = {};
        response.status = 'success';
        response.languageCode = req.body.languageCode;
        result.forEach(function(content, index) {
            content.lang_content.forEach(function(content2) {
                if (content2.lang == req.body.languageCode && content2.description != "") {
                    result[index].description = content2.description;
                }
            });
        });
        response.data = result;
        res.json(response);
    });
};

exports.gallery_list = async function(req, res) {

    var result = await Gallery.find({ 'status': 1, 'type': 'photo' }).sort({ modified_at: -1 }).populate('lang_content').exec();
    result.forEach(function(content, index) {
        if(!content.position) content.position = 99999; 
        content.lang_content.forEach(function(content2) {
            if (content2.lang == req.body.languageCode && content2.description != "") {
                result[index].description = content2.description;
            }
        });
    });

    var result2 = await Gallery.find({ 'status': 1, $or:[{'type': 'video'},{'type': 'vimeo_video'}] }).sort({ modified_at: -1 }).populate('lang_content').exec();
    result2.forEach(function(content, index) {
        content.lang_content.forEach(function(content2) {
            if (content2.lang == req.body.languageCode && content2.description != "") {
                result2[index].description = content2.description;
            }
        });
    });

    var response = {};
    response.status = 'success';
    response.languageCode = req.body.languageCode;
    response.data_photo = result;
    response.data_video = result2;
    res.json(response);
};

exports.gallery_view = function(req, res) {

    var query = Gallery.findOne({ '_id': req.params.id }).populate('lang_content');
    query.exec(function(err, result) {
        var response = {};
        response.status = 'success';
        response.languageCode = req.body.languageCode;
        result.lang_content.forEach(function(content) {
            if (content.lang == req.body.languageCode && content.description != "") {
                result.description = content.description;
            }
        });
        response.data = result;
        res.json(response);
    });
};

exports.blog_list = function(req, res) {

    var query = Blog.find({ 'status': 1 }).sort({ modified_at: -1 }).populate('lang_content');
    query.exec(function(err, result) {
        var response = {};
        response.status = 'success';
        response.languageCode = req.body.languageCode;
        result.forEach(function(content, index) {
            content.lang_content.forEach(function(content2) {
                if (content2.lang == req.body.languageCode && content2.content != "") {
                    result[index].short_description = content2.short_description;
                    result[index].content = content2.content;
                }
            });
        });
        response.data = result;
        res.json(response);
    });
};

exports.blog_view = function(req, res) {

    var query = Blog.findOne({ '_id': req.params.id }).populate('lang_content');
    query.exec(function(err, result) {
        var response = {};
        response.status = 'success';
        response.languageCode = req.body.languageCode;
        result.lang_content.forEach(function(content) {
            if (content.lang == req.body.languageCode && content.content != "") {
                result.content = content.content;
            }
        });
        response.data = result;
        res.json(response);
    });
};

exports.elp_list = function(req, res) {
    var query = Elp.find({ 'status': 1 }).sort({ display_number: 1 });
    query.exec(function(err, result) {
        var response = {};
        response.status = 'success';
        response.data = result;
        res.json(response);
    });
};

exports.elp_view = function(req, res) {
    var query = Elp.findOne({ '_id': req.params.id });
    query.exec(function(err, result) {
        var response = {};
        response.status = 'success';
        response.data = result;
        res.json(response);
    });
};




exports.elp_payment = async function(req, res) {
    var elp = await Elp.findOne({'_id' : req.body.data.elp_id});

    var elpPayment = new ElpPayment();        
    elpPayment.elp_id = elp._id;
    elpPayment.user_id = req.body.data.user.user_id;
    elpPayment.amount = elp.price;
    elpPayment.payment_method = req.body.data.payment.payment_method;
    elpPayment.payment_id = req.body.data.payment.payment_id;
    elpPayment.facility_name = req.body.data.facility_name;
    elpPayment.status = 1;
    elpPayment.save(async function (err, obj) {                            
        if(err)
        {
            var response = {};
            response.status ='error';
            response.msg = err.message;
            return res.json(response);                      
        }
        else
        {    

            var dt = new Date();
            dt.setMonth(dt.getMonth() + elp.duration);
            var user = {};            
            user.elp_id = elp._id;
            user.elp_payment_id = obj._id;
            user.elp_expire = dt;  
            user.elp_purchase = new Date();    
            user.elp_provider_payment_id = null  ; 

            if(elp.persons > 1){

                var coupon = new Coupon(); 
                coupon.title = "elp coupon";
                coupon.code = elpPayment._id;  
                coupon.type = "elp_coupon"; 
                coupon.single_use = 0; 
                coupon.max = elp.persons ; 
                coupon.users = ""; 
                await coupon.save(async function (err) { }); 

            }
            // coupon saved for the elp    
            User.findByIdAndUpdate( req.body.data.user.user_id, user , function (err, userObj) {
                if(err){
                    var response = {};
                    response.status ='error';
                    response.msg = "Some Internal error occured!";
                    return res.json(response);  
                }else{
                    var response = {};
                    response.status ='success';
                    response.elp_expire = dt;
                    response.msg = 'Elp payment Completed successfully.';
                    return res.json(response);
                }
            });        
            
        }                    
    });
};





exports.elp_save_by_coupon = async function(req, res) {

    if(mongoose.Types.ObjectId.isValid(req.body.data.coupon_code)){
        await ElpPayment.findOne({'_id' : req.body.data.coupon_code},  function(err,elp_payment) { 
            if(err){
                var response = {};
                response.status ='error';
                response.msg = "Coupon not found.";
                return res.json(response);    
            }else{
                Elp.findOne({'_id' : elp_payment.elp_id}, function(err,elp) {  
                    if(!err){
                        Coupon.findOne({'code' : elp_payment._id}, function(err,coupon) { 
                            if(!err){
                                var arr =  coupon.users.split(",").map(function(item) { return item.trim(); });
                                arr = arr.filter(function(e){return e}); 
                                if(arr.indexOf(req.body.data.user.user_id) > -1){
                                    var response = {};
                                    response.status ='error';
                                    response.msg = "You already used this coupon.";
                                    return res.json(response);    
                                }
                                if(arr.length < coupon.max){
                                    var query = {'code':elp_payment._id.toString()};
                                    // coupon.users = coupon.users + ',' + req.body.data.user.user_id; 
                                    arr.push(req.body.data.user.user_id); 
                                    arr = arr.join(','); 
                                    Coupon.findOneAndUpdate(query, {users: arr}, {upsert:false}, function(err, doc){
                                        if (err) return res.send(500, { error: err });
                                        if(err)
                                        {
                                            var response = {};
                                            response.status ='error';
                                            response.msg = err.message;
                                            return res.json(response);                      
                                        }
                                        else
                                        {    
                                            Elp.findOne({'persons' : 1}, function(err,elp) {
                                                var dt = new Date();
                                                dt.setMonth(dt.getMonth() + elp.duration);
                                                var user = {};    
                                                user.elp_id = elp._id;
                                                user.elp_expire = dt;    
                                                user.elp_purchase = new Date();   
                                                user.elp_payment_id = null ;  
                                                user.elp_provider_payment_id = elp_payment._id; 

                                                User.findByIdAndUpdate( req.body.data.user.user_id, user , function (err) {
                                                var response = {};
                                                response.status ='success';
                                                response.elp_expire = dt;
                                                response.msg = 'Coupon applied successfully.';
                                                
                                                ElpPayment.findByIdAndUpdate( elp_payment._id, {$inc : {'coupon_used' : 1}} , function (err) {}); 
                                                // User.findByIdAndUpdate( elp_payment.user_id, {$inc : {'elm_coupon_used' : 1}} , function (err) {}); 
                                                return res.json(response);
                                                });    
                                             }) 
                                        }
                                    });
                                }
                                else{
                                    var response = {};
                                    response.status ='error';
                                    response.msg = "Maximum no. of limit reached for this coupon.";
                                    return res.json(response);    
                                }
                            }
                        });
                    }
                });

            }
        });   
    }else{
        var response = {};
        response.status ='error';
        response.msg = "Coupon not found";
        return res.json(response);           

    }
};









exports.page_testimonials = function(req, res) {

    var query = StaticPage.findOne({ '_id': req.body._id }).populate('testimonials');
    if (req.body.page == 'static_page') {
        query = StaticPage.findOne({ '_id': req.body._id }).populate('testimonials');
    } else if (req.body.page == 'product') {
        query = Product.findOne({ '_id': req.body._id }).populate('testimonials');
    } else if (req.body.page == 'part') {
        query = Part.findOne({ '_id': req.body._id }).populate('testimonials');
    }
    else if (req.body.page == 'speaker') {
        query = Speaker.findOne({ '_id': req.body._id }).populate('testimonials');
    }
    else if (req.body.page == 'webinar') {
        query = Webinar.findOne({ '_id': req.body._id }).populate('testimonials');
    }
    else if (req.body.page == 'seminar') {
        query = Seminar.findOne({ '_id': req.body._id }).populate('testimonials');
    }

    query.exec(async function(err, result) {    	
    	/*if(result.testimonials == null)
    	{
    		result.testimonials = await Testimonial.find().exec();
    	} */
        if(err){
            var response = {};
            response.status ='error';
            response.msg = "Testimonial not found";
            return res.json(response);   
        }else{
            var response = {};
            response.status = 'success';
            response.data = result.testimonials;
            res.json(response);
        }
    });
};

// send_contact_email


exports.send_contact_email = function(req, res) {
        if(req.body.data.message !="")
        {

            var params =  {};
            params.slug='contact_us';    
            params.to=config.admin_mail;;                
            params.params = {};      
            params.params.first_name= req.body.data.first_name;        
            params.params.last_name= req.body.data.last_name;
            params.params.email= req.body.data.email;
            params.params.address= req.body.data.address;
            params.params.message= req.body.data.message;

            var flag = sendMail(params);
            if(flag){
            	var response = {};
                response.status ='error';
                response.msg = 'Internal error occured!';
                return res.json(response);                 
            }else {
                var response = {};
                response.status ='success';
                response.msg = 'Successfully sent your message to Admin.';
                return res.json(response);
            }
  

        }
        else
        {
            var response = {};
            response.status ='error';
            response.msg = 'Message can not be empty.';
            return res.json(response);
        }  
};

exports.get_tags_list = function(req, res) {

    var query =  Tag.find().collation({locale: "en" }).sort({name:1}); 

    query.exec(function(err, result) {
        var response = {};
        response.status = 'success';
        response.data = result;
        return res.json(response);
    });
}; 

exports.get_distributor_list = async function(req, res) {

    var regionData =  await Region.findOne({'code': req.body.regionCode}); 
    var query =  Distributor.find({ 'status': 1, 'region': req.body.regionCode}).sort({name:1}); 

    query.exec(function(err, result) {
        var response = {};
        response.status = 'success';
        response.data = result;
        response.regionData = regionData;
        return res.json(response);
    });
}; 

exports.get_bulletin_list = function(req, res) {
   	var query = Bulletin.find({'status': 1});    
    query.sort({ publish_date: -1 });
    query.exec(function (err, result) {
  		var response = {};
        response.status = 'success';
        response.data = result;        
        return res.json(response);
	});     
};