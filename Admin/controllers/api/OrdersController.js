const Order = include("models/Order.js");
const Product = include("models/Product.js");
const OrderProduct = include("models/OrderProduct.js");
const OrderAddress = include("models/OrderAddress.js");
const Coupon = include("models/Coupon.js");
const Webinar = include("models/Webinar.js");

const moment = require('moment');
var CommonController = require('./CommonController');

exports.save = async function(req, res) {

    var order_total = await Order.count({ 'created_at': { $gte: moment().startOf('day') } }).exec();
    var new_order_id = moment().format("YYYYMMDD") + (order_total + 1);

    var order = new Order();
    order.order_id = new_order_id;
    order.user_id = req.body.user.user_id;
    order.subtotal = req.body.cart.subtotal;
    order.discount = req.body.cart.discount;
    order.discountcode = req.body.cart.discountcode;
    order.total = req.body.cart.total;
    order.payment_method = req.body.payment.payment_method;
    order.payment_id = req.body.payment.payment_id;
    order.status = 1;
    order.save(async function(err, newOrder) {

        var orderaddress = new OrderAddress();
        orderaddress.order_id = newOrder._id;
        if (req.body.address) {

            orderaddress.billing_fname = req.body.address.billing_fname;
            orderaddress.billing_lname = req.body.address.billing_lname;
            orderaddress.billing_phone = req.body.address.billing_phone;
            orderaddress.billing_email = req.body.address.billing_email;
            orderaddress.billing_add1 = req.body.address.billing_addr1;
            orderaddress.billing_add2 = req.body.address.billing_add2;
            orderaddress.billing_city = req.body.address.billing_city;
            orderaddress.billing_state = req.body.address.billing_state;
            orderaddress.billing_country = req.body.address.billing_country;
            orderaddress.billing_zipcode = req.body.address.billing_zipcode;
            orderaddress.shipping_fname = req.body.address.shipping_fname;
            orderaddress.shipping_lname = req.body.address.shipping_lname;
            orderaddress.shipping_phone = req.body.address.shipping_phone;
            orderaddress.shipping_email = req.body.address.shipping_email;
            orderaddress.shipping_add1 = req.body.address.shipping_addr1;
            orderaddress.shipping_add2 = req.body.address.shipping_add2;
            orderaddress.shipping_city = req.body.address.shipping_city;
            orderaddress.shipping_state = req.body.address.shipping_state;
            orderaddress.shipping_country = req.body.address.shipping_country;
            orderaddress.shipping_zipcode = req.body.address.shipping_zipcode;
        }
        await orderaddress.save();
        newOrder.order_address = orderaddress;
        await newOrder.save();
        var webinar_info_mail = [];
        for (var i = 0; i < req.body.cart.items.length; i++) {
            var item = req.body.cart.items[i];
            var orderproduct = new OrderProduct();
            orderproduct.order_id = newOrder._id;
            orderproduct.user_id = req.body.user.user_id;
            orderproduct.type = item.type;

            if (item.type == 'product')
                orderproduct.product_id = item.id;
            else if (item.type == 'parts'){
                orderproduct.part_id = item.id;
                orderproduct.type='part';
            }    
            else if (item.type == 'accessories'){
                orderproduct.part_id = item.id;
                orderproduct.type='part';
            }
            else if (item.type == 'webinar') {
                orderproduct.webinar_id = item.id;
                orderproduct.datetime = item.datetime;
            } else if (item.type == 'seminar') {
                orderproduct.seminar_id = item.id;
            }
            
            orderproduct.price = item.price;
            orderproduct.qty = item.qty;
            orderproduct.total = item.total;

            await orderproduct.save();
            newOrder.order_product.push(orderproduct);
            await newOrder.save();

            if (req.body.cart.items[i].type == "webinar") {
                // webinar_info_mail = webinar_info_mail ? (webinar_info_mail+', ') : webinar_info_mail ; 
                // webinar_info_mail += req.body.cart.items[i].name ;
                webinar_info_mail[i] = req.body.cart.items[i].id;
            }
        }
        var webinar_info_mail_content = '';
        if (webinar_info_mail.length > 0) {
            await Webinar.find({
                '_id': { $in: webinar_info_mail }
            }, function(err, docs) {
                if (!err) {
                    docs.forEach((message, index) => {
                        // console.log(message); 
                        webinar_info_mail_content += '<h4>' + index + ').Title: ' + message.title + '</h4><br />';
                        webinar_info_mail_content += '<h4>&nbsp; Description: </h4><p>' + message.description + '</p><br />';
                        webinar_info_mail_content += '<h4>&nbsp; Objective: </h4><p>' + message.objective + '</p><br />';
                        webinar_info_mail_content += '<h4>&nbsp; Topics: </h4><p>' + message.topics + '</p><br />';
                        webinar_info_mail_content += '<h4>&nbsp; Pre Requisite: </h4><p>' + message.pre_requisite + '</p><br />';
                        webinar_info_mail_content += '<h4>&nbsp; Date: </h4><p>' + moment(message.date_time_1).format('D MMM, YYYY') + '-' + moment(message.date_time_2).format('D MMM, YYYY') + '</p><br />';
                        webinar_info_mail_content += '<h4>&nbsp; Contact hour: </h4><p>' + message.contact_hours + '</p><br />';
                        webinar_info_mail_content += '<h4>&nbsp; Meeting Link: </h4><p>' + message.url + '</p><br /><hr>';
                    });
                }
            });
        }

        if (webinar_info_mail_content) {
            var params = {};
            params.slug = 'webinar-purchase-email';
            params.to = req.body.user.email;
            params.params = {};
            params.params.name = req.body.user.name;
            params.params.details = webinar_info_mail_content;
            var str = CommonController.sendMail(params);
            // console.log(str); 
            // console.log('hello'); 
        }

        var query = Order.findOne({ '_id': newOrder._id }).populate('user_id', 'name email phone').populate('order_address');
        query.populate({ path: 'order_product', model: 'OrderProduct', populate: [{ path: 'product_id', model: 'Product', select: 'name slug image' }, { path: 'part_id', model: 'Part', select: 'name slug image' }, { path: 'webinar_id', model: 'Webinar', select: 'title slug date_time_1 date_time_2' }, { path: 'seminar_id', model: 'Seminar', select: 'title slug date_time' }] });

        query.exec(function(err, result) {
            var params = {};
            params.slug = 'order_success';
            params.to = result.user_id.email;
            params.params = {};
            params.params.name = result.user_id.name;
            params.params.details = order_details(result);
            CommonController.sendMail(params);
        });

        var response = {};
        response.status = 'success';
        response.data = req.body;
        return res.json(response);
    });

};

function order_details(result) {
    var strDetails = '<table border="1" style="width:100%">';

    strDetails += '<tr>';
    strDetails += '<th>Order Id</th><td>' + result.order_id + '</td>';
    strDetails += '</tr>';

    strDetails += '<tr>';
    strDetails += '<th>Customer</th><td>';
    strDetails += '<b>Name : </b>' + result.user_id.name + '<br/>';
    strDetails += '<b>Email : </b>' + result.user_id.email + '<br/>';
    strDetails += '<b>Phone : </b>' + result.user_id.phone + '<br/>';
    strDetails += '</td>';
    strDetails += '</tr>';

    strDetails += '<tr>';
    strDetails += '<th>Order Product</th><td>';
    strDetails += '<table border="1" style="width: 90%;margin: 18px;">';
    strDetails += '<tr>';
    strDetails += '<th>Sr.No</th><th>Name</th><th>Price</th><th>Qty</th><th>Total</th>';
    strDetails += '</tr>';

    var i = 1;
    result.order_product.forEach(function(product) {
        strDetails += '<tr>';
        strDetails += '<td>' + i + '</td>';
        if (product.product_id)
            strDetails += '<td>' + product.product_id.name + '</td>';
        else if (product.part_id)
            strDetails += '<td>' + product.part_id.name + '</td>';
        else if (product.webinar_id)
            strDetails += '<td>' + product.webinar_id.title + '</td>';
        else if (product.seminar_id)
            strDetails += '<td>' + product.seminar_id.title + '</td>';

        strDetails += '<td>$' + product.price + '</td>';
        strDetails += '<td>$' + product.qty + '</td>';
        strDetails += '<td>$' + product.total + '</td>';
        strDetails += '</tr>';
        i++;
    });

    strDetails += '</table>';
    strDetails += '</td>';
    strDetails += '</tr>';

    strDetails += '<tr>';
    strDetails += '<th>Billing Address</th><td>';
    strDetails += '<b>' + result.order_address.billing_fname + ' ' + result.order_address.billing_lname + '</b><br/>';
    strDetails += result.order_address.billing_add1 + '<br/>';
    strDetails += '<b>City : </b>' + result.order_address.billing_city + '<br/>';
    strDetails += '<b>State : </b>' + result.order_address.billing_state + '<br/>';
    strDetails += '<b>Country : </b>' + result.order_address.billing_country + '<br/>';
    strDetails += '<b>Zipcode : </b>' + result.order_address.billing_zipcode + '<br/>';
    strDetails += '<b>Phone : </b>' + result.order_address.billing_phone + '<br/>';
    strDetails += '<b>Email : </b>' + result.order_address.billing_email + '<br/>';
    strDetails += '</td></tr>';

    strDetails += '<tr>';
    strDetails += '<th>Shipping Address</th><td>';
    strDetails += '<b>' + result.order_address.shipping_fname + ' ' + result.order_address.shipping_lname + '</b><br/>';
    strDetails += result.order_address.shipping_add1 + '<br/>';
    strDetails += '<b>City : </b>' + result.order_address.shipping_city + '<br/>';
    strDetails += '<b>State : </b>' + result.order_address.shipping_state + '<br/>';
    strDetails += '<b>Country : </b>' + result.order_address.shipping_country + '<br/>';
    strDetails += '<b>Zipcode : </b>' + result.order_address.shipping_zipcode + '<br/>';
    strDetails += '<b>Phone : </b>' + result.order_address.shipping_phone + '<br/>';
    strDetails += '<b>Email : </b>' + result.order_address.shipping_email + '<br/>';
    strDetails += '</td></tr>';


    strDetails += '<tr>';
    strDetails += '<th>Payment Method</th><td>' + result.payment_method + '</td>';
    strDetails += '</tr>';

    strDetails += '<tr>';
    strDetails += '<th>Payment Id</th><td>' + result.payment_id + '</td>';
    strDetails += '</tr>';

    strDetails += '<tr>';
    strDetails += '<th>Sub Total</th><td>$' + result.sub_total + '</td>';
    strDetails += '</tr>';

    if (result.discount > 0) {
        strDetails += '<tr>';
        strDetails += '<th>Discount</th><td>$' + result.discount + '</td>';
        strDetails += '</tr>';
    }

    strDetails += '<tr>';
    strDetails += '<th>Total</th><td>$' + result.total + '</td>';
    strDetails += '</tr>';

    strDetails += '</table>';

    return strDetails;
}

exports.mail = async function(req, res) {

    var query = Order.findOne({ '_id': req.params.order_id }).populate('user_id', 'name email phone').populate('order_address');
    query.populate({ path: 'order_product', model: 'OrderProduct', populate: [{ path: 'product_id', model: 'Product', select: 'name slug image' }, { path: 'part_id', model: 'Part', select: 'name slug image' }, { path: 'webinar_id', model: 'Webinar', select: 'title slug date_time_1 date_time_2' }, { path: 'seminar_id', model: 'Seminar', select: 'title slug date_time' }] });

    query.exec(function(err, result) {

        var params = {};
        params.slug = 'order_success';
        params.to = result.user_id.email;
        params.params = {};
        params.params.name = result.user_id.name;
        params.params.details = order_details(result);

        var str = CommonController.sendMail(params);

        var response = {};
        response.status = "success";
        response.msg = "Mail Sent Successfully.";
        response.to = result.user_id.email;
        return res.json(response);
    });
}


exports.checkCouponCode = async function(req, res) {

    var query = Coupon.findOne({ 'code': req.body.couponCode, 'status': 1, 'expire': { $gte: new Date() } });
    query.exec(async function(err, result) {

        if (result != null && result.applied_on != null) {
            var items = req.body.cart.items;
            total = 0;
            items.forEach(function(content) {
                if (result.applied_on.indexOf(content.type) != -1) {
                    total = total + content.total;
                } else if (result.applied_on.indexOf('category') != -1 && result.category != null && result.category.indexOf(content.category) != -1) {
                    total = total + content.total;
                }
            });
            req.body.cart.subtotal = total;
        }

        var response = {};

        if (result != null && result.single_use == 1) {
            var cnt = await Order.findOne({ 'user_id': req.body.user.user_id, 'discountcode': req.body.couponCode }).exec();
            if (cnt) {
                response.status = 'error';
                response.msg = 'Coupon Code already used.';
                response.type = 'already used.';
                return res.json(response);
            }
        }

        if (result == null) {
            response.status = 'error';
            response.msg = 'Coupon Code is not valid.';
            response.type = 'Result Null.';
        } else if (req.body.cart.subtotal <= 0) {
            response.status = 'error';
            response.msg = 'Coupon Code not Applied.';
            response.type = 'cart total is zero.';
        } else if (result.min > req.body.cart.subtotal) {
            response.status = 'error';
            response.msg = 'Coupon Code Applied on Min. Purchase of $' + result.min;
            response.type = 'Min Purchase Error.';
        } else if (result.min > req.body.cart.subtotal) {
            response.status = 'error';
            response.msg = 'Coupon Code Applied on Min. Purchase of $' + result.min;
            response.type = 'Min Purchase Error.';
        } else if (result.users && result.users.length >= 1 && result.users.indexOf(req.body.user.user_id) == -1) {
            response.status = 'error';
            response.msg = 'Coupon Code is not valid.';
            response.type = 'Not for this user';
        } else if (result.type == 'fix') {
            req.body.cart.total = req.body.cart.subtotal - result.amount;
            response.status = 'success';
            response.discount = result.amount;
            response.discountcode = req.body.couponCode;
            response.msg = 'Coupon Code Applied Successfully. You Got $' + result.amount + ' Discount';
        } else if (result.type == 'percent') {
            var discount = (req.body.cart.subtotal * result.percent / 100);
            discount = discount.toFixed(2);
            req.body.cart.total = req.body.cart.total - discount;
            req.body.cart.total = req.body.cart.total.toFixed(2);
            response.status = 'success';
            response.discount = discount;
            response.discountcode = req.body.couponCode;
            response.msg = 'Coupon Code Applied Successfully. You Got $' + discount + ' Discount';
        }

        return res.json(response);
    });

}