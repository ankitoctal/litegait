const Region = include("models/Region.js");
const Category = include("models/Category.js");
const Part = include("models/Part.js");
const PartContent = include("models/PartContent.js"); 
const QuoteForm = include("models/QuoteForm.js");
const Request = include("models/Request.js");
const Product = include("models/Product.js");


exports.list = async function(req, res) {	    

    //var categories = await Category.find({'type' : req.body.type,'parent_id':"0"}).select('type name slug');
    //var categories = await Category.find({'type' : req.body.type}).select('parent_id type name slug');
    var categories = await Category.aggregate([
    				 	{ $match: { "type": req.body.type } },
						{ $lookup: {from: 'parts',localField: "_id",foreignField: "category",as: 'parts'} }
					  ]);

    var product ="";   

	var query = Part.find({'status' : 1,'type' : req.body.type}).sort({ position: 1 }).populate('lang_content');   	
	if(req.body.category != null)
	{
		if(req.body.is_product=='product')
		{
			product = await Product.findOne({'slug':req.body.category}).select('name slug');
			if(product)			
			{
				query.find({'status' : 1,'product_id':product._id});
			}
		}
		else
		{
			var category = await Category.findOne({'slug':req.body.category});
			if(category)			
			{				
				var allcategory = await Category.find({$or:[{'parent_id': category._id},{'_id': category._id}]}).select('id parent_id name');				
				var allids = new Array();
				allcategory.forEach(function(content,index) {
					allids[index]=content._id;
				});
				
				query.find({'status' : 1,'category':{ $in: allids}});
				//query.find({'status' : 1,'category':category._id});
			}
		}	
	}	

    query.sort({ created_at: -1 });
    query.exec(function (err, result) {		
    	var response = {};
		response.status ='success';		
		response.languageCode =req.body.languageCode;
		response.regionCode =req.body.regionCode;
		response.categories = categories;		
		response.product = product;		
		result.forEach(function(content,index) {
			content.lang_content.forEach(function(content2) {				
				if(content2.lang == req.body.languageCode && content2.description !="")
				{	
					result[index].short_description = content2.short_description;
					result[index].description = content2.description;
				}
			});	
		});
		response.data = result;
		res.json(response);
	}); 	 	    
};

exports.view = async function(req, res) {	
	var query = Part.findOne({'slug' : req.body.slug}).populate('lang_content').populate('category').populate('image_gallery');    	    
    query.lean().exec(async function (err, result) {
    	var response = {};
		response.status ='success';		
		response.languageCode =req.body.languageCode;
		response.regionCode =req.body.regionCode;
		result.lang_content.forEach(function(content) {
			if(content.lang == req.body.languageCode && content.description !="")
			{		
				result.short_description = content.short_description;
				result.description = content.description;
			}
		});				
		response.data = result;
		var categories = await Category.find({'type' : result.type,'parent_id':"0"}).select('name slug');
		response.categories = categories;
		if(result.related_product instanceof Array){
			result.related_product = result.related_product.filter(function (el) {
			  return el ? true : false;
			});
		}
		
		if((typeof result.related_product == 'string' && result.related_product ) || ( result.related_product instanceof Array && result.related_product.length > 0))
		{
			var related_product_arr =  result.related_product instanceof Array ? result.related_product : result.related_product.split(",");
			var rel_products = await Part.find({'status' : 1,'_id':{ $in: related_product_arr}}).sort({ modified_at: -1 }).select('slug name image');
			response.data.related_product = rel_products;
		}

		res.json(response);
	}); 	 	    
};