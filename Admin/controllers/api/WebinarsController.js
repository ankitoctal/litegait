const Speaker = include("models/Speaker.js");
const Webinar = include("models/Webinar.js");
const Topic = include("models/Topic.js");
const Seminar = include("models/Seminar.js"); 
const Category = include("models/Category.js");
const SeminarRequest = include("models/SeminarRequest.js");
const State = include("models/State.js");
const OrderProduct = include("models/OrderProduct.js");
const Order = include("models/Order.js");
const StaticPage = include("models/StaticPage.js");
const moment = require('moment');
var CommonController = require('./CommonController');



exports.upcoming_list = async function(req, res) {	    
   	
    var query = Webinar.find({'status' : 1,'date_time_1':{$gte: new Date()}}).populate('lang_content').populate({path: 'speaker_id',model: 'Speaker',select :'name slug image'}).sort({ date_time: 1 }); 
    if(req.body.limit >= 1)
    {  
    	query.limit(parseInt(req.body.limit,10));
    }   	

    query.sort({ created_at: -1 });
    query.lean().exec(function (err, result) {    	
    	var response = {};
		response.status ='success';		
		response.languageCode =req.body.languageCode;		
		result.forEach(function(content,index) {
			content.lang_content.forEach(function(content2) {				
				if(content2.lang == req.body.languageCode && content2.description !="")
				{	
					result[index].description = content2.description;
					result[index].objective = content2.objective;					
				}
			});	
		});
		response.data = result;	
		res.json(response);      	
	}); 	 	    
};

exports.instructor_list = async function(req, res) {	   
   	
    var query = Speaker.find({'status' : 1}).populate('lang_content').sort({ lname: 1 });     
    query.lean().exec(function (err, result) {    	
    	var response = {};
		response.status ='success';		
		response.languageCode =req.body.languageCode;		
		result.forEach(function(content,index) {
			content.lang_content.forEach(function(content2) {				
				if(content2.lang == req.body.languageCode && content2.bio !="")
				{	
					result[index].bio = content2.bio;										
				}
			});	
		});
		response.data = result;	
		res.json(response);      	
	}); 	 	    
};

exports.instructors_view = async function(req, res) {
   	
    var query = Speaker.findOne({'slug' : req.params.slug}).populate('lang_content'); 
    query.sort({ created_at: -1 });
    query.lean().exec(async function (err, result) { 
    	var webinars = await Webinar.find({'status' : 1,'speaker_id' :  { "$in" : result._id}}).sort({ name: 1 }).select('title slug date_time_1').exec(); 
    	var seminars = await Seminar.find({'status' : 1,'speaker_id' :  { "$in" : result._id},'date_time':{ $gte: new Date() }}).sort({ name: 1 }).select('title slug').exec(); 
    	var response = {};
		response.status ='success';		
		response.languageCode =req.body.languageCode;	
		result.webinars = webinars;	
		result.seminars = seminars;	
		result.lang_content.forEach(function(content2,index) {				
			if(content2.lang == req.body.languageCode && content2.bio !="")
			{	
				result.bio = content2.bio;										
			}
		});	
		
		response.data = result;	
		res.json(response);      	
	}); 	 	    
};

exports.webinar_list = async function(req, res) {	   
   	
   	var categories = await Category.find({'status' : 1,'type' : 'webinar','parent_id':'0'}).sort({ name: 1 });  
   	var topics = await Topic.find({'status' : 1}).sort({ name: 1 });  
   	
    var query = Webinar.find({'status' : 1}).populate('lang_content').populate('speaker_id').sort({ name: 1 }).populate('category'); 
    query.sort({ date_time_1: 1 });
    if(req.body.type == 'recording' && req.body.category)
    {
    	var category = await Category.findOne({'slug' : req.body.category});
	    query.where({'date_time_1':{$lte: new Date()},'category':category._id});
    }
    else if(req.body.type == 'recording')
    {    	
	    query.where({'date_time_1':{$lte: new Date()}});
    }
    else if(req.body.category){
    	var category = await Category.findOne({'slug' : req.body.category});    	
    	query.where({'category':category._id});
    }
    /* if(req.body.type == 'recording')
    {
    	/*
    	if(req.body.category == 'topics' && req.body.subcat){
	    	var topicdata = await Topic.findOne({'slug' : req.body.subcat});
	    	query.where({'date_time_1':{$lte: new Date()}, topics: { '$in': [topicdata.topic] }});
    	}else{
	    	var category = await Category.findOne({'slug' : req.body.category});
	    	query.where({'date_time_1':{$lte: new Date()},'category':category._id});
    	} // comment end here for topic
    	 
    	if(req.body.category){
	    	var category = await Category.findOne({'slug' : req.body.category});
	    	query.where({'date_time_1':{$lte: new Date()},'category':category._id});
    	}else{
	    	var category = await Category.find({'type' : 'webinar'});	    	 	
	    	query.where({'date_time_1':{$lte: new Date()}});
    	}


    }else if(req.body.type != null)
    {

    	if(req.body.category){
	    	var category = await Category.findOne({'slug' : req.body.category});    	
	    	query.where({'date_time_1':{$gte: new Date()},'category':category._id});
    	}else{
	    	var category = await Category.find({'type' : 'webinar'});    	    	 	
	    	query.where({'date_time_1':{$gte: new Date()}});    		
    	}

    }else if(req.body.type == null)
    {    	    	
    	query.where({'date_time_1':{$gte: new Date()}});
    } */
    query.lean().exec(function (err, result) {   
    	var response = {};    	
		response.status ='success';		
		response.languageCode =req.body.languageCode;		
		response.categories =categories;		
		response.topics = topics; 
		result.forEach(function(content,index) {
			content.lang_content.forEach(function(content2) {				
				if(content2.lang == req.body.languageCode && content2.description !="")
				{	
					result[index].description = content2.description;										
				}
			});	
		});
		response.data = result;	
		res.json(response);      	
	}); 	 	    
};

exports.webinar_view = async function(req, res) {

	var categories = await Category.find({'status' : 1,'type' : 'webinar','parent_id':'0'}).sort({ name: 1 });  	   
	var states = await State.find({'status' : 1}).sort({ name: 1 });  	   
   	
    var query = Webinar.findOne({'slug' : req.params.slug}).populate('lang_content').populate('speaker_id').populate('category'); 
    query.sort({ created_at: -1 });
    query.lean().exec(function (err, result) {    	
    	var response = {};
		response.status ='success';		
		response.languageCode =req.body.languageCode;			
		response.categories = categories;	
		response.states = states;	
		result.lang_content.forEach(function(content2,index) {				
			if(content2.lang == req.body.languageCode && content2.description !="")
			{	
				result.description = content2.description;										
			}
		});	
		
		response.data = result;	
		res.json(response);      	
	}); 	 	    
};


exports.seminar_list = async function(req, res) {

	var seminarTopText = await StaticPage.findOne({'slug':'seminar-page-top-text'}).exec();
   	
    //var query = Seminar.find({'status' : 1}).populate('lang_content').populate('speaker_id').sort({ position: 1 }); 
    var query = Seminar.find({'status' : 1,$or: [{'date_time': null }, {'date_time':{$gte: new Date()}}]}).populate('lang_content').populate('speaker_id').sort({ position: 1 }); 
    query.sort({ created_at: -1 });
    query.lean().exec(function (err, result) {    	
    	var response = {};
		response.status ='success';		
		response.languageCode =req.body.languageCode;		
		result.forEach(function(content,index) {
			content.lang_content.forEach(function(content2) {				
				if(content2.lang == req.body.languageCode && content2.description !="")
				{	
					result[index].description = content2.description;										
				}
			});
		});	
		response.data = result;			
		response.seminarTopText =seminarTopText; 		
		res.json(response);      	
	}); 	 	    
};

exports.seminar_view = async function(req, res) {
	
	var seminarTopText = await StaticPage.findOne({'slug':'seminar-page-top-text'}).exec();   
   	
    var query = Seminar.findOne({'slug' : req.params.slug}).populate('lang_content').populate('speaker_id'); 
    query.sort({ created_at: -1 });
    query.lean().exec(function (err, result) {    	
    	var response = {};
		response.status ='success';		
		response.languageCode =req.body.languageCode;	
		result.lang_content.forEach(function(content2,index) {				
			if(content2.lang == req.body.languageCode && content2.description !="")
			{	
				result.description = content2.description;										
			}
		});	
		
		response.data = result;
		response.seminarTopText =seminarTopText;
		res.json(response);      	
	}); 	 	    
};

exports.saveRequestHostForm = async function(req, res) {

	var request = new SeminarRequest();    
    request.seminar_id =req.body.seminar_id;      
	request.name =req.body.name;
	request.email =req.body.email;
	request.phone =req.body.phone;		
	request.address =req.body.address;
	request.message =req.body.message;			
	request.status = 1;				
	request.save(function (err) {
		if(err)
		{
			var response = {};
			response.status ='error';
			response.msg ='Error in saving Request';
            response.error = err;			
			res.json(response);		  		
		}
		else
		{
			var response = {};
			response.status ='success';
			response.msg ='Seminar Host Request saved successfully';			
			res.json(response);	
		} 
	});	
	
};


exports.purchase_by_plan = async function(req, res) {

 OrderProduct.count({user_id: req.body.user_id, webinar_id: req.body.webinar_id }, async function (err, count){ 

    if(count>0){
		var response = {};
		response.status ='error';
		response.msg ='You have already purchased this Webinar.';
        response.error = err;			
		res.json(response);		
    }else{

		var order_total = await Order.count({'created_at':{$gte: moment().startOf('day')}}).exec();  
		var new_order_id=moment().format("YYYYMMDD")+(order_total+1);      		    		
		var order = new Order();		
		order.order_id = new_order_id;
		order.user_id = req.body.user_id;	
		order.subtotal = 0;
		order.discount = 0;
		order.total = 0;
		order.status = 4;		
		order.save(async function (err,newOrder) {		
			if(err)
			{
				var response = {};
				response.status ='error';
				response.msg ='Some internal error occured.';
	            response.error = err;			
				res.json(response);		  		
			}
			else
			{
				var orderproduct = new OrderProduct();		
				orderproduct.order_id = newOrder._id;
				orderproduct.user_id = req.body.user_id;
				orderproduct.webinar_id =  req.body.webinar_id; 
				orderproduct.type = 'webinar';
				orderproduct.datetime = req.body.datetime;
				orderproduct.price = 0;
				orderproduct.qty = 0;
				orderproduct.total = 0;
				await orderproduct.save();
				newOrder.order_product.push(orderproduct);
				await newOrder.save();

				var response = {};
				response.status ='success';
				response.msg ='Webinar purchased successfully.';			
				res.json(response);	
			} 
		}); 
    }
}); 
};






exports.purchase_seminar_by_plan = async function(req, res) {
	 OrderProduct.count({user_id: req.body.user_id, seminar_id: req.body.seminar_id }, async function (err, count){ 
	    if(count>0){
			var response = {};
			response.status ='error';
			response.msg ='You have already purchased this Seminar.';
	        response.error = err;			
			res.json(response);		
	    }else{
			var order_total = await Order.count({'created_at':{$gte: moment().startOf('day')}}).exec();  
			var new_order_id=moment().format("YYYYMMDD")+(order_total+1);      		    		
			var order = new Order();		
			order.order_id = new_order_id;
			order.user_id = req.body.user_id;	
			order.subtotal = 0;
			order.discount = 0;
			order.total = 0;
			order.status = 4;		
			order.save(async function (err,newOrder) {		
				if(err)
				{
					var response = {};
					response.status ='error';
					response.msg ='Some internal error occured.';
		            response.error = err;			
					res.json(response);		  		
				}
				else
				{
					var orderproduct = new OrderProduct();		
					orderproduct.order_id = newOrder._id;
					orderproduct.user_id = req.body.user_id;
					orderproduct.seminar_id =  req.body.seminar_id; 
					orderproduct.type = 'seminar';
					orderproduct.price = 0;
					orderproduct.qty = 0;
					orderproduct.total = 0;
					await orderproduct.save();
					newOrder.order_product.push(orderproduct);
					await newOrder.save();
					var response = {};
					response.status ='success';
					response.msg ='Seminar purchased successfully.';			
					res.json(response);	
				} 
			}); 
	    }
	}); 
};



exports.purchase_webinar_directly = async function(req, res) {

	var order_total = await Order.count({'created_at':{$gte: moment().startOf('day')}}).exec();  
	var new_order_id=moment().format("YYYYMMDD")+(order_total+1);      		    		

	var order = new Order();		
	order.order_id = new_order_id;
	order.user_id = req.body.user.user_id;	
	order.subtotal = req.body.cart.subtotal;
	order.discount = req.body.cart.discount;
	order.discountcode = req.body.cart.discountcode;
	order.total = req.body.cart.total;

	order.status = 1;		
	order.save(async function (err,newOrder) {		



		var webinar_info_mail = []; 
		for (var i = 0; i < req.body.cart.items.length; i++) {
			var item =  req.body.cart.items[i];
			var orderproduct = new OrderProduct();		
			orderproduct.order_id = newOrder._id;
			orderproduct.user_id = req.body.user.user_id;
			
			if(item.type=='product')
				orderproduct.product_id = item.id;			
			else if(item.type=='part')
				orderproduct.part_id = item.id;
			else if(item.type=='webinar')
			{
				orderproduct.webinar_id = item.id;
				orderproduct.datetime = item.datetime;
			}
			else if(item.type=='seminar')
			{
				orderproduct.seminar_id = item.id;
			}			

			orderproduct.type = item.type;
			orderproduct.price = item.price;
			orderproduct.qty = item.qty;
			orderproduct.total = item.total;
			
			await orderproduct.save();
			newOrder.order_product.push(orderproduct);
			await newOrder.save();

			if(req.body.cart.items[i].type == "webinar"){
				// webinar_info_mail = webinar_info_mail ? (webinar_info_mail+', ') : webinar_info_mail ; 
				// webinar_info_mail += req.body.cart.items[i].name ;
				webinar_info_mail[i] =  req.body.cart.items[i].id; 
			}
		}
		var webinar_info_mail_content = ''; 
		if(webinar_info_mail.length > 0){
			await Webinar.find({
							    '_id': { $in: webinar_info_mail}
							}, function(err, docs){						    
							     if(!err){
					     	          docs.forEach((message, index) => {
							              // console.log(message); 
							              webinar_info_mail_content += '<h4>'+index+').Title: '+message.title+'</h4><br />';
							              webinar_info_mail_content += '<h4>&nbsp; Description: </h4><p>'+message.description+'</p><br />';
							              webinar_info_mail_content += '<h4>&nbsp; Objective: </h4><p>'+message.objective+'</p><br />'; 
							              webinar_info_mail_content += '<h4>&nbsp; Topics: </h4><p>'+message.topics+'</p><br />'; 
							              webinar_info_mail_content += '<h4>&nbsp; Pre Requisite: </h4><p>'+message.pre_requisite+'</p><br />'; 
							              webinar_info_mail_content += '<h4>&nbsp; Date: </h4><p>'+moment(message.date_time_1).format('D MMM, YYYY')+'-'+moment(message.date_time_2).format('D MMM, YYYY')+'</p><br />'; 
							              webinar_info_mail_content += '<h4>&nbsp; Contact hour: </h4><p>'+message.contact_hours+'</p><br />'; 
							              webinar_info_mail_content += '<h4>&nbsp; Meeting Link: </h4><p>'+message.url+'</p><br /><hr>'; 
							          });
							     }
				});
		}

		if(webinar_info_mail_content){
			var params =  {};
			params.slug = 'webinar_purchase';	
			params.to = req.body.user.email;				
			params.params = {};	
			params.params.name = req.body.user.name;		
			params.params.details = webinar_info_mail_content;		
			var str = CommonController.sendMail(params);
			// console.log(str); 
			// console.log('hello'); 
		}
		/*
		var query = Order.findOne({'_id' : newOrder._id}).populate('user_id','name email phone').populate('order_address');	
		query.populate({path: 'order_product',model: 'OrderProduct',populate:[{path: 'product_id',model: 'Product',select :'name slug image'},{path: 'part_id',model: 'Part',select :'name slug image'},{path: 'webinar_id',model: 'Webinar',select :'title slug date_time_1 date_time_2'},{path: 'seminar_id',model: 'Seminar',select :'title slug date_time'}] });
		
		query.exec(function (err, result) {					
			var params =  {};
			params.slug = 'order-success';	
			params.to = result.user_id.email;				
			params.params = {};	
			params.params.name = result.user_id.name;		
			params.params.details = order_details(result);					
			CommonController.sendMail(params);
		}); 
		*/ 
		var response = {};
		response.status ='success';
		response.data = req.body;
		return res.json(response);
	});	
	  
};
