var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Tag = new Schema({
    name   : {type: String, required: true},
    status    : {type: Number, required: false},
	},
	{
 		timestamps: { createdAt: 'created_at', updatedAt: 'updated_at'},
 		versionKey: false
	}
);

module.exports = mongoose.model('Tag', Tag);