var mongoose = require('mongoose');
var uniqueValidator = require('mongoose-unique-validator');
var Schema = mongoose.Schema;

var Blog = new Schema({	
	    title   : {type: String, required: true},
	    short_description   : {type: String, required: false},
	    lang_content: [{ type: Schema.Types.ObjectId, ref: 'BlogContent' }],
	    content   : {type: String, required: false},
	    image   : {type: String, required: false},
	    status    : {type: Number, required: false},            
	},
	{
 		timestamps: { createdAt: 'created_at', updatedAt: 'updated_at'},
 		versionKey: false
	}
);

Blog.plugin(uniqueValidator);

module.exports = mongoose.model('Blog', Blog);