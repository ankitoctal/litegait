var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var WebinarContent = new Schema({       
    webinar_id: { type: Schema.Types.ObjectId, ref: 'Webinar' },
    lang    : {type: String, required: true},    
    description    : {type: String, required: false},    
    objective    : {type: String, required: false},    
    topics    : {type: String, required: false},    
	},
	{
 		timestamps: { createdAt: 'created_at', updatedAt: 'updated_at'},
 		versionKey: false
	}
);

module.exports = mongoose.model('WebinarContent', WebinarContent,'webinar_contents');