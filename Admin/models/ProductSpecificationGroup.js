var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ProductSpecificationGroup = new Schema({           
    name    : {type: String, required: true},
    specification_row    : {type: String, required: true},
    },
	{
		
 		timestamps: { createdAt: 'created_at', updatedAt: 'updated_at'},
 		versionKey: false, 		
	}
);

module.exports = mongoose.model('ProductSpecificationGroup', ProductSpecificationGroup,'product_specification_group');