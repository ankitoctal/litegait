var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Menu = new Schema({
	menuname   : {type: String, required: true},
	type   : {type: String, required: true},
    name   : {type: String, required: true},
    parent_id   : {type: String, required: false},
    page_id   : {type: String, required: false},
    category_id   : {type: String, required: false},
    product_id   : {type: String, required: false},
    static_url   : {type: String, required: false},
    weight    : {type: Number, required: false},
    status    : {type: Number, required: false},
	},
	{
 		timestamps: { createdAt: 'created_at', updatedAt: 'updated_at'},
 		versionKey: false
	}
);

Menu.methods.getAllNestedMenu=async function getAllNestedCategory(menuname='',allPages={},allProduct={},allCategory={})
{
	var result = await this.model('Menu').find({status :1,'menuname':menuname }).sort('parent_id weight').lean().exec();
	var menus= {};		
	result.forEach(function(rs) {

		if(rs.type=='page' && Object.keys(allPages).length >= 1)		
			rs.slug = allPages[rs.page_id];	
		else if(rs.type=='category' && Object.keys(allCategory).length >= 1)		
			rs.slug = allCategory[rs.category_id];	
		else if(rs.type=='product' && Object.keys(allProduct).length >= 1)		
			rs.slug = allProduct[rs.product_id];
		else if(rs.type=='static_url')		
			rs.slug = rs.static_url;
		
		if(rs.parent_id == 0)
		{
			menus[rs._id] = rs;	
			menus[rs._id]['childs'] = {};		
		}
		else
		{
			if(menus.hasOwnProperty(rs.parent_id))
			{
				menus[rs.parent_id]['childs'][rs._id] = rs;
				menus[rs.parent_id]['childs'][rs._id]['childs'] = {};
			}
			else
			{				
				for (var i in menus) {
					for (var j in menus[i].childs) {
						if(j == rs.parent_id)		
						{							
							menus[i]['childs'][j]['childs'][rs._id] = rs;
						}
					}				
				}
			}				
			 
		}
	});
	
	return menus;
}

module.exports = mongoose.model('Menu', Menu);