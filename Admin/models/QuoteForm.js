var mongoose = require('mongoose');
var uniqueValidator = require('mongoose-unique-validator');
var Schema = mongoose.Schema;

var QuoteForm = new Schema({
    //product_id: { type: Schema.Types.ObjectId, ref: 'Product' },
    product_id: { type:String, required: true },
    product_type: { type:String, required: false },
    type   : {type: String, required: true},
    title   : {type: String, required: true},     
    slug   : {type: String, required: true,unique: true},
    options   : {type: String, required: false},
    placeholder   : {type: String, required: false},
    required   : {type: Number, required: false},
    position   : {type: Number, required: false},    
    status    : {type: Number, required: false},
	},
	{
 		timestamps: { createdAt: 'created_at', updatedAt: 'updated_at'},
 		versionKey: false
	}
);

QuoteForm.plugin(uniqueValidator);

module.exports = mongoose.model('QuoteForm', QuoteForm,'quote_forms');