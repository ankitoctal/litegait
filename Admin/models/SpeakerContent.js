var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var SpeakerContent = new Schema({       
    speaker_id: { type: Schema.Types.ObjectId, ref: 'Speaker' },
    lang    : {type: String, required: true},    
    bio    : {type: String, required: false},    
	},
	{
 		timestamps: { createdAt: 'created_at', updatedAt: 'updated_at'},
 		versionKey: false
	}
);

module.exports = mongoose.model('SpeakerContent', SpeakerContent,'speaker_contents');