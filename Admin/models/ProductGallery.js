var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ProductGallery = new Schema({       
    product_id: { type: Schema.Types.ObjectId, ref: 'Product' },
    image    : {type: String, required: true},    
	},
	{
 		timestamps: { createdAt: 'created_at', updatedAt: 'updated_at'},
 		versionKey: false
	}
);

module.exports = mongoose.model('ProductGallery', ProductGallery,'product_gallery');