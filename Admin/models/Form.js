var mongoose = require('mongoose');
var uniqueValidator = require('mongoose-unique-validator');
var Schema = mongoose.Schema;

var Form = new Schema({            
    title   : {type: String, required: true},     
    slug   : {type: String, required: true,unique: true},
    body   : {type: String, required: false},
    email   : {type: String, required: false},    
	},
	{
 		timestamps: { createdAt: 'created_at', updatedAt: 'updated_at'},
 		versionKey: false
	}
);

Form.plugin(uniqueValidator);

module.exports = mongoose.model('Form', Form,'forms');