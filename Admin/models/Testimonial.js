var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Testimonial = new Schema({
    name   : {type: String, required: false},
    facility   : {type: String, required: false},
    feedback   : {type: String, required: true},
    image   : {type: String, required: false},
    type   : { type: String,required: false},
    home_page   : { type: Number,required: false},
    status    : {type: Number, required: false},
	},
	{
 		timestamps: { createdAt: 'created_at', updatedAt: 'updated_at'},
 		versionKey: false
	}
);

Testimonial.methods.getAll=async function getAll()
{
	var result = await this.model('Testimonial').find({status :1 }).select('name facility image feedback type').lean().exec();
	var data= {};
	result.forEach(function(rs) {
		//data[rs._id] = rs.name+' - '+ rs.feedback.substring(0,20)+'...';	
		data[rs._id] = rs.name+' - '+ rs.type;	

		if(rs.name =="" && rs.facility !='')
			data[rs._id] = rs.facility+' - '+ rs.type;	
		
	});
	
	return data;
}



module.exports = mongoose.model('Testimonial', Testimonial);