var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Elp = new Schema({
    name   : {type: String, required: true},
    display_name   : {type: String, required: true},
    display_sub_name   : {type: String, required: false},
    price   : {type: Number, required: true},
    display_number   : {type: Number, required: true},
    duration   : {type: Number, required: true},
    persons   : {type: Number, required: false},
    status    : {type: Number, required: false},
	},
	{
 		timestamps: { createdAt: 'created_at', updatedAt: 'updated_at'},
 		versionKey: false
	}
);

module.exports = mongoose.model('Elp', Elp);