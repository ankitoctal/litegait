var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ProductFeatured = new Schema({       
    product_id: { type: Schema.Types.ObjectId, ref: 'Product' },
    description    : {type: String, required: false},    
    left_1_title    : {type: String, required: false},    
    left_1_icon    : {type: String, required: false},    
    left_1_text    : {type: String, required: false},  
    left_2_title    : {type: String, required: false},    
    left_2_icon    : {type: String, required: false},    
    left_2_text    : {type: String, required: false},
    left_3_title    : {type: String, required: false},    
    left_3_icon    : {type: String, required: false},    
    left_3_text    : {type: String, required: false},
    right_1_title    : {type: String, required: false},    
    right_1_icon    : {type: String, required: false},    
    right_1_text    : {type: String, required: false},
    right_2_title    : {type: String, required: false},    
    right_2_icon    : {type: String, required: false},    
    right_2_text    : {type: String, required: false},
    right_3_title    : {type: String, required: false},    
    right_3_icon    : {type: String, required: false},    
    right_3_text    : {type: String, required: false},

	},
	{
 		timestamps: { createdAt: 'created_at', updatedAt: 'updated_at'},
 		versionKey: false
	}
);

module.exports = mongoose.model('ProductFeatured', ProductFeatured,'product_featured');