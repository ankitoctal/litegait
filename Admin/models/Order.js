var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Order = new Schema({	    
    order_id: {type: String, required: true},   
    user_id: { type: Schema.Types.ObjectId, ref: 'User' },   
    subtotal   : {type: Number, required: true},
    discount   : {type: Number, required: false},
    discountcode   : {type: String, required: false},
    total   : {type: Number, required: true},
    payment_method    : {type: String, required: false}, 
    payment_id    : {type: String, required: false},
    ship_method    : {type: String, required: false}, 
    ship_id    : {type: String, required: false},
    status    : {type: Number, required: true},    
   	order_product: [{ type: Schema.Types.ObjectId, ref: 'OrderProduct' }],
   	order_address: { type: Schema.Types.ObjectId, ref: 'OrderAddress' },           
	},
	{
 		timestamps: { createdAt: 'created_at', updatedAt: 'updated_at'},
 		versionKey: false
	}
);

module.exports = mongoose.model('Order', Order);