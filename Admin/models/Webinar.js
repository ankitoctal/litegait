var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Webinar = new Schema({    
    title   : {type: String, required: true},
    slug   : {type: String, required: true,unique: true},  
    category   : { type: Schema.Types.ObjectId, ref: 'Category'},
    speaker_id   : [{ type: Schema.Types.ObjectId, ref: 'Speaker' }],   
    image    : {type: String, required: false},        
   	description    : {type: String, required: false},
   	objective    : {type: String, required: false}, 
   	topics    :    [{type: String}], // {type: String, required: false},
   	lang_content: [{ type: Schema.Types.ObjectId, ref: 'WebinarContent' }],   	   	
   	states    :  [{type: String}], // {type: String, required: false}, 
   	pre_requisite    : {type: String, required: false},  
    state_msg    : {type: String, required: false},  
   	education_level    : {type: String, required: false},  
   	price    : {type: Number, required: false},  
   	date_time_1    : {type: Date, required: false,default:null},
   	date_time_2    : {type: Date, required: false},
   	url    : {type: String, required: false},     	
    recorded_url    : {type: String, required: false},    
   	contact_hours    : {type: String, required: false},
   	testimonials: [{ type: Schema.Types.ObjectId, ref: 'Testimonial' }],  
   	meta_title    : {type: String, required: false},
    meta_description    : {type: String, required: false}, 
    status    : {type: Number, required: false},
	},
	{
 		timestamps: { createdAt: 'created_at', updatedAt: 'updated_at'},
 		versionKey: false
	}
);

module.exports = mongoose.model('Webinar', Webinar);