var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var CaseStudyContent = new Schema({       
    case_study_id: { type: Schema.Types.ObjectId, ref: 'CaseStudy' },
    lang    : {type: String, required: true},    
    description    : {type: String, required: false},
	},
	{
 		timestamps: { createdAt: 'created_at', updatedAt: 'updated_at'},
 		versionKey: false
	}
);

module.exports = mongoose.model('CaseStudyContent', CaseStudyContent,'case_study_contents');