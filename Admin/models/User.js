var mongoose = require('mongoose');
var uniqueValidator = require('mongoose-unique-validator');
var Schema = mongoose.Schema;

var User = new Schema({
    role_id    : {type: Number, required: false},
    name   : {type: String, required: false},
    email    : {type: String, required: true, unique: true,index: true},
    phone    : {type: String, required: false},
    password    : {type: String, required: false,minlength: 6}, 
    address    : {type: String, required: false},
    facility    : {type: String, required: false},
    license_number    : {type: String, required: false},
    organization    : {type: String, required: false},
    zip_code    : {type: String, required: false},
    title    : {type: String, required: false},
    country    : {type: String, required: false},
    state    : {type: String, required: false},
    city    : {type: String, required: false},
    disipline    : {type: String, required: false},  
    elp_id   : { type: Schema.Types.ObjectId, ref: 'Elp',required: false}, 
    elp_payment_id   : { type: Schema.Types.ObjectId, ref: 'ElpPayment',required: false}, 
    elp_expire   : { type: Date, required: false}, 
    elp_purchase   : { type: Date, required: false}, 
    elp_provider_payment_id : { type: Schema.Types.ObjectId, ref: 'ElpPayment',required: false}, 
    // elm_coupon_used :  {type: Number, required: false, default: 0 },
    role_permission    : {type: String, required: false},
    status    : {type: Number, required: false},
	},
	{
 		timestamps: { createdAt: 'created_at', updatedAt: 'updated_at'},
 		versionKey: false
	}
);

User.plugin(uniqueValidator);

module.exports = mongoose.model('User', User);

