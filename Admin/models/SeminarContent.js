var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var SeminarContent = new Schema({       
    seminar_id: { type: Schema.Types.ObjectId, ref: 'Seminar' },
    lang    : {type: String, required: true},    
    description    : {type: String, required: false},
	},
	{
 		timestamps: { createdAt: 'created_at', updatedAt: 'updated_at'},
 		versionKey: false
	}
);

module.exports = mongoose.model('SeminarContent', SeminarContent,'seminar_contents');