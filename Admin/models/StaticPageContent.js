var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var StaticPageContent = new Schema({       
    static_page_id: { type: Schema.Types.ObjectId, ref: 'StaticPage' },
    lang    : {type: String, required: true},
    //title    : {type: String, required: true},
    body    : {type: String, required: false},    
	},
	{
 		timestamps: { createdAt: 'created_at', updatedAt: 'updated_at'},
 		versionKey: false
	}
);

module.exports = mongoose.model('StaticPageContent', StaticPageContent,'static_page_contents');