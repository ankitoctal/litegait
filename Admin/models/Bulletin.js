var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Bulletin = new Schema({
    title   : {type: String, required: true},
    publish_date   : {type: Date, required: true},
    description   : {type: String, required: true},
    image   : {type: String, required: false},    
    htmlfile   : {type: String, required: false},    
    status    : {type: Number, required: false},
	},
	{
 		timestamps: { createdAt: 'created_at', updatedAt: 'updated_at'},
 		versionKey: false
	}
);

module.exports = mongoose.model('Bulletin', Bulletin,'bulletins');