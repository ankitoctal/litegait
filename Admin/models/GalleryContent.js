var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var GalleryContent = new Schema({       
    gallery_id: { type: Schema.Types.ObjectId, ref: 'Gallery' },
    lang    : {type: String, required: true},
    description    : {type: String, required: false},  
	},
	{
 		timestamps: { createdAt: 'created_at', updatedAt: 'updated_at'},
 		versionKey: false
	}
);

module.exports = mongoose.model('GalleryContent', GalleryContent,'gallery_contents');