var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var BlogContent = new Schema({       
    blog_id: { type: Schema.Types.ObjectId, ref: 'Blog' },
    lang    : {type: String, required: true},
    short_description    : {type: String, required: false},
    content    : {type: String, required: false},    
	},
	{
 		timestamps: { createdAt: 'created_at', updatedAt: 'updated_at'},
 		versionKey: false
	}
);

module.exports = mongoose.model('BlogContent', BlogContent,'blog_contents');