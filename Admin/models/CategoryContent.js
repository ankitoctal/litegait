var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var CategoryContent = new Schema({       
    category_id: { type: Schema.Types.ObjectId, ref: 'Category' },
    lang    : {type: String, required: true},
    feature    : {type: String, required: false},
    short_description    : {type: String, required: false},    
	},
	{
 		timestamps: { createdAt: 'created_at', updatedAt: 'updated_at'},
 		versionKey: false
	}
);

module.exports = mongoose.model('CategoryContent', CategoryContent,'category_contents');