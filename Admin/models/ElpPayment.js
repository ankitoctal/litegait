var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ElpPayment = new Schema({
	elp_id: { type: Schema.Types.ObjectId, ref: 'Elp' },
    user_id: { type: Schema.Types.ObjectId, ref: 'User' }, 
	amount   : {type: Number, required: true},
    payment_method    : {type: String, required: false}, 
    payment_id    : {type: String, required: false},
    coupon_used :  {type: Number, required: false, default: 0 },
    facility_name: {type: String, required: false},
    status    : {type: Number, required: false},
	},
	{
 		timestamps: { createdAt: 'created_at', updatedAt: 'updated_at'},
 		versionKey: false
	}
);

module.exports = mongoose.model('ElpPayment', ElpPayment,'elp_payment');