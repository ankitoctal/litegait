var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Email = new Schema({
    slug   : {type: String, required: true},
    subject    : {type: String, required: true},
    body    : {type: String, required: true},    
	},
	{
 		timestamps: { createdAt: 'created_at', updatedAt: 'updated_at'},
 		versionKey: false
	}
);

module.exports = mongoose.model('Email', Email);