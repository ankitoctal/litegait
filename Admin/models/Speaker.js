var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Speaker = new Schema({    
    fname   : {type: String, required: false},
    lname   : {type: String, required: false},
    name   : {type: String, required: true},
    slug   : {type: String, required: true,unique: true},       
    tag_line   : {type: String, required: false},       
    image   : {type: String, required: false},    
   	bio    : {type: String, required: false}, 
   	lang_content: [{ type: Schema.Types.ObjectId, ref: 'SpeakerContent' }],   	   	
   	category    : {type: String, required: false}, 
   	trainers    : {type: String, required: false},  
   	instructors    : {type: String, required: false},  
   	webinars    : {type: String, required: false},
   	testimonials: [{ type: Schema.Types.ObjectId, ref: 'Testimonial' }],  
   	meta_title    : {type: String, required: false},
    meta_description    : {type: String, required: false}, 
    status    : {type: Number, required: false},
	},
	{
 		timestamps: { createdAt: 'created_at', updatedAt: 'updated_at'},
 		versionKey: false
	}
);

module.exports = mongoose.model('Speaker', Speaker);