
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Language = new Schema({
    code   : {type: String, required: true},
    name   : {type: String, required: true},    
    status    : {type: Number, required: false},
	},
	{
 		timestamps: { createdAt: 'created_at', updatedAt: 'updated_at'},
 		versionKey: false
	}
);

Language.methods.getActive = function getActive(callback) {  	
  	return this.model('Language').find({status :1 },callback).select('code name');
}

module.exports = mongoose.model('Language', Language);