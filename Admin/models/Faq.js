var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Faq = new Schema({
	//category   : { type: Schema.Types.ObjectId, ref: 'Category' },
	category   : { type: String, required: false},
    question   : {type: String, required: true},
    lang_content: [{ type: Schema.Types.ObjectId, ref: 'FaqContent' }],  	 
    answer   : {type: String, required: true},
    status    : {type: Number, required: false},
	},
	{
 		timestamps: { createdAt: 'created_at', updatedAt: 'updated_at'},
 		versionKey: false
	}
);

module.exports = mongoose.model('Faq', Faq);