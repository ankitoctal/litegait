var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ProductSpecification = new Schema({           
    model    : {type: String, required: true},
    },
	{
		retainKeyOrder:true,
 		timestamps: { createdAt: 'created_at', updatedAt: 'updated_at'},
 		versionKey: false,
 		strict: false 
	}
);

module.exports = mongoose.model('ProductSpecification', ProductSpecification,'product_specification');