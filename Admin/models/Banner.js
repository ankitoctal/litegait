var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Banner = new Schema({	
    type   : {type: String, required: true},
    title   : {type: String, required: false},
    banner_text_one   : {type: String, required: false},
    banner_text_two   : {type: String, required: false},
    image   : {type: String, required: false},
    status    : {type: Number, required: false},            
    order    : {type: String, required: false},            
	},
	{
 		timestamps: { createdAt: 'created_at', updatedAt: 'updated_at'},
 		versionKey: false
	}
);

module.exports = mongoose.model('Banner', Banner);