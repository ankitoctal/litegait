var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var News = new Schema({
    title   : {type: String, required: true},
    expire_date   : {type: Date, required: true},
    image   : {type: String, required: false},
    short_description   : {type: String, required: true},
    lang_content: [{ type: Schema.Types.ObjectId, ref: 'NewsContent' }],  
    description   : {type: String, required: true},
    status    : {type: Number, required: false},
	},
	{
 		timestamps: { createdAt: 'created_at', updatedAt: 'updated_at'},
 		versionKey: false
	}
);

module.exports = mongoose.model('News', News);