var mongoose = require('mongoose');
var uniqueValidator = require('mongoose-unique-validator');
var Schema = mongoose.Schema;

var Gallery = new Schema({	
		title   : {type: String, required: true},
	    type   : {type: String, required: true},
	    file   : {type: String, required: false},
	    vimeo_url   : {type: String, required: false},
	    tag   : [{type: String, required: false}],
	    description   : {type: String, required: false},
	    position   : {type: Number, required: false},
	    lang_content: [{ type: Schema.Types.ObjectId, ref: 'GalleryContent' }],
	    status    : {type: Number, required: false},            
	},
	{
 		timestamps: { createdAt: 'created_at', updatedAt: 'updated_at'},
 		versionKey: false
	}
);

Gallery.plugin(uniqueValidator);

module.exports = mongoose.model('Gallery', Gallery);