var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Topic = new Schema({    
    topic   : {type: String, required: true},
    slug   : {type: String, required: true,unique: true},  
    status    : {type: Number, required: false},
	},
	{
 		timestamps: { createdAt: 'created_at', updatedAt: 'updated_at'},
 		versionKey: false
	}
);

module.exports = mongoose.model('Topic', Topic);