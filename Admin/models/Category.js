var mongoose = require('mongoose');
var uniqueValidator = require('mongoose-unique-validator');
var Schema = mongoose.Schema;

var Category = new Schema({
	type   : {type: String, required: true},
	slug   : {type: String, required: true,unique: true},
    name   : {type: String, required: true},
    parent_id   : {type: String, required: false},
    image   : {type: String, required: false},
    banner_image   : {type: String, required: false},
    feature    : {type: String, required: false},
    short_description    : {type: String, required: false},
    position   : {type: Number, required: false},
    lang_content: [{ type: Schema.Types.ObjectId, ref: 'CategoryContent' }],
    status    : {type: Number, required: false},
	},
	{
 		timestamps: { createdAt: 'created_at', updatedAt: 'updated_at'},
 		versionKey: false
	}
);


Category.methods.getAllNestedCategory=async function getAllNestedCategory()
{
	var result = await this.model('Category').find({status :1 }).select('code name parent_id').lean().exec();
	var catgories= {};
	result.forEach(function(rs) {
		if(rs.parent_id == 0)
		{
			catgories[rs._id] = rs;	
			catgories[rs._id]['childs'] = {};		
		}
		else
		{
			catgories[rs.parent_id]['childs'][rs._id] = rs; 
		}
	});
	
	return catgories;
}

module.exports = mongoose.model('Category', Category);