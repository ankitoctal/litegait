var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var PartContent = new Schema({       
    part_id: { type: Schema.Types.ObjectId, ref: 'Part' },
    lang    : {type: String, required: true},    
    short_description    : {type: String, required: false},    
    description    : {type: String, required: false},    
	},
	{
 		timestamps: { createdAt: 'created_at', updatedAt: 'updated_at'},
 		versionKey: false
	}
);

module.exports = mongoose.model('PartContent', PartContent,'part_contents');