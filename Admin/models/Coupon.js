var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Coupon = new Schema({
    title   : {type: String, required: true},
    code   : {type: String, required: true},
    type   : {type: String, required: true},
    amount   : {type: Number, required: false},
    percent   : {type: Number, required: false},
    min   : {type: Number, required: false},
    max   : {type: Number, required: false},
    expire   : {type: Date, required: false},
    single_use   : {type: Number, required: false},
    users   : [{type: String, required: false}],
    applied_on    : [{type: String, required: false}],
    category    : [{type: String, required: false}],
    status    : {type: Number, required: false},
	},
	{
 		timestamps: { createdAt: 'created_at', updatedAt: 'updated_at'},
 		versionKey: false
	}
);

module.exports = mongoose.model('Coupon', Coupon);

