var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var OrderAddress = new Schema({       
    order_id: { type: Schema.Types.ObjectId, ref: 'Order' },
    billing_fname    : {type: String, required: true},
    billing_lname    : {type: String, required: false},
    billing_phone    : {type: String, required: false},
    billing_email    : {type: String, required: false},
    billing_add1    : {type: String, required:  true}, 
    billing_add2    : {type: String, required: false},
    billing_city    : {type: String, required:  true},
    billing_state    : {type: String, required:  true},   
    billing_country    : {type: String, required: true},
    billing_zipcode    : {type: String, required: true},
    shipping_fname    : {type: String, required:  function() { return this.shipping_fname != null; }},
    shipping_lname    : {type: String, required: false},
    shipping_phone    : {type: String, required: false},
    shipping_email    : {type: String, required: false},
    shipping_add1    : {type: String, required:  function() { return this.shipping_add1 != null; }}, 
    shipping_add2    : {type: String, required: false},
    shipping_city    : {type: String, required:  function() { return this.shipping_city != null; }},
    shipping_state    : {type: String, required:  function() { return this.shipping_state != null; }},   
    shipping_country    : {type: String, required:  function() { return this.shipping_country != null; }},
    shipping_zipcode    : {type: String, required:  function() { return this.shipping_zipcode != null; }},
	},
	{
 		timestamps: { createdAt: 'created_at', updatedAt: 'updated_at'},
 		versionKey: false
	}
);

module.exports = mongoose.model('OrderAddress', OrderAddress,'order_address');