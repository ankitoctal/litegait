
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Region = new Schema({
    code   : {type: String, required: true},
    name   : {type: String, required: true},
    image   : {type: String, required: false},    
    status    : {type: Number, required: false},
	},
	{
 		timestamps: { createdAt: 'created_at', updatedAt: 'updated_at'},
 		versionKey: false
	}
);

module.exports = mongoose.model('Region', Region);