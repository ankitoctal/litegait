var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Request = new Schema({
    product_id   : { type: Schema.Types.ObjectId, ref: 'Product', required: false},
    part_id   : { type: Schema.Types.ObjectId, ref: 'Part', required: false },
    type   : {type: String, required: false},
    name   : {type: String, required: true},
    email   : {type: String, required: true},
    phone   : {type: String, required: true},
    message   : {type: String, required: false},
    customData   : {type: String, required: false},
    status    : {type: Number, required: false},
	},
	{
 		timestamps: { createdAt: 'created_at', updatedAt: 'updated_at'},
 		versionKey: false
	}
);

module.exports = mongoose.model('Request', Request);