var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var OrderProduct = new Schema({       
    order_id: { type: Schema.Types.ObjectId, ref: 'Order' },    
    user_id: { type: Schema.Types.ObjectId, ref: 'User' },       
    product_id: { type: Schema.Types.ObjectId, ref: 'Product', required: false},  
    part_id: { type: Schema.Types.ObjectId, ref: 'Part', required: false },  
    webinar_id: { type: Schema.Types.ObjectId, ref: 'Webinar', required: false },  
    seminar_id: { type: Schema.Types.ObjectId, ref: 'Seminar', required: false },  
    type    : {type: String, required: false},
    price    : {type: Number, required: true},
    qty    : {type: Number, required: true}, 
    total    : {type: Number, required: true},
    datetime    : {type: String, required: false},
	},
	{
 		timestamps: { createdAt: 'created_at', updatedAt: 'updated_at'},
 		versionKey: false
	}
);

module.exports = mongoose.model('OrderProduct', OrderProduct,'order_products');