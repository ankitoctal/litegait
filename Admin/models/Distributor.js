var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Distributor = new Schema({
	region: { type: String, required: true },
    name   : {type: String, required: true},
    address   : {type: String, required: true},    
    national   : {type: Number, required: false},
    certified   : { type: Number,required: false},
    high_level_product   : { type: Number,required: false},
    status    : {type: Number, required: false},
	},
	{
 		timestamps: { createdAt: 'created_at', updatedAt: 'updated_at'},
 		versionKey: false
	}
);

module.exports = mongoose.model('Distributor', Distributor);