var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var StaticPage = new Schema({
    slug   : {type: String, required: true},
    title    : {type: String, required: true},
    body    : {type: String, required: true},
    lang_content: [{ type: Schema.Types.ObjectId, ref: 'StaticPageContent' }],
    banner_image   : {type: String, required: false},
    page_video   : {type: String, required: false},
    // about_us_bottom_sec    : {type: String, required: false},
    // about_us_contact_info    : {type: String, required: false},
    testimonials: [{ type: Schema.Types.ObjectId, ref: 'Testimonial' }],
    meta_title    : {type: String, required: false},
    meta_description    : {type: String, required: false},    
	},
	{
 		timestamps: { createdAt: 'created_at', updatedAt: 'updated_at'},
 		versionKey: false
	}
);

module.exports = mongoose.model('StaticPage', StaticPage,'static_pages');


