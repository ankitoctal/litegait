var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var NewsContent = new Schema({       
    news_id: { type: Schema.Types.ObjectId, ref: 'News' },
    lang    : {type: String, required: true},
    short_description    : {type: String, required: false},
    description    : {type: String, required: false},    
	},
	{
 		timestamps: { createdAt: 'created_at', updatedAt: 'updated_at'},
 		versionKey: false
	}
);

module.exports = mongoose.model('NewsContent', NewsContent,'news_contents');