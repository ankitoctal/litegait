var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Seminar = new Schema({    
    title   : {type: String, required: true},
    subtitle   : {type: String, required: false},
    slug   : {type: String, required: true,unique: true},                   
    speaker_id   : [{ type: Schema.Types.ObjectId, ref: 'Speaker' }],
    image    : {type: String, required: false},              
   	description    : {type: String, required: false},   	
   	lang_content: [{ type: Schema.Types.ObjectId, ref: 'SeminarContent' }],  	
   	date_time    : {type: Date, required: false},   	
   	location    : {type: String, required: false},  	
   	price    : {type: Number, required: false}, 
   	position   : {type: Number, required: false},
   	testimonials: [{ type: Schema.Types.ObjectId, ref: 'Testimonial' }],  	
   	meta_title    : {type: String, required: false},
    meta_description    : {type: String, required: false}, 
    status    : {type: Number, required: false},
	},
	{
 		timestamps: { createdAt: 'created_at', updatedAt: 'updated_at'},
 		versionKey: false
	}
);

module.exports = mongoose.model('Seminar', Seminar);