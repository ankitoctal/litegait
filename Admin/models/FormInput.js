var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var FormInput = new Schema({
    form_id: { type: Schema.Types.ObjectId, ref: 'Product' },
    type   : {type: String, required: true},
    title   : {type: String, required: true},         
    options   : {type: String, required: false},
    placeholder   : {type: String, required: false},
    required   : {type: Number, required: false},
    position   : {type: Number, required: false},    
    status    : {type: Number, required: false},
	},
	{
 		timestamps: { createdAt: 'created_at', updatedAt: 'updated_at'},
 		versionKey: false
	}
);

module.exports = mongoose.model('FormInput', FormInput,'form_inputs');