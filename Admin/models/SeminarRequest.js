var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var SeminarRequest = new Schema({    
    seminar_id   : { type: Schema.Types.ObjectId, ref: 'Seminar', required: false },    
    name   : {type: String, required: true},
    email   : {type: String, required: true},
    phone   : {type: String, required: true},
    address   : {type: String, required: true},
    message   : {type: String, required: false},    
    status    : {type: Number, required: false},
	},
	{
 		timestamps: { createdAt: 'created_at', updatedAt: 'updated_at'},
 		versionKey: false
	}
);

module.exports = mongoose.model('SeminarRequest',SeminarRequest, 'seminar_request');