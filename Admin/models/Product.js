var mongoose = require('mongoose');
var uniqueValidator = require('mongoose-unique-validator');
var Schema = mongoose.Schema;

var Product = new Schema({
    name: { type: String, required: true },
    slug: { type: String, required: true, unique: true },    
    category: { type: Schema.Types.ObjectId, ref: 'Category' },
    region: { type: String, required: true },
    image: { type: String, required: false },
    short_description: { type: String, required: false },
    description: { type: String, required: false },
    lang_content: [{ type: Schema.Types.ObjectId, ref: 'ProductContent' }],
    image_gallery: [{ type: Schema.Types.ObjectId, ref: 'ProductGallery', required: false }],
    video: { type: String, required: false },
    stock: { type: Number, required: false },
    //tags    : {type: String, required: false}, 
    //featured_product    : {type: Number, required: false},
    price: { type: Number, required: false },
    price_request: { type: Number, required: false },
    featured_product_main: { type: Number, required: false },    
    schedule_service: { type: Number, required: false },    
    specification_related: { type: String, required: false },
    specification_default: { type: String, required: false },
    specification_row: { type: String, required: false },
    specification_row_default: { type: String, required: false },
    position: { type: Number, required: false },
    testimonials: [{ type: Schema.Types.ObjectId, ref: 'Testimonial', required: false }],    
    related_product_text: { type: String, required: false },
    related_product: { type: String, required: false },
    banner_image: { type: String, required: false },
    serial_no: { type: String, required: false },
    service_image: { type: String, required: false },
    service_description: { type: String, required: false },
    meta_title: { type: String, required: false },
    meta_description: { type: String, required: false },
    status: { type: Number, required: false },
}, {
    timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' },
    versionKey: false
});

Product.plugin(uniqueValidator);

module.exports = mongoose.model('Product', Product);