var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ProductSerialno = new Schema({           
    name    : {type: String, required: true},
    serialno    : {type: String, required: true},
    },
	{
		
 		//timestamps: { createdAt: 'created_at', updatedAt: 'updated_at'},
 		versionKey: false, 		
	}
);

module.exports = mongoose.model('ProductSerialno', ProductSerialno,'product_serialno');