var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var State = new Schema({
    name   : {type: String, required: true},
    default_message : {type: String, required: false},
    status    : {type: Number, required: false},
	},
	{
 		timestamps: { createdAt: 'created_at', updatedAt: 'updated_at'},
 		versionKey: false
	}
);

module.exports = mongoose.model('State', State);