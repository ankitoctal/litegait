var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Part = new Schema({
    type   : { type: String,required: false}, 
    product_id   : [{ type: Schema.Types.ObjectId, ref: 'Product',required: false}], 
    category   : { type: Schema.Types.ObjectId, ref: 'Category',required: false},    
    name   : {type: String, required: true},
    slug   : {type: String, required: true,unique: true},   
    sno   : {type: String, required: false},   
    image   : {type: String, required: false},
    image_gallery: [{ type: Schema.Types.ObjectId, ref: 'PartGallery', required: false }], 
    video: { type: String, required: false },
    banner_image: { type: String, required: false },   
    short_description    : {type: String, required: false}, 
   	description    : {type: String, required: false}, 
   	lang_content: [{ type: Schema.Types.ObjectId, ref: 'PartContent' }],   	   	
   	price    : {type: Number, required: false}, 
   	price_request    : {type: Number, required: false},  
   	schedule_service    : {type: Number, required: false},
   	position   : {type: Number, required: false},
   	testimonials: [{ type: Schema.Types.ObjectId, ref: 'Testimonial' }],  
   	meta_title    : {type: String, required: false},
    meta_description    : {type: String, required: false}, 
    status    : {type: Number, required: false},
    related_product  : [{type: String, required: false}],    
	},
	{
 		timestamps: { createdAt: 'created_at', updatedAt: 'updated_at'},
 		versionKey: false
	}
);

module.exports = mongoose.model('Part', Part);