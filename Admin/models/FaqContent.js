var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var FaqContent = new Schema({       
    faq_id: { type: Schema.Types.ObjectId, ref: 'Faq' },
    lang    : {type: String, required: true},
    question    : {type: String, required: false},
    answer    : {type: String, required: false},    
	},
	{
 		timestamps: { createdAt: 'created_at', updatedAt: 'updated_at'},
 		versionKey: false
	}
);

module.exports = mongoose.model('FaqContent', FaqContent,'faq_contents');