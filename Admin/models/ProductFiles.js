var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ProductFiles = new Schema({       
    product_id: { type: Schema.Types.ObjectId, ref: 'Product' },
    user_manual    : {type: String, required: false},    
    maintenance_procedures    : {type: String, required: false},    
    product_datasheet_name    : [{type: String, required: false}],    
    product_datasheet    : [{type: String, required: false}],    
    accessory_manuals    : {type: String, required: false},    
    harness_instructions    : {type: String, required: false},    
    assembly_installation    : {type: String, required: false},    
    treadmill_manuals    : {type: String, required: false},    
    treadmill_maintenance    : {type: String, required: false},    
    brochures    : {type: String, required: false},    
    gaitsens    : {type: String, required: false},    
	},
	{
 		timestamps: { createdAt: 'created_at', updatedAt: 'updated_at'},
 		versionKey: false
	}
);

module.exports = mongoose.model('ProductFiles', ProductFiles,'product_files');