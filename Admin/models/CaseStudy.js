var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var CaseStudy = new Schema({    
    title   : {type: String, required: true},
    slug   : {type: String, required: true,unique: true},      
    synopsis   : {type: String, required: true},         
   	description    : {type: String, required: false},   	
   	lang_content: [{ type: Schema.Types.ObjectId, ref: 'CaseStudyContent' }],  	   	
   	meta_title    : {type: String, required: false},
    meta_description    : {type: String, required: false}, 
    status    : {type: Number, required: false},
	},
	{
 		timestamps: { createdAt: 'created_at', updatedAt: 'updated_at'},
 		versionKey: false
	}
);

module.exports = mongoose.model('CaseStudy', CaseStudy,'case_study');