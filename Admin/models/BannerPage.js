var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var BannerPage = new Schema({	
    name   : {type: String, required: true},
    image   : {type: String, required: true},
	},
	{
 		timestamps: { createdAt: 'created_at', updatedAt: 'updated_at'},
 		versionKey: false
	}
);

module.exports = mongoose.model('BannerPage', BannerPage,'banner_page');