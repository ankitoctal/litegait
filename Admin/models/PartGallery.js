var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var PartGallery = new Schema({       
    part_id: { type: Schema.Types.ObjectId, ref: 'Part' },
    image    : {type: String, required: true},    
	},
	{
 		timestamps: { createdAt: 'created_at', updatedAt: 'updated_at'},
 		versionKey: false
	}
);

module.exports = mongoose.model('PartGallery', PartGallery,'part_gallery');