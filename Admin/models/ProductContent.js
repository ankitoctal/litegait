var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ProductContent = new Schema({       
    product_id: { type: Schema.Types.ObjectId, ref: 'Product' },
    lang    : {type: String, required: true},
    short_description    : {type: String, required: false},
    description    : {type: String, required: false},    
	},
	{
 		timestamps: { createdAt: 'created_at', updatedAt: 'updated_at'},
 		versionKey: false
	}
);

module.exports = mongoose.model('ProductContent', ProductContent,'product_contents');