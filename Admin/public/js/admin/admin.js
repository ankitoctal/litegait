$(document).ready(function() {

    setTimeout(function() { $('div.alert').hide(); }, 5000);

    $('.table-paging').DataTable({
        "pagingType": "full_numbers",
        "pageLength": 25
    });

    $(".deleteBtn").click(function() {
        var r = confirm("Are you sure you want to delete this record?");
        if (r == true) {
            return true;
        } else {
            return false;
        }
    });

    $("#title").blur(function() {
        var val = $(this).val();
        val = val.toLowerCase();
        val = val.replace(/ /g, "-");
        val = val.replace(/[^A-Za-z0-9^_\-]/g, "");

        $('#slug').val(val);

    });

    $(".datepicker").datepicker();

    $('.datetimepicker').appendDtpicker({ "autodateOnStart": false });


    $('.select2').select2({
        placeholder: "Select Values",
        multiple:true
        //allowClear: true
    });
    $('.select2').on("select2:select", function (evt) {
      var element = evt.params.data.element;
      var $element = $(element);
      
      $element.detach();
      $(this).append($element);
      $(this).trigger("change");
    });

    // start filemanager open
    $("body").on("click", ".openfm", function() {
        var obj = $(this).attr('data-input');
        var oWindow = window.open('/Filemanager', "Filemanager", 'left=100,top=100,width=1000,height=700');
        window.SetUrl = function(res) {
            //res.Path = res.Path.replace("/gallery/","");
            //console.log(res);
            if (typeof res === 'object') {
                var path = res.Path;
                path = path.replace(document.location.origin + "/gallery/", "");
                $('#' + obj).val(path);
            } else {
                var path = res;
                path = path.replace(document.location.origin + "/gallery/", "");
                $('#' + obj).val(path);
            }
        };
        return false;
    });
    // end filemanager open

    //Start gallery image add-more
    var add_more = 11;
    $("body").on("click", ".add-more", function() {
        add_more++;
        var html = '<div class="input-group" style="margin-top:5px;"><span class="input-group-btn"><a data-input="gallery_image_' + add_more + '" class="btn btn-primary openfm"><i class="fa fa-picture-o"></i> Choose</a></span><input class="form-control" id="gallery_image_' + add_more + '" type="text" name="gallery_image" value="" readonly=""><span class="input-group-btn remove_add_more"><a data-input="image" class="btn btn-danger"><i class="fa fa-minus"></i></a></span></div>';
        $(".add-more-div").append(html);
    });

    $("body").on("click", ".remove_add_more", function() {
        $(this).parent().remove()
    });

    $("body").on("click", ".add-more-files", function() {
        add_more++;
        var html = '<div class="input-group" style="margin-top:5px;width:100%"><input class="form-control" id="gallery_image_name" type="text" name="gallery_image_name" value="" style="width:24%;margin-right:5px"><div style="width:75%;display:inherit"><span class="input-group-btn"><a data-input="gallery_image_' + add_more + '" class="btn btn-primary openfm"><i class="fa fa-picture-o"></i> Choose</a></span><input class="form-control" id="gallery_image_' + add_more + '" type="text" name="gallery_image" value="" readonly=""><span class="input-group-btn remove_add_more_files"><a data-input="image" class="btn btn-danger"><i class="fa fa-minus"></i></a></span></div></div>';
        $(".add-more-div").append(html);
    });

    $("body").on("click", ".remove_add_more_files", function() {
        $(this).closest('.input-group').remove()
    });
    //End gallery image add-more

});


function getUrlVars() {
    var vars = [],
        hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for (var i = 0; i < hashes.length; i++) {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}