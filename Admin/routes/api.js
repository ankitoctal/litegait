var express = require('express');
var router = express.Router();

var CommonController=require('../controllers/api/CommonController');
var UsersController=require('../controllers/api/UsersController');
var CategoryController=require('../controllers/api/CategoryController');
var ProductsController=require('../controllers/api/ProductsController');
var PartsController=require('../controllers/api/PartsController');
var OrdersController=require('../controllers/api/OrdersController');
var WebinarsController=require('../controllers/api/WebinarsController');  
var ServicesController=require('../controllers/api/ServicesController');  

/* GET home page. */
router.all('/common/page/:slug', CommonController.static_pages);
router.get('/testmail', CommonController.testmail);
router.get('/common/menu_list/:menutype', CommonController.menu_list);
router.get('/common/language_list', CommonController.language_list);
router.get('/common/region_list', CommonController.region_list);
router.all('/common/banner_list', CommonController.banner_list);
router.all('/common/banner_page', CommonController.banner_page);
router.all('/common/testimonail_list', CommonController.testimonail_list);
router.all('/common/latest_news', CommonController.latest_news);
router.all('/common/newsletter_subscribe', CommonController.newsletter_subscribe);
router.all('/common/news_list', CommonController.news_list);
router.all('/common/news_view/:id', CommonController.news_view);
router.all('/common/faq_list', CommonController.faq_list);
router.all('/common/faq_category_list', CommonController.faq_category_list);
router.all('/common/faq_view/:id', CommonController.faq_view);
router.all('/common/gallery_list', CommonController.gallery_list);
router.all('/common/gallery_view/:id', CommonController.gallery_view);
router.all('/common/blog_list', CommonController.blog_list);
router.all('/common/blog_view/:id', CommonController.blog_view);
router.all('/common/elp_list', CommonController.elp_list);
router.all('/common/elp_view/:id', CommonController.elp_view);
router.all('/common/elp_payment', CommonController.elp_payment);
router.all('/common/elp_save_by_coupon', CommonController.elp_save_by_coupon);
router.all('/common/page_testimonials', CommonController.page_testimonials);
router.all('/common/send_contact_email', CommonController.send_contact_email);
router.all('/common/get_tags_list', CommonController.get_tags_list);
router.all('/common/distributor_list', CommonController.get_distributor_list);
router.all('/common/bulletin_list', CommonController.get_bulletin_list);


// users routes
router.get('/users/check_email/:email', UsersController.check_email);
router.all('/users/add', UsersController.add);
router.all('/users/send_forgot_password_mail', UsersController.sendForgotPasswordMail);
router.all('/users/reset_user_password', UsersController.resetUserPassword);
router.all('/users/add_guest', UsersController.addGuest);
router.all('/users/login', UsersController.login);
router.get('/users/getuser/:user_id', UsersController.getUser);
router.all('/users/saveuser', UsersController.saveUser);
router.all('/users/changepassword', UsersController.changePassword);
router.get('/users/orders/:user_id', UsersController.orderList);
router.get('/users/order/:order_id', UsersController.orderView);
router.get('/users/webinars/:user_id', UsersController.webinarList);
router.get('/users/seminars/:user_id', UsersController.seminarList);
router.get('/users/get_user_plan/:user_id', UsersController.getUserPlan);

// category routes
router.all('/category/list/:type', CategoryController.list);

// products routes
router.get('/products/home_page_featured_list', ProductsController.home_page_featured_list);
router.all('/products/list', ProductsController.list);
router.all('/product/view', ProductsController.view);
router.all('/product/getquoteform', ProductsController.getQuoteForm);
router.all('/product/savequoteform', ProductsController.saveQuoteForm);
router.all('/product/saveserviceform', ProductsController.saveServiceForm);
router.all('/product/searchbyserialno', ProductsController.searchBySerialNo);

// parts routes
router.all('/parts/list', PartsController.list);
router.all('/parts/view', PartsController.view);

// order routes
router.all('/order/save', OrdersController.save);
router.all('/order/mail/:order_id', OrdersController.mail);
router.all('/order/check_coupon_code', OrdersController.checkCouponCode);

// webinar routes
router.all('/webinar/upcoming_list', WebinarsController.upcoming_list);

// instructor routes
router.all('/webinar/instructor_list', WebinarsController.instructor_list);
router.all('/webinar/instructor_view/:slug', WebinarsController.instructors_view);
router.all('/webinar/webinar_list', WebinarsController.webinar_list);
router.all('/webinar/webinar_view/:slug', WebinarsController.webinar_view);
router.all('/webinar/seminar_list', WebinarsController.seminar_list);
router.all('/webinar/seminar_view/:slug', WebinarsController.seminar_view);
router.all('/webinar/saverequesthostform', WebinarsController.saveRequestHostForm);
router.all('/webinar/purchase_by_plan', WebinarsController.purchase_by_plan);
router.all('/webinar/purchase_seminar_by_plan', WebinarsController.purchase_seminar_by_plan);
router.all('/webinar/purchase_webinar_directly', WebinarsController.purchase_webinar_directly);

// service section
router.all('/service/document_list', ServicesController.document_list);
router.all('/service/get_service_banner', ServicesController.get_service_banner);


module.exports = router;
