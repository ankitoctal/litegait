var express = require('express');
var router = express.Router();

router.use(async function(req,res,next){
	
	/*if(req.url !='/login' && req.session.user == undefined && req.hostname !='localhost')
	{
		req.flash('success', 'Please Login');			
    	return res.redirect('/admin/login');
	} 	 
   	*/
   	res.locals.site_name = 'Lite Gait';
	var Language = include("models/Language.js");
	var language = new Language();			
	await language.getActive(function(err, result) {  		
		res.locals.activeLanguage = result;		
  	});

	var arr =  req.url.split('/');	
	req.session.controller = arr[1];
	req.session.action = arr[2];
	req.session.reqUrl = req.url;
   	res.locals.session = req.session; 

   	var User = include("models/User.js");
	var user = new User();
	var role_permission='';			
	await User.findOne({'role_id' : '1'},function(err, result) { 		 		
		role_permission = result.role_permission;			
	});

	global.checkPermission = function(manager='') {		
		if(req.session.user == undefined || req.session.user.role_id==1)
		{
			return true;
		} 	
		if(role_permission.indexOf(manager)!= -1)
			return true;
		else
			return false;	 	
	} 	

   	next();
});

var HomeController=require('../controllers/admin/HomeController');
var UsersController=require('../controllers/admin/UsersController'); 
var EmailsController=require('../controllers/admin/EmailsController');
var StaticPagesController=require('../controllers/admin/StaticPagesController');
var StatesController=require('../controllers/admin/StatesController'); 
var LanguagesController=require('../controllers/admin/LanguagesController');
var RegionsController=require('../controllers/admin/RegionsController');
var SubscribersController=require('../controllers/admin/SubscribersController');
var MenusController=require('../controllers/admin/MenusController');
var CategoriesController=require('../controllers/admin/CategoriesController');
var TagsController=require('../controllers/admin/TagsController');
var FaqsController=require('../controllers/admin/FaqsController');
var NewsController=require('../controllers/admin/NewsController');
var BlogsController=require('../controllers/admin/BlogsController');
var GalleryController=require('../controllers/admin/GalleryController');
var TestimonialsController=require('../controllers/admin/TestimonialsController');
var ProductsController=require('../controllers/admin/ProductsController');
var QuoteFormsController=require('../controllers/admin/QuoteFormsController');
var PartsController=require('../controllers/admin/PartsController');
var RequestsController=require('../controllers/admin/RequestsController');
var ServicesController=require('../controllers/admin/ServicesController');
var CouponsController=require('../controllers/admin/CouponsController'); 
var SpeakersController=require('../controllers/admin/SpeakersController');
var WebinarsController=require('../controllers/admin/WebinarsController');
var TopicsController=require('../controllers/admin/TopicsController');
var SeminarsController=require('../controllers/admin/SeminarsController');
var CaseStudyController=require('../controllers/admin/CaseStudyController'); 
var BannersController=require('../controllers/admin/BannersController');
var OrdersController=require('../controllers/admin/OrdersController');
var ElpsController=require('../controllers/admin/ElpsController');
var SeminarHostRequestController=require('../controllers/admin/SeminarHostRequestController');
var FormsController=require('../controllers/admin/FormsController');
var DistributorsController=require('../controllers/admin/DistributorsController');
var BulletinsController=require('../controllers/admin/BulletinsController');

// Ajax Controller //
var AjaxesController = require('../controllers/admin/AjaxesController');

router.get('/', HomeController.dashboard);
router.all('/login', HomeController.login);
router.get('/logout', HomeController.logout);

// users routes
router.get('/users/list', UsersController.list);
router.all('/users/add', UsersController.add);
router.all('/users/export_in_excel', UsersController.export_in_excel);
router.all('/users/view/:user_id', UsersController.view);
router.all('/users/edit/:user_id', UsersController.edit);
router.get('/users/delete/:user_id', UsersController.delete);
router.all('/users/role_permission', UsersController.role_permission);
router.all('/users/import', UsersController.import);

// emails routes
router.get('/emails/list', EmailsController.list);
router.all('/emails/add', EmailsController.add);
router.all('/emails/edit/:id', EmailsController.edit);
router.get('/emails/delete/:id', EmailsController.delete);

// static pages routes
router.get('/staticpages/list', StaticPagesController.list);
router.all('/staticpages/add', StaticPagesController.add);
router.all('/staticpages/edit/:id', StaticPagesController.edit);
router.get('/staticpages/delete/:id', StaticPagesController.delete);
router.get('/staticpages/view/:id', StaticPagesController.view);

// states routes
router.get('/states/list', StatesController.list);
router.all('/states/add', StatesController.add);
router.all('/states/edit/:id', StatesController.edit);
router.get('/states/delete/:id', StatesController.delete);
router.get('/states/status/:id/:status', StatesController.status);

// languages routes
router.get('/languages/list', LanguagesController.list);
router.all('/languages/add', LanguagesController.add);
router.all('/languages/edit/:id', LanguagesController.edit);
router.get('/languages/delete/:id', LanguagesController.delete);
router.get('/languages/status/:id/:status', LanguagesController.status);

// regions routes
router.get('/regions/list', RegionsController.list);
router.all('/regions/add', RegionsController.add);
router.all('/regions/edit/:id', RegionsController.edit);
router.get('/regions/delete/:id', RegionsController.delete);
router.get('/regions/status/:id/:status', RegionsController.status);

// subscribers routes
router.get('/subscribers/list', SubscribersController.list);
router.get('/subscribers/delete/:id', SubscribersController.delete);

// menus routes
router.get('/menu/list/:menuname', MenusController.list);
router.all('/menu/add/:menuname', MenusController.add);
router.all('/menu/edit/:id', MenusController.edit);
router.get('/menu/delete/:id', MenusController.delete);
router.all('/menu/sorting', MenusController.sorting);

// category routes
router.get('/categories/list', CategoriesController.list);
router.all('/categories/add', CategoriesController.add);
router.all('/categories/edit/:id', CategoriesController.edit);
router.get('/categories/delete/:id', CategoriesController.delete);
router.get('/categories/status/:id/:status', CategoriesController.status);

// tag routes
router.get('/tags/list', TagsController.list);
router.all('/tags/export_in_excel', TagsController.export_in_excel);
router.all('/tags/add', TagsController.add);
router.all('/tags/edit/:id', TagsController.edit);
router.get('/tags/delete/:id', TagsController.delete);
router.get('/tags/status/:id/:status', TagsController.status);

// faq routes
router.get('/faqs/list', FaqsController.list);
router.all('/faqs/add', FaqsController.add);
router.all('/faqs/edit/:id', FaqsController.edit);
router.get('/faqs/delete/:id', FaqsController.delete);
router.get('/faqs/status/:id/:status', FaqsController.status);

// news routes
router.get('/news/list', NewsController.list);
router.all('/news/add', NewsController.add);
router.all('/news/edit/:id', NewsController.edit);
router.get('/news/delete/:id', NewsController.delete);
router.get('/news/status/:id/:status', NewsController.status);

// blog routes
router.get('/blogs/list', BlogsController.list);
router.all('/blogs/add', BlogsController.add);
router.all('/blogs/edit/:id', BlogsController.edit);
router.get('/blogs/delete/:id', BlogsController.delete);
router.get('/blogs/status/:id/:status', BlogsController.status);

// Gallery routes
router.get('/gallery/list', GalleryController.list);
router.all('/gallery/add', GalleryController.add);
router.all('/gallery/edit/:id', GalleryController.edit);
router.get('/gallery/delete/:id', GalleryController.delete);
router.get('/gallery/status/:id/:status', GalleryController.status);
router.get('/gallery/share-gallery-url', GalleryController.shareGalleryUrl);

// testimonial routes
router.get('/testimonials/list', TestimonialsController.list);
router.all('/testimonials/add', TestimonialsController.add);
router.all('/testimonials/edit/:id', TestimonialsController.edit);
router.get('/testimonials/delete/:id', TestimonialsController.delete);
router.get('/testimonials/status/:id/:status', TestimonialsController.status);

// products routes
router.get('/products/list', ProductsController.list);
router.all('/products/add', ProductsController.add);
router.all('/products/edit/:id', ProductsController.edit);
router.all('/products/files/:id', ProductsController.files);
router.get('/products/delete/:id', ProductsController.delete);
router.get('/products/view/:id', ProductsController.view);
router.all('/products/copy/:id', ProductsController.copy);
router.get('/products/status/:id/:status', ProductsController.status);
router.post('/products/gallery_delete/:id', ProductsController.gallery_delete);
router.all('/products/featured_product/:id', ProductsController.featured_product);
router.all('/products/specification_list', ProductsController.specification_list);
router.all('/products/specification_delete/:id', ProductsController.specification_delete);
router.all('/products/specification_view/:id', ProductsController.specification_view);
router.all('/products/specification_upload', ProductsController.specification_upload);
router.all('/products/serialno_upload', ProductsController.serialno_upload);

router.all('/products/specification_group_list', ProductsController.specification_group_list);
router.all('/products/specification_group_add', ProductsController.specification_group_add);
router.all('/products/specification_group_edit/:id', ProductsController.specification_group_edit);
router.all('/products/specification_group_delete/:id', ProductsController.specification_group_delete);

//Banner Routes
router.get('/banners/list', BannersController.list);
router.all('/banners/add', BannersController.add);
router.all('/banners/edit/:id', BannersController.edit);
router.get('/banners/delete/:id', BannersController.delete);
router.get('/banners/view/:id', BannersController.view);
router.get('/banners/status/:id/:status', BannersController.status);
router.all('/banners/page', BannersController.page);

// products quote form routes
router.get('/quote_form/view/:id/:type', QuoteFormsController.view);
router.all('/quote_form/add/:id/:type', QuoteFormsController.add);
router.all('/quote_form/edit/:id/:type', QuoteFormsController.edit);
router.get('/quote_form/delete/:id/:fid/:type', QuoteFormsController.delete);

// parts routes
router.get('/parts/list', PartsController.list);
router.all('/parts/add', PartsController.add);
router.all('/parts/edit/:id', PartsController.edit);
router.get('/parts/delete/:id', PartsController.delete);
router.all('/parts/copy/:id', PartsController.copy);
router.get('/parts/status/:id/:status', PartsController.status); 
router.post('/parts/gallery_delete/:id', PartsController.gallery_delete);

// price request routes
router.get('/requests/list', RequestsController.list);
router.get('/requests/delete/:id', RequestsController.delete);
router.get('/requests/view/:id/', RequestsController.view);
router.get('/requests/status/:id/:status', RequestsController.status);

// service request routes
router.get('/services/list', ServicesController.list);
router.get('/services/delete/:id', ServicesController.delete);
router.get('/services/view/:id/', ServicesController.view);
router.get('/services/status/:id/:status', ServicesController.status); 

// seminar host request routes
router.get('/seminar_host_request/list', SeminarHostRequestController.list);
router.get('/seminar_host_request/delete/:id', SeminarHostRequestController.delete);
router.get('/seminar_host_request/view/:id/', SeminarHostRequestController.view);
router.get('/seminar_host_request/status/:id/:status', SeminarHostRequestController.status);

// coupons routes
router.get('/coupons/list', CouponsController.list);
router.all('/coupons/add', CouponsController.add);
router.all('/coupons/edit/:id', CouponsController.edit);
router.get('/coupons/delete/:id', CouponsController.delete);
router.get('/coupons/status/:id/:status', CouponsController.status);

// Speaker routes
router.get('/speakers/list', SpeakersController.list);
router.all('/speakers/add', SpeakersController.add);
router.all('/speakers/edit/:id', SpeakersController.edit);
router.get('/speakers/delete/:id', SpeakersController.delete);
router.get('/speakers/status/:id/:status', SpeakersController.status);

// Webinar routes
router.get('/webinars/list', WebinarsController.list);
router.all('/webinars/add', WebinarsController.add);
router.all('/webinars/edit/:id', WebinarsController.edit);
router.get('/webinars/delete/:id', WebinarsController.delete);
router.get('/webinars/status/:id/:status', WebinarsController.status);
router.get('/webinars/attendies/:id', WebinarsController.attendies);
router.get('/webinars/attendies_export/:id', WebinarsController.attendiesExport);

// Topics routes
router.get('/topics/list', TopicsController.list);
router.all('/topics/add', TopicsController.add);
router.all('/topics/edit/:id', TopicsController.edit);
router.get('/topics/delete/:id', TopicsController.delete);
router.get('/topics/status/:id/:status', TopicsController.status);

// Seminar routes
router.get('/seminars/list', SeminarsController.list);
router.all('/seminars/add', SeminarsController.add);
router.all('/seminars/edit/:id', SeminarsController.edit);
router.get('/seminars/delete/:id', SeminarsController.delete);
router.get('/seminars/status/:id/:status', SeminarsController.status);
router.get('/seminars/copy/:id', SeminarsController.copy);

// Case Study routes
router.get('/casestudy/list', CaseStudyController.list);
router.all('/casestudy/add', CaseStudyController.add);
router.all('/casestudy/edit/:id', CaseStudyController.edit);
router.get('/casestudy/delete/:id', CaseStudyController.delete);
router.get('/casestudy/status/:id/:status', CaseStudyController.status);

// Orders routes
router.get('/order/list', OrdersController.list);
router.all('/order/export_in_excel', OrdersController.export_in_excel);
router.all('/order/view/:id', OrdersController.view);
router.all('/order/status-change', OrdersController.statusChange);
router.all('/order/mail/:id', OrdersController.mail);
router.all('/order/packing_slip/:id', OrdersController.packingSlip);
router.all('/order/edit_address/:id', OrdersController.editAddress);
router.all('/order/edit_shipment/:id', OrdersController.editShipment);

// Elps routes
router.all('/elp/add', ElpsController.add);
router.get('/elp/list', ElpsController.list);
router.all('/elp/edit/:id', ElpsController.edit);
router.all('/elp/view_coupon_user/:id', ElpsController.view_coupon_user);
router.all('/elp/delete/:id', ElpsController.delete);
router.get('/elp/status/:id/:status', ElpsController.status);
router.get('/elp/payments', ElpsController.payments);

// Form routes
router.all('/forms/add', FormsController.add);
router.get('/forms/list', FormsController.list);
router.all('/forms/edit/:id', FormsController.edit);
router.get('/forms/view/:id/', FormsController.view);
router.all('/forms/input_add/:id', FormsController.input_add);
router.all('/forms/input_edit/:id', FormsController.input_edit);
router.get('/forms/input_delete/:id/:fid', FormsController.input_delete);

// Distributors routes
router.get('/distributors/list', DistributorsController.list);
router.all('/distributors/add', DistributorsController.add);
router.all('/distributors/edit/:id', DistributorsController.edit);
router.get('/distributors/delete/:id', DistributorsController.delete);
router.get('/distributors/status/:id/:status', DistributorsController.status);

// Bulletin routes
router.get('/bulletin/list', BulletinsController.list);
router.all('/bulletin/add', BulletinsController.add);
router.all('/bulletin/edit/:id', BulletinsController.edit);
router.get('/bulletin/delete/:id', BulletinsController.delete);
router.get('/bulletin/status/:id/:status', BulletinsController.status);

// Ajax Routes
router.all('/ajax/statuschange/', AjaxesController.statusChange);
router.all('/ajax/deleterecord/', AjaxesController.deleterecord);

module.exports = router; 