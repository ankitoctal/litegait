const express = require('express');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const path = require('path');
const flash = require("express-flash-messages");
const session = require('express-session');
const FileStore = require('session-file-store')(session);
const expressValidator = require('express-validator');
const fileUpload = require('express-fileupload');
const cors = require('cors');

var app = express();
app.locals.moment = require('moment-timezone');
/*filemanager section start here*/
var fs = require('fs');
var multer = require('multer');
var filemanager = require('./filemanager');
var util = require('util');
var config = JSON.parse(
    fs.readFileSync('./filemanager/example/public/Filemanager/scripts/filemanager.config.js')
);
config.connector = {
    // set path to web-server root
    serverRoot: './filemanager/example/public',
    // path to Filemanager sources under server root
    fmSrcPath: '/Filemanager',
    // uploader should be a preconfigured instance of multer
    uploader: multer({ dest: 'public/uploads' })
};
config.options.fileRoot = require('path').join(__dirname, 'public/gallery');
var logger = {
    debug: util.log,
    info: util.log,
    error: util.log
};
var fm = filemanager(config, logger);
app.use('/fm', fm);
app.use(express.static('./filemanager/example/public'));
/*filemanager section finish here*/
app.use(cors());
app.use(flash());
//app.use(expressValidator());
app.use(expressValidator({
    customValidators: {
        isImage: function(value, filename) {

            var extension = (path.extname(filename)).toLowerCase();
            switch (extension) {
                case '.jpg':
                    return '.jpg';
                case '.jpeg':
                    return '.jpeg';
                case '.png':
                    return '.png';
                default:
                    return false;
            }
        },
        isExcel: function(value, filename) {

            var extension = (path.extname(filename)).toLowerCase();
            switch (extension) {
                case '.xls':
                    return '.xls';
                case '.xlsx':
                    return '.xlsx';
                default:
                    return false;
            }
        }
    }
}));


app.use(fileUpload());
app.use(cookieParser());
var session_options = { dir: './sessions' };
app.use(session({
    //store: new FileStore,
    maxAge: 60000,
    secret: 'keyboard',
    resave: false,
    saveUninitialized: true,
}));

// view engine setup
app.engine('pug', require('pug').__express)
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(bodyParser.json());
//app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.urlencoded({ extended: true }));
app.use(express.static(path.join(__dirname, 'public')));

global.base_dir = __dirname;
global.abs_path = function(path) {
    return base_dir + path;
}

app.locals.basedir = path.join(__dirname);

/*app.use(async function(req,res,next){         
    next();
}); */

require('./configs/db.js');
require('./libs/common');

/* -------------- setting the routes ------------------- */
var frontRouter = require('./routes/front');
var adminRouter = require('./routes/admin');
var apiRouter = require('./routes/api');
app.use('/', frontRouter);
app.use('/admin', adminRouter);
app.use('/api', apiRouter);

app.listen(5003,'localhost'); // for localhost
//app.listen(5003); // for server

var https = require('https');
var fs = require('fs');
/*
https.createServer({
        key: fs.readFileSync('key.txt'),
        cert: fs.readFileSync('cert.txt')
    }, app)
    .listen(5003, function() {
        console.log('Example app listening on port 5003! Go to https://localhost:5003/')
    }); */

/*var https = require('https');
var fs = require('fs');
 https.createServer({
        key: fs.readFileSync('server.key'),
        cert: fs.readFileSync('server.crt')
    }, app)
    .listen(5003, function() {
        console.log('Example app listening on port 5003! Go to https://localhost:5003/')
    }); */